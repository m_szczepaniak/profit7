<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 12px;">
	<div style="margin: 0 auto; width: 650px;">	
		<div style="margin: 0 0 36px;"><img src="logo_mail.png" width="200" height="76" /></div>
		
		<div style="margin: 0 0 15px; font-size: 16px; font-weight: bold; color: #000;">{$aModule.lang.new_order_user_email_thank_you}</div>
    
    <div style="margin: 0 0 15px;">{$aModule.lang.new_order_user_email_our_system} <strong style="font-size: 14px;">{$aModule.order.order_number}</strong></div>
    
    
    {if !empty($aModule.order.check_status_key)}
        Aby sprawdzić status zamówienia kliknij link: <a href="{$aModule.order.check_status_link}">Sprawdź status</a> 
        <br />
        Hasło zamówienia: <strong>{$aModule.order.check_status_key}</strong>
        <br />
    {/if}
    {*
    {if $aModule.order.activate_type == 1}
      <div style="margin: 0 0 34px; color: #000;">
        Twoje zamówienie zostało złożone, ale <strong style="color: #c30500;">nie jest jeszcze aktywne</strong>. 
        <br />Aby je aktywować kliknij lub skopiuj do paska adresu przeglądarki poniższy link:
        <br /><a href="{$aModule.order.activate_link}">{$aModule.order.activate_link}</a>
        <br /><br />

      </div>
    {elseif $aModule.order.activate_type == 2}
      <div style="margin: 0 0 34px; font-size: 14px;">
        Twoje zamówienie oraz konto zostało utworzone, ale <strong style="color: #c30500;">nie jest jeszcze aktywne</strong>. 
        <br />Aby je aktywować kliknij lub skopiuj do paska adresu przeglądarki poniższy link:
        <br /><a href="{$aModule.order.activate_link}">{$aModule.order.activate_link}</a>
        <br /><br />
      </div>
    {/if}
    *}
		
    
    
		{if $aModule.order.payment_type == 'bank_transfer' || $aModule.order.payment_type == 'bank_14days'}
			<div style="font-weight: bold; margin: 21px 0 17px 0;">{$aModule.lang.seller_data_header}</div>
			<div style="margin: 0 0 15px 16px; line-height: 150%;">
				{$aModule.lang.seller_name} <strong>{$aModule.seller_data.name}</strong><br/>
        {$aModule.lang.seller_bank_account} <strong>{$aModule.seller_data.bank_account}</strong><br/>
				{$aModule.lang.email_l.transfer_title} <strong>{*{$aModule.lang.email_l.order_no_login} *}{$aModule.order.order_number}</strong><br>
{*				{$aModule.lang.inv_street} <strong>{$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}</strong><br/>*}
{*				{$aModule.lang.inv_postal} <strong>{$aModule.seller_data.postal}</strong><br/>*}
{*				{$aModule.lang.inv_city} <strong>{$aModule.seller_data.city}</strong><br/>*}
{*				{$aModule.lang.seller_bank} <strong>{$aModule.seller_data.bank_name}</strong><br/>*}
        W tytule przelewu proszę podać <strong style="color: #c30500;">TYLKO numer zamówienia</strong> w innym przypadku może to spowodować opóźnienie księgowania.
			</div>
      
    
		{/if}
		
		
		<div style="font-size: 12px; font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0px;">Płatność:</div>
		<table cellspacing="1" cellpadding="1" style="border: 1px solid #e4e4e4; margin: 0 0 15px 0;font-size: 12px;">
			<tr>
				<th style="border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px; font-weight: normal;">{$aModule.lang.to_pay}</th>
				<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 8px; color: #c30500; font-weight: bold;">{$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency} z VAT</td>
			</tr>
			<tr>
				<th style="border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px; font-weight: normal;">{$aModule.lang.choose_payment_type}:</th>
				<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 8px; color: #c30500; font-weight: bold;">{$aModule.order.payment}</td>
			</tr>
		</table>
    <hr style="margin: 0 0 10px;">
      
    
{*		<div style="margin: 0 0 15px;">{$aModule.lang.new_order_user_email_working_hours}</div>*}
		<div style="margin: 0 0 15px;">{$aModule.shipment_info}</div>
    {if $aModule.order.payment_type == 'bank_transfer' || $aModule.order.payment_type == 'bank_14days'}
      <div style="">{$aModule.lang.new_order_user_email_until}</div>
      <div style="">{$aModule.lang.new_order_user_email_payment_booked}</div>
      <br />
    {/if}
    
    <hr style="margin: 0 0 15px;">
    
		<div style="margin: 0 0 15px; font-weight: bold;">{$aModule.order.mail_transport_info}</div>
    
		
		{if $aModule.order.transport_symbol == 'paczkomaty_24_7'}
			<div>
				<div style="font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0;">Adres paczkomatu na który wyślemy przesyłkę:</div>
					<div style="margin: 0 0 15px 16px; line-height: 150%;">
						{if is_array($aModule.order.point_of_receipt)}
							{$aModule.lang.city} <strong>{$aModule.order.point_of_receipt.town} </strong><br>
							{$aModule.lang.address}: <strong>{$aModule.order.point_of_receipt.street} {$aModule.order.point_of_receipt.buildingnumber}</strong><br>
							{if $aModule.order.point_of_receipt.locationdescription != ''}
								Opis: <strong>{$aModule.order.point_of_receipt.locationdescription}</strong>
							{/if}
						{else}
							Kod paczkomatu: {$aModule.order.point_of_receipt}
						{/if}
					</div>
			</div>
			<div>
				<div style="font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0;">Numer telefonu komórkowego dla Paczkomaty InPost:</div>
        <div style="margin: 0 0 15px 16px; line-height: 150%;">
          {$aModule.order.phone_paczkomaty}
        </div>
      </div>
		{else}

			{if !empty($aModule.addresses[1])}
				<div>
					<div style="font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0;">Adres na który wyślemy przesyłkę:</div>
						<div style="margin: 0 0 15px 16px; line-height: 150%;">
							{if $aModule.addresses[1].is_company == '1'}
								{$aModule.lang.company} <strong>{$aModule.addresses[1].company}</strong><br>
                Odbiorca przesyłki: <strong>{$aModule.addresses[1].name} {$aModule.addresses[1].surname}</strong><br>
								{*{$aModule.lang.nip} <strong>{$aModule.addresses[1].nip}</strong><br>*}
								{$aModule.lang.address}: <strong>{$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</strong><br>
								{$aModule.lang.postal} <strong>{$aModule.addresses[1].postal}</strong><br>
								{$aModule.lang.city} <strong>{$aModule.addresses[1].city}</strong><br>
							{else}
								{$aModule.lang.namesurname}: <strong>{$aModule.addresses[1].name} {$aModule.addresses[1].surname}</strong><br>
								{*{$aModule.lang.nip} <strong>---</strong><br>*}
								{$aModule.lang.address}: <strong>{$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</strong><br>
								{$aModule.lang.postal} <strong>{$aModule.addresses[1].postal}</strong><br>
								{$aModule.lang.city} <strong>{$aModule.addresses[1].city}</strong><br>
							{/if}
						</div>	
				</div>
			{/if}		
		{/if}
		
		{if !empty($aModule.addresses[0])}
			<div>
				<div style="font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0;">Dane do wystawienia faktury VAT:</div>
				<div style="margin: 0 0 15px 16px; line-height: 150%;">
					{if $aModule.addresses[0].is_company == '1'}
						{$aModule.lang.company} <strong>{$aModule.addresses[0].company}</strong><br>
						{if !empty($aModule.addresses[0].nip)}{$aModule.lang.nip} <strong>{$aModule.addresses[0].nip}</strong><br>{/if}
						{$aModule.lang.address}: <strong>{$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[0].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[0].city}</strong><br>
					{else}
						{$aModule.lang.namesurname}: <strong>{$aModule.addresses[0].name} {$aModule.addresses[0].surname}</strong><br>
						{*{$aModule.lang.nip} <strong>---</strong><br>*}
						{$aModule.lang.address}: <strong>{$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[0].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[0].city}</strong><br>
					{/if}
				</div>	
			</div>
		{/if}		
		
		{if !empty($aModule.addresses[2])}
			<div>
				<div style="font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0;">Dane do wystawienia drugiej faktury VAT:</div>
				<div style="margin: 0 0 15px 16px; line-height: 150%;">
					{if $aModule.addresses[2].is_company == '1'}
						{$aModule.lang.company}: <strong>{$aModule.addresses[2].company}</strong><br>
						{if !empty($aModule.addresses[2].nip)}{$aModule.lang.nip} <strong>{$aModule.addresses[2].nip}</strong><br>{/if}
						{$aModule.lang.address}: <strong>{$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[2].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[2].city}</strong><br>
					{else}
						{$aModule.lang.namesurname}: <strong>{$aModule.addresses[2].name} {$aModule.addresses[2].surname}</strong><br>
						{*{$aModule.lang.nip} <strong>---</strong><br>*}
						{$aModule.lang.address}: <strong>{$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[2].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[2].city}</strong><br>
					{/if}
				</div>
			</div>
		{/if}		
		
		<hr style="margin: 0 0 5px;">
		<div style="margin: 10px 0 15px 0;">{$aModule.lang.new_order_user_email_partial_realization}</div>
		
		<hr style="margin: 0 0 5px;">

		<div style="font-size: 12px; font-weight: bold; margin: 15px 0 10px 0; padding: 0 0 2px 0px;">{$aModule.lang.content_order1}</div>
		<table cellspacing="1" cellpadding="1" style="border: 1px solid #e4e4e4; font-size: 11px;">
			<tr>
				<th style="width: 50%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_name}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.email_shipment_time}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_quantity}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_price_brutto}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.total_products_brutto}</th>
			</tr>
			{foreach from=$aModule.items key=iId item=aItems name=types}
				<tr>
					{if $aItems.item_type == 'P'}
					<td style="border: 1px solid #e4e4e4; padding: 5px 10px 5px 0;">
						<table cellspacing="0" cellpadding="0" style="font-size: 11px;">
							<tr>
								<td valign="top"><img style="margin: 4px 0 0 0;" src="http://profit24.serwer/images/gfx/email-wciecie.gif" alt="" /></td>
								<td>
									<strong style="color: #c30500; font-size: 12px;">{$aItems.name}</strong><br/>
									{if !empty ($aItems.isbn)}
										{$aModule.lang.isbn} <strong>{$aItems.isbn}</strong>,
									{/if}
									{if !empty ($aItems.publication_year)}
										{$aModule.lang.publication_year} <strong>{$aItems.publication_year}</strong>,
									{/if}
									{if !empty ($aItems.edition)}
										{$aModule.lang.edition} <strong>{$aItems.edition}</strong>
									{/if}<br>
									{if !empty ($aItems.publisher)}
										{$aModule.lang.publisher} <strong>{$aItems.publisher}</strong><br>
									{/if}
									{if !empty ($aItems.authors)}
										{$aModule.lang.author} <strong>{$aItems.authors}</strong><br> 
									{/if}								
								</td>
							</tr>
						</table>
					</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{if $aItems.preview == '1'}{$aModule.lang.shipment_preview}<br/><strong>{$aItems.shipment_date}r.</strong>{else}{if !empty ($aItems.shipment_time)} {$aItems.shipment_time} {/if}{/if}</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{$aItems.quantity}</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{if $aItems.promo_price_brutto > 0}{$aItems.promo_price_brutto|format_number:2:',':' '}{else}{$aItems.price_netto|format_number:2:',':' '}{/if} {$aLang.common.currency}</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{$aItems.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</td>					
					{else}
					<td style="border: 1px solid #e4e4e4; padding: 5px 10px 5px 10px;">
						<strong style="color: #c30500; font-size: 12px;">{$aItems.name}</strong>{if $aItems.packet == '1'}<span><img style="margin: 0 0 -2px 8px;" src="http://profit24.serwer/images/gfx/email-pakiet.gif" alt="Pakiet" /></span>{/if}<br/>
						{if !empty ($aItems.isbn) &&  $aItems.packet != '1'}
							{$aModule.lang.isbn} <strong>{$aItems.isbn}</strong>,
						{/if}
						{if !empty ($aItems.publication_year)}
							{$aModule.lang.publication_year} <strong>{$aItems.publication_year}</strong>,
						{/if}
						{if !empty ($aItems.edition)}
							{$aModule.lang.edition} <strong>{$aItems.edition}</strong>
						{/if}<br>
						{if !empty ($aItems.publisher)}
							{$aModule.lang.publisher} <strong>{$aItems.publisher}</strong><br>
						{/if}
						{if !empty ($aItems.authors)}
							{$aModule.lang.author} <strong>{$aItems.authors}</strong><br> 
						{/if}
					</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;"><strong>{if $aItems.preview == '1'}{$aModule.lang.shipment_preview}<br/><strong>{$aItems.shipment_date}r.</strong>{else}{if !empty ($aItems.shipment_time)} {$aItems.shipment_time} {/if}{/if}</strong></td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;"><strong>{$aItems.quantity}</strong></td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;"><strong>{if $aItems.promo_price_brutto > 0}{$aItems.promo_price_brutto|format_number:2:',':' '}{else}{$aItems.price_netto|format_number:2:',':' '}{/if} {$aLang.common.currency}</strong></td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;"><strong>{$aItems.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></td>
					{/if}
				</tr>
			{/foreach}
		</table>
		<div style="text-align: right; line-height: 150%; margin-top: 24px;">
			<div>{$aModule.lang.total_cost_brutto} <strong>{$aModule.order.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
			<div>{$aModule.lang.total_transport_cost} <strong>{$aModule.order.transport_cost|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
			<div>{$aModule.lang.total_cost} <strong>{$aModule.order.total_value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
			<div style="margin-top: 5px;"><strong>{$aModule.lang.to_pay} <span style="font-size: 14px; color: #c30500; font-weight: bold;">{$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency}</span></strong></div>
		</div>
		
		<hr style="margin: 20px 0 20px;">
		
		{if !empty($aModule.order.transport_remarks)}
			<div style="margin: 0 0 34px;">
				<div style="font-size: 12px; font-weight: bold; margin: 10px 0 17px 0; padding: 0 0 2px 0;">{$aModule.lang.transport_remarks}</div>
				<div style="margin: 0 0 15px 16px; line-height: 150%;">
					{$aModule.order.transport_remarks}
				</div>
			</div>
		{/if}
		
		{if !empty($aModule.order.remarks)}
			<div style="margin: 0 0 34px;">
				<div style="font-size: 12px; font-weight: bold; margin: 10px 0; padding: 0 0 2px 0;">{$aModule.lang.remarks}:</div>
				<div style="margin: 0 0 15px 16px; line-height: 150%;">
					{$aModule.order.remarks}
				</div>
			</div>
		{/if}
		
		<div style="margin: 10px 0 34px;">{$aModule.lang.new_order_user_email_client_ip} {$aModule.order.client_ip}.</div>
		
		<div style="margin: 10px 0 34px;">{$aModule.lang.new_order_user_email_footer_info}</div>

		<div style="margin: 10px 0 34px;">{$aModule.lang.new_order_user_email_footer_info2}</div>

	</div>
</body>
</html>