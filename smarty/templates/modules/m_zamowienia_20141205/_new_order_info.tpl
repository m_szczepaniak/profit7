<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - Podsumowanie złożonego zamówienia</h1>
</div>
<div class="clear"></div>

<div class="summaryTop">
{*	<div class="summaryTopTitle">{$aModule.content}</div>*}
	<div class="summaryTopNr">
		{$aModule.lang.email_l.order_no_login} {$aModule.order.order_number}
		<div class="summaryTopNrInfo">{$aModule.lang.nr_info}</div>
    
    <div class="clear" style="margin-top: 15px"></div>
    
      <div class="summaryTopTitle">
        <strong style="font-size: 18px;">Uwaga ważne !</strong><br />
      {if !empty($aModule.login_message)}
          {$aModule.login_message}
      {/if}
    </div>
	</div>
  {*
	<div class="summaryTopSeller">
		{if !empty($aModule.seller_data)}
			<div class="title">{$aModule.lang.payment_type_}:</div>
			<div class="data">{$aModule.order.payment}</div>
			<div class="clear"></div>
			
			{if $aModule.order.payment_type!='bank_14days'}
			<div class="title">{$aModule.lang.transfer_value}:</div>
			<div class="data" style="color: #c30500;">{$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency}</div>
			<div class="clear"></div>
				<div class="title">{$aModule.lang.seller_data_header}</div>
				<div class="data">
					{$aModule.seller_data.name}<br />
					{$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br />
					{$aModule.seller_data.postal} {$aModule.seller_data.city}<br />
					{$aModule.seller_data.bank_name}<br />
					{$aModule.seller_data.bank_account}<br />				
				</div>
				<div class="clear"></div>
				<div class="title">{$aModule.lang.email_l.transfer_title}</div>
				<div class="data">{$aModule.order.order_number}</div>
			{/if}
		{/if}	
	</div>
  *}
</div>

<div style="float: left;margin: 6px 0 28px 13px;width: 945px;display: inline;">
{*
	{if !empty($aModule.addresses[0])}
	<div class="step2Option step2OptionSummary">
		<div class="koszykPodtytul2"><strong>Dane do wystawienia faktury VAT:</strong></div>
		<div class="mojeAdresy">
			<div class="mojeAdresySrodek">
				{if $aModule.addresses[0].is_company == '1'}
					<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.addresses[0].company}</div>
					<div class="clear"></div>
					{if !empty($aModule.addresses[0].nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.addresses[0].nip}</div>
					<div class="clear"></div>{/if}
					<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</div>
					<div class="clear"></div>
					<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.addresses[0].postal} {$aModule.addresses[0].city}</div>
				{else}
					<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.addresses[0].name} {$aModule.addresses[0].surname}</div>
					<div class="clear"></div>
					<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</div>
					<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.addresses[0].postal} {$aModule.addresses[0].city}</div>
				{/if}
			</div>
		</div>	
	</div>
	{/if}
	
		{if $aModule.order.transport_symbol == 'paczkomaty_24_7'}
			<div class="step2Option step2OptionSummary">
				<div class="koszykPodtytul2"><strong>Dane dla Paczkomaty InPost:</strong></div>
				<div class="mojeAdresy">
					<div class="mojeAdresySrodek">
              <div class="daneGora">Adres paczkomatu:</div>
						{if is_array($aModule.order.point_of_receipt)}
							<div class="daneLewo" style="width: 40px">{$aModule.lang.city}</div><div class="danePrawo"> {$aModule.order.point_of_receipt.town}</div>
							<div class="clear"></div>
							<div class="daneLewo" style="width: 40px">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.order.point_of_receipt.street} {$aModule.order.point_of_receipt.buildingnumber}</div>
							<div class="clear"></div>
							{if $aModule.order.point_of_receipt.locationdescription != ''}
								<div class="daneLewo" style="width: 40px">Opis:</div><div class="danePrawo"> {$aModule.order.point_of_receipt.locationdescription}</div>
							{/if}
						{else}
							<div class="daneLewo" style="width: 40px">Kod paczkomatu:</div><div class="danePrawo"> {$aModule.order.point_of_receipt}</div>
						{/if}
            <div class="clear"></div><br />
            <div class="daneGora">Numer telefonu komórkowego:</div>
            <div class="daneDol" style="width: 40px">{$aModule.order.phone_paczkomaty}</div>
					</div>
				</div>	
			</div>
		{else}
			{if !empty($aModule.addresses[1])}
			<div class="step2Option step2OptionSummary">
				<div class="koszykPodtytul2"><strong>Adres na który wyślemy przesyłkę:</strong></div>
				<div class="mojeAdresy">
					<div class="mojeAdresySrodek">
						{if $aModule.addresses[1].is_company == '1'}
							<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.addresses[1].company}</div>
							<div class="clear"></div>
              {if !empty($aModule.addresses[1].name) || !empty($aModule.addresses[1].surname)}
                <div class="daneLewo">Odbiorca przesyłki:</div><div class="danePrawo"> {$aModule.addresses[1].name} {$aModule.addresses[1].surname}</div>
                <div class="clear"></div>
              {/if}
							<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</div>
							<div class="clear"></div>
							<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.addresses[1].postal} {$aModule.addresses[1].city}</div>
						{else}
							<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.addresses[1].name} {$aModule.addresses[1].surname}</div>
							<div class="clear"></div>
							<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</div>
							<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.addresses[1].postal} {$aModule.addresses[1].city}</div>
						{/if}
					</div>
				</div>	
			</div>
			{/if}
	{/if}
	
	{if !empty($aModule.addresses[2])}
	<div class="step2Option step2OptionSummary">
		<div class="koszykPodtytul2"><strong>Dane do wystawienia drugiej faktury VAT:</strong></div>
		<div class="mojeAdresy">
			<div class="mojeAdresySrodek">
				{if $aModule.addresses[2].is_company == '1'}
					<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.addresses[2].company}</div>
					<div class="clear"></div>
					{if !empty($aModule.addresses[2].nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.addresses[2].nip}</div>
					<div class="clear"></div>{/if}
					<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</div>
					<div class="clear"></div>
					<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.addresses[2].postal} {$aModule.addresses[2].city}</div>
				{else}
					<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.addresses[2].name} {$aModule.addresses[2].surname}</div>
					<div class="clear"></div>
					<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</div>
					<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.addresses[2].postal} {$aModule.addresses[2].city}</div>
				{/if}
			</div>
		</div>
	</div>
	{/if}
	*}
	{* uwagi do zamowienia 
	{if !empty($aModule.order.transport_remarks) || !empty($aModule.order.remarks)}
	<div class="step2Option  step2OptionSummary">
		<div class="koszykPodtytul2"><strong>Uwagi do zamówienia:</strong></div>
		<div class="mojeAdresy">
			<div class="mojeAdresySrodek">
				{if !empty($aModule.order.transport_remarks)}
					<div class="daneGora">{$aModule.lang.transport_remarks}</div>
					<div class="daneDol"> {$aModule.order.transport_remarks}</div>
					<div class="clear"></div>				
				{/if}
				{if !empty($aModule.order.remarks)}
					<div class="daneGora">{$aModule.lang.remarks}:</div>
					<div class="daneDol"> {$aModule.order.remarks}</div>
					<div class="clear"></div>	
				{/if}
			</div>
		</div>	
	</div>
	{/if}
	*}
	<div class="clear"></div>
	<div class="summaryButton">
		<a href="{if $smarty.session.w_user.id > 0 || $smarty.session.wu_cart.step_3_logged_out.registration == '1'}{$aModule.links.user_orders_page}{else}/sprawdz-status-zamowienia{/if}"></a>
	</div>
	


</div>

{*
<div class="koszykKomunikat">
	<strong></strong><br /><br />
	<span><strong>asdasdasd{$aModule.lang.user_data_header}:</strong></span><br />
	<div class="clear" style="height: 20px;"></div>
	<a href="/" class="sendMax" style="float: left;"></a>
</div>
*}