<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 4/4 (Podsumowanie danych kupującego)</h1>
</div>
<div class="clear"></div>
<div id="step2" class="koszykStep4">
  {$aModule.login_box}
  {$aModule.form.validator.header}
	{$aModule.form.validator.err_prefix}
	{$aModule.form.validator.err_postfix}
	{$aModule.form.validator.validator}
	{$aModule.form.validator.ajaxval}
  
  {if !empty($aModule.form.invoice)}
  <div class="step2Option step2Option_1" style="text-align: left; border-left: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Dane, na które wystawimy <br />fakturę{if $aModule.form.delivery.delivery_data_from_invoice == '1'} oraz wyślemy przesyłkę{/if}:</strong></div>
      {if $aModule.form.invoice.is_company == '1'}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Firma:</div>
          <div class="left">{$aModule.form.invoice.company}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">NIP:</div>
          <div class="left">{$aModule.form.invoice.nip}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        {if $aModule.form.delivery.delivery_data_from_invoice == '1'}
          <div class="clear"></div>
          <div class="fRow">
            <div class="fLabel" style="padding-top:0;">Osoba odbierająca:</div>
            <div class="left">{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div class="fRow">
            <div class="fLabel" style="padding-top:0;">Telefon:</div>
            <div class="left">{$aModule.form.recipient.phone}</div>
            <div class="clear"></div>
          </div>
        {/if}
      {else}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Imię i nazwisko:</div>
          <div class="left">{$aModule.form.invoice.name} {$aModule.form.invoice.surname}</div>
          <div class="clear"></div>
        </div>
      {/if}
      <div class="clear"></div>
      <div class="fRow">
				<div class="fLabel" style="padding-top:0;">Adres:</div>
				<div class="left">{$aModule.form.invoice.postal} {$aModule.form.invoice.city} <br />{$aModule.form.invoice.street} {$aModule.form.invoice.number}{if !empty($aModule.form.invoice.number2)}/{$aModule.form.invoice.number2}{/if}</div>
				<div class="clear"></div>
			</div>
      <div class="clear"></div>
  </div>
  {/if}
  
  {if !empty($aModule.form.second_invoice)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Dane, na które wystawimy <br />drugą faktura:</strong></div>
      {if $aModule.form.second_invoice.is_company == '1'}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Firma:</div>
          <div class="left">{$aModule.form.second_invoice.company}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">NIP:</div>
          <div class="left">{$aModule.form.second_invoice.nip}</div>
          <div class="clear"></div>
        </div>
      {else}
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Imię i nazwisko:</div>
          <div class="left">{$aModule.form.second_invoice.name} {$aModule.form.second_invoice.surname}</div>
          <div class="clear"></div>
        </div>
      {/if}
      <div class="clear"></div>
      <div class="fRow">
				<div class="fLabel" style="padding-top:0;">Adres:</div>
				<div class="left">{$aModule.form.second_invoice.postal} {$aModule.form.second_invoice.city} <br />{$aModule.form.second_invoice.street} {$aModule.form.second_invoice.number}{if !empty($aModule.form.second_invoice.number2)}/{$aModule.form.second_invoice.number2}{/if}</div>
				<div class="clear"></div>
			</div>
      <div class="clear"></div>
  </div>
  {/if}

  {if !empty($aModule.form.delivery.city) && $aModule.form.delivery.delivery_data_from_invoice != '1'}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres, na który wyślemy <br />Twoją przesyłkę:</strong></div>
      {if $aModule.form.delivery.is_company == '1'}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Firma:</div>
          <div class="left">{$aModule.form.delivery.company}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Adres:</div>
          <div class="left">{$aModule.form.delivery.postal} {$aModule.form.delivery.city} <br />{$aModule.form.delivery.street} {$aModule.form.delivery.number}{if !empty($aModule.form.delivery.number2)}/{$aModule.form.delivery.number2}{/if}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Osoba odbierająca:</div>
          <div class="left">{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Telefon:</div>
          <div class="left">{$aModule.form.recipient.phone}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        {*<div class="fRow">
          <div class="fLabel" style="padding-top:0;">NIP:</div>
          <div class="left">{$aModule.form.delivery.nip}</div>
          <div class="clear"></div>
        </div>*}
        <div class="clear"></div>
      {else}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Imię i nazwisko:</div>
          <div class="left">{$aModule.form.delivery.name} {$aModule.form.delivery.surname}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Adres:</div>
          <div class="left">{$aModule.form.delivery.postal} {$aModule.form.delivery.city} <br />{$aModule.form.delivery.street} {$aModule.form.delivery.number}{if !empty($aModule.form.delivery.number2)}/{$aModule.form.delivery.number2}{/if}</div>
          <div class="clear"></div>
        </div>
        {if !empty($aModule.form.recipient_phone.phone)}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Telefon:</div>
          <div class="left">{$aModule.form.recipient_phone.phone}</div>
          <div class="clear"></div>
        </div>
        {/if}
      {/if}
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if}
  
{if !empty($aModule.form.paczkomaty.point_of_receipt)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres paczkomatu, na który wyślemy <br />Twoją przesyłkę:</strong></div>
      <div class="fRow">
				<div class="left" id="point_of_receipt_description">
          <input type="hidden" id="point_of_receipt" name="point_of_receipt" value="{$aModule.form.paczkomaty.point_of_receipt}">
          <input type="hidden" id="point_of_receipt_sel" name="point_of_receipt_sel" value="{$aModule.form.paczkomaty.point_of_receipt_sel}">
        </div>
				<div class="clear" style="margin-bottom: 10px;"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0; width: auto;">Numer telefonu komórkowego:</div>
          <div class="left"><strong>{$aModule.form.paczkomaty.phone_paczkomaty}</strong></div>
          <div class="clear"></div>
        </div>
			</div>
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if}
  
  <div class="clear" style="margin-bottom: 40px;"></div>

  
    <div class="koszykPodtytul2">
      <strong>Uwagi dotyczące zamówienia:</strong>
      {if $aModule.form.is_paczkomaty != TRUE}
        <div class="koszykPodtytul2Info">
          Pamiętaj, że kurierzy pracują w godzinach 10 - 17, więc wskazane jest podanie adresu pod którym przebywasz w tych godzinach. 
        </div>
      {/if}
    </div>
  
  
  
    <div class="clear"></div>
	
    <div class="fRowRemarks" >
      <div class="fLabel" style="width: 550px;"><strong>Uwagi dotyczące zamówienia:</strong> <span style="color: #c30500;font-size: 12px;">(pole uwagi nie służy do wpisywania adresów)</span></div>
      <div class="fInput">{$aModule.form.remarks.remarks.input}</div>
    </div>
    
  {if $aModule.form.is_paczkomaty != TRUE}
    <div class="fRowRemarks fRowRemarks_2" style="width: 325px;padding: 0; float: left !important;" id="transport_info">
      <div class="fLabel" style="width: 325px;"><strong>Uwagi dla kuriera:</strong><span style="color: #c30500;font-size: 12px;">(np. nie działa domofon)</span> (max. 150 zn.)</div>
      <div class="fInput">{$aModule.form.remarks.transport_remarks.input}</div>
    </div>
  {/if}
  
  
  <div class="clear" style="height: 30px;"></div>
  
	
  {** BUTTONY **}
  <div class="buttonLine buttonLineBig" style="margin-top: 20px;">
    <a href="/koszyk/step3.html" class="left" style="float: left;background: url('/images/gfx/button-wroc.gif') no-repeat;width: 85px;height: 33px;" title="Wróć"></a>

    <input type="image" src="/images/gfx/button-skladam-zamowienie.gif" value="Składam zamówienie" name="send" alt="Składam zamówienie" tabindex="24" style="cursor: pointer; float: right;" />
  </div>
  
  
  {$aModule.form.validator.footer}
        
</div>
  
<script type="text/javascript">
  {literal}
    $(document).ready(function(){
      
      function formAddress(aData){
        var sStr;
        sStr = '<strong>'+aData[3]+' '+aData[4]+'<br />';
        sStr += aData[1]+' '+aData[2]+'</strong><br /><br />';
        sStr += aData[9]+'<br />';
        return sStr;
      }
      
      if ($("#point_of_receipt").length > 0 && $("#point_of_receipt").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetPaczkomatyMachines.php?term='+$("#point_of_receipt_sel").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          var aData = $.parseJSON(returnData[0].data);
          $("#point_of_receipt_description").html(formAddress(aData));
        });
      }
    });
  {/literal}
</script>