	{$aModule.form.JS}
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{foreach from=$aModule.form.fields.hidden item=f}
		{$f}
	{/foreach}
	

	<input type="hidden" name="payment_id" id="payment_id" value="{$aModule.payment_id}" />
	
	<div class="fRow" style="margin: 10px 0">
		<div class="fInput3" style="float: left;">{$aModule.form.fields.invoice_section.invoice.input}</div>
		<div class="fLabel3" style="float: left; padding: 2px 0 0 5px;">{$aModule.form.fields.invoice_section.invoice.label}</div>
		<div class="clear"></div>
	</div>

	<div class="clear" style="width: 505px;">
		<div class="left_cart">
			<div class="cnt2Header"><span><strong>{$aModule.lang.address_data_header}</strong></span></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.company.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.company.input}</div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.name.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.name.input}</div>
			</div>
								<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.surname.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.surname.input}</div>
			</div>
								<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.street.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.street.input}</div>
			</div>
								<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.number.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.number.input}</div> <div style="float: left; padding-top: 2px; padding-right: 6px">/</div> <div class="fInput_small">{$aModule.form.fields.address_section.number2.input}</div>
			</div>
								<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.postal.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.postal.input}</div>
			</div>
								<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.city.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.city.input}</div>
			</div>
								<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel_small">{$aModule.form.fields.address_section.phone.label}</div>
				<div class="fInput_small">{$aModule.form.fields.address_section.phone.input}</div>
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="right_cart">
			<div class="cnt2Content" id="invoice_data" style="display: {if isset($smarty.post.invoice)}block{else}none{/if};">
				<div class="cnt2Header"><span><strong>{$aModule.lang.invoice_data_header}</strong></span></div>
				{$aModule.form.fields.invoice_section.invoice_company.hidden}
				{$aModule.form.fields.invoice_section.invoice_nip.hidden}
				{$aModule.form.fields.invoice_section.invoice_name.hidden}
				{$aModule.form.fields.invoice_section.invoice_surname.hidden}
				{$aModule.form.fields.invoice_section.invoice_street.hidden}
				{$aModule.form.fields.invoice_section.invoice_number.hidden}
				{$aModule.form.fields.invoice_section.invoice_number2.hidden}
				{$aModule.form.fields.invoice_section.invoice_postal.hidden}
				{$aModule.form.fields.invoice_section.invoice_city.hidden}
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_company.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_company.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_nip.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_nip.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_name.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_name.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_surname.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_surname.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_street.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_street.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_number.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_number.input}</div> <div style="float: left; padding-top: 2px; padding-right: 6px;">/</div> <div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_number2.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_postal.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_postal.input}</div>
				</div>
				<div class="clear"></div>
				<div class="fRow">
					<div class="fLabel_small">{$aModule.form.fields.invoice_section.invoice_city.label}</div>
					<div class="fInput_small">{$aModule.form.fields.invoice_section.invoice_city.input}</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div style="margin-top: 10px; float: left; padding-left: 100px;">
		<div style="float: left;"><input type="submit" class="input_button_2" name="order" value="Zamawiam" /></div>
	</div>
	<div class="clear"></div>
	<div class="fRequiredF left">{$aLang.form.required_fields}</div>
	{*<div class="clear"></div>*}	
{$aModule.form.footer}
