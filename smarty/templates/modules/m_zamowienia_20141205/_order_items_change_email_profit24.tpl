<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 12px;">
	<div style="margin: 0 auto; width: 650px;">	
		<div style="margin: 10px 0 36px;"><img src="logo_mail.png" width="200" height="76" /></div>
		
		<div style="margin: 0 0 22px; font-size: 16px;font-weight: bold;color: #c30500;">W Twoim zamówieniu nr {$aModule.order.order_number} zostały dokonane zmiany</div>
				
			
		{if !empty($aModule.addresses[1])}
			<div>
				<div style="font-weight: bold; margin: 21px 0 7px 0; padding: 0 0 2px 0;">Adres na który wyślemy przesyłkę:</div>
				<div style="margin: 0 0 30px 16px; line-height: 150%;">
					{if $aModule.addresses[1].is_company == '1'}
						{$aModule.lang.company}: <strong>{$aModule.addresses[1].company}</strong><br>
						{*{$aModule.lang.nip} <strong>{$aModule.addresses[1].nip}</strong><br>*}
						{$aModule.lang.address}: <strong>{$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[1].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[1].city}</strong><br>
					{else}
						{$aModule.lang.namesurname}: <strong>{$aModule.addresses[1].name} {$aModule.addresses[1].surname}</strong><br>
						{*{$aModule.lang.nip} <strong>---</strong><br>*}
						{$aModule.lang.address}: <strong>{$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[1].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[1].city}</strong><br>
					{/if}
				</div>
			</div>
		{/if}
		
		{if !empty($aModule.addresses[0])}
			<div>
				<div style="font-weight: bold; margin: 21px 0 7px 0; padding: 0 0 2px 0;">Dane do wystawienia faktury VAT:</div>
				<div style="margin: 0 0 30px 16px; line-height: 150%;">
					{if $aModule.addresses[0].is_company == '1'}
						{$aModule.lang.company} <strong>{$aModule.addresses[0].company}</strong><br>
						{if !empty($aModule.addresses[0].nip)}{$aModule.lang.nip} <strong>{$aModule.addresses[0].nip}</strong><br>{/if}
						{$aModule.lang.address}: <strong>{$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[0].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[0].city}</strong><br>
					{else}
						{$aModule.lang.namesurname}: <strong>{$aModule.addresses[0].name} {$aModule.addresses[0].surname}</strong><br>
						{*{$aModule.lang.nip} <strong>---</strong><br>*}
						{$aModule.lang.address}: <strong>{$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[0].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[0].city}</strong><br>
					{/if}
				</div>	
			</div>
		{/if}		
		
		{if !empty($aModule.addresses[2])}
			<div>
				<div style="font-weight: bold; margin: 21px 0 7px 0; padding: 0 0 2px 0;">Dane do wystawienia drugiej faktury VAT:</div>
				<div style="margin: 0 0 30px 16px; line-height: 150%;">
					{if $aModule.addresses[2].is_company == '1'}
						{$aModule.lang.company}: <strong>{$aModule.addresses[2].company}</strong><br>
						{if !empty($aModule.addresses[2].nip)}{$aModule.lang.nip} <strong>{$aModule.addresses[2].nip}</strong><br>{/if}
						{$aModule.lang.address}: <strong>{$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[2].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[2].city}</strong><br>
					{else}
						{$aModule.lang.namesurname}: <strong>{$aModule.addresses[2].name} {$aModule.addresses[2].surname}</strong><br>
						{*{$aModule.lang.nip} <strong>---</strong><br>*}
						{$aModule.lang.address}: <strong>{$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</strong><br>
						{$aModule.lang.postal} <strong>{$aModule.addresses[2].postal}</strong><br>
						{$aModule.lang.city} <strong>{$aModule.addresses[2].city}</strong><br>
					{/if}
				</div>
			</div>
		{/if}		
		
		<hr style="margin: 0 0 20px;">
		
		<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">Płatność:</div>
		<table cellspacing="1" cellpadding="1" style="border: 1px solid #e4e4e4; margin: 0 0 30px 0;font-size: 12px;">
			<tr>
				<th style="color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.to_pay}</th>
				<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 8px;">{$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency} z VAT</td>
			</tr>
			<tr>
				<th style="color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.choose_payment_type}:</th>
				<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 8px;">{$aModule.order.payment}</td>
			</tr>
		</table>
		
		{if $aModule.order.payment_type == 'bank_transfer' || $aModule.order.payment_type == 'bank_14days'}
			<div style="font-weight: bold; margin: 21px 0 17px 0;">{$aModule.lang.seller_data_header}</div>
			<div style="margin: 0 0 30px 16px; line-height: 150%;">
				{$aModule.lang.seller_name} <strong>{$aModule.seller_data.name}</strong><br/>
				{$aModule.lang.inv_street} <strong>{$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}</strong><br/>
				{$aModule.lang.inv_postal} <strong>{$aModule.seller_data.postal}</strong><br/>
				{$aModule.lang.inv_city} <strong>{$aModule.seller_data.city}</strong><br/>
				{$aModule.lang.seller_bank} <strong>{$aModule.seller_data.bank_name}</strong><br/>
				{$aModule.lang.seller_bank_account} <strong>{$aModule.seller_data.bank_account}</strong><br/>
				{$aModule.lang.email_l.transfer_title} <strong>{*{$aModule.lang.email_l.order_no_login} *}{$aModule.order.order_number}</strong><br>
			</div>
		{/if}
		
		<hr style="margin: 0 0 20px;">

		<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">{$aModule.lang.content_order1}</div>
		<table cellspacing="1" cellpadding="1" style="border: 1px solid #e4e4e4; font-size: 11px;">
			<tr>
				<th style="width: 50%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_name}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.email_shipment_time}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_quantity}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_price_brutto}</th>
				<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.total_products_brutto}</th>
			</tr>
			{foreach from=$aModule.items key=iId item=aItems name=types}
				<tr>
					<td style="border: 1px solid #e4e4e4; padding: 5px 10px 5px 10px;{if $aItems.deleted=='1'} text-decoration: line-through;{/if}">
						<strong style="color: #c30500; font-size: 14px;">{$aItems.name}</strong><br/>
						{if !empty ($aItems.isbn)}
							{$aModule.lang.isbn} <strong>{$aItems.isbn}</strong>,
						{/if}
						{if !empty ($aItems.publication_year)}
							{$aModule.lang.publication_year} <strong>{$aItems.publication_year}</strong>,
						{/if}
						{if !empty ($aItems.edition)}
							{$aModule.lang.edition} <strong>{$aItems.edition}</strong>
						{/if}<br>
						{if !empty ($aItems.publisher)}
							{$aModule.lang.publisher} <strong>{$aItems.publisher}</strong><br>
						{/if}
						{if !empty ($aItems.authors)}
							{$aModule.lang.author} <strong>{$aItems.authors}</strong><br> 
						{/if}
					</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;{if $aItems.deleted=='1'} text-decoration: line-through;{/if}">{if $aItems.preview == '1'}{$aModule.lang.shipment_preview}<br/><strong>{$aItems.shipment_date}r.</strong>{else}{if !empty ($aItems.shipment_time)} {$aItems.shipment_time} {/if}{/if}</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;{if $aItems.deleted=='1'} text-decoration: line-through;{/if}">{$aItems.quantity}</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;{if $aItems.deleted=='1'} text-decoration: line-through;{/if}">{if $aItems.promo_price_brutto > 0}{$aItems.promo_price_brutto|format_number:2:',':' '}{else}{$aItems.price_netto|format_number:2:',':' '}{/if} {$aLang.common.currency}</td>
					<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;{if $aItems.deleted=='1'} text-decoration: line-through;{/if}">{$aItems.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</td>
				</tr>
			{/foreach}
		</table>
		<div style="text-align: right; line-height: 150%; margin-top: 24px;">
			<div>{$aModule.lang.total_cost_brutto} <strong>{$aModule.order.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
			<div>{$aModule.lang.total_transport_cost} <strong>{$aModule.order.transport_cost|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
			<div>{$aModule.lang.total_cost} <strong>{$aModule.order.total_value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
			<div style="margin-top: 5px;"><strong>{$aModule.lang.to_pay} <span style="font-size: 14px; color: #c30500; font-weight: bold;">{$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency}</span></strong></div>
		</div>
		
		<hr style="margin: 20px 0 20px;">
		
		{if !empty($aModule.order.transport_remarks)}
			<div style="margin: 0 0 32px;">
				<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">{$aModule.lang.transport_remarks}</div>
				<div style="margin: 0 0 30px 16px; line-height: 150%;">
					{$aModule.order.transport_remarks}
				</div>
			</div>
		{/if}
		
		{if !empty($aModule.order.remarks)}
			<div style="margin: 0 0 32px;">
				<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">{$aModule.lang.remarks}:</div>
				<div style="margin: 0 0 30px 16px; line-height: 150%;">
					{$aModule.order.remarks}
				</div>
			</div>
		{/if}
		
		<div style="margin: 10px 0 32px;">{$aModule.lang.new_order_user_email_client_ip} {$aModule.order.client_ip}.</div>
		
		<div style="margin: 10px 0 32px;">{$aModule.lang.new_order_user_email_footer_info}</div>

		<div style="margin: 10px 0 32px;">{$aModule.lang.new_order_user_email_footer_info2}</div>
	</div>
</body>
</html>