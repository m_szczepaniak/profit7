<div class="content">
	{$aModule.form.JS}
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{foreach from=$aModule.form.fields.hidden item=f}
		{$f}
	{/foreach}
	<h2 class="podtytulOpis">{$aModule.lang.choose_payment_type}</h2>
	<div class="fRow">
		<div class="fInput" style="padding-left: 6px;">{$aModule.form.fields.payment_choose.inputs}</div>
		<div class="clear"></div>
	</div>
	<h2 class="podtytulOpis">{$aModule.lang.user_data_header}</h2>
	{if isset($aModule.user_data.company) && isset($aModule.user_data.nip)}
		<div class="daneCiemne">
			<div class="daneEtykieta">{$aModule.lang.company}</div>
			<div class="daneTresc"><strong>{$aModule.user_data.company}</strong></div>
		</div>
		<div class="daneJasne">
			<div class="daneEtykieta">{$aModule.lang.inv_nip}</div>
			<div class="daneTresc"><strong>{$aModule.user_data.nip}</strong></div>
		</div>
	{/if}
	<div class="daneCiemne">
		<div class="daneEtykieta">{$aModule.lang.name_surname}</div>
		<div class="daneTresc"><strong>{$aModule.user_data.name} {$aModule.user_data.surname}</strong></div>
	</div>
	<div class="daneJasne">
		<div class="daneEtykieta">{$aModule.lang.street}</div>
		<div class="daneTresc"><strong>{$aModule.user_data.street} {$aModule.user_data.number}{if isset($aModule.user_data.number2)}/{$aModule.user_data.number2}{/if}</strong></div>
	</div>
	<div class="daneCiemne">
		<div class="daneEtykieta">{$aModule.lang.city}</div>
		<div class="daneTresc"><strong>{$aModule.user_data.postal} {$aModule.user_data.city}</strong></div>
	</div>
	<div class="daneJasne ostatnie">
		<div class="daneEtykieta">{$aModule.lang.email}</div>
		<div class="daneTresc"><strong>{$aModule.user_data.email}</strong></div>
	</div>
	
	<h2 class="podtytulOpis">{$aModule.lang.invoice_data_header}</h2>
	<div class="clear" style="padding-left: 13px;">
		<div class="fRow">
			<div class="fInput2">{$aModule.form.fields.inv_invoice.input}</div>
			<div class="fLabel2">{$aModule.form.fields.inv_invoice.label}</div>
			<div class="clear"></div>
		</div>
		<div id="invoice_data" style="display: {if isset($smarty.post.invoice)}block{else}none{/if};">
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_company.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_company.input}</div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_nip.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_nip.input}</div>
			</div>
			<div class="fSpacer"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_name.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_name.input}</div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_surname.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_surname.input}</div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_street.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_street.input}</div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_number.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_number.input} {$aModule.form.fields.inv_number2.input}</div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_postal.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_postal.input}</div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.fields.inv_city.label}</div>
				<div class="fInput">{$aModule.form.fields.inv_city.input}</div>
			</div>
		</div>
	</div>
	<div style="padding: 10px 0; text-align: right;">
		<input type="image" src="/images/gfx/{$sLang}/realizuj_zamowienie.gif" />
	</div>
	{$aModule.form.footer}
</div>