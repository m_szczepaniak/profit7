{if !empty($aAddress.item)}
	<div class="mojeAdresy">
		<div class="mojeAdresySrodek">
			{if $aAddress.item.is_company == '1'}
				<div class="daneLewo">{$aAddress.lang.company}:</div><div class="danePrawo"> {$aAddress.item.company}</div>
				<div class="clear"></div>
    {if !$aAddress.is_transport}<div class="daneLewo">{if !empty($aAddress.item.nip)}{$aAddress.lang.nip}{else}<span style="color: #c30500">{$aAddress.lang.nip}</span>{/if}</div><div class="danePrawo"> {if !empty($aAddress.item.nip)}{$aAddress.item.nip}{else}<span style="color: #c30500">Brak NIP</span> {/if}</div>
				<div class="clear"></div>{/if}
				<div class="daneLewo">{$aAddress.lang.address}:</div><div class="danePrawo"> {$aAddress.item.street} {$aAddress.item.number}{if !empty($aAddress.item.number2)}/{$aAddress.item.number2}{/if}</div>
				<div class="clear"></div>
				<div class="daneLewo">{$aAddress.lang.postal}</div><div class="danePrawo"> {$aAddress.item.postal} {$aAddress.item.city}</div>
        {if $aAddress.is_transport == true}
          <input type="hidden" name="is_delivery_invoice" id="is_delivery_invoice" value="1" />
        {else}
          <input type="hidden" name="select_nip_adress" class="select_nip_adress" value="{if !empty($aAddress.item.nip)}{$aAddress.item.nip}{else}0{/if}" />
        {/if}
			{else}
				<div class="daneLewo">{$aAddress.lang.namesurname}:</div>
        <div class="danePrawo"> <span class="addr_name">{$aAddress.item.name}</span> <span class="addr_surname">{$aAddress.item.surname}</span></div>
				<div class="clear"></div>
				{*<div class="daneLewo">{$aAddress.lang.nip}</div><div class="danePrawo"> ---</div>
				<div class="clear"></div>*}
				<div class="daneLewo">{$aAddress.lang.address}:</div><div class="danePrawo"> {$aAddress.item.street} {$aAddress.item.number}{if !empty($aAddress.item.number2)}/{$aAddress.item.number2}{/if}</div>
				<div class="clear"></div>
        <div class="daneLewo">{$aAddress.lang.postal}</div><div class="danePrawo"> {$aAddress.item.postal} {$aAddress.item.city}</div>
			{/if}
		</div>
	</div>
{/if}