<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 3/4 (Dane kupującego)</h1>
</div>
<div class="clear"></div>
<div id="step2">
	{if !empty($aModule.form.invoice_address)}
    
    
  {$aModule.form.validator.header}
  {$aModule.form.validator.err_prefix}
  {$aModule.form.validator.err_postfix}
  {$aModule.form.validator.validator}
  {$aModule.form.validator.ajaxval}
  <input type="hidden" id="recipient_email_hidden" name="recipient_email_hidden" value="{$smarty.session.wu_cart.user_data.phone}" />

		<div class="step2Option step2Option_1" style="width: 320px; padding: 0 !important;">
			<div class="koszykPodtytul2"><strong>{$aModule.lang.invoice_address}:</strong></div>
			<div class="fRow">
				<div class="fLabel">{*{$aModule.form.invoice_address.label}*}Dane adresowe:</div>
				<div class="fInput">{$aModule.form.invoice_address.input}</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div id="invoice_address_section">
			{if !empty($aModule.invoice_address)}
				<div class="mojeAdresy">
					<div class="mojeAdresySrodek">
						{if $aModule.invoice_address.is_company == '1'}
							<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.invoice_address.company}</div>
							<div class="clear"></div>
							{if !empty($aModule.invoice_address.nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.invoice_address.nip}</div>
							<div class="clear"></div>{/if}
							<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.invoice_address.street} {$aModule.invoice_address.number}{if !empty($aModule.invoice_address.number2)}/{$aModule.invoice_address.number2}{/if}</div>
							<div class="clear"></div>
							<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.invoice_address.postal} {$aModule.invoice_address.city}</div>
						{else}
							<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.invoice_address.name} {$aModule.invoice_address.surname}</div>
							<div class="clear"></div>
							{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
							<div class="clear"></div>*}
							<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.invoice_address.street} {$aModule.invoice_address.number}{if !empty($aModule.invoice_address.number2)}/{$aModule.invoice_address.number2}{/if}</div>
							<div class="clear"></div>
							<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.invoice_address.postal} {$aModule.invoice_address.city}</div>
						{/if}
					</div>
				</div>
			{/if}
			</div>
			<div id="invoice_address_section_loading" style="display: none;" class="loading">{$aLang.common.loading}</div>
		</div>

    {if !empty($aModule.form.invoice2_address)}
      <div class="step2Option step2Option_2" style="width: 300px; padding: 0 !important;">
        <div class="koszykPodtytul2"><strong>{$aModule.lang.second_invoice_title}:</strong></div>
        <div id="invoice2_group">
          <div class="fRow">
            <div class="fLabel">{*{$aModule.form.invoice2_address.label}*}Dane adresowe:</div>
            <div class="fInput">{$aModule.form.invoice2_address.input}</div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div id="invoice2_address_section"></div>
        </div>
      </div>
    {/if}
    
    {if !empty($aModule.form.transport_address)}
      <div class="step2Option step2Option_3" style="width: 300px; padding: 0px !important;">
        <div class="koszykPodtytul2"><strong>{$aModule.lang.transport_address}:</strong></div>
        <div class="fRow">
          <div class="fLabel">{*{$aModule.form.transport_address.label}*}Dane adresowe:</div>
          <div class="fInput">{$aModule.form.transport_address.input}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div id="transport_address_section">
        {if !empty($aModule.transport_address)}
          <div class="mojeAdresy">
            <div class="mojeAdresySrodek">
              {if $aModule.transport_address.is_company == '1'}
                <div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.transport_address.company}</div>
                <div class="clear"></div>
                {*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.transport_address.nip}</div>
                <div class="clear"></div>*}
                <div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.transport_address.street} {$aModule.transport_address.number}{if !empty($aModule.transport_address.number2)}/{$aModule.transport_address.number2}{/if}</div>
                <div class="clear"></div>
                <div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.transport_address.postal} {$aModule.transport_address.city}</div>
              {else}
                <div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.transport_address.name} {$aModule.transport_address.surname}</div>
                <div class="clear"></div>
                {*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
                <div class="clear"></div>*}
                <div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.transport_address.street} {$aModule.transport_address.number}{if !empty($aModule.transport_address.number2)}/{$aModule.transport_address.number2}{/if}</div>
                <div class="clear"></div>
                <div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.transport_address.postal} {$aModule.transport_address.city}</div>
              {/if}
            </div>
          </div>
        {/if}
        </div>
        <div id="transport_address_section_loading" style="display: none;" class="loading">{$aLang.common.loading}</div>
      </div>
    {/if}
    
    {if !empty($aModule.form.transport_address) && !empty($aModule.form.invoice2_address) && !empty($aModule.form.invoice_address)}
      <div class="clear" style="margin-bottom: 40px;"></div>
    {/if}

    {if !empty($aModule.form.recipient)}
      <div id="person_recipient" class="step2Option step2Option_3 shortLabel" style="display: none; width: 350px; padding: 0px !important;">
        <div class="koszykPodtytul2"><strong>Osoba odbierająca przesyłkę:</strong></div>
        
        <div class="fRow">
          <div class="fLabel">{$aModule.form.recipient.name.label}</div>
          <div class="fInput">{$aModule.form.recipient.name.input}</div>
          <div class="clear"></div>
        </div>

        <div class="fRow">
          <div class="fLabel">{$aModule.form.recipient.surname.label}</div>
          <div class="fInput">{$aModule.form.recipient.surname.input}</div>
          <div class="clear"></div>
        </div>

        <div class="fRow">
          <div class="fLabel">{$aModule.form.recipient.phone.label}</div>
          <div class="fInput">{$aModule.form.recipient.phone.input}</div>
          <div class="clear"></div>
        </div>
      </div>
    {/if}
    
            
    {if !empty($aModule.form.recipient_phone)}
      <div id="person_recipient_phone" class="step2Option step2Option_3 shortLabel" style="display: none; width: 350px; padding: 0px !important;">
        <div class="koszykPodtytul2"><strong>Osoba odbierająca przesyłkę:</strong></div>
        
        <div class="fRow">
          <div class="fLabel">{$aModule.form.recipient_phone.phone.label}</div>
          <div class="fInput">{$aModule.form.recipient_phone.phone.input}</div>
          <div class="clear"></div>
        </div>
      </div>
    {/if}
    
		<div class="clear" style="height: 30px;"></div>

		<div class="editAddressesButton">
			<a href="javascript:void(0);" title="{$aModule.lang.edit_addresses}" id="editAddressesButton" class="editAdressButtonCS"></a>
			<span>{$aModule.lang.edit_addresses_content}</span>
			<div class="clear"></div>
		</div>
      
    <div class="clear"></div>
    <div class="corp_grp_input">
      <div class="fRow">
        <div class="fLabel">&nbsp;</div>
        <div class="fInput">{$aModule.form.ceneo.ceneo_agreement.input} {$aModule.form.ceneo.ceneo_agreement.label}</div>
        <div class="clear"></div>
      </div>	
    </div>
      
    <div class="buttonLine buttonLineBig" style="margin-top: 20px;">
      <a href="/koszyk/step2.html" class="left" style="float: left;background: url('/images/gfx/button-wroc.gif') no-repeat;width: 85px;height: 33px;" title="Wróć"></a>

      <div class="buttonDomyslny">
        <div class="buttonDomyslnyLewo"></div>
        <div class="buttonDomyslnySrodek"><a href="javascript:void(0);" onclick="$('#order_logged_in').submit();">Zatwierdź i przejdź dalej <span style="font-size: 16px">&raquo;</span></a></div>
        <div class="buttonDomyslnyPrawo"></div>
      </div>
    </div>
    {$aModule.form.validator.footer}
      
	{else}
		
		<div class="clear"></div>

		<div class="editAddressesButtonInfo" style="border: 2px solid #C30500; padding: 30px 60px 30px 80px;margin: 0 0 18px">
			<a title="{$aModule.lang.edit_addresses}" id="addAddressesButton" class="editAdressButtonCS"></a>
			<span style="color: #C30500; font-size: 13px; font-weight: bold;">Uzupełnij dane adresowe. </span> 
			<span style="font-size: 13px; font-weight: bold;">Kliknij na przycisk obok, aby przejść do dodawania adresu:</span> 
		</div>
	{/if}
</div>



<form action="{$aModule.links.edit_addresses}" id="editAddressesForm" method="post" name="editAddresses">
	<input type="hidden" name="ref" value="{$aModule.links.reflink}" />
	<input type="hidden" name="cart" value="1" />
</form>

  <form action="{$aModule.links.add_addresses}" id="addAddressesForm" method="post" name="addAddresses">
	<input type="hidden" name="ref" value="{$aModule.links.reflink}" />
	<input type="hidden" name="cart" value="1" />
</form>


{literal}
<script type="text/javascript">
  //<![CDATA[
    $(document).ready(function(){
      function copyDataRecipientData() {
        if ($("#person_recipient").is(':visible') === true) {
          $("input[id='recipient[name]']").val($('.addr_name').html());
          $("input[id='recipient[surname]']").val($('.addr_surname').html());
        }
      }
      function checkShowHideRecipientPhone(bShow) {
        if (bShow === true) {
          $("#person_recipient_phone").hide();
        } else {
          if ($("#recipient_email_hidden").val() === '') {
            $("#person_recipient_phone").show();
          }
        }
      }
      
      /** WALIDACJA **/
      $("#order_logged_in input").blur(function() {
        performFieldAjaxValidation("order_logged_in",this.name,$("#order_logged_in :input").serialize());
      });
      $("#order_logged_in input").click(function() {
        performFieldAjaxValidation("order_logged_in",this.name,$("#order_logged_in :input").serialize());
      });
      /** BRZYDKA WALIDACJA JS **/
			$("#order_logged_in").submit(function() {
        if ($("#invoice_address").length > 0) {
          if ($("#invoice_address").val() === ''){
            showSimplePopup('Wybierz dane do faktury VAT', false, 4000);
            return false;
          }
        }
        if ($("#transport_address").length > 0) {
          if ($("#transport_address").val() === ''){
            showSimplePopup('Wybierz dane dostawy', false, 4000);
            return false;
          }
        }
        if ($("#invoice2_address").length > 0) {
          if ($("#invoice2_address").val() === ''){
            showSimplePopup('Wybierz dane do drugiej faktury VAT', false, 4000);
            return false;
          }
        }
        
        if ($(".select_nip_adress[value='0']").length > 0) {
          showSimplePopup('Wybrane dane do faktury VAT nie zawierają NIP.<br /> Uzupełnij dane, lub wybierz inne dane do faktury VAT.', false, 6000);
          return false;
        }
          
        /** \BRZYDKA WALIDACJA JS **/
				performAjaxValidation("order_logged_in", $("#order_logged_in input").serialize());
			  if($("#order_logged_in input:enabled").hasClass("validErr")){
			  	showSimplePopup('Uzupełnij poprawnie formularz!', false, 4000);
			 		return false;
			  } else 
			  	return true;
			});
      
      /** ZMIANA ADRESÓW **/
      $("#invoice_address").change(function() {
        $("#invoice_address_section_loading").show();
        $("#invoice_address_section").load("/internals/GetAddress.php?id="+$("#invoice_address").val(),function() {
          $("#invoice_address_section_loading").hide();
          if ($(".select_nip_adress[value='0']").length > 0) {
            showSimplePopup('Wybrane dane do faktury VAT nie zawierają NIP.<br /> Uzupełnij dane, lub wybierz inne dane do faktury VAT.', false, 6000);
          }
           copyDataRecipientData();
        });
      });
      $("#transport_address").change(function() {
        $("#transport_address_section_loading").show();
        $("#transport_address_section").load("/internals/GetAddress.php?id="+$("#transport_address").val()+"&t=1",function() {
          $("#transport_address_section_loading").hide();
          if ($("#is_delivery_invoice").val() === '1') {
            $("#person_recipient").show();
            checkShowHideRecipientPhone(true);
          } else {
            $("#person_recipient").hide();
            checkShowHideRecipientPhone(false);
          }
          copyDataRecipientData();
        });
      });
      $("#invoice2_address").change(function() {
        $("#invoice2_address_section_loading").show();
        $("#invoice2_address_section").load("/internals/GetAddress.php?id="+$("#invoice2_address").val()+"&i2=1",function() {
          $("#invoice2_address_section_loading").hide();
          if ($(".select_nip_adress[value='0']").length > 0) {
            showSimplePopup('Wybrane dane do faktury VAT nie zawierają NIP.<br /> Uzupełnij dane, lub wybierz inne dane do faktury VAT.', false, 6000);
          }
          copyDataRecipientData();
        });
      });
      
      if ($("#invoice_address").length > 0) {
        $("#invoice_address").trigger('change');
      }
      if ($("#invoice2_address").length > 0) {
        $("#invoice2_address").trigger('change');
      }
      if ($("#transport_address").length > 0) {
        $("#transport_address").trigger('change');
      }
      $("#editAddressesButton").click(function(){
        $("#editAddressesForm").submit();
        return false;
      });
      $("#addAddressesButton").click(function(){
        $("#addAddressesForm").submit();
        return false;
      });
    });
  //]]>
</script>
{/literal}