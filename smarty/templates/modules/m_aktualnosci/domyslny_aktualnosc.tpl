<div style="padding-bottom: 10px; float: left; display: inline; width: 653px; margin: 0 0 0 10px">
	<div class="ikona">
		<div class="newsTytul">
	   		<a href="{$aItem.link}">{$aModule.name}</a>
	    </div>
	</div>
	<div class="newsData right" style="color: #84ad94; float: right; display:inline; margin-right: 17px ">{$aModule.ns_date}</div>
</div>
	{if !empty($aModule.beginning)}
		<div class="beginning">
			{if isset($aModule_versions[$aModule.id].beginning) && !empty($aModule_versions[$aModule.id].beginning)}
	            {$aModule_versions[$aModule.id].beginning}
	        {else}
	        	{$aModule.beginning}
	        {/if}
		</div>
	{/if}
	{if !empty($aModule.images) && $aModule.img_align ne 'bottom'}
		{$aModule.images}
	{/if}

	<div class="content" style="padding: 0 21px 15px 21px;">
		{$aModule.content}
	</div>
	<div class="clear"></div>
	<div class="aktualnoscAuthor" style="padding: 0 0">{$aModule.author}</div>
	<div class="clear"></div>
	{*<div class="aktualnoscData" style="padding: 0 0 15px 0;">({if isset($aModule.modified)}{$aModule.lang.aktualizacja} {$aModule.modified}{/if})</div>*}
	<div class="clear"></div>
	{if !empty($aModule.files)}
		<div class="Pliki" style="padding: 0 0 15px 0;">
			<div class="zalacznikiTXT">{$aModule.lang.zalaczniki}</div>
			<div class="clear"></div>
			<div class="zalacznikiPliki">{$aModule.files}</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	{/if}

	{if !empty($aModule.images) && $aModule.img_align eq 'bottom'}
		{$aModule.images}
	{/if}
	{*<div class="clear"></div>
	<div class="zobaczWszystkie">{$sGoBack}</div>*}