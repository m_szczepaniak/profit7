{foreach from=$aModule.items key=i item=aItem name=news_list}
	<div class="newsBox">
		<div class="ikona">
			<div class="newsData">{$aItem.ns_date}</div>
		</div>
		


        <div class="newsTrescBox">
            {if !empty($aItem.image)}
				<div class="newsIco">
					<a href="{$aItem.link}" class="newsWiecej" rel="nofollow"><img src="{$aItem.image.src}" width="{$aItem.image.width}" height="{$aItem.image.height}" alt="" /></a>
            	</div>
           	{/if}

		<div {if $aItem.marked_out==1}class="newsTytulWyrozniony"{else}class="newsTytul"{/if}>
            <a href="{$aItem.link}">{$aItem.name}</a>
        </div>



                {if isset($aModule.news_versions[$aItem.id].beginning) && !empty($aModule.news_versions[$aItem.id].beginning)}
                	{$aBox.news_versions[$aItem.id].beginning}
                {else}
                	{$aItem.beginning}
                {/if}
        </div>
		<div class="clear"></div>
        <div class="wiecejAktualnosci">
			<a href="{$aItem.link}" class="newsWiecej" rel="nofollow"></a>
		</div>
    </div>
    
    
    <div class="clear"></div>
{/foreach}
{if isset($aModule.pager.links) && !empty($aModule.pager.links)}
	<div class="pagerNews">
		{$aLang.pager.page_dots} <strong>{$aModule.pager.links}</strong>
		<span>[{$aLang.pager.current_page} {$aModule.pager.current_page} {$aLang.pager.from} {$aModule.pager.total_pages} - {$aModule.lang.pager_total_items} <strong>{$aModule.pager.total_items}</strong>]</span>
	</div>
	
	
{/if}