<div class="photosTop">
	{foreach from=$aImages item=im key=i}
		<div class="photoCnt">
			<div class="textPhoto">
				{if isset($im.big.src)}
					<a href="{$im.big.src}" rel="lytebox['photos']" title="{$im.decoded_description}" target="_blank"><img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" /></a>
				{else}
					<img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" />
				{/if}
				<div class="clear"></div>
			</div>
			{if isset($im.short_description) && !empty($im.short_description)}
				<div class="photoDescription">
					{$im.short_description}
				</div>
			{elseif isset($im.description) && !empty($im.description)}
				<div class="photoDescription">
					{$im.description}
				</div>
			{/if}
			{if isset($im.author)}
				<div class="photoDescription">
					<div class="imgAuthor">
						<span>{$aModule.lang.fot}</span> {$im.author}
					</div>
				</div>
			{/if}
		</div>		
		{if $i % 4 eq 3}
			<div class="clear"></div>
		{/if}
	{/foreach}
</div>
<div class="clear"></div>