<div class="photosLeft">
	{foreach from=$aImages item=im key=i}
		<div class="photoCnt">
			<div class="textPhoto">
				{if isset($im.big.src)}
					<a href="{$im.big.src}" rel="lytebox['photos']" title="{$im.decoded_description}" target="_blank"><img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" /></a>
				{else}
					<img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" />
				{/if}
			</div>
			<div class="clear"></div>
			{if isset($im.short_description) && !empty($im.short_description)}
				<div class="photoDescription" style="width: {$im.width}px;">
					{$im.short_description}
				</div>
			{elseif isset($im.description) && !empty($im.description)}
				<div class="photoDescription" style="width: {$im.width}px;">
					{$im.description}
				</div>
			{/if}
			{if isset($im.author)}
				<div class="photoDescription" style="width: {$im.width}px;">
					<div class="imgAuthor">
						<span>{$aModule.lang.fot}</span> {$im.author}
					</div>
				</div>
			{/if}
		</div>
	{/foreach}
</div>