{literal}
<style type="text/css">
	.lpPaczkomaty {}
	.lpPaczkomatyCenter {
		border-left: 2px solid #000000;
		border-right: 2px solid #000000;
		border-top: 2px solid #000000;
	}
	.lpPaczkomatyCode {
		width: 732px;
		height: 415px;
		background: url(/images/editor/lp_paczkomaty/code3.png) no-repeat 0 0;
		position: relative;
	}
	.lpPaczkomatyCode div {
		position: absolute;
		top: 259px;
		left: 186px;
		width: 378px;
		text-align: center;
		color: #d41422;
		font-family: Arial, Helvetica, sans-serif;
		font-weight: bold;
		font-size: 40px;
		line-height: 100%;
	}
	.lpPaczkomaty .content {
		padding: 20px 45px;
		font-size: 11px;
		line-height: 140%;
	}
	.lpPaczkomaty .content li {
		padding: 0 0 6px 0;	
	}
	
</style>
{/literal}

{if $smarty.get.source != 'paczkomaty'}
	<div class="paragraph lpPaczkomaty">
		{*<a href="/paczkomaty-mini-przewodnik/" target="_blank" title="JUŻ NIE CZEKAM. ODBIERAM WYGODNIE PRZEZ PACZKOMATY"><img src="/images/editor/lp_paczkomaty/top.jpg" /></a>*}
		<div class="lpPaczkomatyCenter">
			<div class="lpPaczkomatyCode"><div>{$aModule.items.0.name}</div></div>
			<div class="content">{$aModule.items.0.content}</div>						
		</div>
		<img src="/images/editor/lp_paczkomaty/bottom.jpg" />
	</div>
{else}
	<div class="paragraph lpPaczkomaty">
		<a href="/paczkomaty-mini-przewodnik/" target="_blank" title="JUŻ NIE CZEKAM. ODBIERAM WYGODNIE PRZEZ PACZKOMATY"><img src="/images/editor/lp_paczkomaty/top.jpg" /></a>
		<div class="lpPaczkomatyCenter">
			<div class="lpPaczkomatyCode"><div>{$aModule.items.1.name}</div></div>
			<div class="content">{$aModule.items.1.content}</div>						
		</div>
		<img src="/images/editor/lp_paczkomaty/bottom.jpg" />
	</div>
{/if}