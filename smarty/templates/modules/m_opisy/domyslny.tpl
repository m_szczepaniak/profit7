{foreach from=$aModule.items key=i item=par}
	<div class="paragraph">
		{if (!empty($par.name))}
			<h3>{$par.name}</h3>
		{/if}
		{if $par.img_align ne 'bottom' && !empty($par.images)}
			{$par.images}
		{/if}
		<div class="content">{$par.content}</div>
		{if $par.img_align eq 'bottom' && !empty($par.images)}
			{$par.images}
		{/if}
		{if !empty($par.files)}
			<div class="clear"></div>
			<div class="Pliki">
				<div class="zalacznikiTXT">{$aModule.lang.zalaczniki}</div>
				<div class="clear"></div>
				<div class="zalacznikiPliki">{$par.files}</div>
			</div>
		{/if}
		<div class="clear"></div>
	</div>
{/foreach}