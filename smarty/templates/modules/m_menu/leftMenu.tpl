{if $aMenu.display_name == '1'}
<ul>
	<li{if isset($bNewsSelected)} class="selected"{/if}><span>{$aMenu.name}&nbsp;</span>
		<ul>
			{foreach from=$aMenu.items item=aItem}
				<li{if isset($aItem.selected)} class="selected"{/if}><a href="{$aItem.link}">{$aItem.name}</a></li>
			{/foreach}
		</ul>
	</li>
</ul>
{else}
	<ul>
		{foreach from=$aMenu.items item=aItem}
			<li{if isset($aItem.selected)} class="selected"{/if}><a href="{$aItem.link}">{$aItem.name}&nbsp;</a></li>
		{/foreach}
	</ul>
{/if}