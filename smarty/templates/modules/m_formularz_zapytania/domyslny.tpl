<div id="formText">
	{if !empty($aModule.text)}
		{$aModule.text}
	{/if}
</div>
<div class="content">

<script type="text/javascript">
				// <![CDATA[
					var aVisPoll = new Array();
				// ]]>
</script>

	{$aModule.form.JS}
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{foreach from=$aModule.form.fields key=i item=group}
		{if isset($aModule.form.groups[$i])}
			<div class="grupaPul">{$aModule.form.groups[$i]}</div>
		{/if}
		{foreach from=$group key=k item=field}
			{if $field.type eq 'agreement'}
			<div class="fRow">
				<div class="fLabel1" >{$field.label}</div>
				<div class="fInput1" >{$field.input}</div>
			</div>
			{else}
			<div class="fRow">
				<div class="fLabel">{$field.label}</div>
				<div class="fInput">{$field.input}</div>
				<div class="fLabel">{$field.description}</div>
				<div class="clear"></div>
			</div>
			{if isset($field.dependent)}
					{foreach from=$field.dependent key=iAId item=aFields}
						<div id="dep_{$aFields.depends_on_value}" style="display: {$aFields.display};">
						{foreach from=$aFields.items key=j item=aDField}
							<div class="fRowPyt">
								<div class="fPytanie">{$aDField.label}</div>
								<div class="fOdp">{$aDField.input}{if isset($aDField.other_input)}{$aDField.other_label} {$aDField.other_input}{/if}</div>
							</div>
						{/foreach}
						</div>
					{/foreach}
				{/if}
			{/if}
		{/foreach}
	{/foreach}	
	<div class="clear"></div>
	<div class="fRow">
				<div class="fLabel">&nbsp;</div>
		<input type="image" name="send" value="" src="images/gfx/{$sLang}/wyslij_zapytanie.gif" style="margin-left: 286px; float: left" />
	</div>
	<div class="clear"></div>
	<div class="fRow" style="border: 0;">
		<div class="fRequiredF">{$aLang.form.required_fields}</div>
	</div>
	{$aModule.form.footer}
</div>

{if !empty($aModule.fdescription)}
	<div>
		{$aModule.fdescription}
	</div>
{/if}