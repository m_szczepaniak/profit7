{if !empty($aModule.text)}
	<div class="content" style="padding-bottom: 20px;">
		{$aModule.text}
	</div>
{/if}
<div class="content">
	{$aModule.form.JS}
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{foreach from=$aModule.form.fields key=i item=group}
		{if isset($aModule.form.groups[$i])}
			<div class="grupaPol">{$aModule.form.groups[$i]}</div>
		{/if}
		{foreach from=$group key=k item=field}
			{if $field.type eq 'agreement'}
			<div class="fRow">
				<div class="fInput" >asdasda{$field.input}</div>
				<div class="fLabel" >{$field.label}</div>
				<div class="clear"></div>
			</div>
			{else}
			<div class="fRow">
				<div class="fLabel">{$field.label}</div>
				
				<div class="fInput">
					{if $field.type eq 'textarea'}<div class="textareaTop"></div><div class="textareaObszar">{/if}
					{if $field.type eq 'text' or $field.type eq 'email'}<div class="inputText">{/if}
						{$field.input}
					{if $field.type eq 'text' or $field.type eq 'email'}</div>{/if}
					{if $field.type eq 'textarea'}</div><div class="textareaBottom"></div>{/if}
				</div>
				<div class="clear"></div>
			</div>
			{/if}
		{/foreach}
	{/foreach}	
	<div class="fRow">
		<div class="fLabel">&nbsp;</div>
		<div class="fInput">
			<input type="image" name="send" value="" src="/images/gfx/{$sLang}/wyslij.png" style="float: left;" />
		</div>
	</div>
	<div class="fRow">
		<div class="fLabel">
			<div class="fRequiredF"><span class="gwiazdka"></span>{$aLang.form.required_fields}</div>
		</div>
	</div>

	{$aModule.form.footer}
</div>