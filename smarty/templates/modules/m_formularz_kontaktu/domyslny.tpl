{if !empty($aModule.text)}
	<div class="formularzText">
		{$aModule.text}
	</div>
	<div class="clear"></div>
{/if}
{if !empty($aModule.fdescription)}
	<div class="formularzText">
		{$aModule.fdescription}
	</div>
	<div class="clear"></div>
{/if}

<div class="formularzTresc">
	{$aModule.form.JS}
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{foreach from=$aModule.form.fields key=i item=group}
		{*{if isset($aModule.form.groups[$i])}
			<div class="grupaPul">{$aModule.form.groups[$i]}</div>
		{/if}*}
		{foreach from=$group key=k item=field}
			{if $field.type eq 'agreement'}
			<div class="fRow">
				<div class="fLabel1" >{$field.label}</div>
				<div class="fInput1" >{$field.input}</div>
			</div>
			{else}
			<div class="fRow">
				<div class="fLabel">{$field.label}</div>
				<div class="fInput">{$field.input}</div>
				{if !empty($field.description)}<div class="fLabel2"><a href="" onmouseover="showHintBox(this, '{$field.description}');">[?]</a></div>{/if}
				<div class="clear"></div>
			</div>
			{/if}
		{/foreach}
	{/foreach}
	<div class="clear"></div>
	<div class="fRow" style="border: 0;">
		<div class="fRequiredF">{$aLang.form.required_fields}</div>
	</div>
	<div class="clear"></div>
	<div class="fRow">
		<div class="fLabel">&nbsp;</div>
		<input class="sendBig" type="submit" name="send" value="Wyślij zapytanie" src="/images/gfx/sendBig.gif" style="float: right;" />
	</div>

	{$aModule.form.footer}
</div>