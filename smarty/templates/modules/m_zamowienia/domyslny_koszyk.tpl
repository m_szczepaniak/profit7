<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 1/4 (zawartość koszyka)</h1>
</div>
<div class="clear"></div>

<form name="cart_1" action="/koszyk/recount.html" method="post" id="koszykForm"/>
	<table cellspacing="1" cellpadding="1" class="koszyk">
		<tr>
			<th style="text-align: left;"></th>
			<th class="pozycja" style="text-align: left;">Lp.</th>
			<th class="nazwa" style="text-align: left;" colspan="2">{$aModule.lang.list_name}</th>
			<th class="cena" style="width: 90px;">{$aModule.lang.email_shipment_time}</th>
			<th class="cena" style="width: 90px;">{$aModule.lang.list_price_brutto}</th>
			<th class="ilosc" style="width: 64px;">{$aModule.lang.list_quantity}</th>
			<th class="wartosc" style="width: 102px;">{$aModule.lang.total_products_brutto}</th>
			<th class="schowek" style="width: 80px;">{$aModule.lang.list_repository}</th>
			<th class="usun" style="width: 30px;">{$aModule.lang.list_delete}</th>
		</tr>
		{assign var=pozycja value=0}
		{foreach from=$aModule.cart.products key=iId item=aItems name=types}
			{assign var=pozycja value=$pozycja+1}
			<tr class="hover">
				<td><input class="group_selected" type="checkbox" name="selected_products[]" value="{$iId}" /></td>
				<td class="pozycja">{$pozycja}.</td>
				<td class="zdjecie"><a href="{$aItems.link}">{if !empty($aItems.image.src)}<img src="{$aItems.image.src}" alt="{$aItems.name}" />{else}<img src="/images/gfx/brak.png" alt="Brak okładki" />{/if}</a></td>
				<td class="nazwa" valign="middle"><a href="{$aItems.link}">{if isset($aItems.link)}{$aItems.name}</a>{else}{$aItems.name}{/if}
					{if !empty($aModule.cart.discount_code.books) && in_array($iId, $aModule.cart.discount_code.books) }<span style="color: #c30500;"> - Rabat {$aModule.cart.discount_code.code_discount}%</span>{/if}
				</td>
				<td class="cena Center"><strong>{if $aItems.shipment_date!=''}od:<br/>{$aItems.shipment_date_f}r. {else}{$aItems.shipment_time}{/if}</strong></td>
				<td class="cena" style="text-align: center;">{if $aItems.promo_price > 0}<span>{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</span><br/>{$aItems.promo_price|format_number:2:',':' '} {$aLang.common.currency}{else}{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}{/if}</td>
				<td class="ilosc" style="text-align: center;">		
					<input class="input_lista_koszyk" type="text" value="{$aItems.quantity}" maxlength="4" type="text" id="itm_{$iId}" name="itm[{$iId}]"/>
					<div class="updownbuttons">
						<a class="up_item" onclick="changeItmValueUp('{$iId}');" href="javascript:void(0);"></a>
						<a class="down_item" onclick="changeItmValueDown('{$iId}');" href="javascript:void(0);"></a>
					</div>
				</td>
				<td class="wartosc" style="text-align: center;">{if $aItems.total_without_promo ne $aItems.total_price_brutto}<span>{$aItems.total_without_promo|format_number:2:',':' '} {$aLang.common.currency}</span><br/> <strong>{$aItems.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{else}<strong>{$aItems.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{/if}</td>
				<td class="schowek" style="text-align: center;"><a href="{$aItems.repository_link}" class="delete"><img src="/images/gfx/ikona_schowek.gif" alt="" /></a></td>
				<td class="usun" style="text-align: center;"><a href="{$aItems.delete_link}" class="delete"><img src="/images/gfx/ikona_usun.gif" alt="" /></a></td>
			</tr>
		{/foreach}
			<tr>
				<td class="suma" style="border-top: 0;border-bottom: 0;" colspan="10">
					<label style="margin-left:9px;padding: 6px 15px 0 0;" class="left"><input style="margin-right:5px" class="left" type="checkbox" id="group_check_all" />zaznacz wszystkie</label>
					<button type="submit" name="recount" value="Przelicz koszyk" style="float:right;border:0;background:none;cursor:pointer;"><img src="/images/gfx/button-przelicz-koszyk.gif" class="przeliczKoszyk right" /></button>
					<button type="submit" name="group_action" value="group_delete" style="margin-right:5px; border:0;float:right;background:none;cursor:pointer;"><img style="opacity:0.5" src="/images/gfx/group_delete.gif" class="card_group_action right" alt="group_delete"></button>
					<div class="cena right">{$aModule.cart.total_price_brutto|format_number:2:',':' '}{$aLang.common.currency}</div>
					<div class="right" style="padding: 6px 15px 0 0;">{$aModule.lang.total_cost_brutto}</div>
					{*{if $aModule.cart.total_without_promo ne $aModule.cart.total_price_brutto}
						<span>{$aModule.cart.total_without_promo|format_number:2:',':' '}{$aLang.common.currency}</span>			
						<strong>{$aModule.cart.total_price_brutto|format_number:2:',':' '}{$aLang.common.currency}</strong>
					{else}
						<strong style="padding: 8px 15px 0 0;">{$aModule.cart.total_price_brutto|format_number:2:',':' '}{$aLang.common.currency}</strong>
					{/if}
					*}
				</td>
			</tr>		
	</table>
	
	<div class="clear" style="margin-top: 10px;"></div>
	
{if !empty($aModule.transport_preview_info)}
	<div class="koszykWysInfo" style="color: #970501; font-size: 12px; font-weight: bold; padding: 15px; float: right;">
					<span style="text-align: center;display: block;">
					  <b style="font-size: 24px;">Uwaga ważne:</b> <br /><br />
					  <div style="font-size: 14px; text-align: right!important; ">{$aModule.transport_preview_info}</div>
					</span>
		<div class="clear"></div>
	</div>
{/if}

	{*<div id="koszyk_kod_rabatowy">
		<div class="hintBox" onmouseover="showhint('Pole na kod rabatowy.', this, event, '160px')"></div>	
		<input class="input_rabat_koszyk text right" type="text" value="{$aModule.cart.discount_code}" maxlength="32" type="text" name="discount_code" />
		<div class="name right">{$aModule.lang.discount_code}</div>
	</div>*}
	<div class="buttonLine buttonLineBig">
		<a href="/" title="Wstecz" style="float: left;width: 160px;height: 33px;background: url('/images/gfx/button-wroc-do-zakupow.gif');"></a>
{*		<input class="right" type="image" src="/images/gfx/button-zatwierdz-i-przejdz.gif" name="step2" value="Zatwierdź i przejdź do wyboru danych i płatności" onclick="this.form.action='/koszyk/step2.html#cart';"/>*}
    <div class="buttonDomyslny">
      <div class="buttonDomyslnyLewo"></div>
      <div class="buttonDomyslnySrodek"><a onclick="$('#koszykForm').attr('action', '/koszyk/step2.html#cart'); $('#koszykForm').submit();">Zatwierdź i przejdź dalej <span style="font-size: 16px">&raquo;</span></a></div>
      <div class="buttonDomyslnyPrawo"></div>
    </div>
    <div class="clear"></div>
	</div>
</form>

{$aModule.form}

{literal}
	<script type="text/javascript">

		$('#group_check_all').on('click', function(){

			var val = $(this).is(':checked');
			console.log(val);
			if (false == val) {
				$('.group_selected').prop('checked', false);
			} else {
				$('.group_selected').prop('checked', true);
			}
		});

		$('.group_selected, #group_check_all').click (function(){

			var checked = $('.group_selected:checked').length;

			if (checked > 0) {
				$('.card_group_action').removeAttr('disabled');
				$('.card_group_action').css({ opacity: 1 });
			} else {
				$('.card_group_action').attr('disabled', 'disabled');
				$('.card_group_action').css({ opacity: 0.5 });
			}
		});
	</script>
{/literal}