<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 4/4 (Podsumowanie danych kupującego)</h1>
</div>
<div class="clear"></div>
<div id="step2" class="koszykStep4">
  {$aModule.login_box}
  {$aModule.form.validator.header}
	{$aModule.form.validator.err_prefix}
	{$aModule.form.validator.err_postfix}
	{$aModule.form.validator.validator}
	{$aModule.form.validator.ajaxval}
  
  {if !empty($aModule.form.invoice)}
  <div class="step2Option step2Option_1" style="text-align: left; border-left: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Dane, na które wystawimy <br />fakturę{if $aModule.form.delivery.delivery_data_from_invoice == '1'} oraz wyślemy przesyłkę{/if}:</strong></div>
      {if $aModule.form.invoice.is_company == '1'}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Firma:</div>
          <div class="left">{$aModule.form.invoice.company}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">NIP:</div>
          <div class="left">{$aModule.form.invoice.nip}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        {if $aModule.form.delivery.delivery_data_from_invoice == '1'}
          <div class="clear"></div>
          <div class="fRow">
            <div class="fLabel" style="padding-top:0;">Osoba odbierająca:</div>
            <div class="left">{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div class="fRow">
            <div class="fLabel" style="padding-top:0;">Telefon:</div>
            <div class="left">{$aModule.form.recipient.phone}</div>
            <div class="clear"></div>
          </div>
        {/if}
      {else}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Imię i nazwisko:</div>
          <div class="left">{$aModule.form.invoice.name} {$aModule.form.invoice.surname}</div>
          <div class="clear"></div>
        </div>
      {/if}
      <div class="clear"></div>
      <div class="fRow">
				<div class="fLabel" style="padding-top:0;">Adres:</div>
				<div class="left">{$aModule.form.invoice.postal} {$aModule.form.invoice.city} <br />{$aModule.form.invoice.street} {$aModule.form.invoice.number}{if !empty($aModule.form.invoice.number2)}/{$aModule.form.invoice.number2}{/if}</div>
				<div class="clear"></div>
			</div>
      <div class="clear"></div>
  </div>
  {/if}
  
  {if !empty($aModule.form.second_invoice)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Dane, na które wystawimy <br />drugą faktura:</strong></div>
      {if $aModule.form.second_invoice.is_company == '1'}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Firma:</div>
          <div class="left">{$aModule.form.second_invoice.company}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">NIP:</div>
          <div class="left">{$aModule.form.second_invoice.nip}</div>
          <div class="clear"></div>
        </div>
      {else}
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Imię i nazwisko:</div>
          <div class="left">{$aModule.form.second_invoice.name} {$aModule.form.second_invoice.surname}</div>
          <div class="clear"></div>
        </div>
      {/if}
      <div class="clear"></div>
      <div class="fRow">
				<div class="fLabel" style="padding-top:0;">Adres:</div>
				<div class="left">{$aModule.form.second_invoice.postal} {$aModule.form.second_invoice.city} <br />{$aModule.form.second_invoice.street} {$aModule.form.second_invoice.number}{if !empty($aModule.form.second_invoice.number2)}/{$aModule.form.second_invoice.number2}{/if}</div>
				<div class="clear"></div>
			</div>
      <div class="clear"></div>
  </div>
  {/if}

  {if !empty($aModule.form.delivery.city) && $aModule.form.delivery.delivery_data_from_invoice != '1'}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres, na który wyślemy <br />Twoją przesyłkę:</strong></div>
      {if $aModule.form.delivery.is_company == '1'}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Firma:</div>
          <div class="left">{$aModule.form.delivery.company}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Adres:</div>
          <div class="left">{$aModule.form.delivery.postal} {$aModule.form.delivery.city} <br />{$aModule.form.delivery.street} {$aModule.form.delivery.number}{if !empty($aModule.form.delivery.number2)}/{$aModule.form.delivery.number2}{/if}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Osoba odbierająca:</div>
          <div class="left">{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Telefon:</div>
          <div class="left">{$aModule.form.recipient.phone}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        {*<div class="fRow">
          <div class="fLabel" style="padding-top:0;">NIP:</div>
          <div class="left">{$aModule.form.delivery.nip}</div>
          <div class="clear"></div>
        </div>*}
        <div class="clear"></div>
      {else}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Imię i nazwisko:</div>
          <div class="left">{$aModule.form.delivery.name} {$aModule.form.delivery.surname}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Adres:</div>
          <div class="left">{$aModule.form.delivery.postal} {$aModule.form.delivery.city} <br />{$aModule.form.delivery.street} {$aModule.form.delivery.number}{if !empty($aModule.form.delivery.number2)}/{$aModule.form.delivery.number2}{/if}</div>
          <div class="clear"></div>
        </div>
        {if !empty($aModule.form.recipient_phone.phone)}
        <div class="fRow">
          <div class="fLabel" style="padding-top:0;">Telefon:</div>
          <div class="left">{$aModule.form.recipient_phone.phone}</div>
          <div class="clear"></div>
        </div>
        {/if}
      {/if}
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if}
  
{if !empty($aModule.form.paczkomaty.point_of_receipt)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres paczkomatu, na który wyślemy <br />Twoją przesyłkę:</strong></div>
      <div class="fRow">
				<div class="left" id="point_of_receipt_description">
          <input type="hidden" id="point_of_receipt" name="point_of_receipt" value="{$aModule.form.paczkomaty.point_of_receipt}">
          <input type="hidden" id="point_of_receipt_sel" name="point_of_receipt_sel" value="{$aModule.form.paczkomaty.point_of_receipt_sel}">
        </div>
				<div class="clear" style="margin-bottom: 10px;"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0; width: auto;">Numer telefonu komórkowego:</div>
          <div class="left"><strong>{$aModule.form.paczkomaty.phone_paczkomaty}</strong></div>
          <div class="clear"></div>
        </div>
			</div>
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if}
{if !empty($aModule.form.poczta_polska.point_of_receipt_poczta_polska)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres placówki pocztowej, w której <br /> odbierzesz przesyłkę:</strong></div>
      <div class="fRow">
				<div class="left" id="receipient">
          <input type="hidden" id="point_of_receipt_poczta_polska" name="point_of_receipt_poczta_polska" value="{$aModule.form.poczta_polska.point_of_receipt_poczta_polska}">
          <input type="hidden" id="point_of_receipt_sel_poczta_polska" name="point_of_receipt_sel_poczta_polska" value="{$aModule.form.poczta_polska.point_of_receipt_sel_poczta_polska}">
        </div>
        <div class="left" id="point_of_receipt_description_poczta_polska">
          <input type="hidden" id="point_of_receipt_poczta_polska" name="point_of_receipt_poczta_polska" value="{$aModule.form.poczta_polska.point_of_receipt_poczta_polska}">
          <input type="hidden" id="point_of_receipt_sel_poczta_polska" name="point_of_receipt_sel_poczta_polska" value="{$aModule.form.poczta_polska.point_of_receipt_sel_poczta_polska}">
        </div>
				<div class="clear" style="margin-bottom: 10px;"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0; width: auto;">Numer telefonu komórkowego:</div>
          <div class="left"><strong>{$aModule.form.recipient.phone}</strong></div>
          <div class="clear"></div>
          <div class="fLabel" style="padding-top:0; width: auto;">Osoba odbierająca:</div>
          <div class="left"><strong>{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</strong></div>
          <div class="clear"></div>
        </div>
			</div>
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if}
{if !empty($aModule.form.ruch.point_of_receipt_ruch)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres punktu sieci RUCH, w którym <br /> odbierzesz przesyłkę:</strong></div>
      <div class="fRow">
				<div class="left" id="receipient">
          <input type="hidden" id="point_of_receipt_ruch" name="point_of_receipt_ruch" value="{$aModule.form.ruch.point_of_receipt_ruch}">
          <input type="hidden" id="point_of_receipt_sel_ruch" name="point_of_receipt_sel_ruch" value="{$aModule.form.ruch.point_of_receipt_sel_ruch}">
        </div>
        <div class="left" id="point_of_receipt_description_ruch">
          <input type="hidden" id="point_of_receipt_ruch" name="point_of_receipt_ruch" value="{$aModule.form.ruch.point_of_receipt_ruch}">
          <input type="hidden" id="point_of_receipt_sel_ruch" name="point_of_receipt_sel_ruch" value="{$aModule.form.ruch.point_of_receipt_sel_ruch}">
        </div>
				<div class="clear" style="margin-bottom: 10px;"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0; width: auto;">Numer telefonu komórkowego:</div>
          <div class="left"><strong>{$aModule.form.recipient.phone}</strong></div>
          <div class="clear"></div>
          <div class="fLabel" style="padding-top:0; width: auto;">Osoba odbierająca:</div>
          <div class="left"><strong>{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</strong></div>
          <div class="clear"></div>
        </div>
			</div>
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if}  
{if !empty($aModule.form.orlen.point_of_receipt_orlen)}
  <div class="step2Option step2Option_1" style="border-left: 1px solid #eae9e9; border-right: 1px solid #eae9e9; width: 300px; padding:10px!important; ">
    <div class="koszykPodtytul2" style="height: 30px;"><strong>Adres stacji Orlen, na której <br /> odbierzesz przesyłkę:</strong></div>
      <div class="fRow">
				<div class="left" id="receipient">
          <input type="hidden" id="point_of_receipt_orlen" name="point_of_receipt_orlen" value="{$aModule.form.orlen.point_of_receipt_orlen}">
          <input type="hidden" id="point_of_receipt_sel_orlen" name="point_of_receipt_sel_orlen" value="{$aModule.form.orlen.point_of_receipt_sel_orlen}">
        </div>
        <div class="left" id="point_of_receipt_description_orlen">
          <input type="hidden" id="point_of_receipt_orlen" name="point_of_receipt_orlen" value="{$aModule.form.orlen.point_of_receipt_orlen}">
          <input type="hidden" id="point_of_receipt_sel_orlen" name="point_of_receipt_sel_orlen" value="{$aModule.form.orlen.point_of_receipt_sel_orlen}">
        </div>
				<div class="clear" style="margin-bottom: 10px;"></div>
        <div class="fRow">
          <div class="fLabel" style="padding-top:0; width: auto;">Numer telefonu komórkowego:</div>
          <div class="left"><strong>{$aModule.form.recipient.phone}</strong></div>
          <div class="clear"></div>
          <div class="fLabel" style="padding-top:0; width: auto;">Osoba odbierająca:</div>
          <div class="left"><strong>{$aModule.form.recipient.name} {$aModule.form.recipient.surname}</strong></div>
          <div class="clear"></div>
        </div>
			</div>
      <div class="clear"></div>
  </div>
  <div class="clear"></div>
  {/if} 
  
  <div class="clear" style="margin-bottom: 40px;"></div>

  
    {*<div class="koszykPodtytul2">*}
      {*<strong>Uwagi dotyczące zamówienia:</strong>*}
      {if $aModule.form.is_paczkomaty != TRUE  && $aModule.form.is_poczta_polska != TRUE && $aModule.form.is_ruch != TRUE && $aModule.form.is_orlen != TRUE}
        <div class="koszykPodtytul2Info">
          Pamiętaj, że kurierzy pracują w godzinach 10 - 17, więc wskazane jest podanie adresu pod którym przebywasz w tych godzinach. 
        </div>
      {/if}
    {*</div>*}
  
  
  
    {*<div class="clear"></div>*}
	
    {*<div class="fRowRemarks" >*}
      {*<div class="fLabel" style="width: 550px;"><strong>Uwagi dotyczące zamówienia:</strong> <span style="color: #c30500;font-size: 12px;">(pole uwagi nie służy do wpisywania adresów)</span></div>*}
      {*<div class="fInput">{$aModule.form.remarks.remarks.input}</div>*}
    {*</div>*}
    
  {*{if $aModule.form.is_paczkomaty != TRUE && $aModule.form.is_poczta_polska != TRUE && $aModule.form.is_ruch != TRUE && $aModule.form.is_orlen != TRUE}*}
    {*<div class="fRowRemarks fRowRemarks_2" style="width: 325px;padding: 0; float: left !important;" id="transport_info">*}
      {*<div class="fLabel" style="width: 325px;"><strong>Uwagi dla kuriera:</strong><span style="color: #c30500;font-size: 12px;">(np. nie działa domofon)</span> (max. 150 zn.)</div>*}
      {*<div class="fInput">{$aModule.form.remarks.transport_remarks.input}</div>*}
    {*</div>*}
  {*{/if}*}
  <div class="clear" style="height: 30px;"></div>
		<div class="editAddressesButtonInfo" style="background: url('/images/gfx/imeem.png') 35px center no-repeat; border: 2px solid #C30500; padding: 30px 30px 30px 80px; margin: 0 0 18px; text-align: center">
       <span style="font-size: 13px; font-weight: bold;"> Faktura VAT w formie elektronicznej przesłana zostanie po zrealizowaniu zamówienia na Twój adres e-mail <strong>{if $smarty.session.w_user.email != ""}{$smarty.session.w_user.email}{else}{$aModule.cart.step_3_logged_out.email}{/if}</strong></span> 
		</div>
      
  <div class="clear" style="height: 30px;"></div>
  
	
  {** BUTTONY **}
  <div class="buttonLine buttonLineBig" style="margin-top: 20px;">
    <a href="/koszyk/step3.html" class="left" style="float: left;background: url('/images/gfx/button-wroc.gif') no-repeat;width: 85px;height: 33px;" title="Wróć"></a>
    
    {strip}
    <input type="submit" class="doOrderButton" 
           {if $aModule.cart.payment.ptype == "card" || $aModule.cart.payment.ptype == "platnoscipl"}
             value="Zamów i zapłać" 
           {elseif $aModule.cart.payment.ptype == "bank_14days"}
             value="Zamów i zapłać w ciągu 14 dni" 
           {elseif $aModule.cart.payment.ptype == "bank_transfer"}
             value="Zamów i zapłać przelewem" 
           {elseif $aModule.cart.payment.ptype == "postal_fee"}
             value="Zamów i zapłać przy odbiorze" 
           {else}
             value="Zamów i zapłać"
           {/if}
           alt="Składam zamówienie i płacę" name="send" tabindex="24" style="cursor: pointer; float: right;" />
    {/strip}
    <div class="buttonLineBigCena" style="margin-top: 3px; float: right;"><strong>Wartość do zapłaty: <b><span id="total_cost2">{$aModule.cart.to_pay|format_number:2:',':' '}</span> {$aLang.common.currency}</b></strong></div>
  </div>
  
  
  {$aModule.form.validator.footer}
        
</div>
  
<script type="text/javascript">
  {literal}
    $(document).ready(function(){
      
      function formAddress(aData){
        var sStr;
        sStr = '<strong>'+aData[3]+' '+aData[4]+'<br />';
        sStr += aData[1]+' '+aData[2]+'</strong><br /><br />';
        sStr += aData[9]+'<br />';
        return sStr;
      }
      
      function formAddressPocztaPolska(aData){
        var sStr;
        sStr = '<strong>'+aData['postcode']+' '+aData['city']+'<br />';
        sStr += aData['street']+'</strong><br /><br />';
        sStr += aData['hours']+'<br />';
        return sStr;
      }
      
      function formAddressPaczkaWRuchu(aData){
        var sStr;
        sStr = '<strong>'+aData['city']+' ';
        sStr += aData['street']+'</strong><br /><br />';
        sStr += aData['locale']+'<br />';        
        sStr += aData['hours']+'<br />';
        return sStr;
      }
      
      function formAddressOrlen(aData){
        var sStr;
        sStr = '<strong>'+aData['PostalCode']+' '+aData['City']+'<br />';
        sStr += aData['AddressLine']+'</strong><br /><br />';
        sStr += aData['Name']+'<br /><br />'+aData['FormatedHours'];
        
        return sStr;
      }
      
      if ($("#point_of_receipt").length > 0 && $("#point_of_receipt").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetPaczkomatyMachines.php?term='+$("#point_of_receipt_sel").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          var aData = $.parseJSON(returnData[0].data);
          $("#point_of_receipt_description").html(formAddress(aData));
        });
      }
      
      if ($("#point_of_receipt_poczta_polska").length > 0 && $("#point_of_receipt_poczta_polska").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetUrzedyWydajaceEPrzesylki.php?term='+$("#point_of_receipt_sel_poczta_polska").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          var aData = $.parseJSON(returnData[0].data);
          $("#point_of_receipt_description_poczta_polska").html(formAddressPocztaPolska(aData));
        });
      }
      
      if ($("#point_of_receipt_ruch").length > 0 && $("#point_of_receipt_ruch").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetPaczkaWRuchuPoints.php?term='+$("#point_of_receipt_sel_ruch").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          var aData = $.parseJSON(returnData[0].data);
          $("#point_of_receipt_description_ruch").html(formAddressPaczkaWRuchu(aData));
        });
      }
      
      if ($("#point_of_receipt_orlen").length > 0 && $("#point_of_receipt_orlen").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetOrlenPoints.php?term='+$("#point_of_receipt_sel_orlen").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          var aData = $.parseJSON(returnData[0].data);
          $("#point_of_receipt_description_orlen").html(formAddressOrlen(aData));
        });
      }
    });
  {/literal}
</script>