<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 2/4 (podsumowanie koszyka)</h1>
</div>
<div class="clear"></div>
<div id="step2">
	<div class="clear"></div>
	{$aModule.form.header}			
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.second_invoice}	
	{$aModule.form.payment_platnosci}
	
	<div class="koszykPodtytul2">
		<div id="koszyk_kod_rabatowy">
			<div class="name">{$aModule.lang.discount_code}:</div>
			<input class="input_rabat_koszyk text left" type="text" value="{$aModule.cart.discount_code.code}" maxlength="32" type="text" name="discount_code" />
			<div id="confirmDiscountCode"></div>
			<div class="hintBox" onmouseover="showhint('Pole na kod rabatowy.', this, event, '160px')"></div>	
		</div>
		<strong>{$aModule.lang.list_items}</strong>
	</div>
	<div class="clear"></div>
	<table cellspacing="0" cellpadding="0" class="koszykPods">
		<tr>
			<th class="pozycja Center">Lp.</th>
			<th class="nazwa Left"  colspan="2">{$aModule.lang.list_name}</th>
			<th class="cena Center" style="width: 90px;">{$aModule.lang.email_shipment_time}</th>
			<th class="cena Center" style="width: 90px;">{$aModule.lang.list_price_brutto}</th>
			<th class="ilosc Center" style="width: 50px;">{$aModule.lang.list_quantity}</th>
			<th class="wartosc Center" style="width: 100px;">{$aModule.lang.total_products_brutto}</th>
			<th class="faktura Center" style="width: 80px;">{$aModule.lang.second_invoice}</th>
		</tr>
		{assign var=pozycja value=0}
		{foreach from=$aModule.cart.products key=iId item=aItems name=types}
			{assign var=pozycja value=$pozycja+1}
			<tr>
				<td class="pozycja Center">{$pozycja}.</td>
				<td class="zdjecie"><a href="{$aItems.link}">{if !empty($aItems.image.src)}<img style="width:25px;" src="{$aItems.image.src}" alt="{$aItems.name}" />{else}<img style="width:25px;" src="/images/gfx/brak.png" alt="Brak okładki" />{/if}</a></td>
				<td class="nazwa">{if isset($aItems.link)}<a href="{$aItems.link}" target="_blank">{$aItems.name}</a>{else}{$aItems.name}{/if}
          
          {if !empty($aItems.promotions)}
          <ul>
            {foreach from=$aItems.promotions  item=aPromotion}
              <li><span style="color: #c30500;">{$aPromotion.message}</span></li>
            {/foreach}
          </ul>
          {/if}
          {*
					{if isset($aItems.promo_types) && $aItems.promo_types.code_discount_books === true }
            <span style="color: #c30500;"> - Rabat {$aModule.cart.discount_code.code_discount}%</span>
          {/if}
          *}
				</td>
				<td class="cena Center"><strong>{if $aItems.shipment_date!=''}od:<br/>{$aItems.shipment_date_f}r. {else}{if $aItems.shipment_days > 0}{$aItems.shipment_days} - {$aItems.transport_days} {$aModule.lang.days} {else} {$aModule.lang.shipment_now} {/if}{/if} </strong></td>
				<td class="cena Center">{if $aItems.promo_price > 0}<span>{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</span><br/>{$aItems.promo_price|format_number:2:',':' '} {$aLang.common.currency}{else}{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}{/if}</td>
				<td class="ilosc Center">{$aItems.quantity}</td>
				<td class="wartosc Center">{if $aItems.total_without_promo ne $aItems.total_price_brutto}<span>{$aItems.total_without_promo|format_number:2:',':' '} {$aLang.common.currency}</span><br/> <strong>{$aItems.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{else}<strong>{$aItems.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{/if}</td>
				<td class="faktura Center">{$aModule.invoice2.items[$iId]}</td>
			</tr>
		{/foreach}	
	</table>
	
	<table cellspacing="0" cellpadding="0" class="koszykSuma" style="border-bottom: 0;border-top: 1px solid #eae9e9;margin-top: 1px;">	
		<tr>
			<td width="500" class="koszykSumaInfo">
				{if !empty($aModule.cart.discount_code) && empty($aModule.cart.discount_code.books) && empty($aModule.cart.discount_code.extra) && $aModule.cart.discount_code.code_discount > 0.00}
					<table cellspacing="0" cellpadding="0">	
						<tr>
							<td valign="middle" align="center">
								<img src="/images/gfx/ikona_procent.gif" />
							</td>					
							<td width="100%">
								{$aModule.kod_rabatowy_info}
							</td>					
						</tr>
					</table>					
				{else}&nbsp;{/if}
			</td>
			<td align="right">
				<table cellspacing="0" cellpadding="0" class="right">	
			       <tr>
              <td class="koszykSumyPrawo" valign="baseline">
                  Wartość katalogowa: <s>{$aModule.cart.total_without_promo|format_number:2:',':' '}</s>{$aLang.common.currency}
                  <div class="clear"></div>
                  Wartość z rabatami: <span>
                    <strong> 
                      {if $aModule.cart.old.promo_price > 0.00}
                        {$aModule.cart.old.promo_price|format_number:2:',':' '}
                      {else}
                        {$aModule.cart.total_price_brutto|format_number:2:',':' '}
                      {/if}
                      {$aLang.common.currency} 
                    </strong>
                  </span>
                  <div class="clear"></div>
                  {* $aModule.cart.old.promo_price != $aModule.cart.total_price_brutto && *}
                  {if !empty($aModule.cart.discount_code) && empty($aModule.cart.discount_code.books) && empty($aModule.cart.discount_code.extra) && $aModule.cart.discount_code.code_discount > 0.00}
                    <strong>Wartość z kodem rabatowym ({$aModule.cart.discount_code.code_discount}%): <span style="color: #cc0400">{$aModule.cart.total_price_brutto|format_number:2:',':' '}{$aLang.common.currency}</span></strong>
                  {/if} 
                  {if !empty($aModule.cart.discount_code) && (!empty($aModule.cart.discount_code.books) ||  !empty($aModule.cart.discount_code.extra)) && $aModule.cart.discount_code.code_discount > 0.00}
                    <strong>Wartość z kodem rabatowym: <span style="color: #cc0400">{$aModule.cart.total_price_brutto|format_number:2:',':' '}{$aLang.common.currency}</span></strong>
                  {/if} 
              </td>
			       </tr>	
			       {if ($aModule.cart.user_balance > 0)}
			       	<tr>
			           <td class="koszykSumyPrawo">
				          {$aModule.lang.user_balance_info}:
				          <strong style="color: #c30500;">{$aModule.cart.user_balance|format_number:2:',':' '}{$aLang.common.currency}</strong>
			           	</td>
			        </tr> 
			       {/if}    
				</table>				
			</td>
		</tr>
	</table>	
	
	<div class="clear" style="height: 10px;"></div>
	<div class="koszykPodtytul2"><strong>{$aModule.lang.payment}</strong></div>
	<div class="clear"></div>
	<table cellspacing="1" cellpadding="1" class="koszyk platnosci">
		<tr>
			<th class="dostawa" style="width: 256px;border: 0;">Sposób dostawy</th>
			<th class="platnosc Left" style="border: 0;">Metoda płatności</th>
		</tr>
		{foreach from=$aModule.payment_form item=transport key=t name=kreska}
			
			<tr>
				<td style="vertical-align: top;{if !$smarty.foreach.kreska.last}border-bottom: 1px solid #eae9e9;{/if}" width="270">
					{if !empty($transport[0].transport_logo)}<img class="left" style="padding: 0 10px 0 0;" src="/{$transport[0].transport_logo}" alt="{$t}" />{/if}
					<span>
						<br />
						{if ($t) == 'odbior-osobisty'}{$transport[0].transport_name}<br />(na terenie Warszawy)
						{elseif ($t) == 'paczkomaty_24_7'}Odbiór osobisty w <br />
							{$transport[0].transport_name} <br />
							<a style="color:#C30500; text-decoration:underline;" href="/paczkomaty-mini-przewodnik/" target="_blank">co to jest ?</a>
            {elseif ($t) == 'poczta_polska'}
              <br />
							{*$transport[0].transport_name*}
              Poczta Polska <br />
              <strong>odbiór w placówce pocztowej</strong><br />
                <a style="color:#C30500; text-decoration:underline;" href="http://www.poczta-polska.pl/paczki-i-listy/odbior/paczki/odbior-w-punkcie/" target="_blank">co to jest ?</a>
            {elseif ($t) == 'ruch'}
              {*$transport[0].transport_name*}
              Paczka w RUCHu <br />
              <strong>odbiór w kiosku RUCHu</strong><br />
                <a style="color:#C30500; text-decoration:underline;" href="https://www.paczkawruchu.pl/#/pl/odbierz-paczke" target="_blank">co to jest ?</a>
            {elseif ($t) == 'orlen'}
              {*$transport[0].transport_name*}
              Stacja z Paczką <br />
              <strong>odbiór na stacji Orlen</strong><br />
                <a style="color:#C30500; text-decoration:underline;" href="http://stacjazpaczka.pl/oferta-dla-klientow-indywidualnych.html" target="_blank">co to jest ?</a>
            {elseif ($t) == 'tba'}
              {$transport[0].transport_name}
              <strong>Warszawa i okolice</strong><br />
            {else}
							{$transport[0].transport_name} 	
						{/if}
					</span>	
				</td>
				<td style="vertical-align: top;{if !$smarty.foreach.kreska.last} border-bottom: 1px solid #eae9e9;{/if}">
					<ul class="payment_types" id="{$t}">
						{foreach from=$transport item=payment key=k}
							<li>
								<div class="left">
									<input type="radio" value="{$payment.id}" id="payment_{$payment.id}" name="payment_type" class="payment_type_radio left" {if isset($aModule.cart.payment.id) && $aModule.cart.payment.id eq $payment.id} checked="checked"{/if} />
                  <label style="margin-left: 5px;padding-top: 3px;cursor: pointer;float: left;" for="payment_{$payment.id}">{$payment.name} (koszt dostawy - <strong style="color: #cc0400;">{if $payment.cost == 0}<span id="gratis">{$aModule.lang.transport_gratis}</span>{else}{$payment.cost}{$aLang.common.currency}{/if}</strong>)</label>
								</div>
								{if !empty($payment.description)}<div class="hintBox" style="margin: 2px 0 0 8px;" onmouseover="showhint('{$payment.description}', this, event, '220px')"></div>{/if}
								<div class="clear"></div>
							</li>
						{/foreach}
					</ul>
					{if $t == "paczkomaty_24_7"}
						<div class="clear"></div>			
								
						<div id="choose_paczkomaty" style="display: none;">
              <div class="choose_paczkomaty_line">
                <span id="point_of_receipt_pools" {if !empty($smarty.session.wu_cart.step_2.point_of_receipt_sel)}style="display:none;"{/if}>
                  <strong>Wprowadź kod pocztowy lub miejscowość:&nbsp;&nbsp;</strong>
                  <input class="redInput" name="point_of_receipt_sel" id="point_of_receipt_sel" type="text" style="width: 280px" value="{$smarty.session.wu_cart.step_2.point_of_receipt_sel|htmlspecialchars}" />&nbsp;<font color="#c30500"><b>*</b></font>
                </span>
                <input type="hidden" class="point_of_receipt" name="point_of_receipt" id="point_of_receipt" value="{$smarty.session.wu_cart.step_2.point_of_receipt|htmlspecialchars}" />
                
                <div class="clear"></div>
                <div style="{if empty($smarty.session.wu_cart.step_2.point_of_receipt_sel)}display: none;{/if} float:left; background: #ffd000; padding: 10px; border: 2px dotted #000;" id="point_of_receipt_change">
                  Wybrany paczkomat: 
                  <div id="point_of_receipt_description">{$smarty.session.wu_cart.step_2.point_of_receipt_sel|htmlspecialchars}</div>
                  <br />
                  <a href="javascript:void(0);" style="color: #c30500;" id="do_change_point_of_receipt">Zmień paczkomat</a> &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="javascript:void(0);" style="color: #c30500; font-weight: normal !important;" id="do_show_point_of_receipt">Pokaż na mapie</a>
                </div>
                <div class="clear"></div>
                {*$aModule.paczkomaty*}
                Sprawdź gdzie jest najbliższy paczkomat InPost <a href="http://www.paczkomaty.pl/pl/dla-ciebie/znajdz-paczkomat#narrow-content" target="blank"><strong>na mapie.</strong></a>
              </div>
              
							<div class="choose_paczkomaty_line">
                
                W celu dokonania obsługi wysyłki zamówienia przy użyciu usługi <strong>Paczkomaty InPost</strong>, Twój adres e-mail <strong>{$smarty.session.w_user.email}</strong> oraz numer telefonu komórkowego zostanie przekazany firmie spedycyjnej InPost. <strong>Sprawdź dokładnie swoje dane</strong>, ponieważ na Twój adres e-mail i telefon komórkowy zostanie wysłane hasło pozwalające na odbiór paczki w wybranym przez Ciebie paczkomacie.
              </div>
							<div class="choose_paczkomaty_line"><strong>Numer telefonu komórkowego:</strong>&nbsp;&nbsp;&nbsp;<input type="text" value="{$smarty.session.wu_cart.step_2.phone_paczkomaty|htmlspecialchars}" class="redInput" name="phone_paczkomaty" id="phone_paczkomaty" /><span id="phone_paczkomaty_war">*</span></div>
							<div class="choose_paczkomaty_line">
                <input checked="checked" type="checkbox" value="1" name="regulations_paczkomaty" id="regulations_paczkomaty" {if $smarty.session.wu_cart.step_2.regulations_paczkomaty}checked="checked"{/if} />&nbsp;&nbsp;<label for="regulations_paczkomaty">Akceptuję <a target="blank" href="http://www.paczkomaty.pl/dokumenty/Regulamin_swiadczenia_uslugi_Paczkomaty.pdf">regulamin</a> Paczkomaty InPost</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#c30500">*</font> - pole wymagane
							</div>
						</div>					
					{/if}
          
					{if $t == "poczta_polska"}
						<div class="clear"></div>			
								
						<div id="choose_poczta_polska" style="display: none;">
              <div class="choose_poczta_polska_line">
                <span id="point_of_receipt_pools_poczta_polska" {if !empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_poczta_polska)}style="display:none;"{/if}>
                  <strong>Wprowadź kod pocztowy lub miejscowość:&nbsp;&nbsp;</strong>
                  <input class="redInput" name="point_of_receipt_sel_poczta_polska" id="point_of_receipt_sel_poczta_polska" type="text" style="width: 280px" value="{$smarty.session.wu_cart.step_2.point_of_receipt_sel_poczta_polska|htmlspecialchars}" />&nbsp;<font color="#c30500"><b>*</b></font>
                </span>
                <input type="hidden" class="point_of_receipt" name="point_of_receipt_poczta_polska" id="point_of_receipt_poczta_polska" value="{$smarty.session.wu_cart.step_2.point_of_receipt_poczta_polska|htmlspecialchars}" />
                
                <div class="clear"></div>
                <div style="{if empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_poczta_polska)}display: none;{/if} float:left; background: #e71905; padding: 10px; border: 2px dotted #000; color: #fff;" id="point_of_receipt_change_poczta_polska">
                  Wybrana placówka pocztowa z <strong>Odbiorem w punkcie</strong>: 
                  <div id="point_of_receipt_description_poczta_polska">{$smarty.session.wu_cart.step_2.point_of_receipt_sel_poczta_polska|htmlspecialchars}</div>
                  <br />
                  <a href="javascript:void(0);" style="color: #fedb00;" id="do_change_point_of_receipt_poczta_polska">Zmień placówkę pocztową</a> &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="javascript:void(0);" style="color: #fedb00;" id="do_show_point_of_receipt_poczta_polska">Pokaż na mapie</a>
                </div>
                <div class="clear"></div>
                {*$aModule.poczta_polska*}
                Sprawdź gdzie jest najbliższa placówka pocztowa z <strong>Odbiorem w punkcie</strong> <a href="http://odbiorwpunkcie.poczta-polska.pl/" target="blank"><strong>na mapie.</strong></a>
              </div>
              
							<div class="choose_poczta_polska_line">
                
                W celu realizacji wysyłki zamówienia przy użyciu usługi <strong>Odbiór w punkcie</strong>, Twój adres e-mail <strong>{$smarty.session.w_user.email}</strong>, numer telefonu komórkowego oraz dane do dostawy zostaną przekazane Poczcie Polskiej. <strong>Sprawdź dokładnie swoje dane</strong>, ponieważ na Twój adres e-mail lub telefon komórkowy zostanie wysłana wiadomość o przesyłce oczekującej na odbiór w wybranej przez Ciebie placówce pocztowej.
              </div>
							<div class="choose_poczta_polska_line"><strong>Numer telefonu komórkowego:</strong>&nbsp;&nbsp;&nbsp;<input type="text" value="{$smarty.session.wu_cart.step_2.phone_poczta_polska|htmlspecialchars}" class="redInput" name="phone_poczta_polska" id="phone_poczta_polska" /><span id="phone_poczta_polska_war">*</span></div>
							<div class="choose_poczta_polska_line">
                <input checked="checked" type="checkbox" value="1" name="regulations_poczta_polska" id="regulations_poczta_polska" {if $smarty.session.wu_cart.step_2.regulations_poczta_polska}checked="checked"{/if} />&nbsp;&nbsp;<label for="regulations_poczta_polska">Akceptuję <a target="blank" href="http://www.pocztex.pl/jowisz/uploads/2013/12/REGULAMIN-POCZTEX.zip">regulamin</a> usługi Odbiór w punkcie</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#c30500">*</font> - pole wymagane
							</div>
						</div>					
					{/if}
          
					{if $t == "ruch"}
						<div class="clear"></div>			
								
						<div id="choose_ruch" style="display: none;">
              <div class="choose_ruch_line">
                <span id="point_of_receipt_pools_ruch" {if !empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_ruch)}style="display:none;"{/if}>
                  <strong>Wprowadź miejscowość lub ulicę:&nbsp;&nbsp;</strong>
                  <input class="redInput" name="point_of_receipt_sel_ruch" id="point_of_receipt_sel_ruch" type="text" style="width: 280px" value="{$smarty.session.wu_cart.step_2.point_of_receipt_sel_ruch|htmlspecialchars}" />&nbsp;<font color="#c30500"><b>*</b></font>
                </span>
                <input type="hidden" class="point_of_receipt" name="point_of_receipt_ruch" id="point_of_receipt_ruch" value="{$smarty.session.wu_cart.step_2.point_of_receipt_ruch|htmlspecialchars}" />
                
                <div class="clear"></div>
                <div style="{if empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_ruch)}display: none;{/if} float:left; background: #504747; padding: 10px; border: 2px dotted #000; color: #fff;" id="point_of_receipt_change_ruch">
                  Wybrany kiosk RUCHu, w którym odbierzesz przesyłkę: 
                  <div id="point_of_receipt_description_ruch">{$smarty.session.wu_cart.step_2.point_of_receipt_sel_ruch|htmlspecialchars}</div>
                  <br />
                  <a href="javascript:void(0);" style="color: #fedb00;" id="do_change_point_of_receipt_ruch">Zmień kiosk RUCHu</a> &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="javascript:void(0);" style="color: #fedb00;" id="do_show_point_of_receipt_ruch">Pokaż na mapie</a>
                </div>
                <div class="clear"></div>
                {*$aModule.ruch*}
                Sprawdź <a href="https://www.paczkawruchu.pl/#/pl/znajdz-kiosk" target="blank"><strong>na mapie</strong></a> gdzie znajduje się najbliższy kiosk RUCHu, w którym możesz odebrać przesyłkę
              </div>
              
							<div class="choose_ruch_line">
                
                W celu realizacji wysyłki zamówienia przy użyciu usługi <strong>Paczka w Ruchu</strong>, Twój adres e-mail <strong>{$smarty.session.w_user.email}</strong>, numer telefonu komórkowego oraz dane do dostawy zostaną przekazane firmie "RUCH" S.A. <strong>Sprawdź dokładnie swoje dane</strong>, ponieważ na Twój adres e-mail lub telefon komórkowy zostanie wysłana wiadomość o przesyłce oczekującej na odbiór w wybranym przez Ciebie kiosku RUCH.
              </div>
							<div class="choose_ruch_line"><strong>Numer telefonu komórkowego:</strong>&nbsp;&nbsp;&nbsp;<input type="text" value="{$smarty.session.wu_cart.step_2.phone_ruch|htmlspecialchars}" class="redInput" name="phone_ruch" id="phone_ruch" /><span id="phone_ruch_war">*</span></div>
							<div class="choose_ruch_line">
                <input checked="checked" type="checkbox" value="1" name="regulations_ruch" id="regulations_ruch" {if $smarty.session.wu_cart.step_2.regulations_ruch}checked="checked"{/if} />&nbsp;&nbsp;<label for="regulations_ruch">Akceptuję <a target="blank" href="https://www.paczkawruchu.pl/files/Regulamin.pdf">regulamin</a> usługi Paczka w Ruchu</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#c30500">*</font> - pole wymagane
							</div>
						</div>					
					{/if}
          
          
					{if $t == "orlen"}
						<div class="clear"></div>			
								
						<div id="choose_orlen" style="display: none;">
              <div class="choose_orlen_line">
                <span id="point_of_receipt_pools_orlen" {if !empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_orlen)}style="display:none;"{/if}>
                  <strong>Wprowadź kod pocztowy lub miejscowość:&nbsp;&nbsp;</strong>
                  <input class="redInput" name="point_of_receipt_sel_orlen" id="point_of_receipt_sel_orlen" type="text" style="width: 280px" value="{$smarty.session.wu_cart.step_2.point_of_receipt_sel_orlen|htmlspecialchars}" />&nbsp;<font color="#c30500"><b>*</b></font>
                </span>
                <input type="hidden" class="point_of_receipt" name="point_of_receipt_orlen" id="point_of_receipt_orlen" value="{$smarty.session.wu_cart.step_2.point_of_receipt_orlen|htmlspecialchars}" />
                
                <div class="clear"></div>
                <div style="{if empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_orlen)}display: none;{/if} float:left; background: #da3522; padding: 10px; border: 2px dotted #000; color: #fff;" id="point_of_receipt_change_orlen">
                  Wybrana stacja Orlen, na której odbierzesz przesyłkę: 
                  <div id="point_of_receipt_description_orlen">{$smarty.session.wu_cart.step_2.point_of_receipt_sel_orlen|htmlspecialchars}</div>
                  <br />
                  <a href="javascript:void(0);" style="color: #fedb00;" id="do_change_point_of_receipt_orlen">Zmień stację Orlen</a> &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="javascript:void(0);" style="color: #fedb00;" id="do_show_point_of_receipt_orlen">Pokaż na mapie</a>
                </div>
                <div class="clear"></div>
                {*$aModule.orlen*}
                Sprawdź <a href="http://stacjazpaczka.pl/nasze-punkty.html" target="blank"><strong>na mapie</strong></a> gdzie znajduje się najbliższa stacja Orlen, na której możesz odebrać przesyłkę
              </div>
              
							<div class="choose_orlen_line">
                
                W celu realizacji wysyłki zamówienia przy użyciu usługi <strong>Stacja z Paczką</strong>, Twój adres e-mail <strong>{$smarty.session.w_user.email}</strong>, numer telefonu komórkowego oraz dane do dostawy zostaną przekazane firmie Orlen S.A. <strong>Sprawdź dokładnie swoje dane</strong>, ponieważ na Twój adres e-mail lub telefon komórkowy zostanie wysłana wiadomość o przesyłce oczekującej na odbiór na wybranej przez Ciebie stacji Orlen.
              </div>
							<div class="choose_orlen_line"><strong>Numer telefonu komórkowego:</strong>&nbsp;&nbsp;&nbsp;<input type="text" value="{$smarty.session.wu_cart.step_2.phone_orlen|htmlspecialchars}" class="redInput" name="phone_orlen" id="phone_orlen" /><span id="phone_orlen_war">*</span></div>
							<div class="choose_orlen_line">
                <input checked="checked" type="checkbox" value="1" name="regulations_orlen" id="regulations_orlen" {if $smarty.session.wu_cart.step_2.regulations_orlen}checked="checked"{/if} />&nbsp;&nbsp;<label for="regulations_orlen">Akceptuję <a target="blank" href="http://stacjazpaczka.pl/regulamin/">regulamin</a> usługi Stacja z Paczką</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#c30500">*</font> - pole wymagane
							</div>
						</div>					
					{/if}
					{if $t == "tba"}
						<div class="clear"></div>			
								
						<div id="choose_tba" style="display: none;">
              <div class="choose_tba_line">
                <span id="point_of_receipt_pools_tba" {if !empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_tba)}style="display:none;"{/if}>
                  <strong>Wprowadź kod pocztowy doręczenia przesyłki:&nbsp;&nbsp;</strong>
                  <input class="redInput" name="point_of_receipt_sel_tba" id="point_of_receipt_sel_tba" type="text" style="width: 280px" value="{$smarty.session.wu_cart.step_2.point_of_receipt_sel_tba|htmlspecialchars}" />&nbsp;<font color="#c30500"><b>*</b></font>
                </span>
                <input type="hidden" class="point_of_receipt" name="point_of_receipt_tba" id="point_of_receipt_tba" value="{$smarty.session.wu_cart.step_2.point_of_receipt_tba|htmlspecialchars}" />
                
                <div class="clear"></div>
                <div style="{if empty($smarty.session.wu_cart.step_2.point_of_receipt_sel_tba)}display: none;{/if} float:left; background: green; padding: 10px; border: 2px dotted #000; color: #fff;" id="point_of_receipt_change_tba">
                  Wprowadzony kod pocztowy znajduje się w obszarze doręczeń firmy TBA Express:
                  <div id="point_of_receipt_description_tba">{$smarty.session.wu_cart.step_2.point_of_receipt_sel_tba|htmlspecialchars}</div>
                  <br />
                  <a href="javascript:void(0);" style="color: #fff;" id="do_change_point_of_receipt_tba">Zmień kod pocztowy doręczenia przesyłki</a> &nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <br />
                <div class="clear"></div>
                {*$aModule.tba*}
              </div>
              <div class="choose_orlen_line">
                Kurier Warszawa i okolice.<br />
                {*Aby skorzystać z tej formy dostawy, kod pocztowy doręczenia przesyłki powinien znajdować się w obszarze doręczeń firmy kurierskiej TBA Express.*}
              </div>
						</div>					
					{/if}          
          
				</td>
			</tr>
		{/foreach}
	</table>

	<div class="clear"></div>

  
	<div class="koszykWysCena" style="display: none;">
		<div class="koszykWysCenaTransport"  {if $aModule.cart.payment.cost <= 0.00}style="display: none;"{/if}>
			{$aModule.lang.total_transport_cost} <strong><span id="total_transport_cost">{if $aModule.cart.payment.cost>0} {$aModule.cart.payment.cost|format_number:2:',':' '}{else}{$aModule.cart.payment.cost|format_number:2:',':' '}{/if}</span> {$aLang.common.currency}</strong>
		</div>
		<div class="clear"></div>
		<strong>{$aModule.lang.to_pay} <b><span id="total_cost">{$aModule.cart.to_pay|format_number:2:',':' '}</span> {$aLang.common.currency}</b></strong> 
	</div>
  
	<div class="koszykWysInfo" style="display: none;">
		<span>{$aModule.shipment_info}</span>
		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>
	<div id="paymentBankSection"{if $aModule.payment_bank_show == '0'} style="display: none;"{/if}>
		<h2>Wybierz swój bank, z którego chcesz wykonać przelew elektroniczny:</h2>
		{if !empty($aModule.payment_bank)}
		<ul>
			{foreach from=$aModule.payment_bank item=bank key=t name=payment_bank_form}
				<li>{$bank.input}<img src="/images/gfx/platnoscipl/{$bank.code}.gif" alt="" />{$bank.label}</li>
			{/foreach}
		</ul>
		{/if}
	</div>
	<div class="clear"></div>

  
	<div class="buttonLine buttonLineBig" style="margin-top: 20px;">
		<a href="/koszyk" class="left" style="float: left;background: url('/images/gfx/button-zmien-zawartosc-koszyka.gif') no-repeat;width: 216px;height: 33px;" title="Zmień zawartość koszyka"></a>
    <div class="buttonDomyslny">
      <div class="buttonDomyslnyLewo"></div>
      <div class="buttonDomyslnySrodek"><a onclick="$('#step2Form').submit();">Zatwierdź i przejdź dalej <span style="font-size: 16px">&raquo;</span></a></div>
      <div class="buttonDomyslnyPrawo"></div>
    </div>
    <div class="buttonLineBigCena" style="margin-top: 3px;"><strong>{$aModule.lang.to_pay} <b><span id="total_cost2">{$aModule.cart.to_pay|format_number:2:',':' '}</span> {$aLang.common.currency}</b></strong></div>
    <div class="clear"></div>
	</div>
</div>
  
  

{** JS ukrywanie uwag dla kuriera oraz pokazywanie dostępnych paczkomatow odbioru **}
{literal}
	<script type="text/javascript">
    
		$(document).ready(function() {
      $("input:checkbox[name^=\'item_2nd\']").change(function() {
        var iChecked = $("input:checkbox[name^=\'item_2nd\']:checked").length;
        var iCountCheckbox = $("input:checkbox[name^=\'item_2nd\']").length;
        if (iChecked === iCountCheckbox) {
          $( this ).tooltip( "destroy" );
          $(this).prop('checked', false);
          $(this).tooltip({
            position: {
              at: "left top",
              show: { effect: "blind", duration: 50 }
            },
            content: "<span style='color: red; font-weight: bold;'>Przynajmniej jedna pozycja musi pozostać na pierwszej fakturze</span>"
          });
          $(this).tooltip( "open" );
        } else if((iChecked+1) === iCountCheckbox) {
          // został jeden do zaznaczenia
          
          var oNotChecked = $("input:checkbox[name^=\'item_2nd\']:not(:checked)");
          $( oNotChecked ).tooltip( "destroy" );
          $( oNotChecked ).tooltip({
            position: {
              at: "left top",
              show: { effect: "blind", duration: 50 }
            },
            content: "<span style='color: red; font-weight: bold;'>Przynajmniej jedna pozycja musi pozostać na pierwszej fakturze</span>"
          });
          
        } else {
          $( "td.faktura input" ).tooltip( "destroy" );
          $("td.faktura input").attr('title', 'Znaznacz jeśli pozycja powinna\n znaleźć się na drugiej fakturze')
           .tooltip({
            position: {
              at: "left top"
            }
          });
        }
      });
      
      $("td.faktura input").attr('title', 'Znaznacz jeśli pozycja powinna\n znaleźć się na drugiej fakturze')
        .tooltip({
         position: {
           at: "left top"
         }
       });
      
      function formAddress(aData){
        var sStr;
        sStr = '<strong>'+aData[3]+' '+aData[4]+'<br />';
        sStr += aData[1]+' '+aData[2]+'</strong><br /><br />';
        sStr += aData[9]+'<br />';
        return sStr;
      }
      
      function formAddressPocztaPolska(aData){
        var sStr;
        sStr = '<strong>'+aData['postcode']+' '+aData['city']+'<br />';
        sStr += aData['street']+'</strong><br /><br />';
        sStr += aData['hours']+'<br />';
        return sStr;
      }

      function formAddressPaczkaWRuchu(aData){
        var sStr;
        sStr = '<strong>'+aData['city']+' ';
        sStr += aData['street']+'</strong><br /><br />';
        sStr += aData['locale']+'<br />';        
        sStr += aData['hours']+'<br />';
        return sStr;
      }    

      function formAddressOrlen(aData){
        var sStr;
        sStr = '<strong>'+aData['PostalCode']+' '+aData['City']+'<br />';
        sStr += aData['AddressLine']+'</strong><br /><br />';
        sStr += aData['Name']+'<br /><br />'+aData['FormatedHours'];
        
        return sStr;
      }
    
      if ($("#point_of_receipt").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetPaczkomatyMachines.php?term='+$("#point_of_receipt_sel").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          var aData = $.parseJSON(returnData[0].data);
          $("#point_of_receipt_description").html(formAddress(aData));
        });
      }
      
      if ($("#point_of_receipt_poczta_polska").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetUrzedyWydajaceEPrzesylki.php?term='+$("#point_of_receipt_sel_poczta_polska").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          if (returnData[0] !== undefined) {
            var aData = $.parseJSON(returnData[0].data);
            $("#point_of_receipt_description_poczta_polska").html(formAddressPocztaPolska(aData));
          }
        });
      }
      
      if ($("#point_of_receipt_ruch").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetPaczkaWRuchuPoints.php?term='+$("#point_of_receipt_sel_ruch").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          if (returnData[0] !== undefined) {
            var aData = $.parseJSON(returnData[0].data);
            $("#point_of_receipt_description_ruch").html(formAddressPaczkaWRuchu(aData));
          }
        });
      }
      
      if ($("#point_of_receipt_orlen").val() !== '') {
        // uzupełniamy dane punktu odbioru
        $.ajax({
          url: '/internals/GetOrlenPoints.php?term='+$("#point_of_receipt_sel_orlen").val(),
          dataType: 'json'
        })
        .done(function(returnData) {
          if (returnData[0] !== undefined) {
            var aData = $.parseJSON(returnData[0].data);
            $("#point_of_receipt_description_orlen").html(formAddressOrlen(aData));
          }
        });
      }

      if ($("#paczkomaty_24_7").size()) {
        $("input[name='payment_type']").click( function(){
          if($(this).parent().parent().parent().attr('id') == 'paczkomaty_24_7') {
            $("#choose_paczkomaty").show();
          } else {
            $("#choose_paczkomaty").hide();
          }
        });
      }
      
      if ($("#poczta_polska").size()) {
        $("input[name='payment_type']").click( function(){
          if($(this).parent().parent().parent().attr('id') == 'poczta_polska') {
            $("#choose_poczta_polska").show();
          } else {
            $("#choose_poczta_polska").hide();
          }
        });
      }
      
      if ($("#ruch").size()) {
        $("input[name='payment_type']").click( function(){
          if($(this).parent().parent().parent().attr('id') == 'ruch') {
            $("#choose_ruch").show();
          } else {
            $("#choose_ruch").hide();
          }
        });
      }

      if ($("#orlen").size()) {
        $("input[name='payment_type']").click( function(){
          if($(this).parent().parent().parent().attr('id') == 'orlen') {
            $("#choose_orlen").show();
          } else {
            $("#choose_orlen").hide();
          }
        });
      }    
    
      if ($("#tba").size()) {
        $("input[name='payment_type']").click( function(){
          if($(this).parent().parent().parent().attr('id') == 'tba') {
            $("#choose_tba").show();
          } else {
            $("#choose_tba").hide();
          }
        });
      }      
    
      $("#step2Form").submit(function(){
        if ($("input[name='payment_type']:checked").parent().parent().parent().attr('id') === 'paczkomaty_24_7') {
          if ($("#point_of_receipt").val() === '') {
            showSimplePopup('Wybierz paczkomat', false, 4000);
            return false;
          }
          if ($("#phone_paczkomaty").val() === '') {
            showSimplePopup('Podaj numer telefonu komórkowego dla Paczkomatów InPost', false, 4000);
            return false;
          }
          if ($("#regulations_paczkomaty:checked").length <= 0) {
            showSimplePopup('Zaakceptuj regulamin Paczkomatów InPost', false, 4000);
            return false;
          }
        }
 
        if ($("input[name='payment_type']:checked").parent().parent().parent().attr('id') === 'poczta_polska') {
          if ($("#point_of_receipt_poczta_polska").val() === '') {
            showSimplePopup('Wybierz placówkę pocztową z Odbiorem w punkcie', false, 4000);
            return false;
          }
          
          if ($("#phone_poczta_polska").val() === '') {
            showSimplePopup('Podaj numer telefonu komórkowego aby otrzymać sms z powiadomieniem o przesyłce', false, 4000);
            return false;
          }
          
          if ($("#regulations_poczta_polska:checked").length <= 0) {
            showSimplePopup('Zaakceptuj regulamin usługi Odbiór w punkcie', false, 4000);
            return false;
          }
        }
        
        if ($("input[name='payment_type']:checked").parent().parent().parent().attr('id') === 'ruch') {
          if ($("#point_of_receipt_ruch").val() === '') {
            showSimplePopup('Wybierz punkt sieci "RUCH"', false, 4000);
            return false;
          }
          
          if ($("#phone_ruch").val() === '') {
            showSimplePopup('Podaj numer telefonu komórkowego aby otrzymać sms z powiadomieniem o przesyłce', false, 4000);
            return false;
          }
          
          if ($("#regulations_ruch:checked").length <= 0) {
            showSimplePopup('Zaakceptuj regulamin usługi Paczka w Ruchu', false, 4000);
            return false;
          }
        }  
        
        if ($("input[name='payment_type']:checked").parent().parent().parent().attr('id') === 'orlen') {
          if ($("#point_of_receipt_orlen").val() === '') {
            showSimplePopup('Wybierz stację Orlen', false, 4000);
            return false;
          }
          
          if ($("#phone_orlen").val() === '') {
            showSimplePopup('Podaj numer telefonu komórkowego aby otrzymać sms z powiadomieniem o przesyłce', false, 4000);
            return false;
          }
          
          if ($("#regulations_orlen:checked").length <= 0) {
            showSimplePopup('Zaakceptuj regulamin usługi Stacja z Paczką', false, 4000);
            return false;
          }
        }
        
        if ($("input[name='payment_type']:checked").parent().parent().parent().attr('id') === 'tba') {
          if ($("#point_of_receipt_tba").val() === '') {
            showSimplePopup('Wprowadź poprawny kod pocztowy doręczenia przesyłki', false, 4000);
            return false;
          }
        }        
      });
      
			$("input[name='payment_type']").each( function(){
				if ($(this).attr('checked') === 'checked'){
					$(this).click();
				}
			});
      
      $("#do_show_point_of_receipt").click( function() {
        var sURL = "/internals/popupPointOfReceipt.php?code="+$("#point_of_receipt").val();
        window.open(sURL, "Lokalizacja paczkomatu", "width=792,height=750");
      });
      
      $("#do_show_point_of_receipt_poczta_polska").click( function() {
        var sURL = "/internals/popupPointOfReceipt.php?code="+$("#point_of_receipt_poczta_polska").val();
        window.open(sURL, "Lokalizacja Urzędu Pocztowego", "width=792,height=780");
      });
      
      $("#do_show_point_of_receipt_ruch").click( function() {
        var sURL = "/internals/popupPointOfReceipt.php?code="+$("#point_of_receipt_ruch").val();
        window.open(sURL, "Lokalizacja punktu sieci RUCH", "width=792,height=780");
      });

      $("#do_show_point_of_receipt_orlen").click( function() {
        var sURL = "/internals/popupPointOfReceipt.php?code="+$("#point_of_receipt_orlen").val();
        window.open(sURL, "Lokalizacja stacji Orlen", "width=792,height=780");
      });    
    
      $("#do_change_point_of_receipt").click( function() {
        $("#point_of_receipt_pools").show();
        $("#point_of_receipt_change").hide();
        $("#point_of_receipt").val('');
        $("#point_of_receipt_sel").val('');
        $("#point_of_receipt_description").html('');
      });
      
      $("#do_change_point_of_receipt_poczta_polska").click( function() {
        $("#point_of_receipt_pools_poczta_polska").show();
        $("#point_of_receipt_change_poczta_polska").hide();
        $("#point_of_receipt_poczta_polska").val('');
        $("#point_of_receipt_sel_poczta_polska").val('');
        $("#point_of_receipt_description_poczta_polska").html('');
      });
      
      $("#do_change_point_of_receipt_ruch").click( function() {
        $("#point_of_receipt_pools_ruch").show();
        $("#point_of_receipt_change_ruch").hide();
        $("#point_of_receipt_ruch").val('');
        $("#point_of_receipt_sel_ruch").val('');
        $("#point_of_receipt_description_ruch").html('');
      });

      $("#do_change_point_of_receipt_orlen").click( function() {
        $("#point_of_receipt_pools_orlen").show();
        $("#point_of_receipt_change_orlen").hide();
        $("#point_of_receipt_orlen").val('');
        $("#point_of_receipt_sel_orlen").val('');
        $("#point_of_receipt_description_orlen").html('');
      });    

      $("#do_change_point_of_receipt_tba").click( function() {
        $("#point_of_receipt_pools_tba").show();
        $("#point_of_receipt_change_tba").hide();
        $("#point_of_receipt_tba").val('');
        $("#point_of_receipt_sel_tba").val('');
        $("#point_of_receipt_description_tba").html('');
      });   
    
      $("#point_of_receipt_sel").autocomplete({
        source: "/internals/GetPaczkomatyMachines.php",
        minLength: 2,
        contentType: "application/json; charset=utf-8",
        select: function(event, ui) { 
         var aData = $.parseJSON(ui.item.data);
         $("#point_of_receipt_pools").hide();
         $("#point_of_receipt_description").html(formAddress(aData));
         $("#point_of_receipt_change").show();
         $("#point_of_receipt").val(ui.item.id);
        }
      }) 
      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var termsString = $.trim(this.term);
        var searchMask = termsString;
        var regEx = new RegExp("("+searchMask+")", "ig");
        var replaceMask = "<span style='font-weight:bold;'>$1</span>";

        var t = item.label.replace(regEx, replaceMask);

        return $( "<li></li>" )
              .data( "item.autocomplete", item )
              .append( "<a>" + t + "</a>" )
              .appendTo( ul );
      };
      
      $("#point_of_receipt_sel_poczta_polska").autocomplete({
        source: "/internals/GetUrzedyWydajaceEPrzesylki.php",
        minLength: 2,
        contentType: "application/json; charset=utf-8",
        select: function(event, ui) { 
         var aData = $.parseJSON(ui.item.data);
         $("#point_of_receipt_pools_poczta_polska").hide();
         $("#point_of_receipt_description_poczta_polska").html(formAddressPocztaPolska(aData));
         $("#point_of_receipt_change_poczta_polska").show();
         $("#point_of_receipt_poczta_polska").val(ui.item.id);
        }
      }) 
      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var termsString = $.trim(this.term);
        var searchMask = termsString;
        var regEx = new RegExp("("+searchMask+")", "ig");
        var replaceMask = "<span style='font-weight:bold;'>$1</span>";

        var t = item.label.replace(regEx, replaceMask);

        return $( "<li></li>" )
              .data( "item.autocomplete", item )
              .append( "<a>" + t + "</a>" )
              .appendTo( ul );
      };
      
      $("#point_of_receipt_sel_ruch").autocomplete({
        source: "/internals/GetPaczkaWRuchuPoints.php",
        minLength: 2,
        contentType: "application/json; charset=utf-8",
        select: function(event, ui) { 
         var aData = $.parseJSON(ui.item.data);
         $("#point_of_receipt_pools_ruch").hide();
         $("#point_of_receipt_description_ruch").html(formAddressPaczkaWRuchu(aData));
         $("#point_of_receipt_change_ruch").show();
         $("#point_of_receipt_ruch").val(ui.item.id);
        }
      }) 
      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var termsString = $.trim(this.term);
        var searchMask = termsString;
        var regEx = new RegExp("("+searchMask+")", "ig");
        var replaceMask = "<span style='font-weight:bold;'>$1</span>";

        var t = item.label.replace(regEx, replaceMask);

        return $( "<li></li>" )
              .data( "item.autocomplete", item )
              .append( "<a>" + t + "</a>" )
              .appendTo( ul );
      };


      $("#point_of_receipt_sel_orlen").autocomplete({
        source: "/internals/GetOrlenPoints.php",
        minLength: 2,
        contentType: "application/json; charset=utf-8",
        select: function(event, ui) { 
         var aData = $.parseJSON(ui.item.data);
         $("#point_of_receipt_pools_orlen").hide();
         $("#point_of_receipt_description_orlen").html(formAddressOrlen(aData));
         $("#point_of_receipt_change_orlen").show();
         $("#point_of_receipt_orlen").val(ui.item.id);
        }
      }) 
      .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var termsString = $.trim(this.term);
        var searchMask = termsString;
        var regEx = new RegExp("("+searchMask+")", "ig");
        var replaceMask = "<span style='font-weight:bold;'>$1</span>";

        var t = item.label.replace(regEx, replaceMask);

        return $( "<li></li>" )
              .data( "item.autocomplete", item )
              .append( "<a>" + t + "</a>" )
              .appendTo( ul );
      };
      
      $("#point_of_receipt_sel_tba").keyup(function(){
        var tbaPostcode =  $("#point_of_receipt_sel_tba").val();
        var tbaPostcodeLength =  tbaPostcode.length;
        
        if (tbaPostcodeLength == 6) {
          if (tbaPostcode.match(/^\d{2}-\d{3}$/)) {

            $.ajax({
                type: "POST",
                url: "/internals/validateTBAPostcode.php",
                cache: false,
                data: {
                    "postcode": tbaPostcode,
                },
                success: function(aResponse){
                    var aData = $.parseJSON(aResponse);
                    
                    if (aData['status'] == true) {
                      $("#point_of_receipt_pools_tba").hide();
                      $("#point_of_receipt_description_tba").html('<strong>'+tbaPostcode+'</strong>');
                      $("#point_of_receipt_change_tba").show();
                      $("#point_of_receipt_tba").val(tbaPostcode);
                    } else {
                      showSimplePopup('Wprowadzony kod pocztowy znajduje się poza obszarem doręczeń firmy TBA Express', false, 4000);
                      $("#point_of_receipt_sel_tba").val('');
                      return false;
                    }
                }
            });        
          } else {
              showSimplePopup('Wprowadzony kod pocztowy nie jest poprawny', false, 4000);
              return false;
          }
        }
      });              
      
		});
	</script>
{/literal}