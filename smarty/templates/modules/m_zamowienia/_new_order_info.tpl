<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - Podsumowanie złożonego zamówienia</h1>
</div>
<div class="clear"></div>

<div class="summaryTop" style="font-size: 20px; padding:20px; text-align: center">

  
  <strong style="font-size: 28px; display: block; padding:0 0 10px 0;">Dziękujemy</strong><br />
  <p>Właśnie został do Ciebie wysłany e-mail z wszelkimi ważnymi informacjami <b>(sprawdź SPAM)</b></p>
<br />
  <p>Sprawdź szczegóły złożonego zamówienia - kliknij <a href="{$aModule.links.details}" style="color: #a10100; text-decoration: underline;">(tutaj)</a></p>

  <br />
  
	<div class="clear"></div>
  <div class="buttonDomyslny" style="float: left; margin-left: 350px; margin-top: 5px;">
    <div class="buttonDomyslnyLewo"></div>
    <div class="buttonDomyslnySrodek"><a href="/" style="font-size: 13px; padding-top: 8px;">Kontynuuj przeglądanie księgarni</span></a></div>
    <div class="buttonDomyslnyPrawo"></div>
  </div>
	
{$aModule.script.js}

</div>