<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 11px; background: url({$sBaseHref}/images/gfx/mail_top_tlo.gif) repeat-x;">
	<div style="width: 100%">
		<div style="margin: 0 auto; width: 650px;">	
			<div style="height: 76px; ">
				{*<div style="background: transparent url({$sBaseHref}/images/gfx/logo_mail.gif) no-repeat; float: left; display: inline; width: 200px; height: 76px; margin: 10px 0 0 0;"></div>*}
				<img src="logo_mail.gif" width="200" height="76" />
			</div>
			<div style="height: 23px; text-align: right; flaot: right; color: #000; padding: 15px 0 0 0; font-weight: bold; font-size: 14px;">{$aModule.lang.email_l.order_no_login} {$aModule.order.order_number}</div>
			<div style="width: 650px;">	
				<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">{$aModule.lang.content_order1}</div>
				<table cellspacing="1" cellpadding="1" style="border: 1px solid #e4e4e4; font-size: 11px;">
					<tr>
						<th style="width: 50%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_name}</th>
						<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.email_shipment_time}</th>
						<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_quantity}</th>
						<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.list_price_brutto}</th>
						<th style="width: 10%; color: #c30500; border: 1px solid #e4e4e4; background: #dfe2e4; text-align: center; padding: 0 9px;">{$aModule.lang.total_products_brutto}</th>
					</tr>
					{foreach from=$aModule.items key=iId item=aItems name=types}
						<tr>
							<td style="border: 1px solid #e4e4e4; padding: 5px 10px 5px 10px;">
								<strong style="color: #c30500; font-size: 14px;">{$aItems.name}</strong><br/>
								{if !empty ($aItems.isbn)}
									{$aModule.lang.isbn} <strong>{$aItems.isbn}</strong>,
								{/if}
								{if !empty ($aItems.publication_year)}
									{$aModule.lang.publication_year} <strong>{$aItems.publication_year}</strong>,
								{/if}
								{if !empty ($aItems.edition)}
									{$aModule.lang.edition} <strong>{$aItems.edition}</strong>
								{/if}<br />
								{if !empty ($aItems.publisher)}
									{$aModule.lang.publisher} <strong>{$aItems.publisher}</strong><br />
								{/if}
								{if !empty ($aItems.authors)}
									{$aModule.lang.author} <strong>{$aItems.authors}</strong><br /> 
								{/if}
							</td>
							<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{if $aItems.preview == '1'}{$aModule.lang.shipment_preview}<br/><strong>{$aItems.shipment_date}r.</strong>{else}{if !empty ($aItems.shipment_time)} {$aItems.shipment_time} {/if}{/if}</td>
							<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{$aItems.quantity}</td>
							<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{if $aItems.promo_price_brutto > 0}{$aItems.promo_price_brutto|format_number:2:',':' '}{else}{$aItems.price_brutto|format_number:2:',':' '}{/if} {$aLang.common.currency}</td>
							<td style="border: 1px solid #e4e4e4; text-align: center; padding: 10px 0;">{$aItems.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</td>
						</tr>
					{/foreach}
				</table>

				<div style="float: right; text-align: right; line-height: 150%; margin-top: 24px;">
					<div>{$aModule.lang.total_cost_brutto} <strong>{$aModule.order.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
					<div>{$aModule.lang.total_transport_cost} <strong>{$aModule.order.transport_cost|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
					<div>{$aModule.lang.total_cost} <strong>{$aModule.order.total_value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
					<div style="margin-top: 5px;"><strong>{$aModule.lang.to_pay} <span style="font-size: 14px; color: #c30500; font-weight: bold;">{$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency}</span></strong></div>
				</div>
				
				{if !empty($aModule.addresses[0])}
				<div style="clear: both;"></div>
				<div class="step2Option step2OptionSummary">
					<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">Dane do wystawienia faktury VAT:</div>
					<div style="margin: 0 0 40px 31px; line-height: 150%;">
						{if $aModule.addresses[0].is_company == '1'}
							{$aModule.lang.company} <strong>{$aModule.addresses[0].company}</strong><br>
							{$aModule.lang.nip} <strong>{$aModule.addresses[0].nip}</strong><br>
							{$aModule.lang.address}: <strong>{$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</strong><br>
							{$aModule.lang.postal} <strong>{$aModule.addresses[0].postal} {$aModule.addresses[0].city}</strong><br>
						{else}
							{$aModule.lang.namesurname}: <strong>{$aModule.addresses[0].name} {$aModule.addresses[0].surname}</strong><br>
							{*{$aModule.lang.nip} <strong>---</strong><br>*}
							{$aModule.lang.address}: <strong>{$aModule.addresses[0].street} {$aModule.addresses[0].number}{if !empty($aModule.addresses[0].number2)}/{$aModule.addresses[0].number2}{/if}</strong><br>
							{$aModule.lang.postal} <strong>{$aModule.addresses[0].postal} {$aModule.addresses[0].city}</strong><br>
						{/if}
					</div>	
				</div>
				{/if}
				
				{if !empty($aModule.addresses[1])}
				<div style="clear: both;"></div>
				<div class="step2Option step2OptionSummary">
					<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">Adres na który wyślemy przesyłkę:</div>
					<div style="margin: 0 0 40px 31px; line-height: 150%;">
						{if $aModule.addresses[1].is_company == '1'}
							{$aModule.lang.company} <strong>{$aModule.addresses[1].company}</strong><br>
              Odbiorca przesyłki: <strong>{$aModule.addresses[0].name} {$aModule.addresses[0].surname}</strong><br>
							{*{$aModule.lang.nip} <strong>{$aModule.addresses[1].nip}</strong><br>*}
							{$aModule.lang.address}: <strong>{$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</strong><br>
							{$aModule.lang.postal} <strong>{$aModule.addresses[1].postal} {$aModule.addresses[1].city}</strong><br>
						{else}
							{$aModule.lang.namesurname}: <strong>{$aModule.addresses[1].name} {$aModule.addresses[1].surname}</strong><br>
							{*{$aModule.lang.nip} <strong>---</strong><br>*}
							{$aModule.lang.address}: <strong>{$aModule.addresses[1].street} {$aModule.addresses[1].number}{if !empty($aModule.addresses[1].number2)}/{$aModule.addresses[1].number2}{/if}</strong><br>
							{$aModule.lang.postal} <strong>{$aModule.addresses[1].postal} {$aModule.addresses[1].city}</strong><br>
						{/if}
					</div>	
				</div>
				{/if}
				
				{if !empty($aModule.addresses[2])}
				<div style="clear: both;"></div>
				<div class="step2Option step2OptionSummary">
					<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">Dane do wystawienia drugiej faktury VAT:</div>
					<div style="margin: 0 0 40px 31px; line-height: 150%;">
						{if $aModule.addresses[2].is_company == '1'}
							{$aModule.lang.company}: <strong>{$aModule.addresses[2].company}</strong><br>
							{$aModule.lang.nip} <strong>{$aModule.addresses[2].nip}</strong><br>
							{$aModule.lang.address}: <strong>{$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</strong><br>
							{$aModule.lang.postal} <strong>{$aModule.addresses[2].postal} {$aModule.addresses[2].city}</strong><br>
						{else}
							{$aModule.lang.namesurname}: <strong>{$aModule.addresses[2].name} {$aModule.addresses[2].surname}</strong><br>
							{*{$aModule.lang.nip} <strong>---</strong><br>*}
							{$aModule.lang.address}: <strong>{$aModule.addresses[2].street} {$aModule.addresses[2].number}{if !empty($aModule.addresses[2].number2)}/{$aModule.addresses[2].number2}{/if}</strong><br>
							{$aModule.lang.postal} <strong>{$aModule.addresses[2].postal} {$aModule.addresses[2].city}</strong><br>
						{/if}
					</div>
				</div>
				{/if}

				{if !empty($aModule.order.invoice)}
					<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">{$aModule.lang.invoice_data_header}</div>
					<div style="margin: 0 0 40px 31px; line-height: 150%;">
						{$aModule.lang.inv_name_surname} <strong>{$aModule.order.invoice_company}</strong><br/>
						{if !empty($aModule.order.invoice_nip)}
							{$aModule.lang.inv_nip} <strong>{$aModule.order.invoice_nip}</strong><br/>
						{/if}
						{$aModule.lang.inv_street} <strong>{$aModule.order.invoice_street} {$aModule.order.invoice_number} {if $aModule.order.invoice_number2 ne ''}/ {$aModule.order.invoice_number2}{/if}</strong><br/>
						{$aModule.lang.inv_postal} <strong>{$aModule.order.invoice_postal}</strong><br/>
						{$aModule.lang.inv_city} <strong>{$aModule.order.invoice_city}</strong><br/>
						
					</div>
				{/if}
			</div>
		</div>
	</div>	
</body>
</html>
