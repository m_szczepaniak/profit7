<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 3/4 (Dane kupującego)</h1>
</div>
<div class="clear"></div>
<div id="step2">
  {$aModule.login_box}
  {$aModule.form.validator.header}
	{$aModule.form.validator.err_prefix}
	{$aModule.form.validator.err_postfix}
	{$aModule.form.validator.validator}
	{$aModule.form.validator.ajaxval}
  
  {$aModule.form.main_data.registration.input}
  <div class="registrationForm" style="display: none; width: auto;">
    <div class="formularz">	
      <div class="noRegstrationHeader">
        <div class="clear"></div>
        <strong>Zakupy bez rejestracji:</strong>
        <div class="clear"></div>
      </div>
      
      {** PODSTAWOWE DANE **}
      <div style="margin: 2px; padding: 10px; border: 1px solid #e7e7e7; float: left;">
        <div class="clear"></div>
        <div class="zamowienie_bez_rejestracji_title">Podstawowe dane:</div>
        <br />
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.main_data.phone.label}</div>
          <div class="fInput">{$aModule.form.main_data.phone.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.main_data.email.label}</div>
          <div class="fInput">{$aModule.form.main_data.email.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.main_data.confirm_email.label}</div>
          <div class="fInput">{$aModule.form.main_data.confirm_email.input}</div>
          <div class="clear"></div>
        </div>
        <div id="group_only_registration_data">
          <div class="fRow">
            <div class="fLabel">{$aModule.form.main_data.passwd.label}</div>
            <div class="fInput">{$aModule.form.main_data.passwd.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.main_data.passwd2.label}</div>
            <div class="fInput">{$aModule.form.main_data.passwd2.input}</div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
      
      {** DANE DO FAKTURY **}
      <div style="margin: 2px; padding: 10px; border: 1px solid #e7e7e7; float: left;">
        <div class="clear"></div>
        <div class="zamowienie_bez_rejestracji_title">Dane do faktury:</div>
        <div class="clear"></div>

        <div class="fRow fRowType">
          <div class="fLabel">Wybierz typ:</div>
          <div class="fInput">{$aModule.form.invoice.is_company.input}</div>
        </div>
        <div class="clear"></div>

        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.company.label}</div>
          <div class="fInput">{$aModule.form.invoice.company.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.name.label}</div>
          <div class="fInput">{$aModule.form.invoice.name.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.surname.label}</div>
          <div class="fInput">{$aModule.form.invoice.surname.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.nip.label}</div>
          <div class="fInput">{$aModule.form.invoice.nip.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.street.label}</div>
          <div class="fInput">{$aModule.form.invoice.street.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow fRowMini">
          <div class="fLabel">{$aModule.form.invoice.number.label}</div>
          <div class="fInput">{$aModule.form.invoice.number.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow fRowMini" style="width: 170px;">
          <div class="fLabel" style="width: 60px;">{$aModule.form.invoice.number2.label}</div>
          <div class="fInput">{$aModule.form.invoice.number2.input}</div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.postal.label}</div>
          <div class="fInput">{$aModule.form.invoice.postal.input}</div>
          <div class="clear"></div>
        </div>
        <div class="fRow">
          <div class="fLabel">{$aModule.form.invoice.city.label}</div>
          <div class="fInput">{$aModule.form.invoice.city.input}</div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="clear"></div>
      
      {** ADRES DOSTAWY **}
      {if !empty($aModule.form.delivery)}
        <div style="margin: 2px; padding: 10px; border: 1px solid #e7e7e7; float: left;">
          <div class="clear"></div>

          <div class="fRow fRowType">
            <div class="fLabel">{$aModule.form.delivery.delivery_data_from_invoice.label}</div>
            <div class="fInput">{$aModule.form.delivery.delivery_data_from_invoice.input}</div>
          </div>
          <div class="clear"></div>

          <div id="group_delivery_data">

            <div id="do_copy_data_from_invoice" style="margin-left: 130px; background: url('/images/gfx/button-skopiuj-dane-z-faktury.png'); width: 183px; height: 26px; cursor: pointer;"></div>

            <div class="fRow fRowType">
              <div class="fLabel">Wybierz typ:</div>
              <div class="fInput">{$aModule.form.delivery.is_company.input}</div>
            </div>
            <div class="clear"></div>

            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.company.label}</div>
              <div class="fInput">{$aModule.form.delivery.company.input}</div>
              <div class="clear"></div>
            </div>
            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.name.label}</div>
              <div class="fInput">{$aModule.form.delivery.name.input}</div>
              <div class="clear"></div>
            </div>
            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.surname.label}</div>
              <div class="fInput">{$aModule.form.delivery.surname.input}</div>
              <div class="clear"></div>
            </div>
              {*
            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.nip.label}</div>
              <div class="fInput">{$aModule.form.delivery.nip.input}</div>
              <div class="clear"></div>
            </div>
            *}
            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.street.label}</div>
              <div class="fInput">{$aModule.form.delivery.street.input}</div>
              <div class="clear"></div>
            </div>
            <div class="fRow fRowMini">
              <div class="fLabel">{$aModule.form.delivery.number.label}</div>
              <div class="fInput">{$aModule.form.delivery.number.input}</div>
              <div class="clear"></div>
            </div>
            <div class="fRow fRowMini" style="width: 170px;">
              <div class="fLabel" style="width: 60px;">{$aModule.form.delivery.number2.label}</div>
              <div class="fInput">{$aModule.form.delivery.number2.input}</div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.postal.label}</div>
              <div class="fInput">{$aModule.form.delivery.postal.input}</div>
              <div class="clear"></div>
            </div>
            <div class="fRow">
              <div class="fLabel">{$aModule.form.delivery.city.label}</div>
              <div class="fInput">{$aModule.form.delivery.city.input}</div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      {/if}
      
      {** DANE DO DRUGIEJ FAKTURY **}
      {if !empty($aModule.form.second_invoice)}
        <div style="margin: 2px; padding: 10px; border: 1px solid #e7e7e7; float: left;">
          <div class="zamowienie_bez_rejestracji_title">Dane do drugiej faktury:</div>
          <div class="clear"></div>

          <div class="fRow fRowType">
            <div class="fLabel">Wybierz typ:</div>
            <div class="fInput">{$aModule.form.second_invoice.is_company.input}</div>
          </div>
          <div class="clear"></div>

          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.company.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.company.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.name.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.name.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.surname.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.surname.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.nip.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.nip.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.street.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.street.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow fRowMini">
            <div class="fLabel">{$aModule.form.second_invoice.number.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.number.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow fRowMini" style="width: 170px;">
            <div class="fLabel" style="width: 60px;">{$aModule.form.second_invoice.number2.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.number2.input}</div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.postal.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.postal.input}</div>
            <div class="clear"></div>
          </div>
          <div class="fRow">
            <div class="fLabel">{$aModule.form.second_invoice.city.label}</div>
            <div class="fInput">{$aModule.form.second_invoice.city.input}</div>
            <div class="clear"></div>
          </div>
        </div>
     {/if}
     
     {*if !empty($aModule.form.recipient)*}
        {if  $aModule.form.is_poczta_polska == '1'}
          <input type="hidden" id="is_poczta_polska" name="is_poczta_polska" value="1" />
        {/if}
        {if  $aModule.form.is_ruch == '1'}
          <input type="hidden" id="is_ruch" name="is_ruch" value="1" />
        {/if}
        {if  $aModule.form.is_orlen == '1'}
          <input type="hidden" id="is_orlen" name="is_orlen" value="1" />
        {/if}
        <div class="clear"></div>
        <div id="person_recipient" style="margin: 2px; padding: 10px; border: 1px solid #e7e7e7; float: left;">
          <div class="zamowienie_bez_rejestracji_title">Osoba odbierająca przesyłkę:</div>
          <div class="clear"></div>

          <div class="fRow">
            <div class="fLabel">{$aModule.form.recipient.name.label}</div>
            <div class="fInput">{$aModule.form.recipient.name.input}</div>
            <div class="clear"></div>
          </div>

          <div class="fRow">
            <div class="fLabel">{$aModule.form.recipient.surname.label}</div>
            <div class="fInput">{$aModule.form.recipient.surname.input}</div>
            <div class="clear"></div>
          </div>

          <div class="fRow">
            <div class="fLabel">{$aModule.form.recipient.phone.label}</div>
            <div class="fInput">{$aModule.form.recipient.phone.input}</div>
            <div class="clear"></div>
          </div>
        </div>
      {*/if*}
          
        <div class="clear"></div>


        <div class="corp_grp_input">
          <div class="fRow">
            <div class="fRow"><input type="checkbox" id="select_all" /><label for="select_all">Akceptuję wszystkie</label></div>
          </div>
          <div class="clear"></div>
            <div class="left-group" id="agreements_checkbox">
              <div class="fRow">
                <div class="fLabel">&nbsp;</div>
                <div class="fInput">{$aModule.form.accept.reg.input} {$aModule.form.accept.reg.label}</div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
              <div class="fRow">
                <div class="fLabel">&nbsp;</div>
                <div class="fInput">{$aModule.form.ceneo.ceneo_agreement.input} {$aModule.form.ceneo.ceneo_agreement.label}</div>
                <div class="fInfo">{$aModule.lang.ceneo_agreement_info}</div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
              <div class="fRow">
                <div class="fLabel">&nbsp;</div>
                <div class="fInput">{$aModule.form.accept.promotions_agreement.input} {$aModule.form.accept.promotions_agreement.label}</div>
                <div class="fInfo">{$aModule.lang.promotions_agreement_info}</div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
              <div class="fRow">
                {*<div class="fLabel">&nbsp;</div>*}
                {*<div class="fInput">{$aModule.form.accept.priv.input} {$aModule.form.accept.priv.label}</div>*}
                <div class="clear"><input type="hidden" name="sentForm" value="1" /></div>
              </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="wymagane" style="margin-top: 10px;">
          <span>*</span> Pola obowiązkowe
        </div>
    </div>
    {** BUTTONY **}
    <div class="buttonLine buttonLineBig" style="margin-top: 20px;">
      <a href="/koszyk/step2.html" class="left" style="float: left;background: url('/images/gfx/button-wroc.gif') no-repeat;width: 85px;height: 33px;" title="Wróć"></a>

      <div class="buttonDomyslny">
        <div class="buttonDomyslnyLewo"></div>
        <div class="buttonDomyslnySrodek"><a onclick="$('#order_without_register').submit();">Zatwierdź i przejdź dalej <span style="font-size: 16px">&raquo;</span></a></div>
        <div class="buttonDomyslnyPrawo"></div>
      </div>
    </div>
  </div>
    

  {$aModule.form.validator.footer}
        
</div>
  
<script type="text/javascript">
  {literal}
    $(document).ready(function(){

        $("#select_all").on( "click", function() {
            if ($("#select_all").prop('checked') == true) {
                $("#agreements_checkbox input[type=\'checkbox\']").prop('checked', 'checked');
            } else {
                $("#agreements_checkbox input[type=\'checkbox\']").removeProp('checked');
            }
        });
      
      var ctlPressed = false; //Flag to check if pressed the CTL key
      var ctl = 17; //Key code for Ctl Key
      var c = 67; //Key code for "c" key

      jQuery(document).keydown(function(e)
      {
          if (e.keyCode === ctl) 
            ctlPressed = true;
      }).keyup(function(e)
      {
          if (e.keyCode === ctl) 
            ctlPressed = false;
      });
      
      function returnFalse(e) {        
              return false;
      }
      $("input#email,input#confirm_email").focus(function() {
        $(document).bind("contextmenu", returnFalse);
      });
      $('input#email,input#confirm_email').blur(function() {
        $(document).unbind("contextmenu", returnFalse);
      });
      
    
      jQuery("input#email,input#confirm_email").keydown(function(e)
      {
          if (ctlPressed && e.keyCode === c) {
            alert("Kopiowanie pól email, jest wyłączone.");
            return false;
          }
      });
    
      /** WALIDACJA **/
      $("#order_without_register input").blur(function() {
        performFieldAjaxValidation("order_without_register",this.name,$("#order_without_register :input").serialize());
      });
      $("#order_without_register input").click(function() {
        performFieldAjaxValidation("order_without_register",this.name,$("#order_without_register :input").serialize());
      });
      
			$("#order_without_register").submit(function() {
				performAjaxValidation("order_without_register", $("#order_without_register input").serialize());
			  if($("#order_without_register input:enabled").hasClass("validErr")){
			  	showSimplePopup('Uzupełnij poprawnie formularz!', false, 4000);
			 		return false;
			  } else 
			  	return true;
			});
      
      /** POKAZANIE PÓL ZAMÓWIENIA BEZ REJESTRACJI **/
      $("#do_order_without_registration").click ( function () {
        $("#group_only_registration_data").hide();
        $("input:hidden[name=registration]").val('2');
        $(".registrationForm").show();
        $(".noRegstrationHeader strong").html('Zakupy bez rejestracji');
        $('html, body').animate({
          scrollTop: $('.noRegstrationHeader').offset().top
        }, 500);
      });
      
      /** REJESTRACJA **/
      $("#do_register_form").click( function () {
        $("#group_only_registration_data").show();
        $("input:hidden[name=registration]").val('1');
        $(".registrationForm").show();
        $(".noRegstrationHeader strong").html('Zakupy z rejestracją');
        $('html, body').animate({
          scrollTop: $('.noRegstrationHeader').offset().top
        }, 500);
      });

      if ($("#registration").val() === '1') {
        $("#group_only_registration_data").show();
        $(".registrationForm").show();
        $(".noRegstrationHeader strong").html('Zakupy z rejestracją');
      }
      else if ($("#registration").val() === '2') {
        $("#group_only_registration_data").hide();
        $(".registrationForm").show();
        $(".noRegstrationHeader strong").html('Zakupy bez rejestracji');
      }
      
      /** KOPIOWANIE DANYCH Z FAKTURY DO DOSTAWY **/
      $("#do_copy_data_from_invoice").click( function () {
        var iWartosc = $("input[name='invoice[is_company]'][type='radio']:checked").val();
        $("input[name='delivery[is_company]'][type='radio'][value='"+iWartosc+"']")
                .click();
      
        $("input[name^='invoice['][type='text']").each( function () {
          var sPrefix = $(this).attr('name');
          var sPostfix = sPrefix.replace('invoice', '');
          $("input[name='delivery"+sPostfix+"']").val($(this).val());
        });
      });
      
      /** UKRYWANIE PÓL **/
      
      /**
       * funkcja decyduje czy grupa pól osoba odbierająca przesyłkę 
       *  ma być pokazana czy schowana
       *  Logika tego jest dość trudna ;)
       * 
       * @param {string} sGroup
       * @param {int} iIsCompany
       * @param {int} iDeliveryDataFrom
       * @returns {Boolean}
       */
      function hideShowRecipient(sGroup, iIsCompany, iDeliveryDataFrom) {
        
        if (sGroup === 'invoice' && iIsCompany === '1' && iDeliveryDataFrom === '1') {
          $("#person_recipient").show();
        } else if(sGroup === 'delivery' && iIsCompany === '1' && iDeliveryDataFrom === '2') {
          $("#person_recipient").show();
        } else {
          if ((sGroup === 'invoice' && iDeliveryDataFrom === '1')
              || (sGroup === 'delivery' && iDeliveryDataFrom === '2')
            ) {
                
            //$("#person_recipient").hide();
          }
        }
      }// end of hideShowRecipient() function
      
      
      $("input[name$='[is_company]']").change( function() {
        var sName = $(this).attr('name');
        var sPrefix = sName.replace("[is_company]", '');
        if ($(this).val() === '0') {
          /** OSOBA PRYWATNA **/
          $("input[name='"+sPrefix+"[company]']").parent().parent().hide();
          $("input[name='"+sPrefix+"[nip]']").parent().parent().hide();
          $("input[name='"+sPrefix+"[name]']").parent().parent().show();
          $("input[name='"+sPrefix+"[surname]']").parent().parent().show();
          hideShowRecipient(sPrefix, '0', $("input[name='delivery[delivery_data_from_invoice]']:checked").val());
        } else {
          hideShowRecipient(sPrefix, '1', $("input[name='delivery[delivery_data_from_invoice]']:checked").val());
          /** FIRMA LUB INSTYTUCJA **/
          $("input[name='"+sPrefix+"[company]']").parent().parent().show();
          $("input[name='"+sPrefix+"[nip]']").parent().parent().show();
          $("input[name='"+sPrefix+"[name]']").parent().parent().hide();
          $("input[name='"+sPrefix+"[surname]']").parent().parent().hide();
        }
      });
      /** INNE DANE DO DOSTAWY **/
      $("input[name='delivery[delivery_data_from_invoice]']").change( function () {
        if ($(this).val() === '1') {
          $("#group_delivery_data").hide();
          hideShowRecipient('invoice', $("input[name='invoice[is_company]']:checked").val(), '1');
          hideShowRecipient('delivery', $("input[name='delivery[is_company]']:checked").val(), '1');
        } else {
          hideShowRecipient('invoice', $("input[name='invoice[is_company]']:checked").val(), '2');
          hideShowRecipient('delivery', $("input[name='delivery[is_company]']:checked").val(), '2');
          $("#group_delivery_data").show();
        }
      });
      
      /** DOMYŚLNA KONFIGURACJA **/
      if ($("input[name='invoice[is_company]']:checked").val() === undefined) {
        $("input[name='invoice[is_company]'][value='0']").attr('checked', 'checked').trigger("change");
      }
      if ($("input[name='delivery[delivery_data_from_invoice]']:checked").val() === undefined) {
        $("input[name='delivery[delivery_data_from_invoice]'][value='1']").attr('checked', 'checked').trigger("change");
      }
      if ($("input[name='delivery[is_company]']:checked").val() === undefined) {
        $("input[name='delivery[is_company]'][value='0']").attr('checked', 'checked').trigger("change");
      }
      if ($("input[name='second_invoice[is_company]']").html() !== undefined && $("input[name='second_invoice[is_company]']:checked").val() === undefined) {
        $("input[name='second_invoice[is_company]'][value='0']").attr('checked', 'checked').trigger("change");
      }
      
      /** PODPOWIADANIE KODOW I MIAST **/
      if ($("input[name$='[postal]']").length > 0){
        $("input[name$='[postal]']").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
      }
      if ($("input[name$='[city]']").length > 0){
        $("input[name$='[city]']").each( function () {
          var sName = $(this).attr('name');
          var sPrefix = sName.replace('[city]', '');
          $(this).autocomplete({ 
            source: function( request, response ) {
            $.ajax({
                url: "/internals/GetPostalCities.php",
                dataType: "json",
                data: {
                  postal: $("input[name='"+sPrefix+"[postal]']").val(),
                  term: request.term
                },
                /*beforeSend: function ( xhr ) {
                  $(this).attr("disabled", "disabled");
                },*/
                success: function( data ) {
                  // $(this).removeAttr("disabled");
                  response( data );
                }
              });
            },
            minLength: 2
          });
        });
      }
      
      /** WYGENEROWANIE CHANGE NA INPUTACH TYPU RADIO FORMULARZ W CELU ODŚWIEŻENIA WIDOKU **/
      $("input:radio:checked").trigger('change');
      
      /** PRZEPISYWANIE ZAWARTOSCI DANYCH DO FAKTURY DO OSOBY DO ODBIORU **/
      // po nacisnieciu klawiatury w zmienianym polu, pole to nie jest już przepisywane automatycznie
      var nameChanged = 0;
      $('input[name="recipient[name]"]').on('keyup', function (){
        nameChanged = 1;
      });
      var surnameChanged = 0;
      $('input[name="recipient[surname]"]').on('keyup', function (){
        surnameChanged = 1;
      });
      
      var phoneChanged = 0;
      $('input[name="recipient[phone]"]').on('keyup', function (){
        phoneChanged = 1;
      });
      // przepisywanie automatyczne
      if ($('input[name="recipient[name]"]').val() === '') {
        $('input[name="invoice[name]"]').on('keyup', function (){
          if (nameChanged == 0) {
            $('input[name="recipient[name]"]').val($(this).val());
          }
        });
      }
      if ($('input[name="recipient[surname]"]').val() === '') {
        $('input[name="invoice[surname]"]').on('keyup', function (){
          if (surnameChanged == 0) {
            $('input[name="recipient[surname]"]').val($(this).val());
          }
        });
      }
      if ($('input[name="phone"]').val() === '') {
        $('input[name="phone"]').on('keyup', function (){
          if (phoneChanged == 0) {
            $('input[name="recipient[phone]"]').val($(this).val());
          }
        });
      }
    });
  {/literal}
</script>