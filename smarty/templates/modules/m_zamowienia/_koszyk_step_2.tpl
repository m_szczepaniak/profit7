<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 2/2 (Dane kupującego)</h1>
</div>
<div class="clear"></div>
<div id="step2">
	<div class="clear"></div>
	{$aModule.form.header}			
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.second_invoice}	
	{$aModule.form.payment_platnosci}
	
	<div class="koszykPodtytul2">
		<div id="koszyk_kod_rabatowy">
			<div class="name">{$aModule.lang.discount_code}:</div>
			<input class="input_rabat_koszyk text left" type="text" value="{$aModule.cart.discount_code.code}" maxlength="32" type="text" name="discount_code" />
			<div id="confirmDiscountCode"></div>
			<div class="hintBox" onmouseover="showhint('Pole na kod rabatowy.', this, event, '160px')"></div>	
		</div>
		<strong>{$aModule.lang.list_items}</strong>
	</div>
	<div class="clear"></div>
	<table cellspacing="0" cellpadding="0" class="koszykPods">
		<tr>
			<th class="pozycja Center">Lp.</th>
			<th class="nazwa Left"  colspan="2">{$aModule.lang.list_name}</th>
			<th class="cena Center" style="width: 90px;">{$aModule.lang.email_shipment_time}</th>
			<th class="cena Center" style="width: 90px;">{$aModule.lang.list_price_brutto}</th>
			<th class="ilosc Center" style="width: 50px;">{$aModule.lang.list_quantity}</th>
			<th class="wartosc Center" style="width: 100px;">{$aModule.lang.total_products_brutto}</th>
			<th class="faktura Center" style="width: 80px;">{$aModule.lang.second_invoice}</th>
		</tr>
		{assign var=pozycja value=0}
		{foreach from=$aModule.cart.products key=iId item=aItems name=types}
			{assign var=pozycja value=$pozycja+1}
			<tr>
				<td class="pozycja Center">{$pozycja}.</td>
				<td class="zdjecie"><a href="{$aItems.link}">{if !empty($aItems.image.src)}<img style="width:25px;" src="{$aItems.image.src}" alt="{$aItems.name}" />{else}<img style="width:25px;" src="/images/gfx/brak.png" alt="Brak okładki" />{/if}</a></td>
				<td class="nazwa">{if isset($aItems.link)}<a href="{$aItems.link}" target="_blank">{$aItems.name}</a>{else}{$aItems.name}{/if}
					{if !empty($aModule.cart.discount_code.books) && in_array($iId, $aModule.cart.discount_code.books) }<span style="color: #c30500;"> - Rabat {$aModule.cart.discount_code.code_discount}%</span>{/if}
				</td>
				<td class="cena Center"><strong>{if $aItems.shipment_date!=''}od:<br/>{$aItems.shipment_date_f}r. {else}{if $aItems.shipment_days > 0}{$aItems.shipment_days} - {$aItems.transport_days} {$aModule.lang.days} {else} {$aModule.lang.shipment_now} {/if}{/if} </strong></td>
				<td class="cena Center">{if $aItems.promo_price > 0}<span>{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</span><br/>{$aItems.promo_price|format_number:2:',':' '} {$aLang.common.currency}{else}{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}{/if}</td>
				<td class="ilosc Center">{$aItems.quantity}</td>
				<td class="wartosc Center">{if $aItems.total_without_promo ne $aItems.total_price_brutto}<span>{$aItems.total_without_promo|format_number:2:',':' '} {$aLang.common.currency}</span><br/> <strong>{$aItems.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{else}<strong>{$aItems.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{/if}</td>
				<td class="faktura Center">{$aModule.invoice2.items[$iId]}</td>
			</tr>
		{/foreach}	
	</table>
	
	<table cellspacing="0" cellpadding="0" class="koszykSuma" style="border-bottom: 0;border-top: 1px solid #eae9e9;margin-top: 1px;">	
		<tr>
			<td width="500" class="koszykSumaInfo">
				{if !empty($aModule.cart.discount_code) && empty($aModule.cart.discount_code.books) && $aModule.cart.discount_code.code_discount > 0.00}
					<table cellspacing="0" cellpadding="0">	
						<tr>
							<td valign="middle" align="center">
								<img src="/images/gfx/ikona_procent.gif" />
							</td>					
							<td width="100%">
								{$aModule.kod_rabatowy_info}
							</td>					
						</tr>
					</table>					
				{else}&nbsp;{/if}
			</td>
			<td align="right">
				<table cellspacing="0" cellpadding="0" class="right">	
			       <tr>
			           <td class="koszykSumyPrawo" valign="baseline">
						Cena katalogowa: <s>{$aModule.cart.total_without_promo|format_number:2:',':' '}</s>{$aLang.common.currency}
						<div class="clear"></div>
						Cena z rabatami: <span>
							<strong> 
								{if $aModule.cart.old.promo_price > 0.00}
									{$aModule.cart.old.promo_price|format_number:2:',':' '}
								{else}
									{$aModule.cart.total_price_brutto|format_number:2:',':' '}
								{/if}
								{$aLang.common.currency} 
							</strong>
						</span>
						<div class="clear"></div>
						{* $aModule.cart.old.promo_price != $aModule.cart.total_price_brutto && *}
						{if !empty($aModule.cart.discount_code) && empty($aModule.cart.discount_code.books) && $aModule.cart.discount_code.code_discount > 0.00}
							<strong>Cena z kodem rabatowym ({$aModule.cart.discount_code.code_discount}%): <span style="color: #cc0400">{$aModule.cart.total_price_brutto|format_number:2:',':' '}{$aLang.common.currency}</span></strong>
						{/if} 
			           </td>
			       </tr>	
			       {if ($aModule.cart.user_balance > 0)}
			       	<tr>
			           <td class="koszykSumyPrawo">
				          {$aModule.lang.user_balance_info}:
				          <strong style="color: #c30500;">{$aModule.cart.user_balance|format_number:2:',':' '}{$aLang.common.currency}</strong>
			           	</td>
			        </tr> 
			       {/if}    
				</table>				
			</td>
		</tr>
	</table>	
	
	<div class="clear" style="height: 10px;"></div>
	<div class="koszykPodtytul2"><strong>{$aModule.lang.payment}</strong></div>
	<div class="clear"></div>
	<table cellspacing="1" cellpadding="1" class="koszyk platnosci">
		<tr>
			<th class="dostawa" style="width: 256px;border: 0;">Sposób dostawy</th>
			<th class="platnosc Left" style="border: 0;">Metoda płatności</th>
		</tr>
		{foreach from=$aModule.payment_form item=transport key=t name=kreska}
			{*if $t != "paczkomaty_24_7"*}
			<tr>
				<td style="vertical-align: middle;{if !$smarty.foreach.kreska.last}border-bottom: 1px solid #eae9e9;{/if}" width="270">
					{if !empty($transport[0].transport_logo)}<img class="left" style="padding: 0 10px 0 0;" src="/{$transport[0].transport_logo}" alt="{$t}" />{/if}
					<span>
						<br />
						{if ($t) == 'odbior-osobisty'}{$transport[0].transport_name}<br />(na terenie Warszawy){else}
						{$transport[0].transport_name}
						{/if}
					</span>	
				</td>
				<td{if !$smarty.foreach.kreska.last} style="border-bottom: 1px solid #eae9e9;"{/if}>
					<ul class="payment_types" id="{$t}">
						{foreach from=$transport item=payment key=k}
							<li>
								<div class="left">
									<input type="radio" value="{$payment.id}" id="payment_{$payment.id}" name="payment_type" class="payment_type_radio left" {if isset($aModule.cart.payment.id) && $aModule.cart.payment.id eq $payment.id} checked="checked"{/if} />
									<label style="margin-left: 5px;padding-top: 3px;cursor: pointer;float: left;" for="payment_{$payment.id}">{$payment.name} (koszt dostawy - {if $payment.cost == 0}<span id="gratis">{$aModule.lang.transport_gratis}</span>{else}{$payment.cost}{$aLang.common.currency}{/if})</label>
								</div>
								{if !empty($payment.description)}<div class="hintBox" style="margin: 2px 0 0 8px;" onmouseover="showhint('{$payment.description}', this, event, '220px')"></div>{/if}
								<div class="clear"></div>
							</li>
						{/foreach}
					</ul>
				</td>
			</tr>
			{*/if*}
		{/foreach}
	</table>
	<div class="clear"></div>			
			
	<div id="choose_paczkomaty" style="display: none;">
		<div class="choose_paczkomaty_line">Wybierz paczkomat: {$aModule.paczkomaty}</div>
		<div class="choose_paczkomaty_line">W celu dokonania obsługi wysyłki zamówienia przy użyciu usługi <strong>Paczkomaty 24/7</strong>, Twój adres e-mail <strong>{$smarty.session.w_user.email}</strong> oraz numer telefonu komórkowego zostanie przekazany firmie spedycyjnej InPost. <strong><u>Sprawdź dokładnie swoje dane</u></strong>, ponieważ na Twój adres e-mail i telefon komórkowy zostanie wysłane hasło pozwalające na odbiór paczki w wybranym przez Ciebie paczkomacie.</div>
		<div class="choose_paczkomaty_line"><strong>Numer telefonu komórkowego:</strong>&nbsp;&nbsp;&nbsp;<input type="text" value="" name="phone_paczkomaty" id="phone_paczkomaty" /><span id="phone_paczkomaty_war">*</span></div>
		<div class="choose_paczkomaty_line">
			<input type="checkbox" value="1" name="regulations_paczkomaty" id="regulations_paczkomaty" />&nbsp;&nbsp;<label for="regulations_paczkomaty">Akceptuję <a href="http://www.paczkomaty.pl/dokumenty/Regulamin_swiadczenia_uslugi_Paczkomaty.pdf">regulamin</a> Paczkomaty 24/7</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#c30500">*</font> - pole wymagane
		</div>
	</div>
	
	<div class="koszykWysCena">
		
		<div class="koszykWysCenaTransport"  {if $aModule.cart.payment.cost <= 0.00}style="display: none;"{/if}>
			{$aModule.lang.total_transport_cost} <strong><span id="total_transport_cost">{if $aModule.cart.payment.cost>0} {$aModule.cart.payment.cost|format_number:2:',':' '}{else}{$aModule.cart.payment.cost|format_number:2:',':' '}{/if}</span> {$aLang.common.currency}</strong>
		</div>
		<div class="clear"></div>
		<strong>{$aModule.lang.to_pay} <b><span id="total_cost">{$aModule.cart.to_pay|format_number:2:',':' '}</span> {$aLang.common.currency}</b></strong> 
	</div>
	<div class="koszykWysInfo" style="display: none;">
		<span>{$aModule.shipment_info}</span>
		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>
	<div id="paymentBankSection"{if $aModule.payment_bank_show == '0'} style="display: none;"{/if}>
		<h2>Wybierz swój bank, z którego chcesz wykonać przelew elektroniczny:</h2>
		{if !empty($aModule.payment_bank)}
		<ul>
			{foreach from=$aModule.payment_bank item=bank key=t name=payment_bank_form}
				<li>{$bank.input}<img src="/images/gfx/platnoscipl/{$bank.code}.gif" alt="" />{$bank.label}</li>
			{/foreach}
		</ul>
		{/if}
	</div>
	<div class="clear"></div>

<div class="step2Option step2Option_1" style="width: 320px;">
		<div class="koszykPodtytul2"><strong>{$aModule.lang.invoice_address}:</strong></div>
		<div class="fRow">
			<div class="fLabel">{*{$aModule.form.invoice_address.label}*}Dane adresowe:</div>
			<div class="fInput">{$aModule.form.invoice_address.input}</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div id="invoice_address_section">
		{if !empty($aModule.invoice_address)}
			<div class="mojeAdresy">
				<div class="mojeAdresySrodek">
					{if $aModule.invoice_address.is_company == '1'}
						<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.invoice_address.company}</div>
						<div class="clear"></div>
						{if !empty($aModule.invoice_address.nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.invoice_address.nip}</div>
						<div class="clear"></div>{/if}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.invoice_address.street} {$aModule.invoice_address.number}{if !empty($aModule.invoice_address.number2)}/{$aModule.invoice_address.number2}{/if}</div>
						<div class="clear"></div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.invoice_address.postal} {$aModule.invoice_address.city}</div>
					{else}
						<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.invoice_address.name} {$aModule.invoice_address.surname}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.invoice_address.street} {$aModule.invoice_address.number}{if !empty($aModule.invoice_address.number2)}/{$aModule.invoice_address.number2}{/if}</div>
						<div class="clear"></div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.invoice_address.postal} {$aModule.invoice_address.city}</div>
					{/if}
				</div>
			</div>
		{/if}
		</div>
		<div id="invoice_address_section_loading" style="display: none;" class="loading">{$aLang.common.loading}</div>
	</div>
	<div class="step2Option step2Option_2" style="width: 300px;">
		<div class="koszykPodtytul2"><strong>{$aModule.lang.transport_address}:</strong></div>
		<div class="fRow">
			<div class="fLabel">{*{$aModule.form.transport_address.label}*}Dane adresowe:</div>
			<div class="fInput">{$aModule.form.transport_address.input}</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div id="transport_address_section">
		{if !empty($aModule.transport_address)}
			<div class="mojeAdresy">
				<div class="mojeAdresySrodek">
					{if $aModule.transport_address.is_company == '1'}
						<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.transport_address.company}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.transport_address.nip}</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.transport_address.street} {$aModule.transport_address.number}{if !empty($aModule.transport_address.number2)}/{$aModule.transport_address.number2}{/if}</div>
						<div class="clear"></div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.transport_address.postal} {$aModule.transport_address.city}</div>
					{else}
						<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.transport_address.name} {$aModule.transport_address.surname}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.transport_address.street} {$aModule.transport_address.number}{if !empty($aModule.transport_address.number2)}/{$aModule.transport_address.number2}{/if}</div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.transport_address.postal} {$aModule.transport_address.city}</div>
					{/if}
				</div>
			</div>
		{/if}
		</div>
		<div id="transport_address_section_loading" style="display: none;" class="loading">{$aLang.common.loading}</div>
	</div>
	
	<div class="step2Option step2Option_3" style="width: 300px; padding: 0 !important;">
		<div class="koszykPodtytul2"><strong>{$aModule.lang.second_invoice_title}:</strong></div>
		<div class="step2Option_3Content" {if $aModule.cart.second_invoice=='1'}style="display: none;"{/if}>{$aModule.lang.second_invoice_content}</div>
		<a href="javascript:void(0);" id="show_second_invoice" title="{$aModule.lang.show_second_invoice}" {if $aModule.cart.second_invoice=='1'}style="display: none;"{/if}></a>
		<div id="invoice2_group" {if $aModule.cart.second_invoice!='1'}style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{*{$aModule.form.invoice2_address.label}*}Dane adresowe:</div>
				<div class="fInput">{$aModule.form.invoice2_address.input}</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div id="invoice2_address_section"></div>
			<div id="invoice2_address_section_loading" style="display: none;" class="loading">{$aLang.common.loading}</div>
			<div id="invoice2_help" {if $aModule.cart.second_invoice!='1'}style="display: none;"{/if}>
				{$aModule.lang.invoice2_help}
			</div>
			<div id="hide_second_invoice"><span>{$aModule.lang.hide_second_invoice}</span></div>
		</div>
	</div>
	
	<div class="clear" style="height: 30px;"></div>
	
	<div class="editAddressesButton">
		<a href="javascript:void(0);" title="{$aModule.lang.edit_addresses}" id="editAddressesButton"></a>
		<span>{$aModule.lang.edit_addresses_content}</span>
		<div class="clear"></div>
	</div>


	<div class="koszykPodtytul2"><strong>{$aModule.lang.comments_orders}:</strong></div>
	<div class="clear"></div>
	
	<div class="fRowRemarks" >
		<div class="fLabel" style="width: 550px;"><strong>{$aModule.form.remarks.label}</strong> <span style="color: #c30500;font-size: 12px;">(pole uwagi nie służy do wpisywania adresów)</span></div>
		<div class="fInput">{$aModule.form.remarks.input}</div>
	</div>

	<div class="fRowRemarks fRowRemarks_2" style="width: 325px;padding: 0; float: left !important;" id="transport_info">
		<div class="fLabel" style="width: 325px;"><strong>{$aModule.form.transport_remarks.label}</strong><span style="color: #c30500;font-size: 12px;">(np. nie działa domofon)</span> (max. 150 zn.)</div>
		<div class="fInput">{$aModule.form.transport_remarks.input}</div>
	</div>
	
	<div class="clear" style="height: 30px;"></div>
	
	<input type="image" src="/images/gfx/button-skladam-zamowienie.gif" value="Składam zamówienie" name="send" alt="Składam zamówienie" tabindex="24" style="cursor: pointer; float: right;" />
	
	<div class="buttonLine buttonLineBig" style="margin: 27px 0 0;">
		<a href="/koszyk/" class="left" style="float: left;background: url('/images/gfx/button-zmien-zawartosc-koszyka.gif') no-repeat;width: 216px;height: 33px;" title="Zmień zawartość koszyka"></a>
		<div class="clear"></div>
	</div>

	{$aModule.form.footer}
</div>

<form action="{$aModule.links.edit_addresses}" id="editAddressesForm" method="post" name="editAddresses">
	<input type="hidden" name="ref" value="{$aModule.links.reflink}" />
	<input type="hidden" name="cart" value="1" />
</form>
	
{** JS ukrywanie uwag dla kuriera oraz pokazywanie dostępnych paczkomatow odbioru **}
{literal}
	<script type="text/javascript">
		var bPaczkomaty = false;
		var bOpek = false;
		if ($("#paczkomaty_24_7").size()) {
			bPaczkomaty = true;
		}
		if ($("#opek-przesylka-kurierska").size()) {
			bOpek = true;
		}
			

		if (bPaczkomaty == true) {
			$("input[name='payment_type']").click( function(){
					if($(this).parent().parent().parent().attr('id') == 'paczkomaty_24_7') {
						$("#choose_paczkomaty").show();
						$("#transport_info").hide();
						$(".step2Option_2").hide();
					} else {
						$("#choose_paczkomaty").hide();
						$("#transport_info").show();
						$(".step2Option_2").show();
					}
			});
		}
		
		if (bOpek == true) {
			$("input[name='payment_type']").click( function(){
					if($(this).parent().parent().parent().attr('id') == 'opek-przesylka-kurierska') {
						$("#choose_paczkomaty").show();
						$("#transport_info").hide();
						$(".step2Option_2").hide();
					} else {
						$("#choose_paczkomaty").hide();
						$("#transport_info").show();
						$(".step2Option_2").show();
					}
			});
		}
			
		$(document).ready(function() {
			$("input[name='payment_type']").each( function(){
				if ($(this).attr('checked') == true){
					$(this).click();
				}
			});
		});
	</script>
{/literal}