<div class="koszykTytul">
	<h1><strong>Przechowalnia</strong></h1>
</div>
<div class="clear"></div>
<div id="koszykForm" style="">
	<table cellspacing="1" cellpadding="1" class="koszyk" style="width: 976px;">
		<tr>
			<th class="pozycja" style="text-align: left;">{$aModule.lang.name}</th>
			<th class="wartosc" style="width: 85px;">{$aModule.lang.value}</th>
			<th class="schowek" style="width: 46px;">{$aModule.lang.move}</th>
			<th class="usun" style="width: 35px;">{$aModule.lang.delete}</th>
		</tr>
		{foreach from=$aModule.items item=item}
			<tr>
				<td class="pozycja"><a href="{$item.link}">{$item.name}</a></td>
				<td class="wartosc" style="text-align: center;">
					{if $item.shipment_time!=3}{if $item.promo_price > 0}<span>{$item.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</span><br/><strong>{$item.promo_price|format_number:2:',':' '} {$aLang.common.currency}{else}{$item.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{/if}{/if}
				</td>
				{if $item.prod_status ne '0' && $item.shipment_time ne '3'}
					<td class="schowek" style="text-align: center;"><a href="{$item.add_to_cart_link}"><img src="/images/gfx/ikona_koszyk.gif" alt="" /></a></td>
				{else}
					<td class="schowek" style="text-align: center;"><span class="moveToCartInactive"><img src="/images/gfx/ikona_koszyk_szara.gif" alt="" /></span></td>
				{/if}
				<td class="usun" style="text-align: center;"><a href="{$item.delete_link}"><img src="/images/gfx/ikona_usun.gif" alt="" /></a></td>
			</tr>
		{/foreach}
	</table>
</div>