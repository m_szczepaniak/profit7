<div class="productPhotos">
	{foreach from=$aImages item=im key=i}
		<div class="photoCnt">
			<div class="squares"><div class="square left"></div><div class="square right"></div></div>
			<div class="photo">
				{if isset($im.b_src)}
					<a href="javascript: void(0);" onclick="showPopup('popup.php', '{$im.b_src}', {$im.b_width}, {$im.b_height}, '{$im.js_description}' ); return false;" title="{$im.decoded_description}"><img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" /></a>
				{else}
					<img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" />
				{/if}
			</div>
			{if isset($im.short_description) && !empty($im.short_description)}
				<div class="photoDescription"">
					{$im.short_description}
				</div>
			{elseif isset($im.description) && !empty($im.description)}
				<div class="photoDescription">
					{$im.description}
				</div>
			{/if}
			<div class="squares"><div class="square left"></div><div class="square right"></div></div>
		</div>
	{/foreach}
</div>