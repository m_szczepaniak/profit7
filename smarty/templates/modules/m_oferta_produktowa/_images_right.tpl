<div class="photosRight">
	{foreach from=$aImages item=im key=i}
		<div class="photoCnt" style="width: {$iMaxWidth}px;">
			<div class="photo">
				{if isset($im.b_src)}
					<a href="javascript: void(0);" onclick="showPopup('popup.php', '{$im.b_src}', {$im.b_width}, {$im.b_height}, '{$im.js_description}' ); return false;" title="{$im.decoded_description}"><img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" /></a>
				{else}
					<img alt="{$im.decoded_description}" src="{$im.src}" width="{$im.width}" height="{$im.height}" />
				{/if}
			</div>
			<div class="clear"></div>		
			{if isset($im.short_description) && !empty($im.short_description)}
				<div class="description" style="width: {$im.width}px;">
					{$im.short_description}
				</div>
			{elseif isset($im.description) && !empty($im.description)}
				<div class="description" style="width: {$im.width}px;">
					{$im.description}
				</div>
			{/if}
		</div>
	{/foreach}
</div>