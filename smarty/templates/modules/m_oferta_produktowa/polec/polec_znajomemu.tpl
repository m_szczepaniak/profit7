<div class="powiadom_znajomego">
	<div class="powiadom_tresc">
	<img src="/images/gfx/pl/powiadom_mail_naglowek.jpg" alt="" />
	<div style="clear: both;"></div>
		{if $aModule.step eq '1'}
			<div class="info">{$aModule.lang.notify_friend}<span style="text-decoration: underline; color: #004883;">{$aModule.item_name}</span></div>
			<div class="clear"></div>
			{$aModule.form.JS}
			{$aModule.form.header}
			{$aModule.form.err_prefix}
			{$aModule.form.err_postfix}
			{$aModule.form.validator}
			{$aModule.form.fields.id}
					<div class="fRow" style="margin-top: 10px; padding-left: 7px;">
						<div class="fLabel">{$aModule.form.fields.s_mail.label}</div>
						<div class="fInput" style="text-align: left;">{$aModule.form.fields.s_mail.input}</div>
					</div>
					<div class="fRow" style="margin-top: 25px; padding-left: 7px;">
						<div class="fLabel">{$aModule.form.fields.s_name.label}</div>
						<div class="fInput" style="text-align: left;">{$aModule.form.fields.s_name.input}</div>
					</div>
					<div class="fRow" style="margin-top: 25px; padding-left: 7px;">
						<div class="fLabel">{$aModule.form.fields.r_mail.label}</div>
						<div class="fInput" style="text-align: left;">{$aModule.form.fields.r_mail.input}</div>
					</div>
					<div class="fRow" style="margin-top: 25px; padding-left: 7px;">
						<div class="fLabel" style="width: 177px;">{$aModule.form.fields.description.label}</div>
						<div class="fInput" style="text-align: left;">{$aModule.form.fields.description.input}</div>
					</div>
					<div class="fRow_" style="margin-top: 25px;">
						<div class="fLabel" style="float: left; width: 184px;">&nbsp;</div>
						<div class="fInput" style="text-align: left; float: left;"><input type="image" src="/images/gfx/{$sLang}/wyslij.png" /></div>
					</div>
					<div class="clear"></div>
			{$aModule.form.footer}
		{else}
			<div class="info">{$aModule.lang.send}</div>
		{/if}
	</div>
</div>