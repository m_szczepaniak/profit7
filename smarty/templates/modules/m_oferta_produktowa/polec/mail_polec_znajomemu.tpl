<div style="width: 100%; height: 100%; background-color: #ffffff; font-family: Tahoma; font-size: 12px; margin: 0; padding: 0; line-height: 130%;">
	<div style="width: 603px; border: 1px solid #cfdae2; background-color: #ffffff; margin: auto; padding: 10px;">
		<img src="{$aModule.logo_link}" alt="" />
		<span style="font-weight: bold;">{$aModule.s_name} (<a href="mailto:{$aModule.s_mail}" style="color: #004883;">{$aModule.s_mail}</a>)</span> przesyła ci następującą wiadomość:
		<div style="clear: both;"></div>
		{if $aModule.description ne ''}
			<div style="width: 580px; border: 1px solid #cfdae2; background-color: #f1f6f9; padding: 10px; margin: 10px auto;">
				{$aModule.description}
			</div>
		{/if}
		<div style="clear: both;"></div>
		<div style="color: #004883; font-size: 20px; margin-top: 10px; padding-bottom: 10px;">Polecana publikacja:</div>
		<div style="float: left; width: 75px;"><img src="{$aModule.item.image}" alt=""/></div>
		<div style="float: right; width: 520px;">
			<div style="font-size: 14px; color: #004883; width: 100%; font-weight: bold"><a href="{$aModule.item.link}" style="color: #004883; text-decoration: none;">{$aModule.item.name}</a></div>
			{if isset($aModule.item.issue)}<span style="color: #004883; font-size: 12px; font-weight: bold">{$aModule.lang.issue}{$aModule.item.issue}</span>{/if}
			<div style="height: 1px; overflow: hidden; background-color: #cfdae2; margin-top: 9px; width: 100%;></div>


<div style="clear: both;"></div>

			<div style="margin-top: 10px;">
				{foreach from=$aModule.item.authors key=rola item=authors}
					<div style="clear: both;">
						{if $rola eq 'autor' and $authors|@count gt 1}
							{$aLang.common.authors}:
						{else}
							{$rola}: 
						{/if}
						{foreach from=$authors item=author name=prod_authors}
							{$author.name}{if !$smarty.foreach.prod_authors.last}, {/if}
						{/foreach}
					</div>
				{/foreach}
			<div style="height: 10px;"></div>
			{if isset($aModule.item.publication_year)}{$aModule.lang.publication_year}<strong>{$aModule.item.publication_year}</strong><br />{/if}
			{if isset($aModule.item.pages) and $aModule.item.pages > 0}{$aModule.lang.pages}<strong>{$aModule.item.pages}</strong><br />{/if}
			{if isset($aModule.item.isbn)}{$aModule.lang.isbn}<strong>{$aModule.item.isbn}</strong><br />{/if}
			{if isset($aModule.item.series_name)}{$aModule.lang.series}<strong>{$aModule.item.series_name}</strong><br />{/if}
			{if isset($aModule.item.index)}{$aModule.lang.index}<strong>{$aModule.item.index}</strong>{/if}
			</div>
		</div>
		<div style="clear: both;"></div>
		{if $aModule.short_description ne ''}
			<div style="width: 595px;">
				{$aModule.short_description}
			</div>
		{/if}
		<div style="float: right;"><a href="{$aModule.item.link}" style="font-weight: bold; color: #004883;">Czytaj pełny opis publikacji</a></div>
		<div style="clear: both;"></div>
		<div style="width: 580px; border: 1px solid #cfdae2; background-color: #f1f6f9; padding: 10px; margin: 10px auto; font-size: 11px;">
			{$aModule.footer}
		</div>
	</div>
</div>