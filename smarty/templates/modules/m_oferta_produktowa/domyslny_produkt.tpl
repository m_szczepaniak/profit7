<div class="produktWlasciwy" itemscope itemtype="http://data-vocabulary.org/Product">
	<div class="produktWlasciwyZdjecie">
		{if !empty($aModule.item.images[0].big) || !empty($aModule.item.images[0].small)}
			{if !empty($aModule.item.images[0].big)}
				<a href="{$aModule.item.images[0].big}" rel="lytebox['photos']" target="_blank" title="{if !empty($aModule.item.images[0].description)}{$aModule.item.images[0].description}{else}{$aModule.item.plain_name}{/if}" id="photoToBigLink" itemprop="image"><img src="{$aModule.item.images[0].small}" alt="{if !empty($aModule.item.images[0].description)}{$aModule.item.images[0].description}{else}{$aModule.item.plain_name}{/if}" id="photoToBig"/></a>
			{else}
				<img src="{$aModule.item.images[0].small}" alt="{if !empty($aModule.item.images[0].description)}{$aModule.item.images[0].description}{else}{$aModule.item.plain_name}{/if}" itemprop="image" />
			{/if}
			{if $aModule.item.images|@count > 1}
				<div class="produktWlasciwyZdjecieThumbs">
					{foreach from=$aModule.item.images item=image}
						<a href="javascript:void(0);" onclick="javascript:changePhoto('{$image.big}', '{$image.small}');"><img src="{$image.thumb}" alt="" /></a>
					{/foreach}
				</div>
			{/if}
		{else}
			<img src="/images/gfx/brak2.png" alt="" />
		{/if}
		<div class="produktWlasciwyLogo">
			{if !empty($aModule.item.publisher.logo)}
			<div class="clear" style="height: 14px;"></div>
			<a href="{get_publisher_link string=$aModule.item.publisher.name}" title="{$aModule.item.publisher.name}"><img src="/images/publishers/{$aModule.item.publisher.logo}" alt="{$aModule.item.publisher.name}" /></a>
			{/if}
		</div>
		
		<div class="produktWlasciwyLubiacze">
			<div id="fb-root"></div>
			{literal}
			<script type="text/javascript">(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) {return;}
			  js = d.createElement(s); js.id = id;
			  js.async=true; js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			{/literal}
			<div class="fb-like" data-href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" data-send="false" data-layout="button_count" data-width="116" data-show-faces="true"></div>
      {*
			{literal}<script type="text/javascript" src="http://apis.google.com/js/plusone.js">{lang: 'pl'}</script>{/literal}			
			<div class="produktWlasciwyLubiaczeGoogle">
				<g:plusone size="small" href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"></g:plusone>
			</div>	
      *}
      <!-- Place this tag where you want the +1 button to render. -->
      <div class="produktWlasciwyLubiaczeGoogle">
        <div class="g-plusone" data-size="small" href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"></div>
      </div>
      {literal}
      <!-- Place this tag after the last +1 button tag. -->
      <script type="text/javascript">
        window.___gcfg = {lang: 'pl'};

        (function() {
          var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
          po.src = 'https://apis.google.com/js/plusone.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
      </script>
      {/literal}
      
		</div>
		
	</div>
	<div class="produktWlasciwyTytul">
		<div class="produktWlasciwyTytuly {if $aModule.item.type == '1'}ikonaAudiobook{else}ikonaDruk{/if}">
			<h1 itemprop="name">{$aModule.item.name}</h1>
			{*<div class="clear"></div>*}
			{if !empty($aModule.item.name2)}
				<div class="clear"></div>
				<div class="produktWlasciwyNazwa2">
					{$aModule.item.name2}
				</div>
			{/if}
		</div>
		{*tu byly kiedys lubiacze *}
	</div>
	<div class="produktWlasciwyTytulBorder"></div>
	{*&nbsp;*}
	{if $aModule.item.is_news=='1' || $aModule.item.is_bestseller=='1' || $aModule.item.is_previews == '1'}
		<div class="produktListaOpcja" style="width: 566px; float: left;">
			{if $aModule.item.is_bestseller=='1'}<div class="produktListaOpcjaBestseller"></div>{/if}
			{if $aModule.item.is_news=='1' && $aModule.item.is_bestseller=='1' || $aModule.item.is_previews=='1' && $aModule.item.is_bestseller=='1'}<div class="produktListaOpcjaSeparator"></div>{/if}
			{if $aModule.item.is_previews == '1'}<div class="produktListaOpcjaZapowiedz"></div>{/if}
			{if $aModule.item.is_news=='1'}<div class="produktListaOpcjaNews" itemprop="condition" content="new"></div>{/if}
		</div>
	{/if}		
	{*&nbsp;*}
	<div class="produktWlasciwyDane" itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
		<div class="produktWlasciwyOpisyPodstawowe">
			{if !empty($aModule.item.original_title)}
			<div class="produktWlasciwyTytulorg">
				{$aModule.lang.original_title}: <span>{$aModule.item.original_title}</span>
			</div>
			{/if}
			{if !empty($aModule.item.language)}
			<div class="produktWlasciwyJezyk">
				{$aModule.lang.language}: <span>{if !empty($aModule.item.language_flag)}<img src="images/languages/{$aModule.item.language_flag}" title="{$aModule.item.language}" alt="{$aModule.item.language}"/>{else}{$aModule.item.language}{/if}</span>
			</div>
			{/if}
			{if !empty($aModule.item.publication_year)}
			<div class="produktWlasciwyRok">
				{$aModule.lang.publication_year}: <span>{$aModule.item.publication_year}</span>
			</div>
			{/if}
			{if !empty($aModule.item.edition)}
			<div class="produktWlasciwyWydanie">
				{$aModule.lang.edition}: <span>{$aModule.item.edition}</span>{*{if $aModule.item.edition > 1} <a href="/poprzednie-wydania/id{$aModule.item.id},{$aModule.item.plain_name}.html">(pozostałe wydania)</a>{/if}*}
			</div>
			{/if}
			{if !empty($aModule.item.isbn)}
			<div class="produktWlasciwyIsbn">
				{$aModule.lang.isbn}<span itemprop="identifier" content="isbn:{$aModule.item.isbn}">{$aModule.item.isbn}</span>
			</div>
			{/if}
			<div class="produktWlasciwyWydawnictwo">
				{$aModule.lang.publisher}<span>
					<a href="{get_publisher_link string=$aModule.item.publisher.name}" itemprop="brand">{$aModule.item.publisher.name}</a>
				</span>
			</div>
			{if !empty($aModule.item.series)}
				<div class="produktListaSerie">
					{$aModule.lang.series}
						<span><a href="{get_series_link publisher=$aModule.item.publisher.name series=$aModule.item.series}">{$aModule.item.series}</a></span>
				</div>
			{/if}
			
			
			{if !empty($aModule.item.authors)}	
			<div class="clear" style="height: 10px;"></div>
			<div class="produktWlasciwyAutorzy">
				{foreach from=$aModule.item.authors key=rid item=aRole name=auth_roles}
					{if !empty($aRole)}
						{$rid}:<span>
						{foreach from=$aRole key=id item=aAuthor name=prod_series}
							<a href="{$aAuthor.link}">{$aAuthor.name}{if !$smarty.foreach.prod_series.last}, {/if}</a>
						{/foreach}
						</span>
						{if !$smarty.foreach.auth_roles.last}<br /> {/if}
					{/if}
				{/foreach}
			</div>
			{/if}
		</div>
		
		{if !empty($aModule.item.name2) || !empty($aModule.item.binding) || !empty($aModule.item.dimension) || !empty($aModule.item.pages) || !empty($aModule.item.original_language) || !empty($aModule.item.translator) || !empty($aModule.item.volume_nr) || !empty($aModule.item.volumes) || !empty($aModule.item.volume_name) || !empty($aModule.item.city) || !empty($aModule.item.legal_status_date)}
		<div class="produktWlasciwyOpisyDodatkowe">
			<strong>Dodatkowe informacje:</strong>
			<div class="clear" style="height: 6px;"></div>
			{if !empty($aModule.item.binding)}
			<div class="produktWlasciwyOkladka">
				{$aModule.lang.binding}: <span>{$aModule.item.binding}</span>
			</div>
			{/if}
			{if !empty($aModule.item.dimension)}
			<div class="produktWlasciwyWymiary">
				{$aModule.lang.dimension}: <span>{$aModule.item.dimension}</span>
			</div>
			{/if}
			{if !empty($aModule.item.pages)}
			<div class="produktWlasciwyStrony">
				{$aModule.lang.pages}: <span>{$aModule.item.pages}</span>
			</div>
			{/if}
			{if !empty($aModule.item.original_language)}
			<div class="produktWlasciwyJezykorg">
				{$aModule.lang.original_language}: <span>{if !empty($aModule.item.original_language_flag)}<img src="images/languages/{$aModule.item.original_language_flag}" alt="{$aModule.item.original_language}" title="{$aModule.item.original_language}"/>{else}{$aModule.item.original_language}{/if}</span>
			</div>
			{/if}
			{if !empty($aModule.item.translator)}
			<div class="produktWlasciwyStrony">
				{$aModule.lang.translator}: <span>{$aModule.item.translator}</span>
			</div>
			{/if}
			{if !empty($aModule.item.volume_nr)}
			<div class="produktWlasciwyTom">
				{$aModule.lang.volume_nr}: <span>{$aModule.item.volume_nr}{if !empty($aModule.item.volume_name)} ({$aModule.item.volume_name}){/if}{if !empty($aModule.item.volumes)} z {$aModule.item.volumes}{/if}</span>
			</div>
			{/if}
			{if !empty($aModule.item.city)}
			<div class="produktWlasciwyMiasto">
				{$aModule.lang.city}: <span>{$aModule.item.city}</span>
			</div>
			{/if}
			{if !empty($aModule.item.legal_status_date)}
			<div class="produktWlasciwyMiasto">
				{$aModule.lang.legal_status_date}: <span>{$aModule.item.legal_status_date}</span>
			</div>
			{/if}
			{if $aModule.ordersCount > 0}
				<div class="clear"></div>
				<div class="btn btn-info ordersCount" style="margin-top:20px;" onmouseover="showhint('<div class=\'ordersPopup\' style=\'margin-top:5px\'><h5><b>Wcześniejsze zamówienia</b></h5><br>{foreach from=$aModule.orders item=order key=tid name=orders_loop}<p>Z dnia {$order.invoice_date_pay} <a href=\'{$order.link|escape:"html"}\'>{$order.order_number
				}</a> - zamówiono {$order.quantity} szt , dostarczono {$order.quantity} szt</p>{/foreach}</div>', this, event, '400px')">Ten produkt był już zamawiany <b>{$aModule.ordersCount}</b> razy</div>
			{/if}
		</div>
		{/if}	
		{********************* pliki ! *****************************}
		<div class="itemDetailsFiles">
		{if isset($aModule.item.files)}
			<div class="itemDetailsFilesTop"></div>
			<div class="itemDetailsFilesMid">
			{$aModule.lang.download}
				<ul>
					{foreach from=$aModule.item.files key=k item=file}
						<li>{$file.name}: <a href="{$file.link}" target="_blank">{$file.filename|replace:"_":" "}</a></li>
					{/foreach}
				</ul>
			</div>
			<div class="itemDetailsFilesBottom"></div>
		{/if}
	</div>
	{********************* /pliki ! *****************************}
		<div class="clear"></div>
		
		{if $aModule.item.prod_status == '1'}
			<div class="clear"></div>
			<div class="produktWlasciwyCzas">
        {strip}
				{$aModule.lang.shipment}: 
            <span itemprop="availability" 
                  {if $aModule.item.shipment_time == '0' || $aModule.item.shipment_time == '1' || $aModule.item.shipment_time == '2'} content="in_stock" {else} content="instore_only" {/if}
                  >
                {$aModule.item.shipment}
            </span>
        {/strip}
			</div>
		{elseif $aModule.item.prod_status == '3'}
      <span itemprop="availability" content="preorder"></span>
			<div class="clear"></div>
			<div class="produktWlasciwyCzas">
				{$aModule.lang.shipment_preview}: <span>{$aModule.item.shipment_date}r.</span>
			</div>
    {else}
      <span itemprop="availability" content="out_of_stock"></span>
		{/if}
		<div class="clear"></div>
		<div>
			<div class="produktWlasciwyCeny"{if $aModule.item.promo_price == 0} style="padding-top: 7px;"{/if}>
				{if $aModule.item.price_brutto ne '0,00'}
			          {if $aModule.item.shipment_time!='3'}
						  <span style="display: none" itemprop="price">{$aModule.item.promo_price}</span>
						  <span style="display: none" itemprop="currency">PLN</span>
			            {if $aModule.item.promo_price > 0}
                    <strong>{$aModule.item.promo_text} <span>{$aModule.item.promo_price|format_number:2:',':' '}{$aModule.lang.currency}</span><span itemprop="currency" content="PLN"></span></strong><br />
			              {$aModule.lang.you_old_price} <span>{$aModule.item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span>
			            {else}
			              <strong style="padding-top: 3px;">{$aLang.common.cena_w_ksiegarni} <span style="padding-top: 3px;">{$aModule.item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span></strong>
			            {/if}
			          {/if}
				{/if}
			</div>
			<div class="produktWlasciwyPrzyciski">
				<a href="{$aModule.item.repository_link}" title="Dodaj do schowka" class="przyciskDuzySchowek">dodaj do schowka</a>
        
        {if (($aModule.item.prod_status == '1' || $aModule.item.prod_status == '3') && $aModule.item.price_brutto ne '0,00') && ($aModule.item.shipment_time != '3')}
          {strip}
					<a onclick="addCardShowPopup(
                              {$aModule.item.id}, 
                              '{$aModule.item.link}', 
                              '{$aModule.item.name|replace:"'":"&#039;"|replace:"\n":""|htmlspecialchars}', 
                              {if $aModule.item.type=='1'}'audio'{else}'print'{/if}, 
                              {if $aModule.item.images[0].thumb != ''}'{$aModule.item.images[0].thumb}'{else}'/images/gfx/brak.png'{/if});" href="javascript:void(0);" title="Dodaj do koszyka" class="przyciskDuzyKoszyk">dodaj do koszyka</a>
              {/strip}
            {if ($aModule.item.prod_status <> '1' && $aModule.item.prod_status <> '2')}
              <br />
              <a href="javascript:void(0);" title="Jeśli książka stanie się dostępna w naszej ofercie, zostaniesz o tym automatycznie poinformowany." class="przyciskZapiszDostepnosc" style="float: right; width: 120px;">poinformuj o dostępności</a>
              <br />
            {/if}
         {else}
          <div class="przyciskZapytaj">
          {if ($aModule.item.shipment_time == '3')}
            
          {elseif ($aModule.item.prod_status == '2')}
              <span>nakład wyczerpany</span><br />
          {else}
              <span>książka niedostępna</span><br />
          {/if}
            <div style="float: left">
            {if ($aModule.item.prod_status <> '1' && $aModule.item.prod_status <> '2')}
              <a href="javascript:void(0);" title="Jeśli książka stanie się dostępna w naszej ofercie, zostaniesz o tym automatycznie poinformowany." class="przyciskZapiszDostepnosc">poinformuj o dostępności</a>
              <br />
            {/if}
            <a href="javascript:void(0);" title="Zapytaj o produkt" class="przyciskZapytaj">zapytaj o książkę</a>
            </div>
         </div>
        {/if}


        
        
			</div>	
		</div>
		
		{*********** zapytaj o produkt ********************}
		{literal}
		<script type="text/javascript">
		  $(document).ready(function(){
		    
		    $("a.przyciskZapytaj").click(function () {
		      $(".produktWlasciwyFormularz").slideToggle("slow");
		    });
        
		    $("a.przyciskZapiszDostepnosc").click(function () {
		      $(".produktWlasciwyFormularzZapisuDostepnosci").slideToggle("slow");
		    });
		
		    $(".boxKoszykSrodek .wiecej").click(function () {
		        $(".boxKoszykSrodek .wiecej").fadeOut("slow");
		      });
		    
		  });
		</script>
		{/literal}	
    <div class="clear"></div>
		{if ($aModule.item.prod_status <> '1' && $aModule.item.prod_status <> '2')}
			<div class="produktWlasciwyFormularzZapisuDostepnosci" style="display: none; width: 566px;">
				<div style="margin-top: 30px;">
          <hr />
          <strong>Jeśli książka stanie się dostępna w naszej ofercie, zostaniesz o tym automatycznie poinformowany.</strong><br /><br />
					{literal}
					<script type="text/javascript">
						// <![CDATA[
						function validateNotifAvailableForm(oForm, sEmailErr) {
							oForm.p_mail.value = oForm.p_mail.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
							oForm.p_captcha.value = oForm.p_captcha.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
              if (!oForm.n_regulations.checked) {
								alert("Musisz zaakceptować zgodę.");
								return false;
							}
							if ((oForm.p_mail.value == '' || oForm.p_captcha.value == '')) {
								alert("Podaj adres email oraz kod z obrazka");
								return false;
							}
							if (oForm.p_mail.value != '' && !oForm.p_mail.value.match(/^[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6}$/i)) {
								alert(sEmailErr);
								return false;
							}
							return true; // formularz zablokowany
						}
						// ]]>
					</script>
					{/literal}
					<form method="post" action="" onsubmit="return validateNotifAvailableForm(this, '{$aModule.lang.email_err}');">
            <input type="hidden" name="do" value="add_notif_available" />
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.your_mail}</div>
							<input class="text" type="text" name="p_mail" value="{if isset($smarty.session.w_user.email)}{$smarty.session.w_user.email}{/if}" maxlength="64" class="p_mail" />
						<div class="clear"></div>
						<div class="nazwa" style="margin-top: 5px;">{$aModule.lang.captcha}:</div>
              <img src="/captcha.php" alt="" /><br />
							<input class="text" type="text" name="p_captcha" value="" maxlength="6" class="p_captcha" />
						<div class="clear"></div>
            <div style="margin-top: 5px;">
              <div style="float: left;"><input type="checkbox" name="n_regulations" id="nf_regulations" value="1" {if $aModule.action eq 'edit'} readonly="readonly"{/if} /></div>
              <div style="color: grey; padding-top: 0px; font-size: 11px;"> <label for="nf_regulations">{$aLang.common.regulations} <span class="required"></span></label></div>
              <div class="clear"></div>
            </div>
            <input type="submit" value="Zapisz" class="save_produktWlasciwyFormularzZapisuDostepnosci" />
					</form>
          <hr />
				</div>
			</div>
		{/if}
    
		{if ($aModule.item.prod_status == '0') || ($aModule.item.shipment_time == '3')}
			<div class="produktWlasciwyFormularz" style="display: none">
				<div style="margin-top: 30px;">
					{literal}
					<script type="text/javascript">
						// <![CDATA[
						function validateNotifForm(oForm, sNoDataErr, sEmailErr) {
							oForm.p_text.value = oForm.p_text.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
							oForm.p_mail.value = oForm.p_mail.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
							oForm.p_captcha.value = oForm.p_captcha.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
              if (!oForm.n_regulations.checked) {
								alert("Musisz zaakceptować zgodę.");
								return false;
							}
							if ((oForm.p_text.value == '' || oForm.p_mail.value == '' || oForm.p_captcha.value == '')) {
								alert(sNoDataErr);
								return false;
							}
							if (oForm.p_mail.value != '' && !oForm.p_mail.value.match(/^[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6}$/i)) {
								alert(sEmailErr);
								return false;
							}
							return true;
						}
						// ]]>
					</script>
					{/literal}
					<form method="post" action="" onsubmit="return validateNotifForm(this, '{$aModule.lang.no_data_err}', '{$aModule.lang.email_err}');">
            <input type="hidden" name="do" value="add_notif" />
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.title}:</div>
							<input type="text" class="text" size="50" readonly="readonly" name="p_title" value="{$aModule.item.name}" />
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.ask_content}:</div>
							<textarea class="text" name="p_text" rows="4" cols="30"></textarea>
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.your_mail}</div>
							<input class="text" type="text" name="p_mail" value="{if isset($smarty.session.w_user.email)}{$smarty.session.w_user.email}{/if}" maxlength="64" class="p_mail" />
						<div class="clear"></div>
						<div class="nazwa">{$aModule.lang.captcha}</div>
							<input class="text" type="text" name="p_captcha" value="" maxlength="6" class="p_captcha" /><img src="/captcha.php" alt="" />
						<div class="clear"></div>
            <div>
              <div style="float: left;"><input type="checkbox" name="n_regulations" id="nf_regulations" value="1" {if $aModule.action eq 'edit'} readonly="readonly"{/if} /></div>
              <div style="color: grey; padding-top: 0px; font-size: 11px;"> <label for="nf_regulations">{$aLang.common.regulations} <span class="required"></span></label></div>
              <div class="clear"></div>
            </div>
						<input type="image" src="/images/gfx/przycisk_wyslij_zapytanie.gif" style="margin: 10px 0 0 210px;" />
					</form>
				</div>
			</div>
		{/if}
    
	</div>
	{if !empty($aModule.item.free_delivery) && ($aModule.item.prod_status == '1' || $aModule.item.prod_status == '3')}
	<div class="produktWlasciwyPrzesylka">
		<div class="produktWlasciwyPrzesylkaNazwa">
			<div><strong>DARMOWA WYSYŁKA!</strong><br/>Książkę w tej cenie wyślemy do Ciebie za darmo za pomocą:</div>
		</div>
		<ul>
			{foreach from=$aModule.item.free_delivery key=i item=transport name=transport}
				<li style="background-image: url(/images/gfx/wysylka-{$transport.symbol}.png);"></li>
			{/foreach}
		</ul>
	</div>
	{/if}
    <div style="text-align: right; float:right; padding: 0; margin: 0 24px 0 0;">
			<table style="border-top: 1px solid #e6e6e6; border-bottom: 1px solid #e6e6e6; padding: 5px;">
        <tr>
          <td colspan="2" style="font-size: 12px; text-align: right; font-weight: bold;">Koszt dostawy:</td>
				</tr>
        
      {foreach from=$aModule.item.transport_options key=i item=transport_opt name=transport_options}
        <tr>
          <td>{$transport_opt.name}</td> 
          <td><b>od&nbsp;{$transport_opt.cost|format_number:2:',':' '}&nbsp;zł</b></td>
        </tr>
      {/foreach}
			</table>
		</div>
	{if !empty($aModule.item.categories)}
	<div class="produktWlasciwyKategorie" style="float: left;">
		<strong>Kategorie:</strong>
		<ul itemprop="category">
		{foreach from=$aModule.item.categories key=id item=aCat name=prod_categories}
			<li><a href="/{$aCat.symbol}">{$aCat.name}</a>{if !$smarty.foreach.prod_categories.last},{/if}</li>
		{/foreach}
		</ul>
	</div>
	<div class="clear" style="height: 15px;"></div>
	{/if}
	{if !empty($aModule.item.description)}
		<div class="produktWlasciwyOpis" itemprop="description">
			{if !empty($aModule.item.tags)}
				<div class="produktWlasciwyTagi">
					<div class="produktWlasciwyTagiGora"></div>
					<div class="produktWlasciwyTagiSrodek">
						<h2>Słowa kluczowe</h2>
						<div class="produktWlasciwyTagiTresc">
							{foreach from=$aModule.item.tags item=tag key=tid name=tags_loop}
							<a href="{$tag.link}">{$tag.tag}</a>
							{/foreach}
						</div>	
					</div>
					<div class="produktWlasciwyTagiDol"></div>
				</div>
			{/if}

			{$aModule.item.description}
		</div>
	{/if}
</div>