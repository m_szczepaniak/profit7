<div class="produktWlasciwy">
	<div class="produktWlasciwyZdjecie">
		{if !empty($aModule.item.images[0].small)}
			{if !empty($aModule.item.images[0].big)}
				<a href="{$aModule.item.images[0].big}" rel="lytebox['photos']" target="_blank" title="{if !empty($aModule.item.images[0].description)}{$aModule.item.images[0].description}{else}{$aModule.item.plain_name}{/if}" id="photoToBigLink"><img src="{$aModule.item.images[0].small}" alt="{if !empty($aModule.item.images[0].description)}{$aModule.item.images[0].description}{else}{$aModule.item.plain_name}{/if}" id="photoToBig"/></a>
			{else}
				<img src="{$aModule.item.images[0].small}" alt="{if !empty($aModule.item.images[0].description)}{$aModule.item.images[0].description}{else}{$aModule.item.plain_name}{/if}" />
			{/if}
			{if $aModule.item.images|@count > 1}
				<div class="produktWlasciwyZdjecieThumbs">
					{foreach from=$aModule.item.images item=image}
						<a href="javascript:void(0);" onclick="javascript:changePhoto('{$image.big}', '{$image.small}');"><img src="{$image.thumb}" alt="" /></a>
					{/foreach}
				</div>
			{/if}
		 {else}
		 	<img src="/images/gfx/brak2.png" alt="" />
		 {/if}
		<div class="produktWlasciwyLogo">
			{if !empty($aModule.item.publisher.logo)}
			<div class="clear" style="height: 14px;"></div>
			<a href="{get_publisher_link string=$aModule.item.publisher.name}" title="{$aModule.item.publisher.name}"><img src="/images/publishers/{$aModule.item.publisher.logo}" alt="{$aModule.item.publisher.name}" /></a>
			{/if}
		</div>
		
		<div class="produktWlasciwyLubiacze">
			<div id="fb-root"></div>
			{literal}
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) {return;}
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			{/literal}
			<div class="fb-like" data-href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" data-send="false" data-layout="button_count" data-width="116" data-show-faces="true"></div>


			{literal}<script type="text/javascript" src="https://apis.google.com/js/plusone.js">{lang: 'pl'}</script>{/literal}
			<div class="produktWlasciwyLubiaczeGoogle">
				<g:plusone size="small" href="https://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"></g:plusone>
			</div>					
		</div>
				
	</div>
	<div class="produktWlasciwyTytul">
		<div class="produktWlasciwyTytuly">
			<h1>{$aModule.item.name}</h1>
			<div class="clear"></div>
			{if !empty($aModule.item.name2)}
			<div class="clear"></div>
			<div class="produktWlasciwyNazwa2">
				{$aModule.item.name2}
			</div>
			{/if}
		</div>
		<div class="produktWlasciwyLinki">
			<a href="http://www.facebook.com/share.php?u={$aModule.base_url}{$aModule.item.link}" class="linkFacebook" title="Facebook" target="_blank"></a>
			<a href="http://www.twitter.com/home?status={$aModule.item.name|truncate:74:'...':true|replace:' ':'+'|strip_pl}+-+{$aModule.base_url}{$aModule.item.short_link}" class="linkTwitter" title="Twitter" target="_blank"></a>
			<a href="http://blip.pl/dashboard/?body={$aModule.item.name|truncate:130:'...':true} - {$aModule.base_url}{$aModule.item.short_link}" class="linkBlip" title="Blip" target="_blank"></a>
			<a href="http://nasza-klasa.pl/sledzik?shout={$aModule.item.name|truncate:120:'...':true} - {$aModule.base_url}{$aModule.item.short_link}" class="linkSledzik" title="Śledzik" target="_blank"></a>
			<a href="http://flaker.pl/add2flaker.php?url={$aModule.base_url}{$aModule.item.short_link}&title={$aModule.item.name|truncate:120:'...':true}" class="linkFlaker" title="Flaker" target="_blank"></a>
		</div>
	</div>
	&nbsp;	
	<div class="produktWlasciwyDane">
			
		{if !empty($aModule.item.publication_year)}
		<div class="produktWlasciwyRok">
			{$aModule.lang.publication_year}: <span>{$aModule.item.publication_year}</span>
		</div>
		{/if}

		{if !empty($aModule.item.edition)}
		<div class="produktWlasciwyWydanie">
			{$aModule.lang.edition}: <span>{$aModule.item.edition}</span>{*{if $aModule.item.edition > 1} <a href="/poprzednie-wydania/id{$aModule.item.id},{$aModule.item.plain_name}.html">(pozostałe wydania)</a>{/if}*}
		</div>
		{/if}

		{if !empty($aModule.item.publisher.name)}
			<div class="produktWlasciwyWydawnictwo">
				{$aModule.lang.publisher}<span>
					<a href="{get_publisher_link string=$aModule.item.publisher.name}" itemprop="brand">{$aModule.item.publisher.name}</a>
				</span>
			</div>
		{/if}

		{if !empty($aModule.item.series)}
			<div class="produktListaSerie">
				{$aModule.lang.series}
					<span><a href="{get_series_link publisher=$aModule.item.publisher.name series=$aModule.item.series}">{$aModule.item.series}</a></span>
			</div>
		{/if}

		{if !empty($aModule.item.authors)}	
		<div class="clear" style="height: 10px;"></div>
		<div class="produktWlasciwyAutorzy">
			{foreach from=$aModule.item.authors key=rid item=aRole name=auth_roles}
				{if !empty($aRole)}
					{$rid}:<span>
					{foreach from=$aRole key=id item=aAuthor name=prod_series}
						<a href="{$aAuthor.link}">{$aAuthor.name}{if !$smarty.foreach.prod_series.last}, {/if}</a>
					{/foreach}
					</span>
					{if !$smarty.foreach.auth_roles.last}<br /> {/if}
				{/if}
			{/foreach}
		</div>
		{/if}



		<div class="produktWlasciwySklad">
			{$aModule.lang.composition}:
			<ul class="in_packet">
				{foreach from=$aModule.item.packet key=k item=packet name=pack}
					<li><a href="#{$k+1}">{$packet.name}</a></li>
				{/foreach}
			</ul>
		</div>	
		
		<div class="clear"></div>
		
		{if $aModule.item.prod_status == '1'}
			<div class="clear"></div>	
			<div class="produktWlasciwyCzas">
				{$aModule.lang.shipment}: <span>{$aModule.item.shipment}</span>
			</div>
		{elseif $aModule.item.prod_status == '3'}
			<div class="clear"></div>
			<div class="produktWlasciwyCzas">
				{$aModule.lang.shipment_preview}: <span>{$aModule.item.shipment_date}r.</span>
			</div>
		{/if}
		<div class="clear"></div>	
		<div>
			<div class="produktWlasciwyCeny" {if $aModule.item.promo_price == 0} style="padding-top: 7px;"{/if}>
				{if $aModule.item.price_brutto ne '0,00'}
					{if $aModule.item.promo_price > 0}
						{$aModule.lang.you_old_price} <span>{$aModule.item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span><br />
						<strong>{$aModule.item.promo_text} <span>{$aModule.item.promo_price|format_number:2:',':' '}{$aModule.lang.currency}</span></strong>
					{else}
						<strong style="padding-top: 3px;">{$aLang.common.cena_w_ksiegarni} <span style="padding-top: 3px;">{$aModule.item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span></strong>	
					{/if}
				{/if}
			</div>
			<div class="produktWlasciwyPrzyciski">
				<a href="{$aModule.item.repository_link}" title="Dodaj do schowka" class="przyciskDuzySchowek">dodaj do schowka</a>
				{if ($aModule.item.shipment_time == '3')}
					<div class="przyciskZapytaj">				
						<a href="javascript:void(0);" title="Zapytaj o produkt" class="przyciskZapytaj">zapytaj o książkę</a>
					</div>
				{elseif (($aModule.item.prod_status == '1' || $aModule.item.prod_status == '3') && $aModule.item.price_brutto ne '0,00')}
          {strip}
					<a href="javascript:void(0);" 
             onclick="addCardShowPopup(
                              {$aModule.item.id}, 
                              '{$aModule.item.link}', 
                              '{$aModule.item.name|replace:"'":"&#039;"|replace:"\n":""|htmlspecialchars}',
                              {if $aModule.item.type=='1'}'audio'{else}'print'{/if}, 
                              {if $aModule.item.image[0].thumb != ''}'{$aModule.item.image[0].thumb}'{else}'/images/gfx/brak.png'{/if});" 
             title="Dodaj do koszyka" 
             class="przyciskDuzyKoszyk">dodaj do koszyka</a>
              {/strip}
				{elseif ($aModule.item.prod_status == '2')}
					<div class="przyciskZapytaj">			
						<span>nakład wyczerpany</span><br />
						<a href="javascript:void(0);" title="Zapytaj o produkt" class="przyciskZapytaj">zapytaj o książkę</a>
					</div>			
				{else}
					<div class="przyciskZapytaj">				
						<span>książka niedostępna</span><br />
						<a href="javascript:void(0);" title="Zapytaj o produkt" class="przyciskZapytaj">zapytaj o książkę</a>
					</div>
				{/if}		
			</div>
		</div>
		
		{*********** zapytaj o produkt ********************}
		{literal}
		<script>
		  $(document).ready(function(){
		    
		    $("a.przyciskZapytaj").click(function () {
		      $(".produktWlasciwyFormularz").slideToggle("slow");
		    });
		
		    $(".boxKoszykSrodek .wiecej").click(function () {
		        $(".boxKoszykSrodek .wiecej").fadeOut("slow");
		      });
		    
		  });
		</script>
		{/literal}		
		{if ($aModule.item.prod_status == '0') || ($aModule.item.shipment_time == '3')}
			<div class="produktWlasciwyFormularz" style="display: none">
				<div style="margin-top: 30px;">
					{literal}
					<script type="text/javascript">
						// <![CDATA[
						function validateNotifForm(oForm, sNoDataErr, sEmailErr) {
							oForm.p_text.value = oForm.p_text.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
							oForm.p_mail.value = oForm.p_mail.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
							oForm.p_captcha.value = oForm.p_captcha.value.replace(/^\s+/g, '').replace(/\s+$/g, '');
							if ((oForm.p_text.value == '' || oForm.p_mail.value == '' || oForm.p_captcha.value == '')) {
								alert(sNoDataErr);
								return false;
							}
							if (oForm.p_mail.value != '' && !oForm.p_mail.value.match(/^[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6}$/i)) {
								alert(sEmailErr);
								return false;
							}
							return true;
						}
						// ]]>
					</script>
					{/literal}
					<form method="post" action="" onsubmit="return validateNotifForm(this, '{$aModule.lang.no_data_err}', '{$aModule.lang.email_err}');">
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.title}:</div>
							<input type="text" class="text" size="50" readonly="readonly" name="p_title" value="{$aModule.item.name}" />
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.ask_content}:</div>
							<textarea class="text" name="p_text" rows="4" cols="30"></textarea>
						<div class="clear"></div>
							<div class="nazwa">{$aModule.lang.your_mail}</div>
							<input class="text" type="text" name="p_mail" value="{if isset($smarty.session.w_user.email)}{$smarty.session.w_user.email}{/if}" maxlength="64" class="p_mail" />
						<div class="clear"></div>
						<div class="nazwa">{$aModule.lang.captcha}</div>
							<input class="text" type="text" name="p_captcha" value="" maxlength="6" class="p_captcha" /><img src="/captcha.php" alt="" />
						<div class="clear"></div>
						<input type="image" src="/images/gfx/przycisk_wyslij_zapytanie.gif" style="margin: 10px 0 0 210px;" />
					</form>
				</div>
			</div> 
		{/if}	
	</div>

	{if !empty($aModule.item.categories)}
	<div class="clear"></div>
	<div class="produktWlasciwyKategorie">
		{$aModule.lang.categories}<strong>Kategorie:</strong> 
		{foreach from=$aModule.item.categories key=id item=aCat name=prod_categories}
			<span><a href="/{$aCat.symbol}">{$aCat.name}</a></span>
		{/foreach}
	</div>
	<div class="clear" style="height: 15px;"></div>
	{/if}
	{if !empty($aModule.item.description)}
	<div class="produktWlasciwyOpis">
	
		{if !empty($aModule.item.tags)}
		<div class="produktWlasciwyTagi">
			<div class="produktWlasciwyTagiGora"></div>
			<div class="produktWlasciwyTagiSrodek">
				<h2>Słowa kluczowe</h2>
				<div class="produktWlasciwyTagiTresc">
					{foreach from=$aModule.item.tags item=tag key=tid name=tags_loop}
					<a href="{$tag.link}">{$tag.tag}</a>
					{/foreach}
				</div>	
			</div>
			<div class="produktWlasciwyTagiDol"></div>
		</div>
		{/if}	
	
		{$aModule.item.description}
	</div>
	{/if}
	<div class="clear" style="height: 20px;"></div>
	{if !empty($aModule.item.name2) || !empty($aModule.item.binding) || !empty($aModule.item.dimension) || !empty($aModule.item.pages) || !empty($aModule.item.original_language) || !empty($aModule.item.translator) || !empty($aModule.item.volume_nr) || !empty($aModule.item.volumes) || !empty($aModule.item.volume_name) || !empty($aModule.item.city) || !empty($aModule.item.legal_status_date)}
	<div class="produktWlasciwyOpisyDodatkowe">
		<strong>Dodatkowe informacje:</strong>
		
		{if !empty($aModule.item.binding)}
		<div class="produktWlasciwyOkladka">
			{$aModule.lang.binding}: <span>{$aModule.item.binding}</span>
		</div>
		{/if}
		{if !empty($aModule.item.dimension)}
		<div class="produktWlasciwyWymiary">
			{$aModule.lang.dimension}: <span>{$aModule.item.dimension}</span>
		</div>
		{/if}
		{if !empty($aModule.item.pages)}
		<div class="produktWlasciwyStrony">
			{$aModule.lang.pages}: <span>{$aModule.item.pages}</span>
		</div>
		{/if}
		{if !empty($aModule.item.original_language)}
		<div class="produktWlasciwyJezykorg">
			{$aModule.lang.original_language}: <span>{if !empty($aModule.item.original_language_flag)}<img src="images/languages/{$aModule.item.original_language_flag}" alt="{$aModule.item.original_language}" title="{$aModule.item.original_language}"/>{else}{$aModule.item.original_language}{/if}</span>
		</div>
		{/if}
		{if !empty($aModule.item.translator)}
		<div class="produktWlasciwyStrony">
			{$aModule.lang.translator}: <span>{$aModule.item.translator}</span>
		</div>
		{/if}
		{if !empty($aModule.item.volume_nr)}
		<div class="produktWlasciwyTom">
			{$aModule.lang.volume_nr}: <span>{$aModule.item.volume_nr}{if !empty($aModule.item.volume_name)} ({$aModule.item.volume_name}){/if}{if !empty($aModule.item.volumes)} z {$aModule.item.volumes}{/if}</span>
		</div>
		{/if}
		{if !empty($aModule.item.city)}
		<div class="produktWlasciwyMiasto">
			{$aModule.lang.city}: <span>{$aModule.item.city}</span>
		</div>
		{/if}
		{if !empty($aModule.item.legal_status_date)}
		<div class="produktWlasciwyMiasto">
			{$aModule.lang.legal_status_date}: <span>{$aModule.item.legal_status_date}</span>
		</div>
		{/if}
	</div>
	{/if}
</div>

<div class="boxGlownyTytul">
	<div class="boxGlownyTytulLewo"></div>
	<div class="boxGlownyTytulSrodek">
		<h2>{$aModule.lang.composition}</h2>
	</div>
	<div class="boxGlownyTytulPrawo"></div>
</div>

{foreach from=$aModule.item.packet key=k item=packet name=items}
	<div class="produktLista">
		<div class="produktListaZdjecie">
			{if $packet.images[0].thumb != ''}<img src="{$packet.images[0].thumb}" alt="{$packet.plain_name}" />{else}<img src="/images/gfx/brak.png" alt="" />{/if}
		</div>

		<div class="left">
			<a name="{$k+1}"></a>
			<div class="produktListaTytul">
				<a href="{$packet.link}" title="{$packet.plain_name|strip}">{$packet.name}</a>
			</div>
			{if !empty($packet.publication_year)}
			<div class="produktListaRok">
				{$aModule.lang.publication_year}: <span>{$packet.publication_year}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				{if !empty($packet.edition)}{$aModule.lang.edition}: <span>{$packet.edition}</span>{/if}
			</div>
			{/if}
			{if !empty($packet.isbn)}
			<div class="produktListaIsbn">
				 {$aModule.lang.isbn} <span>{$packet.isbn}</span>
			</div>
			{/if}
			{if !empty($packet.publisher.name)}
			<div class="produktListaWydawnictwo">
				{$aModule.lang.publisher} <span><a href="{get_publisher_link string=$packet.publisher.name}">{$packet.publisher.name}</a></span>
			</div>
			{/if}
			{if !empty($packet.series)}
				<div class="produktListaSerie">
					{$aModule.lang.series}
						<span><a href="{get_series_link publisher=$packet.publisher.name series=$packet.series}">{$packet.series}</a></span>
				</div>
			{/if}

			{if !empty($packet.authors)}	
						<div class="produktListaAutorzy">
							{foreach from=$packet.authors key=rid item=aRole}
								{if !empty($aRole)}
									{$rid}:<span>
									{foreach from=$aRole key=id item=aAuthor name=prod_series}
										<a href="{$aAuthor.link}">{$aAuthor.name}{if !$smarty.foreach.prod_series.last}, {/if}</a>
									{/foreach}
									</span>
									{if !$smarty.foreach.auth_roles.last} {/if}
								{/if}
							{/foreach}
						</div>
					{/if}
		</div>
		
		<div class="right" style="padding-top: 25px;">
			<div class="produktListaCeny">
				{if $packet.price_brutto ne '0,00'}
					{if $packet.promo_price > 0}
						{$aModule.lang.you_old_price_brutto} <span>{$packet.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span><br />
						<strong>{$packet.promo_text} <span>{$packet.promo_price|format_number:2:',':' '}{$aModule.lang.currency}</span></strong>
					{else}
						<strong>{$aLang.common.cena_w_ksiegarni} <span>{$packet.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span></strong>	
					{/if}
				{/if}
			</div>
		</div>
		
		<div class="clear"></div>
		{if !empty($packet.item.description)}
		<div class="produktWlasciwyOpis">

			{if !empty($aModule.item.tags)}
			<div class="produktWlasciwyTagi">
				<div class="produktWlasciwyTagiGora"></div>
				<div class="produktWlasciwyTagiSrodek">
					<h2>Słowa kluczowe</h2>
					<div class="produktWlasciwyTagiTresc">
						{foreach from=$aModule.item.tags item=tag key=tid name=tags_loop}
						<a href="{$tag.link}">{$tag.tag}</a>
						{/foreach}
					</div>	
				</div>
				<div class="produktWlasciwyTagiDol"></div>
			</div>
			{/if}
			
			{$packet.item.description}
		</div>
		{/if}
		
	</div>
	{if !$smarty.foreach.items.last}
		<div class="produktListaSeparator"></div>
	{/if}
{/foreach}