<div class="container3">
	<div class="cnt3Header"><span>{$aModule.lang.files}&nbsp;</span></div>
	<div class="cnt3Content">
		<div class="files">
			<ul>
			{foreach from=$aFiles item=im key=i}
				<li><a href="{$im.src}" target="_blank">{if !empty($im.description)}{$im.description} ({$im.filename} - {$im.size}){else}{$im.filename} ({$im.size}){/if}</a></li>
			{/foreach}
			</ul>
		</div>
	</div>
</div>