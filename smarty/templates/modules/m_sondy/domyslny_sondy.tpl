<div class="sondaTytul">
    {$aModule.item.name}
</div>

<div id="wyniki">
    {foreach from=$aModule.item.answers item=aItem name=answers_list}
        <div class="sondaOpcja">
            <div class="sondaOpcjaTytul">
                {$aItem.answer} ({$aItem.votes_p}%)
            </div>        
            <div style="width: {$aItem.votes_p}%; height: 12px; background: transparent url(/images/gfx/sonda_pasek.gif) repeat-x; float: left; margin-right: 7px;"></div>
		</div>
	{/foreach}
	<div class="sondaOpcjaGlosow">
		{$aModule.lang.total_votes} <span>{$aModule.item.total_votes}</span>
    </div>
</div>
{if !empty($aModule.items)}
    <div class="separatorNews"></div>
    <div id="sondaInne">
        <div class="sondaTytul">
            {$aModule.lang.other_polls}
        </div>
    	<div id="sondaLista">
    		<ul>
			{foreach from=$aModule.items item=aItem name=polls_list}
				<li><a href="{$aItem.link}">{$aItem.name}</a></li>
			{/foreach}
            </ul>
		</div>
    </div>
{/if}