<div class="listaKategorii">
	{foreach from=$aModule.menu key=k item=li}
		{if isset($li.children)}
			{include file=$aModule.sub_template aMenu=$li.children}
		{/if}
	{/foreach}
</div>