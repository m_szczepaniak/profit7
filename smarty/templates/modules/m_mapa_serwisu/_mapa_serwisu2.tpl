<ul>
	{foreach from=$aMenu key=k item=li}
		<li><a href="{$li.link}"{if isset($li.target)} target="{$li.target}"{/if}>{$li.name}</a>
		{if isset($li.children)}
			{include file=$aModule.sub_template aMenu=$li.children}
		{/if}
		</li>
	{/foreach}
</ul>