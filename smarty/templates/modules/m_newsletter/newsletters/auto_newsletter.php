<?php 

function getNewsletterHtml($sBaseHref,&$lang,&$aItems,&$aImages,&$sDescriptionHTML){
	global $aConfig;
	
	$sHtml='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<table border="0" cellpadding="0" cellspacing="0" width="720" style="font-size: 11px;margin: 10px auto;font-size: 11px; color: #474747;font-family: Tahoma, Arial, Verdana, Helvetica;">
		<tr>
			<td width="40%">
				<a href="'.$sBaseHref.'" style="text-decoration: none;"><img src="logo_newsletter.gif" alt="" border="0"></a>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" align="right" border="0" style="font-size: 11px;color: #474747;font-family: Tahoma, Arial, Verdana, Helvetica;">
					<tr>
						<td align="right" style="padding: 0 0 13px;">
							<a href="'.$sBaseHref.'/moje-konto" style="color: #474747;text-decoration: none;"><strong>Zaloguj się</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Wysłano: '.date('d.m.Y').'r.
						</td>
					</tr>
					<tr>
						<td align="right">
							<img src="indeksy.gif" width="96" height="34" style="padding: 0;"><a href="'.$sBaseHref.'/indeks-autorow"><img src="indeksy_autorow.gif" width="69" height="34" border="0"></a><a href="'.$sBaseHref.'/indeks-wydawnictw"><img src="indeksy_wydawnictw.gif" width="93" height="34" border="0"></a><a href="'.$sBaseHref.'/indeks-serii"><img src="indeksy_serii.gif" width="54" height="34" border="0"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 3px solid #c31e1e; padding: 16px 0 0 0;"><a href="'.$sBaseHref.'/"><img src="dzial_naukowy.gif" width="173" height="35" border="0"></a><a href="'.$sBaseHref.'/dzial-jezykowy"><img src="dzial_jezykowy.gif" width="173" height="35" border="0"></a><a href="'.$sBaseHref.'/dzial-ogolny"><img src="dzial_ogolny.gif" width="156" height="35" border="0"></a></td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 1px solid #e6e6e6; padding: 12px 0 0 0;">
			';
			if($sDescriptionHTML != ''){
				$sHtml .= $sDescriptionHTML.'<br/><br/>';
			}
			foreach ($aImages as &$aImg) {
				$sPhotoFile = $aImg[0];
				if (file_exists($sPhotoFile) == true) {
					$aImg['photo_src'] = $sPhotoFile;
					preg_match("/(.*)\/(.*)$/", $sPhotoFile, $aMatch);
					$sHtml .= '<center><img border="0" src="'.$aMatch[2].'" alt="" /></center><br/><br/>';
				}
				unset($aImg);
			}
			$sHtml .= '
			</td>
		</tr>
		<tr>
			<td colspan="2" style="margin: 0 0 17px;" valign="top">
				
				<table cellpadding="0" cellspacing="0" width="720">
					<tr>';
						foreach($aItems as $i=>$item){
							$aItemsNr++;
							$item['shortTitle'] = myTruncate($item['title'], 60);
							
							$sHtml.='
								<td style="border-bottom: 1px solid #e6e6e6;padding: 9px 0 0 0;" valign="top">	
									<table style="font-size: 11px; padding: 0 0 7px 0;" width="240">
													<tr>
														<td width="84" valign="top" style="text-align: center;">
															<a href="'.$item['product_link'].'" style="font-size: 11px"><img src="'.$item['image_link'].'" alt="'.$item['title'].'" style="padding: 0; border: 1px solid #414141;margin: 3px 0 10px 5px;"></a>
														</td>
														<td style="font-size: 11px" valign="top">
															<b style="margin: 0 0 10px 0;"><a href="'.$item['product_link'].'" style="color: #000;font-size: 11px;text-decoration: none;">'.$item['shortTitle'].'</a><br></b>';
										if(!empty($item['authors'])){
												foreach($item['authors'] as $sIRola => $aRole){
													foreach ($aRole as $iAKey => $aAuthor) {
														$sHtml.= '<span style="color: #7a7a7a;">'.$aAuthor['name'].'</span>';
													}
												}
										}
										if(!empty($item['publisher'])){
											$sHtml .= '<br><br><span style="color: #104586;">'.$item['publisher'].'</span><br>';
										}
										
										if(!empty($item['is_news'])){
											$sHtml .= '<img src="przycisk_nowosci.png" /><br />';
										} 
										elseif (!empty($item['is_previews'])) {
											$sHtml .= '<img src="przycisk_zapowiedz.png" /><br />';
										}
										$sHtml.='</td>
											</tr>
									</table>
								</td>';
									
									
							if($aItemsNr % 3 == 0){
								$sHtml .= '
									</tr>
								</table>
								<table cellpadding="0" cellspacing="0">
									<tr>
								';
							}

						}
			$sHtml.='
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; font-size: 12px;color: #434343;font-weight: bold;"><br>Zobacz wszystkie nowości:</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; font-size: 12px;color: #434343;font-weight: bold;padding: 13px 0 22px;">
				<a href="'.$sBaseHref.'/nowosci-dzial-naukowy"><img src="p_dzial_naukowy.gif" width="135" height="33" border="0"></a>
				<a href="'.$sBaseHref.'/nowosci-dzial-jezykowy"><img src="p_dzial_jezykowy.gif" width="135" height="33" border="0"></a>
				<a href="'.$sBaseHref.'/nowosci-dzial-ogolny"><img src="p_dzial_ogolny.gif" width="135" height="33" border="0"></a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0" style="font-size: 11px;color: #474747;font-family: Tahoma, Arial, Verdana, Helvetica;">
					<tr>
						<td><img src="zobacz_gora.gif" width="720" height="32" border="0"></td>
					</tr>
					<tr>
						<td>
							<table width="720" style="border-right: 1px solid #d2d5da;border-left: 1px solid #d2d5da;background-color: #ebebeb;font-size: 11px;color: #474747;font-family: Tahoma, Arial, Verdana, Helvetica;">
								<tr>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Nowości:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/nowosci-dzial-naukowy" style="color: #313030;text-decoration: none;">Dział naukowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/nowosci-dzial-jezykowy" style="color: #313030;text-decoration: none;">Dział językowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/nowosci-dzial-ogolny" style="color: #313030;text-decoration: none;">Dział ogólny</a>
											</li>
										</ul>
									</td>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Bestsellery:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/bestsellery-dzial-naukowy" style="color: #313030;text-decoration: none;">Dział naukowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/bestsellery-dzial-jezykowy" style="color: #313030;text-decoration: none;">Dział językowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/bestsellery-dzial-ogolny" style="color: #313030;text-decoration: none;">Dział ogólny</a>
											</li>
										</ul>
									</td>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Promocje:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/promocje-dzial-naukowy" style="color: #313030;text-decoration: none;">Dział naukowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/promocje-dzial-jezykowy" style="color: #313030;text-decoration: none;">Dział językowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/promocje-dzial-ogolny" style="color: #313030;text-decoration: none;">Dział ogólny</a>
											</li>
										</ul>
									</td>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Indeksy:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/indeks-autorow" style="color: #313030;text-decoration: none;">Autorzy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/indeks-wydawnictw" style="color: #313030;text-decoration: none;">Wydawnictwa</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/indeks-serii" style="color: #313030;text-decoration: none;">Serie wydawnicze</a>
											</li>
										</ul>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><img src="zobacz_dol.gif" width="720" height="4" border="0"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="688" style="margin: 0 auto; font-size: 11px;font-family: Tahoma, Arial, Verdana, Helvetica;">
					<tr>
						<td colspan="2" align="center" style="border-bottom: 1px solid #e6e6e6; padding: 16px 0 17px;">
							<b>Profit M Spółka z ograniczoną odpowiedzialnością</b> Al. Jerozolimskie 134, 02-305 Warszawa<br>
							NIP 525-22-45-459, KRS: 0000756053 Sąd Rejonowy dla miasta stołecznego Warszawy w Warszawie, XII Wydział Gospodarczy KRS
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="padding: 10px 0 0;font-size: 10px;">
							Wiadomość ta została przesłana w ramach subskrypcji newsletter-a Księgarni Internetowej Profit24.pl. Jeśli nie chcą Państwo otrzymywać informacji o naszych nowościach i promocjach, to prosimy o kliknięcie na następujący link - <a href="{delete_link}" style="color: #104586;">rezygnacja z subskrypcji</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<img src="przycisk_nowosci.png" style="display: none; width:1px; height: 1px;" />
	<img src="przycisk_zapowiedz.png" style="display: none; width:1px; height: 1px;" />
</body>
</html>
';
	return $sHtml;
}

function myTruncate($string, $limit, $break=" ", $pad="..."){
	// return with no change if string is shorter than $limit 
	if(strlen($string) <= $limit) return $string;
	
	// is $break present between $limit and the end of the string? 
	if(false !== ($breakpoint = strpos($string, $break, $limit))) {
		 if($breakpoint < strlen($string) - 1) {
		 	 $string = substr($string, 0, $breakpoint) . $pad; 
		 } 
	}	
	return $string; 
}
	

function getNewsletterImages(){
global $aConfig;
	$aImages=array(
		'/images/gfx/mailing/logo_newsletter.gif',
		'/images/gfx/mailing/indeksy.gif',
		'/images/gfx/mailing/indeksy_autorow.gif',
		'/images/gfx/mailing/indeksy_wydawnictw.gif',
		'/images/gfx/mailing/indeksy_serii.gif',
		'/images/gfx/mailing/dzial_naukowy.gif',
		'/images/gfx/mailing/dzial_jezykowy.gif',
		'/images/gfx/mailing/dzial_ogolny.gif',
		'/images/gfx/mailing/p_dzial_naukowy.gif',
		'/images/gfx/mailing/p_dzial_jezykowy.gif',
		'/images/gfx/mailing/p_dzial_ogolny.gif',
		'/images/gfx/mailing/zobacz_gora.gif',
		'/images/gfx/mailing/zobacz_dol.gif',
		'/images/gfx/przycisk_nowosci.png',
		'/images/gfx/przycisk_zapowiedz.png',	
	);
	$aImg = array();
	foreach($aImages as $iKey=>$sImg){
		$sImg = $aConfig['common']['client_base_path'].$sImg;
		// okreslenie typu obrazka
		$aImage = getimagesize($sImg);
		$aImg[] = array($sImg, $aImage['mime']);
	}
	return $aImg;
}

?>