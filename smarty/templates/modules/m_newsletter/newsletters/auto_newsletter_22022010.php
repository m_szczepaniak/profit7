<?php 

function getNewsletterHtml($sBaseHref,&$lang,&$aItems){
	$sHtml='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<table border="0" cellpadding="0" cellspacing="0" width="720" style="font-size: 11px;margin: 10px auto;font-size: 11px; color: #474747;font-family: Tahoma, Arial, Verdana, Helvetica;">
		<tr>
			<td width="40%">
				<a href="'.$sBaseHref.'" style="text-decoration: none;"><img src="logo_newsletter.gif" alt="" border="0"></a>
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" align="right" border="0">
					<tr>
						<td align="right" style="padding: 0 0 13px;">
							<a href="'.$sBaseHref.'/moje-konto" style="color: #474747;text-decoration: none;"><strong>Zaloguj się</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Wysłano: '.date('d.m.Y').'r.
						</td>
					</tr>
					<tr>
						<td align="right">
							<img src="indeksy.gif" width="96" height="34" style="padding: 0;"><a href="'.$sBaseHref.'/indeks-autorow"><img src="indeksy_autorow.gif" width="69" height="34" border="0"></a><a href="'.$sBaseHref.'/indeks-wydawnictw"><img src="indeksy_wydawnictw.gif" width="93" height="34" border="0"></a><a href="'.$sBaseHref.'/indeks-serii"><img src="indeksy_serii.gif" width="54" height="34" border="0"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 3px solid #c31e1e; padding: 16px 0 0 0;"><a href="'.$sBaseHref.'/"><img src="dzial_naukowy.gif" width="173" height="35" border="0"></a><a href="'.$sBaseHref.'/dzial-jezykowy"><img src="dzial_jezykowy.gif" width="173" height="35" border="0"></a><a href="'.$sBaseHref.'/dzial-ogolny"><img src="dzial_ogolny.gif" width="156" height="35" border="0"></a></td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 14px 0 17px;">
				';
				foreach($aItems as $i=>$item){
					$sHtml.='<table style="border-bottom: 1px solid #e6e6e6;font-size: 11px; padding: 11px 0;" width="720">
								<tr>
									<td width="62">
										<a href="'.$item['product_link'].'" style="font-size: 11px"><img src="'.$item['image_link'].'" alt="'.$item['title'].'" width="42" style="padding: 1px; border: 1px solid #dad4ca;"></a>
									</td>
									<td style="font-size: 11px">
										<b style="padding: 0 0 6px 0;"><a href="'.$item['product_link'].'" style="color: #000;" style="font-size: 11px;">'.$item['title'].'</a><br></b>';
					if(!empty($item['publisher'])){
						$sHtml.= $lang['publisher'].': <span style="color: #104586;">'.$item['publisher'].'</span><br>';
					}
					if(!empty($item['authors'])){
						$sHtml.= $lang['authors'].': <span style="color: #104586;">'.$item['authors'].'</span>';
					}
					$sHtml.='</td>
								</tr>
							</table>';
				}
				$sHtml.='
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; font-size: 12px;color: #434343;font-weight: bold;">Zobacz wszystkie nowości:</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; font-size: 12px;color: #434343;font-weight: bold;padding: 13px 0 22px;">
				<a href="'.$sBaseHref.'/"><img src="p_dzial_naukowy.gif" width="135" height="33" border="0"></a>
				<a href="'.$sBaseHref.'/dzial-jezykowy"><img src="p_dzial_jezykowy.gif" width="135" height="33" border="0"></a>
				<a href="'.$sBaseHref.'/dzial-ogolny"><img src="p_dzial_ogolny.gif" width="135" height="33" border="0"></a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td><img src="zobacz_gora.gif" width="720" height="32" border="0"></td>
					</tr>
					<tr>
						<td>
							<table width="720" style="border-right: 1px solid #d2d5da;border-left: 1px solid #d2d5da;background-color: #ebebeb;">
								<tr>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Nowości:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/nowosci-dzial-naukowy" style="color: #313030;text-decoration: none;">Dział naukowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/nowosci-dzial-jezykowy" style="color: #313030;text-decoration: none;">Dział językowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/nowosci-dzial-ogolny" style="color: #313030;text-decoration: none;">Dział ogólny</a>
											</li>
										</ul>
									</td>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Bestsellery:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/bestsellery-dzial-naukowy" style="color: #313030;text-decoration: none;">Dział naukowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/bestsellery-dzial-jezykowy" style="color: #313030;text-decoration: none;">Dział językowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/bestsellery-dzial-ogolny" style="color: #313030;text-decoration: none;">Dział ogólny</a>
											</li>
										</ul>
									</td>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Promocje:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/promocje-dzial-naukowy" style="color: #313030;text-decoration: none;">Dział naukowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/promocje-dzial-jezykowy" style="color: #313030;text-decoration: none;">Dział językowy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/promocje-dzial-ogolny" style="color: #313030;text-decoration: none;">Dział ogólny</a>
											</li>
										</ul>
									</td>
									<td style="padding: 20px 0 23px 27px;">
										<h2 style="color: #a10100;font-size:12px;margin:0;padding:0 0 14px;">Indeksy:</h2>
										<ul style="margin: 0;padding:0 0 0 18px;line-height: 20px;">
											<li>
												<a href="'.$sBaseHref.'/indeks-autorow" style="color: #313030;text-decoration: none;">Autorzy</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/indeks-wydawnictw" style="color: #313030;text-decoration: none;">Wydawnictwa</a>
											</li>
											<li>
												<a href="'.$sBaseHref.'/indeks-serii" style="color: #313030;text-decoration: none;">Serie wydawnicze</a>
											</li>
										</ul>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><img src="zobacz_dol.gif" width="720" height="4" border="0"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="688" style="margin: 0 auto; font-size: 11px;">
					<tr>
						<td colspan="2" align="center" style="border-bottom: 1px solid #e6e6e6; padding: 16px 0 17px;">
							<b>Profit Spółka Jawna</b> Plac Defilad 1, Metro Centrum, lok.2002D 00-110 Warszawa 1<br>
							NIP 525-22-45-459, KRS: 0000756053 Sąd Rejonowy dla miasta stołecznego Warszawy w Warszawie, XII Wydział Gospodarczy KRS
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="padding: 10px 0 0;font-size: 10px;">
							Wiadomość ta została przesłana w ramach subskrypcji newsletter-a Księgarni Internetowej Profit24.pl. Jeśli nie chcą Państwo otrzymywać informacji o naszych nowościach i promocjach, to prosimy o kliknięcie na następujący link - <a href="DO_WSTAWIENIA" style="color: #104586;">rezygnacja z subskrypcji</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<table cellpadding="0" cellspacing="0" width="720" style="font-size: 11px;margin: 20px auto;font-size: 11px;">
		<tr>
			<td width="40%">
				<a href="'.$sBaseHref.'"><img src="logo_newsletter.gif" alt=""></a>
			</td>
			<td width="60%" align="right">
				<div style="color: #474747;">
					<a href="'.$sBaseHref.'/moje-konto" style="color: #474747;"><strong>Zaloguj się</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
					Wysłano: '.date('d.m.Y').'r.
				</div>
				<div style="width: 332px;height: 33px !important;float: right;margin: 10px 0 0;background: url("'.$sBaseHref.'/images/gfx/indeksy_srodek.gif") repeat-x;">
					<div style="width: 5px;height: 33px;float: left;background: url("indeksy_lewo.gif") no-repeat;"></div>
						<div style="width: 306px;height: 33px;float: left;background: url("'.$sBaseHref.'/images/gfx/indeksy_ikona.gif") no-repeat 17px 10px;">
						<H2 style="font-size: 12px;font-family: Arial, Tahoma, Verdana, Helvetica !important;font-weight: bold;color: #fff;padding: 8px 0 5px 49px;float: left;">Indeksy:</H2>
						<div style="float: right;font-size: 11px;color: #fff;padding: 9px 0 0 0;text-align: center;">
							<a style="font-weight: bold;color: #fff;margin: 0 7px;" href="'.$sBaseHref.'/indeks-autorow">Autorów</a>|<a style="font-weight: bold;color: #fff;margin: 0 7px;" href="'.$sBaseHref.'/indeks-wydawnictw">Wydawnictw</a>|<a style="font-weight: bold;color: #fff;margin: 0 7px;" style="margin-right: 0pt" href="'.$sBaseHref.'/indeks-serii">Serii</a>
						</div>
					</div>
					<div style="width: 5px;height: 33px;float: right;background: url("'.$sBaseHref.'/images/gfx/indeksy_prawo.gif") no-repeat;"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<ul style="width: 720px;height: 35px;list-style: none;border-bottom: 3px solid #c31e1e;margin: 25px 0 0;">
					<li style="float: left;"><a style="display: block;height: 35px;background: url("'.$sBaseHref.'/images/gfx/dzial_naukowy.gif") no-repeat 0 0;width: 173px;" title="Dział naukowy" href="'.$sBaseHref.'/dzial-naukowy"></a></li>
					<li style="float: left;"><a style="display: block;height: 35px;background: url("'.$sBaseHref.'/images/gfx/dzial_jezykowy.gif") no-repeat 0 0;width: 175px;" title="Dział językowy" href="'.$sBaseHref.'/dzial-jezykowy"></a></li>
					<li style="float: left;margin: 0;"><a style="display: block;height: 35px;background: url("'.$sBaseHref.'/images/gfx/dzial_ogolny.gif") no-repeat 0 0;width: 156px;" title="Dział ogólny" href="'.$sBaseHref.'/dzial-ogolny"></a></li>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 15px 0 20px;">		
	';
	
	foreach($aItems as $i=>$item){
		$sHtml.='<table style="border-bottom: 1px solid #e6e6e6;" width="720" style="font-size: 11px; padding: 11px 0;">
					<tr>
						<td width="62">
							<a href="'.$item['product_link'].'" style="font-size: 11px"><img src="'.$item['image_link'].'" alt="'.$item['title'].'" width="42" style="padding: 1px; border: 1px solid #dad4ca;"></a>
						</td>
						<td style="font-size: 11px">
							<b style="padding: 0 0 6px 0;"><a href="'.$item['product_link'].'" style="color: #000;" style="font-size: 11px;">'.$item['title'].'</a><br></b>';
		if(!empty($item['publisher'])){
			$sHtml.= $lang['publisher'].': <span style="color: #104586;">'.$item['publisher'].'</span><br>';
		}
		if(!empty($item['authors'])){
			$sHtml.= $lang['authors'].': <span style="color: #104586;">'.$item['authors'].'</span>';
		}
		$sHtml.='</td>
					</tr>
				</table>';
	}
	$sHtml.='</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="font-size: 12px; font-weight: bold;">
				Zobacz wszystkie nowości:<br>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="padding: 10px 0 10px 130px;">
				<a href="'.$sBaseHref.'/" style="width: 142px;height: 33px;padding: 6px 0 0 0;background: url("przycisk_indeksy.gif") no-repeat;text-align: center;font-size: 13px;color: #fff;font-weight: bold;display: block;text-decoration: none !important;cursor: pointer;float: left;margin: 0 5px;">Dział naukowy</a>
				<a href="'.$sBaseHref.'/dzial-jezykowy" style="width: 142px;height: 33px;padding: 6px 0 0 0;background: url("przycisk_indeksy.gif") no-repeat;text-align: center;font-size: 13px;color: #fff;font-weight: bold;display: block;text-decoration: none !important;cursor: pointer;float: left;margin: 0 5px;">Dział językowy</a>
				<a href="'.$sBaseHref.'/dzial-ogolny" style="width: 142px;height: 33px;padding: 6px 0 0 0;background: url("przycisk_indeksy.gif") no-repeat;text-align: center;font-size: 13px;color: #fff;font-weight: bold;display: block;text-decoration: none !important;cursor: pointer;float: left;margin: 0 5px;">Dział ogólny</a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="720" cellpadding="0" cellspacing="0" style="font-size: 11px">
					<tr>
						<td style="width: 5px;height: 32px;background: url("'.$sBaseHref.'/images/gfx/dol_tytul_lewo.gif") no-repeat;"></td>
						<td style="width: 710px;height: 32px;background: url("'.$sBaseHref.'/images/gfx/dol_tytul_srodek.gif") repeat-x;"><h2 style="font-size: 13px;font-weight: bold;color: #fff;font-family: Arial, Tahoma, Verdana, Helvetica !important;padding: 0 10px 0;">Zobacz także:</h2></td>
						<td style="width: 5px;height: 32px;background: url("'.$sBaseHref.'/images/gfx/dol_tytul_prawo.gif") no-repeat;"></td>
					</tr>
					<tr>
						<td colspan="3" style="border-left: 1px solid #d0d3d8;border-right: 1px solid #d0d3d8;background: #fefefe url("'.$sBaseHref.'/images/gfx/dol_srodek.gif") repeat-x bottom;">

						</td>
					</tr>
					<tr>
						<td style="width: 5px;height: 5px;background: url("dol_dol_lewo.gif") no-repeat;overflow: hidden;"></td>
						<td style="width: 710px;height: 5px;background: url("dol_dol_srodek.gif") repeat-x;overflow: hidden;"></td>
						<td style="width: 5px;height: 5px;background: url("dol_dol_prawo.gif") no-repeat;overflow: hidden;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="688" style="margin: 0 auto; font-size: 11px;">
					<tr>
						<td colspan="2" align="center" style="border-bottom: 1px solid #e6e6e6; padding: 16px 0 17px;">
							<b>Profit Spółka Jawna</b> Plac Defilad 1, Metro Centrum, lok.2002D 00-110 Warszawa 1<br>
							NIP 525-22-45-459, KRS: 0000756053 Sąd Rejonowy dla miasta stołecznego Warszawy w Warszawie, XII Wydział Gospodarczy KRS
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="padding: 10px 0 0;font-size: 10px;">
							Wiadomość ta została przesłana w ramach subskrypcji newsletter-a Księgarni Internetowej Profit24.pl. Jeśli nie chcą Państwo otrzymywać informacji o naszych nowościach i promocjach, to prosimy o kliknięcie na następujący link - <a href="DO_WSTAWIENIA" style="color: #104586;">rezygnacja z subskrypcji</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>';
	return $sHtml;
}

function getNewsletterImages(){
global $aConfig;
	$aImages=array(
		'images/gfx/logo_newsletter.gif',
		'images/gfx/indeksy_lewo.gif',
		'images/gfx/dol_dol_srodek.gif',
		'images/gfx/dol_dol_prawo.gif',
		'images/gfx/dol_dol_lewo.gif',
		'images/gfx/przycisk_indeksy.gif'
	);
	$aImg = array();
	foreach($aImages as $iKey=>$sImg){
		$sImg = $aConfig['common']['client_base_path'].$sImg;
		// okreslenie typu obrazka
		$aImage = getimagesize($sImg);
		$aImg[] = array($sImg, $aImage['mime']);
	}
	dump($aImg);
	return $aImg;
}

?>