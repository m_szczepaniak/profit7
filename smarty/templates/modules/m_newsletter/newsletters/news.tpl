<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

{literal}
<style type="text/css">
body {
	font-family: Tahoma, Arial, Verdana, Helvetica;
	padding: 0;
	margin: 0;
	font-size: 11px;
	background: #fff;
}
h1,h2,h3,h4,h5,h6,p,form,fieldset,img,ul {
	margin: 0;
	padding: 0;
	border: 0;
}
a {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
#ne_container {
	margin: 20px auto;
	font-size: 11px;
}
#ne_linkiRejestracja, #ne_linkiRejestracja a {
	color: #474747;
}
.boxIndeksy {
	width: 332px;
	height: 33px !important;
	float: right;
	margin: 10px 0 0;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/indeksy_srodek.gif") repeat-x;
}
.boxIndeksyLewo {
	width: 5px;
	height: 33px;
	float: left;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/indeksy_lewo.gif") no-repeat;
}
.boxIndeksySrodek {
	width: 306px;
	height: 33px;
	float: left;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/indeksy_ikona.gif") no-repeat 17px 10px;
}
.boxIndeksyPrawo {
	width: 5px;
	height: 33px;
	float: right;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/indeksy_prawo.gif") no-repeat;
}
.boxIndeksy H2 {
	font-size: 12px;
	font-family: Arial, Tahoma, Verdana, Helvetica !important;
	font-weight: bold;
	color: #fff;
	padding: 8px 0 5px 49px;
	float: left;
}
.boxIndeksy .boxIndeksyTresc {
	float: right;
	font-size: 11px; 
	color: #fff; 
	padding: 9px 0 0 0;
	text-align: center;
}
.boxIndeksy a {
	font-weight: bold;
	color: #fff;
	margin: 0 7px;
}
ul#tabsDzialyGora {
	width: 720px;
	height: 35px;
	list-style: none;
	border-bottom: 3px solid #c31e1e;
	margin: 25px 0 0;
}
ul#tabsDzialyGora li {
	float: left;
}
ul#tabsDzialyGora li a {
	display: block;
	height: 35px;
}
ul#tabsDzialyGora li a:hover, ul#tabsDzialyGora li.ui-tabs-selected a  {
	background-position: 0 -35px;
}
#tabsDzial-0 a {
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dzial_naukowy.gif") no-repeat 0 0;
	width: 173px;
}
#tabsDzial-1 a {
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dzial_jezykowy.gif") no-repeat 0 0;
	width: 175px;
}
#tabsDzial-2 a {
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dzial_ogolny.gif") no-repeat 0 0;
	width: 156px;
}
.produktListaZdjecie {
	width: 106px;
	float: left;
	text-align: center;
	overflow: hidden;
}
.produktListaZdjecie img {
	padding: 1px;
	border: 1px solid #dad4ca;
}
.produktListaTytul {
	font-weight: bold;
	padding: 0 0 2px 0;
	color: #000;
}
.produktListaTytul a {
	color: #000;
}
#dolTytulLewo {
	width: 5px;
	height: 32px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_tytul_lewo.gif") no-repeat;
}
#dolTytulSrodek {
	width: 710px;
	height: 32px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_tytul_srodek.gif") repeat-x;
}
#dolTytulSrodek h2 {
	font-size: 13px;
	font-weight: bold;
	color: #fff;	
	font-family: Arial, Tahoma, Verdana, Helvetica !important;
	padding: 0 10px 0;
}
#dolTytulPrawo {
	width: 5px;
	height: 32px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_tytul_prawo.gif") no-repeat;
}
#dolSrodek {
	border-left: 1px solid #d0d3d8;
	border-right: 1px solid #d0d3d8;
	background: #fefefe url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_srodek.gif") repeat-x bottom;
}
#dolDolLewo {
	width: 5px;
	height: 5px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_dol_lewo.gif") no-repeat;
	overflow: hidden;
}
#dolDolSrodek {
	width: 710px;
	height: 5px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_dol_srodek.gif") repeat-x;
	overflow: hidden;
}
#dolDolPrawo {
	width: 5px;
	height: 5px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_dol_prawo.gif") no-repeat;
	overflow: hidden;
}
a.sendBig {
	width: 142px;
	height: 33px;
	padding: 6px 0 0 0;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/przycisk_indeksy.gif") no-repeat;
	text-align: center;
	font-size: 13px;
	color: #fff;
	font-weight: bold;
	display: block;
	text-decoration: none !important;
	cursor: pointer;
	float: left;
	margin: 0 5px;
}
.menuDolne {
	float: left;
	width: 120px;
	padding: 25px 0 25px 48px;
}
.menuDolneKontakt {
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/dol_separator.gif") no-repeat 0 27px;
	padding: 25px 0 25px 30px;
	width: auto;
}
.menuDolneKontakt img {
	margin: 0 0 -6px 0;
}
.menuDolneKontakt li {
	margin: 12px 0 0 0 !important;
}
.menuDolne h2 {
	color: #a10100;
	font-size: 12px;
	font-weight: bold;
	height: 22px;
	display: inline;
}
.menuDolne ul {
	list-style: none;
	margin: 20px 0 10px 0;
}
.menuDolne ul li {
	margin: 7px 0 0 0;
	padding: 0 0 0 11px;
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/li_menu_dol.gif") no-repeat 0 6px;
}
.menuDolne ul li a {
	color: #313030;
}
.menuDolne ul li ul {
	margin: 0;
	padding-top: 0;
}
.menuDolne ul li ul li {
	background: url("{/literal}{$sBaseHref}{literal}/images/gfx/menu_dolne_li.gif") no-repeat 0 4px;
	padding: 0 0 0 12px;
}
</style>
{/literal}

<body>
	<table id="ne_container" cellpadding="0" cellspacing="0" width="720" style="font-size: 11px">
		<tr>
			<td width="40%">
				<a href="{$sBaseHref}"><img src="{$sBaseHref}/images/gfx/logo_newsletter.gif" alt=""></a>
			</td>
			<td width="60%" align="right">
				<div id="ne_linkiRejestracja">
					<a href="{$sBaseHref}/moje-konto"><strong>Zaloguj się</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
					Wysłano: 00.00.0000
				</div>
				<div class="boxIndeksy">
					<div class="boxIndeksyLewo"></div>
					<div class="boxIndeksySrodek">
						<H2>Indeksy:</H2>
						<div class="boxIndeksyTresc">
							<a href="{$sBaseHref}/indeks-autorow">Autorów</a>|<a href="{$sBaseHref}/indeks-wydawnictw">Wydawnictw</a>|<a style="margin-right: 0pt" href="{$sBaseHref}/indeks-serii">Serii</a>
						</div>
					</div>
					<div class="boxIndeksyPrawo"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<ul id="tabsDzialyGora">
					<li id="tabsDzial-0"><a title="Dział naukowy" href="{$sBaseHref}/dzial-naukowy"></a></li>
					<li id="tabsDzial-1"><a title="Dział językowy" href="{$sBaseHref}/dzial-jezykowy"></a></li>
					<li id="tabsDzial-2" style="margin: 0;"><a title="Dział ogólny" href="{$sBaseHref}/dzial-ogolny"></a></li>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 15px 0 20px;">
				{foreach from=$aItems key=i item=item name=items}
				<table style="border-bottom: 1px solid #e6e6e6;" width="720" style="font-size: 11px">
					<tr>
						<td width="62">
							<a href="{$item.product_link}" style="font-size: 11px"><img src="{$item.image_link}" alt="{$item.title}"></a>
						</td>
						<td style="font-size: 11px">
							<b><a href="{$item.product_link}" style="color: #000;" style="font-size: 11px;padding: 0 6px 0 0;">{$item.title}</a><br></b>
							{if !empty($item.publisher)}{$lang.publisher}: <span style="color: #104586;">{$item.publisher}</span>{/if}<br>
							{if !empty($item.authors)}{$lang.authors}: <span style="color: #104586;">{$item.authors}</span>{/if}
						</td>
					</tr>
				</table>
				{/foreach}
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="font-size: 12px; font-weight: bold;">
				Zobacz wszystkie nowości:<br>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="padding: 10px 0 10px 130px;">
				<a href="{$sBaseHref}/dzial-naukowy" class="sendBig">Dział naukowy</a>
				<a href="{$sBaseHref}/dzial-jezykowy" class="sendBig">Dział językowy</a>
				<a href="{$sBaseHref}/dzial-ogolny" class="sendBig">Dział ogólny</a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="720" cellpadding="0" cellspacing="0" style="font-size: 11px">
					<tr>
						<td id="dolTytulLewo"></td>
						<td id="dolTytulSrodek"><h2>Zobacz także:</h2></td>
						<td id="dolTytulPrawo"></td>
					</tr>
					<tr>
						<td colspan="3" id="dolSrodek">
							<div class="menuDolne">
								<h2>Nowości:</h2>
								<ul>
									<li>
										<a href="{$sBaseHref}/nowosci-dzial-naukowy">Dział naukowy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/nowosci-dzial-jezykowy">Dział językowy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/nowosci-dzial-ogolny">Dział ogólny</a>
									</li>
								</ul>
							</div>
							<div class="menuDolne">
								<h2>Bestsellery:</h2>
								<ul>
									<li>
										<a href="{$sBaseHref}/bestsellery-dzial-naukowy">Dział naukowy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/bestsellery-dzial-jezykowy">Dział językowy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/bestsellery-dzial-ogolny">Dział ogólny</a>
									</li>
								</ul>
							</div>
							<div class="menuDolne">
								<h2>Promocje:</h2>
								<ul>
									<li>
										<a href="/promocje-dzial-naukowy">Dział naukowy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/promocje-dzial-jezykowy">Dział językowy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/promocje-dzial-ogolny">Dział ogólny</a>
									</li>
								</ul>
							</div>
							<div class="menuDolne">
								<h2>Indeksy:</h2>
								<ul>
									<li>
										<a href="{$sBaseHref}/indeks-autorow">Autorzy</a>
									</li>
									<li>
										<a href="{$sBaseHref}/indeks-wydawnictw">Wydawnictwa</a>
									</li>
									<li>
										<a href="{$sBaseHref}/indeks-serii">Serie wydawnicze</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
					<tr>
						<td id="dolDolLewo"></td>
						<td id="dolDolSrodek"></td>
						<td id="dolDolPrawo"></td>
					</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="688" style="margin: 0 auto; font-size: 11px;">
					<tr>
						<td colspan="2" align="center" style="border-bottom: 1px solid #e6e6e6; padding: 16px 0 17px;">
							<b>Profit M</b> Spółka z ograniczoną odpowiedzialnością Al. Jerozolimskie 134, 02-305 Warszawa<br>
							NIP 525-22-45-459, KRS: 0000756053 Sąd Rejonowy dla miasta stołecznego Warszawy w Warszawie, XII Wydział Gospodarczy KRS
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" style="padding: 10px 0 0;font-size: 10px;">
							Wiadomość ta została przesłana w ramach subskrypcji newsletter-a Księgarni Internetowej Profit24.pl. Jeśli nie chcą Państwo otrzymywać informacji o naszych nowościach i promocjach, to prosimy o kliknięcie na następujący link - <a href="DO_WSTAWIENIA" style="color: #104586;">rezygnacja z subskrypcji</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>