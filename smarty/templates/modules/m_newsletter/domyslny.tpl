<div class="registrationForm" style="padding-top: 10px;">
{*<div class="naglowekDlugi">{$aModule.name}{if isset($aModule.subname)}{$aModule.subname}{/if}</div>*}
	<div class="formContainer">
		{if !empty($sFText)}
			{$sFText}
		{/if}
		<form action="{$aModule.page_link}" method="post" name="newsletterForm" id="newsletterForm" style="padding: 10px 0 0 0;">
			<div class="fRow">
				<div class="fLabel"><label for="nf_email">{$aModule.lang.email} <span class="required">*</span></label></div>
				<div class="fInput"><input type="text" name="n_email" class="text iEmail" id="nf_email"{if isset($aModule.data.n_email)} value="{$aModule.data.n_email}"{/if}{if $aModule.action eq 'edit'} readonly="readonly"{/if} /></div>
				<div class="clear"></div>
			</div>			
      <div>
				<div style="float: left;"><input type="checkbox" name="n_regulations" id="nf_regulations" value="1" {if $aModule.action eq 'edit'} readonly="readonly"{/if} /></div>
        <div style="color: grey; padding-top: 5px;"> <label for="nf_regulations">{$aLang.common.regulations} <span class="required"></span></label></div>
				<div class="clear"></div>
			</div>			
{*			<div class="fTextRow" style="padding: 10px 0 0 0;"><strong>X{$aModule.lang.categories}</strong> <span class="required">*</span></div>
			<div class="newsletterGroup" style="padding-left: 15px;">
			{foreach from=$aModule.categories_groups key=iGId item=aGroup}
				{literal}
					<script type="text/javascript">
					  $(document).ready(function(){
					    
					    $(".newsletterGroupName_{/literal}{$iGId}{literal}").click(function () {
					      $(".categories_{/literal}{$iGId}{literal}").slideToggle("slow");
					    });

					  });
					</script>
				{/literal}
				{if isset($aModule.categories[$iGId])}
					{if $aGroup.show_name eq '1'}<div class="newsletterGroupName newsletterGroupName_{$iGId}">{$aGroup.name}</div>{/if}
					<ul class="categories categories_{$iGId}">
						{foreach from=$aModule.categories[$iGId] item=cat}						
							<li>
								<div class="fCheckbox"><input type="checkbox" name="n_categories[{$cat.value}]" value="1" id="f_cat_{$cat.value}" class="check"{if isset($aModule.data.n_categories[$cat.value])}{$aModule.data.n_categories[$cat.value]}{/if} /></div>
								<div class="fChkLabel"><label for="f_cat_{$cat.value}">{$cat.label}</label></div>
								<div class="clear"></div>
							</li>
						{/foreach}
					</ul>
				{/if}
				<div class="clear"></div>
			{/foreach}
			</div>
	*}
  <br />
	<div class="koszykPodtytulNewsletter">Subskrypcja newsletter-a:</div>
	<div class="fTextRow">
		<div class="newsletterGroup">
		{foreach from=$aModule.categories_groups key=iGId item=aGroup}
			
		{assign var=ilosc value=$aModule.items.items|@count}
		{assign var=linia value=$ilosc/3+1|string_format:"%d"}
			
			{if isset($aModule.categories[$iGId])}
				{if $aGroup.show_name eq '1'}<div class="koszykPodtytul2 newsletterGroupName newsletterGroupName_{$iGId}">{$aGroup.name}</div>{/if}
				<div class="clear"></div>	
				<ul class="categories categories_{$iGId}">
					{foreach from=$aModule.categories[$iGId] item=cat name=news_list}
						{if $cat.forced == '1'}
							<input type="hidden" name="n_categories[{$cat.value}]" value="1" />
						{else}	
							<li>
								<div class="fCheckbox"><input type="checkbox" name="n_categories[{$cat.value}]{if $cat.forced == '1'}_d{/if}" value="1" id="f_cat_{$cat.value}" class="check"{if $cat.forced == '1'} checked="checked" disabled="disabled"{else}{if isset($aModule.n_categories[$cat.value])}{$aModule.n_categories[$cat.value]}{/if}{/if} /></div>
								<div class="fChkLabel"><label for="f_cat_{$cat.value}">{$cat.label}</label></div>
								<div class="clear"></div>
							</li>
							{if $i mod $linia eq $linia-1 && !$smarty.foreach.news_list.last}</ul><ul class="categories categories_{$iGId}">{/if}
						{/if}
					{/foreach}
				</ul>
			{/if}
			<div class="clear"></div>
		{/foreach}
		</div>
	<div class="clear"></div>
</div>		
			<div class="clear"></div>
			<div class="fRequiredF" style="padding-top: 15px; padding-bottom: 10px;">{$aLang.form.required_fields}</div>
			<div class="clear"></div>
			<div class="fSendButton" style="float: right; padding-right: 15px;">
				<input type="submit" name="send" value="{$aModule.sendbutton}" class="sendBig" />
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>