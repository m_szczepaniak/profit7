{if !empty($aModule.results)}
<div style="position: relative; z-index: 1;">
	<div id="sortowanie">
			<form action="{$aModule.page_link}" method="get" id="sorterFilterSearchForm">
				<div style="float: left;">
					<span>{$aLang.common.filter}</span>
					<select name="filter">
						{foreach from=$aLang.common.filter_links key=k item=v}
							<option value="{$k}"{if isset($smarty.get.filter)}{if $k eq $smarty.get.filter} selected="selected"{/if}{else}{if $k eq 'avaible'} selected="selected"{/if}{/if}>{$v}</option>
						{/foreach}
					</select>
				</div>
				<div style="float: left; margin-left: 8px;">
					<span>{$aLang.common.sort}</span>
					<select name="sort">
					{foreach from=$aLang.common.sorter_links key=k item=v}
						<option value="{$k}"{if isset($smarty.get.sort)}{if $k eq $smarty.get.sort} selected="selected"{/if}{else}{if $k eq 'data'} selected="selected"{/if}{/if}>{$v}</option>
					{/foreach}
					</select>
				</div>	
			</form>
	</div>
</div>
{else}
<div style="position: relative; z-index: 1;">
	<div id="sortowanie">
			<form action="{$aModule.page_link}" method="get" id="FilterSearchForm">
				<div style="float: left;">
					<span>{$aLang.common.filter}</span>
					<select name="filter">
						{foreach from=$aLang.common.filter_links key=k item=v}
							<option value="{$k}"{if isset($smarty.get.filter)}{if $k eq $smarty.get.filter} selected="selected"{/if}{else}{if $k eq 'avaible'} selected="selected"{/if}{/if}>{$v}</option>
						{/foreach}
					</select>
				</div>
			</form>
	</div>
</div>
{/if}	
<div id="wyszukiwarkaInfo">
		{if !empty($aModule.asterisk_suggestion)}
			<div id="wyszukiwarkaInfoGwiazdka">
				<div id="wyszukiwarkaInfoGwiazdkaTop"></div>
				<div id="wyszukiwarkaInfoGwiazdkaCenter">
					<div id="wyszukiwarkaInfoGwiazdkaText">{$aModule.asterisk_suggestion}</div>
				</div>
				<div id="wyszukiwarkaInfoGwiazdkaBottom"></div>
			</div>
		{/if}		
		
		{if $aModule.message ne ''}
			<div id="wyszukiwarkaInfoFraza">
				{*<div>{$aModule.lang.criteria_search}</div>*}
				{$aModule.message}
			</div>
		{/if}

		{if $aModule.message_found ne ''}
			<div id="wyszukiwarkaInfoPozycje">
				<div style="float: left; font-weight: normal;">{$aModule.lang.found_items}&nbsp;</div>
				{$aModule.message_found}
			</div>
		{/if}
</div>

{if !empty($aModule.return_link)}
		<a class="powrotX" href="{$aModule.return_link}">&laquo; {$aModule.lang.back_to}</a>
{/if}
<div class="produktListaSeparator"></div>
<div class="clear" style="height: 10px;"></div>
{if isset($aModule.pager.links) && !empty($aModule.pager.links)}
	<div class="pagerLinks">
		{$aModule.pager.links}
		<div class="clear"></div>
		<span class="opis">[{$aLang.pager.current_page} {$aModule.pager.current_page} {$aLang.pager.from} {$aModule.pager.total_pages} - {$aLang.pager.total_items} {$aModule.pager.total_items}]</span>
	</div>
{/if}
<div class="produktListaSeparator"></div>
{foreach from=$aModule.results key=i item=item name=results}
	<div class="produktLista">
		<div class="produktListaZdjecie">
			<a href="{$item.link}">{if $item.image[0].thumb != ''}<img src="{$item.image[0].thumb}" alt="{$item.plain_name}" />{else}<img src="/images/gfx/brak.png" alt="" />{/if}</a>
		</div>
		
		<div class="left">
			<div class="produktListaTytul">
				<a href="{$item.link}">{$item.title}</a>{if !empty($item.name2)}<br/>
						{$item.name2}
						{/if}
			</div>
			
			{if $item.is_news=='1' || $item.is_bestseller=='1'}
			<div class="clear"></div>
			<div class="produktListaOpcja">
				{if $item.is_bestseller=='1'}<div class="produktListaOpcjaBestseller"></div>{/if}
				{if $item.is_news=='1' && $item.is_bestseller=='1'}<div class="produktListaOpcjaSeparator"></div>{/if}
				{if $item.is_news=='1'}<div class="produktListaOpcjaNews"></div>{/if}
			</div>
			<div class="clear"></div>
			{/if}	
			
			{if !empty($item.publication_year)}
			<div class="produktListaRok">
				{$aModule.lang.publication_year} <span>{$item.publication_year}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				{if !empty($item.edition)}{$aModule.lang.edition}: <span>{$item.edition}</span>{/if}
			</div>
			{/if}
			{if !empty($item.isbn) && $item.isbn != '000-0-00-000000-0'}
			<div class="produktListaIsbn">
				 {$aModule.lang.isbn} <span>{$item.isbn}</span>
			</div>
			{/if}
			{if !empty($item.publisher_name)}
			<div class="produktListaWydawnictwo">
				{$aModule.lang.publisher} <span><a href="{get_publisher_link string=$item.publisher_name}">{$item.publisher_name}</a></span>
			</div>
			{/if}
			{if !empty($item.series)}
			<div class="produktListaSerie">
				{$aModule.lang.series}
						<span><a href="{get_series_link publisher=$item.publisher_name series=$item.series}">{$item.series}</a></span>
			</div>
			{/if}
			{if !empty($item.authors)}	
				<div class="produktListaAutorzy">
					{foreach from=$item.authors key=rid item=aRole}
						{if !empty($aRole)}
							{$rid}:<span>
							{foreach from=$aRole key=id item=aAuthor name=prod_series}
								<a href="{$aAuthor.link}">{$aAuthor.name}{if !$smarty.foreach.prod_series.last}, {/if}</a>
							{/foreach}
							</span>
							{if !$smarty.foreach.auth_roles.last} {/if}
						{/if}
					{/foreach}
				</div>
			{/if}
		</div>
		
		<div class="right">
			<div class="produktListaCzas">
				{if ($item.prod_status !== '1')}&nbsp;{else}{$aModule.lang.shipment}: <span>{$item.shipment}</span>{/if}
			</div>
			<div class="produktListaCeny">
				{if $item.price_brutto ne '0,00'}
					{if $item.shipment_time!='3'}
						{if $item.promo_price > 0}
							{$aLang.common.old_price} <span>{$item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency} zł</span><br />
							<strong>{$item.promo_text} <span>{$item.promo_price|format_number:2:',':' '}{$aModule.lang.currency} zł</span></strong>
						{else}
							<strong>{$aLang.common.cena_w_ksiegarni} <span>{$item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency} zł</span></strong>	
						{/if}
					{/if}
				{/if}
			</div>
			<div class="produktListaPrzyciski">
				{if ($item.shipment_time == '3')}
							<span>na zamówienie</span>
				{elseif ($item.prod_status == '1')}
					{strip}
          <a href="javascript:void(0);" 
             onclick="addCardShowPopup(
                          {$item.id}, 
                          '{$item.link}', 
                          '{$item.name|replace:"'":"&#039;"|replace:"\n":""|htmlspecialchars}',
                          {if $item.type=='1'}'audio'{else}'print'{/if}, 
                          {if $item.image[0].thumb != ''}'{$item.image[0].thumb}'{else}'/images/gfx/brak.png'{/if});" 
              title="Złóż zamówienie" 
              class="przyciskKoszyk"></a>
          {/strip}
				{else}
					<span>niedostępna</span>
				{/if}		
				<a href="{$item.repository_link}" title="{$aBox.lang.przechowalnia}" class="przyciskSchowek"></a>
			</div>
		</div>
	</div>
	{if !$smarty.foreach.results.last}
		<div class="produktListaSeparator"></div>
	{/if}
{/foreach}

{if isset($aModule.pager.links) && !empty($aModule.pager.links)}
	<div class="pagerLinks">
		{$aModule.pager.links}
		<div class="clear"></div>
		<span class="opis">[{$aLang.pager.current_page} {$aModule.pager.current_page} {$aLang.pager.from} {$aModule.pager.total_pages} - {$aLang.pager.total_items} {$aModule.pager.total_items}]</span>
	</div>
{/if}