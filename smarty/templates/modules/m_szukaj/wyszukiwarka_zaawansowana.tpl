{if isset($smarty.get.result)}
	{include file=$aModule.results_template}
{else}
<div class="searchForm">
	<form action="{$aModule.page_link}" method="get" name="searchForm" id="searchForm">
		<strong>{$aModule.lang.phrase}</strong>
		<input type="text" name="q" id="qi" class="text" style="width: 280px;" value="{if isset($smarty.get.q)}{$smarty.get.q|stripslashes}{/if}" class="inputText" />
		<div style="padding: 5px 0 0 0;">
			<input type="checkbox" name="aval" id="aval" {if isset($smarty.get.aval)}checked="checked"{/if} /><label for="aval" style="margin: 4px 0 0 5px;">{$aModule.lang.search_aval}</label>
		</div>

		<div class="clear" style="height: 25px;"></div>
		<div class="fRow">
			<div class="label">{$aModule.lang.top_cats}: </div>
			<select name="top_cat" id="top_cat_ajx" class="text">
				{foreach from=$aModule.top_cats item=tcat}
					<option value="{$tcat.value}" {if isset($smarty.get.top_cat)&&($smarty.get.top_cat==$tcat.value)} selected="selected"{/if}>{$tcat.label}</option>
				{/foreach}
			</select>
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.sub_cats}: </div>
			<select name="cat" id="cats_ajx" class="text"{if empty($aModule.sub_cats)} disabled="disabled"{/if}>
				{if !empty($aModule.sub_cats)}
					{foreach from=$aModule.sub_cats item=scat}
						<option value="{$scat.value}" {if isset($smarty.get.cat)&&($smarty.get.cat==$scat.value)} selected="selected"{/if}>{$scat.label}</option>
					{/foreach}
				{/if}
			</select>
		</div>
		<input type="hidden" name="adv" id="adv" value="1" />
		<input type="hidden" name="result" id="result1" value="1" />

		<div class="fRow">
			<div class="label">{$aModule.lang.author}</div><input type="text" name="autor" id="autor" class="text" value="{if isset($smarty.get.autor)}{$smarty.get.autor|stripslashes}{/if}" class="inputText" />
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.year}</div>
			<select name="year" id="year" class="text">
				{foreach from=$aModule.years item=year}
					<option value="{$year.value}" {if isset($smarty.get.year)&&($smarty.get.year==$year.value)} selected="selected"{/if}>{$year.label}</option>
				{/foreach}
			</select>
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.publisher}</div><input type="text" name="publisher" class="text" id="publisher_aci" value="{if isset($smarty.get.publisher)}{$smarty.get.publisher|stripslashes}{/if}" class="inputText" />
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.series}</div>
			<select name="series" class="text" id="series_aci" {if empty($aModule.curr_series)} disabled="disabled"{/if}>
			{if !empty($aModule.curr_series)}
				{foreach from=$aModule.curr_series item=cs}
					<option value="{$cs.value}" {if isset($smarty.get.series)&&($smarty.get.series==$cs.value)} selected="selected"{/if}>{$cs.label}</option>
				{/foreach}
			{/if}
			</select>
			{*<input type="text" name="series" class="text" id="series_aci" value="{if isset($smarty.get.series)}{$smarty.get.series|stripslashes}{/if}" class="inputText" />*}
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.tag}</div><input type="text" name="tag" class="text" id="tag_aci" value="{if isset($smarty.get.tag)}{$smarty.get.tag|stripslashes}{/if}" class="inputText" />
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.isbn}</div><input type="text" name="isbn" id="isbn" class="text" value="{if isset($smarty.get.isbn)}{$smarty.get.isbn|stripslashes}{/if}" class="inputText" />
		</div>
		<div class="fRow">
			<div class="label">{$aModule.lang.price_from}</div>
			<input type="text" name="price_from" class="text" style="width: 70px; margin-right: 7px;" id="price_from" value="{if isset($smarty.get.price_from)}{$smarty.get.price_from}{/if}" class="inputText" />
			{$aModule.lang.price_to}
			<input type="text" name="price_to" class="text" style="width: 70px; margin-left: 7px;" id="price_to" value="{if isset($smarty.get.price_to)}{$smarty.get.price_to}{/if}" class="inputText" />
		</div>

		<div class="clear" style="height: 20px;"></div>
		<input style="float: right;" type="submit" class="sendBig" value="szukaj" />

		<div class="clear"></div>
		{*if $aModule.adv eq '1'}
			<span class="label">&nbsp;</span><a href="/szukaj/{$aModule.mod_link}" class="hideAdv">{$aModule.lang.hide_adv}</a>
		{else}
			<span class="label">&nbsp;</span><a href="/szukaj/{$aModule.mod_link}" class="showAdv">{$aModule.lang.show_adv}</a>
		{/if*}
	</form>
</div>
{/if}