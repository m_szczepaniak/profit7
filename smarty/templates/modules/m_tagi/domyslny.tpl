{foreach from=$aModule.items.tags item=tag}
	{$tag.tag}
{/foreach}
<div class="clear"></div>
{if isset($aModule.items.pager.links) && !empty($aModule.items.pager.links)}
	{*<div class="pagerLinks">
		{$aModule.items.pager.links}&nbsp;&nbsp;&nbsp;&nbsp;[{$aLang.pager.current_page} {$aModule.items.pager.current_page} {$aLang.pager.from} {$aModule.items.pager.total_pages} - {$aLang.pager.total_items} {$aModule.items.pager.total_items}]
	</div>*}
	<div class="pagerLinks">
		{$aModule.items.pager.links}
	</div>
{/if}