<div class="autorzyLitery">
	<ul>
		{foreach from=$aModule.letters key=k item=letter name=lista}
			<li {if $smarty.foreach.lista.last}style=" padding: 0;" {/if}{if isset($letter.link)}{/if}>{if isset($letter.link)}<a href="{$letter.link}" {if $letter.letter eq $aModule.items.letter|upper}class="active"{/if}>{$letter.letter|upper}</a>{else}{$letter.letter|upper}{/if}</li>
		{/foreach}
	</ul>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="autorzySpis">
{assign var=ilosc value=$aModule.items.items|@count}
{assign var=linia value=$ilosc/3+1|string_format:"%d"}
	{if isset($aModule.items.items)}
		<ul>
			{foreach from=$aModule.items.items key=i item=aItem name=news_list}
				<li><a href="{$aItem.link}">{$aItem.name}</a></li>
				{if $i mod $linia eq $linia-1 && !$smarty.foreach.news_list.last}</ul><ul>{/if}
			{/foreach}
		</ul>
	{/if}
	<div class="clear"></div>
</div>

<div class="clear"></div>
{if isset($aModule.items.pager.links) && !empty($aModule.items.pager.links)}
	<div class="pagerLinks">
		{$aModule.items.pager.links}
		<div class="clear"></div>
		<span class="opis">[{$aLang.pager.current_page} {$aModule.items.pager.current_page} {$aLang.pager.from} {$aModule.items.pager.total_pages} - {$aModule.lang.pager.total_items} {$aModule.items.pager.total_items}]</span>
	</div>
{/if}