<div class="indeksSerii indeksSeriiPoziom">
	<ul>
		<li>
			<div class="calosc">
				{if !empty($aModule.items.publisher.photo)}
					<div class="nazwa"><img src="/images/publishers/{$aModule.items.publisher.photo}" alt="{$aModule.items.publishername}" /></div>
				{/if}
				<div class="nazwa">{$aModule.items.publisher.name}</div>				
			</div>	
		</li>
	</ul>
<div class="clear"></div>

{if !empty($aModule.items.items)}
	<div class="serie">
		{assign var=ilosc value=$aModule.items.items|@count}
		{assign var=linia value=$ilosc/4+1|string_format:"%d"}
		<ul>
			{foreach from=$aModule.items.items key=i item=item name=items_list}
				<li><a href="{$item.link}"{if $item.name|count_characters>21} onmouseover="showhint('{$item.name}', this, event, '220px');"{/if}>{if $item.name|count_characters>21}{$item.name|truncate:21:"...":true}{else}{$item.name}{/if}</a></li>
				{if $i mod $linia eq $linia-1 && !$smarty.foreach.news_list.last}</ul><ul>{/if}
			{/foreach}
		</ul>
		<div class="clear"></div>
	</div>
	{/if}
	<div class="clear"></div>
	{if isset($aModule.items.pager.links) && !empty($aModule.items.pager.links)}
		<div class="pagerLinks">
			{$aModule.items.pager.links}
			<div class="clear"></div>
			<span class="opis">[{$aLang.pager.current_page} {$aModule.items.pager.current_page} {$aLang.pager.from} {$aModule.items.pager.total_pages} - {$aModule.lang.pager.total_items} {$aModule.items.pager.total_items}]</span>
		</div>
	{/if}
</div>