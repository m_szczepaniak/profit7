<div class="indeksSerii">
	{if isset($aModule.items.items)}
		<ul>
			{foreach from=$aModule.items.items key=i item=aItem name=news_list}
				<li>
				<div class="calosc">
				{if !empty($aItem.photo)}
					<a href="{$aItem.link}" class="zdjecie"><img src="/images/publishers/{$aItem.photo}" alt="{$aItem.name}" /></a><div class="clear"></div><a href="{$aItem.link}" class="nazwa">{$aItem.name}</a>
				{else}
					<a href="{$aItem.link}" class="zdjecie">{$aItem.name}</a>
				{/if}
				</div>	
				</li>
			{/foreach}
		</ul>
	{/if}
	<div class="clear"></div>
</div>

<div class="clear"></div>
{if isset($aModule.items.pager.links) && !empty($aModule.items.pager.links)}
	<div class="pagerLinks">
		{$aModule.items.pager.links}
		<div class="clear"></div>
		<span class="opis">[{$aLang.pager.current_page} {$aModule.items.pager.current_page} {$aLang.pager.from} {$aModule.items.pager.total_pages} - {$aLang.pager.total_items} {$aModule.items.pager.total_items}]</span>
	</div>
{/if}

{*
<div class="indeksSerii">
{assign var=ilosc value=$aModule.items.items|@count}
{assign var=linia value=$ilosc/4+1|string_format:"%d"}
	{if isset($aModule.items.items)}
		<ul>
			{foreach from=$aModule.items.items key=i item=aItem name=news_list}
				<li>
				<div class="calosc">
				{if !empty($aItem.photo)}
					<a href="{$aItem.link}" class="zdjecie"><img src="/images/publishers/{$aItem.photo}" alt="{$aItem.name}" /></a><div class="clear"></div><a href="{$aItem.link}" class="nazwa">{$aItem.name}</a>
				{else}
					<a href="{$aItem.link}" class="zdjecie">{$aItem.name}</a>
				{/if}
				</div>
				</li>
				{if $i mod $linia eq $linia-1 && !$smarty.foreach.news_list.last}</ul><ul>{/if}
			{/foreach}
		</ul>
	{/if}
	<div class="clear"></div>
</div>

<div class="clear"></div>
{if isset($aModule.items.pager.links) && !empty($aModule.items.pager.links)}
	<div class="pagerLinks">
		{$aModule.items.pager.links}
		<div class="clear"></div>
		<span class="opis">[{$aLang.pager.current_page} {$aModule.items.pager.current_page} {$aLang.pager.from} {$aModule.items.pager.total_pages} - {$aLang.pager.total_items} {$aModule.items.pager.total_items}]</span>
	</div>
{/if}
*}