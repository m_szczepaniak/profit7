<div style="position: relative; z-index: 1;">
	<div id="sortowanie">
		<form action="{$aModule.page_link}" method="get" id="sorterForm">
		{if isset($smarty.get.p)}
			<input type="hidden" name="current_page" value="{$smarty.get.p}" />
		{/if}
			<div style="float: left;">
				<span>{$aLang.common.filter}</span>
				<select name="filter">
					{foreach from=$aLang.common.filter_links key=k item=v}
						<option value="{$k}"{if isset($smarty.get.filter)}{if $k eq $smarty.get.filter} selected="selected"{/if}{else}{if $k eq 'avaible'} selected="selected"{/if}{/if}>{$v}</option>
					{/foreach}
				</select>
			</div>
			<div style="float: left; margin-left: 8px;">
				<span>{$aLang.common.sort}</span>
				<select name="sort">
				{foreach from=$aLang.common.sorter_links key=k item=v}
					<option value="{$k}"{if isset($smarty.get.sort)}{if $k eq $smarty.get.sort} selected="selected"{/if}{else}{if $k eq 'data'} selected="selected"{/if}{/if}>{$v}</option>
				{/foreach}
				</select>
			</div>	
		</form>
	</div>
</div>

{if $aModule.offer.items|@count > 0}
	{if !empty($aModule.offer.items)}
		
		{if isset($aModule.offer.pager.links) && !empty($aModule.offer.pager.links)}
			<div class="pagerLinks pagerLinksTop">
				{$aModule.offer.pager.links}
				<div class="clear"></div>
				<span class="opis">[{$aLang.pager.current_page} {$aModule.offer.pager.current_page} {$aLang.pager.from} {$aModule.offer.pager.total_pages} - {$aLang.pager.total_items} {$aModule.offer.pager.total_items}]</span>
			</div>
		{/if}	
		<div class="produktListaSeparator"></div>

		{foreach from=$aModule.offer.items key=i item=item name=items}
			<div class="produktLista">
				<div class="produktListaZdjecie">
					<a href="{$item.link}">{if $item.image[0].thumb != ''}<img src="{$item.image[0].thumb}" alt="{$item.plain_name}" />{else}<img src="/images/gfx/brak.png" alt="" />{/if}</a>
				</div>
				
				<div class="left">
					<div class="produktListaTytul">
						<a href="{$item.link}">{$item.name}</a>
					</div>
					
					{if $item.is_news=='1' || $item.is_bestseller=='1'}
					<div class="clear"></div>
					<div class="produktListaOpcja">
						{if $item.is_bestseller=='1'}<div class="produktListaOpcjaBestseller"></div>{/if}
						{if $item.is_news=='1' && $item.is_bestseller=='1'}<div class="produktListaOpcjaSeparator"></div>{/if}
						{if $item.is_news=='1'}<div class="produktListaOpcjaNews"></div>{/if}
					</div>
					<div class="clear"></div>
					{/if}				
					
					{if !empty($item.publication_year)}
					<div class="produktListaRok">
						{$aModule.lang.publication_year} <span>{$item.publication_year}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						{if !empty($item.edition)}{$aModule.lang.edition} <span>{$item.edition}</span>{/if}
					</div>
					{/if}
					{if !empty($item.isbn) && $item.isbn != '000-0-00-000000-0'}
					<div class="produktListaIsbn">
						{$aModule.lang.isbn} <span>{$item.isbn}</span>
					</div>
					{/if}
					{if !empty($item.publisher.name)}
					<div class="produktListaWydawnictwo">
						{$aModule.lang.publisher} <span><a href="{get_publisher_link string=$item.publisher.name}">{$item.publisher.name}</a></span>
					</div>
					{/if}
					{if !empty($item.series)}
					<div class="produktListaSerie">
						{$aModule.lang.series}
						{foreach from=$item.series key=id item=aSeria name=prod_series}
							<span><a href="{get_series_link publisher=$item.publisher_name series=$aSeria.name}">{$aSeria.name}</a></span>
							{if !$smarty.foreach.prod_series.last}, {/if}
						{/foreach}
					</div>
					{/if}
					{if !empty($item.authors)}	
						<div class="produktListaAutorzy">
							{foreach from=$item.authors key=rid item=aRole}
								{if !empty($aRole)}
									{$rid}:<span>
									{foreach from=$aRole key=id item=aAuthor name=prod_series}
										<a href="{$aAuthor.link}">{$aAuthor.name}{if !$smarty.foreach.prod_series.last}, {/if}</a>
									{/foreach}
									</span>
									{if !$smarty.foreach.auth_roles.last} {/if}
								{/if}
							{/foreach}
						</div>
					{/if}
				</div>
				
				<div class="right">
					<div class="produktListaCzas">
						{if ($item.prod_status !== '1')}&nbsp;{else}{$aModule.lang.shipment}: <span>{$item.shipment}</span>{/if}
					</div>
					<div class="produktListaCeny">
						{if $item.price_brutto ne '0,00' && $item.shipment_time!=3}
							{if $item.promo_price > 0}
								{$aLang.common.old_price} <span>{$item.price_brutto|format_number:2:',':' '}{$aLang.common.currency}</span><br />
								<strong>{$item.promo_text} <span>{$item.promo_price|format_number:2:',':' '}{$aLang.common.currency}</span></strong>
							{else}
								<strong>{$aLang.common.cena_w_ksiegarni} <span>{$item.price_brutto|format_number:2:',':' '}{$aLang.common.currency}</span></strong>	
							{/if}
						{/if}
					</div>
					<div class="produktListaPrzyciski">
						{if ($item.shipment_time == '3')}
							<span>tylko na zamówienie</span>
						{elseif ($item.prod_status == '1')}
              {strip}
							<a href="javascript:void(0);" 
                 onclick="addCardShowPopup(
                              {$item.id}, 
                              '{$item.link}', 
                              '{$item.name|replace:"'":"&#039;"|replace:"\n":""|htmlspecialchars}',
                              {if $item.type=='1'}'audio'{else}'print'{/if}, 
                              {if $item.image[0].thumb != ''}'{$item.image[0].thumb}'{else}'/images/gfx/brak.png'{/if});" 
                  title="Złóż zamówienie" 
                  class="przyciskKoszyk"></a>
              {/strip}
						{elseif ($item.prod_status == '2')}
							<span>nakład wyczerpany</span>						
						{else}
							<span>niedostępna</span>
						{/if}
						<a href="{$item.repository_link}" title="{$aBox.lang.przechowalnia}" class="przyciskSchowek"></a>
					</div>
				</div>
			</div>
			{if !$smarty.foreach.items.last}
				<div class="produktListaSeparator"></div>
			{/if}
		{/foreach}

		<div class="clear"></div>
		{if isset($aModule.offer.pager.links) && !empty($aModule.offer.pager.links)}
			<div class="pagerLinks">
				{$aModule.offer.pager.links}
				<div class="clear"></div>
				<span class="opis">[{$aLang.pager.current_page} {$aModule.offer.pager.current_page} {$aLang.pager.from} {$aModule.offer.pager.total_pages} - {$aLang.pager.total_items} {$aModule.offer.pager.total_items}]</span>
			</div>
		{/if}
	{/if}
{else}
	<span style="padding-left: 10px;">{$aLang.common.no_positions}</span>
{/if}