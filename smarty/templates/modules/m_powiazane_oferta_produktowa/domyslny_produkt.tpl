{if $smarty.session.wu_cart.just_added ne '1'}
	<div class="itemImage">
		{if !empty($aModule.item.image.big)}
			<a href="{$aModule.item.image.big}" rel="lytebox['photos']" target="_blank"><img src="{$aModule.item.image.thumb}" alt="{$aModule.item.plain_name}"/></a>
		{else}
			<img src="{$aModule.item.image.thumb}" alt=""/>
		{/if}
	</div>
	<div class="itemDetails">
		<h1 class="itemTitle">{$aModule.item.name}</h1>
		{if $aModule.item.prod_status eq '2'}<div style="float: left; margin-left: 5px;"><img src="/images/gfx/pl/nowosc.gif" alt="nowosc"/></div>{/if}
		{if $aModule.item.prod_status eq '3'}<div style="float: left; margin-left: 5px;"><img src="/images/gfx/pl/zapowiedz.gif" alt="zapowiedz"/></div>{/if}
	
		<div class="clear"></div>
	
		{if isset($aModule.item.issue)}<span class="issue">{$aModule.lang.issue}{$aModule.item.issue}</span>{/if}
	
		<div class="itemDetailsSpacer"></div>
		<div class="itemDetailsFiles">
			{if isset($aModule.item.files)}
				<div class="itemDetailsFilesTop"></div>
				<div class="itemDetailsFilesMid">
				{$aModule.lang.download}
					<ul>
						{foreach from=$aModule.item.files key=k item=file}
							<li><a href="{$file.link}" target="_blank">{$aModule.lang.ftype[$file.ftype]}</a></li>
						{/foreach}
					</ul>
				</div>
				<div class="itemDetailsFilesBottom"></div>
			{/if}
	
		</div>
		<div style="float: left; width: 341px;">
		<div class="itemDetailsAuthors">
		{foreach from=$aModule.item.authors key=rola item=authors}
			<div class="authors">
				{if $rola eq 'autor' and $authors|@count gt 1}
					{$aLang.common.authors}
				{else}
					{$rola}: 
				{/if}
				{foreach from=$authors item=author name=prod_authors}
					<a href="{$author.link}">{$author.name}	</a>{if !$smarty.foreach.prod_authors.last}, {/if}
				{/foreach}
			</div>
		{/foreach}
		</div>
	
		{if !empty($aModule.item.packet)}
			{$aModule.lang.in_packet}
			<ul class="in_packet">
				{foreach from=$aModule.item.packet key=k item=packet name=pack}
					<li><a href="#{$k+1}">{$packet.name}</a></li>
				{/foreach}
			</ul>
		{/if}
		<div class="clear"></div>
		{if isset($aModule.item.tags)}
			<div style="margin-top: 20px;">{$aModule.lang.tags}
				{foreach from=$aModule.item.tags key=i item=tag name=prod_tags}
					<a href="{$tag.link}">{$tag.tag}</a>{if !$smarty.foreach.prod_tags.last}, {/if}
				{/foreach}
			</div>
		{/if}
	
		<div class="itemDetailsInfo">
			{if isset($aModule.item.publication_year)}{$aModule.lang.publication_year}<span>{$aModule.item.publication_year}</span><br />{/if}
			{if isset($aModule.item.pages) and $aModule.item.pages ne '0'}{$aModule.lang.pages}<span>{$aModule.item.pages}</span><br />{/if}
			{if isset($aModule.item.isbn)}{$aModule.lang.isbn}<span>{$aModule.item.isbn}</span><br />{/if}
			{if isset($aModule.item.series_name)}{$aLang.common.series_name}<span><a href="{$aModule.item.series_link}" class="non-bold">{$aModule.item.series_name}</a></span><br />{/if}
			{if !empty($aModule.item.index)}{$aModule.lang.index}
				{foreach from=$aModule.item.index item=index name=indeksy}
					<span><a href="{$index.index_link}" class="non-bold">{$index.name}</a></span>{if !$smarty.foreach.indeksy.last}, {/if}
				{/foreach}
			{/if}
		</div>
		
		<div class="itemDetailsPrice">
			{if $aModule.item.no_price_text eq '' && $aModule.$item.shipment_time!=3}
				{if $aModule.item.promo_price_brutto ne '0,00'}
					<span class="old_price">{$aModule.lang.old_price}{$aModule.item.price_brutto}{$aModule.lang.currency}</span><br />
					{$aModule.lang.price}<span class="price">{$aModule.item.promo_price_brutto}{$aModule.lang.currency}</span>	
				{else}
					{if $aModule.item.promocyjna > 0}
						<span class="old_price">{$aModule.lang.old_price}{$aModule.item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span><br />	
						{$aModule.lang.price}<span class="price">{$aModule.item.promocyjna|format_number:2:',':' '}{$aModule.lang.currency}</span>	
					{else}
						{$aModule.lang.price}<span class="price">{$aModule.item.price_brutto|format_number:2:',':' '}{$aModule.lang.currency}</span>	
					{/if}
					{if $aModule.item.user_price > 0 and $aModule.item.user_price ne $aModule.item.price_brutto}
						<br />{$aLang.common.price_for_you}<span class="discount">{$aModule.item.user_price|format_number:2:',':' '}{$aLang.common.currency}</span>
					{/if}
				{/if}
			{else}
				{$aModule.item.no_price_text}
			{/if}
		</div>
			{if $aModule.item.prod_status ne '0' and $aModule.item.prod_status ne '3' and $aModule.item.no_price_text eq ''}
				<div class="itemAddToCart"><a href="{$aModule.item.cart_link}">{$aModule.lang.add}</a></div>
			{/if}	
		<div class="clear"></div>
		<div class="itemDetailsPriceOptions">
			{if $aModule.item.prod_status ne '0' and $aModule.item.prod_status ne '3' and $aModule.item.no_price_text eq ''}
				<span class="przechowalnia"><a href="{$aModule.item.repository_link}">{$aModule.lang.przechowalnia}</a></span>
			{/if}
			<span class="powiadom"><a href="javascript:void(0);" onclick="javascript:window.open('/polec-znajomemu/id{$aModule.item.id}.html', 'powiadom', 'width=520,height=460,scrollbars=no');">{$aModule.lang.powiadom}</a></span>
			{if $aModule.item.prod_status eq '0' or $aModule.item.prod_status eq '3'}
				<span class="powiadomienie"><a href="javascript:void(0);" onclick="javascript:showDiv('powiadomienie');">{$aModule.lang.powiadomienie}</a></span>
			{/if}
		</div>
		
		{if isset($aModule.item.prod_status_text)}
			<div class="itemDetailsStatusText">
				{$aModule.item.prod_status_text}
			</div>
		{/if}
	</div>
	</div>
	<div class="clear"></div>
	
	<div style="display: none; margin-top: 15px;" id="powiadomienie">
		<div class="powiadomienie_gora"></div>
			<div class="powiadomienie_m">
				<div class="powiadomienie_m_zamknij"><a href="javascript:void(0);" onclick="javascript:showDiv('powiadomienie');">zamknij</a></div>
				{if $aModule.item.prod_status eq '0'}
					{$aModule.lang.powiadomienie_0}
				{else}
					{$aModule.lang.powiadomienie_3}			
				{/if}
					<div style="margin-top: 7px;">
						<div style="float: left; margin-top: 2px">{$aModule.lang.your_mail}</div>
						<form method="post" action="">
							<input type="hidden" name="p_status" value="{$aModule.item.prod_status}" class="p_mail"/>
							<input type="text" name="p_mail" value="{if isset($smarty.session.w_user.email)}{$smarty.session.w_user.email}{/if}"  class="p_mail" style=" float: left; margin: 0 4px 0 5px;" />
							<input type="image" src="/images/gfx/powiadomienie_wyslij.gif" style=" float: left;" />
						</form>
					</div>
			</div>
		<div class="powiadomienie_dol"></div>	
	</div>
	
	<div class="itemDescription">
		{$aModule.item.description}
	</div>
	
	<div class="clear"></div>
	
	{if !empty($aModule.item.packet)}
		{foreach from=$aModule.item.packet key=k item=packet name=pack}
		<div class="packetItem">
			<div class="packetItemImage">
				<img src="{$packet.image}" alt=""/>
			</div>
			<div class="itemDetails" style="width: 691px;">
				<a name="{$k+1}"></a>
				<h1 class="itemTitle">{$packet.name}</h1>
				{if isset($packet.issue)}<span class="issue">{$aModule.lang.issue}{$packet.issue}</span>{/if}
				{if isset($packet.files)}
					<div class="itemDetailsFiles">
						<div class="itemDetailsFilesTop"></div>
						<div class="itemDetailsFilesMid">
						{$aModule.lang.download}
							<ul>
								{foreach from=$packet.files key=k item=file}
									<li><a href="{$file.link}">{$aModule.lang.ftype[$file.ftype]}</a></li>
								{/foreach}
							</ul>
						</div>
						<div class="itemDetailsFilesBottom"></div>
					</div>
				{/if}
	
				<div class="clear"></div>
	
				<div class="itemDetailsAuthors">
				{foreach from=$packet.authors key=rola item=authors}
					<div class="authors">{$rola|capitalize}: 
						{foreach from=$authors item=author name=prod_authors}
							<a href="{$author.link}">{$author.name}</a>{if !$smarty.foreach.prod_authors.last}, {/if}
						{/foreach}
					</div>
				{/foreach}
				</div>
			
				{if isset($packet.tags)}
					<div style="margin-top: 20px;">{$aModule.lang.tags}
						{foreach from=$packet.tags key=i item=tag name=prod_tags}
							<a href="{$tag.link}">{$tag.tag}</a>{if !$smarty.foreach.prod_tags.last}, {/if}
						{/foreach}
				</div>
				{/if}
	
				<div class="clear"></div>
	
				<div class="itemDetailsPrice">
					{$aModule.lang.normal_price}<span class="normal_price">{$packet.price_brutto}{$aModule.lang.currency}</span>
					{if $packet.discount ne '0'}&nbsp;|&nbsp;W pakiecie taniej o: <span class="discount">19 %</span>{/if}
				</div>
				<div class="clear"></div>
				<div class="itemDetailsInfo">
					{if isset($packet.publication_year)}{$aModule.lang.publication_year}<span>{$packet.publication_year}</span><br />{/if}
					{if isset($packet.pages) and $packet.pages ne '0'}{$aModule.lang.pages}<span>{$packet.pages}</span><br />{/if}
					{if isset($packet.isbn)}{$aModule.lang.isbn}<span>{$packet.isbn}</span>{/if}
				</div>
			</div>
	
			<div class="clear"></div>	
	
			<div class="itemDescription">
				{$packet.description}
			</div>
	
			<div class="clear"></div>
		</div>	
		{/foreach}
	
	{/if}
	
	{if isset($aModule.item.recipients)}
	<div class="naglowekDlugi">{$aModule.lang.recipients}</div>
	<div class="cont">
		{$aModule.item.recipients}
	</div>
	{/if}
	
	{if isset($aModule.item.reviews)}
	<div class="naglowekDlugi">{$aModule.lang.reviews}</div>
	<div class="cont">
		{$aModule.item.reviews}
	</div>
	{/if}
{/if}