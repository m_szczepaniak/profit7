
  {$aModule.form.validator.header}
	{$aModule.form.validator.err_prefix}
	{$aModule.form.validator.err_postfix}
	{$aModule.form.validator.validator}
	{$aModule.form.validator.ajaxval}
  
  <div id="step2" style="width: 788px">
    <div class="registFormBigRegis noRegistratorion" style="margin: auto auto; width: 390px; font-size: 14px; float: none; height: auto;">
      <h2>Wprowadź hasło* oraz adres e-mail, aby: </h2>
        <ul>
          <li>sprawdzić status zamówienia,</li>
          <li>pobrać fakturę, </li> 
          <li>opłacić zamówienie.</li>
        </ul>

      <div style="margin-bottom: 30px; " class="clear"></div>
       
      <div class="fRow">
        <div class="fLabel" style="width: 150px;"><strong>Hasło*:</strong><br />Hasło zamówienia zostało wysłane w emailu po złożeniu zamówienia</div>
        <div class="fInput">{$aModule.form.check_status_key}</div>
      </div>
      <div class="clear"></div>

      <div class="fRow" style="margin-top: 20px;">
        <div class="fLabel" style="width: 150px;"><strong>Adres e-mail:</strong></div>
        <div class="fInput">{$aModule.form.email}</div>
      </div>
      <div class="clear"></div>

		<div style="margin-bottom: 20px;" class="clear"></div>
    
      <div class="buttonDomyslny" style="margin: 0 10px 0 0;">
        <div class="buttonDomyslnyLewo"></div>
        <div class="buttonDomyslnySrodek"><a href="javascript:void(0);" onclick="$('#get_order').submit();">Sprawdź <span style="font-size: 16px">&raquo;</span></a></div>
        <div class="buttonDomyslnyPrawo"></div>
      </div>
      <div class="clear"></div>
      <div style="margin: 20px 0 0 0; font-size: 12px;">
        * Hasło zamówienia zostało wysłane w emailu po złożeniu zamówienia.
      </div>
    </div>
  </div>
  {$aModule.form.validator.footer}
  
  
  <script type="text/javascript">
  {literal}
    $(document).ready(function(){
      /** WALIDACJA **/
      $("#get_order input").blur(function() {
        performFieldAjaxValidation("get_order",this.name,$("#get_order :input").serialize());
      });
      $("#get_order input").click(function() {
        performFieldAjaxValidation("get_order",this.name,$("#get_order :input").serialize());
      });
      $("#get_order").submit(function() {
				performAjaxValidation("get_order", $("#get_order input").serialize());
			  if($("#get_order input:enabled").hasClass("validErr")){
			  	showSimplePopup('Uzupełnij poprawnie formularz!', false, 4000);
			 		return false;
			  } else 
			  	return true;
			});
    });
  {/literal}
  </script>