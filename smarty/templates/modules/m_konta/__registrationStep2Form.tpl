{if !empty($aModule.text)}
	{$aModule.text}
{/if}
{$aModule.form.JS}
<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.ajaxval}
	{if !empty($aModule.form.ref_link)}{$aModule.form.ref_link}{/if}
	
	<div class="koszykPodtytul2">{$aModule.lang.header_vat}</div>	
	<div class="clear"></div>
	<div id="addr1_grp" style="float: left;width: 460px;">
		<div class="fRow fRowType">
			<div class="fLabel">{*{$aModule.form.addr_1.is_company.label}*}{$aModule.lang.faktura_vat}</div>
			<div class="fInput">{$aModule.form.addr_1.is_company.input}</div>
		</div>
		<div class="clear"></div>
		<div class="priv_grp"{if $aModule.form.addr_1.is_company.value == '1'} style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.priv.name.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.name.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.priv.surname.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.surname.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.priv.street.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.street.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini">
				<div class="fLabel">{$aModule.form.addr_1.priv.number.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.number.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini" style="width: 170px;">
				<div class="fLabel" style="width: 60px;">{$aModule.form.addr_1.priv.number2.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.number2.input}</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.priv.postal.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.postal.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_postal}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.priv.city.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.city.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.priv.phone.label}</div>
				<div class="fInput">{$aModule.form.addr_1.priv.phone.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_phone2}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
		</div>
			
		<div class="corp_grp"{if $aModule.form.addr_1.is_company.value == '0'} style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.company.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.company.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.nip.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.nip.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_nip}', this, event, '320px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.street.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.street.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini">
				<div class="fLabel">{$aModule.form.addr_1.corp.number.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.number.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini" style="width: 170px;">
				<div class="fLabel" style="width: 60px;">{$aModule.form.addr_1.corp.number2.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.number2.input}</div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.postal.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.postal.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_postal}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.city.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.city.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.name.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.name.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_name}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.surname.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.surname.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_surname}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_1.corp.phone.label}</div>
				<div class="fInput">{$aModule.form.addr_1.corp.phone.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_phone2}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>			
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.addr_1.name.label}</div>
			<div class="fInput">{$aModule.form.addr_1.name.input}</div>
			<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_addr_name}', this, event, '220px')"></div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="newsletterBox" style="margin: 50px 0 0;">
		<div class="newsletterBoxTop"></div>
		<div class="newsletterBoxContent">
			{*<h3>{$aModule.lang.info_register_box_title}</h3>*}
			{$aModule.lang.info_register_box_content_2}		
		</div>
		<div class="newsletterBoxBot"></div>
	</div>
</div>

<div class="clear" style="height: 8px;"></div>
{*<div class="koszykPodtytul2">{$aModule.lang.header_transport}</div>	
<div class="clear"></div>*}

<div class="fRow fRowType" style="width: 668px;margin-bottom: 10px;">
	<div class="fLabel" style="width: 264px;">{*{$aModule.form.transport_addres.label}*}Domyślny adres dostarczenia przesyłki:</div>
	<div class="fInput" style="margin-top: 8px;">{$aModule.form.transport_addres.input}</div>
</div>

<div id="addr2_grp" class="registrationForm"{if $aModule.form.transport_addres.value == '0'} style="display: none;"{else} style="margin-top: 0;"{/if}>
	<div style="float: left; width: 50%;margin: 0 0 30px;width: 610px;" class="formularz">
		<div class="fRow fRowType" style="width: 600px;">
			<div class="fLabel">{$aModule.form.addr_2.is_company.label}</div>
			<div class="fInput">{$aModule.form.addr_2.is_company.input}</div>
			<div class="clear"></div>
		</div>
		<div class="priv_grp"{if $aModule.form.addr_2.is_company.value == '1'} style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.priv.name.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.name.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_name2}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.priv.surname.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.surname.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_surname2}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.priv.street.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.street.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini">
				<div class="fLabel">{$aModule.form.addr_2.priv.number.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.number.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini" style="width: 170px;">
				<div class="fLabel" style="width: 60px;">{$aModule.form.addr_2.priv.number2.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.number2.input}</div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.priv.postal.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.postal.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_postal}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.priv.city.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.city.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.priv.phone.label}</div>
				<div class="fInput">{$aModule.form.addr_2.priv.phone.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_phone}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
		</div>	
		<div class="corp_grp"{if $aModule.form.addr_2.is_company.value == '0'} style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.company.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.company.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.nip.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.nip.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_nip}', this, event, '320px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.street.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.street.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini">
				<div class="fLabel">{$aModule.form.addr_2.corp.number.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.number.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini" style="width: 170px;">
				<div class="fLabel" style="width: 60px;">{$aModule.form.addr_2.corp.number2.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.number2.input}</div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.postal.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.postal.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_postal}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.city.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.city.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.name.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.name.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_name}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.surname.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.surname.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_surname}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>			
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_2.corp.phone.label}</div>
				<div class="fInput">{$aModule.form.addr_2.corp.phone.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_phone2}', this, event, '220px')"></div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.addr_2.name.label}</div>
			<div class="fInput">{$aModule.form.addr_2.name.input}</div>
			<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_addr_name}', this, event, '160px')"></div>
			<div class="clear"></div>
		</div>		
	</div>
</div>

<div class="clear" style="border-top: 1px solid #eae9e9;height: 16px;"></div>
{if isset($aModule.form.fields.newsletter)}
	{include file=$aModule.form.fields.newsletter.template}
{/if}
{if isset($aModule.form.fields.regulations_agreement)}
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.regulations_agreement.regulations.input}</div>
		<div class="fLabel2">{$aModule.form.fields.regulations_agreement.regulations.label}</div>
		<div class="clear"></div>
	</div>
{/if}

	
	<div class="clear" style="height: 30px;"></div>	
	<div class="fRow" >
		<div class="fInput">{$aModule.lang.privacy_text}</div>
		<div class="clear"></div>
	</div>
	
	{if isset($aModule.form.fields.privacy_agreement)}
		<div class="fRow">
			<div class="fLabel" style="width: 26px;">{$aModule.form.fields.privacy_agreement.privacy.input}</div>
			<div class="fLabel2 left" style="padding-top: 6px">{$aModule.form.fields.privacy_agreement.privacy.label}</div>
			<div class="clear"></div>
		</div>
	{/if}
	
	<div class="clear"></div>	

	<div>
		{$aLang.form.required_fields}
	</div>
	
	<div class="buttonLine">
		<a class="left" href="{$aModule.back_link}" style="background: url('/images/gfx/button-krok1.gif'); width: 215px; height: 33px;" title="Wróć do kroku 1/2"></a>
		<input type="image" src="/images/gfx/button-rejestruj.gif" name="send" class="right" value="Rejestruj" />
		<div class="clear"></div>
	</div>
	
	{$aModule.form.footer}