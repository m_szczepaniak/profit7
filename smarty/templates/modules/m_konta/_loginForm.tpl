<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{if isset($aModule.form.fields.ref.input)}
		{$aModule.form.fields.ref.input}
	{/if}
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.login.label}</div>
		<div class="fInput">{$aModule.form.fields.login.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.passwd.label}</div>
		<div class="fInput">{$aModule.form.fields.passwd.input}</div>
		<div class="clear"></div>
	</div>
	{*<div class="fRow" style="margin: 0;">
		<div class="fLabel"></div>
		<div class="fInput" style="width: 267px;">
	
		</div>
		<div class="clear"></div>
	</div>*}
	<div class="fRow">
		<div class="fLabel">&nbsp;</div>
		<div class="fLabelKonto left">
			<div class="fTextRow2" style="width: 270px;">
				<div style="float: left;margin: 2px 0 0;">
					<a href="{$aModule.links.remind_passwd}" class="login_cart_link">{$aLang.common.remind_passwd}</a> | <a href="{$aModule.links.register}" class="login_cart_link">{$aLang.common.register}</a>
				</div>
				<div style="margin: 2px 0 0 3px;float: right;">{$aModule.form.fields.remember.label}</div>
				<div class="right">{$aModule.form.fields.remember.input}</div>
				<div class="clear"></div>	
			</div>
		</div>
	</div>
	<div class="clear" style="height: 20px;"></div>
		<div class="wymagane">
			{$aLang.form.required_fields}
		</div>
	<div class="fRow" style="float: right; margin-right: 10px; display: inline;">
		<div class="clear" style="height: 10px;"></div>
		<div class="fSendButton" style="float: right;">
			<input type="submit" name="send" class="sendBig" value="Zaloguj" />
		</div>
	</div>
	{$aModule.form.footer}
	<div class="clear"></div>
</div>