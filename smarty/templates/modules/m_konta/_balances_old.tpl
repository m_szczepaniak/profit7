{if $aModule.balances|@count gt 0}
<div id="koszykForm" class="koszykFormZamowienia">
	<table cellspacing="0" cellpadding="0" class="koszyk" id="koszykFormZamowienia">
		<tr>
			<th class="order_lp Center">Lp.</th>
			<th class="order_id Left" style="width: 200px;">{$aModule.lang.description}</th>
			<th class="order_date" style="width: 96px;">{$aModule.lang.order_date}</th>
			<th class="status" style="width: 96px;">{$aModule.lang.order_value}</th>
			<th class="orders_price" style="width: 164px;">{$aModule.lang.payment_form2}</th>
			<th class="details" style="width: 58px;">{$aModule.lang.payment_date}</th>
			<th class="details" style="width: 58px;">{$aModule.lang.paid_amount}</th>
			<th class="details" style="width: 58px;">{$aModule.lang.balance}</th>
		</tr>
		{foreach from=$aModule.balances key=id item=balance name=balances_list}
			<tr>
				<td class="order_lp Center">{$balance.lp}</td>
				<td class="order_id_balance">{if !empty($balance.order_link)}<a href="{$balance.order_link}">{$balance.description}</a>{else}{$balance.description}{/if}</td>
				<td class="Center">{if !empty($balance.order_date)}{$balance.order_date}{else}---{/if}</td>
				<td class="Center">{if !empty($balance.order_value)}{$balance.order_value}{else}---{/if}</td>
				<td class="Center">{if !empty($balance.payment)}{$balance.payment}{else}---{/if}</td>
				<td class="Center">{if !empty($balance.payment_date)}{$balance.payment_date}{else}---{/if}</td>
				<td class="Center" style="font-weight: bold;">{$balance.paid_amount}</td>
				<td class="Center" style="font-weight: bold;color: #c30500;">{$balance.balance}</td>
			</tr>
		{/foreach}
	</table>
</div>
{else}
	<div style="padding: 15px;">{$aModule.lang.no_user_balances}</div>
{/if}

{if isset($aModule.pager.links) && !empty($aModule.pager.links)}
	<div class="pagerLinks">
		{$aModule.pager.links}
		<div class="clear"></div>
		<span class="opis">({$aLang.pager.current_page} {$aModule.pager.current_page} {$aLang.pager.from} {$aModule.pager.total_pages} - {$aLang.pager.total_items} {$aModule.pager.total_items})</span>
	</div>
{/if}