{if $aModule.is_logged_in}
	<div class="kontoOpcje">
		<ul>
		{foreach from=$aModule.buttons item=aButton name=tab}
			<li{if $aButton.selected} class="selected"{/if}>
				<a href="{$aButton.link}">
					<span class="kontoOpcjeLewo"></span>
					<span class="kontoOpcjeSrodek">{$aButton.label}</span>
					<span class="kontoOpcjePrawo"></span>
				</a>		
			</li>
		{/foreach}
		</ul>
	</div>
	<div class="clear"></div>
{/if}

{if $aModule.confirm_account_message != null}
	{literal}
	<style>
		.info-alert {
			background-color: #d9edf7;
			border-color: #bce8f1;
			padding: 10px;
			font-size:14px;
			margin-top: 20px;
		}
		.info-alert-text {
			color: #31708f;
		}
	</style>
	{/literal}
	<div class="info-alert info-alert-text">{$aModule.confirm_account_message} <b><a class="info-alert-text" href="/moje-konto/generate-activation-mail.html">link</a></b> </div>
{/if}

{if isset($aModule.template) && !empty($aModule.template)}
	{include file=$aModule.template}
{/if}