{if !empty($aModule.text)}
	{$aModule.text}
{/if}
{$aModule.form.JS}
<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.ajaxval}

	<div class="clear"></div>
	<div id="addr0_grp" style="float: left;width: 460px;">
		<div class="fRow fRowType">
			<div class="fLabel">{$aModule.form.addr_0.is_company.label}</div>
			<div class="fInput">{$aModule.form.addr_0.is_company.input}</div>
		</div>
		<div class="clear"></div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.addr_0.name.label}</div>
			<div class="fInput">{$aModule.form.addr_0.name.input}</div>
			<div class="clear"></div>
		</div>
		<div class="priv_grp"{if $aModule.form.addr_0.is_company.value == '1'} style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.priv.name.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.name.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.priv.surname.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.surname.input}</div>
				<div class="clear"></div>
			</div>
        {*
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.priv.nip.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.nip.input}</div>
				<div class="clear"></div>
			</div>
      *}
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.priv.street.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.street.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini">
				<div class="fLabel">{$aModule.form.addr_0.priv.number.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.number.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini" style="width: 170px;">
				<div class="fLabel" style="width: 60px;">{$aModule.form.addr_0.priv.number2.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.number2.input}</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.priv.postal.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.postal.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_postal}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.priv.city.label}</div>
				<div class="fInput">{$aModule.form.addr_0.priv.city.input}</div>
				<div class="clear"></div>
			</div>
		</div>
			
		<div class="corp_grp"{if $aModule.form.addr_0.is_company.value == '0'} style="display: none;"{/if}>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.corp.company.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.company.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.corp.nip.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.nip.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_nip}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.corp.street.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.street.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini">
				<div class="fLabel">{$aModule.form.addr_0.corp.number.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.number.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fRow fRowMini" style="width: 170px;">
				<div class="fLabel" style="width: 60px;">{$aModule.form.addr_0.corp.number2.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.number2.input}</div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.corp.postal.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.postal.input}</div>
				<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_postal}', this, event, '160px')"></div>
				<div class="clear"></div>
			</div>
			<div class="fRow">
				<div class="fLabel">{$aModule.form.addr_0.corp.city.label}</div>
				<div class="fInput">{$aModule.form.addr_0.corp.city.input}</div>
				<div class="clear"></div>
			</div>
		</div>
		{if !empty($aModule.form.addr_0.default_invoice)}
		<div class="fRow">
			<div class="fLabel">{$aModule.form.addr_0.default_invoice.label}</div>
			<div class="fInput">{$aModule.form.addr_0.default_invoice.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.addr_0.default_transport.label}</div>
			<div class="fInput">{$aModule.form.addr_0.default_transport.input}</div>
			<div class="clear"></div>
		</div>
		{/if}
	</div>
	{*<div class="newsletterBox">
		<div class="newsletterBoxTop"></div>
		<div class="newsletterBoxContent">
			<h3>{$aModule.lang.info_register_box_title}</h3>
			{$aModule.lang.info_register_box_content_2}		
		</div>
		<div class="newsletterBoxBot"></div>
	</div>*}
</div>

<div class="clear"></div>

	<div>
		{$aLang.form.required_fields}
	</div>
	
	<div class="buttonLine">
{*    <a href="/koszyk/step2.html" class="left" style="float: left;background: url('/images/gfx/button-wroc.gif') no-repeat;width: 85px;height: 33px;" title="Wróć"></a>*}
		<a class="left" href="{$aModule.edit_back_link}" style="background: url('/images/gfx/button-wroc.gif') no-repeat;width: 85px;height: 33px;" title="Wróć"></a>
		<input type="image" src="/images/gfx/button-zapisz-dane.gif" name="send" class="right" value="Rejestruj" />
		<div class="clear"></div>
	</div>
	
	{$aModule.form.footer}