﻿{if !empty($aModule.text)}
	{$aModule.text}
{/if}
{$aModule.form.JS}
<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	
	<div class="koszykPodtytul2">{$aModule.lang.header_login}:</div>	
	<div class="clear"></div>

	<div style="float: left; width: 50%;" class="formularz">	
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.name_section.email.label}</div>
			<div class="fInput">{$aModule.form.fields.name_section.email.input}</div>
			<div class="hintBox" onmouseover="showhint('Twój adres e-mail będzie jednocześnie loginem do Twojego konta.', this, event, '320px')"></div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.login_section.passwd.label}</div>
			<div class="fInput">{$aModule.form.fields.login_section.passwd.input}</div>
			<div class="hintBox" onmouseover="showhint('Od 6 do 16 znaków. NIE MOŻE zawierać znaków narodowych oraz: \ / \'\' \' (ukośniki, cudzysłów, apostrof).', this, event, '320px')"></div>
			<div class="clear"></div>
		</div>
	</div>
	<div style="float: left; width: 50%;" class="formularz">
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.name_section.confirm_email.label}</div>
			<div class="fInput">{$aModule.form.fields.name_section.confirm_email.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.login_section.passwd2.label}</div>
			<div class="fInput">{$aModule.form.fields.login_section.passwd2.input}</div>
			<div class="clear"></div>
		</div>
	</div>
<div class="clear"></div>
<div class="koszykPodtytul2">{$aModule.lang.header_send}:</div>	
<div class="clear"></div>	
<div style="float: left; width: 50%;" class="formularz">
	{if isset($aModule.form.fields.name_section)}
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.name_section.name.label}</div>
			<div class="fInput">{$aModule.form.fields.name_section.name.input}</div>
			<div class="clear"></div>
		</div>
	{/if}
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.address_section.street.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.street.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.address_section.postal.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.postal.input}</div>
		<div class="hintBox" onmouseover="showhint('W formacie 11-222, np. 00-203', this, event, '160px')"></div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.phone_section.phone.label}</div>
		<div class="fInput">{$aModule.form.fields.phone_section.phone.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.address_section.company.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.company.input}</div>
		<div class="clear"></div>
	</div>
</div>
<div style="float: left; width: 50%;" class="formularz">
	{if isset($aModule.form.fields.name_section)}
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.name_section.surname.label}</div>
			<div class="fInput">{$aModule.form.fields.name_section.surname.input}</div>
			<div class="clear"></div>
		</div>
	{/if}
	<div class="fRow fRowMini">
		<div class="fLabel">{$aModule.form.fields.address_section.number.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.number.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow fRowMini" style="width: 135px;">
		<div class="fLabel" style="width: 60px;">{$aModule.form.fields.address_section.number2.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.number2.input}</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.address_section.city.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.city.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">&nbsp;</div>
		<div class="fInput">&nbsp;</div>
		<div class="clear" style="height: 5px;"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.address_section.nip.label}</div>
		<div class="fInput">{$aModule.form.fields.address_section.nip.input}</div>
		<div class="hintBox" onmouseover="showhint('W formacie 111-222-33-44 lub 111-22-33-444 lub 1112223344', this, event, '320px')"></div>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>
	
<div class="koszykPodtytul2">
	{$aModule.form.fields.company_section.invoice_data.input}
	{$aModule.lang.header_invoice}:
</div>

{if isset($aModule.form.fields.company_section)}
<div id="invoice_section_area" {if $aModule.form.fields.company_section.show == '0'}style="display:none;"{/if}>
	<div class="clear"></div>	
	<div style="float: left; width: 50%;" class="formularz">
		<div class="clear"></div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.company_section.company.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.company.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.company_section.street.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.street.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.company_section.postal.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.postal.input}</div>
			<div class="hintBox" onmouseover="showhint('W formacie 11-222, np. 00-203', this, event, '160px')"></div>
			<div class="clear"></div>
		</div>
	</div>
	<div style="float: left; width: 50%;" class="formularz">
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.company_section.nip.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.nip.input}</div>
			<div class="hintBox" onmouseover="showhint('W formacie 111-222-33-44 lub 111-22-33-444 lub 1112223344', this, event, '320px')"></div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="fRow fRowMini">
			<div class="fLabel">{$aModule.form.fields.company_section.number.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.number.input}</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="fRow fRowMini" style="width: 135px;">
			<div class="fLabel" style="width: 60px;">{$aModule.form.fields.company_section.number2.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.number2.input}</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.company_section.city.label}</div>
			<div class="fInput">{$aModule.form.fields.company_section.city.input}</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
	{/if}

<div class="clear"></div>
	{if isset($aModule.form.fields.newsletter)}
		{include file=$aModule.form.fields.newsletter.template}
	{/if}
	{if isset($aModule.form.fields.regulations_agreement)}
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.regulations_agreement.regulations.input}</div>
			<div class="fLabel2">{$aModule.form.fields.regulations_agreement.regulations.label}</div>
			<div class="fInfo">{$aModule.lang.promotions_agreement_info}</div>
			<div class="clear"></div>
		</div>
	{/if}
	
	<div class="clear" style="height: 30px;"></div>	
	<div class="fRow" >
		<div class="fInput">{$aModule.lang.privacy_text}</div>
		<div class="clear"></div>
	</div>
	
	{*{if isset($aModule.form.fields.privacy_agreement)}*}
		{*<div class="fRow">*}
			{*<div class="fLabel" style="width: 26px;">{$aModule.form.fields.privacy_agreement.privacy.input}</div>*}
			{*<div class="fLabel2 left" style="padding-top: 6px">{$aModule.form.fields.privacy_agreement.privacy.label}</div>*}
			{*<div class="clear"></div>*}
		{*</div>*}
	{*{/if}*}
	
	<div class="clear"></div>	

	<div>
		{$aLang.form.required_fields}
	</div>
	<div class="fRow" style="float: right; margin-right: 10px; display: inline;">
		<div class="clear" style="height: 10px;"></div>
		<div class="fSendButton right">
			<input type="submit" name="send" class="sendBig" value="Rejestruj" />
		</div>
	</div>
	{$aModule.form.footer}
</div>