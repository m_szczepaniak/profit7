{****
<div class="koszykTytul">
	<h1><strong>Koszyk</strong> - krok 2/2 (Dane kupującego)</h1>
</div>
****}
<div class="clear"></div>

<div class="registForm registFormBig">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{if isset($aModule.form.fields.ref.input)}
		{$aModule.form.fields.ref.input}
	{/if}
	
	<div class="koszykPodtytul2">{$aModule.lang.login_description}:</div>
	<div class="clear"></div>
	
	<div class="left registFormBigLogin" style="width: 477px;">
		<div class="fRow">
			<div class="fLabel">&nbsp;</div>
			<div class="fInput"><h2>{$aModule.lang.login_title}</h2></div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.login.label}:</div>
			<div class="fInput">{$aModule.form.fields.login.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.passwd.label}:</div>
			<div class="fInput">{$aModule.form.fields.passwd.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel"></div>
			<div class="fInput">
				<div class="left">{$aModule.form.fields.remember.input}</div>
				<div style="margin: 2px 0 0 3px;float: left;">{$aModule.form.fields.remember.label}</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="fRow" style="width: 386px;">
			<a href="{$aModule.links.change_passwd}" class="login_cart_link">{$aLang.common.remind_passwd}</a>
			<input class="right" type="image" src="/images/gfx/button-zaloguj-sie.gif" name="send" value="Zaloguj się" />
		</div>
		<div class="clear" style="height: 16px;"></div>
		<div class="wymagane">
			{$aLang.form.required_fields}
		</div>
	</div>
	{$aModule.form.footer}
	<div class="registFormBigRegis">
		{$aModule.lang.login_registration_description}
		<div class="clear"></div>
		<div class="formRegister">
			<form action="{$aModule.links.register}" method="post" name="register">
				<input type="hidden" name="ref" value="{$aModule.links.reflink}" />
				<input type="hidden" name="cart" value="1" />
				<input type="image" src="/images/gfx/button-zaloz-konto.gif" name="send" class="right send" value="Załóż konto" />
			</form>
		</div>
	</div>
{*
	<div class="buttonLine buttonLineBig">
		<a href="/koszyk" class="left" style="width: 216px; height: 33px;background: url('/images/gfx/button-zmien-zawartosc-koszyka.gif');" title="Zmień zawartość koszyka"></a>
		<div class="clear"></div>
	</div>
*}
</div>