<div class="fTextRow" style="padding: 15px 0 0 0;">
  <div class="koszykPodtytulNewsletter">
    Czy ustawić hasło do konta ?
  </div>
  <div class="clear" style="padding: 15px 0 0 0;"></div>
  <div class="clear" style="padding: 15px 0 0 0;"></div>
  {if !empty($aModule.do_disconnect.links.do_disconnect)}
    <a class="border-radius" href="{$aModule.do_disconnect.links.do_disconnect}">Nie, dziękuję</a>
  {/if}
  {if !empty($aModule.do_disconnect.links.change_passwd)}
    <a class="border-radius red-button" style="font-size: 13px" href="{$aModule.do_disconnect.links.change_passwd}">Ustaw hasło</a>
  {/if}
  <div class="clear" style="padding: 15px 0 0 0;"></div>
  <span style="font-size: 13px;">(Ustaw hasło, aby zachować dostęp do konta na Profit24.pl)</span>
</div>