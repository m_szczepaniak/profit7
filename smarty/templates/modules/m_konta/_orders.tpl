{if $aModule.orders|@count gt 0}
<div id="koszykForm" class="koszykFormZamowienia">
	<table cellspacing="0" cellpadding="0" class="koszyk" id="koszykFormZamowienia">
		<tr>
			<th class="order_lp Center">Lp.</th>
			<th class="order_id Left">{$aModule.lang.order_id}</th>
			<th class="order_date" style="width: 110px;">{$aModule.lang.order_date}</th>
			<th class="status Left" style="width: 210px;">{$aModule.lang.orders_status}</th>
			<th class="orders_price" style="width: 118px;">{$aModule.lang.orders_price}</th>
			<th class="details" style="width: 60px;">{$aModule.lang.details}</th>
		</tr>
		{assign var=pozycja value=0}
		{foreach from=$aModule.orders key=id item=order name=order_list}
			{assign var=pozycja value=$pozycja+1}
			<tr>
				<td class="order_lp Center">{$order.lp}</td>
				<td class="order_id"><strong><a href="{$order.order_link}">{*{$aModule.lang.order_id}: *}{$order.order_number}</a></strong></td>
				<td class="order_date">{$order.order_date}</td>
				<td class="status Left">{if !empty($order.order_pay_link)}<strong>{/if}{$order.order_status_text}{if !empty($order.order_pay_link)}</strong>{/if}{if !empty($order.order_pay_link)}<a href="{$order.order_pay_link}"><img src="/images/gfx/button-oplac.gif" alt="Opłać" /></a>{/if}{if !empty($order.order_pay2_link)}<a href="{$order.order_pay2_link}"><img src="/images/gfx/button-doplac.png" alt="Dopłać" /></a>{/if}</td>
				<td class="orders_price" style="text-align: center; color: #c30500; font-weight: bold;">{$order.price|format_number:2:',':' '} {$aLang.common.currency}</td>
				<td class="details" style="text-align: center"><a href="{$order.order_link}"><img src="/images/gfx/ikona_lupa.png" alt="" /></a></td>
			</tr>
		{/foreach}
	</table>
</div>
{else}
	<div style="padding: 15px;">{$aModule.lang.no_user_orders}</div>
{/if}

{if isset($aModule.pager.links) && !empty($aModule.pager.links)}
	<div class="pagerLinks">
		{$aModule.pager.links}
		<div class="clear"></div>
		<span class="opis">({$aLang.pager.current_page} {$aModule.pager.current_page} {$aLang.pager.from} {$aModule.pager.total_pages} - {$aLang.pager.total_items} {$aModule.pager.total_items})</span>
	</div>
{/if}