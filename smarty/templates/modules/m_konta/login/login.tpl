<div class="newsBox newsTresc" style="padding-top: 20px;">
	
	<div class="content">
		{$aModule.form.header}
		{$aModule.form.err_prefix}
		{$aModule.form.err_postfix}
		{$aModule.form.validator}
		{if isset($aModule.form.fields.ref.input)}
			{$aModule.form.fields.ref.input}
		{/if}
		
		<div style="font-size: 13px; font-weight: bold; text-align: center; margin-bottom: 15px;">{$aLang.common.to_logged}</div>
		
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.login.label}</div>
			<div class="fInput">{$aModule.form.fields.login.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.passwd.label}</div>
			<div class="fInput">{$aModule.form.fields.passwd.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">&nbsp;</div>
			<div class="fSendButton">
				<input type="image" name="send" value="" src="/images/gfx/{$sLang}/zaloguj.gif" class="send" />
			</div>
		</div>

		<div class="fTextRow2"><a href="{$aModule.links.change_passwd}">{$aLang.common.remind_passwd}</a></div>			
		<div class="fTextRow2"><a href="{$aModule.links.register}">{$aLang.common.register}</a></div>

		{$aModule.form.footer}
	</div>
</div>