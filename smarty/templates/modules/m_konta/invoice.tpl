<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td width="40%" align="left">
			<img src="/images/gfx/faktura-logo.jpg" width="200px" height="76px"/><br/>
		</td>
		<td width="60%" align="right" style="font-size: 9pt;">
			<br/>
			{$aModule.seller_data.name}<br/>
			{$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br/>
			{$aModule.seller_data.postal} {$aModule.seller_data.city}<br/>
			{$aModule.seller_data.bank_name} {$aModule.lang.seller_bank_account} {$aModule.seller_data.bank_account}<br/>
		</td>
	</tr>
	<tr>
		<td width="70%" align="left" style="font-size: 15pt;">
			{if $aModule.pro_forma == '1'}{$aModule.lang.list_proforma_nr}{else}{$aModule.lang.list_vat_nr}{/if} <strong>{$aModule.invoice_number}</strong> / <span style="font-size: 13pt;">{$aModule.lang.oryginal_copy}</span>
		</td>
		<td width="30%" align="right">
			{$aModule.lang.list_city_day} {$aModule.invoice_date}
		</td>
	</tr>
</table>
<hr>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;">
	<tr>
		<th width="60%">
			<strong>{$aModule.lang.user_data_header}</strong><br/>
		</th>
		<th width="40%">
			<strong>{$aModule.lang.invoice_data_header}</strong><br/>
		</th>
	</tr>
	<tr>
		<td width="60%">
			<strong>{$aModule.seller_data.invoice_name}</strong><br/>
			{$aModule.seller_data.postal} {$aModule.seller_data.city} {$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br/>
			{*{$aModule.seller_data.bank_name} {$aModule.lang.seller_bank_account} {$aModule.seller_data.bank_account}<br/>*}
			{$aModule.lang.list_nip} {$aModule.seller_data.nip}, {$aModule.lang.list_regon} {$aModule.seller_data.regon}<br/><br/>
		</td>
		<td width="40%">
			{if $aModule.address.is_company == '1'}
				<strong>{$aModule.address.company}</strong><br/>
				{$aModule.address.street} {$aModule.address.number}{if !empty($aModule.address.number2)}/{$aModule.address.number2}{/if},{$aModule.lang.postal} {$aModule.address.postal} {$aModule.address.city}<br/>
				{if !empty($aModule.address.nip)}{$aModule.address.nip}<br/>{/if}
				
			{else}
				<strong>{$aModule.address.name} {$aModule.address.surname}</strong><br/>
				{$aModule.address.street} {$aModule.address.number}{if !empty($aModule.address.number2)}/{$aModule.address.number2}{/if},{$aModule.lang.postal} {$aModule.address.postal} {$aModule.address.city}<br/>
			{/if}
		</td>
	</tr>
</table>
<hr>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;">
	<tr>
		<td width="25%">
			<strong>{$aModule.lang.selected_payment}</strong> {$aModule.order.payment}<br/><br/>
		</td>
		<td width="40%" align="center">
			<strong>{$aModule.lang.selected_transport}</strong> {$aModule.order.transport}<br/><br/>
		</td>
		<td width="35%" align="right">
			{if !empty($aModule.order.transport_number)}<strong>{$aModule.lang.list_transport_nubmer}</strong> {$aModule.order.transport_number}{/if}<br/><br/>
		</td>	
	</tr>
</table>
<table width="510px" cellspacing="0" cellpadding="0" style="font-size: 8pt;" border="1">
	<tr>
		<th width="17px" align="center" rowspan="2">{$aModule.lang.list_lp}</th>
		<th width="100px" align="left" rowspan="2">{$aModule.lang.list_name}</th>
		<th width="82px" align="center" rowspan="2">{$aModule.lang.list_symbol}</th>
		<th width="22px" align="center" rowspan="2">{$aModule.lang.list_quantity}</th>
		<th width="22px" align="center" rowspan="2">{$aModule.lang.list_jm}</th>
		<th width="45px" align="center" rowspan="2">{$aModule.lang.list_price_brutto}</th>
		<th width="27px" align="center" rowspan="2">{$aModule.lang.list_discount}</th>
		<th width="45px" align="center" rowspan="2">{$aModule.lang.list_promo_price_netto}</th>
		<th width="45px" align="center" rowspan="2">{$aModule.lang.list_value_netto}</th>
		<th width="60px" align="center" colspan="2">{$aModule.lang.list_vat}</th>
		<th width="45px" align="center" rowspan="2">{$aModule.lang.list_value_brutto}</th>
	</tr>
	<tr>
		<th width="19px" align="center">{$aModule.lang.list_st}</th>
		<th width="41px" align="center">{$aModule.lang.list_price}</th>
	</tr>
	{foreach from=$aModule.invoice_items key=iId item=aItems name=types}
		<tr>
			<td width="17px" align="center">{$aItems.lp}</td>
			<td width="100px" align="left">{$aItems.name}</td>
			<td width="82px" align="center">{$aItems.isbn}</td>
			<td width="22px" align="center">{$aItems.quantity}</td>
			<td width="22px" align="center">{$aItems.jm}</td>
			<td width="45px" align="center">{$aItems.price_brutto|format_number:2:',':' '}</td>
			<td width="27px" align="center">{$aItems.discount|format_number:2:',':' '}</td>
			<td width="45px" align="center">{$aItems.promo_price_netto|format_number:2:',':' '}</td>
			<td width="45px" align="center">{$aItems.value_netto|format_number:2:',':' '}</td>
			<td width="19px" align="center">{$aItems.vat}</td>
			<td width="41px" align="center">{$aItems.vat_currency|format_number:2:',':' '}</td>
			<td width="45px" align="center">{$aItems.value_brutto|format_number:2:',':' '}</td>
		</tr>
	{/foreach}
</table>
<br/>
<table width="510px" cellspacing="0" cellpadding="0" style="font-size: 8pt;" border="0">
	<tr>
		<td width="177px" align="center"></td>
		<td width="66px" align="center">{$aModule.order.quantity}</td>
		<td width="72px" align="center"></td>
		<td width="45px" align="center">{*$aModule.lang.list_together*}</td>
		<td width="45px" align="center">{*$aModule.order.value_netto*}</td>
		<td width="19px" align="center"></td>
		<td width="41px" align="center">{$aModule.lang.list_together}{*$aModule.order.vat*}</td>
		<td width="45px" align="center">{$aModule.order.value_brutto}</td>
	</tr>
</table>
<br/>
<table width="510px" cellspacing="0" cellpadding="0" style="font-size: 8pt;" border="0">
	<tr>
		<td width="235px"></td>
		<td width="275px">
			<table width="275px" cellspacing="0" style="font-size: 8pt;" border="1" align="center">
				<tr>
					<td width="40px"></td>
					<td width="24px"></td>
					<td width="81px">{$aModule.lang.list_value_netto}</td>
					<td width="65px">{$aModule.lang.list_price_vat}</td>
					<td width="65px">{$aModule.lang.list_value_brutto}</td>
				</tr>
				{foreach from=$aModule.vat item=aVat key=iVat name=vats}
					<tr>
						<td width="40px">{if $smarty.foreach.vats.first}W tym{/if}</td>
						<td width="24px">{$iVat}%</td>
						<td width="81px">{$aVat.value_netto}</td>
						<td width="65px">{$aVat.vat_currency}</td>
						<td width="65px">{$aVat.value_brutto}</td>
					</tr>
				{/foreach}
					<tr>
						<td width="40px">Ogółem</td>
						<td width="24px">XXX</td>
						<td width="81px">{$aModule.order.value_netto}</td>
						<td width="65px">{$aModule.order.vat}</td>
						<td width="65px">{$aModule.order.value_brutto}</td>
					</tr>
			</table>
		</td>
	</tr>
</table>
<br/>
<hr>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="0">
	<tr>
		<td width="60%">
			<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="0">
				<tr>
					<td>
						{if $aModule.order.payment_type == 'bank_14days'}
							{if $aModule.invoice_date_14 != ''}
								<strong>termin do zaplaty {$aModule.invoice_date_14}</strong><br/><br/>
							{/if}
							<strong>{$aModule.lang.list_reciptient}</strong> {$aModule.seller_data.name}<br/>
							{$aModule.seller_data.postal} {$aModule.seller_data.city} {$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br/>
							<strong>{$aModule.lang.list_bank}</strong> {$aModule.seller_data.bank_name}<br/>
							<strong>{$aModule.lang.list_number_account}</strong> {$aModule.seller_data.bank_account}<br/>
							<strong>{$aModule.lang.list_transfer}</strong> {$aModule.order.order_number}
						{/if}
					</td>				
				</tr>
			</table>
		</td>
		<td width="40%">
			<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="1" align="center">
				<tr>
					<td>	
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td width="10%"></td>
								<td width="80%" style="font-size: 16pt">
									{$aModule.lang.list_pay} <strong>{$aModule.order.value_brutto} zł</strong><br/>
									<span style="font-size: 9pt;">{$aModule.lang.list_dictionary}<strong>{$aModule.order.total_value_brutto_words}</strong></span><br/>
								</td>	
								<td width="10%"></td>
							</tr>
						</table>
					</td>			
				</tr>
			</table>
		</td>
	</tr>
</table>
<br/><br/><br/><br/><br/>
<table width="510px" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="0">
	<tr>
		<td width="170px">
			. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .<br/>
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.list_vat_reception}</span>
		</td>
		<td width="65px">
			. . . . . . . . . . . . .<br/>
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.list_vat_data}</span>
		</td>
		<td width="108px"></td>
		<td width="167px" align="right">
			. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .<br/>
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.list_vat_acting}</span>
		</td>
	</tr>
</table>