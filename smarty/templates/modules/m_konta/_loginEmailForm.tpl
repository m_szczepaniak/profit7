<div class="content">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.JS}
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.login.label}</div>
		<div class="fInput">{$aModule.form.fields.login.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.email.label}</div>
		<div class="fInput">{$aModule.form.fields.email.input}</div>
		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>
	<div class="fRow">
		<div class="fLabel">
			<div class="fRequiredF">{$aLang.form.required_fields}</div>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="fRow" style="padding-top: 2px;">
		<div class="fLabel">&nbsp;</div>
		<input type="submit" class="input_button_2" value="Wyślij" name="send" />
	</div>
	{$aModule.form.footer}
</div>