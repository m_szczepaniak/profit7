<div {*id="koszykForm" *}class="orders">
  {if !empty($aModule.details.header_message)}
    <div style="color: #c30500; text-align: center; font-weight: bold;">
      {$aModule.details.header_message}
      <br /><br /><br />
    </div>
  {/if}
	<div class="left" style="width: 290px;">
		<div class="ordersNr">{$aModule.lang.order_id_mini}: <strong>{$aModule.details.order_number}</strong></div>
		<div class="ordersData left">{$aModule.lang.order_date}: <strong>{$aModule.details.order_date}</strong></div>
		{if !empty($aModule.details.status_3_update)}<div class="ordersData left">&nbsp;|&nbsp;{$aModule.lang.order_realization_date}: <strong>{$aModule.details.status_3_update}</strong></div>{/if}
	</div>
	{if $aModule.details.order_status!='4' && $aModule.details.order_status!='5'}
		{*<div class="ordersProforma"><a href="{$aModule.details.proforma_link}" target="_blank" title="Pobierz na dysk fakturę pro forma"></a></div>*}
	{elseif $aModule.details.order_status!='5'}
		{if !empty($aModule.details.proforma_link)}
			<div class="ordersVat"><a href="{$aModule.details.proforma_link}" target="_blank" title="Pobierz na dysk fakturę VAT"></a></div>
		{/if}
		<div class="ordersInvoices" style="width: 165px;float: left;">
			<div>
			{foreach from=$aModule.details.proformas key=iId item=invoice name=types}
					<a style="padding: 10px;margin: 5px;border: 1px solid #ccc;background: #eee;height: 25px;float:left;margin-bottom: 10px;" href="{$invoice.link}">Pobierz Fakturę nr.{$invoice.invoice_number}</a>
			{/foreach}
			</div>
		</div>
	{/if}
	<div class="ordersStatus">{$aModule.lang.order_status}:<br /> <strong>{$aModule.details.order_status_text}</strong></div>
	<div class="clear" style="height: 30px;"></div>	

	{if !empty($aModule.details.addresses[0])}
	<div class="step2Option step2OptionSummary">
		<div class="koszykPodtytul2"><strong>Dane do faktury VAT:</strong></div>
		<div class="mojeAdresy">
			<div class="mojeAdresySrodek">
				{if $aModule.details.addresses[0].is_company == '1'}
					<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.details.addresses[0].company}</div>
					<div class="clear"></div>
					{if !empty($aModule.details.addresses[0].nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.details.addresses[0].nip}</div>
					<div class="clear"></div>{/if}
					<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.details.addresses[0].street} {$aModule.details.addresses[0].number}{if !empty($aModule.details.addresses[0].number2)}/{$aModule.details.addresses[0].number2}{/if}</div>
					<div class="clear"></div>
					<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.details.addresses[0].postal} {$aModule.details.addresses[0].city}</div>
				{else}
					<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.details.addresses[0].name} {$aModule.details.addresses[0].surname}</div>
					<div class="clear"></div>
					{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
					<div class="clear"></div>*}
					<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.details.addresses[0].street} {$aModule.details.addresses[0].number}{if !empty($aModule.details.addresses[0].number2)}/{$aModule.details.addresses[0].number2}{/if}</div>
					<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.details.addresses[0].postal} {$aModule.details.addresses[0].city}</div>
				{/if}
			</div>
		</div>
	</div>
	{/if}
		
	{if $aModule.details.transport_symbol == 'paczkomaty_24_7' ||  $aModule.details.transport_symbol == 'poczta_polska' ||  $aModule.details.transport_symbol == 'ruch' || $aModule.details.transport_symbol == 'orlen'}
		<div class="step2Option step2OptionSummary" style="padding: 0 !important;">
			<div class="koszykPodtytul2"><strong>Adres punktu odbioru:</strong></div>
			<div class="mojeAdresy">
				<div class="mojeAdresySrodek">	
					{if is_array($aModule.details.point_of_receipt)}
						{$aModule.details.point_of_receipt.details}
					{else}
						<div class="daneLewo">Kod paczkomatu: </div><div class="danePrawo">{$aModule.details.point_of_receipt}</div>
						<div class="clear"></div>
					{/if}
				</div>
			</div>	
		</div>
    {else}


		{if !empty($aModule.details.addresses[1])}
		<div class="step2Option step2OptionSummary" style="padding: 0 !important;">
			<div class="koszykPodtytul2"><strong>Dane do dostawy:</strong></div>
			<div class="mojeAdresy">
				<div class="mojeAdresySrodek">	
					{if $aModule.details.addresses[1].is_company == '1'}
						<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.details.addresses[1].company}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.details.addresses[1].nip}</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.details.addresses[1].street} {$aModule.details.addresses[1].number}{if !empty($aModule.details.addresses[1].number2)}/{$aModule.details.addresses[1].number2}{/if}</div>
						<div class="clear"></div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.details.addresses[1].postal} {$aModule.details.addresses[1].city}</div>
					{else}
						<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.details.addresses[1].name} {$aModule.details.addresses[1].surname}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.details.addresses[1].street} {$aModule.details.addresses[1].number}{if !empty($aModule.details.addresses[1].number2)}/{$aModule.details.addresses[1].number2}{/if}</div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.details.addresses[1].postal} {$aModule.details.addresses[1].city}</div>
					{/if}
				</div>
			</div>	
		</div>
		{/if}
	{/if}

	<div class="clear"></div>
	<div class="koszykPodtytul2" style="margin-bottom: 13px;"><strong>Lista zamówionych pozycji:</strong></div>
	<div class="clear"></div>
	<table cellspacing="0" cellpadding="0" class="koszykPods">
		<tr>
			<th class="pozycja Center">Lp.</th>
			<th class="nazwa Left">{$aModule.lang.list_position}</th>
			<th class="status Center" style="width: 56px;">{$aModule.lang.status}</th>
			<th class="cena Center" style="width: 80px;">{$aModule.lang.list_price_brutto}</th>
			<th class="ilosc Center" style="width: 27px;">{$aModule.lang.list_quantity}</th>
			<th class="wartosc Center" style="width: 88px;">{$aModule.lang.total_products_brutto}</th>
		</tr>
		{assign var=pozycja value=0}
		{foreach from=$aModule.details.items key=iId item=aItems name=types}
			{if $aItems.item_type<>'P' && $aItems.parent_id<=0}
				{*dump var=$aModule.details.items*}
				{assign var=pozycja value=$pozycja+1}
				<tr{if $aItems.deleted == '1'} class="deleted"{/if}>
					<td class="pozycja Center">{$pozycja}</td>
					<td class="nazwa Left{if $aItems.packet == 1} nazwaPakiet{/if}">
						{if isset($aItems.link)}<a href="{$aItems.link}">{$aItems.name}</a>{else}{$aItems.name}{/if}{if $aItems.deleted == '1'} <img src="/images/gfx/usunieto.gif" alt="Usunięto" />{/if}{if $aItems.packet == 1}<img src="/images/gfx/lista-pakiet.gif" alt="Pakiet" />{/if}
						{if $aItems.packet == 1}
							<ul>
								{foreach from=$aItems.items key=iId2 item=aItems2 name=types2}
								<li>{$aItems2.name}</li>
								{/foreach}
							</ul>
						{/if}
					</td>
					<td class="status Center">{$aItems.status_text}</td>
					<td class="cena Center">{if $aItems.promo_price_brutto > 0}<span>{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</span><br/>{$aItems.promo_price_brutto|format_number:2:',':' '} {$aLang.common.currency}{else}{$aItems.price_brutto|format_number:2} {$aLang.common.currency}{/if}</td>
					<td class="ilosc Center">{$aItems.quantity}</td>
					<td class="wartosc Center">{if $aItems.discount_currency > 0}<span>{$aItems.nopromo_value_brutto} {$aLang.common.currency}</span><br/> <strong>{$aItems.value_brutto} {$aLang.common.currency}</strong>{else}<strong>{$aItems.value_brutto} {$aLang.common.currency}</strong>{/if}</td>
				</tr>
			{/if}
		{/foreach}
	</table>
	<table cellspacing="0" cellpadding="0" class="koszykSuma" style="border-bottom: 0;border-top: 1px solid #eae9e9;margin-top: 1px;">	
		<tr>
			<td class="suma">
				<div class="cena right">{$aModule.details.value_brutto_invoice1|format_number:2:',':' '}{$aLang.common.currency}</div>
				<div class="right" style="padding: 7px 7px 0 0;">{$aModule.lang.total_cost2}</div>
				<div class="clear" style="height: 10px;"></div>
			</td>
		</tr>
	</table>
	<div class="clear" style="height: 15px;"></div>
	
	{if $aModule.details.second_invoice == '1' && !empty($aModule.details.items2)}	

		{if !empty($aModule.details.addresses[2])}
		<div class="step2Option step2OptionSummary">
			<div class="koszykPodtytul2"><strong>Dane do drugiej faktury VAT:</strong></div>
			<div class="mojeAdresy">
				<div class="mojeAdresySrodek">
					{if $aModule.details.addresses[2].is_company == '1'}
						<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$aModule.details.addresses[2].company}</div>
						<div class="clear"></div>
						{if !empty($aModule.details.addresses[2].nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$aModule.details.addresses[2].nip}</div>
						<div class="clear"></div>{/if}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.details.addresses[2].street} {$aModule.details.addresses[2].number}{if !empty($aModule.details.addresses[2].number2)}/{$aModule.details.addresses[2].number2}{/if}</div>
						<div class="clear"></div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.details.addresses[2].postal} {$aModule.details.addresses[2].city}</div>
					{else}
						<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$aModule.details.addresses[2].name} {$aModule.details.addresses[2].surname}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$aModule.details.addresses[2].street} {$aModule.details.addresses[2].number}{if !empty($aModule.details.addresses[2].number2)}/{$aModule.details.addresses[2].number2}{/if}</div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$aModule.details.addresses[2].postal} {$aModule.details.addresses[2].city}</div>
					{/if}
				</div>
			</div>	
		</div>
		{/if}

		<div class="clear"></div>
		<div class="koszykPodtytul2" style="margin-bottom: 13px;"><strong>Lista zamówionych pozycji, zawartych na drugiej fakturze:{*{$aModule.lang.payment}*}</strong></div>
		<div class="clear"></div>
		<table cellspacing="0" cellpadding="0" class="koszykPods">
			<tr>
				<th class="pozycja Center">Lp.</th>
				<th class="nazwa Left">{$aModule.lang.list_position}</th>
				<th class="status Center" style="width: 56px;">{$aModule.lang.status}</th>
				<th class="cena Center" style="width: 80px;">{$aModule.lang.list_price_brutto}</th>
				<th class="ilosc Center" style="width: 27px;">{$aModule.lang.list_quantity}</th>
				<th class="wartosc Center" style="width: 88px;">{$aModule.lang.total_products_brutto}</th>
			</tr>
			{assign var=pozycja value=0}
			{foreach from=$aModule.details.items2 key=iId item=aItems name=types}
				{if $aItems.item_type<>'P' && $aItems.parent_id<=0}
					{assign var=pozycja value=$pozycja+1}
					<tr{if $aItems.deleted == '1'} class="deleted"{/if}>
						<td class="pozycja Center">{$pozycja}</td>
						<td class="nazwa Left{if $aItems.packet == 1} nazwaPakiet{/if}">
							{if isset($aItems.link)}<a href="{$aItems.link}">{$aItems.name}</a>{else}{$aItems.name}{/if}{if $aItems.deleted == '1'} <img src="/images/gfx/usunieto.gif" alt="Usunięto" />{/if}{if $aItems.packet == 1}<img src="/images/gfx/lista-pakiet.gif" alt="Pakiet" />{/if}
							{if $aItems.packet == 1}
								<ul>
									{foreach from=$aItems.items key=iId2 item=aItems2 name=types2}
									<li>{$aItems2.name}</li>
									{/foreach}
								</ul>
							{/if}
						</td>
						<td class="status Center">{$aItems.status_text}</td>
						<td class="cena Center">{if $aItems.promo_price_brutto > 0}<span>{$aItems.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</span><br/>{$aItems.promo_price_brutto|format_number:2:',':' '} {$aLang.common.currency}{else}{$aItems.price_brutto|format_number:2} {$aLang.common.currency}{/if}</td>
						<td class="ilosc Center">{$aItems.quantity}</td>
						<td class="wartosc Center">{if $aItems.discount_currency > 0}<span>{$aItems.nopromo_value_brutto} {$aLang.common.currency}</span><br/> <strong>{$aItems.value_brutto} {$aLang.common.currency}</strong>{else}<strong>{$aItems.value_brutto} {$aLang.common.currency}</strong>{/if}</td>
					</tr>
				{/if}
			{/foreach}
		</table>
		<table cellspacing="0" cellpadding="0" class="koszykSuma" style="border-bottom: 0;border-top: 1px solid #eae9e9;margin-top: 1px;">	
			<tr>
				<td class="suma">
					<div class="cena right">{$aModule.details.value_brutto_invoice2|format_number:2:',':' '}{$aLang.common.currency}</div>
					<div class="right" style="padding: 7px 7px 0 0;">{$aModule.lang.total_cost2}</div>
					<div class="clear" style="height: 10px;"></div>
				</td>
			</tr>
		</table>
		<div class="clear" style="height: 15px;"></div>
	{/if}
	
	<div class="totalValue">
		<div class="totalValueNormal">Łączna suma zamówienia: <strong>{$aModule.details.total_value_brutto} {$aLang.common.currency}</strong></div>
		{if $aModule.details.paid_amount gt 0}
			<div class="totalValueNormal">Zapłacono: <strong>{$aModule.details.paid_amount|format_number:2:',':' '} {$aLang.common.currency}</strong></div>
		{/if}
		Kwota do zapłaty: <strong>{$aModule.details.to_pay|format_number:2:',':' '} {$aLang.common.currency}</strong>
	</div>	
	
	<div class="clear"></div>
	<div class="ordersInfo">
		<ul>
			<li>{$aModule.lang.order_transport_type}: <strong>{$aModule.details.transport}</strong></li>
			{if !empty($aModule.details.transport_number)}<li>{$aModule.lang.transport_number}: <strong>{$aModule.details.transport_number}</strong></li>{/if}
			<li>{$aModule.lang.payment}: <strong>{$aModule.details.payment}</strong></li>
			{if !empty($aModule.details.invoice_number)}<li>{$aModule.lang.invoice_number}: <strong>{$aModule.details.invoice_number}</strong></li>{/if}
			{if !empty($aModule.details.invoice_number2)}<li>{$aModule.lang.invoice_number2}: <strong>{$aModule.details.invoice_number2}</strong></li>{/if}
			{if !empty($aModule.details.discount_code)}<li>{$aModule.lang.discount_code2}: <strong>dd{$aModule.details.discount_code}</strong></li>{/if}
		</ul>	
	</div>		
	
	<a name="platnosci"></a>
	{if !empty($aModule.details.payment_form)}
	<form action="{$aModule.details.payment_form.action}" method="post" name="register">
		<input type="hidden" name="ref" value="{$aModule.details.payment_form.reflink}" />
		{if $aModule.details.payment_form.card == '1'}
		<input type="hidden" name="payment_bank" value="c" />
		{else}
		<div class="clear"></div>
		<div id="paymentBankSection"{if $aModule.payment_bank_show == '0'} style="display: none;"{/if}>
			<h2>Wybierz swój bank, z którego chcesz wykonać przelew elektroniczny:</h2>
			<ul>
				{foreach from=$aModule.details.payment_form.payments item=bank key=t name=payment_bank_form}
						<li>{$bank.input}<img for  src="/images/gfx/platnoscipl/{$bank.code}.gif" alt="" />{$bank.label}</li>
				{/foreach}
			</ul>
		</div>
		{/if}
		<div class="clear"></div>
		<input type="image" src="/images/gfx/button-oplac2.gif" name="send" class="right" style="margin: 26px 0 0;" value="Wykonaj przelew" />
	</form>
	{/if}	
</div>	
{if $smarty.session.w_user.id > 0}
<div class="buttonLine" style="margin: 27px 0 0;">
	<a class="left" href="/moje-konto/orders.html" style="background: url('/images/gfx/button-twoje-zamowienia.gif'); width: 172px; height: 33px;" title="Wróć do swoich danych"></a>
	<div class="clear"></div>
</div>
{/if}

{if !empty($aModule.details.js)}	
  {$aModule.details.js}
{/if}