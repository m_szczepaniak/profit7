{if !empty($aModule.text)}
	{$aModule.text}
{/if}
{$aModule.form.JS}
<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.ajaxval}
	{if !empty($aModule.form.ref_link)}{$aModule.form.ref_link}{/if}
	
	<div class="koszykPodtytul2">{$aModule.lang.header_login}</div>
	<div class="clear"></div>

	<div style="float: left; width: 50%;" class="formularz">	
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.name.label}</div>
			<div class="fInput">{$aModule.form.fields.name.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.surname.label}</div>
			<div class="fInput">{$aModule.form.fields.surname.input}</div>
			<div class="clear"></div>
		</div>
		
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.email.label}</div>
			<div class="fInput">{$aModule.form.fields.email.input}</div>
			{*<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_email}', this, event, '320px')"></div>*}
			<div class="clear"></div>
			<div class="hide_message" style="display:none">
				{$aLang.common.hint_email}
			</div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.confirm_email.label}</div>
			<div class="fInput">{$aModule.form.fields.confirm_email.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.passwd.label}</div>
			<div class="fInput">{$aModule.form.fields.passwd.input}</div>
			{*<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_passwd}', this, event, '320px')"></div>*}
			<div class="clear"></div>
			<div class="hide_message" style="display:none">
				{$aLang.common.hint_passwd}
			</div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.passwd2.label}</div>
			<div class="fInput">{$aModule.form.fields.passwd2.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.phone.label}</div>
			<div class="fInput">{$aModule.form.fields.phone.input}</div>
			{*<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_phone}', this, event, '320px')"></div>*}
			<div class="clear"></div>
			<div class="hide_message" style="display:none">
				{$aLang.common.hint_phone}
			</div>			
		</div>
	</div>
	<div class="newsletterBox">
		<div class="newsletterBoxTop"></div>
		<div class="newsletterBoxContent">
			{*<h3>{$aModule.lang.info_register_box_title}</h3>*}
			{$aModule.lang.info_register_box_content_1}		
		</div>
		<div class="newsletterBoxBot"></div>
	</div>

<div class="clear"></div>

<div class="corp_grp_input">
	<div class="fRow">
		<div class="fLabel">&nbsp;</div>
		<div class="fInput">{$aModule.form.accept.reg.input} {$aModule.form.accept.reg.label}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">&nbsp;</div>
		<div class="fInput">{$aModule.form.accept.promotions_agreement.input} {$aModule.form.accept.promotions_agreement.label}</div>
		<div class="fInfo">{$aModule.lang.promotions_agreement_info}</div>
		<div class="clear"></div>
	</div>	
	{*<div class="fRow">*}
		{*<div class="fLabel">&nbsp;</div>*}
		{*<div class="fInput">{$aModule.form.accept.priv.input} {$aModule.form.accept.priv.label}</div>*}
		{*<div class="clear"><input type="hidden" name="sentForm" value="1" /></div>*}
	{*</div>	*}
</div>

<div id="registerForm_info" style="display: none;"></div>

<div class="clear"></div>
<div class="wymagane" style="margin-top: 40px;">
	<span>*</span> Pola obowiązkowe
</div>

<div class="buttonLine">
	<a class="left" href="/" style="background: url('/images/gfx/button-wroc-do.gif'); width: 206px; height: 33px;" tabindex="9"></a>
	<input type="image" src="/images/gfx/button-zakladam.png" name="send" class="right" value="Zakładam konto" tabindex="8" />
	<div class="clear"></div>
</div>


	<div class="fRow" style="float: right; margin-right: 10px; display: inline;">
		<div class="clear" style="height: 10px;"></div>
		<div class="fSendButton right">
			
		</div>
	</div>
	{$aModule.form.footer}
</div>