{if !empty($aModule.text)}
	{$aModule.text}
{/if}
{$aModule.form.JS}
<div class="registrationForm" style="padding-top: 0;">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	
	<div class="mojeKontoNazwa">{$aModule.lang.header_user_data}</div>
	<div class="clear"></div>

	<div style="float: left; width: 50%;" class="formularz">	
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.name.label}</div>
			<div class="fInput">{$aModule.form.fields.name.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.surname.label}</div>
			<div class="fInput">{$aModule.form.fields.surname.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.phone.label}</div>
			<div class="fInput">{$aModule.form.fields.phone.input}</div>
			<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_phone}', this, event, '220px')"></div>
			<div class="clear"></div>
		</div>
	</div>

	<div class="clear"></div>
	
	<div class="clear"></div>
	<div class="wymagane" style="margin-top: 40px;">
		<span>*</span> Pola obowiązkowe
	</div>

	<div class="buttonLine">
		<a class="left" href="{$aModule.edit_mainpage_link}" style="background: url('/images/gfx/button-wroc-do-swoich-danych.gif'); width: 206px; height: 33px;" tabindex="9"></a>
		<input type="image" src="/images/gfx/button-zapisz-dane.gif" name="send" class="right" value="Zmień dane" tabindex="8" />
		<div class="clear"></div>
	</div>


	<div class="fRow" style="float: right; margin-right: 10px; display: inline;">
		<div class="clear" style="height: 10px;"></div>
		<div class="fSendButton right">
			
		</div>
	</div>
	{$aModule.form.footer}
</div>