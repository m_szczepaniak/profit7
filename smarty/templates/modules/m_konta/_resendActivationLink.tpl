<div style="width: 100%; height: 100%; background-color: #ffffff; font-family: Tahoma; font-size: 12px; margin: 0; padding: 0; line-height: 130%;">

		{$aModule.header} {$aModule.site_name}<br /><br />
		{$aModule.content}<br />
		<a href="{$aModule.activation_link}" style="color: #004883; text-decoration: none; font-weight: bold;">{$aModule.activation_link}</a><br /><br />
		{$aModule.login_site} <a href="{$aModule.login_page_link}" style="color: #004883; text-decoration: none; font-weight: bold;">{$aModule.login_page_link}</a><br /><br />

</div>