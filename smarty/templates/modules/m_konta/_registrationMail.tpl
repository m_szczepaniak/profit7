<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 11px;">
	<div style="float: left; display: inline; width: 200px; height: 76px; margin: 10px 0 15px 20px;">
		<img src="logo_mail.png" alt="" />
	</div>
	<div style="clear: both;"></div>
	<div style="width: 100%; height: 100%; background-color: #ffffff; font-family: Tahoma; font-size: 12px; margin: 0; padding: 0 0 0 20px; line-height: 130%;">
		{$aModule.header} {$aModule.site_name}<br /><br />
		{$aModule.content}<br />
		{$aModule.login_site} <a href="{$aModule.login_page_link}" style="color: #c30500; text-decoration: none; font-weight: bold;">{$aModule.login_page_link}</a><br /><br />
		{$aModule.login_data}<br />
		{$aModule.email_text} {$aModule.email}<br />
		{$aModule.passwd_text} {$aModule.passwd}<br /><br />
		{$aModule.end}
		{if isset($aModule.discount_code)}
			<br /><br />{$aModule.discount_code}
		{/if}
	</div>
</body>
</html>