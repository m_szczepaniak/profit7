{if !empty($aModule.text)}
	{$aModule.text}
{/if}
{$aModule.form.JS}
<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{$aModule.form.ajaxval}
	
	{*<div class="koszykPodtytul2">{$aModule.lang.header_login}</div>
	<div class="clear"></div>*}

	<div style="float: left; width: 50%;" class="formularz">	
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.email.label}</div>
			<div class="fInput">{$aModule.form.fields.email.input}</div>
			<div class="hintBox" onmouseover="showhint('{$aLang.common.hint_email}', this, event, '320px')"></div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.confirm_email.label}</div>
			<div class="fInput">{$aModule.form.fields.confirm_email.input}</div>
			<div class="clear"></div>
		</div>
	</div>

	<div class="clear"></div>
	<div class="wymagane" style="margin-top: 40px;">
		<span>*</span> Pola obowiązkowe
	</div>

	<div class="buttonLine">
		<a class="left" href="{$aModule.edit_mainpage_link}" style="background: url('/images/gfx/button-wroc-do-swoich-danych.gif'); width: 206px; height: 33px;" tabindex="9"></a>
		<input type="image" src="/images/gfx/button-zapisz-zmiany.gif" name="send" class="right" value="Zapisz zmiany" tabindex="8" />
		<div class="clear"></div>
	</div>

	{$aModule.form.footer}
</div>