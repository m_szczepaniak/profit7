<div class="zakladkiBox">
	<div class="saldoInfo">{$aModule.w_user.money}</div>
	{if !empty($aModule.balance_history)}
	<div id="historyInfo">{$aModule.lang.balance_history}</div>
	<table cellspacing="0" cellpadding="5" class="balanceHistory">
		<tr>
			<th>{$aModule.lang.operation_date}</th>
			<th>{$aModule.lang.operation_type}</th>
			<th>{$aModule.lang.operation_name}</th>
			<th>{$aModule.lang.operation_amount}</th>
			<th>{$aModule.lang.operation_balance}</th>
		</tr>
		{foreach from=$aModule.balance_history item=aItem name=history_list}
			<tr>
				<td {if isset($smarty.foreach.history_list.last)}style="border-bottom: 0;"{/if}>{$aItem.operation_date}<br />{$aItem.operation_hour}</td>
				<td class="operation{$aItem.operation}" {if isset($smarty.foreach.history_list.last)}style="border-bottom: 0;"{/if}>{$aModule.lang.operation_types[$aItem.operation]}</td>
				<td {if isset($smarty.foreach.history_list.last)}style="border-bottom: 0;"{/if}>{if $aItem.otype eq 'article'}{$aModule.lang.bought_article}<br /><strong>{$aItem.name}</strong>{elseif $aItem.otype eq 'packet'}{$aModule.lang.bought_packet}<br /><strong>{$aModule.lang.order_no} {$aItem.order_id}</strong> - {$aItem.order_date} {$aItem.order_hour}{else}{$aModule.lang.bought_advice}<br /><strong>{$aItem.name}</strong>{/if}</td>
				<td class="operation{$aItem.operation}" {if isset($smarty.foreach.history_list.last)}style="border-bottom: 0;"{/if}>{if $aItem.operation eq '0'}-&nbsp;{else}+&nbsp;{/if}{$aItem.amount}&nbsp;{$aLang.common.currency}</td>
				<td {if isset($smarty.foreach.history_list.last)}style="border-bottom: 0;"{/if}><strong>{$aItem.balance_after} {$aLang.common.currency}</strong><br />{$aItem.balance_before}&nbsp;{$aLang.common.currency}</td>
			</tr>
		{/foreach}
		{if isset($aModule.pager.links) && !empty($aModule.pager.links)}
			<tr>
				<td colspan=5 style="border-bottom: 0;">
					<div class="pagerNews" style="border: 0;">
						<div class="licznik">{$aLang.pager.page_dots} {$aModule.pager.links}</div>
						<div class="strona"><span class="zwykly">[{$aLang.pager.current_page} {$aModule.pager.current_page} {$aLang.pager.from} {$aModule.pager.total_pages} - {$aModule.lang.pager_total_items} <strong>{$aModule.pager.total_items}</strong>]</span></div>
					</div>
				</td>
			</tr>
		{/if}
	</table>
	{else}
		<div class="message" style="margin-left: 10px;">{$aModule.lang.no_user_balance_history}</div>
	{/if}
</div>