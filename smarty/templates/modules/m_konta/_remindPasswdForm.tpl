<div class="registrationForm">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.email.label}</div>
		<div class="fInput">{$aModule.form.fields.email.input}</div>
		<div class="clear"></div>
	</div>
  <div class="fRow">
		<div class="fLabel"><div style="margin-top: 24px"><label for="cap">Tekst z obrazka: *</label></div></div>
		<div class="fInput"><img src="/captcha.php" /><br /><input type="text" name="cap" id="cap" /></div>
		<div class="clear"></div>
	</div>  
	<div class="clear"></div>
	<div class="clear" style="height: 20px;"></div>
			<div class="wymagane">
				{$aLang.form.required_fields}
			</div>
		<div class="fRow" style="float: right; margin-right: 10px; display: inline;">
			<div class="clear" style="height: 10px;"></div>
			<div class="fSendButton" style="float: right;">
				<input type="submit" name="send" class="sendBig" value="Wyślij" />
			</div>
		</div>
	{$aModule.form.footer}
</div>