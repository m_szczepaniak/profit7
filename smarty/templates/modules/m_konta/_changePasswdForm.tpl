<div class="content">
	{*<span>{$aLang.common.chngpasswd_1}</span>*}
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	
	<div class="fRow">
		<div class="fLabel" style="width: 120px;">{$aModule.form.fields.passwd.label}</div>
		<div class="fInput">{$aModule.form.fields.passwd.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel" style="width: 120px;">{$aModule.form.fields.passwd2.label}</div>
		<div class="fInput">{$aModule.form.fields.passwd2.input}</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="fRow">
		<div class="fLabel" style="width: 120px">
			<div class="fRequiredF">{$aLang.form.required_fields}</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="fRow" style="padding-top: 10px;">
		<div class="fLabel" style="width: 120px;">&nbsp;</div>
		<div class="fInput left">
			<input type="submit" class="input_button" value="Zmień hasło" name="send" />
		</div>
	</div>
	{$aModule.form.footer}
	<div class="clear"></div>
</div>