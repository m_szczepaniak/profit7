{$aModule.form.JS}
<div class="registrationForm registrationFormX" style="">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.old_passwd.label}</div>
		<div class="fInput">{$aModule.form.fields.old_passwd.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow" style="">
		<div class="fLabel" style="">{$aModule.form.fields.passwd.label}</div>
		<div class="fInput" style="">{$aModule.form.fields.passwd.input}</div>
		<div class="clear"></div>
	</div>
	<div class="fRow">
		<div class="fLabel">{$aModule.form.fields.passwd2.label}</div>
		<div class="fInput">{$aModule.form.fields.passwd2.input}</div>
		<div class="clear"></div>
	</div>
	<div class="clear" style="height: 20px;"></div>
	<div class="wymagane">
		{$aLang.form.required_fields}
	</div>

	<div class="buttonLine" style="margin-top: 40px;">
		<a class="left" href="{$aModule.edit_mainpage_link}" style="background: url('/images/gfx/button-wroc-do-swoich-danych.gif'); width: 206px; height: 33px;" tabindex="9"></a>
		<input src="/images/gfx/button-zapisz-zmiany.gif" name="send" class="right" value="Zapisz zmiany" tabindex="8" type="image">
		<div class="clear"></div>
	</div>
	{$aModule.form.footer}
	<div class="clear"></div>
</div>