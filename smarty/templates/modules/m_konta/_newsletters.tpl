<form action="{$aModule.page_link_news}" method="post" name="newsletterForm" id="newsletterForm" style="padding: 10px 0 0 0;">
	<div class="fTextRow" style="padding: 5px 0 0 0;">
		<div class="koszykPodtytulNewsletter">{*{$aModule.form.fields.newsletter.label}*}Subskrypcja newsletter-a:</div>
		<div class="newsletterInfo" style="position: relative;">
			{$aModule.lang.newsletter_info}
			<div class="clear"></div>
		</div>
		<div class="newsletterGroup" style="padding-top: 0;">
		{foreach from=$aModule.categories_groups key=iGId item=aGroup}

		{assign var=ilosc value=$aModule.items.items|@count}
		{assign var=linia value=$ilosc/3+1|string_format:"%d"}
			
			{if isset($aModule.categories[$iGId])}
				{if $aGroup.show_name eq '1'}<div class="koszykPodtytul2 newsletterGroupName newsletterGroupName_{$iGId}">{$aGroup.name}</div>{/if}
				<div class="clear"></div>	
				<ul class="categories categories_{$iGId}">
					{foreach from=$aModule.categories[$iGId] item=cat name=news_list}
						{if $cat.forced == '1'}
							<input type="hidden" name="n_categories[{$cat.value}]" value="1" />
						{else}
							<li>
								<div class="fCheckbox"><input type="checkbox" name="n_categories[{$cat.value}]{if $cat.forced == '1'}_d{/if}" value="1" id="f_cat_{$cat.value}" class="check"{if $cat.forced == '1'} checked="checked" disabled="disabled"{else}{if isset($aModule.n_categories[$cat.value])} checked="checked"{/if}{/if} /></div>
								<div class="fChkLabel"><label for="f_cat_{$cat.value}" title="{$cat.label}">{$cat.label|truncate:36:"...":true}</label></div>
								<div class="clear"></div>
							</li>
							{if $i mod $linia eq $linia-1 && !$smarty.foreach.news_list.last}</ul><ul class="categories categories_{$iGId}">{/if}
						{/if}
					{/foreach}
				</ul>
			{/if}
			<div class="clear"></div>
		{/foreach}
		</div>
	</div>
	<div class="buttonLine">
		<a class="left" href="/" style="background: url('/images/gfx/button-wroc.gif'); width: 85px; height: 33px;" tabindex="9"></a>
		<input type="image" src="/images/gfx/button-zapisz-zmiany.gif" name="send" class="right" value="{$aModule.lang.newsletters_change_button}" tabindex="8" />
		<div class="clear"></div>
	</div>
</form>