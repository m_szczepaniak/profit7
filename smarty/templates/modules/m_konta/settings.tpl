{if !empty($aModule.text)}
	{$aModule.text}
{/if}

{$aModule.form.header}
{$aModule.form.err_prefix}
{$aModule.form.err_postfix}
{$aModule.form.validator}

<div class="fTextRow" style="padding: 15px 0 0 0;">
	<div class="koszykPodtytulNewsletter">
		{$aModule.lang.your_settings}
	</div>

	<div class="clear"></div>
	<div class="corp_grp_input" style="padding: 15px 0 0 0;">
				
				<div class="fRow">
					<div class="fInput">{$aModule.form.accept.reg.input} {$aModule.form.accept.reg.label}</div>
					<div class="clear"></div>
				</div>
				<div class="fRow">
					<div class="fInput">{$aModule.form.accept.promotions_agreement.input} {$aModule.form.accept.promotions_agreement.label}</div>
					<div class="clear"></div>
				</div>	
				<div class="fRow">
					<div class="fInput">{$aModule.form.accept.priv.input} {$aModule.form.accept.priv.label}</div>
					<div class="clear"><input type="hidden" name="sentForm" value="1" /></div>
				</div>	
				<div class="fRow">
					<div class="fInput">{$aLang.form.required_fields_select}</div>
					<div class="clear"></div>
				</div>	
	</div>
	<br />
	{if $aModule.discount > 0 || !empty($aModule.user_privs)}
		<div class="koszykPodtytulNewsletter">
			{$aModule.lang.your_discount_h}
		</div>
		<div class="clear"></div>

		<div class="rabatTwojeKonto" style="padding: 10px 0 0 0;">
			{if $aModule.discount > 0}
				{$aModule.lang.your_discount} <span>{$aModule.discount}%</span>
				<br />
			{/if}
			{if !empty($aModule.user_privs)}
				<br />
				{$aModule.lang.user_privileges}
				<ul style="padding: 0 0 0 10px;">
				{foreach from=$aModule.user_privs item=sPrivilege}
					<li>{$sPrivilege}</li>
				{/foreach}
				</ul>
				<br />
			{/if}
		</div>
	{/if}
</div>

<div class="buttonLine">
	<input type="image" src="/images/gfx/button-zapisz-zmiany.gif" name="send" class="right" value="{$aModule.lang.newsletters_change_button}" tabindex="8" />
	<div class="clear"></div>
</div>

{$aModule.form.footer}