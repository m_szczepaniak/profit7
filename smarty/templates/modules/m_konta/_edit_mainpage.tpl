<div class="mojeKontoNazwa">{$aModule.lang.header_data}</div>
<div class="clear"></div>
<div class="mojeKonto">
	{foreach from=$aModule.edit_sections key=i item=aButton name=tab}
	<div class="mojeKontoOpcje">
		<div class="mojeKontoTytul"><a class="edytuj" href="{$aButton.link}">{$aButton.label}</a></div>
		{if !$smarty.foreach.tab.last}	
			<a href="{$aButton.link}" class="mojeKontoEdytuj" title="{$aModule.lang.change_data}"></a>
		{else}
			<a href="{$aButton.link}" class="mojeKontoUsun" title="{$aModule.lang.change_data}"></a>
		{/if}
		<div class="clear"></div>
	</div>
	{/foreach}
	<div class="clear"></div>
</div>