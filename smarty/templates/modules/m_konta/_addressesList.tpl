<div class="mojeKontoNazwa">{$aModule.lang.header_your_data}</div>
<div class="clear"></div>
<div class="mojeKonto">
	{if !empty($aModule.addresses)}
		{foreach from=$aModule.addresses key=id item=address name=addresses_list}
			<div class="mojeAdresy">
				<div class="mojeAdresyLewo">
					<h3>{$address.address_name}</h3>
					{if $address.is_company == '1'}<strong>{$aModule.lang.type_corp}</strong>{else}<strong>{$aModule.lang.type_private}</strong>{/if}
					<div class="clear"></div>
					<div class="mojeAdresySzary">
					{if $address.default_status>-1} 
						{if $address.default_status==3} {$aModule.lang.default_3}{/if}
						{if $address.default_status==2} {$aModule.lang.default_2}{/if}
						{if $address.default_status==1} {$aModule.lang.default_1}{/if}
						{if $address.default_status==0} {$aModule.lang.is_transport_extra}{/if}
					{else} 
						{if $address.is_invoice == '1'}{$aModule.lang.is_invoice}{elseif $address.is_transport == '1'}{$aModule.lang.is_transport}{else}{$aModule.lang.is_transport_extra}{/if}{/if}</div>
				</div>	
				<div class="mojeAdresySrodek">
					{if $address.is_company == '1'}
						<div class="daneLewo">{$aModule.lang.company}</div><div class="danePrawo"> {$address.company}</div>
						<div class="clear"></div>
						{if !empty($address.nip)}<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> {$address.nip}</div>
						<div class="clear"></div>{/if}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$address.street} {$address.number}{if !empty($address.number2)}/{$address.number2}{/if}</div>
						<div class="clear"></div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$address.postal} {$address.city}</div>
					{else}
						<div class="daneLewo">{$aModule.lang.namesurname}:</div><div class="danePrawo"> {$address.name} {$address.surname}</div>
						<div class="clear"></div>
						{*<div class="daneLewo">{$aModule.lang.nip}</div><div class="danePrawo"> ---</div>
						<div class="clear"></div>*}
						<div class="daneLewo">{$aModule.lang.address}:</div><div class="danePrawo"> {$address.street} {$address.number}{if !empty($address.number2)}/{$address.number2}{/if}</div>
						<div class="daneLewo">{$aModule.lang.postal}</div><div class="danePrawo"> {$address.postal} {$address.city}</div>
					{/if}
				</div>
				<div class="mojeAdresyRight">
					<a href="{$address.edit_link}" class="mojeKontoEdytuj" title="{$aModule.lang.edit_address}" style="{if !empty($address.delete_link)}margin: 5px 0 0;{else}margin: 20px 0 0;{/if}"></a>
					{if !empty($address.delete_link)}<a href="{$address.delete_link}" class="mojeKontoUsunDane" title="{$aModule.lang.delete_address}" style="margin: 2px 0 0;"></a>{/if}
				</div>
				<div class="clear"></div>
			</div>
		{/foreach}

		<a href="{$aModule.add_address_link}" class="dodajAdres" title="{$aModule.lang.add_address}"></a>
	{else}
		<div class="mojeAdresy">
			<div class="mojeAdresyLewo">
				<h3>Brak zdefiniowanego adresu</h3>
			</div>	
			<div class="mojeAdresyRight">
				<a href="{$aModule.add_first_address_link}" class="mojeKontoUzupelnij" title="{$aModule.lang.add_first_address}" style="margin: -2px 0 0;"></a>
			</div>
			<div class="clear"></div>
		</div>
	{/if}
	<div class="buttonLine" style="margin-top: 40px;">
		<a class="left" href="{$aModule.edit_mainpage_link}" style="background: url('/images/gfx/button-wroc-do-swoich-danych.gif'); width: 206px; height: 33px;" tabindex="9"></a>
		<div class="clear"></div>
	</div>	
</div>