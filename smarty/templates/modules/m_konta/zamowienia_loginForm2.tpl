<div class="clear"></div>
<div class="order_login_form registForm registFormBig">
	{$aModule.form.header}
	{$aModule.form.err_prefix}
	{$aModule.form.err_postfix}
	{$aModule.form.validator}
	{if isset($aModule.form.fields.ref.input)}
		{$aModule.form.fields.ref.input}
	{/if}
	
{*	<div class="koszykPodtytul2"><strong></strong></div>*}
	<div class="clear"></div>
	
	<div class="left registFormBigLogin">
		<div class="fRow">
			<div class="fLabel"><h2>{$aModule.lang.login_title}</h2></div>
			<a href="{$aModule.links.fb_login}" style="margin: 15px 25px 0 0; float:right; width: 98px; height: 24px; background: url('/images/gfx/fb-login.png');"></a>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.login.label}:</div>
			<div class="fInput">{$aModule.form.fields.login.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel">{$aModule.form.fields.passwd.label}:</div>
			<div class="fInput">{$aModule.form.fields.passwd.input}</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<div class="fLabel"></div>
			<div class="fInput">
				<div class="left">{$aModule.form.fields.remember.input}</div>
				<div style="margin: 2px 0 0 3px;float: left;">{$aModule.form.fields.remember.label}</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="fRow">
			<a href="{$aModule.links.change_passwd}" class="order_login_cart_link">{$aLang.common.remind_passwd}</a>
		</div>
		<div class="clear" style="height: 6px;"></div>
		<div class="wymagane">
			{$aLang.form.required_fields}
     	<input style="margin-left: 20px;" class="order_login_button" type="image" src="/images/gfx/button-zaloguj-sie.gif" name="send" title="Zaloguj się" value="Zaloguj się" />
		</div>
	</div>
	{$aModule.form.footer}
	<div class="registFormBigRegis">
		{$aModule.lang.login_registration_description}
		<div class="clear"></div>
		<div class="formRegister">
      {*
			<form action="$aModule.links.register" method="post" name="register">
				<input type="hidden" name="ref" value="{$aModule.links.reflink}" />
				<input type="hidden" name="cart" value="1" />
				<input type="image" src="/images/gfx/button-zaloz-konto.gif" name="send" class="right send" title="Załóż konto" value="Załóż konto" />
			</form>
      *}
      <div id="do_register_form" class="right send" style="cursor: pointer; background: url('/images/gfx/button-zaloz-konto.gif'); background-repeat: no-repeat; width: 104px; height: 24px;"></div>
		</div>
	</div>
  <div class="registFormBigRegis noRegistratorion">
		{$aModule.lang.order_no_login}
    <br /><br /><br />
		<div class="clear"></div>
		<div class="formRegister">
      <input id="do_order_without_registration" title="Zakupy bez rejestracji" type="image" src="/images/gfx/button-zakupy-bez-rejestracji.png" name="send" class="right send" value="Zakupy bez rejestracji" />
      {*
			<form action="{$aModule.links.register}" method="post" name="register">
				<input type="hidden" name="ref" value="{$aModule.links.reflink}" />
				<input type="hidden" name="cart" value="1" />
				<input type="image" src="/images/gfx/button-zaloz-konto.gif" name="send" class="right send" value="Załóż konto" />
			</form>
      *}
		</div>
	</div>
</div>