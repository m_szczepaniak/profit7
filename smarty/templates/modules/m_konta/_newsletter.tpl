{literal}
<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){
    $("#newsletterMore").click(function () {
        $(".newsletterGroup").slideToggle("slow");
      });
});           
//]]>
</script>
{/literal}

<div class="fTextRow" style="padding: 5px 0 0 0;">
	<div class="koszykPodtytulNewsletter">{*{$aModule.form.fields.newsletter.label}*}Newsletter z nowościami w Profit24:</div>
	
	<div class="newsletterInfo" style="position: relative;">
		{$aModule.lang.newsletter_info}
		<div id="newsletterMore" style="position: relative;"></div>
		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>	
		<div class="newsletterGroup">
		{foreach from=$aModule.form.newsletter.categories_groups key=iGId item=aGroup}
			{literal}
				<script type="text/javascript">
				  $(document).ready(function(){
		
				    $(".newsletterGroupName_{/literal}{$iGId}{literal}").click(function () {
				      $(".categories_{/literal}{$iGId}{literal}").slideToggle("slow");
				    });
		
				  });
				</script>
			{/literal}
			
		{assign var=ilosc value=$aModule.items.items|@count}
		{assign var=linia value=$ilosc/3+1|string_format:"%d"}
			
			{if isset($aModule.form.newsletter.categories[$iGId])}
				{if $aGroup.show_name eq '1'}<div class="koszykPodtytul2 newsletterGroupName newsletterGroupName_{$iGId}">{$aGroup.name}</div>{/if}
				<div class="clear"></div>	
				<ul class="categories categories_{$iGId}">
					{foreach from=$aModule.form.newsletter.categories[$iGId] item=cat name=news_list}
						{if $cat.forced == '1'}
							<input type="hidden" name="n_categories[{$cat.value}]" value="1" />
						{else}
							<li>
								<div class="fCheckbox"><input type="checkbox" name="n_categories[{$cat.value}]{if $cat.forced == '1'}_d{/if}" value="1" id="f_cat_{$cat.value}" class="check"{if $cat.forced == '1'} checked="checked" disabled="disabled"{else}{if isset($aModule.form.newsletter.n_categories[$cat.value])}{$aModule.form.newsletter.n_categories[$cat.value]}{/if}{/if} /></div>
								<div class="fChkLabel"><label title="{$cat.label}" for="f_cat_{$cat.value}">{$cat.label|truncate:36:"...":true}</label></div>
								<div class="clear"></div>
							</li>
							{if $i mod $linia eq $linia-1 && !$smarty.foreach.news_list.last}</ul><ul class="categories categories_{$iGId}">{/if}
						{/if}
					{/foreach}
				</ul>
			{/if}
			<div class="clear"></div>
		{/foreach}
		</div>
	<div class="clear"></div>
</div>