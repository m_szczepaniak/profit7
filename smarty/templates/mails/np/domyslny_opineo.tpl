<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 16px;color: #000;">
	<table cellpadding="0" cellspacing="0" width="620" style="margin: 50px auto 60px;">
		<tr>
			<td align="center">
				<a href="http://www.nieprzeczytane.pl"><img src="images/gfx/mail_opinie_logo.png" alt="" style="border: 0;" /></a><br />
				<br />
				</td>
		</tr>
		<tr>
			<td colspan="2">
				<table style=" text-align: center;" width="100%" cellpadding="0" cellspacing="0">
<tr>
			<td colspan="2"  align="center" style="color:dimgray; font-size: 16px;">
				<br />
				<b>
				Drogi czytelniku,<br />
				bardzo dziękujemy za zaufanie i wybranie naszej księgarni.</b><br />
				<br />
				Będzie nam niezmiernie miło, jeżeli podzielisz się swoją opinią na nasz temat: <br />
				<br />
				<p align="center">
				<a href="http://www.okazje.info.pl/sklep-internetowy/nieprzeczytane-pl/#dodaj-opinie"><img src="images/gfx/mail_opinie_marian.png" style="border: 0;"/></a>
				<a href="http://www.facebook.com/nieprzeczytane/reviews/"><img src="images/gfx/mail_opinie_stefan.png" style="border: 0;"/></a>
				</p>
			</td>
		</tr>
				
				
				
				</table>
			</td>
		</tr>
		<tr>
		<td align="center">
		<a href="http://instagram.com/nieprzeczytane.pl/"><img src="images/gfx/insta.gif" style="border: 0;"/></a>
		
		</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="color:dimgray; font-size: 16px;">
				<br><br>
				Zapraszamy do odwiedzenia<br />naszej księgarni w przyszłości.
				<br />
				<br />
				<b>Pozdrawiamy,</b><br />
				<b>Zespół niePrzeczytane.pl</b>		
			</td>
			
		</tr>
		<tr>
			<td colspan="2" align="center" style="font-size: 11px; ">
				<img src="images/gfx/mail_opinie_sledz.png" width="45%" style="border: 0;"/>			
				<a href="http://facebook.com/nieprzeczytane"><img src="images/gfx/mail_opinie_facebook.png" width="10%" style="border: 0;margin-bottom:35px"/></a>
				<a href="http://twitter.com/niePrzeczytane"><img src="images/gfx/mail_opinie_twitter.png" width="10%" style="border: 0;margin-bottom:35px"/></a>
				<a href="http://instagram.com/nieprzeczytane.pl"><img src="images/gfx/mail_opinie_insta.png" width="10%" style="border: 0;margin-bottom:35px"/></a>
				<a href="http://youtube.com/c/nieprzeczytanepl"><img src="images/gfx/mail_opinie_youtube.png" width="10%" style="border: 0;margin-bottom:35px"/></a>
				
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="padding: 12px 0 12px; font-size: 11px;border: 1px solid #e6e6e6;background: #f8f8f8; ">
								<p style="color:dimgray; line-height:15px">
				
				e-mail: <b><a href="mailto:kontakt@nieprzeczytane.pl" style="text-decoration: none; color:dimgray">kontakt@nieprzeczytane.pl</a></b> <br />
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="font-size: 10px;color: #474747;padding: 25px 0 24px;">
				Prosimy nie odpowiadać na tę wiadomość, została ona wysłana automatycznie. <br> Zapraszamy do kontaktu poprzez 
				Centrum pomocy niePrzeczytane.pl <a style="color: #104586;text-decoration: underline" href="http://www.nieprzeczytane.pl/pomoc">http://www.nieprzeczytane.pl/pomoc</a>
			</td>
		</tr>
	</table>
</body>
</html>