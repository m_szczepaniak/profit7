<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 13px;color: #000;">
	<table cellpadding="0" cellspacing="0" width="620" style="margin: 50px auto 60px;">
		<tr>
			<td align="center">
				<a href="http://www.profit24.pl"><img src="http://www.profit24.pl/images/gfx/mail_opinie_logo.png" alt="" style="border: 0;" /></a><br />
				<br />
				</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0 0 12px;">
				<table style="border-radius: 15px 15px 0 0;border: 1px solid #e6e6e6;background: #f8f8f8; text-align: center;" width="100%" cellpadding="0" cellspacing="0">
<tr>
			<td colspan="2"  align="center" style="border-bottom: 2px solid #e6e6e6; padding: 0 0 12px;font-size: 13px;">
				<br />
				<b>
				Szanowny Kliencie, <br />
				dziękujemy za dokonanie zakupu w naszej księgarni.<br />
				Mamy nadzieję, że spełniliśmy pokładane w nas oczekiwania.</b>
				<br /><br />
				Opinię na temat naszej księgarni możesz zamieścić<br /> na portalu Facebook lub w serwisie okazje.info.pl</b><br />
				<br />
				<p align="center">
				<a href="https://www.facebook.com/pg/profit24pl/reviews/"><img src="http://www.profit24.pl/images/gfx/mail_opinie_facebook_star.png" style="border: 0; padding: 5px;"/></a>
				
				<a href="http://www.okazje.info.pl/sklep-internetowy/profit24-pl/#dodaj-opinie"><img src="http://www.profit24.pl/images/gfx/mail_opinie_okazje.png" style="border: 0;padding: 5px;"/></a>
				</p>
				
			</td>
		</tr>
				
				
				
				</table>
			</td>
		</tr>
		
		<tr>
			<td colspan="2" align="center" style="border-bottom: 2px solid #e6e6e6;padding: 0 0 18px;font-size: 13px;">
				Z góry dziękujemy za wszystkie pozytywne komentarze, które są dla nas motywacją do dalszej pracy.<br /> Jesteśmy także wdzięczni za uwagi, które są dla nas zawsze sygnałem do poprawy.<br />
				<br />
				<b>Pozdrawiamy serdecznie!</b><br />
				<b>Zespół Profit24.pl</b>		
			</td>
			
		</tr>
	
		<tr>
		<td colspan="2" align="center" style=" border-radius:0 0 15px 15px; border: 1px solid #e6e6e6;padding: 12px 0 12px; font-size: 11px; background: #f8f8f8;"> 
				
				<p style="color:dimgray;">
				<b>Bądź na bieżąco z promocjami.<br /> Śledź nas na:</b>
				</p>			
				<a href="http://facebook.com/profit24pl"><img src="http://www.profit24.pl/images/gfx/mail_opinie_facebook.png" width="10%" style="border: 0;"/></a>
				<a href="http://plus.google.com/u/1/109862193070558060199"><img src="http://www.profit24.pl/images/gfx/mail_opinie_google.png" width="10%" style="border: 0;"/></a>
				<a href="http://www.youtube.com/channel/UC0s4H7BkcLZ1wxG874syc2A"><img src="http://www.profit24.pl/images/gfx/mail_opinie_youtube.png" width="10%" style="border: 0;"/></a>
				
				
				
				<p style="color:dimgray; line-height:15px">
				e-mail: <b><a href="mailto:kontakt@profit24.pl" style="text-decoration: none; color:dimgray">kontakt@profit24.pl</a></b> <br />
				</p>
				</td>
		
			
		</tr>
		
	
		
		<tr>
			<td colspan="2" align="center" style="font-size: 10px;color: #474747;padding: 25px 0 24px;">
				Ta wiadomość została wysłana z adresu e-mail przeznaczonego wyłącznie do przesyłania informacji, który nie akceptuje poczty przychodzącej. Proszę nie odpowiadać na tę wiadomość. W razie jakichkolwiek pytań proszę odwiedzić Centrum pomocy profit24.pl <a style="color: #104586;text-decoration: underline" href="http://www.profit24.pl/pomoc">http://www.profit24.pl/pomoc</a> lub skontaktować się z nami w dowolnie wybrany sposób.
			</td>
		</tr>
		
	</table>
</body>
</html>