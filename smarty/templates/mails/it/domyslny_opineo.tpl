<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 13px;color: #000;">
	<table cellpadding="0" cellspacing="0" width="620" style="margin: 50px auto 60px;">
		<tr>
			<td width="350" style="padding: 0 0 24px;font-size: 13px;">{$sContent}</td>
			<td align="right" style="padding: 0 0 24px;">
				<a href="http://www.ksiegarnia.it"><img src="logo.gif" alt="" style="border: 0;" /></a><br />
				<br />
				<a href="{$sLink2}"><img src="slucham-swoich-klientow.gif" alt="" style="border: 0;" /></a>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0 0 24px;">
				<table style="border: 1px solid #e6e6e6;background: #f8f8f8; text-align: center;" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<a href="{$sLink}"><img src="wystaw-opinie-na.gif" alt="" style="padding: 18px 0;border: 0;" /></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom: 1px solid #e6e6e6;padding: 0 0 24px;font-size: 13px;">
				<b>Pod powyższym linkiem znajduje się krótka ankieta, której wypełnienie nie zajmie więcej<br /> niż 1 minutę.</b><br />
				<br />
				Twoja ocena pozwoli nam poprawiać jakość naszej pracy, by kolejne Twoje zakupy były jeszcze<br /> bardziej satysfakcjonujące.
			</td>
		</tr>
		<tr>
			<td style="border-bottom: 2px solid #e6e6e6;padding: 25px 0 24px;font-size: 13px;">
				W wolnej chwili zapraszamy Cię na nasz profil na portalu:<br />
				<br />
				Z góry dziękujemy za poświęcony nam czas i zapraszamy ponownie do zakupów na <a style="color: #104586;text-decoration: underline;font-weight: bold;" href="http://www.ksiegarnia.it">www.ksiegarnia.it</a><br />
				<br />
				<b>Pozdrawiamy</b><br />
				<b>Zespół Internetowa Księgarnia.IT</b>	
			</td>
			<td style="border-bottom: 2px solid #e6e6e6;" align="right">
				<a href="https://www.facebook.com/ksiegarnia.it"><img src="facebook.gif" alt="" style="border: 0;" /></a>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="padding: 25px 0 24px;border-bottom: 1px solid #e6e6e6;font-size: 11px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="font-size: 10px;color: #474747;padding: 25px 0 24px;">
				Ta wiadomość została wysłana z adresu e-mail przeznaczonego wyłącznie do przesyłania informacji, który nie akceptuje poczty przychodzącej. Proszę nie odpowiadać na tę wiadomość. W razie jakichkolwiek pytań proszę odwiedzić Centrum pomocy Internetowa Księgarnia.IT <a style="color: #104586;text-decoration: underline" href="http://www.ksiegarnia.it/pomoc">http://www.ksiegarnia.it/pomoc</a> lub skontaktować się z nami w dowolnie wybrany sposób.
			</td>
		</tr>
	</table>
</body>
</html>