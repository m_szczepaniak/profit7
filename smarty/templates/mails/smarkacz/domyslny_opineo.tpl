<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 16px;color: #000;">
<table cellpadding="0" cellspacing="0" width="620" style="margin: 50px auto 60px;">
    <tr>
        <td align="center" style="background: #f8f8f8;">
            <a href="http://www.smarkacz.pl"><img src="http://www.smarkacz.pl/images/gfx/mail_opinie_logo.jpg" alt=""
                                                  style="border-bottom: 8px solid #e6e6e6;"/></a><br/>
            <br/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table style=" text-align: center; background: #f8f8f8;" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="center" style="color:dimgray;font-size: 16px;">
                        <br/>
                        <b>
                            Dziękujemy za zakupy<br/> w najbardziej kreatywnym sklepie w sieci :)</b><br/>
                        <br/>
                        Napisz co o nas myślisz
                        <br/>w serwisie Ceneo.pl lub na Facebooku <br/> klikając poniższe puzzle: <br/>
                        <br/>
                </tr>
                <tr cellpadding="0" cellspacing="0" style="border: 0;">
                    <td cellpadding="0" cellspacing="0" style="border: 0;">
                            <a href="http://www.ceneo.pl/sklepy/smarkacz.pl-s22596"><img
                                        src="http://www.smarkacz.pl/images/gfx/mail_opinie_ceneo.png" align="right"
                                        style="border: 0; float: right;"/></a>
                    </td>
                    <td cellpadding="0" cellspacing="0" style="border: 0;">
                        <a href="http://www.facebook.com/smarkaczpl/reviews/"><img
                                    src="http://www.smarkacz.pl/images/gfx/mail_opinie_facebook.png" align="left"
                                    style="border: 0; float: left;"/></a>
                    </td>
                </tr>


            </table>
        </td>
    </tr>

    <tr>
        <td colspan="2" align="center" style="color:dimgray; font-size: 16px;background: #f8f8f8;">
            Nie możemy się już doczekać kiedy znowu do nas wrócisz <br/> po kolejne kreatywne produkty.
            <br/>
            <br/>
            <b>Pozdrawiamy,</b><br/>
            <b>Zespół smarkacz.pl</b>
        </td>

    </tr>
    <tr>
        <td colspan="2" align="center" style=" font-size: 11px; ">
            <a href="http://facebook.com/smarkaczpl"><img src="http://www.smarkacz.pl/images/gfx/mail_opinie_sledz.png"
                                                          style="border: 0;"/></a>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" style="font-size: 11px;">
            <p style="color:dimgray; line-height:15px">
                e-mail: <b><a href="mailto:kontakt@smarkacz.pl" style="text-decoration: none; color:dimgray">kontakt@smarkacz.pl</a></b>
                <br/>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" style="font-size: 10px;color: #474747;padding: 25px 0 0;">
            Ten e-mail został wysłany automatycznie przez kreatywnego robota, który nie lubi poczty przychodzącej.<br/>
            Proszę nie odpowiadać na tę wiadomość. Robot nie umie czytać. <br/>W razie jakichkolwiek pytań proszę
            odwiedzić adres <a style="color: #104586;text-decoration: underline" href="http://www.smarkacz.pl/pomoc">http://www.smarkacz.pl/pomoc</a>
            <br/>lub skontaktować się z naszym Biurem Obsługi Klienta.
        </td>
    </tr>
</table>
</body>
</html>