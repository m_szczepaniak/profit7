<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{literal}
<style type="text/css">
	a {
		color: #1D79B8;
		text-decoration: underline;
	}
	p {
		padding: 0 0 23px;
		margin: 0;
	}
</style>
{/literal}
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 15px; margin: 0; font-size: 12px;color: #000;">
	<div style="margin: 0 auto; width: 650px; font-size: 12px;">	
		<img src="logoSMARKACZ_mail.png"/><br/>
		<br/>
		<br/>
		{$sContent}
		<br>
		<br>
		{*$sFooter*}
		<p>
Pozdrawiamy,<br />
Zespół smarkacz.pl<br />
<a href="http://www.smarkacz.pl/kontakt-z-nami/"><u>http://www.smarkacz.pl/kontakt-z-nami/</u></a> 
</p>
<p>
&nbsp;
</p>
<p>
Prosimy
<strong>NIE
odpowiadać</strong>
na tę wiadomość, ponieważ wysłana jest z adresu e-mail
przeznaczonego do przesyłania komunikatów systemowych i poczta
przychodząca nie jest akceptowana.
</p>
<p>
W
przypadku pytań lub wątpliwości proszę odwiedzić Centrum Pomocy
smarkacz.pl na stronie <a href="http://www.smarkacz.pl/kontakt-z-nami/"><u>http://www.smarkacz.pl/kontakt-z-nami/</u></a> <u>
</u>i
wybrać dogodną dla siebie formę kontaktu z naszym zespołem.
</p>
	</div>
</body>
</html>