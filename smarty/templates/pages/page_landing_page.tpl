<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$sLang}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={$sCharset}" />
<title>{$aPage.title}</title>
{if !empty($aMeta.description)}
<meta name="description" content="{$aMeta.description|htmlspecialchars}" />
{/if}
{if !empty($aMeta.keywords)}
<meta name="Keywords" content="{$aMeta.keywords}" />
{/if}
{if !empty($aMeta.robots)}
  <meta name="Robots" content="{$aMeta.robots}" />
{else}
  <meta name="Robots" content="index,follow" />
{/if}
<meta name="viewport" content="width=device-width, initial-scale=1.0">

{if isset($aFBOGTags)}
{foreach from=$aFBOGTags key=og_key item=og_value}
<meta property="{$og_key}" content="{$og_value|htmlspecialchars}" />
{/foreach}
{/if}
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<!--[if lt IE 7]>
<link href="/css/styles_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link rel="stylesheet" href="/{php} require_once('css_assets.php'); echo $minified->merge('css/style.css','css',$included_styles).'?v='.md5(file_get_contents('css/style.css')); {/php}" type="text/css" />
<script type="text/javascript" src="/{php} require_once('js_assets.php');echo $minified->merge('js/scripts.js','js',$included_scripts).'?v='.md5(file_get_contents('js/scripts.js')); {/php}"></script>
<script type="text/javascript" src="/js/slimbox205.js"></script>
<script type="application/ld+json">
{literal}{{/literal}
  "@context" : "http://schema.org",
  "@type" : "WebSite",
  "name" : "Profit24.pl",
  "alternateName" : "{$aPage.name}",
  "url" : "{$sBaseHref}",
  "potentialAction": {literal}{{/literal}
      "@type": "SearchAction",
      "target": "{$sBaseHref}/szukaj?q={literal}{{/literal}search_term_string{literal}}{/literal}&result=1",
      "query-input": "required name=search_term_string"
    {literal}}{/literal}
{literal}}{/literal}
{literal}{{/literal}
  "@context": "http://schema.org",
  "@type": "Organization",
  "url" : "{$sBaseHref}",
  "logo": "{$sBaseHref}/images/gfx/logo_204.gif"
{literal}}{/literal}
</script>
{*{if isset($sHeaderJS)}{$sHeaderJS}{/if}*}
{*{if isset($sHeaderJSIncludes)}{$sHeaderJSIncludes}{/if}*}
{include file='pages/parts_pages/shared_js.tpl'}
</head>
<body style="background-color: #EFEFEF;">
		<noscript><img height="1" width="1" style="display:none" alt="" src="https://www.facebook.com/tr?id=1624496397638191&ev=PageView&noscript=1" /></noscript>
{include file="pages/parts_pages/dialog.tpl"}

<div style="margin: 0 auto;">
				{if isset($sMsg)}{$sMsg}{/if}
				{if isset($aAreas.3)}{$aAreas.3}{/if}
</div>

{if isset($sGoogleAnalytics)}
	{$sGoogleAnalytics}
{/if}
</body>
</html>