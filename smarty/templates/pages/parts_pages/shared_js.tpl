{literal}
	<script type="text/javascript">
		"use strict";(function(scriptUrl){if(!window.bb){window.bb=function(){
			for(var _len=arguments.length,params=Array(_len),_key=0;_key<_len;_key++){
				params[_key]=arguments[_key]}return window.bb.q.push(params)};window.bb.q=[]
		;var script=document.createElement("script")
				;var firstScript=document.getElementsByTagName("script")[0];script.async=true
		;script.src=scriptUrl;firstScript.parentNode.insertBefore(script,firstScript)}
		})("https://shop.buybox.click/bb-shop-133.js");
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WT5QKXN');</script>
	<!-- End Google Tag Manager -->

	<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq)return;
			n = f.fbq = function () {
				n.callMethod ?
						n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq)f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
				'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1624496397638191');
		fbq('track', 'PageView');
	</script>

{/literal}