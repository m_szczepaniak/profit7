<div id="dialog-form" title="Dodaj do koszyka" style="display: none">
  <div class="ui-widget add-card-info" style="display: none">
      <div class="ui-corner-all" style="padding: .8em .8em;"> 
          <p>
              <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
              <strong id="add-card-info-message"></strong>
          </p>
      </div>
  </div>
  
  <div class="dialog-form-content-item">
    <table>
      <tr>
        <th style="width: 280px;">Pozycja</th>
        <th style="width: 95px">&nbsp;</th>
        <th style="width: 70px;">Ilość</th>
      </tr>
      
      <tr>
        <td>
          <div id="add-card-dialog-image"></div>
          <div id="add-card-dialog-title"></div>
        </td>
        <td>
          <div id="add-card-dialog-type-audio"></div>
          <div id="add-card-dialog-type-print"></div>
        </td>
        <td>
          <form id="form-add-card">
            <fieldset>
              <label for="count"></label>
              <input tabindex="0" maxlength="4" value="1" type="text" name="count" id="count" />
            </fieldset>
          </form>
          <div class="updownbuttons">
						<a class="up_item" onclick="changeCountUp();" href="javascript:void(0);"></a>
						<a class="down_item" onclick="changeCountDown();" href="javascript:void(0);"></a>
					</div>
        </td>
      </tr>
    </table>
    <p class="validateTips"></p>
    
  </div>
</div>

<div id="popup-inline" title="Informacja" style="display: none">
  <div class="ui-widget add-card-info">
      <div class="ui-corner-all" style="padding: .8em .8em;"> 
          <p>
              <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
              <strong class="popup-content-message"></strong>
          </p>
      </div>
  </div>
</div>