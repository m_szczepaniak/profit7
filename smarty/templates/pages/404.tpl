<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>404 - Strona nie istnieje - profit24.pl</title>
		<meta name="Robots" content="noindex,nofollow" />
		<link href="/css/styles.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="/css/style-404.css" type="text/css">
		<!--[if lt IE 7]>
			<link href="/css/styles_ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<link rel="icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<script src="/js/functions_04072013.js"></script>
		<script src="/js/jquery/jquery-1.9.1.min.js"></script>
		<script src="/js/fmanalytics.js"></script>
	</head>
	<body>
		<noscript><img height="1" width="1" style="display:none" alt="" src="https://www.facebook.com/tr?id=1624496397638191&ev=PageView&noscript=1" /></noscript>
		{include file="pages/parts_pages/dialog.tpl"}
		<div id="info_cookies" style="display:none; text-align: center; background: url('/images/gfx/box_tytul_srodek.gif') repeat-x 0; padding: 10px;">
			Strona używa cookie w celu świadczenia usług. Korzystając ze strony zgadzasz się na użycie plików cookie. <a href="/polityka-cookies">Więcej informacji o cookie</a>
			&nbsp;<a href="javascript:void(0);" id="hide_info_cookies" style="text-decoration: none; padding-top: 5px; color: #000; font-size: 1.6em">×</a>
			<script>
				{literal}
$("#hide_info_cookies").click( function () {
	SetCookie('info_cookies', '66', 0);
	$("#info_cookies").hide();
});
if (GetCookie('info_cookies') !== '66') {
	$("#info_cookies").show();
}
{/literal}
			</script>
		</div>
		<div id="kontener">
			<div id="fb-root"></div>
			{literal}
			<script type="application/javascript">(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.async=true; js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			{/literal}
			<div id="page404">
				<div class="page404__top">
					<img src="/images/gfx/404_top.png" alt="">
					<p>Podana strona nie istnieje</p>			
				</div>
				<div class="page404__content">
					<a href="/" class="page404__linkhome">Strona główna</a>
					<div class="page404__search">
						<form action="/szukaj" method="get" name="searchBoxForm">
							<input autocomplete="off" name="q" id="qibox" type="text">
							<div class="page404__search__submit">
								<input name="submit" value="" type="submit">
							</div>
							<input type="hidden" name="result" value="1" />
						</form>
					</div>
					<div class="clear"></div>
				</div>
				<div class="page404__book">
					<img src="/images/gfx/404_book.png" alt="">
				</div>
				{if isset($aAreas.10)}{$aAreas.10}{/if}
			</div>
		</div>
		{*<script src="http://app.sugester.pl/profit24/widget.js"></script>*}
		{if isset($sGoogleAnalytics)}
		{$sGoogleAnalytics}
		{/if}
		<script>
			{if !empty($smarty.get.abpid) && !empty($smarty.get.abpcid)}
{literal}
(function(abpid, abpcid , param1 , param2, param3) {

	if(param1 == ""){
		param1 = null;
	}
	if(param2 == ""){
		param2 = null;
	}
	if(param3 == ""){
		param3 = null;
	}

	var a4bstag = document.createElement('script');
	a4bstag.type = "text/javascript";
	a4bstag.async = true;
	a4bstag.src = 'https://transactions.a4b-tracking.com/aff/' + abpid + '/' + abpcid  + "/" + param1 + "/" + param2 + "/" + param3;
	document.getElementsByTagName("head")[0].appendChild(a4bstag);
}){/literal}("{$smarty.get.abpid}", "{$smarty.get.abpcid}", "{$smarty.get.abpar1}", "{$smarty.get.abpar2}", "{$smarty.get.abpar3}");
{/if}
		</script>

		{literal}
		<script>
			var _smid = "9xggneingrv9ewih";
var _smcustom = true;
(function(w, r, a, sm, s ) {
	w['SalesmanagoObject'] = r;
	w[r] = w[r] || function () {( w[r].q = w[r].q || [] ).push(arguments)};
	sm = document.createElement('script'); sm.type = 'text/javascript'; sm.async = true; sm.src = a;
	s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(sm, s);
})(window, 'sm', ('https:' == document.location.protocol ? 'https://' : 'http://') + 'app3.emlgrid.com/static/sm.js');
		</script>
		{/literal}
	</body>
</html>
