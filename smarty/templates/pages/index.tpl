<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$sLang}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={$sCharset}" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>{$aPage.title}</title>
{if !empty($aMeta.description)}
<meta name="Description" content="{$aMeta.description|htmlspecialchars}" />
{/if}
{if !empty($aMeta.keywords)}
<meta name="Keywords" content="{$aMeta.keywords}" />
{/if}
{if !empty($aMeta.robots)}
  <meta name="Robots" content="{$aMeta.robots}" />
{else}
  <meta name="Robots" content="index,follow" />
{/if}
{if isset($aFBOGTags)}
{foreach from=$aFBOGTags key=og_key item=og_value}
<meta name="{$og_key}" content="{$og_value|htmlspecialchars}" />
{/foreach}
{/if}
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<!--[if lt IE 7]>
<link href="/css/styles_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link rel="stylesheet" href="/{php} require_once('css_assets.php'); echo $minified->merge('css/style.css','css',$included_styles).'?v='.md5(file_get_contents('css/style.css')); {/php}" type="text/css" />
<script type="text/javascript" src="/{php} require_once('js_assets.php');echo $minified->merge('js/scripts.js','js',$included_scripts).'?v='.md5(file_get_contents('js/scripts.js')); {/php}"></script>
<script type="application/ld+json">
{literal}{{/literal}
  "@context" : "http://schema.org",
  "@type" : "WebSite",
  "name" : "Profit24.pl",
  "alternateName" : "{$aPage.name}",
  "url" : "{$sBaseHref}",
  "potentialAction": {literal}{{/literal}
      "@type": "SearchAction",
      "target": "{$sBaseHref}/szukaj?q={literal}{{/literal}search_term_string{literal}}{/literal}&result=1",
      "query-input": "required name=search_term_string"
    {literal}}{/literal}
{literal}}{/literal}
{literal}{{/literal}
  "@context": "http://schema.org",
  "@type": "Organization",
  "url" : "{$sBaseHref}",
  "logo": "{$sBaseHref}/images/gfx/logo_204.gif"
{literal}}{/literal}
</script>
{if isset($sHeaderJS)}{$sHeaderJS}{/if}
{if isset($sHeaderJSIncludes)}{$sHeaderJSIncludes}
{/if}
{include file='pages/parts_pages/shared_js.tpl'}
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
{literal}
	window.criteo_q = window.criteo_q || [];
	var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
	window.criteo_q.push(
		{ event: "setAccount", account: 57447},
		{ event: "setEmail", email: "{/literal}{if isset($smarty.session.w_user.email)}{$smarty.session.w_user.email}{/if}{literal}" },
		{ event: "setSiteType", type: deviceType},
		{ event: "viewHome"});
{/literal}
</script>
</head>
<body>
	<noscript><img height="1" width="1" style="display:none" alt=""
				   src="https://www.facebook.com/tr?id=1624496397638191&ev=PageView&noscript=1"
		/></noscript>
<div id="scroll-to-top">
	<a href="#" class="arrow"></a>
</div>
	{literal}
	<script>
		$(document).scroll(function(e){
			if($(document).scrollTop() > 500)
				$('#scroll-to-top').show();
			else
				$('#scroll-to-top').hide();
		});

	</script>
	{/literal}
{include file="pages/parts_pages/dialog.tpl"}
<div id="info_cookies" style="display:none; text-align: center; background: url('/images/gfx/box_tytul_srodek.gif') repeat-x 0; padding: 10px;">
   Korzystając ze strony zgadzasz się na użycie plików cookie oraz warunki polityki prywatności w celu świadczenia usług. <a href="/polityka-cookies">Więcej informacji o cookie</a>
  &nbsp;<a href="javascript:void(0);" id="hide_info_cookies" style="text-decoration: none; padding-top: 5px; color: #000; font-size: 1.6em">×</a>
  <script>
    {literal}
    $("#hide_info_cookies").click( function () {
		SetCookie('info_cookies', '66', 0);
		$("#info_cookies").hide();
    });
    if (GetCookie('info_cookies') !== '66') {
      $("#info_cookies").show();
    }
    {/literal}
  </script>
</div>
<div id="kontener">
	<div class="poziomy2">
		<div id="logo"><h1><a href="/"><img src="/images/gfx/logo_204.gif" alt="{$aPage.name}" /></a></h1></div>
		{if isset($aAreas.2)}{$aAreas.2}{/if}
	</div>
	<div class="poziomy">
		{if isset($aAreas.6)}{$aAreas.6}{/if}
	</div>
  {if isset($sMsg)}{$sMsg}{/if}
	<div class="poziomy">
		<div class="kolLewa">
			{if isset($aAreas.8)}{$aAreas.8}{/if}
			{if isset($aAreas.9)}{$aAreas.9}{/if}
		</div>
		<div class="kolPrawa">
			{if isset($aAreas.5)}{$aAreas.5}{/if}
		</div>
	</div>

	<div id="fb-root"></div>
	{literal}
	<script type="application/javascript">(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) {return;}
	  js = d.createElement(s); js.id = id;
	  js.async=true; js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	{/literal}
	<div class="boxFacebook">
		<div class="fb-like-box" data-href="http://www.facebook.com/pages/Ksi%C4%99garnia-internetowa-profit24compl/318972512575" data-width="1002" data-show-faces="true" data-stream="false" data-header="false"></div>
	</div>
	
	<div id="dol">
		<div id="dolTytul">
			<div id="dolTytulLewo"></div>
			<div id="dolTytulSrodek">
				<h2>Profit24.pl</h2>
			</div>
			<div id="dolTytulPrawo"></div>
		</div>
		<div id="dolSrodek">
			{if isset($aAreas.7)}{$aAreas.7}{/if}
		</div>
		<div id="dolDol">
			<div id="dolDolLewo"></div>
			<div id="dolDolSrodek"></div>
			<div id="dolDolPrawo"></div>
		</div>
	</div>
	<div class="clear"></div>

	<div id="footer">
		<div id="loga">
			<div id="logaTytul">Akceptujemy płatności:</div>
			<div id="logaAll">
				<div class="logaRamka" style="margin: 0 0 0 30px; background: #fff;">
					<div id="logoMtransfer"></div>
					<div id="logoMulti"></div>
					<div id="logoPrzelew24"></div>
					<div id="logoNordea"></div>
					<div id="logoInteligo"></div>
					<div id="logoVisa"></div>
					<div id="logoMastercard"></div>
					<div id="logoJcb"></div>
					<div id="logoNiebieskie"></div>
					<div id="logoMaestro"></div>
					<div id="logoPko_sa"></div>		
				</div>
				<div class="logaRamka" style="margin: 0 0 0 45px; background: #fff;">
					<div id="logoMillennium"></div>
					<div id="logoBank_bgz"></div>
					<div id="logoBank_bph"></div>
					<div id="logoBos"></div>
					<div id="logoBosbank24"></div>
					<div id="logoCiti_handlowy"></div>
					<div id="logoPekao24"></div>
					<div id="logoDeutsche_bank"></div>
				</div>
				<div class="logaRamka" style="margin: 0 0 0 85px; background: #fff;">
					<div id="logoDiners"></div>
					<div id="logoEbgz"></div>
					<div id="logoIng"></div>
					<div id="logoKb24"></div>
					<div id="logoKradyt_bank"></div>
					<div id="logoLukas"></div>
					<div id="logoPko"></div>
				</div>
			</div>
		</div>
    {literal}
    <script>
      $('#logaAll').cycle({ 
          fx:    'scrollUp', 
          sync:   0, 
          delay: -2000 
      });	
    </script>
    {/literal}
		<div id="footer1">{$aPage.footer}</div>
		<div id="footer2">{$aRealization.text} {$aRealization.link}</div>
	</div>
</div>
{*<script src="https://s3-eu-west-1.amazonaws.com/app.sugester.pl/profit24/widget.js"></script>*}
{if isset($sGoogleAnalytics)}
	{$sGoogleAnalytics}
{/if}
{*
{if $smarty.server.REMOTE_ADDR == '83.18.63.86'}
	{literal}
	<div id="UnizetoSealMO1OFYPE"></div><script type="text/javascript">document.write(unescape("%3Cscript src='https://seal.certum.pl/static/js/seal/seal.js' type='text/javascript'%3E%3C/script%3E"));</script><script type="text/javascript">Unizeto.Seal.install("MO1OFYPE", 6);</script>
	{/literal}
{/if}
*}
<script>
    {if !empty($smarty.get.abpid) && !empty($smarty.get.abpcid)}
    {literal}
    (function(abpid, abpcid , param1 , param2, param3) {

      if(param1 == ""){
        param1 = null;
      }
      if(param2 == ""){
        param2 = null;
      }
      if(param3 == ""){
        param3 = null;
      }

      var a4bstag = document.createElement('script');
      a4bstag.type = "text/javascript";
      a4bstag.async = true;
      a4bstag.src = 'https://transactions.a4b-tracking.com/aff/' + abpid + '/' + abpcid  + "/" + param1 + "/" + param2 + "/" + param3;
      document.getElementsByTagName("head")[0].appendChild(a4bstag);
    }){/literal}("{$smarty.get.abpid}", "{$smarty.get.abpcid}", "{$smarty.get.abpar1}", "{$smarty.get.abpar2}", "{$smarty.get.abpar3}");
  {/if}
</script>

	{literal}
		<script>
            var _smid = "9xggneingrv9ewih";
            var _smcustom = true;
            (function(w, r, a, sm, s ) {
                w['SalesmanagoObject'] = r;
                w[r] = w[r] || function () {( w[r].q = w[r].q || [] ).push(arguments)};
                sm = document.createElement('script'); sm.type = 'text/javascript'; sm.async = true; sm.src = a;
                s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(sm, s);
            })(window, 'sm', ('https:' == document.location.protocol ? 'https://' : 'http://') + 'app3.emlgrid.com/static/sm.js');
		</script>
	{/literal}
</body>
</html>