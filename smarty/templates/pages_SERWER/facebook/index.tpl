<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$sLang}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset={$sCharset}" />
<title>{$aPage.title}</title>
{if !empty($aMeta.description)}
<meta name="Description" content="{$aMeta.description}" />
{/if}
{if !empty($aMeta.keywords)}
<meta name="Keywords" content="{$aMeta.keywords}" />
{/if}
<meta name="Robots" content="index,follow" />
<link href="/css/styles_020310.css" rel="stylesheet" type="text/css" />
<link href="/css/lytebox.php?lang={$sLang}" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]>
<link href="/css/styles_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="/js/functions.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/behaviours_new.js"></script>
<script type="text/javascript" src="/js/lytebox/lytebox.php?photo={$aLang.lytebox.image}&amp;from={$aLang.lytebox.from}"></script>
<script type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-ui-1.7.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="/js/tooltip.js"></script>
{if isset($sHeaderJS)}{$sHeaderJS}{/if}
{if isset($sHeaderJSIncludes)}{$sHeaderJSIncludes}
{/if}
</head>
<body>
<div id="kontener">
	<div class="poziomy2">
		<div id="logo"><a href="/" title="{$aPage.name}"></a></div>
		{if isset($aAreas.2)}{$aAreas.2}{/if}
	</div>
	<div class="poziomy">
		{if isset($aAreas.6)}{$aAreas.6}{/if}
	</div>
	<div class="poziomy">
		<div class="kolLewa">
			{if isset($aAreas.8)}{$aAreas.8}{/if}
			{if isset($aAreas.9)}{$aAreas.9}{/if}
		</div>
		<div class="kolPrawa">
			{if isset($aAreas.5)}{$aAreas.5}{/if}
		</div>
	</div>
	
	<div class="clear"></div>
	<div class="boxFacebook">
		<script type="text/javascript" src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/pl_PL"></script>
		<script><http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/pl_PL"></script>
		<script>type="text/javascript">FB.init("c66f6d29581a6cd7f605d7a38ab510ec");</script>
		<fb:fan profile_id="318972512575" stream="0" connections="10" logobar="1" width="1000"></fb:fan>
	</div>
	
	
	<div id="dol">
		<div id="dolTytul">
			<div id="dolTytulLewo"></div>
			<div id="dolTytulSrodek">
				<h2>Profit24.com.pl</h2>
			</div>
			<div id="dolTytulPrawo"></div>
		</div>
		<div id="dolSrodek">
			{if isset($aAreas.7)}{$aAreas.7}{/if}
		</div>
		<div id="dolDol">
			<div id="dolDolLewo"></div>
			<div id="dolDolSrodek"></div>
			<div id="dolDolPrawo"></div>
		</div>
	</div>
	<div class="clear"></div>

	{literal}
	<script type="text/javascript">
		$('#logaAll').cycle({ 
		    fx:    'scrollUp', 
		    sync:   0, 
		    delay: -2000 
		});	
	</script>
	{/literal}

	<div id="footer">
		<div id="loga">
			<div id="logaTytul">Akceptujemy płatności:</div>
			<div id="logaAll">
				<div id="logaRamka" style="margin: 0 0 0 30px;">
					<div id="logoMtransfer"></div>
					<div id="logoMulti"></div>
					<div id="logoPrzelew24"></div>
					<div id="logoNordea"></div>
					<div id="logoInteligo"></div>
					<div id="logoVisa"></div>
					<div id="logoMastercard"></div>
					<div id="logoJcb"></div>
					<div id="logoNiebieskie"></div>
					<div id="logoMaestro"></div>
					<div id="logoPko_sa"></div>		
				</div>
				<div id="logaRamka" style="margin: 0 0 0 45px;">
					<div id="logoMillennium"></div>
					<div id="logoBank_bgz"></div>
					<div id="logoBank_bph"></div>
					<div id="logoBos"></div>
					<div id="logoBosbank24"></div>
					<div id="logoCiti_handlowy"></div>
					<div id="logoPekao24"></div>
					<div id="logoDeutsche_bank"></div>
				</div>
				<div id="logaRamka" style="margin: 0 0 0 85px;">
					<div id="logoDiners"></div>
					<div id="logoEbgz"></div>
					<div id="logoIng"></div>
					<div id="logoInvest_bank"></div>
					<div id="logoKb24"></div>
					<div id="logoKradyt_bank"></div>
					<div id="logoLukas"></div>
					<div id="logoPko"></div>
				</div>
			</div>
		</div>
		<div id="footer1">{$aPage.footer}</div>
		<div id="footer2">{$aRealization.text} {$aRealization.link}</div>
	</div>
</div>
{if isset($sGoogleAnalytics)}
	{$sGoogleAnalytics}
{/if}

</body>
</html>