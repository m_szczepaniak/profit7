{if isset($sPageHeader)}
	<h1 class="pageHeader">{$sPageHeader}</h1>
{/if}
<div class="messageLight">{$sMsg}</div>
{if isset($sMsgGoBack) && isset($sMsgGoBackLink)}
	<a href="{$sMsgGoBackLink}" class="goBack">{$sMsgGoBack}</a>
{/if}