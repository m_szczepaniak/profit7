{strip}
	<div class="photos{$sPhotosAlign}" style="width: {math equation="a+b" a=$iPhotoMaxWidth b=10}px;">
		{foreach from=$aPhotos key=i item=aPhoto}
			<div class="container">
				<div class="photo">
					{if isset($aPhoto.b_photo)}
						<a href="javascript: void(0);" onclick="showPopup('popup.php', '{$aPhoto.b_photo}', {$aPhoto.b_width}, {$aPhoto.b_height}, '{$aPhoto.description}' ); return false;" title="{$aPhoto.decoded_description}"><img alt="{$aPhoto.decoded_description}" src="{$aPhoto.photo}" width="{$aPhoto.width}" height="{$aPhoto.height}" /></a>
					{else}
						<img alt="{$aPhoto.decoded_description}" src="{$aPhoto.photo}" width="{$aPhoto.width}" height="{$aPhoto.height}" />
					{/if}
				</div>
				{if !empty($aPhoto.description)}
					<div class="description" style="width: {$aPhoto.width}px;">
						{$aPhoto.description}
					</div>
				{/if}
			</div>
		{/foreach}
	</div>
{/strip}