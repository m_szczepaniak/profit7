<div class="boxBoczny boxNewsletter">
	<div class="boxBocznyTytul">
		<div class="boxBocznyTytulLewo"></div>
		<div class="boxBocznyTytulSrodek">
			<h2>{$aBox.name}</h2>
		</div>
		<div class="boxBocznyTytulPrawo"></div>
	</div>
	<div class="boxBocznySrodek">
		<div class="boxNewsletterTresc">
			Podaj e-mail aby otrzymywać informacje o nowościach i promocjach
		</div>
		<form action="/szukaj" method="get" id="newsletterBoxForm">
			<input name="q" id="qib" value="" type="text">
			<input name="result" id="result" value="1" type="hidden">
			<input id="send" type="image" name="send" value="" src="/images/gfx/przycisk_dodaj.gif" />
		</form>
	</div>
	<div class="boxGlownyDol">
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>	
</div>
