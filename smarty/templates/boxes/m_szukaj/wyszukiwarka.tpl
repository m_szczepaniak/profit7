<div id="wyszukiwarka">
	<div id="wyszukiwarkaSrodek">
		<form action="{$aBox.page_link}" method="get" name="searchBoxForm" id="searchBoxForm">
			<div class="qibox">
				<input autocomplete="off" style="outline:none" name="q" id="qibox" type="text" value="{if isset($smarty.get.q)}{$smarty.get.q|htmlspecialchars|stripslashes}{else}Wpisz tytuł, autora lub ISBN...{/if}" onfocus="if (this.value == 'Wpisz tytuł, autora lub ISBN...') this.value = '';" onblur="if (this.value == '') this.value = 'Wpisz tytuł, autora lub ISBN...';" onclick="if (this.value != 'Wpisz tytuł, autora lub ISBN...') this.select();" />
			</div>
			<div class="top_cat">
				<select name="top_cat" id="top_cat">
					{foreach from=$aBox.top_cats item=tcat}
						<option value="{$tcat.value}"{if isset($smarty.get.top_cat) && ($smarty.get.top_cat == $tcat.value)} selected="selected"{/if}>{$tcat.label}</option>
					{/foreach}
				</select>
			</div>
			<input type="submit" name="submit" id="szukaj" style="background-image: url(/images/gfx/ikona_szukaj_duza.png); border: solid 0px #000000; width: 92px; height: 39px; cursor: pointer;" value="" />
			<div class="clear"></div>
			<div class="searchBoxFormBottom">
				<input name="aval" type="checkbox" id="aval" {if isset($smarty.get.aval)}checked="checked"{/if}/><label style="cursor: pointer;" for="aval">szukaj również w niedostępnych</label>
				<a href="{$aBox.page_link}" class="hideAdv">wyszukiwanie zaawansowane &raquo;</a>
				<input type="hidden" name="result" id="result1" value="1" />
			</div>		
		</form>
		<div id="qibox_results"></div>
	</div>
</div>