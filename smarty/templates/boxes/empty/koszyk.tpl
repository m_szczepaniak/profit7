<script>
  {literal}
 $(function() {
   {/literal}
  showSimplePopup('{strip}
  
  
	{if $aBox.type eq '1'}
		<span class="powiadomienie_t">{$aBox.lang.just_added}{$aBox.name|replace:'\'':"&#039;"}</span>
	{elseif $aBox.type eq '2'}
		<span class="powiadomienie_t">{$aBox.lang.just_added_repository}{$aBox.name|replace:'\'':"&#039;"}</span>
	{elseif $aBox.type eq '3'}
		<span class="powiadomienie_t">{$aBox.name|replace:'\'':"&#039;"}</span>
	{elseif $aBox.type eq '6'}
		<span class="powiadomienie_t">{$aBox.name|replace:'\'':"&#039;"}</span>
	{elseif $aBox.type eq '5'}
		<div class="powiadomienie_t2">{$aBox.name|replace:'\'':"&#039;"}
			<ul>
			{foreach from=$aBox.notifs item=sNotif}
				<li>{$sNotif}</li>
			{/foreach}
			</ul>
		{$aBox.footer}</div>
	{else}
		<span class="powiadomienie_t">{$aBox.name|replace:'\'':"&#039;"|strip}</span>
	{/if}


{/strip}', 1, 4000);
  {literal}
 });
 {/literal}
</script>  

<noscript>
  <div class="message">
    {if $aBox.type eq '1'}
      <span class="powiadomienie_t">{$aBox.lang.just_added}{$aBox.name}</span>
    {elseif $aBox.type eq '2'}
      <span class="powiadomienie_t">{$aBox.lang.just_added_repository}{$aBox.name}</span>
    {elseif $aBox.type eq '3'}
      <span class="powiadomienie_t">{$aBox.name}</span>
    {elseif $aBox.type eq '6'}
      <span class="powiadomienie_t">{$aBox.name}</span>
    {elseif $aBox.type eq '5'}
      <div class="powiadomienie_t2">{$aBox.name}
        <ul>
        {foreach from=$aBox.notifs item=sNotif}
          <li>{$sNotif}</li>
        {/foreach}
        </ul>
      {$aBox.footer}</div>
    {else}
      <span class="powiadomienie_t">{$aBox.name}</span>
    {/if}
  </div>
</noscript>