<div class="menuDolne menuDolneLast">
	<h2>{$aBox.name}</h2>
	<ul>
	{foreach from=$aBox.menu key=k item=li name=menu_list}
		<li {if $li.selected} class="selected{if $smarty.foreach.menu_list.last} last{/if}"{elseif $smarty.foreach.menu_list.last} class="last"{/if} id="menu_item_{$li.id}">
			{if !isset($li.children)}
				<a href="{if !isset($li.children)}{$li.link}{else}javascript:void(0);{/if}"{if $li.new_window eq '1'} target="_blank"{/if}{if isset($li.children)}{* onclick="toggleSubMenuVisibility({$li.id}, true);" *}{/if}>{$li.name}
				</a>
			{else}
				<strong>{$li.name}</strong>
			{/if}
		{if isset($li.children)}
			{include file=$aBox.sub_template aMenu=$li.children iParentId=$li.id bParentSelected=$li.selected}
		{/if}
		</li>
	{/foreach}
	</ul>
</div>