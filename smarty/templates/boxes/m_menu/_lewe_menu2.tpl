<ul id="sub_menu_{$iParentId}" style="display: {if $bParentSelected}block{else}none{/if};">
	{foreach from=$aMenu item=li key=ii name=left_menu}
		<li{if $li.selected} class="selected{if $ii == count($aMenu)-1} last{/if}"{elseif $ii == count($aMenu)-1} class="last"{/if} id="menu_item_{$li.id}"><a href="{if !isset($li.children)}{$li.link}{else}javascript:void(0);{/if}"{if $li.new_window eq '1'} target="_blank"{/if}{if isset($li.children)} onclick="toggleSubMenuVisibility({$li.id}, false);"{/if}>{$li.name}</a>
		{if isset($li.children)}
			{include file=$aBox.sub_template aMenu=$li.children iParentId=$li.id bParentSelected=$li.selected}
		{/if}
		</li>
	{/foreach}
</ul>