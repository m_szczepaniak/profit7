<div class="rightBoxTitle">
	{$aBox.name}
</div>
<div class="rightBox">
	<div class="menuWlasciwe">
		<ul>
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<li{if $li.selected} class="selected{if $smarty.foreach.menu_list.last} last{/if}"{elseif $smarty.foreach.menu_list.last} class="last"{/if}><a href="{$li.link}"{if $li.new_window eq '1'} target="_blank"{/if}><span>{$li.name}</span></a>
			{if isset($li.children)}
				{include file=$aBox.sub_template aMenu=$li.children}
			{/if}
			</li>
		{/foreach}
		</ul>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>