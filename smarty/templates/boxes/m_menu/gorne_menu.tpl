{literal}
<script type="text/javascript">
	$(function() {
		$("#tabsDzialy").tabs(
				{ cookie: { expires: 30 },
					select: function(event, ui) { 
						var item = ui.panel.id.split("-");
						document.getElementById("top_cat").selectedIndex = parseInt(item[1])+1;
					}
				}
			);
	});
</script>
{/literal}

<div id="tabsDzialy">
	<ul id="tabsDzialyGora">
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<li{if $smarty.foreach.menu_list.last} style="margin: 0;"{/if}><a href="#tabsDzialy-{$k}">{$li.name}</a></li>
		{/foreach}
	</ul>
	<div id="tabsDzialySrodek">
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<div id="tabsDzialy-{$k}" class="tabsDzialySrodek">
				{if isset($li.children)}
					{include file=$aBox.sub_template aMenu=$li.children}
				{/if}
				<div class="clear"></div>
				<div class="wiecej"><div class="wiecejLewo"></div><a href="{$li.link}">{$aLang.common.more}</a><div class="wiecejPrawo"></div></div>
			</div>
		{/foreach}
	</div>
	<div class="boxGlownyDol">
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>
</div>