<ul style="padding: 6px 0 0 0;">
	{foreach from=$aMenu item=li key=ii name=left_menu}
			<li{if $li.selected == '1'} class="selected"{/if}><a href="{$li.link}"{if $li.name|count_characters>24} onmouseover="showhint('{$li.name}', this, event, '200px');"{/if}>{if $li.name|count_characters>24}{$li.name|truncate:24:"...":true}{else}{$li.name}{/if}</a></li>
			{if $ii==11 && !$smarty.foreach.left_menu.last}</ul><ul class="rozwiniety">{/if}
	{/foreach}
</ul>
<div class="clear"></div>
{if !empty($sReturnlink)}<div class="powrot"><div class="powrotLewo"></div><a href="{$sReturnlink}">powrót</a><div class="powrotPrawo"></div></div>{/if}
{if $ii>11}<div class="strzalka wiecej-{$k}"></div>{/if}

<div class="boxGlownyDol">
	<div class="boxGlownyDolLewo"></div>
	<div class="boxGlownyDolSrodek"></div>
	<div class="boxGlownyDolPrawo"></div>
</div>