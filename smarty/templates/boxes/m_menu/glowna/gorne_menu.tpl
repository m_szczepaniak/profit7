{literal}
<script type="text/javascript">
	var tabsDzialyState = 0;
  	$(document).ready(function(){

	    $("#tabsDzialy-0").hover(function () {
	        if(tabsDzialyState == 0) {
	        	tabsDzialyState = 1;
	    		$("#tabsDzialy-0 .rozwiniety").slideDown("1000");
	        }
	      }, function () {
	    	  if(tabsDzialyState == 1) {
		    	  $("#tabsDzialy-0 .rozwiniety").slideUp("1000",
					function(){
		    		  tabsDzialyState = 0;
					});
	    	  }
	      });

	    $("#tabsDzialy-1").hover(function () {
	        if(tabsDzialyState == 0) {
	        	tabsDzialyState = 1;
	    		$("#tabsDzialy-1 .rozwiniety").slideDown("1000");
	        }
	      }, function () {
	    	  if(tabsDzialyState == 1) {
		    	  $("#tabsDzialy-1 .rozwiniety").slideUp("1000",
					function(){
		    		  tabsDzialyState = 0;
					});
	    	  }
	      });
	   
	    $("#tabsDzialy-2").hover(function () {
	        if(tabsDzialyState == 0) {
	        	tabsDzialyState = 1;
	    		$("#tabsDzialy-2 .rozwiniety").slideDown("1000");
	        }
	      }, function () {
	    	  if(tabsDzialyState == 1) {
		    	  $("#tabsDzialy-2 .rozwiniety").slideUp("1000",
					function(){
		    		  tabsDzialyState = 0;
					});
	    	  }
	      });
	    
	});
</script>
{/literal}

<div id="tabsDzialy">
	<ul id="tabsDzialyGora">
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<li id="tabsDzial-{$k}" {if $smarty.foreach.menu_list.last} style="margin: 0;"{/if}{if $k == $aBox.selected} class="ui-tabs-selected"{/if}>
				<a title="{$li.name}" href="{$li.link}" onclick="SetCookie('box_categories',{$k},7);">
					<span class="tabsDzialL"></span>
					<span class="tabsDzialC">{$li.name}</span>
					<span class="tabsDzialR"></span>
				</a>
			</li>
		{/foreach}
	</ul>
	<div id="tabsDzialySrodek">
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<div id="tabsDzialy-{$k}" class="tabsDzialySrodek">
				{if isset($li.children)}
					{include file=$aBox.sub_template aMenu=$li.children sReturnlink=$li.returnlink}
				{/if}
				<div class="clear"></div>
			</div>
		{/foreach}
	</div>
</div>