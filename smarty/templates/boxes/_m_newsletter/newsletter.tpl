{if isset($aBox.js) && !empty($aBox.js)}
	<script type="text/javascript">
		// <![CDATA[
		{foreach from=$aBox.js item=js}
			{$js}
		{/foreach}
		// ]]>
	</script>
{/if}
<div class="boksNewsletter" style="margin-top: 10px;">
	<div class="naglowekBoksP naglowekNewsletter"></div>
	<div class="trescBoksP trescNewsletter">
		<div class="naglowekKrotkiTresc naglowekKrotki">
			{$aBox.name}
		</div>
		<div class="content">
			{$aBox.settings.info}
		</div>
		<form action="{$aBox.page_link}" method="post" name="newsletterBoxForm" id="newsletterBoxForm">
			<input id="n_email" type="text" name="n_email" />
			{*if count($aBox.categories) eq 1}
				<input type="hidden" name="n_categories[{$aBox.categories.0.value}]" value="1" />
			{elseif $aBox.settings.show_categories eq '1'}
				<ul>
					{foreach from=$aBox.categories item=cat}
						<li>
							<div class="checkbox"><input type="checkbox" name="n_categories[{$cat.value}]" value="1" id="cat_{$cat.value}" class="check" /></div>
							<div class="category"><label for="cat_{$cat.value}">{$cat.label}</label></div>
						</li>
					{/foreach}
				</ul>
			{/if*}
			<input id="sendEmail" type="image" name="submit" value="" src="/images/gfx/zapisz_email.gif" />
		</form>
	</div>
	<div class="stopkaBoksP stopkaNewsletter"></div>
</div>