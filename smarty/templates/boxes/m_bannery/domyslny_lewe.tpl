<div class="boxBanneryLewo">
    {foreach from=$aBox.items key=i item=aItem name=banner}
        <div class="banner"{if $smarty.foreach.banner.last} style="padding: 0;"{/if}>
        	{if $aItem.flash eq '1'}
        		<div id="banner_{$aBox.page_id}_{$aItem.id}"></div>
        		<script type="text/javascript">
				// <![CDATA[
					var so = new SWFObject("/{$aBox.base_dir}/{$aItem.src}", "banner_{$aBox.page_id}_{$aItem.id}", "{$aItem.width}", "{$aItem.height}", "8");
					{if !empty($aItem.url)}
						so.addVariable("url", "{$aItem.url}");
						so.addVariable("new_window", "{$aItem.new_window}");
					{/if}
						so.addParam('wmode', 'opaque');
					so.addParam('quality', 'high');
					so.write("banner_{$aBox.page_id}_{$aItem.id}");
				// ]]>
				</script>
           	{else}
           		{if !empty($aItem.url)}
               		<a href="{$aItem.url}"{if $aItem.new_window eq '1'} target="_blank"{/if} rel="nofollow"><img src="/{$aBox.base_dir}/{$aItem.src}" width="{$aItem.width}" height="{$aItem.height}" alt="" /></a>
               	{else}
               		<img src="/{$aBox.base_dir}/{$aItem.src}" width="{$aItem.width}" height="{$aItem.height}" alt="" />
               	{/if}
           	{/if}
        </div>
    {/foreach}
</div>