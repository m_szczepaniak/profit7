{if isset($aBox.js) && !empty($aBox.js)}
	<script type="text/javascript">
		// <![CDATA[
		{foreach from=$aBox.js item=js}
			{$js}
		{/foreach}
		// ]]>
	</script>
{/if}
<div class="boxBoczny boxNewsletter">
	<div class="boxBocznyTytul">
		<div class="boxBocznyTytulLewo"></div>
		<div class="boxBocznyTytulSrodek">
			<h2>{$aBox.name}</h2>
		</div>
		<div class="boxBocznyTytulPrawo"></div>
	</div>
	<div class="boxBocznySrodek">
		<div class="boxNewsletterTresc">
			Podaj e-mail aby otrzymywać informacje o nowościach i promocjach
		</div>
		
		<form action="{$aBox.page_link}" method="post" name="newsletterBoxForm" id="newsletterBoxForm">
			<input type="hidden" name="n_frombox" value="1" />
			<input id="n_email" type="text" name="n_email" />
			{*if count($aBox.categories) eq 1}
				<input type="hidden" name="n_categories[{$aBox.categories.0.value}]" value="1" />
			{elseif $aBox.settings.show_categories eq '1'}
				<ul>
					{foreach from=$aBox.categories item=cat}
						<li>
							<div class="checkbox"><input type="checkbox" name="n_categories[{$cat.value}]" value="1" id="cat_{$cat.value}" class="check" /></div>
							<div class="category"><label for="cat_{$cat.value}">{$cat.label}</label></div>
						</li>
					{/foreach}
				</ul>
			{/if*}
			<input id="send" type="image" name="submit" src="/images/gfx/przycisk_dodaj.gif" alt="dodaj"/>
		</form>
	</div>
	<div class="boxGlownyDol">
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>	
</div>