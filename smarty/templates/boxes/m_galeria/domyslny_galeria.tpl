<div id="mingal">
	<div class="tytulGal">{$aBox.lang.name}</div>
	{foreach from=$aBox.items key=i item=aItem}
		<div class="przegladGaleriGlowna">
			{if !empty($aItem.image)}
				<a href="{$aItem.link}" class="newsWiecej"><img src="{$aItem.image.src}" width="{$aItem.image.width}" height="{$aItem.image.height}" alt="" /></a>
			{/if}
		</div>
	{/foreach}
	<div class="clear"></div>
	<ul>
		{foreach from=$aBox.items key=i item=aItem name=minigalList}
			<li><a href="{$aItem.link}">{$aItem.name}</a></li>
		{/foreach}
	</ul>
	<div class="clear"></div>
	{*<div class="wiecejL">
		<a href="{$aBox.page_link}">{$aBox.lang.more}</a>
	</div>*}
</div>