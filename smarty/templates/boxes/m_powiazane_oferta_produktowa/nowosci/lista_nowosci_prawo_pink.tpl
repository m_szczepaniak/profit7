<div class="boxBoczny boxNowosci boxBocznyPink">
	<div class="boxBocznyTytul">
		<div class="boxBocznyTytulLewo"></div>
		<div class="boxBocznyTytulSrodek">
			<h2>{$aBox.name}</h2>
		</div>
		<div class="boxBocznyTytulPrawo"></div>
	</div>
	<div class="boxBocznySrodek">
		{foreach from=$aBox.items key=i item=item}
			<div class="produktBoczny">
				<div class="produktBocznyZdjecia produktBocznyZdjeciaL">
					<a href="{$item.link}" title="{$item.name|htmlspecialchars}">{if $item.image.src != ''}<img src="{$item.image.src}" alt="{$item.name|htmlspecialchars}"/>{else}<img src="/images/gfx/brak.png" alt="" />{/if}</a>
				</div>
				<div class="produktBocznyTresc">
					<div class="produktBocznyTytul">
						<a href="{$item.link}" title="{$item.name|htmlspecialchars}">{$item.name|truncate:76:"...":true}</a>
					</div>
					{if $item.is_news=='1' || $item.is_bestseller=='1' || $item.is_previews == '1'}
					<div class="produktListaOpcja">
						{if $item.is_bestseller=='1'}<div class="produktListaOpcjaBestseller"></div>{/if}
						{if $item.is_news=='1' && $item.is_bestseller=='1' || $item.is_previews=='1' && $item.is_bestseller=='1'}<div class="produktListaOpcjaSeparator"></div>{/if}
						{if $item.is_previews == '1'}<div class="produktListaOpcjaZapowiedz"></div>{/if}
						{if $item.is_news=='1'}<div class="produktListaOpcjaNews"></div>{/if}
					</div>
					{/if}		
					<div class="clear"></div>			
					<div class="produktBocznyAutor">
						<div class="authors">
							{foreach from=$item.authors item=role name=auth_roles}
								{if !$smarty.foreach.auth_roles.first}
									{php}break;{/php}
								{/if}
								{foreach from=$role key=autor item=author name=prod_authors}
									{$author.name}{if !$smarty.foreach.prod_authors.last}, {/if}
									{if $autor % 3 eq 2}{php}break;{/php}{/if}
								{/foreach}
							{/foreach}
						</div>
					</div>
				</div>
				<div class="clear" style="height: 8px;"></div>
				<div class="produktCeny">
					{if $item.price_brutto ne '0,00'}
						{if $item.promo_price > 0}
							<strong>{$item.promo_text} <span>{$item.promo_price|format_number:2:',':' '}{if $item.promo_price < 1000}{$aBox.lang.currency} zł{/if}</span></strong>
							{$aLang.common.old_price} <span>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000}{$aBox.lang.currency} zł{/if}</span>
						{else}
							<strong>{$aLang.common.cena_w_ksiegarni} <span>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000}{$aBox.lang.currency} zł{/if}</span></strong>	
						{/if}
					{/if}
				</div>						
			</div>
		{/foreach}
		<div class="clear"></div>
		<div class="wiecej"><div class="wiecejLewo"></div><a href="{$aBox.link}">{$aLang.common.all}</a><div class="wiecejPrawo"></div></div>
	</div>
	<div class="boxGlownyDol">
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>	
</div>