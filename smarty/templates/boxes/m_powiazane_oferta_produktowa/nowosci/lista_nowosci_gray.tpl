{if $aBox.mainbox}
<div class="boxGlowny boxGlownyGray">
	<div class="boxGlownyTytul">
		<div class="boxGlownyTytulLewo"></div>
		<div class="boxGlownyTytulSrodek">
			<h2>{$aBox.name}</h2><a class="rss" href="/rss{$aBox.link}.html">RSS</a><div id="nav_{$aBox.boxid}" class="nav"></div>
		</div>
		<div class="boxGlownyTytulPrawo"></div>
	</div>
	{literal}
	<script>
	  $(document).ready(function(){
		  $({/literal}"#newsbox_{$aBox.boxid}"{literal}).cycle({ 
				    fx:     'fade', 
				    speed:  'slow', 
				    timeout: 9000, 
				    pager:  {/literal}'#nav_{$aBox.boxid}'{literal}
				});
	  });
	</script>
	{/literal}
	<div class="boxGlownySrodek" id="newsbox_{$aBox.boxid}">
	{*<div class="one_news_box_page" id="box_news_{$aBox.boxid}_page_0">*}
{/if}
{*	<div class="clear"></div>*}

		<div class="one_news_box_page" id="box_news_0">
		{foreach from=$aBox.items key=i item=item}
			{if ($i%9)==0 && $i>1}
				</div>
				<div class="one_news_box_page" id="box_news_{$i}">
			{/if}
			
			<div class="produkt">
				<div class="produktZdjecia">
					<a href="{$item.link}">{if $item.image.src != ''}<img src="{$item.image.src}" alt="{$item.name|htmlspecialchars}"/>{else}<img src="/images/gfx/brak.png" alt="" />{/if}</a>
				</div>
				<div class="produktTresc">
					<div class="produktTytul">
						<a href="{$item.link}" title="{$item.name}">{$item.name|truncate:76:"...":true}</a>
					</div>
					{if $item.is_news=='1' || $item.is_bestseller=='1' || $item.is_previews == '1'}
					<div class="produktListaOpcja">
						{if $item.is_bestseller=='1'}<div class="produktListaOpcjaBestseller"></div>{/if}
						{if $item.is_news=='1' && $item.is_bestseller=='1' || $item.is_previews=='1' && $item.is_bestseller=='1'}<div class="produktListaOpcjaSeparator"></div>{/if}
						{if $item.is_previews == '1'}<div class="produktListaOpcjaZapowiedz"></div>{/if}
						{if $item.is_news=='1'}<div class="produktListaOpcjaNews"></div>{/if}
					</div>
					{/if}	
					<div class="clear"></div>
					<div class="produktAutor">
						<div class="authors">
							{foreach from=$item.authors item=role name=auth_roles}
								{if !$smarty.foreach.auth_roles.first}
									{php}break;{/php}
								{/if}
								{foreach from=$role key=autor item=author name=prod_authors}
									{$author.name}{if !$smarty.foreach.prod_authors.last}, {/if}
									{if $autor % 3 eq 2}{php}break;{/php}{/if}
								{/foreach}
							{/foreach}
						</div>
					</div>
				</div>
				
				{*
				zmiany darmowa przesyłka				
				<div class="produktPrzyciski">
					<a href="{$item.repository_link}" title="{$aBox.lang.przechowalnia}" class="przyciskSchowek"></a>
					{if ($item.shipment_time == '3')}
						<span>na zamówienie</span>
					{elseif ($item.prod_status == '1' || $item.prod_status == '3')}
						<a href="{$item.cart_link}" title="Złóż zamówienie" class="przyciskKoszyk"></a>
					{else}
						<span>niedostępna</span>
					{/if}		
				</div>
				*}
				
				<div class="produktCeny">
					{if $item.price_brutto ne '0,00'}
						{if $item.shipment_time!='3'}
							{if $item.promo_price > 0}
								<strong>{$item.promo_text} <span>{$item.promo_price|format_number:2:',':' '}{if $item.promo_price < 1000}{$aBox.lang.currency} zł{/if}</span></strong><br />
								{$aLang.common.old_price} <span>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000}{$aBox.lang.currency} zł{/if}</span>
							{else}
								<strong>{$aLang.common.cena_w_ksiegarni} <span>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000}{$aBox.lang.currency} zł{/if}</span></strong>	
							{/if}
						{/if}
					{/if}
				</div>
			</div>
			{if $i mod 3 eq 2}<div class="clear"></div>{/if}
		{/foreach}
		</div>
		
	{if $aBox.mainbox}
		{*</div>*}

{*
	{foreach name=newsdivs from=$aBox.divs item=aItm key=i}
			<div class="one_news_box_page" id="{$aItm}"></div>
	{/foreach}
*}

	</div>
	
	<div class="boxGlownyDol">
		<div class="clear"></div>
			<div class="wiecej"><div class="wiecejLewo"></div><a href="{$aBox.link}">{$aLang.common.all}</a><div class="wiecejPrawo"></div></div>
		<div class="clear"></div>
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>	
</div>
{/if}