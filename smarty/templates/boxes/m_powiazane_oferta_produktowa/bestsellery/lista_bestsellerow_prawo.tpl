<div class="boxBoczny boxBestsellery">
	<div class="boxBocznyTytul">
		<div class="boxBocznyTytulLewo"></div>
		<div class="boxBocznyTytulSrodek">
			<h2>{$aBox.name}</h2>
		</div>
		<div class="boxBocznyTytulPrawo"></div>
	</div>
	<div class="boxBocznySrodek">
		{foreach from=$aBox.items key=i item=item}
			<div class="produktBoczny">
				<div class="produktBocznyTresc">
					<div class="produktBocznyTytul">
						<a href="{$item.link}" title="{$item.name|htmlspecialchars}">{$item.name|truncate:76:"...":true}</a>
					</div>
					<div class="produktBocznyAutor">
						<div class="authors">
							{foreach from=$item.authors item=role name=auth_roles}
								{if !$smarty.foreach.auth_roles.first}
									{php}break;{/php}
								{/if}
								{foreach from=$role key=autor item=author name=prod_authors}
									{$author.name}{if !$smarty.foreach.prod_authors.last}, {/if}
									{if $autor % 3 eq 2}{php}break;{/php}{/if}
								{/foreach}
							{/foreach}
						</div>
					</div>
				</div>
				<div class="produktBocznyZdjecia">
					<a href="{$item.link}" title="{$item.name|htmlspecialchars}">{if $item.image.src != ''}<img src="{$item.image.src}" alt="{$item.name|htmlspecialchars}"/>{else}<img src="/images/gfx/brak.png" alt="" />{/if}</a>
				</div>
			</div>
		{/foreach}
		<div class="clear"></div>
		<div class="wiecej"><div class="wiecejLewo"></div><a href="{$aBox.link}">{$aLang.common.all}</a><div class="wiecejPrawo"></div></div>
	</div>
	<div class="boxGlownyDol">
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>	
</div>