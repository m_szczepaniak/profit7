<p class="page404__products-title">{$aBox.name}</p>
<div class="page404__products-container">
	<div class="page404__products">
		{foreach from=$aBox.items key=i item=item}
			<div class="page404__produkt produkt">
				<div class="produktZdjecia">
					<a href="{$item.link}">{if $item.image.src != ''}<img src="{$item.image.src}" alt="{$item.plain_name}" title="{$item.plain_name}"/>{else}<img src="/images/gfx/brak.png" alt="" />{/if}</a>
				</div>
				<div class="produktTresc">
					<div class="produktTytul">
						<a href="{$item.link}" title="{$item.name}">{$item.name|truncate:76:"...":true}</a>
					</div>
					{if $item.is_news=='1' || $item.is_bestseller=='1' || $item.is_previews == '1'}
						<div class="produktListaOpcja">
							{if $item.is_bestseller=='1'}<div class="produktListaOpcjaBestseller"></div>{/if}
							{if $item.is_news=='1' && $item.is_bestseller=='1' || $item.is_previews =='1' && $item.is_bestseller=='1'}<div class="produktListaOpcjaSeparator"></div>{/if}
							{if $item.is_previews == '1'}<div class="produktListaOpcjaZapowiedz"></div>{/if}
							{if $item.is_news=='1'}<div class="produktListaOpcjaNews"></div>{/if}
						</div>
					{/if}
					<div class="clear"></div>
					<div class="produktAutor">
						<div class="authors">
							{foreach from=$item.authors item=role name=auth_roles}
								{if !$smarty.foreach.auth_roles.first}
									{php}break;{/php}
								{/if}
								{foreach from=$role key=autor item=author name=prod_authors}
									{$author.name}{if !$smarty.foreach.prod_authors.last}, {/if}
									{if $autor % 3 eq 2}{php}break;{/php}{/if}
								{/foreach}
							{/foreach}
						</div>
					</div>
				</div>
				<div class="produktCeny">
					{if $item.price_brutto ne '0,00'}
						{if $item.shipment_time!='3'}
							{if $item.promo_price > 0}
								<strong><span>{$item.promo_price|format_number:2:',':' '}{if $item.promo_price < 1000} zł{/if}</span></strong><br><span>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000} zł{/if}</span>
							{else}
								<strong><span>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000} zł{/if}</span></strong>
							{/if}
						{/if}
					{/if}
				</div>
			</div>
			{if $i mod 4 eq 3}<div class="clear"></div>{/if}
		{/foreach}
	</div>
</div>