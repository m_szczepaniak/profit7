<div class="boxGlowny boxTagi">
	<div class="boxTagiGora"></div>
	<div class="boxGlownySrodek">
		<div class="boxTagiTytul">
			<h2>{$aBox.name}</h2>
		</div>
		<div class="clear"></div>
		<div class="boxTagiLinki">
			{foreach from=$aBox.items item=tag}
				{$tag.tag}
			{/foreach}
		</div>
	</div>
	<div class="boxGlownyDol">
		<div class="clear"></div>
			<div class="wiecej"><div class="wiecejLewo"></div><a href="{$aBox.link}">{$aLang.common.all}</a><div class="wiecejPrawo"></div></div>
		<div class="clear"></div>
		<div class="boxGlownyDolLewo"></div>
		<div class="boxGlownyDolSrodek"></div>
		<div class="boxGlownyDolPrawo"></div>
	</div>	
</div>



{*
<div class="boxNewsletterLewo"></div>
	<div class="boxNewsletterSrodek">
		<h2>{$aBox.name}</h2>
		<div class="tagiLogo"></div>
	</div>
<div class="boxNewsletterPrawo"></div>
<div class="clear"></div>
<div class="boxNewsletterGlowny">
	<div class="boxNewsletterTresc" style="text-align: center; line-height: 150%;">
		{foreach from=$aBox.items item=tag}
			{$tag.tag}
		{/foreach}
	</div>
</div>
<a class="wiecej" href="/tagi">
	{$aLang.common.more}
</a>
<div class="boxNewsletterDol"></div>
*}