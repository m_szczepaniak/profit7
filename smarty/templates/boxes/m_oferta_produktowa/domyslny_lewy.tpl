<div class="boksLewy">
{if !empty($aBox.items)}
	<div class="boksHeader">{$aBox.name}</div>
	{foreach from=$aBox.items key=i item=item}
		<div class="productItem">
				<div class="newsItemImage"><a href="{$item.link}"><img src="{$item.image.thumb}" alt="{$item.plain_name}" /></a></div>
		</div>
	{/foreach}
	<div class="clear"></div>
	<div class="link_all"><a class="allBoks" href="{$aBox.link}">{$aLang.common.see_all}</a></div>
	<div class="clear"></div>
{/if}
</div>