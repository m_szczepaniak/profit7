<div id="categoriesBox">
	{foreach from=$aBox.aMenu.item key=i item=cat}
		<div class="categoryCnt{if ($i+1)%3 eq 0} noMargin{/if}">
			<div class="header">
				<div class="nameCnt">
					<div class="name">
						<a href="{$cat.link}"{if isset($cat.target)} target="{$cat.target}"{/if}>
							<span class="dot"></span><span class="content">{$cat.name}</span><span class="dot"></span>
						</a>
					</div>
					<div class="subHeader"><span>{$aBox.lang.subcategories}</span></div>
				</div>
				{if isset($aBox.aPhotos[$cat.id])}
					<div class="icoCnt">
						<img src="{$aBox.aPhotos[$cat.id].src}" width="{$aBox.aPhotos[$cat.id].width}" height="{$aBox.aPhotos[$cat.id].height}" alt="{if !empty($aBox.aPhotos[$cat.id].description)}{$aBox.aPhotos[$cat.id].description}{else}{$cat.name}{/if}" />
					</div>
				{/if}
			</div>
			<div class="subcategories">
				<div class="items">
				{foreach from=$cat.children.item key=j item=subcat name=subcats}
					{if $j lt $aBox.aSettings.subcategories}
						 <a href="{$subcat.link}"{if isset($subcat.target)} target="{$subcat.target}"{/if}>{$subcat.name}</a>{if $j lt $aBox.aSettings.subcategories-1 && $smarty.foreach.subcats.last eq false}&nbsp;&nbsp;&middot;&nbsp; {/if}
					{/if}
				{/foreach}
				{if count($cat.children.item) gt $aBox.aSettings.subcategories}
					&nbsp;&middot;&nbsp;<span class="all"><a href="{$cat.link}"{if isset($cat.target)} target="{$cat.target}"{/if}>{$aBox.lang.more}</a></span>
				{/if}
				</div>
			</div>
			<div class="corners"><div class="right"></div></div>
		</div>
	{/foreach}
	<div class="clear"></div>
</div>