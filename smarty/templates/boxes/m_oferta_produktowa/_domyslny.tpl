<div class="leftContainer">
	<div class="header"><div>{$aBox.name}</div></div>
	<div class="content">
		<div class="boxFormContainer">
			{if !$aBox.is_logged_in}
				{$aBox.form.header}
				{$aBox.form.err_prefix}
				{$aBox.form.err_postfix}
				{$aBox.form.validator}
				{if isset($aBox.form.fields.ref)}
					{$aBox.form.fields.ref}
				{/if}
				<div class="fRow">
					<div class="fLabel">{$aBox.form.fields.login.label}</div>
					<div class="fInput">{$aBox.form.fields.login.input}</div>
					<div class="clear"></div>
				</div>
				<div class="fRow">
					<div class="fLabel">{$aBox.form.fields.passwd.label}</div>
					<div class="fInput">{$aBox.form.fields.passwd.input}</div>
					<div class="clear"></div>
				</div>
				<div class="fSendButton">
					<input type="image" name="send" value="" src="images/gfx/{$sLang}/send.gif" class="send" />
				</div>
				<div class="fTextRow"><a href="{$aBox.links.register}">{$aLang.common.register}</a></div>
				<div class="fTextRow"><a href="{$aBox.links.change_passwd}">{$aLang.common.remind_passwd}</a></div>
				{$aBox.form.footer}
				<div class="clear"></div>
			{else}
				<div class="fTextRow2">{$aBox.lang.logged_as}</div>
				<div class="fTextRow2"><a href="{$aBox.page_link}">{$aBox.logged_as}</a></div>
				<div class="fTextRow"><a href="{$aBox.links.logout}">{$aBox.lang.logout}</a></div>
			{/if}
		</div>
	</div>
	<div class="footer"></div>
</div>