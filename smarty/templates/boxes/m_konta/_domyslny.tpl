<div id="boxLogowania">
	<div class="header">
		<span>{$aBox.name}</span>
	</div>
	{if !$aBox.is_logged_in}
		{$aBox.form.header}
		{$aBox.form.err_prefix}
		{$aBox.form.err_postfix}
		{$aBox.form.validator}
		{if isset($aBox.form.fields.ref)}
			{$aBox.form.fields.ref}
		{/if}
		<div id="boxLoginForm">
			<div class="fRow">
				<div class="fLabel">{$aBox.form.fields.login.label}</div>
				<div class="fInput">{$aBox.form.fields.login.input}</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="fRow">
				<div class="fLabel">{$aBox.form.fields.passwd.label}</div>
				<div class="fInput">{$aBox.form.fields.passwd.input}</div>
				<div class="clear"></div>
			</div>
			<div class="fSendButton">
				<input type="submit" name="send" value="Zaloguj" src="/images/gfx/submit_90.gif" class="send90" />
			</div>
			<div id="require">
				<div class="fTextRow"><a href="{$aBox.links.change_passwd}">{$aLang.common.remind_passwd}</a></div>
				<div class="fTextRow1"><strong><a href="{$aBox.links.register}">{$aLang.common.register}</a></strong></div>
			</div>
		</div>
		{$aBox.form.footer}
	{else}
		<img src="/images/gfx/user.gif"  style="float: left; margin-left: 25px; margin-top: 20px; margin-right: 20px;"/>
		<div class="welcome" style="margin-top: 20px; float: left; width: 150px;">
			{$aBox.lang.welcome} <a href="{$aBox.page_link}">{$aBox.logged_as}</a> (<a href="{$aBox.logout_link}">{$aBox.lang.logout}</a>)
		</div>
		<div class="linkiBox" style="float: left;">
		<div class="clear"></div>
			<ul>
				{foreach from=$aBox.links key=l_name item=link name=linkBox}
					<li><a id="{$l_name}" href="{$link}">{$aBox.lang[$l_name]}</a></li>
				{/foreach}
			</ul>
		</div>
	{/if}
</div>