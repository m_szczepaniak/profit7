<div class="right" style="width: 780px;">	
	{if !$aBox.is_logged_in}
		<div id="linkiRejestracjaNew">
			<strong>Witaj w Profit24.pl - </strong><a class="red" href="{$aBox.links.register}"><strong>{$aLang.common.register}</strong></a>&nbsp;| 
			<a class="red" href="{$aBox.links.login}"><strong>{$aLang.common.do_login}</strong></a>&nbsp;| 
      <a href="{$aBox.links.fb_register}"><img id="button-polacz-z-facebook" src="/images/gfx/polacz-z-facebook.png" alt="facebook" /></a>&nbsp;|
			<strong><a href="/pomoc">Pomoc</a></strong>&nbsp;| 
      
      <div id="linkiSchowek">
        <a href="/koszyk" class="koszyk">Koszyk</a>
        <a href="/schowek" class="schowek">Schowek</a>
      </div>
      
		</div>
	{else}
		<div id="linkiRejestracja">
			{$aBox.lang.welcome}&nbsp;&nbsp;<a href="{$aBox.page_link}"><strong>{$aBox.logged_as}</strong></a>&nbsp;&nbsp;|&nbsp;
			<a href="{$aBox.links.edit}"><strong>{$aBox.lang.edit}</strong></a>&nbsp;&nbsp;|&nbsp;
			<a href="{$aBox.links.logout}"><strong>{$aBox.lang.logout}</strong></a>&nbsp;&nbsp;|&nbsp;
			<strong><a href="/pomoc">Pomoc</a></strong>&nbsp;&nbsp;|&nbsp;
      
      <div id="linkiSchowek">
        <a href="/koszyk" class="koszyk">Koszyk</a>
        <a href="/schowek" class="schowek">Schowek</a>
      </div>
      
		</div>
	{/if}
</div>