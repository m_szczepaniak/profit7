<div class="leftBox">
    <div id="sondaPytanie">
        {$aBox.item.name}
    </div>
    <div id="sondaWarianty">
        <form action="{$aBox.item.link}" name="pollBoxForm" method="post">
            <ul>
            	{foreach from=$aBox.item.answers item=aItem key=i name=poll_answers}
                    <li>
                    	<div class="sondaPole">
	                        <div class="poleRadio"><input type="radio" name="answer" value="{$aItem.id}" id="a_{$aItem.id}" /></div>
	                        <div class="poleLabel"><label for="a_{$aItem.id}">{$aItem.answer}</label></div>
						</div>
                    </li>
                {/foreach}
            </ul>

            <div class="clear"></div>
            <div id="sondaIleGlosowaloBox">
                <div style="float: left; padding-right: 10px;">
                    <input type="image" src="/images/gfx/{$sLang}/glosuj.gif" name="submit" />
                </div>
                <div id="zobaczWyniki">
					<a href="{$aBox.item.link}">{$aBox.lang.see_results}</a>&nbsp;&nbsp;|&nbsp;&nbsp;
				</div>
                <div id="sondaIleGlosowalo">
                    {$aBox.lang.votes} <strong><span>{$aBox.item.total_votes}</span></strong>
                </div>
				<div class="clear"></div>
				

                <div class="clear"></div>
            </div>
        </form>
    </div>
</div>