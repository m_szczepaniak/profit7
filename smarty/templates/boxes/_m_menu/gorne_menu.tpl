<div class="menuGlowne">
	<div class="menuWlasciwe">
	<div class="menuWlasciweLeft"></div>
		<ul>
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<li{if $li.selected} class="selected{if $smarty.foreach.menu_list.last} last{/if}"{elseif $smarty.foreach.menu_list.last} class="last"{/if}><a href="{$li.link}"{if $li.new_window eq '1'} target="_blank"{/if}><span class="left_menu_link"></span><span class="menu_link">{$li.name}</span><span class="right_menu_link"></span></a>
			{if isset($li.children)}
				{include file=$aBox.sub_template aMenu=$li.children}
			{/if}
			</li>
		{/foreach}
		</ul>
	<div class="menuWlasciweRight"></div>
	</div>
	<div class="clear"></div>
</div>