<ul>
	{foreach from=$aMenu item=li key=ii name=left_menu}
		<li{if $li.selected} class="selected{if $ii == count($aMenu)-1} last{/if}"{elseif $ii == count($aMenu)-1} class="last"{/if}><a href="{$li.link}"{if $li.new_window eq '1'} target="_blank"{/if}><span>{$li.name}</span></a>
		{if isset($li.children)}
			{include file=$aBox.sub_template aMenu=$li.children}
		{/if}
		</li>
	{/foreach}
</ul>