<div class="boxMenuNazwa">{$aBox.parent.name}</div>

<div class="menuB">
	<div class="menuBTresc">
		<ul>
		{foreach from=$aBox.menu key=k item=li name=menu_list}
			<li{if $li.selected} class="selected"{/if}><a href="{$li.link}"{if $li.new_window eq '1'} target="_blank"{/if}{if $smarty.foreach.menu_list.last} style="border-bottom: 0;"{/if}><span>{$li.name}</span></a>
			{if isset($li.children)}
				{include file=$aBox.sub_template aMenu=$li.children}
			{/if}
			</li>
		{/foreach}
		</ul>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>