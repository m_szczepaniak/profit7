<div class="clear" style="margin-top: 10px;"></div>
<div class="polecane {if $bKoszyk}koszyk{/if}">
	<div class="boxGlownyTytul">
		<div class="boxGlownyTytulLewo"></div>
		<div class="boxGlownyTytulSrodek">
			<h1>{*<a href="{$aBox.link}">*}{$aBox.name}{*</a>*}</h1>
		</div>
		<div class="boxGlownyTytulPrawo"></div>
	</div>
	<div class="boxTresc" id="newsbox_{$aBox.boxid}">
		<div class="slider">
			{foreach from=$aBox.items key=i item=item name=item}
				<div class="produkt">
					<div class="produktZdjecia">
						<a href="{$item.link}">{if $item.image[0].small != ''}<img src="{$item.image[0].small}" alt="{$item.name|htmlspecialchars}"/>{else}<img src="/images/gfx/brak2.png" alt="" class="img-responsive"/>{/if}</a>
					</div>
					<div class="produktPrawo">
						<div class="produktTresc">
							<div class="clear"></div>
							<div class="produktTytul">
								<a href="{$item.link}" title="{$item.name}">{$item.name|truncate:76:"...":true}</a>
							</div>
							<div class="produktAutor">
								<div class="authors">
									{foreach from=$item.authors item=role name=auth_roles}
										{if !$smarty.foreach.auth_roles.first}
											{php}break;{/php}
										{/if}
										{foreach from=$role key=autor item=author name=prod_authors}
											{$author.name}{if !$smarty.foreach.prod_authors.last}, {/if}
											{if $autor % 3 eq 2}{php}break;{/php}{/if}
										{/foreach}
									{/foreach}
								</div>
							</div>
						</div>

						<div class="produktCeny">
							{if $item.price_brutto ne '0,00'}
								{if $item.shipment_time!='3'}
									{if $item.promo_price > 0}
										<span><span class="profit-price">{$item.promo_text}</span> <span class="new-price">{$item.promo_price|format_number:2:',':' '}{if $item.promo_price < 1000}{$aBox.lang.currency} zł{/if}</span></span>
										<span>{$aLang.common.old_price} <span class="old-price">{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000}{$aBox.lang.currency} zł{/if}</span></span>

									{else}
										<span><span>{$aLang.common.cena_w_ksiegarni}</span> <span class="new-price"><strong>{$item.price_brutto|format_number:2:',':' '}{if $item.price_brutto < 1000}{$aBox.lang.currency} zł{/if}</strong></span></span>
									{/if}
								{/if}
							{/if}
						</div>
					</div>
					<div class="produktListaPrzyciski">
						{if $bKoszyk}
							{if ($item.shipment_time == '3')}
								<span>na zamówienie</span>
							{elseif (($item.prod_status == '1' || $item.prod_status == '3') && $item.price_brutto ne '0,00')}
								{strip}
									<a href="{$item.cart_link}"
									   title="Złóż zamówienie" class="przyciskKoszyk produktListaPrzyciskiClass"></a>
								{/strip}
							{elseif ($item.prod_status == '2')}
								{*<span>nakład wyczerpany</span>*}
							{else}
								{*<span>niedostępna</span>*}
							{/if}
						{/if}
					</div>
				</div>
			{/foreach}
		</div>
	</div>
    {if $aBox.mainbox}
		<div class="clear"></div>
		<div class="bBigBottom">
			<div class="wiecej"><a href="{$aBox.link}">{$aLang.common.all}</a></div>
		</div>
    {/if}
</div>
<script type="text/javascript">
  {literal}
    $(document).ready(function(){
        $(".slider").addClass("slider-480");
        $('.slider-480').slick({
            lazyLoad: 'ondemand',
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,

            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
			]
        });

   });
  {/literal}
</script>
