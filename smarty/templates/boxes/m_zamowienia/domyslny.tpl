{if $aBox.cart.total_quantity > 0}
{literal}
<script type="text/javascript">
var boxKoszykState = 0;
  $(document).ready(function(){
    


    $(".boxKoszyk").hover(function () {
        if(boxKoszykState == 0) {
        	boxKoszykState = 1;
    		$("#boxKoszykRozw").slideDown("1000");
    		$("#boxKoszykRozwSrodek").css("z-index","9999")
        }
      }, function () {
    	  if(boxKoszykState == 1) {
	    	  $("#boxKoszykRozw").slideUp("1000",
						function(){
					boxKoszykState = 0;
				});
    	  }
      });


  /* 
    $(".boxKoszykSrodek .wiecej").hover(function () {
        $(".boxKoszykSrodek .wiecej").fadeOut("slow");
      });

    $("#boxKoszykRozw .wiecej").hover(function () {
        $(".boxKoszykSrodek .wiecej").fadeIn("slow");
      });

    $(".boxKoszyk .wiecej").hover(function () {
        $("#boxKoszykRozw .wiecej").fadeIn("slow");
      });

    $("#boxKoszykRozw .wiecej").hover(function () {
        $("#boxKoszykRozw .wiecej").fadeOut("slow");
      });
    */
  });
</script>
{/literal}
{/if}
<div class="boxKoszyk">
	<div class="boxKoszykGlowny">
		<h2><a href="/koszyk">{$aBox.name}</a></h2>
		<div class="boxKoszykPodsumowanie">{if $aBox.cart.total_quantity > 0}Egzemplarzy: <strong>{$aBox.cart.total_quantity}</strong>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;<strong>{$aBox.cart.total_price_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong>{else}{$aBox.lang.empty_msg}{/if}</div>
		{*{if $aBox.cart.total_quantity > 0}<div class="wiecej"></div>{/if}*}
	</div>
	
	<div id="boxKoszykRozw">
		<div id="boxKoszykRozwGora">
			<div id="boxKoszykRozwGoraLewo"></div>
			<div id="boxKoszykRozwGoraSrodek"></div>
			<div id="boxKoszykRozwGoraPrawo"></div>
		</div>	
		<div id="boxKoszykRozwSrodek">
			<table>
				<tr>
					<th class="tytul">Tytuł książki:</th>
					<th class="ilosc">Ilość:</th>
					<th class="cena">Cena:</th>
				</tr>		
			{foreach from=$aBox.cart.products item=aItem key=i name=cart_products}
				<tr>
					<td class="tytul">{$aItem.name}</td>
					<td class="ilosc">{$aItem.quantity}</td>
					<td class="cena">{if $aItem.promo_price > 0}<div>{$aItem.price_brutto|format_number:2:',':' '} {$aLang.common.currency}</div>{$aItem.promo_price|format_number:2:',':' '} {$aLang.common.currency}{else}{$aItem.price_brutto|format_number:2:',':' '} {$aLang.common.currency}{/if}</td>
				</tr>
			{/foreach}
			</table>
			<a href="/koszyk" class="przejdz"></a>
		</div>
		<div id="boxKoszykRozwDol">
			<div id="boxKoszykRozwDolLewo"></div>
			<div id="boxKoszykRozwDolSrodek"></div>
			<div id="boxKoszykRozwDolPrawo"></div>
		</div>
		{*<div class="wiecej"></div>*}
	</div>
</div>