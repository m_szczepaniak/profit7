<div id="menu">
	<ul>
		{foreach from=$aBox.menu key=k item=li name=menu_gorne}
			<li{if $li.selected} class="selected"{/if}>
				<a href="{$li.link}"{if $li.new_window eq '1'} target="_blank"{/if} id="{$li.symbol}"{if $smarty.foreach.menu_gorne.last} style="width: 159px;"{/if}>{$li.name}</a>
			</li>
		{/foreach}
	</ul>
</div>			