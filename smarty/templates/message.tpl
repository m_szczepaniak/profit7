<script type="text/javascript">
  {literal}
 $(function() {
   {/literal}
  showSimplePopup('{strip}{$sMsgText|replace:'\'':'"'|strip}{/strip}', 1, {if $iCloseAfterSeconds == '-1' || $iCloseAfterSeconds == ''}{if $sMsgText|strlen > 50}4000{else}2000{/if}{else}{$iCloseAfterSeconds}{/if});
  {literal}
 });
 {/literal}
</script>  

<noscript>
  <div class="message">{$sMsgText}</div>
</noscript>