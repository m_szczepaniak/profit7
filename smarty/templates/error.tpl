<script type="text/javascript">
  {literal}
 $(function() {
   {/literal}
  showSimplePopup('{strip}{$sMsgText|replace:'\'':'"'|strip}{/strip}', -1, {if $iCloseAfterSeconds == '-1' || $iCloseAfterSeconds == ''}10000{else}{$iCloseAfterSeconds}{/if});
  {literal}
 });
 {/literal}
</script>  

<noscript>
  <div class="error">{$sMsgText}</div>
</noscript>