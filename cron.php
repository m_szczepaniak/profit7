<?php
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : dirname(__FILE__));
$_SERVER['SERVER_NAME'] = 'www.profit24.pl';

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
//include_once ($_SERVER['DOCUMENT_ROOT'].'/inc/functions.inc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
//include_once ($_SERVER['DOCUMENT_ROOT'].'/inc/header.inc.php');

set_time_limit(750);
// maksymalna liczba sekund wykonania skryptu
$iMaxExecTime = ini_get('max_execution_time');
$iHour = intval(date("G")); // aktualna godzina
$iMinute = intval(date("i"));  // aktualna minuta

// czas rozpoczecia wykonywania skryptu
$iTime = microtime();
$iTime = explode(' ', $iTime);
$iTime = $iTime[1] + $iTime[0];
$iStartTime = $iTime;

function getRemainingTime() {
	global $iStartTime, $iMaxExecTime;
	
	$iTime = microtime();
	$iTime = explode(" ", $iTime);
	$iTime = $iTime[1] + $iTime[0];
	$iEndTime = $iTime;
	return $iMaxExecTime - ceil($iEndTime - $iStartTime);
}

// pobranie listy wszystkich modulow
$sSql = "SELECT id, symbol FROM ".$aConfig['tabls']['prefix']."modules ORDER BY symbol";
$aModules =& Common::GetAll($sSql);

foreach ($aModules as $aSearchModule) {
	if (file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$aConfig['common']['cms_dir'].'modules/'.$aSearchModule['symbol'].'/internals/cron.php')) {
		// jezeli dla danego modulu istnieje plik cron.php - dolaczenie go
		include_once($_SERVER['DOCUMENT_ROOT'].'/'.$aConfig['common']['cms_dir'].'modules/'.$aSearchModule['symbol'].'/internals/cron.php');
	}
}
?>