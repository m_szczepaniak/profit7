<?php
use Listener\ElasticListener;

/**
 * Klasa wspólna synchronizacji elementów
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 * 
 * @property Module_Common $oProductsCommon
 */
class CommonSynchro {
	private $aStockMapping = array();
  public $iSourceId;
  // mapowanie domyslne dla ABE - sztywne
  private $aLanguagesByCode = array(
      3 => array(
        'eng' => 3,
        'pol' => 1,
      )
  );
	
	public function __construct() {}
	
	/**
	 * Uniwersalna metoda do sprawdzania czy w danej tabeli istnieje rekord
	 *
	 * @global object $pDbMgr
	 * @global array $aConfig
	 * @param string $sProductsTable
	 * @param string $sItemColName
	 * @param string $sVal
	 * @return int - czy rekord istnieje
	 */
	public function recordProductsTablesExists($sProductsTable, $sItemColName, $sVal) {
		global $pDbMgr, $aConfig;
		
		if ($sProductsTable == '' || $sItemColName == '' || $sVal == '') return (-1);
		
		$sSql = "SELECT ".$sItemColName." FROM ".$aConfig['tabls']['prefix'].$sProductsTable."
						 WHERE ".$sItemColName." = '".$sVal."'";
		$mRet = $pDbMgr->GetOne('profit24', $sSql);
		
		if ($mRet === false) {
			return -1;
		} elseif ($mRet == null) {
			return 0;
		} elseif ($mRet == '') {
			return 0;
		} else {
			return 1;
		}
	}// end of recordProductsTablesExists() method
	
	
	/**
	 * Metoda dodaje do przekazanej tabeli przekazany zestaw danych
	 * $aArrayAttr = 
	 *	[1] => [
	 *					'name' => 'TEST'
	 *					'symbol' => 'TEST123'
	 *					],
	 *	[2] => [
	 *					'name' => 'TEST2'
	 *					'symbol' => 'TEST456'
	 *					]
	 *
	 * @param type $sProductsTable
	 * @param type &$aArrayAttr  - UWAGA zmienna przekazana zostanie po wykonaniu metody wyczyszczona !!
	 * @return bool  
	 */
	public function InsertProdAttr($sProductsTable, &$aArrayAttr) {
		global $pDbMgr, $aConfig;
		
		if ($sProductsTable == '' || empty($aArrayAttr) ||!is_array($aArrayAttr)) return (-1);
		
		foreach ($aArrayAttr as &$aValues) {
			$bRet = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix'].$sProductsTable, $aValues, '', false);
			if ($bRet == false) {
				return false;
			}
		}
		
		return true;
	}// end of InsertProdAttr() method
	
	
	/**
	 * Metoda pobiera wszystkie wiązania wszystkich kategorii powiązanych
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param string $sSourceSymbol
	 * @param array $aCatIds
	 * @return array 
	 */
	private function getOneSourceMapping($sSourceSymbol, $aCatIds) {
		global $aConfig, $pDbMgr;
		if (empty($aCatIds) || !is_array($aCatIds)) return array();
		
		$sSql = "SELECT page_id FROM ".$aConfig['tabls']['prefix']."menus_items_mappings AS A
						 JOIN sources AS B
							ON A.source_id = B.id
						 WHERE A.source_category_id IN (".implode(',', $aCatIds).") AND B.symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetCol('profit24', $sSql);
	}// end of getOneSourceMapping() method
	
	
	/**
	 * Metoda pobiera mapowania dla zadanego źródła, dla zadanych id kategorii źródła
	 *
	 * @param string $sSourceSymbol - symbol źródła
	 * @param array $aProdCatsIds - id kategorii źródła
	 * @return array 
	 */
	public function getSourceMappings($sSourceSymbol, &$aProdCatsIds) {
		$aCats = $this->getOneSourceMapping($sSourceSymbol, $aProdCatsIds);
		
		foreach ($aCats as $iCat) {
				$aPath = $this->getCategoriesPathIDs($iCat, 1);
				if(!empty($aPath)) {
					$aCats = array_merge($aCats, $aPath);
				}
		}
		if(!empty($aCats)){
			$aCats = array_unique($aCats);
		}
		
		return $aCats;
	} // end of getSourceMappings() method
	
	
	/**
   * Funkcja tworzaca na podstawie przekazanego do niej Id strony
   * jej pelna sciezke od korzenia do niej
   *
   * @param	integer	$iId	- ID strony do ktorej ma byc tworzona sciezka
   * @param	integer	$iLangId	- Id wersji jezykowej
   * @return	array	- uporzadkowana tablica ze sciezka do strony
   */
  private function getCategoriesPathIDs($iId, $iLangId) {
	global $aConfig, $pDbMgr;
	$aPath		= array();
		
  	$sSql = "SELECT A.id, IFNULL(A.parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
  					 WHERE A.id = ".$iId." AND
  					 			 A.language_id = ".$iLangId;
  	$aItem =& $pDbMgr->GetRow('profit24', $sSql);
  	if(!empty($aItem)) {
	  	$aPath[] = $aItem['id'];
	
	  	while (intval($aItem['parent_id']) > 0) {
	  		$sSql = "SELECT id, IFNULL(parent_id, 0) AS parent_id
	  					 FROM ".$aConfig['tabls']['prefix']."menus_items
	  					 WHERE id = ".$aItem['parent_id']." AND
	  					 			 language_id = ".$iLangId;
		  	$aItem =& $pDbMgr->GetRow('profit24', $sSql);
		  	$aPath[] = $aItem['id'];
	  	}
		return array_reverse($aPath);
  	} else {
  		return false;
  	}
  } // end getPathItems() function
	
	
	/**
	 * Zwraca kategorę główną (pierwszego poziomu) dla produktu
	 * 
	 * @param $aCats - lista kategorii produktu
	 * @return unknown_type
	 */
	public function getPageId(&$aCats){
	global $aTopCategories;
	
		// pobierz kategorie glowne
		if(empty($aTopCategories)){
			$aTopCategories = $this->getTopCategories();
		}
		// wybierz z kategorii dodatkowych produktu kategorie glowne
		$aMatched = array_values(array_intersect($aCats,$aTopCategories));
		// zwroc pierwsza kategorie glowna
		if(!empty($aMatched)){
			return $aMatched[0];
		}
		// lub brak
		else{
			return false;
		}
	} // end of getPageId() function
	
	
	/**
	* Dodawanie roku do słownika lat
	* 
	* @param $iYear - rok
	* @return bool - success
	*/
	function addYearToDict($iYear){
		global $aConfig, $pDbMgr;
		
		if($iYear > 0) {
			$sSql="SELECT count(id) FROM ".$aConfig['tabls']['prefix']."products_years
						WHERE year = ".$iYear;
			if(intval($pDbMgr->GetOne('profit24', $sSql)) == 0){
				$aValues = array(
					'year' => $iYear,
					'created' => 'NOW()',
					'created_by' => 'import'
				);
				if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_years", $aValues, "", false) === false) {
					return false;
				} else {
					return true;
				}
			}
		}
		return true;
	}// end of addYearToDict() function
	
	
	/**
	* Pobiera id serii odanej nazwie i wydawnictwie,
	* jesli nie istnieje dodaje ja do tabeli products_series
	* 
	* @param string $sSeries - nazwa poszukiwanej serii
	* @param int $iPublisherId - id wydawnictwa poszukiwanejc serii
	* @return int - id poszukiwanej serii/false
	*/
	function getSeriesId($sSeries, $iPublisherId){
		global $aConfig, $pDB, $pDbMgr;
		
		$sSql="SELECT id FROM products_series
					WHERE publisher_id = ".$iPublisherId."
					AND name = ".$pDB->quoteSmart(stripslashes($sSeries));
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if($iId > 0) {
			// mamy już tę serię
			return $iId;
		} else {
			// tu trzeba jednak dodać nową
			$aValues = array(
				'name' => $sSeries,
				'publisher_id' => $iPublisherId,
				'created' => 'NOW()',
				'created_by' => 'import'
			);
			if (($iId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_series", $aValues)) === false) {
				$this->addErrorLog("błąd dodawania serii ".$sSeries);
				return false;
			}
			else {
				$this->addLog("dodano serie ".$sSeries);
				return $iId;
			}			
		}
		return false;
	} // end of getSeriesId() function

	
	/**
	 * Metoda dodaje autorów rozdzielonych przyecinkiem
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param type $PId
	 * @param type $sAuthors
	 * @return boolean 
	 */
	private function addAuthors($PId, $sAuthors, $iAuthorRoleId){
		global $aConfig, $pDbMgr;
		// $iAuthorRoleId = 1;
		
		if(!empty($sAuthors)){
			$aAuthors=split(',',$sAuthors);
			
			foreach($aAuthors as $iKey=>$sAuth){
				
				$sAuth=trim($sAuth);
				if(!empty($sAuth)){
					$aValues=array(
						'product_id' => $PId,
						'author_id' => $this->getAuthorId($sAuth),
						'role_id' => $iAuthorRoleId,
						'order_by' => $iKey
					);
					
					if ($this->checkAutor($aValues['product_id'], $aValues['author_id'], $aValues['role_id']) == false) {
						if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_to_authors", $aValues,'',false) === false) {
							$this->addErrorLog(date('d.m.Y')." ".date('H:i:s')." błąd dodawania autorów do produktu ".$PId);
							return false;
						}

						$authorListener = new ElasticListener();
						$authorListener->onProductAuthorAdd($PId);
					} else {
						// istnieje już ten autor przy tej pozycji
					}
				}
			}
		}
		return true;
	}// end of addAuthors() method


	/**
	 * Metoda dodaje powiazanie produktu ze źródłem, dodatkowo sprawdza czy zmienił się md5
	 *
	 * @param type $iProductId
	 * @param type $sSourceIndex
	 * @param type $iSourceId
	 * @param string|type $sNewMD5
	 * @global array $aConfig
	 * @return bool|int
	 * @global array $aConfig
	 * @global object $pDbMgr
	 */
	public function addBookIndeks($iProductId, $sSourceIndex, $iSourceId, $sNewMD5 = '', $mainIndex = null) {
		global $aConfig, $pDbMgr;
		
		if (intval($iProductId) <= 0 || $sSourceIndex == '' || intval($iSourceId) <= 0) return false;


		
		// pobierzmy dane
		$sSql = "SELECT md5, id FROM ".$aConfig['tabls']['prefix']."products_to_sources
						 WHERE product_id = ".$iProductId." AND 
						 	   source_id = ".$iSourceId." AND 
						 	   source_index = '".$sSourceIndex."' AND 
						 	   main_index = ".($mainIndex === true ? '"1"': '"'.$mainIndex.'"');
		$aProdSources = $pDbMgr->GetRow('profit24', $sSql);
		
		// zmiana MD5
		if (!empty($aProdSources) && $sNewMD5 !== $aProdSources['md5']) {
			$aValues = array(
				'md5' => $sNewMD5
			);
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_to_sources", $aValues, ' id = '.$aProdSources['id']) === false) {
				return false;
			}
			// md5 został zmieniony, należy zaktualizować produkt
			return 2;// status 2 oznacza pozwolenie na aktualizację
		}
		
		if (empty($aProdSources)) {
			$aValues = array(
					'product_id' => $iProductId,
					'source_id' => $iSourceId,
					'source_index' => $sSourceIndex,
					'md5' => ($sNewMD5 != '' ? $sNewMD5 : 'NULL')
			);
			if (null !== $mainIndex) {
				$aValues['main_index'] = $mainIndex;
			}
			if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_to_sources", $aValues, '', false) === false) {
				return false;
			}
			return 2;
		} else {
			// istnieje produkt i prawdopodobnie ma ten sam md5
			return true;
		}
	}// end of addBookIndeks() method
	
	
	/**
	 * Metoda generuje md5 dla produktu, bez cen oraz statusu produktu
	 * 
	 * @param array $aProduct
	 * @return string
	 */
	public function getProductMD5($aProduct) {
		unset($aProduct['_stock'], $aProduct['_wholesale_price'], $aProduct['vat'], 
				  $aProduct['price_brutto'], $aProduct['price_netto'], $aProduct['prod_status'],
					$aProduct['shipment_time']);
		return md5(serialize($aProduct));
	}// end of getProductMD5() method
	

	/**
	 * Metoda sprawdza czy autor jest już powiązany z daną książką
	 *
	 * @global array $aConfig
	 * @param int $PId - id książki
	 * @param int $iAId - id autora
	 * @param int $iAuthorRoleId - id roli autora
	 * @return boolean|int
	 */
	function checkAutor($PId, $iAId, $iAuthorRoleId) {
		global $aConfig, $pDbMgr;

		if (intval($PId) > 0 && intval($iAId) > 0 && intval($iAuthorRoleId) > 0) {
			$sSql = "SELECT id 
							 FROM ".$aConfig['tabls']['prefix']."products_to_authors 
							 WHERE 
								product_id = ".$PId." AND 
								author_id = ".$iAId." AND 
								role_id = ".$iAuthorRoleId;
			return (intval($pDbMgr->GetOne('profit24', $sSql)) > 0 ? true : false);
		}
		return false;
	} // end of checkAutor() method
	
	

	/**
	* Pobiera id autora, jeśli nie istnieje dodaje go do bazy
	* @param string $sAuthor - autor w postaci Imie Nazwisko
	* @return int - id autora
	*/
	private function getAuthorId($sAuthor){
		global $aConfig, $pDB, $pDbMgr;

		if(($iSep = mb_strrpos($sAuthor,' ',0, 'utf8')) !== false){
			$sName = trim(mb_substr($sAuthor,0,$iSep, 'utf8'));
			$sSurname = trim(mb_substr($sAuthor,$iSep+1,mb_strlen($sAuthor, 'utf8')-$iSep, 'utf8'));
		} else {
			$sSurname=$sAuthor;
			$sName='';
		}

		$sSql="SELECT id FROM products_authors
					WHERE name = ".$pDB->quoteSmart(stripslashes($sName))." AND surname = ".$pDB->quoteSmart(stripslashes($sSurname));
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if ($iId > 0) {
			return $iId;
		}

		$aValues = array(
			'name' => $sName,
			'surname' => $sSurname,
			'index_letter' => strtoupper(mb_substr($sSurname, 0, 1, 'utf8')),
			'created' => 'NOW()',
			'created_by' => 'import'
		);
		if (($iId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_authors_import", $aValues)) === false) {
			$this->addErrorLog("error adding Author ".$sName.' '.$sSurname);
			return false;
		}
		else {
			//dopisanie do tabeli autora i utworzenie mapowania
			$aValues2 = $aValues;
			$aValues2['id'] = $iId;
			$bErr = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_authors", $aValues2, '', false);
			if($bErr === false) {
				$this->addErrorLog("error adding Author ".$sName.' '.$sSurname);
				return $bErr;
			}

			$aValues3=array(
				'author_id' => $iId, 	
				'author_import_id' => $iId
			);

			$pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_authors_mappings",
										 $aValues3, '', false);

			$authorListener = new ElasticListener();
			$authorListener->onAuthorAdd($iId);

			return $iId;
			//koniec dopisania
		}
	} // end of getAuthorId() function
	
	
	/**
	*	 Dodaje powiązanie serii do produktu
	* 
	* @param int $PId - id produktu
	* @param string $sSeries - nazwa serii
	* @param int $iPublisherId - id wydawnictwa
	* @return mixed - false|id serii
	*/
	private function addSeries($PId, $sSeries, $iPublisherId){
		global $aConfig, $pDbMgr;
	
		if(!empty($sSeries)){
			$iSeriesId = $this->getSeriesId($sSeries, $iPublisherId);
			$aValues = array(
				'product_id' => $PId,
				'series_id' => $iSeriesId
			);
			if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_to_series", $aValues, '', false) === false) {
				$this->addErrorLog("błąd dodawania serii do produktu ".$PId);
				return false;
			}
		}
		return $iSeriesId;
	} // end of addSeries() function
	
	
	/**
	 * Metoda pobiera serie dla produktu
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param type $PId
	 * @return type
	 */
	private function getSeries($PId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_to_series
						 WHERE product_id = ".$PId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSeries() method


	/**
	 * Metoda pobiera id wydawcy z własciwej tabeli wydawców produktów
	 * 
	 * @global array $aConfig
	 * @global object $pDB
	 * @param string $sName
	 * @return int
	 */
	public function getProductPublisherId($sPublisher) {
		global $aConfig, $pDB;
		$sPublisher = trim($sPublisher);

		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers
						 WHERE name = ".$pDB->quoteSmart(stripslashes($sPublisher));
		return Common::GetOne($sSql);
	}// end of getProductPublisherId() method
  
  
	/**
	 * Metoda pobiera nazwę wydawcy na podstawie
	 * 
	 * @global array $aConfig
	 * @global object $pDB
	 * @param string $sName
	 * @return int
	 */
	public function getProductPublisherName($iPublisherId) {
		global $aConfig;

		$sSql = "SELECT name FROM ".$aConfig['tabls']['prefix']."products_publishers
						 WHERE id = ".$iPublisherId;
		return Common::GetOne($sSql);
	}// end of getProductPublisherName() method
	
  
  /**
   * Metoda aktualizuje wydawcę
   * 
   * @global object $pDbMgr
   * @param int $iProductId
   * @param string $sPublisher
   * @return boolean
   */
  public function updatePublisher($iProductId, $sPublisher) {
    global $pDbMgr;
    
    $sSql = "SELECT publisher_id 
             FROM products
             WHERE id = ".$iProductId." AND publisher_id IS NULL ";
    $iPublisherId = $pDbMgr->GetOne('profit24', $sSql);
    if (intval($iPublisherId) <= 0 && $sPublisher != '') {
      // brak wydawcy
      $iPublisherId = $this->getPublisherId($sPublisher);
      if ($iPublisherId > 0) {
        
        $aValues = array(
            'publisher_id' => $iPublisherId
        );
        $pDbMgr->Update('profit24', 'products', $aValues, ' id = '.$iProductId);
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }// end of updatePublisher
  
  
	/**
	 * Metoda Pobiera id wydawnictwa, jeśli nie ma w bazie dodaje (do tabeli wydawnictw,
	 *	tabeli ukrytej i tworzy między nimi mapowianie (opisane w getPublisherId.png)
	 *
	 * @global array $aConfig - globalna tablica konfiguracji
	 * @global type $pDB - objekt bazy danych
	 * @global object $pDbMgr - objekt menadzera baz danych
	 * @param type $sPublisher - nazwa wydawnictwa
	 * @return boolean|string  - id wydawnictwa lub błąd
	 */
	public function getPublisherId($sPublisher){
		global $aConfig, $pDB, $pDbMgr;
		
		$sPublisher=trim($sPublisher);
		if ($sPublisher === '') {
			return 'NULL';
		}
		$sSql="SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers_import
					WHERE name = ".$pDB->quoteSmart(stripslashes($sPublisher));
		$iIId = $pDbMgr->GetOne('profit24', $sSql);
		
		// wydawnictwo istnieje w tabeli ukrytej
		if($iIId > 0) {
			$sSql = "SELECT publisher_id 
							FROM ".$aConfig['tabls']['prefix']."products_publishers_mappings
							WHERE publisher_import_id = ".$iIId;
			$iPId=intval($pDbMgr->GetOne('profit24', $sSql));
			// mamy mapowanie - zwracamy id
			if($iPId >0 ) {
				return $iPId;
			}
		}
		
		// nie mamy mapowania
		if(!empty($sPublisher)) {
			
			$iId = $this->getProductPublisherId($sPublisher);
			if (intval($iId) <= 0) {

				// dodajemy do tabeli glownej
				$aValues=array(
					'name'=>$sPublisher,
					'index_letter' => strtoupper(mb_substr($sPublisher, 0, 1, 'utf8')),
					'created' => 'NOW()',
					'created_by' => 'import'
				);
				$iId=0;
				if (($iId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_publishers", $aValues)) === false) {
					$this->addErrorLog("błąd dodawania wydawcy ".$sPublisher);
					return false;
				}
			}
			
			if (intval($iId) > 0 && intval($iIId) <= 0) {
				
				$this->addLog("dodano wydawcę ".$sPublisher);
				// nie mamy w tabeli importu - dodajemy
				if(intval($iIId) <= 0) {
					$aValues=array(
						'name'=>$sPublisher
					);
					if (($iIId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_publishers_import", $aValues)) === false) {
						$this->addErrorLog("błąd dodawania wydawcy ".$sPublisher);
						return false;
					}
				}
			}
			if ($iId > 0 && $iIId > 0) {
				// dodajemy mapowanie
				$aValues = array(
					'publisher_id' => $iId,
					'publisher_import_id' => $iIId
				);
				if (($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_publishers_mappings", $aValues,'',false)) === false) {
					$this->addErrorLog("błąd dodawania mapowania wydawcy ".$sPublisher);
					return false;
				}
				return $iId;
			} else {
				$this->addErrorLog("błąd dodawania mapowania wydawcy, brak idkow");
				return false;
			}
		} else {
			return 'NULL';
		}		
	} // end of getPublisherId() method
  
  /**
   * Metoda pobiera id wersji językowej na podstawie tabeli kodowej języków
   * 
   * @global object $pDbMgr
   * @param string $sLangCode
   * @return null|int
   */
  protected function getLanguageIdByCode($sLangCode) {
   global $pDbMgr;
   
    if ($this->iSourceId > 0) {
      $iSId = $this->iSourceId;
    } 
    elseif ($this->oSourceElements->iSourceId > 0) {
      $iSId = $this->oSourceElements->iSourceId;
    }
    
    if ($iSId > 0 && $sLangCode != '') {
      if (isset($this->aLanguagesByCode[$iSId][$sLangCode])) {
        return $this->aLanguagesByCode[$iSId][$sLangCode];
      }
      
      $sSql = "SELECT name 
               FROM products_languages_mappings
               WHERE source_symbol = '".$sLangCode."'
                     source_id = ".$iSId;
      $sLangName = $pDbMgr->GetOne('profit24', $sSql);
      $iLangId = $this->getLanguageId($sLangName);
      if ($iLangId > 0) {
        $this->aLanguagesByCode[$iSId][$sLangCode] = $iLangId;
        return $iLangId;
      }
    }
    return NULL;
  }// end of getLanguageIdByCode() method
	
  
  /**
   * Pobiera id jędyka, jeśli nie istanieje dodaje go do bazy
   * 
   * @param string $sLanguage - język
   * @return int - id języka
   */
  protected function getLanguageId($sLanguage){
    global $aConfig, $pDbMgr, $pDB;
    
    if(!empty($sLanguage)){
      $sSql="SELECT id FROM products_languages
             WHERE language = ".$pDB->quoteSmart(stripslashes(trim($sLanguage)));
      $iId = $pDbMgr->GetOne('profit24', $sSql);
      if($iId > 0)
        return $iId;

      $aValues=array(
        'language' => trim($sLanguage),
        'created' => 'NOW()',
        'created_by' => 'import'
      );
      if (($iId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_languages", $aValues)) === false) {
        echo "error adding Language \n";
        dump($aValues);
        return 'NULL';
      }
      else
        return $iId;		
    }
    else
      return 'NULL';	
  } // end of getLanguageId() function

	
	/**
	* Dodaje powiązania kategorii do produktu
	* 
	* @param int $PId - id produktu
	* @param array $aCategories - tablica z id kategorii
	* @return bool - success
	*/
	protected function addExtraCategories($PId, $aCategories , $bInsert = true){
		global $aConfig, $pDbMgr;
		$aOldCategories = array();

//        //TODO zmiana kategorii
//
//        include_once($aConfig['common']['base_path'].'LIB/Product/ProductUpdater.php');
//        $productUpdater = new ProductUpdater();
//        $productUpdater->updateProductMainCategory($PId);
		
		if ($bInsert === false) {
			$aOldCategories = $this->getExtraCategories($PId);
      sort($aOldCategories, SORT_NUMERIC);
		}
    sort($aCategories, SORT_NUMERIC);
		if(!empty($aCategories) && ($aOldCategories != $aCategories)){
			foreach($aCategories as $iCat){
			// dodaj jeśli nowa kategoria jest niepusta, 
			// jeśli jest różna od nieposortowane,
			// nie jest istnieje aktualnie to mapowanie
			if (!empty($iCat) && 
					($iCat != $aConfig['import']['unsorted_category']) && 
					($iCat != $aConfig['import']['unsorted_category_editio']) && 
					!in_array($iCat, $aOldCategories)) {
					$aValues=array(
						'product_id' => $PId,
						'page_id' => $iCat
					);

					if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_extra_categories", $aValues, '', false) === false) {
						return false;
					}
				}
			}
		}
		return true;
	} // end of addExtraCategories() function
	
	
	/**
	 * Metoda pobiera dodatkowe kategorie dla produktu
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	private function &getExtraCategories($iId){
		global $aConfig, $pDbMgr;
		
		$sSql="SELECT page_id
					 FROM ".$aConfig['tabls']['prefix']."products_extra_categories
					 WHERE product_id = ".$iId;
		return $pDbMgr->GetCol('profit24', $sSql);
	}// end of getExtraCategories() function 
	
	
	/**
	* Pobiera kategorie główne (działy)
	* @return array - tablica z id kategorii głównych
	*/
	private function getTopCategories(){
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id 
					FROM ".$aConfig['tabls']['prefix']."menus_items
					WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					AND mtype = '0'
					AND parent_id IS NULL";
		return $pDbMgr->GetCol('profit24', $sSql);
	} // end of getTopCategories() function
	
	
	/**
	* Tworzy folder i ustawia prawa dostępu dla folderu na okładke produktu
	* 
	* @param unknown_type $iPId
	* @return unknown_type
	*/
	protected function createProductImageDir($iPId){
		global $aConfig;
		$old_umask = umask(0);
		$iRange = intval(floor($iPId/1000))+1;
		if (!is_dir($aConfig['common']['base_path']."images/photos/okladki/".$iRange.'/'.$iPId)) {
			mkdir($aConfig['common']['base_path']."images/photos/okladki/".$iRange.'/'.$iPId,0777,true);
		}
		umask($old_umask);
		return $aConfig['common']['base_path']."images/photos/okladki/".$iRange.'/'.$iPId;
	} // end of createProductImageDir() function
	

	/**
	* Wysyła maila z logiem importu
	* @param $sTopic - temat maila
	* @param $sContent - treść maila
	* @return void
	*/
	function sendInfoMail($sTopic,$sContent){
		global $aConfig;

		if(!Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'], $sTopic, $sContent))
			dump('error sending mail');
	} // end of sendInfoMail() function
	
	
	/**
	 * Metoda dodaje log do tablicy logów
	 *
	 * @param type $sLog 
	 */
	function addLog($sLog) {
		$this->aLog[] = $sLog;
	}// end of addLog() method
	
	
	/**
	 * Metoda dodaje error log do tablicy error logów
	 *
	 * @param type $sErrorLog 
	 */
	function addErrorLog($sErrorLog) {
		$this->aErrorLog[] = $sErrorLog;
	}// end of addErrorLog() method
	
	
	/**
	* Metoda pobiera tekstową wartość węzła xml
	* 
	* @param object ref $oXml - obiekt XMLReader
	* @return string - wartosc
	*/
	function getTextNodeValue(&$oXml) {
		
		// przejscie do kolejnego wezla
		if (!$oXml->isEmptyElement) {
			$sStr = $oXml->readString();
			$oXml->read();
			if ($oXml->nodeType == XMLReader::TEXT 
					|| $oXml->nodeType == XMLReader::CDATA) {
				
				return trim($oXml->value);
			} elseif ($oXml->nodeType == XMLReader::WHITESPACE
								|| $oXml->nodeType == XMLReader::SIGNIFICANT_WHITESPACE) {
				
				return trim($sStr);
			}
		}
		return '';
	} // end of getTextNodeValue() method
	
	
	/**
	* Metoda pobiera tekstową wartość elementu, 
	* metoda stworzona w razie potrzeby dane z CSV należałoby filtrować to tutaj najłatwiej
	*  
	* 
	* @param object ref $oXml - obiekt XMLReader
	* @return string - wartosc
	*/
	function getText($sVal) {
		
		if ($sVal != '') {
			return trim($sVal);
		}
	} // end of getTextNodeValue() method
	
	
	/**
	* Czyści błędne kody ascii z opisu
	* @param string $sStr - string wejściowy
	* @return string - przeczyszczony
	*/
  function clearAsciiCrap($sStr) {
    $sStr = (string)str_replace("\x0A", '<br />', $sStr);
    return (string)str_replace(array("\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x09","\x0B","\x0C","\x0D","\x0E","\x0F","\x10","\x11","\x12","\x13","\x14","\x15","\x16","\x17","\x18","\x19","\x1A","\x1B","\x1C","\x1D","\x1E","\x1F"), ' ', $sStr);
  } // end of clearAsciiCrap() function
	
	
	/**
	* Pobiera id oprawy, jeśli nie istnieje dodaje go
	 * 
	* @param string $sBinding - oprawa
	* @return int - id oprawy
	*/
	function getBindingId($sBinding){
		global $aConfig,$pDB, $pDbMgr;

		if(!empty($sBinding)){
			$sSql="SELECT id FROM products_bindings
						WHERE binding = ".$pDB->quoteSmart(stripslashes(trim($sBinding)));
			$iId=$pDbMgr->GetOne('profit24', $sSql);
			if($iId > 0)
				return $iId;

			$aValues=array(
				'binding'=>trim($sBinding),
				'created'=>'NOW()',
				'created_by'=>'import'
			);
			if (($iId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_bindings", $aValues)) === false) {
				echo "error adding Binding \n";
				dump($aValues);
				return 'NULL';
			}
			else
				return $iId;	
		}
		else
			return 'NULL';			
	} // end of getBindingId() function
	
	
	/**
	 * Metoda sprawdza czy jest to nowość
	 *
	 * @global type $aConfig
	 * @param integer $iId
	 * @return return array
	 */
	public function &getNews($iId){
		global $aConfig, $pDbMgr;
		
		$sSql="SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_news
					 WHERE product_id = ".$iId;
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getNews() method
	
	
	/**
	 * Metoda sprawdza czy jest to nowość
	 *
	 * @global type $aConfig
	 * @param integer $iId
	 * @return return array
	 */
	public function &getPreviews($iId){
		global $aConfig, $pDbMgr;
		
		$sSql="SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_previews
					 WHERE product_id = ".$iId;
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getPreviews() method
	
	
	/**
	 * Dodaje pozycję do nowości
	 * 
	 * @param int $iPId - id produktu
	 * @param int $iPageId - id kategorii nowości
	 * @return bool success
	 */
	public function addNews($iPId, $iPageId = 423){
		global $aConfig, $pDbMgr;
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='1'
							WHERE id = ".$iPId;
		if($pDbMgr->Query('profit24', $sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($iPId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news='1'
							WHERE id = ".$iPId;
			if($pDbMgr->Query('profit24', $sSql) === false) {
				$bIsError = true;
			}
		}
	 	
 		$aValues = array (
	 		'product_id' => $iPId,
	 		'news_from' => 'NOW()',
	 		'page_id' => $iPageId
	 	);
 	
 		if($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_news",
 										$aValues,'',false) === false) {
			$bIsError = true;
		}
		return !$bIsError;
	} // end of addNews() method
	
	
	/**
	 * Dodaje pozycję do nowości
	 * 
	 * @param int $iPId - id produktu
	 * @param int $iPageId - id kategorii nowości
	 * @return bool success
	 */
	public function addPreviews($iPId, $iPageId = 2337){
		global $aConfig, $pDbMgr;
    $bIsError = false;
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_previews = '1'
							WHERE id = ".$iPId;
		if($pDbMgr->Query('profit24', $sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($iPId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_previews = '1'
							WHERE id = ".$iPId;
			if($pDbMgr->Query('profit24', $sSql) === false) {
				$bIsError = true;
			}
		}
	 	
 		$aValues = array (
	 		'product_id' => $iPId,
	 		'page_id' => $iPageId
	 	);
 	
 		if($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_previews",
 										$aValues,'',false) === false) {
			$bIsError = true;
		}
		return !$bIsError;
	} // end of addPreviews() method
	
	
	/**
	 * Sprawdza czy istnieje wpis w tabeli shadow dla produktu o danym id
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iId - id produktu
	 * @return bool
	 */
	protected function existShadow($iId) {
		global $aConfig, $pDbMgr;

		// sprawdzanie cienia
		$sSql = "SELECT count(id) FROM ".$aConfig['tabls']['prefix']."products_shadow
							WHERE id = ".$iId;
		return ($pDbMgr->GetOne('profit24', $sSql) > 0);
	} // end of existShadow() method
	
	
	/**
	 * Metoda zmienia zmienia rozmiar i przenosi zdjęcie do wybranego katalogu
	 *
	 * @global array $aConfig
	 * @param string $sTmpFileName - ścieżka pliku źródłowego
	 * @param array $aImageSize - rozmiar zdjęcia źródłowego
	 * @param string $sDestDir - ścieżka pliku docelowego
	 * @param string $sNewWidthHeight - nowy rozmiar pliku np.250x60
	 * @param string $sNewName - nazwa pliku docelowego
	 * @param type $iMode - chmod w jakim tworzone są pliki
	 * @param type $iQuality - jakość
	 * @return boolean 
	 */
	protected function resizeAndMoveImageTo($sTmpFileName, &$aImageSize, $sDestDir, $sNewWidthHeight, $sNewName='', $iMode=0664, $iQuality=90) {
		$iNewWidth	= 0;
		$iNewHeight	= 0;

		// od tego miejsca zaczyna sie kod odpowiedzialny za zmiane rozmiaru obrazka
		$aNewSize = explode('x', $sNewWidthHeight);
		if ($aImageSize[0] > $aNewSize[0] || $aImageSize[1] > $aNewSize[1]) {
			// okreslenie nowych rozmiarow
			if ($aImageSize[0] > $aImageSize[1]) {
				// szerokosc jest wieksza od wysokosci
				$iNewWidth =& $aNewSize[0];
				$iNewHeight = ceil(($iNewWidth * $aImageSize[1]) / $aImageSize[0]);
			}
			else {
				// wysokosc jest wieksza od szerokosci
				$iNewHeight =& $aNewSize[1];
				$iNewWidth = ceil(($iNewHeight * $aImageSize[0]) / $aImageSize[1]);
			}
			if ($iNewWidth > $aNewSize[0]) {
				// szerokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
				// szerokosc maxymalna, dostosowanie szerokosci do szerokosci max.
				// przeliczenie na nowo wysokosci
				$iNewWidth = $aNewSize[0];
				$iNewHeight = ceil(($iNewWidth * $aImageSize[1]) / $aImageSize[0]);
			}
			if ($iNewHeight > $aNewSize[1]) {
				// wysokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
				// wysokosc maxymalna, dostosowanie wysokosci do wysokosci max.
				// przeliczenie na nowo szerokosci
				$iNewHeight = $aNewSize[1];
				$iNewWidth = ceil(($iNewHeight * $aImageSize[0]) / $aImageSize[1]);
			}
			// utworzenie obrazka o nowych rozmiarach
			if ($aImageSize['mime'] == "image/pjpeg" ||
					$aImageSize['mime'] == "image/jpeg" ||
					$aImageSize['mime'] == "image/jpg") {
				$oHandle = imagecreatefromjpeg($sTmpFileName);
			}
			elseif ($aImageSize['mime'] == "image/png") {
				$oHandle = imagecreatefrompng($sTmpFileName);
			}
			elseif ($aImageSize['mime'] == "image/gif") {
				$oHandle = imagecreatefromgif($sTmpFileName);
			}
			else {
				return false;
			}
			$oDstImg = imagecreatetruecolor($iNewWidth, $iNewHeight);
			if (!$oDstImg) {
				return false;
			}

		// przezroczystosc
			if ($aImageSize['mime'] == "image/png") {
				// PNG
				imagealphablending($oDstImg, false);
				$iColorTransparent = imagecolorallocatealpha($oDstImg, 0, 0, 0, 127);
				imagefill($oDstImg, 0, 0, $iColorTransparent);
				imagesavealpha($oDstImg, true);
			}
			elseif ($aImageSize['mime'] == "image/gif") {
				// GIF
				$iTranspInd = imagecolortransparent($oHandle);
				if ($iTranspInd >= 0) {
					$aTranspColor = imagecolorsforindex($oHandle, $iTranspInd);
					$iTranspIndex = imagecolorallocate($oDstImg, $aTranspColor['red'], $aTranspColor['green'], $aTranspColor['blue']);
					imagefill($oDstImg, 0, 0, $iTranspIndex);
					imagecolortransparent($oDstImg, $iTranspIndex);
				}
			}

			if (!imagecopyresampled($oDstImg, $oHandle, 0, 0, 0, 0, $iNewWidth, $iNewHeight, $aImageSize[0], $aImageSize[1])) {
				return false;
			}
			imagedestroy($oHandle);
			if ($aImageSize['mime'] == "image/pjpeg" ||
					$aImageSize['mime'] == "image/jpeg" ||
					$aImageSize['mime'] == "image/jpg") {
				if (!imagejpeg($oDstImg, $sDestDir.'/'.$sNewName, $iQuality)) {
					return false;
				}
			}
			elseif ($aImageSize['mime'] == "image/png") {
				if (!imagepng($oDstImg, $sDestDir.'/'.$sNewName)) {
					return false;
				}
			}
			elseif ($aImageSize['mime'] == "image/gif") {
				if (!imagegif($oDstImg, $sDestDir.'/'.$sNewName)) {
					return false;
				}
			}
			else {
				return false;
			}
			@imagedestroy($oDstImg);
			@chmod($sDestDir.'/'.$sNewName, $iMode);
		}
		else {
			// zwykle przeniesienie pliku bez zmiany jego rozmiaru
			if (!@copy($sTmpFileName, $sDestDir.'/'.$sNewName)) {
				return false;
			}
		}
		// zmieniamy atrybuty
		@chmod($sDestDir.'/'.$sNewName, $iMode);
		return true;
	} // end of resizeAndMoveImageTo() method
	
	
	/**
	 * Pobiera dane załącznika produktu o danym id
	 * 
	 * @param int $iId - id produktu
	 * @return array ref - tablica z id, cena brutto i netto załącznika 
	 */
	public function getAttachment($iId) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT A.id,A.price_brutto,A.price_netto
						FROM ".$aConfig['tabls']['prefix']."products_attachments A
						WHERE A.product_id= ".$iId;	
		return $pDbMgr->GetRow('profit24', $sSql);
	} // end of getAttachment() function
	
	
	/**
	 * Metoda zamienia stan produktu ze źródła stan na magazynie wg. mapowań Profit24.pl
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param string $sSrcSymPrefix
	 * @param mixed $mStatus
	 * @return string
	 */
	public function parseProductStock($sSrcSymPrefix, $mStatus) {
		global $aConfig, $pDbMgr;
		$iSId = 0;
		
		// potrzebne tylko jeśli stock jest zdefiniowany
		if (isset($mStatus)) {
			if (empty($this->aStockMapping)) {
				// ustawiamy id źródła pobieramy id źródła
				if ($this->iSourceId > 0) {
					$iSId = $this->iSourceId;
				} 
				elseif ($this->oSourceElements->iSourceId > 0) {
					$iSId = $this->oSourceElements->iSourceId;
				} else {
					$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources 
									 WHERE symbol='".$sSrcSymPrefix."'";
					$iSId = intval($pDbMgr->GetOne('profit24', $sSql));
				}
				
				// jeśli mamy w koncu ten source id
				$sSql = "SELECT input, output 
								 FROM ".$aConfig['tabls']['prefix']."products_stock_source_mapping
								 WHERE source_id = ".$iSId;
				$this->aStockMapping = $pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC, false);
			}
			
			
			// pobieramy wszystkie mapowania statusów
			if (!empty($this->aStockMapping)) {
				// sprwawdźmy stock
				if (isset($this->aStockMapping[$mStatus])) {
					return sprintf($this->aStockMapping[$mStatus], $mStatus);
				} else {
					// sprwdźmy elementy które mają podany zakres
					foreach ($this->aStockMapping as $sIn => $sOut) {
						if (strstr($sIn, '-')) {
							$aMinMax = explode('-', $sIn);
							if ( isset($aMinMax[0]) && isset($aMinMax[1]) ) {
								if ($aMinMax[0] <= intval($mStatus) && $aMinMax[1] >= intval($mStatus)) {
									return sprintf($sOut, $mStatus);
								}
							}
						}
					}
				}
				
			} else {
				// niestety nie mamy, trzeba dla bezpieczeństwa usunąć status
				return '';
			}
		}
		return '';
	}// end of parseProductStock() method
	
	
	/**
	 * Pobiera mapowania kategorii dla wydawnictwa
	 * 
	 * @param int $iPublisherId - id wydawnictwa
	 * @return array - tablica z id kategorii głównej i id kategorii nowości
	 */
	public function getPublisherCategories($iPublisherId){
		global $aConfig;

		$sSql = "SELECT main_category, news_category 
						FROM ".$aConfig['tabls']['prefix']."products_publishers
						WHERE id = ".$iPublisherId;
		return Common::GetRow($sSql);
	} // end of getPublisherCategories()
	
	
	/**
	 * Pobiera mapowania kategorii dla serii
	 * 
	 * @param int $iSeriesId - id serii
	 * @return array - tablica z id kategorii głównej i id kategorii nowości
	 */
	public function getSeriesCategories($iSeriesId){
		global $aConfig;
		$sSql = "SELECT main_category, news_category 
						FROM ".$aConfig['tabls']['prefix']."products_series
						WHERE id = ".$iSeriesId;
		return Common::GetRow($sSql);
	} // end of getSeriesCategories()
		
  
	/**
   * Metoda pobiera domyślną kategorię główną nowości lub zapowiedzi na podstawie źródła.
   * 
   * @param string $sType - news|previews
   * @param type $sSourceSymbol - symbol źródła
   * @return int|boolean
   */
  private function getDefaultMainCategoryBySource($sType, $sSourceSymbol) {
    
    if ($sType == 'news') {
      
      switch ($sSourceSymbol) {
        case 'abe':
          // jezykowy
          $iNewsMainCategory = 424;
        break;
        case 'dictum':
          // ogolny
          $iNewsMainCategory = 423;
        break;
        case 'azymut':
          // ogolny
          $iNewsMainCategory = 423;
        break;
		case 'atenuem':
		  // ogolny
		  $iNewsMainCategory = 423;
		break;
        case 'helion':
          // helion domyślnie naukowy
          $iNewsMainCategory = 425;
        break;
        case 'olesiejuk':
          // ogolny - ustalone 16.10.2012
          $iNewsMainCategory = 423;
        break;

        case 'siodemka':
          // ogolny - ustalone 16.10.2012
          $iNewsMainCategory = 423;
        break;

        case 'Platon':
          $iNewsMainCategory = 423;
        break;
      }
      
      return $iNewsMainCategory;
    } elseif ($sType == 'previews') {
      
      switch ($sSourceSymbol) {
        case 'abe':
          // jezykowy
          $iPreviewsMainCategory = 2339;
        break;
        case 'dictum':
          // ogolny
          $iPreviewsMainCategory = 2337;
        break;
        case 'azymut':
          // ogolny
          $iPreviewsMainCategory = 2337;
        break;
		case 'ateneum':
		  // ogolny
		  $iPreviewsMainCategory = 2337;
		break;
        case 'helion':
          // helion domyślnie naukowy
          $iPreviewsMainCategory = 2338;
        break;
        case 'olesiejuk':
          // ogolny - ustalone 16.10.2012
          $iPreviewsMainCategory = 2337;
        break;

        case 'siodemka':
          // ogolny - ustalone 16.10.2012
          $iPreviewsMainCategory = 2337;
        break;

        case 'Platon':
          $iPreviewsMainCategory = 2337;
        break;
      }
      
      return $iPreviewsMainCategory;
    }
    return false;
  }// end of getDefaultMainCategoryBySource() method
  
  
  /**
   * Metoda sprawdza czy dany produkt powinien być głównym jego indeksem produktu
   * 
   * @param array $aProduct
   * @return bool
   */
  private function _checkSourceMainIndex($aProduct) {
    return (isset($aProduct['_stock']) && is_numeric($aProduct['_stock']) && intval($aProduct['_stock']) > 0 && $aProduct['_id'] != '');
  }// end of _checkSourceMainIndex() method
  
  
	/**
	 * Metoda dokonuje aktualizacji stanu produktu
	 *
	 * @param string $sSrcSymPrefix - symbol źródła używany do prefixu źródła w tabeli stanów produktów
	 * @param integer $iProductId - id produktu
	 * @param array $aProduct - tablica danych produktu
	 * @param bool $bUpdate - czy ma zostac wykonana aktualizacja
	 * @param int	 $iNewsMainCategory - kategoria gówna nowości
	 * @param int	 $iPreviewsMainCategory - kategoria gówna zapowiedzi
	 * @return bool Czy się powiodło
	 */
	public function proceedProductStock($sSrcSymPrefix, $iProductId, $aProduct, $bUpdate, $iNewsMainCategory = NULL, $iPreviewsMainCategory = NULL) {
		global $aConfig, $pDbMgr;
    
    // XXX fix publishers
    /*
    if ($aProduct['_publisher'] != '' && $iProductId > 0) {
      $this->updatePublisher($iProductId, $aProduct['_publisher']);
    }
    */
    
		if (intval($iNewsMainCategory) <= 0 && intval($iPreviewsMainCategory) <= 0) {
			// jeśli nie ustawiono kategorii głownych
			if ($aProduct['publisher_id'] > 0) {
				$aPublisherCats = $this->getPublisherCategories($aProduct['publisher_id']);
			}
			if (!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0) {
				// nadpisanie id głównej kategorii nowości
				$iNewsMainCategory = $aPublisherCats['news_category'];
				// wybranie id zapowiedzi
				$iPreviewsMainCategory = $this->getPreviewPageIdByNewsPageId($iNewsMainCategory);
			}
			
			// sprawdzamy czy seria wydawnicza ma zdefiniowaną kategorię główną nowości
			if (intval($aProduct['_items_series_id']) > 0) {
				$aSeriesCategoryTMP = $this->getSeriesCategories($aProduct['_items_series_id']);
				if (intval($aSeriesCategoryTMP['news_category']) > 0) {
					// zmieniamy kategorię główną nowości
					$iNewsMainCategory = $aSeriesCategoryTMP['news_category'];
					$iPreviewsMainCategory = $this->getPreviewPageIdByNewsPageId($iNewsMainCategory);
				}
			}

			if ($iNewsMainCategory <= 0 && $iPreviewsMainCategory <= 0) {
				$iNewsMainCategory = $this->getDefaultMainCategoryBySource('news', $sSrcSymPrefix);
        $iPreviewsMainCategory = $this->getDefaultMainCategoryBySource('previews', $sSrcSymPrefix);
			}
		}
		
		if (isset($aProduct['_stock'])) {
			$aProduct['_stock'] = $this->parseProductStock($sSrcSymPrefix, $aProduct['_stock']);
		}
		
		$aStatusValues = array();
		if (isset($aProduct['prod_status']) && $aProduct['prod_status'] != '') $aStatusValues[$sSrcSymPrefix.'_status'] = $aProduct['prod_status'];
		if (isset($aProduct['price_brutto']) && $aProduct['price_brutto'] != '') $aStatusValues[$sSrcSymPrefix.'_price_brutto'] = $aProduct['price_brutto'];
		if (isset($aProduct['vat']) && $aProduct['vat'] != '') $aStatusValues[$sSrcSymPrefix.'_vat'] = $this->vatMapper($aProduct['vat']);
		if (isset($aProduct['price_netto']) && $aProduct['price_netto'] != '' && 
				isset($aProduct['price_brutto']) && $aProduct['price_brutto'] != '' &&
				isset($aProduct['vat']) && $aProduct['vat'] != '') {
			$fAttCorrection = 0;
			$aAtt = $this->getAttachment($iProductId);
			if(!empty($aAtt) && $aAtt['price_brutto'] > 0){
				$fAttCorrection = $aAtt['price_brutto'];
			}
			$aStatusValues[$sSrcSymPrefix.'_price_netto'] = Common::formatPrice2(($aProduct['price_brutto'] - $fAttCorrection)/(1+intval($aStatusValues[$sSrcSymPrefix.'_vat'])/100));
		}
		if (isset($aProduct['shipment_time']) && $aProduct['shipment_time'] != '') {
			$aStatusValues[$sSrcSymPrefix.'_shipment_time'] = $aProduct['shipment_time'];
		} elseif($bUpdate === false) {
			$aStatusValues[$sSrcSymPrefix.'_shipment_time'] = '1';
		}
		if (isset($aProduct['_shipment_date']) && $aProduct['_shipment_date'] != '') $aStatusValues[$sSrcSymPrefix.'_shipment_date'] = $aProduct['_shipment_date'];
		if (isset($aProduct['_wholesale_price']) && $aProduct['_wholesale_price'] != '') $aStatusValues[$sSrcSymPrefix.'_wholesale_price'] = $aProduct['_wholesale_price'];
		if (isset($aProduct['_stock']) && $aProduct['_stock'] != '') $aStatusValues[$sSrcSymPrefix.'_stock'] = $aProduct['_stock'];
		if (isset($aProduct['_location']) && $aProduct['_location'] != '') $aStatusValues[$sSrcSymPrefix.'_location'] = $aProduct['_location'];
		if (!empty($aStatusValues)) {
			$aStatusValues[$sSrcSymPrefix.'_last_import'] = 'NOW()';
		
      // jeśli źródło obsługuje wiele indeksów do jednego produktu, jeśli stan produkt w źródle jest numeryczny i jego wartość: > 0 to jest to indeks głowny, 
      // (nie sprawdzamy, na którym indeksie jest najwiecej bo musielibyśmy w tabeli trzymać ilości produktów na danym indekse)
      if (isset($this->_bSupportMainSourceId) && $this->_bSupportMainSourceId === TRUE) {
        if ($this->_checkSourceMainIndex($aProduct)) {
          
          
          $aValues = array(
              'main_index' => '0'
          );
          if ($pDbMgr->Update('profit24', 'products_to_sources', $aValues, ' product_id = '.$iProductId.' AND source_id = '.$this->iSourceId.' AND source_index <> "'.$aProduct['_id'].'"') === false) {
            $this->addErrorLog("Błąd aktualizacji stanu produktu 1 - ".$iProductId);
            return false;
          }
          
          $aValues = array(
              'main_index' => '1'
          );
          if ($pDbMgr->Update('profit24', 'products_to_sources', $aValues, ' product_id = '.$iProductId.' AND source_id = '.$this->iSourceId.' AND source_index = "'.$aProduct['_id'].'"') === false) {
            $this->addErrorLog("Błąd aktualizacji stanu produktu 2 - ".$iProductId);
            return false;
          }
          
          // główny indeks, aktualizacja stanu magazynowego
          if (($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aStatusValues, "id = ".$iProductId)) === false) {
            $this->addErrorLog("Błąd aktualizacji stanu produktu 4 - ".$iProductId);
            return false;
          }
          // czy aktualne źródło produktu jest jego głównym ?
          $this->proceedProductFlags($sSrcSymPrefix, $iProductId, $aProduct['publication_year'], $bUpdate, $aProduct['prod_status'], $aProduct['_shipment_date'], $iNewsMainCategory, $iPreviewsMainCategory);
          return true;
        }
      } else {
        // normalna aktualizacja stanu magazynowego, jeśli nie wspiera sprawdzania głównego id w źródle
        if (($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aStatusValues, "id = ".$iProductId)) === false) {
          $this->addErrorLog("Błąd aktualizacji stanu produktu 5 - ".$iProductId);
          return false;
        }
        // czy aktualne źródło produktu jest jego głównym ?
        $this->proceedProductFlags($sSrcSymPrefix, $iProductId, $aProduct['publication_year'], $bUpdate, $aProduct['prod_status'], $aProduct['_shipment_date'], $iNewsMainCategory, $iPreviewsMainCategory);
        return true;
      }
      

		} else {
      $this->proceedProductFlags($sSrcSymPrefix, $iProductId, $aProduct['publication_year'], $bUpdate, $aProduct['prod_status'], $aProduct['_shipment_date'], $iNewsMainCategory, $iPreviewsMainCategory);
      
			// brak danych 
			return true;
		}
	}// end of proceedProductStock() method
  
  
  /**
   * Metoda sprawdza czy podane źródło jest głównym dla przekazanego produktu
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @param type $sSrcSymPrefix
   * @param type $iProductId
   * @return type
   */
  public function checkSourceIsMainSource($sSrcSymPrefix, $iProductId) {
    global $pDbMgr;
    
    $iSource = 0;
    if (isset($this->oSourceElements->iSourceId) && $this->oSourceElements->iSourceId > 0) {
      $iSource = $this->oSourceElements->iSourceId;
    } elseif (isset($this->iSourceId) && $this->iSourceId > 0) {
      $iSource = $this->iSourceId;
    } else {
      $iSource = 0;
    }
    
    if ($iSource > 0) {
      $sSql = "SELECT P.id
              FROM products AS P
              WHERE P.source = '".$iSource."' AND P.id = ".$iProductId;
    } else {
      $sSql = "SELECT P.id
              FROM products AS P
              JOIN sources AS S ON P.source = S.id
              WHERE P.id = ".$iProductId."
              AND S.symbol =  '".$sSrcSymPrefix."'";
    }
    
    return (intval($pDbMgr->GetOne('profit24', $sSql)) > 0 ? TRUE : FALSE);
  }// end of checkSourceIsMainSource() method
  
	
	/**
	 * Metoda odpublikowuje wiele produktów na raz
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param string $sSrcSymPrefix
	 * @param array $aIds
	 * @return bool
	 */
	public function unpublishProductSource($sSrcSymPrefix, $aIds) {
		global $aConfig, $pDbMgr;
		$iMaxCountInOneUpdate = 5;
		$bIsErr = false;
		
		$aStatusValues = array(
				$sSrcSymPrefix.'_status' => '0',
				$sSrcSymPrefix.'_last_import' => 'NOW()'
		);
		
		if (count($aIds) > $iMaxCountInOneUpdate) {
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aStatusValues, ' id IN('.implode(',', $aIds).')') === false) {
				$bIsErr = true;
			}
		} else {
			foreach ($aIds as $iPId) {
				if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aStatusValues, ' id = '.$iPId) === false) {
					$bIsErr = true;
					break;
				}
			}
		}
		
		return $bIsErr === false ? true : false;
	}// end of unpublishProductSource() method
	
	
	/**
	* Mapuje stawki vat ze starych na nowe wg zadanej tablicy
	* 
	* @param iVat - wejsciowa stakwa vat
	* @return zmapowana stakwa vat
	*/
	function vatMapper($iVat) {
		
		$aVatMap=array(
			7=>8,
			22=>23,
			'7'=>8,
			'22'=>23
		);
		if($aVatMap[intval(trim($iVat))]>0)
			return $aVatMap[$iVat];
		else 
			return $iVat;		
	}//end of vatMapper() function
	
	
	/**
	 * Metoda pobiera dane produktu
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iId
	 * @param array $aColsSelect
	 * @return array
	 */
	public function getProductArgs($iId, $aColsSelect = array('*')) {
		global $aConfig, $pDbMgr;

		$sSql = 'SELECT '.implode(',', $aColsSelect).' 
							FROM '.$aConfig['tabls']['prefix'].'products
							WHERE id = '.$iId;
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getProductArgs() method
	
	
	/**
	 * Metoda sprawdza czy produkt istnieje w bazie danych na podstawie: 
	 *	plain_isbn, isbn_10, isbn_13, ean_10
	 * Dodatkowo ustawia zmienne: 
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param array $aProduct - dane produktu
   * @param bool  $bSearchByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym
   * @param bool $bSearchOnlyByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym i wyszukujemy tylko po tym indeksie dostawcy
	 * @return integer 
	 */
	public function refCheckProductExists($aProduct, $bSearchByMainSourceIndex = FALSE, $bSearchOnlyByMainSourceIndex = FALSE) {

		$aDBProduct = $this->refMatchProductByISBNs($aProduct, array('id'), $bSearchByMainSourceIndex, $bSearchOnlyByMainSourceIndex);
		// dopisujemy jedynie do inserta potrzebne rzeczy
		if (!empty($aDBProduct) && is_array($aDBProduct) && intval($aDBProduct['id']) > 0) {
			return $aDBProduct['id'];
		} else {
			// nie znaleziono pasującego produktu
			return false;
		}
		return false;
	}// end of refCheckProductExists() method
	
	
	/**
	 * Metoda pobiera dane produktu na podstawie azymut bookindeksu
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param type $sAzymutBookindeks
	 * @param type $aColsSelect
	 * @return type
	 */
	private function getProductByAzymutBookindeks($sAzymutBookindeks, $aColsSelect = array('id')) {
		global $aConfig, $pDbMgr;
		
		$sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM '.$aConfig['tabls']['prefix'].'products
									 WHERE azymut_index = "'.$sAzymutBookindeks.'"';
		return $pDbMgr->GetRow('profit24', $sSqlSelect);
	}// end of getProductByAzymutBookindeks() method
	
	
  /**
   * Metoda pobiera dane produktu dopasowanego po indeksie dostawcy
   * 
   * @global object $pDbMgr
   * @param int $iSourceBId - id źródła
   * @param int $iProductIndexSource - id produktu w źródle
   * @param array $aColsSelect - wybierane kolumny
   * @param bool $bSearchOnlyByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym i wyszukujemy tylko po tym indeksie dostawcy
   * @return array
   */
  private function _getMatchProductBySourceIndex($iProductIndexSource, $iSourceBId, $aColsSelect, $bSearchOnlyByMainSourceIndex) {
		global $pDbMgr;
    
    // sprawdźmy czy produkt istnieje na podstawie tabeli products_to_sources
    $iProductId = $this->getProductIdBySourceId($iProductIndexSource, $iSourceBId, $bSearchOnlyByMainSourceIndex);
    if ($iProductId > 0) {
      $sSqlGetProducts = 'SELECT '.implode(',', $aColsSelect).'
               FROM products
               WHERE id = '.$iProductId;
      $aDBProduct = $pDbMgr->GetRow('profit24', $sSqlGetProducts);
     }
     return $aDBProduct;
  }// end of _getMatchProductBySourceIndex() method
  
  
	/**
	 * Metoda dopasowuje produkt po ISBNach, pobiera także zadane kolumny
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param array $aProduct - dane produktu
	 * @param array $aColsSelect - wybierane kolumny z tabeli products
   * @param bool  $bSearchByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym
   * @param bool $bSearchOnlyByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym i wyszukujemy tylko po tym indeksie dostawcy
	 * @return array
	 */
	public function refMatchProductByISBNs($aProduct, $aColsSelect = array('id'), $bSearchByMainSourceIndex = FALSE, $bSearchOnlyByMainSourceIndex = FALSE){
		global $pDbMgr;
		$aDBProduct = array();

		
		if (isset($aProduct['azymut_index']) && $aProduct['azymut_index'] != '') {
			// jeśli jest bookindeks, sprawdzmy czy cos znajdziemy
			$aDBProduct = $this->getProductByAzymutBookindeks($aProduct['azymut_index'], $aColsSelect);
		}

     if ($bSearchByMainSourceIndex === TRUE && empty($aDBProduct) && $this->iSourceId > 0 && $aProduct['_id'] != '') {
      $aDBProduct = $this->_getMatchProductBySourceIndex($aProduct['_id'], $this->iSourceId, $aColsSelect, $bSearchOnlyByMainSourceIndex);
      if ($bSearchOnlyByMainSourceIndex === TRUE) {
        // Zwracamy już tu, jesli sprawdzamy WYŁĄCZNIE główny index - super siodemka
        return $aDBProduct;
      }
    }
		
		$sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
										isbn_plain = "%1$s" OR 
										isbn_10 = "%1$s" OR 
										isbn_13 = "%1$s" OR 
										ean_13 = "%1$s" OR 
                    streamsoft_indeks = "%1$s" OR
                    standard_code = "%1$s"
                    ';
		
		if (empty($aDBProduct) && $aProduct['isbn_plain'] != '') {

			$sMatchedType = 'isbn_plain';
			$sSql = sprintf($sSqlSelect, $aProduct['isbn_plain']);
			$aDBProduct = $pDbMgr->GetRow('profit24', $sSql);
		} 
		if (empty($aDBProduct) && $aProduct['isbn_10'] != '') {

			$sMatchedType = 'isbn_10';
			$sSql = sprintf($sSqlSelect, $aProduct['isbn_10']);
			$aDBProduct = $pDbMgr->GetRow('profit24', $sSql);
		} 
		if (empty($aDBProduct) && $aProduct['isbn_13'] != '') {

			$sMatchedType = 'isbn_13';
			$sSql = sprintf($sSqlSelect, $aProduct['isbn_13']);
			$aDBProduct = $pDbMgr->GetRow('profit24', $sSql);
		} 
		if (empty($aDBProduct) && $aProduct['ean_13'] != '') {

			$sMatchedType = 'ean_13';
			$sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
			$aDBProduct = $pDbMgr->GetRow('profit24', $sSql);
			
		}

		/*
		 * NIE JEST TO POTRZEBNE
		if (isset($aProduct[$sMatchedType]) && $aProduct[$sMatchedType] != '') {
			$aProduct['_matched_type'] = $sMatchedType;
			$aProduct['_matched_product'] = $aDBProduct;
		}
		 */
		return $aDBProduct;
	}// end of refMatchProductByISBNs() method
	
	
	/**
	 * Metoda pobiera status produktu
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iPId
	 * @return array
	 */
	public function getProductStock($iPId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."products_stock
						 WHERE id = ".$iPId."
						 LIMIT 1";
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getProductStock() method
	
	
	/**
	 * Metoda sprawdza typ isbn'a i ustawia isbn_13 i isbn_10(jeśli nie zostały wcześniej uzupełnione), 
	 * jeśli ean_13 jest nie pusty to nie uzupełniamy isbn_13 pomimo zgodnego typu
	 * 
	 * @param array $aProduct
	 * @param bool $bInsert
	 * @return array
	 */
	public function refProceedIsbn(&$aProduct, $bInsert) {
		
		$sPossibleType = $this->checkISBNClean($aProduct['isbn_plain']);
		if ($sPossibleType == 'isbn_13' && 
				(empty($aProduct['ean_13']) || $aProduct['ean_13'] === 'NULL') && 
				(empty($aProduct['isbn_13'])) ||  $aProduct['isbn_13'] === 'NULL') {
			// jeśli nie jest to ean_13, a pasuje do typu isbn_13 to jest to ten typ
			$aProduct['isbn_13'] = $aProduct['isbn_plain'];
		} 
		elseif ($sPossibleType == 'isbn_10' && 
					  (empty($aProduct['isbn_10']) || $aProduct['isbn_10'] === 'NULL')) {
			$aProduct['isbn_10'] = $aProduct['isbn_plain'];
		}
		
		if ($bInsert === false) {
			// zawsze usuwamy jeśli update!! - nie jest potrzebny później
			unset($aProduct['isbn_plain']);
			unset($aProduct['isbn']);
		}
		return $aProduct;
	}// end of refProceedIsbn() method
	
	
	/**
	 * Metoda sprawdza czy produkt istnieje w bazie danych na podstawie ISBN
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param string $sPlainISBN
	 * @return integer 
	 */
	public function checkCategoryExists($aItem) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories 
						 WHERE source_id='".$aItem['source_id']."' AND 
									 source_category_id='".$aItem['source_category_id']."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of checkProductExists() method
	

	/**
	 * Metoda pobiera id produktu na podstawie id źródła oraz id produktu w źródłe
	 *
	 * @global array $aConfig
	 * @global type $pDbMgr
	 * @param int $iSourceBId - id produktu w źródle
	 * @param int $iSourceId - id źródła
   * @param bool $bSearchOnlyByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym
	 * @return integer - id produktu w profit24
	 */
	public function getProductIdBySourceId($iProductIndexSource, $iSourceId, $bSearchOnlyByMainSourceIndex = FALSE) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT product_id 
						 FROM ".$aConfig['tabls']['prefix']."products_to_sources
						 WHERE source_id=".$iSourceId." 
							 AND source_index='".$iProductIndexSource."'
               ".($bSearchOnlyByMainSourceIndex === TRUE ? ' AND main_index = "1" ' : '').
             ' ORDER BY main_index DESC';
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getProductIdBySourceId() method
	
	
	/**
	 * Metoda czyści zbędne zmienne produktu, czyli takie z prefixem "_"
	 *
	 * @param type $aProduct
	 * @return type 
	 */
	public function clearProductVars($aProduct) {
		
		foreach ($aProduct as $sKey => $mVal) {
			preg_match("/^(_.*)/", $sKey, $aMatches);
			if ($aMatches[1] != '') {
				unset($aProduct[$sKey]);
			}
		}
		return $aProduct;
	}// end of clearProductVars() method

	
	/**
	 * Metoda na podstawie kategorii ze źródła wybiera na podstawie mapowań kategorie profit24, 
	 *	jeśli mapowanie nie istnieje sprawdza czy podana kategoria istnieje, jeśli nie dodaje ją
	 *	do puli wszystkich kategorii źródła
	 *
	 * @param array $aCategories - kategorie źródła
	 * @return mixed - array - kategorie profit24 | false - błąd dodawania
	 */
	protected function proceedCategories($aCategories) {
		global $aConfig, $pDbMgr;
		
		$aProfitCategories = array();
		
		// w pierwszej kolejności wybieramy pasujące kategorie
		if (isset($aCategories) && !empty($aCategories) && is_array($aCategories)) {
			$aProfitCategories = $this->getSourceMappings($this->oSourceElements->sSoruceSymbol, array_keys($aCategories));
		}

		// jeśli brak kategorii to należy dodać do puli kategorii które są dostępne
		if (!empty($aCategories) && empty($aProfitCategories)) {
			
			// lecimy z tymi kategoriami
			foreach ($aCategories as $iCatId => $sCatName) {

				$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
								 WHERE source_category_id = '".$iCatId."' AND source_id = ".$this->oSourceElements->iSourceId;
				$iIdMC = $pDbMgr->GetOne('profit24', $sSql);
				if ($iIdMC <= 0) {
						$this->addErrorLog("Brak kategorii w tabeli kategorii źródła menus_items_mappings_categories ".$iCatId.":".$sCatName." ");
				}
				/*
				aktualnie wyłączyłem to dodawanie, wszystkie kategorie źródeł są osobno synchronizowane
				if ($iCatId > 0 && $sCatName != '' && $sCatName !== '1') {
					$aValues = array(array(
							'source_id' => $this->oSourceElements->iSourceId,
							'source_category_id' => $iCatId,
							'name' => $sCatName
					));

					if ($this->recordProductsTablesExists("menus_items_mappings_categories", 'source_category_id', $iCatId) === 0) {
						// kategoria jeszcze nie dodana do tej tabeli
						if ($this->InsertProdAttr("menus_items_mappings_categories", $aValues) === false) {
							$this->addErrorLog("Wystąpił błąd podczas dodawania nowej kategorii 
																	o id w źródle: ".$iCatId);
							return false;
			}
		}
				}
				*/
			}
		}
		
		return $aProfitCategories;
	}// end of proceedCategories() method
	
	
	/**
	 * Metoda sprawdza typ ISBN
	 * 
	 * @param type $sIsbn
	 * @return string
	 */
	public function checkISBNClean($sIsbn) {
		
		$sIsbnClean = preg_replace('/[^0-9a-zA-Z]/', '', $sIsbn);
		$iCount = mb_strlen($sIsbnClean, 'UTF-8');
		if ($iCount == 13) {
			return 'isbn_13';
		}
		elseif ($iCount == 10) {
			return 'isbn_10';
		} else {
			return 'isbn_plain';
		}
	}// end of checkISBNClean() method
	
	
	/**
	 * Usuwa pozycję z nowości
	 * 
	 * @param int $PId - id produktu
	 * @return bool success
	 */
	public function delNews($PId){
		global $aConfig;
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='0'
							WHERE id = ".$PId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($PId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news='0'
							WHERE id = ".$PId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
	 	
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_news
						 WHERE product_id = ".$PId;
 		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		return !$bIsError;
	} // end of addNews() function
	
	
	/**
	 * Usuwa pozycję z zapowiedzi
	 * 
	 * @param int $PId - id produktu
	 * @return bool success
	 */
	public function delPreviews($PId){
		global $aConfig;
		$bIsError = false;
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_previews='0', shipment_date = NULL
							WHERE id = ".$PId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($PId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_previews='0', shipment_date = NULL
							WHERE id = ".$PId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
	 	
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_previews
						 WHERE product_id = ".$PId;
 		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
    
		/*
		$this->setSourceSymbol();
		if (isset($this->oSourceElements->sSoruceSymbol) && $this->oSourceElements->sSoruceSymbol != '') {
			$aValues = array(
					$this->oSourceElements->sSoruceSymbol."_shipment_date" => 'NULL'
			);
			if (Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues, ' id='.$PId) === false) {
				$bIsError = true;
			}
		}
		*/	
		return !$bIsError;
	} // end of delPreviews() function
	
	
	/**
	 * Metoda na podstawie id kategorii glownej nowosci ustala id kategorii gównej zapowiedzi
	 * 
	 * @param type $iNewsMainCategory
	 * @return int|null
	 */
	public function getPreviewPageIdByNewsPageId($iNewsMainCategory) {
		if ($iNewsMainCategory > 0) {
			switch ($iNewsMainCategory) {
				case '423':
					return 2337;
				break;
				case '424':
					return 2339;
				break;
				case '425':
					return 2338;
				break;
			}
		}
		return null;
	}// end of getPreviewPageIdByNewsPageId() method
	
	
	/**
	 * Metoda ustawia flagi nowości oraz zapowiedzi produktu
   * XXX metoda powinna także sprawdzać aktualnie przetważany produkt 
   *  w aktualnym źródle jest jego głównym źródłem !
   *  jednak nie ma to zastosowania, ponieważ najczęściej w tym przypadku nie 
   *  ma zapisanej ceny produktu, a co za tym idzie raczej nie będzie 
   *  to główne źródło produktu
	 * 
	 * @param int $iId - id produktu
	 * @param int $iProductYear - rok wydania produktu
	 * @param bool $bUpdateProduct - czy jest to aktualizacja produktu
   * @param int $iNewMainProdStatus - id nowego głównego statusu produktu
	 * @param date $dShipmentDate - data premiery produktu w formiacie \d{4}-\d{2}-\d{2}
	 * @param int $iNewsMainCategory - głowna kategoria nowosci
	 * @param int $iPreviewsMainCategory - głowna kategoria zapowiedzi
	 */
	public function proceedProductFlags($sSrcSymPrefix, $iId, $iProductYear, $bUpdateProduct, $iNewMainProdStatus, $dShipmentDate = NULL, $iNewsMainCategory = NULL, $iPreviewsMainCategory = NULL) {
		//dump($iId, $iProductYear, $bUpdateProduct, $dShipmentDate, $iNewsMainCategory, $iPreviewsMainCategory);
    if ($this->checkSourceIsMainSource($sSrcSymPrefix, $iId) !== TRUE) {
      // nie jest to kategoria główna, uciekamy
      return true;
    }
    
		if ($iNewsMainCategory > 0 && $iPreviewsMainCategory <= 0) {
			$iPreviewsMainCategory = $this->getPreviewPageIdByNewsPageId($iNewsMainCategory);
		}
		
		if ($this->checkAddPreviews($dShipmentDate)) {
			$aPreviews = $this->getPreviews($iId);
			if (empty($aPreviews)) {
//				dump("Dodaje zapowiedz do ".$iId);
				if ($this->addPreviews($iId, $iPreviewsMainCategory) === false) {
					echo 'blad dodawania zapowiedzi';
					return false;
				}
				
				if ($bUpdateProduct == true) {
					// wywalmy z nowości
					$aNews = $this->getNews($iId);
					if (!empty($aNews)) {
						if ($this->delNews($iId) === false) {
							echo 'blad usuwania nowosci';
							return false;
						}
					}
				}
			}
		} elseif($iProductYear == date('Y') && $this->checkAddNews($dShipmentDate, $iNewMainProdStatus, $bUpdateProduct)) {
			$aPreviews = $this->getPreviews($iId);
			// nie ustawiamy nowości jeśli jest to aktualizacja lub jesli wcześniej była zapowiedź
			if ($bUpdateProduct == false || !empty($aPreviews)) {
				$aNews = $this->getNews($iId);
				if (empty($aNews) ) {
					// dump("Dodaje nowosc do ".$iId);
					if ($this->addNews($iId, $iNewsMainCategory) === false) {
						echo 'blad dodawania nowosci';
						return false;
					}

					if ($bUpdateProduct == true) {
						// wywalmy z zapowiedzi
						//$aPreviews = $this->getPreviews($iId);
						if (!empty($aPreviews)) {
							if ($this->delPreviews($iId) === false) {
								echo 'blad usuwania zapowiedzi';
								return false;
							}
						}
					}
				}
			}
		} else {
			// sprawdźmy czy zapowiedź powinna wciąż być zapowiedzią
			$aPreviews = $this->getPreviews($iId);
			if (!empty($aPreviews)) {
				// jest zapowiedzią
				if ($this->checkDelPreviews($iId, $dShipmentDate, $iNewMainProdStatus)) {
					// ok usuwamy zapowiedź
					if ($this->delPreviews($iId) === false) {
						echo 'blad usuwania zapowiedzi';
						return false;
					}
				}
			}
		}
		return true;
	}// end of proceedProductFlags() method
	
	
	
	/**
	* Dodaje załącznik do produktu
	* 
	* @param int $iId - id produktu
	* @param array $aAttachments - tablica załaczników
	* @return bool - success
	*/
	function addAttachment($iProductId, $aAttachment, $bOlesiejuk){
		global $aConfig;
		
		if(!empty($aAttachment)){
			if ($bOlesiejuk == true) {
				$iAttTypeId = '6';
			} elseif ($aAttachment['attachment_type'] == '6') {
				$iAttTypeId = '6';
			} else {
        // to jak narazie tylko dla olesiejuka, inni nie posiadaja info o załącznikach
				// $iAttTypeId = getAttachmentTypeId(convertAzymutPublType($aAttachment['publication_type']));
      }
			if($iAttTypeId > 0){
				$aValues = array(
					'price_brutto' => Common::FormatPrice2($aAttachment['price_brutto']),
					'price_netto' => Common::FormatPrice2($aAttachment['price_netto']),
					'vat' => $aAttachment['vat'],
					'attachment_type' => $iAttTypeId,
					'product_id' => $iProductId,
				);
        if (isset($aAttachment['siodemka_index'])) {
          $aValues['siodemka_index'] = $aAttachment['siodemka_index'];
        }
				if (($AId = Common::Insert($aConfig['tabls']['prefix']."products_attachments", $aValues)) === false) {
					$this->addErrorLog("błąd dodawania załącznika dla produktu ".$iProductId);
					return false;
				}
				else{
					$this->addLog("dodano załącznik ".$AId." - dla produktu ".$iProductId);
				}
				return true;
			}
		}
		else return false;
	} // end of addAttachment() function

	
	/**
	 * Metoda pobiera symbol źródła na podstawie id źródła
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iSId
	 * @return string
	 */
	public function getSourceSymbol($iSId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT symbol FROM ".$aConfig['tabls']['prefix']."sources WHERE id=".$iSId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceSymbol() method
	
	
	/**
	 * Dodaje wpisy o powiązanych do bazy
	 * 
	 * @param aLinks  powiązane
	 * @param iId  id produktu
	 * @return bool
	 */
	private function addLinking($aLinks, $iId) {
		global $aConfig;

		$aValues['product_id']=$iId;
		foreach($aLinks as $aId) {
			$aValues['linked_id'] = $aId['id'];
			if ($this->checkLinked($aValues['product_id'], $aValues['linked_id']) === false) {
				if (Common::Insert($aConfig['tabls']['prefix']."products_linked", $aValues) === false) {
					return false;
				}
			} else {
				// istnieje już to powiązanie dla tego produktu
			}
		} 

		return true;
	}// end of addLinking() function
	
	
	/**
	 * Metoda sprawdza czy istnieje już powiazanie o podanych danych
	 *
	 * @global type $aConfig
	 * @param integer $iPId - id produktu 
	 * @param integer $iLId - id produktu wiazanego
	 * @return bool
	 */
	private function checkLinked($iPId, $iLId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_linked
						 WHERE linked_id = ".$iLId." AND product_id = ".$iPId;
		return (intval(Common::GetOne($sSql)) > 0 ? true : false );
	}// end of checkLinked() function
	
	
	/**
	 * Metoda pobiera autorow produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @return	array	- autorzy produktu
	 */
	private function getAuthors($iId) {
		global $aConfig, $pDbMgr;
		
		// pobranie autorow ksiazki
		$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
					FROM " . $aConfig['tabls']['prefix'] . "products_to_authors A
					JOIN " . $aConfig['tabls']['prefix'] . "products_authors_roles B
					ON B.id = A.role_id
					JOIN " . $aConfig['tabls']['prefix'] . "products_authors C
					ON C.id = A.author_id
					WHERE A.product_id = " . $iId . "
					ORDER BY B.order_by";

		$aResult = & $pDbMgr->GetAll('profit24', $sSql);
		$sAuthors = '';

		foreach ($aResult as $aAuthor) {
			$sAuthors .= $aAuthor['author'] . ', ';
		}
		return substr($sAuthors, 0, -2);
	}// end of getAuthors() function
	
	
	/**
	 * Metoda ustawia zmienną symbolu źródła na podstawie id źródła
	 * 
	 * @return void
	 */
	public function setSourceSymbol() {
		
		// ustawianie zmiennych na wstępie
		if (!isset($this->oSourceElements->iSourceId) || !isset($this->oSourceElements->iSourceId)) {
      $this->oSourceElements = new stdClass();
			if ($this->iSourceId > 0) {
				$iSId = $this->iSourceId;
				$this->oSourceElements->iSourceId = $this->iSourceId;
			} 
			elseif ($this->oSourceElements->iSourceId > 0) {
				$iSId = $this->oSourceElements->iSourceId;
				$this->iSourceId = $this->oSourceElements->iSourceId;
			}
		}
		if (!isset($this->oSourceElements->sSoruceSymbol)) {
			$this->oSourceElements->sSoruceSymbol = $this->getSourceSymbol($iSId);
		}
	}// end of setSourceSymbol() method


	/**
	 * @param $aProduct
	 * @param $iId
	 * @param $aProfitCategories
	 * @return bool
	 */
	public function proceedCategoriesMerge($aProduct, $iId, $aProfitCategories) {
		global $pDbMgr, $aConfig;

		$this->setSourceSymbol();

		if (isset($aProduct['_items_category']) && !empty($aProduct['_items_category'])) {
			if (empty($aProfitCategories)) {
				$aProfitCategories = $this->proceedCategories($aProduct['_items_category']);
			}

			if (!empty($aProfitCategories)) {
				// mamy mapowanie kategorii to dodajemy kategorie dodatkowe produktu
				$this->addExtraCategories($iId, $aProfitCategories, false);


				$aProductVals = [
					'page_id' => $this->getPageId($aProfitCategories)
				];

				if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products", $aProductVals, ' id='.$iId) === false) {
					$this->onUpdateErr($iId, $aProduct);
					$this->addErrorLog("Wystąpił błąd podczas aktualizacji produktu ".$iId);
					return false;
				} else {
					$elasticListener = new ElasticListener();
					$elasticListener->onProductUpdate($iId);
					dump($iId);
				}
			}
		}
		return true;
	}


	/**
	 * Metoda aktualizuje dane produktu
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param array $aProduct
	 * @param int $iId
	 * @param int $iSourceBId
	 * @param array $aProfitCategories
	 * @param int $iNewsMainCategory
	 * @param int $iPreviewsMainCategory
	 * @return boolean
	 */
	public function proceedUpdateProduct($aProduct, $iId, $iSourceBId, $aProfitCategories = array(), $iNewsMainCategory = NULL, $iPreviewsMainCategory = NULL) {
		global $aConfig, $pDbMgr;
		$aProductWITHISBN = $aProduct;
    
    // dodanie do shadowa
    include_once('modules/m_oferta_produktowa/Module_Common.class.php');
    $pProductsCommon = new Module_Common();

		$this->setSourceSymbol();
		switch ($this->oSourceElements->iSourceId) {
			case '7': // dictum
			case '8': // siodemka
			case '9': // olesiejuk
				return $iId;
			break;
		}

		
		$aProduct = $this->refProceedIsbn($aProduct, false);
		if (!empty($aProfitCategories)) {
			$aProfitCategories = $aProfitCategories;
		} else {
			// XXX TODO w siodemka nie ma nazwy kategorii i tam dodawanie 
			// nowych kategorii moze odbywać się wyłącznie za pomocą skryptu synchronizacji kategorii
			$aProfitCategories = $this->proceedCategories($aProduct['_items_category']);
			if (empty($aProfitCategories)) {
				if (intval($aProduct['page_id']) > 0) {
					$aProfitCategories = array($aProduct['page_id']);
					// dump($aProfitCategories);
				} else {
					$this->addErrorLog("Brak kategorii produktu, id źródła: ".$iSourceBId." isbn: ".$aProduct['isbn_plain']);
					return false; // uciekamy brak kategorii
				}
			}
		}

		if (intval($aProduct['publication_year']) <= 0) {
			unset($aProduct['publication_year']); // brak roku wydania
		}

		if (!isset($aProduct['publisher_id'])) {
			$aProduct['publisher_id'] = $this->getPublisherId($aProduct['_publisher']);
		}
    if ($aProduct['_language_code'] != '') {
       $iProdLanguageId = $this->getLanguageIdByCode($aProduct['_language_code']);
       if ($iProdLanguageId > 0) {
        $aProduct['language'] = $iProdLanguageId;
       }
    }
		// $aProduct['language'] = '1' - jezyk polski
		if (!isset($aProduct['shipment_time']) || $aProduct['shipment_time'] == '') {
			// domyślnie 1 - 2-3 dni
			$aProduct['shipment_time'] = '1'; // - shipment time - nie ma na stanie - trzeba zamowic
		}
		if (isset($aProduct['short_description']) && $aProduct['short_description'] != '') {
			$aProduct['short_description'] = trimString(strip_tags(br2nl($aProduct['short_description'],' ')),200);
		}
		
		// wybieramy kategorię główną
		$aProduct['page_id'] = $this->getPageId($aProfitCategories);
		$aProduct['binding'] = $this->getBindingId($aProduct['_binding']); //  nadpisanie binging
		$aProduct['published'] = isset($aProduct['published']) ? $aProduct['published'] : '1';
		$aProduct['modified'] = 'NOW()';
		$aProduct['modified_by'] = $this->oSourceElements->sSoruceSymbol;
		$aProduct['source'] = $this->oSourceElements->iSourceId;
		$aProduct['last_import'] = 'NOW()';
		$aProduct['reviewed'] = '1';
		$aProduct['packet'] = '0';
		unset($aProduct['created']);
		unset($aProduct['created_by']);
		
		$aProductVals = $this->clearProductVars($aProduct);
    if ($this->checkUniqueRef($aProductVals, $iId) === true) {
		unset($aProductVals['prod_status']);

	  if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products", $aProductVals, ' id='.$iId) === false) {
        $this->onUpdateErr($iId, $aProduct);
        $this->addErrorLog("Wystąpił błąd podczas aktualizacji produktu ".$iId);
        return false;
      } else {
        // Zaktualizowano PRODUKT

        // dodaj bookindeks dla produktu
        //$this->addBookIndeks($iId, $iSourceBId, $this->oSourceElements->iSourceId);

        if(!empty($aProduct['_linked'])) {
          $this->addLinking($aProduct['_linked'], $iId);
        }

        // mamy mapowanie kategorii to dodajemy kategorie dodatkowe produktu
        $this->addExtraCategories($iId, $aProfitCategories, false);

        // stan produktu
        $this->proceedProductStock($this->oSourceElements->sSoruceSymbol, $iId, $aProduct, true, $iNewsMainCategory, $iPreviewsMainCategory);

        // rok do tabelki
        if (isset($aProduct['publication_year']) && is_numeric($aProduct['publication_year']) && $aProduct['publication_year'] > 0) {
          $this->addYearToDict($aProduct['publication_year']);
        }

        // okladka - związana ze źródłem
        if ($aProduct['_img'] != '' && method_exists($this, 'addImage') ) {// && $this->bTestMode === false // nie importuj jeśli tryb testowy
          $this->addImage($iId, $iSourceBId, $aProductWITHISBN);
          unset($aProductWITHISBN);
        }

        // autorzy
        if (isset($aProduct['_items_authors']) && $aProduct['_items_authors'] != '' && trim($this->getAuthors($iId)) == '') {
          $this->addAuthors($iId, $aProduct['_items_authors'], 1);
        }

        // tłumaczenia
        if (!empty($aProduct['_items_translations']) && $aProduct['_items_translations'] != '' && trim($this->getAuthors($iId)) == '') {
          $this->addAuthors($iId, $aProduct['_items_translations'], 7);
        }

        // seria wydawnicza
        $sOldSeries = $this->getSeries($iId);
        if (isset($aProduct['_items_series']) && !empty($aProduct['_items_series']) && 
                  is_array($aProduct['_items_series']) && empty($sOldSeries)) {
          foreach ($aProduct['_items_series'] as $sSeriesName) {
            if($aProduct['publisher_id'] > 0 && !empty($sSeriesName)) {
              $this->addSeries($iId, $sSeriesName, $aProduct['publisher_id']);
            }
          }
        }

        // przeniesione do update stock
        // $this->proceedProductFlags($iId, $aProduct['publication_year'], true, $aProduct['_shipment_date'], $iNewsMainCategory);

        // attachments
        if (!empty($aProduct['_attachments'])) {
          $aAtt = $this->getAttachment($iId);
          if (empty($aAtt)) {
            if ($this->addAttachment($iId, $aProduct['_attachments'], true) === false) {
              $this->addErrorLog('wystąpił błąd podczas dodawania załącznika do produktu '.$iId);
            }
          }
        }

        if ($aProduct['published'] == '1') {
          // Aktualizacja shadowa
          if ($pProductsCommon->updateShadow($iId) === false) {
            $this->addErrorLog("Wystąpił błąd podczas aktualizacji produktu w shadow, ".$aProduct['_id']);
            return false;
          }
          if ($pProductsCommon->updateShadowAuthors($iId) === false) {
            $this->addErrorLog("Wystąpił błąd podczas aktualizacji autorów produktu w shadow, ".$aProduct['_id']);
            return false;
          }
          if ($pProductsCommon->updateShadowTags($iId) === false) {
            $this->addErrorLog("Wystąpił błąd podczas aktualizacji tagów produktu w shadow, ".$aProduct['_id']);
            return false;
          }
        } elseif (isset($aProduct['published']) && $aProduct['published'] == '0') {
          $pProductsCommon->deleteShadow($aProduct['_id']);
          $pProductsCommon->unpublishProductTags($aProduct['_id']);
        }

		  $elasticListener = new ElasticListener();
		  $elasticListener->onProductUpdate($iId);

        return $iId;
      }
      return false;
    } else {
      return $iId;
    }
	}// end of proceedUpdateProduct() method
  
  /**
   * 
   * @param array $aProductData
   * @param int $iNotId
   * @return bool
   */
  public function checkUniqueRef(&$aProductData, $iNotId = 0) {
    
    if (isset($aProductData['isbn_plain']) && $aProductData['isbn_plain'] != '') {
      if ($this->checkIndeksUnique($aProductData['isbn_plain'], $iNotId) === false) {
        return false;
      }
    }
    
    if (isset($aProductData['streamsoft_indeks']) && $aProductData['streamsoft_indeks'] != '') {
      if ($this->checkIndeksUnique($aProductData['streamsoft_indeks'], $iNotId) === false) {
        return false;
      }
    }
    
    if (isset($aProductData['isbn_10']) && $aProductData['isbn_10'] != '') {
      if ($this->checkIndeksUnique($aProductData['isbn_10'], $iNotId) === false) {
        unset($aProductData['isbn_10']);
      }
    }
    
    if (isset($aProductData['isbn_13']) && $aProductData['isbn_13'] != '') {
      if ($this->checkIndeksUnique($aProductData['isbn_13'], $iNotId) === false) {
        unset($aProductData['isbn_13']);
      }
    }
    
    if (isset($aProductData['ean_13']) && $aProductData['ean_13'] != '') {
      if ($this->checkIndeksUnique($aProductData['ean_13'], $iNotId) === false) {
        unset($aProductData['ean_13']);
      }
    }
    return true;
  }

	/**
	 *
	 * @global DatabaseManager $pDbMgr
	 * @param string $sIndeks
	 * @param int $iNotId
	 * @return int
	 */
	private function checkIndeksUnique($sIndeks, $iNotId = 0)
	{
		global $pDbMgr;

		$sSqlSelect = 'SELECT id
               FROM products
               WHERE 
                (
                  isbn_plain = "%1$s" OR 
                  isbn_10 = "%1$s" OR 
                  isbn_13 = "%1$s" OR 
                  ean_13 = "%1$s" OR
                  streamsoft_indeks = "%1$s" 
                )
                ';
		$sSqlSelect = sprintf($sSqlSelect, $sIndeks);
		$aIdks = $pDbMgr->GetCol('profit24', $sSqlSelect);
		if ($iNotId > 0) {
			if (($key = array_search($iNotId, $aIdks)) !== false) {
				unset($aIdks[$key]);
			}
		}
		if (!empty($aIdks)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Metoda dokonuje dodania produktu do bazy danych wraz z atrybutami
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param array $aProduct
	 * @return boolean 
	 */
	public function proceedInsertProduct($aProduct, $iSourceBId, $aProfitCategories = array(), $iNewsMainCategory = NULL, $iPreviewsMainCategory = NULL, $mainIndex = NULL) {
		global $aConfig, $pDbMgr;
		
		// robimy porządek z ISBN'ami
		$aProduct = $this->refProceedIsbn($aProduct, true);
		
		if (!empty($aProfitCategories)) {
			$aProfitCategories = $aProfitCategories;
		} else {
			// XXX TODO w siodemka nie ma nazwy kategorii i tam dodawanie 
			// nowych kategorii moze odbywać się wyłącznie za pomocą skryptu synchronizacji kategorii
			$aProfitCategories = $this->proceedCategories($aProduct['_items_category']);
			if (empty($aProfitCategories)) {
				if (intval($aProduct['page_id']) > 0) {
					$aProfitCategories = array($aProduct['page_id']);
				} else {
					$this->addErrorLog("Brak kategorii produktu, id źródła: ".$iSourceBId." isbn: ".$aProduct['isbn_plain'].' kategorie w źródle: '.print_r($aProduct['_items_category'], true));
					/*
					$this->sendInfoMail('KATEGORIE', "Brak kategorii produktu, id źródła: ".$iSourceBId.
																					 " isbn: ".$aProduct['isbn_plain'].' kategorie w źródle: '.
																					 print_r($aProduct['_items_category'], true));
					 */
					return false; // uciekamy brak kategorii
				}
			}
		}
		
		// wybieramy kategorię główną
		$aProduct['page_id'] = $this->getPageId($aProfitCategories);
		$aProduct['binding'] = $this->getBindingId($aProduct['_binding']); //  nadpisanie okladki
		$aProduct['publisher_id'] = $this->getPublisherId($aProduct['_publisher']);
		// $aProduct['language'] = '1' - jezyk polski
    if ($aProduct['_language_code'] != '') {
      $iProdLanguageId = $this->getLanguageIdByCode($aProduct['_language_code']);
      if ($iProdLanguageId > 0) {
       $aProduct['language'] = $iProdLanguageId;
      }
    }
		if (!isset($aProduct['shipment_time']) || $aProduct['shipment_time'] == '') {
			// domyślnie 1 - 2-3 dni
			$aProduct['shipment_time'] = '1'; // - shipment time - nie ma na stanie - trzeba zamowic
		}
		$aProduct['published'] = isset($aProduct['published']) ? $aProduct['published'] : '1';
		$aProduct['created'] = 'NOW()';
		$aProduct['created_by'] = $this->oSourceElements->sSoruceSymbol;
		$aProduct['source'] = $this->oSourceElements->iSourceId;
		$aProduct['last_import'] = 'NOW()';
		$aProduct['reviewed'] = '1';
		$aProduct['packet'] = '0';
    
    if (!isset($aProduct['plain_name']) || empty($aProduct['plain_name'])) {
      $aProduct['name']['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($aProduct['name'], ' ')));
    }

		if (isset($aProduct['short_description']) && $aProduct['short_description'] != '') {
			$aProduct['short_description'] = trimString(strip_tags(br2nl($aProduct['short_description'],' ')),200);
		}
		
		$aProductVals = $this->clearProductVars($aProduct);
    if ($this->checkUniqueRef($aProductVals) === true) {
		unset($aProductVals['prod_status']);
		unset($aProductVals['shipment_time']);
		unset($aProductVals['shipment_date']);
      	$iId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products", $aProductVals);
    }
		if ($iId !== false) {

			// dodaj bookindeks dla produktu
			$this->addBookIndeks($iId, $iSourceBId, $this->oSourceElements->iSourceId, '', $mainIndex);
			
			//  mamy mapowanie kategorii to dodajemy kategorie dodatkowe produktu
			$this->addExtraCategories($iId, $aProfitCategories, true);

			// seria wydawnicza
			if (isset($aProduct['_items_series']) && !empty($aProduct['_items_series']) && is_array($aProduct['_items_series'])) {
				foreach ($aProduct['_items_series'] as $sSeriesName) {
					if($aProduct['publisher_id'] > 0 && !empty($sSeriesName)) {
						$aProduct['_items_series_id'] = $this->addSeries($iId, $sSeriesName, $aProduct['publisher_id']);
					}
				}
			}
			
			// stan produktu
			$this->proceedProductStock($this->oSourceElements->sSoruceSymbol, $iId, $aProduct, false, $iNewsMainCategory, $iPreviewsMainCategory);

			// rok do tabelki
			$this->addYearToDict($aProduct['publication_year']);

			// okladka - związana ze źródłem
			if ($aProduct['_img'] != '') { //  && $this->bTestMode === false nie importuj jeśli tryb testowy
				$this->oSourceElements->addImage($iId, $iSourceBId, $aProduct);
			}

			// autorzy
			if (isset($aProduct['_items_authors']) && $aProduct['_items_authors'] != '') {
				$this->addAuthors($iId, $aProduct['_items_authors'], 1);
			}
			
			// tłumaczenia
			if (!empty($aProduct['_items_translations']) && $aProduct['_items_translations'] != '') {
				$this->addAuthors($iId, $aProduct['_items_translations'], 7);
			}
			


			// przeniesienie do proceedProductStock
			// $this->proceedProductFlags($iId, $aProduct['publication_year'], false, $aProduct['_shipment_date']);

			
			// attachments
			if (!empty($aProduct['_attachments'])) {
				if ($this->addAttachment($iId, $aProduct['_attachments'], true) === false) {
					$this->addErrorLog('wystąpił błąd podczas dodawania załącznika do produktu '.$iId);
				}
			}
			

			// shadow add
			if ($this->oProductsCommon->existShadow($iId) == false) {
				if ($this->oProductsCommon->addShadow($iId) === false) {
					$this->addErrorLog('wystąpił błąd podczas dodawania produktu do shadowa, '.$iId);
				}
			}

			$elasticListener = new ElasticListener();
			$elasticListener->onProductUpdate($iId);

			return true;
		} else {
			
			$this->addErrorLog('wystąpił błąd podczas dodawania produktu , '.mysql_error()."\r\n\r\n".print_r($aProduct, true));
			return false;
		}
	}// end of proceedInsertProduct() method
	
	/**
	 * Metoda sprawdza czy można zaktualizować podany produkt
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param array $aProduct - tablica danych produktu
	 * @param mixed $mRetMD5 - zwraca funkcja addBookIndeks, jeśli 2 to znaczy że można zmieniać produkt
	 * @param int $iProductId
	 * @return boolean
	 */
	public function checkUpdateProduct($aProduct, $mRetMD5, $iProductId = 0) {
		global $aConfig, $pDbMgr;
		
		// stare warunki aktualizacji
		/*
		  || 
				($aProduct['published'] == '0' && 
					($aProduct['source'] == '0' || 
					 $aProduct['source'] == '1' ||
					 $aProduct['source'] == '5' || 
					 $aProduct['source'] == '6')
				 && intval($aProduct['publisher_id']) <= 0)
		 */
		 
		if ($this->iSourceId == '11') {
		  return false;
		}
		
		if (!isset($aProduct['modiefied_by_service']) && $iProductId > 0) {
			$sSql = "SELECT modiefied_by_service FROM ".$aConfig['tabls']['prefix']."products
							 WHERE id = ".$iProductId;
			$aProduct['modiefied_by_service'] = $pDbMgr->GetOne('profit24', $sSql);
		}
		// $mRetMD5 - oznacza zmieniony MD5 i można aktualizować
		if ($mRetMD5 === 2 && (!isset($aProduct['modiefied_by_service']) || $aProduct['modiefied_by_service'] == '0')) 
		{
//			dump($aProduct);
//			dump('Jedziemy z aktualizacją produktu');
			return true;
		}
//		dump($aProduct);
//		dump('Nie trzeba aktualizować produktu');
		return false;
	}// end of checkUpdateProduct() method
	
	
	/**
	 * Metoda sprawdza czy możda dodać produkt do nowości
	 * 
	 * @param string $dShipmentDate data premiery produktu
   * @param int $iNewMainProdStatus Status produktu - możemy dodawać do nowości wyłącznie produkty które są dostępne prod_status = '1'
   * @param int $bUpdateProduct czy produkt jest aktualnie dodawany, czy aktualizowany
	 * @return boolean
	 */
	public function checkAddNews($dShipmentDate, $iNewMainProdStatus, $bUpdateProduct) {
    if ($bUpdateProduct == TRUE && intval($iNewMainProdStatus) != 1) {
      // jeśli jest to aktualizacja i aktualny status produktu jest inny niż dostępny
      // to nie wrzucamy do nowości
      return false;
    }
		if (empty($dShipmentDate) || $dShipmentDate == '0000-00-00') return true; // domyslnie można dodawać do nowości
		
		$iDataDB = str_replace('-','', $dShipmentDate); // powinno byc szybsze
		if (intval($iDataDB) <= 0) return false;
		
		$iDataNow = date('Ymd');
		
		return ($iDataDB <= $iDataNow) ? true : false;
	}// end of checkAddNews() method
	
	
	/**
	 * Metoda sprawdza czy można usunąć zapowiedź
	 * 
   * @param int $iId id produktu
	 * @param string $dShipmentDate data premiery produktu
   * @param int $iMainNewProdStatus nowa głowny status produktu
	 * @return boolean
	 */
	public function checkDelPreviews($iId, $dShipmentDate, $iMainNewProdStatus) {
		global $aConfig, $pDbMgr;
    
    if ($iMainNewProdStatus == '1') {
      // jest na stanie
      // wywalamy z zapowiedzi
      return true;
    }
		
		if (empty($dShipmentDate) || $dShipmentDate == '0000-00-00') return false; // domyslnie nie można usuwać zapowiedzi
		
		$iDataZEW = str_replace('-','', $dShipmentDate); // powinno byc szybsze
		if (intval($iDataZEW) <= 0) return false;
		
		$iDataNow = date('Ymd');
		if ($iDataZEW <= $iDataNow) {
			// data jest wcześniejsza, sprawdźmy stan produktu
			$sSql = "SELECT prod_status, shipment_date 
							 FROM ".$aConfig['tabls']['prefix']."products 
							 WHERE id = ".$iId;
			$aProduct = $pDbMgr->GetRow('profit24', $sSql);
			$iDataDB = str_replace('-','', $aProduct['shipment_date']);
			if ($aProduct['prod_status'] != '3' && $iDataDB > 0 && $iDataDB <= $iDataNow) {
				// czyli status wewnętrzny oficjalny produktu jest różny od 3 to znaczy że produkt nie jest zapowiedzią
				// i data w bazie danych jest
				return true;
			}
		}
		return false;
	}// end of checkDelPreviews() method
	
	
	/**
	 * Metoda sprawdza czy możda dodać produkt do nowości
	 * 
	 * @param string $dShipmentDate data premiery produktu, format daty RRRR-MM-DD
	 * @return boolean
	 */
	public function checkAddPreviews($dShipmentDate) {
		if (empty($dShipmentDate) || $dShipmentDate == '0000-00-00') return false; // domyslnie można dodawać do nowości
		
		$iDataXML = str_replace('-','', $dShipmentDate); // powinno byc szybsze
		if (intval($iDataXML) <= 0) return false;
		
		$iDataNow = date('Ymd');
		
		return ($iDataXML >= $iDataNow) ? true : false;
	}// end of checkAddPreviews() method
	
	
	/**
	 * Aktualizujemy ean_13
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iId
	 * @param string $sNewEan
	 * @return boolean
	 */
	protected function updateEAN($iId, $sNewEan) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT ean_13 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id=".$iId;
		$sEanNow = $pDbMgr->GetOne('profit24', $sSql);
		if ($sEanNow != $sNewEan) {
			$aValues = array('ean_13' => $sNewEan);
			return $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products", $aValues, " id=".$iId);
		}
		return true;
	}// end of updateEAN() method
	

	/**
	 * Metoda wysyła maila o wystąpieniu błędu
	 * 
	 * @param type $PId
	 * @param type $aValues
	 */
	function onUpdateErr($PId, $aValues) {
		
		if (mysql_errno() == '1062') {
			$sInfoLog = mysql_error();
			$sInfoLog .= "\r\n id produtu : ".$PId."\r\n";
			$sInfoLog .= print_r($aValues, true);
			$this->sendInfoMail("Import - DUPLIKAT", $sInfoLog);
		}
	}// end of getUpdateErr() method
  
  
  /**
   * Funkcja pobiera id serii wydawniczej
   * 
   * @global array $aConfig
   * @param int $iProductId
   * @return int
   */
  private function getSeriesProductMapped($iProductId) {
    global $aConfig;

    $sSql = "SELECT series_id FROM ".$aConfig['tabls']['prefix']."products_to_series 
             WHERE product_id = ".$iProductId;
    return Common::GetOne($sSql);
  }// end of getSeriesProductMapped() method
  
  
  /**
   * Metoda na podstawie id produktu, id kategorii głównej produktu, 
   *  wybiera kategorię główną zapowiedzi
   * 
   * @param string $sSourceSymbol - symbol źródła
   * @param int $iId
   * @param int $iPublisherId
   * @return int
   */
  public function getPreviewMainCategory($sSourceSymbol, $iId, $iPublisherId) {
    
    $iPreviewsMainCategory = $this->getDefaultMainCategoryBySource('previews', $sSourceSymbol);
    
    // jeśli nie ustawiono kategorii głownych
    if ($iPublisherId > 0) {
      $aPublisherCats = $this->getPublisherCategories($iPublisherId);
    }
    if (!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0) {
      // nadpisanie id głównej kategorii nowości
      $iNewsMainCategory = $aPublisherCats['news_category'];
      // wybranie id zapowiedzi
      $iPreviewsMainCategory = $this->getPreviewPageIdByNewsPageId($iNewsMainCategory);
    }

    $iTmpSeriesId = $this->getSeriesProductMapped($iId);//getSeriesId($iId);
    if (intval($iTmpSeriesId) > 0) {
      $aSeriesCategoryTMP = $this->getSeriesCategories($iTmpSeriesId);
      if (intval($aSeriesCategoryTMP['news_category']) > 0) {
        // zmieniamy kategorię główną nowości
        $iNewsMainCategory = $aSeriesCategoryTMP['news_category'];
        $iPreviewsMainCategory = $this->getPreviewPageIdByNewsPageId($iNewsMainCategory);
      }
    }
    return $iPreviewsMainCategory;
  }// end of getPreviewMainCategory() method
  
  /**
   * Metoda na podstawie id produktu, id kategorii głównej produktu, 
   *  wybiera kategorię główną nowości
   * 
   * @param string $sSourceSymbol - symbol źródła
   * @param int $iId
   * @param int $iPublisherId
   * @return int
   */
  public function getNewsMainCategory($sSourceSymbol, $iId, $iPublisherId) {
    
    $iNewsMainCategory = $this->getDefaultMainCategoryBySource('news', $sSourceSymbol);
    
    // jeśli nie ustawiono kategorii głownych
    if ($iPublisherId > 0) {
      $aPublisherCats = $this->getPublisherCategories($iPublisherId);
    }
    if (!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0) {
      // nadpisanie id głównej kategorii nowości
      $iNewsMainCategory = $aPublisherCats['news_category'];
    }

    $iTmpSeriesId = getSeriesProductMapped($iId);
    if (intval($iTmpSeriesId) > 0) {
      $aSeriesCategoryTMP = $this->getSeriesCategories($iTmpSeriesId);
      if (intval($aSeriesCategoryTMP['news_category']) > 0) {
        // zmieniamy kategorię główną nowości
        $iNewsMainCategory = $aSeriesCategoryTMP['news_category'];
      }
    }
    
    return $iNewsMainCategory;
  }// end of getNewsMainCategory() method
  
  /**
   * Metoda na podstawie id głównej kategorii produktu przyporządkowuje id kategorii zapowiedzi
   * 
   * @param type $iPageId
   * @return int
   */
  public function getPreviewPageIdByProductPageId($iPageId) {
    switch ($iPageId) {
      default:
      case '1':
        return 2337;
      break;
      case '147':
        return 2339;
      break;
      case '261':
        return 2338;
      break;
    }
  }// end of getPreviewPageIdByProductPageId() method
  
  
  /**
   * Metoda na podstawie id głównej kategorii produktu przyporządkowuje id kategorii zapowiedzi
   * 
   * @param type $iPageId
   * @return int
   */
  public function getBestsellerPageIdByProductPageId($iPageId) {
    switch ($iPageId) {
      default:
      case '1':// ogolny
        return 500;
      break;
      case '147':// jezykowy
        return 499;
      break;
      case '261': // naukowy
        return 498;
      break;
    }
  }// end of getBestsellerPageIdByProductPageId() method
  
  
  /**
   * Metoda na podstawie id głównej kategorii produktu przyporządkowuje id kategorii zapowiedzi
   * 
   * @param type $iPageId
   * @return int
   */
  public function getNewsPageIdByProductPageId($iPageId) {
    switch ($iPageId) {
      default:
      case '1':// ogolny
        return 423;
      break;
      case '147':// jezykowy
        return 424;
      break;
      case '261': // naukowy
        return 425;
      break;
    }
  }// end of getBestsellerPageIdByProductPageId() method
}// end of CommonSynchro() Class
?>
