<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-10-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */

use LIB\EntityManager\Entites\OrdersMailsSended;

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');
global $pDbMgr;

$ordersMailsSended = new OrdersMailsSended($pDbMgr);

/**
 * Metoda pobiera dni wolne od pracy
 *
 * @return array
 */
function getFreeDays(){
  $sSql = "SELECT UNIX_TIMESTAMP(free_day)
							 FROM ".$aConfig['tabls']['prefix']."orders_free_days";
  return Common::GetCol($sSql);
}

function getYesturdayDateMySQL() {
  $iDays = -1;
  $iDate = time();
  $aDate = getdate($iDate);
  $aFreeDays = getFreeDays();

  if($aDate['wday'] == 0 || $aDate['wday'] == 6 || in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
    --$iDays;
  }
  // dopóki nie osiągniemy potrzebnej ilości dni
  while($iDaysCount < $iDays){
    // dodaj dzień
    $iDate -= (24 * 60 * 60);
    $aDate = getdate($iDate);
    // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
    if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
      $iDaysCount++;
    }
  }
  return date("Y-m-d", $iDate);
}

/**
 * @XXX @TODO tutaj jakieś prace były w trakcie przerwane, ciężko teraz powiedzieć jaki był tu cel, ale może wrócić ten temat więc wyjątkowo zostawiam
 *
 *
 * Metoda pobiera dni wolne od pracy
 *
 * @return array
function getFreeDays(){
  $sSql = "SELECT UNIX_TIMESTAMP(free_day)
							 FROM ".$aConfig['tabls']['prefix']."orders_free_days";
  return Common::GetCol($sSql);
}

function getYesturdayDateMySQL() {
  $iDays = -1;
  $iDate = time();
  $aDate = getdate($iDate);
  $aFreeDays = getFreeDays();

  if($aDate['wday'] == 0 || $aDate['wday'] == 6 || in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
    --$iDays;
  }
  // dopóki nie osiągniemy potrzebnej ilości dni
  while($iDaysCount < $iDays){
    // dodaj dzień
    $iDate -= (24 * 60 * 60);
    $aDate = getdate($iDate);
    // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
    if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
      $iDaysCount++;
    }
  }
  return date("Y-m-d", $iDate);
}
*/

/**
 * Metoda pobiera symbol serwisu na podstawie id serwisu
 *
 * @global type $aConfig
 * @param type $iWebsiteId
 * @return type 
 */
function getWebsiteSymbol($iWebsiteId) {
  global $aConfig;

  $sSql = "SELECT code FROM " . $aConfig['tabls']['prefix'] . "websites
           WHERE id=" . $iWebsiteId;
  return Common::GetOne($sSql);
}
// end of getWebsiteSymbol() method

$sMYSQLDate = getYesturdayDateMySQL();

global $pDbMgr;

$sSql = '
SELECT O.order_number, O.website_id, O.email, O.id
FROM orders AS O
WHERE 
IF (O.send_date != "0000-00-00", DATE(O.send_date), DATE(O.shipment_date_expected)) = "'.$sMYSQLDate.'"
AND
(
  (
    (
      O.payment_type = "bank_transfer" OR
      O.payment_type = "platnoscipl" OR
      O.payment_type = "card"
    )
    AND O.to_pay <= O.paid_amount
  )
  OR
  (
    O.payment_type = "postal_fee" OR
    O.payment_type = "bank_14days"
  )
  OR
  (
    O.second_payment_type = "postal_fee" OR
    O.second_payment_type = "bank_14days"
  )
)
AND 
(
  O.order_status = "1" OR
  O.order_status = "2" 
)
AND dont_send_info_not_send_on_time = "0"
';
$aData = $pDbMgr->GetAll('profit24', $sSql);

foreach ($aData as $aItem) {

  $exists = $ordersMailsSended->checkExists($aItem['id'], OrdersMailsSended::TYPE_NOT_SEND_ON_TIME);
  if (intval($exists) <= 0) {
    $ordersMailsSended->add($aItem['id'], OrdersMailsSended::TYPE_NOT_SEND_ON_TIME);

    $sWebsite = getWebsiteSymbol($aItem['website_id']);
    setWebsiteSettings(1, $sWebsite);
    $_GET['lang_id'] = 1;
    $aSiteSettings = getSiteSettings($sWebsite);
    $sFrom = $aConfig['default']['website_name'];
    $sFromMail = $aSiteSettings['orders_email'];

    $aMail = Common::getWebsiteMail('mail_not_send_on_time', $sWebsite);
    Common::sendWebsiteMail($aMail, $sFrom, $sFromMail, $aItem['email'], $aMail['content'], $aVars, $aValues, array(), array(), $sWebsite);
  }
}
//return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite);