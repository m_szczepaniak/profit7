<?php
/**
 * Klasa synchronizacji statusów produktów ze źródła Dictum do Profit24.pl
 * 
 * @todo dodać automatyczne czyszczenie starych pozycji, dawno nie synchronizowanych
 *  UPDATE products_stock SET dictum_status='0', dictum_stock = NULL WHERE dictum_status = '1' AND dictum_last_import < '2013-10-22';
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');

header('Content-Type: text/html; charset=UTF-8');
class dictumStatusSynchro extends CommonSynchro {
	private $iLimitIteration;
	private $bTestMode;
	public $aLog = array();
	public $aErrorLog = array();
	public $iSourceId = 0;

	
	public function __construct($bTestMode, $iLimitIteration = 0) {
		$this->iLimitIteration = $iLimitIteration;
		$this->bTestMode = $bTestMode;
		$this->iSourceId = $this->getSourceId('dictum');
	}
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sLogin = '0627';
		$sPassword = '0627_3559';
		$sQuery = "http://www.dictum.pl/books/stany.xml?login=".$sLogin."&password=".$sPassword;
		$sFileName = date("siH_dmY").'_status.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/dictum/".$sFileName;
		// otwarcie pliku
		$rFp = fopen($sFilePath, "w");
		if ($rFp) {
			$oCURL = curl_init();
      //curl_setopt($oCURL, CURLOPT_INTERFACE, "188.165.19.148");
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFp);

			curl_exec($oCURL);

			curl_close($oCURL);
			fclose ($rFp);
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda parsuje książki po jednej
	 *
	 * @param type $oXml
	 * @return type 
	 */
	private function parseItem(&$oXml) {
		global $pDbMgr, $aConfig;
		
		$aValues = array();
		$iIteration = 0;
		$iSourceBId = 0;
    
    /*
     * v 1.0
     * Uwaga modyfikacja wprowadzona - Dictum nawet w przypadku kiedy wystawia stan 1 - to i tak nie posiada na stanie - Uzgodnione Ł. Głuchowski, M. Chudy 
     * 
     * v 1.1
     * @date 25.10.2013 r.
     * Jeśli w dictum jest więcej niż 5 egz. to dana pozycja tylko wtedy pownna być dostępna - Uzgodnione K. Małż, M. Chudy
     */
    $iMinQuantity = 5;
		
		// czyścimy na wstępnie dane ksiażki
		while(@$oXml->read()) {
			switch ($oXml->nodeType) {
				case XMLReader::ELEMENT:
					if ($oXml->name == 'book') {
						$aProduct = array();
						
						$iSourceBId = $oXml->getAttribute('id'); // id książki w źródle
						/*
						 * TODO w następnych iteracjach zaimplementować to działanie
						 * 
						  stock = stan magazynowy książki gdzie:
								10 oznacza, że książka jest dostępna w liczbie 10 egz. lub więcej;
								1-9 oznacza faktyczny stan danej książki;
								0 oznacza brak książki na magazynie.
						 */
						$iStock = $oXml->getAttribute('stock'); // ilość produktow w źródle
						$aProduct['shipment_time'] = '1';
						$iIteration++; // licznik iteracji
						if ($this->bTestMode == true && $this->iLimitIteration <= $iIteration) {
							return;
						}


						//addToAzymutBuffer($aValues);
						$iPId = $this->getProductIdBySourceId($iSourceBId, $this->iSourceId);
						if ($iPId > 0) {
							// Produkt istnieje
							// XXX TODO update - następna iteracja
							echo ' UPDEJT - '.$iPId;
							$aProductDB = $this->getProductArgs($iPId, array('prod_status', 'shipment_date', 'publication_year'));
							if ($aProductDB['prod_status'] != '3') {
								if (intval($iStock) > $iMinQuantity) {
									$aProduct['prod_status'] = '1';
								} else {
									$aProduct['prod_status'] = '0';
								}
							} else {
								// pozostaje prod_status - 3
								$aProduct['prod_status'] = '3';
							}
							// jeśli data wydania z DB jest nie pusta to status produktu może być 3
							// w przeciwnym wypadku coś jest nie tak i zmieniamy prod_status na 0
							if (empty($aProductDB['shipment_date']) && $aProduct['prod_status'] == '3' ) {
								$aProduct['prod_status'] = '0';
								$aNewProdStatus = array('prod_status' => '0');// trigger nie zmienia statusu z zapowiedzi na niedostepny
								if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix'].'products', $aNewProdStatus, ' source = "'.$this->iSourceId.'" id = '.$iPId) === false) {
									// Produkt nie istnieje, nic nie rób tylko loguj
									$this->addErrorLog("Brak produktu o id źródla: ".$iSourceBId.' id produktu: '.$iPId);
								}
							}
              
              $aProduct['publication_year'] = $aProductDB['publication_year'];
							$aProduct['_stock'] = $iStock;
              
              if ($aProductDB['prod_status'] != '3') {
								if (intval($iStock) > $iMinQuantity) {
									$aProduct['prod_status'] = '1';
								} else {
									$aProduct['prod_status'] = '0';
								}
							} else {
								// pozostaje prod_status - 3
								$aProduct['prod_status'] = $aProductDB['prod_status'];
							}
              
              // uwaga jeśli produkt jest na stanie to w stock powinno zmienić na dostępny w okreslonej ilości
              if (intval($iStock) > $iMinQuantity) {
                $aProduct['prod_status'] = '1';
              }
							$this->proceedProductStock('dictum', $iPId, $aProduct, true);
						} else {
							// Produkt nie istnieje, nic nie rób tylko loguj
							$this->addLog("Brak produktu o id źródla: ".$iSourceBId);
						}
					}
				break;
			}
		}
	}// end of parseItem() method
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda parsuje plik XML
	 *
	 * @param string $sXMLFile
	 * @return boolean 
	 */
	public function parseXMLFile($sXMLFile) {
		
		if (file_exists($sXMLFile)) {
			$oXml = new XMLReader();
			$oXml->open($sXMLFile, 'UTF-8');
			
			while(@$oXml->read()) {
				switch ($oXml->nodeType) {
					case XMLReader::END_ELEMENT :
						switch ($oXml->name) {
							case 'books':
								// KONIEC XMLa
								$oXml->close();
							break;
						}
					break;

					case XMLReader::ELEMENT :
						switch ($oXml->name) {
							case 'books':
								// lecimy po książkach
								$this->parseItem($oXml);
							break;
						}
					break;
				}
			}
		} else {
			$this->addErrorLog("Brak pliku XML '".$sXMLFile."'");
			// brak XML'a
			return false;
		}
	}// end of parseXMLFile() method
  
  
  /**
   * Metoda pobiera ilość ostatnio zmienonych pozycji, 
   * użyte do odpublikowania starych, nie synchronizowanych pozycji
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @return int
   */
  private function _getCountLastUpdatedItems () {
		global $pDbMgr;
    
    $sSql = "SELECT COUNT(id) "
            . "FROM products_stock "
            . "WHERE dictum_last_import >= DATE_SUB( CURDATE( ) , INTERVAL 1 DAY ) "
            . "LIMIT 30";
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of _getCountLastUpdatedItems() method
  
  /**
   * Metoda odpublikowuje produkty, które nie były synchronizowane wczoraj i dziś
   * 
   * @global object $pDbMgr
   * @return bool
   */
  private function _unpublishProducts() {
    global $pDbMgr;
    
    $aValues = array(
        'dictum_status' => '0',
        'dictum_stock' => 'NULL'
    );
    $sWhere = ' dictum_last_import < DATE_SUB( CURDATE( ) , INTERVAL 1 DAY ) AND '
            . ' dictum_status <> "0" ' ;// stock nie jest tak ważny
    return $pDbMgr->Update('profit24', 'products_stock', $aValues, $sWhere);
  }// end of _unpublishProducts() method
  
  
  /**
   * Metoda odpublikowuje produkty, które nie były synchronizowane wczoraj i dziś,
   * jeśli przez te dni zostało zmienione więcej niż 30 produktów
   */
  public function unpublishOutOfDateProducts() {
    
    // sprawdźmy najpierw czy dziś i wczoraj, zmieniono więcej niż 30 pozycji
    if (intval($this->_getCountLastUpdatedItems()) >= 30) {
      if ($this->_unpublishProducts() === FALSE) {
        echo 'ERR';
        $this->addErrorLog("Wystąpił błąd podczas odpublikowywania starych pozycji");
        return false;
      } else {
        echo 'OKI';
      }
    } else {
      echo 'ERR2';
    }
  }// end of unpublishOutOfDateProducts() method
}// end of dictumStatusSynchro() Class




$bTestMode = false; //  czy test mode
$iLimitIteration = 500000000000;
$oDictumImport = new dictumStatusSynchro($bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oDictumImport->importSaveXMLData();
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'XML/dictum/012502_25102013_status.XML';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oDictumImport->parseXMLFile($sFilename);
	/*
	dump($oDictumImport->aErrorLog);
	dump($oDictumImport->aLog);
	 */
  $oDictumImport->unpublishOutOfDateProducts();
  if ($bTestMode == false) {
    !unlink($sFilename);
  }
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
	
}

$oDictumImport->sendInfoMail("IMPORT DICTUM-STATUS OK", print_r($oDictumImport->aLog, true));
$oDictumImport->sendInfoMail("IMPORT DICTUM-STATUS ERR", print_r($oDictumImport->aErrorLog, true));

// TODO mail z informacją o imporcie
echo 'koniec';