<?php
/**
 * Skrypt importu katalogów zdjęć ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');


include_once($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');	

class olesiejukImportImageCatalog extends CommonSynchro {
	private $iLimitIteration = 0;
					
	public function __construct($iLimitIteration) {
		$this->iLimitIteration = $iLimitIteration;
	}
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		
		$sLogin = 'ksiegarniaprofit';
		$sPassword = '08profit2012ks';
		$sQuery = "ftp://".$sLogin.":".$sPassword."@195.42.113.31/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	
	/**
	 * Metoda parsuje plik XML
	 *
	 * @param string $sCSVFile
	 * @return boolean 
	 */
	public function parseCSVFile($sCSVFile) {
		$iIteration = 0;
		$sDir = '';
		
		if (file_exists($sCSVFile)) {
			$handle = fopen($sCSVFile, "rb");
			if ($handle) {
					while (($sLine = fgets($handle, 4096)) !== false) {
						if ($iIteration == 0) {
							$this->clearTable();
						}
						// cała linia
						$this->parseLine($sLine, $sDir);

						// $this->bTestMode == true && 
						if ($this->iLimitIteration <= $iIteration) {
							return;
						}
						$iIteration++;
					}
					if (!feof($handle)) {
							echo "Błąd: niespodziewany błąd fgets()\n";
							$this->addErrorLog("Błąd: niespodziewany błąd fgets() w pliku - '".$sCSVFile."'");
							return false;
					}
					fclose($handle);
			}
		} else {
			$this->addErrorLog("Brak pliku XML '".$sCSVFile."'");
			// brak XML'a
			return false;
		}
	}// end of parseCSVFile() method
	
	
	/**
	 * Metoda czysci na wstępie tabelę ze zdjęciami od Olesiejuka
	 * 
	 * @global array $aConfig
	 * @global type $pDbMgr
	 * @return type
	 */
	private function clearTable() {
		global $aConfig, $pDbMgr;
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."_olesiejuk_images WHERE 1=1";
		return $pDbMgr->Query('profit24', $sSql);
	}// end of clearTable() method
	
	
	/**
	 * Metoda przetważa linię danych CSV ew. dodaje nowy rekord
	 * 
	 * @global array $aConfig
	 * @global type $pDbMgr
	 * @param type $sLine
	 * @param type $sDir
	 */
	public function parseLine($sLine, &$sDir) {
		global $aConfig, $pDbMgr;
		$aMatches = array();
		$sLine = trim($sLine);
		$sFilename = '';
		
		// jeśli jest to linia z nazwą katalogu to 
		preg_match("|^\./(\d+):|", $sLine, $aMatches);
		
		if ($aMatches[1] != '') {
			$sDir = $aMatches[1];
		} else {
			preg_match("/(.*\.jpg)$/", $sLine, $aMatches);
			$sFilename = $aMatches[1];
		}
		if ($sDir != '' && $sFilename != '') {
			$aValues = array(
					'dir' => $sDir,
					'filename' => $sFilename
			);
			return $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."_olesiejuk_images", $aValues, '', false);
		}
	}// end of parseLine() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveCSVData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_catalog_list.csv';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/olesiejuk/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'OKLADKI/lista.txt');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
}// end of Class


// import katalogów okładek
$iLimitIteration = 100000000;
$bTestMode = false;
$oImageCatalog = new olesiejukImportImageCatalog($iLimitIteration);

if ($bTestMode == false) {
	$sFilename = $oImageCatalog->importSaveCSVData();
} else {
	$sFilename = 'CSV/olesiejuk/015706_30082012_catalog_list.csv';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImageCatalog->parseCSVFile($sFilename);
	$oImageCatalog->sendInfoMail("IMPORT OLESIEJUK IMPORT KATALOGOW ZDJEC - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImageCatalog->sendInfoMail("IMPORT OLESIEJUK IMPORT KATALOGOW ZDJEC ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
}
?>
