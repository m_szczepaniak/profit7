<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module_Common.class.php');
$oCommon = new Module_Common();

include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/communicator/sources/Merlin/OrderStatusConsumer.php');

global $pDbMgr;

$orderStatusConsumer = new communicator\sources\Merlin\OrderStatusConsumer($pDbMgr, $argv);

$orderStatusConsumer->startConsuming();