<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-08-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */
/**
 * Description of fixReservations
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');


$sSql = 'SELECT id AS source_id, provider_id, column_prefix_products_stock
FROM sources';
$aSources = $pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC);
dump($aSources);

$sSql = 'SELECT PSR.products_stock_id, SUM( PSR.quantity ) as reservation_quantity, PSR.source_id, PS.profit_g_reservations, PS.profit_j_reservations, PS.profit_e_reservations, P.name
FROM products_stock_reservations AS PSR
JOIN products_stock AS PS 
  ON PS.id = PSR.products_stock_id
JOIN products AS P 
  ON P.id = PS.id
GROUP BY PSR.products_stock_id, PSR.source_id';
$aReservations = $pDbMgr->GetAll('profit24', $sSql);

if (!empty($aReservations)) {
  $sChangedStr = '';
  $sChangedStr .= implode(';', array_keys($aReservations[0])) ."\n";
  foreach ($aReservations as $aReservation) {
    $iSourceId = $aReservation['source_id'];
    $aSource = $aSources[$iSourceId];
    $sColumnReservation = $aSources[$iSourceId]['column_prefix_products_stock'].'_reservations';
    if (intval($aReservation[$sColumnReservation]) != ($aReservation['reservation_quantity'])) {
      $aReservation['reservation_column'] = $sColumnReservation;
      $sChangedStr .= implode(';', $aReservation)."\n";
      $aValues = array(
          $sColumnReservation => $aReservation['reservation_quantity']
      );
      $pDbMgr->Update('profit24', 'products_stock', $aValues, ' id = '.$aReservation['products_stock_id'].' LIMIT 1');
      dump($aValues);
    }
  }
  file_put_contents(__DIR__.'/fixReservation.csv', $sChangedStr);
} else {
  echo 'Brak błędnych rezerwacji';
}

$sSql = 'UPDATE products_stock 
SET profit_g_reservations = 0
WHERE id IN (
  SELECT id FROM (
    SELECT products_stock.id FROM products_stock AS products_stock
    LEFT JOIN  products_stock_reservations AS PSR
    ON products_stock.id = PSR.products_stock_id
    WHERE PSR.id IS NULL 
    AND products_stock.profit_g_reservations > 0
  ) AS TMP
)';
$pDbMgr->Query('profit24', $sSql);

$sSql = 'UPDATE products_stock 
SET profit_e_reservations = 0
WHERE id IN (
  SELECT id FROM (
    SELECT products_stock.id FROM products_stock AS products_stock
    LEFT JOIN  products_stock_reservations AS PSR
    ON products_stock.id = PSR.products_stock_id
    WHERE PSR.id IS NULL 
    AND products_stock.profit_e_reservations > 0
  ) AS TMP
)';
$pDbMgr->Query('profit24', $sSql);


$sSql = 'UPDATE products_stock 
SET profit_j_reservations = 0
WHERE id IN (
  SELECT id FROM (
    SELECT products_stock.id FROM products_stock AS products_stock
    LEFT JOIN  products_stock_reservations AS PSR
    ON products_stock.id = PSR.products_stock_id
    WHERE PSR.id IS NULL 
    AND products_stock.profit_j_reservations > 0
  ) AS TMP
)';
$pDbMgr->Query('profit24', $sSql);