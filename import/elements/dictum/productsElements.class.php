<?php
/**
 * Klasa parsowania elementów ze źródła Dictum do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class productsElements extends CommonSynchro {
	public $aProduct = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'dictum';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'book', //  głowny tag książki
				'id', // id książki w bazie źródła
				'title', // tytuł książki
				'originaltitle', // tytuł oryginalny
				'author', // autor książki
				'translator', // tłumacz książki
				'status', // status książki, możliwe są: „W sprzedaży”; „Nowość”, „Zapowiedź”, „Dodruk”
				'ean', // kod kreskowy
				'isbn', // isbn książki
				'publisher', // wydawca
				'purchasegrossprice', // XXX TODO DOPYTAĆ JAK wróci Marcin Chudy
				'grossprice', // cena detaliczna brutto
				'vat', // stawka podatku VAT
				'releasedate', // data premiery rynkowej
				'dateofsale', // data dystrybucji, czyli moment, w którym książka fizycznie pojawia się w magazynie Dictum
				'issueyear', // rok wydania
				'issuenumber', // numer wydania książki
				'category', // kategoria książki
				'series', // seria do jakiej należy książka
				'description', // opis książki
				'cover', // rodzaj oprawy
				'sites',// liczba stron
				'height',// wysokość oprawy
				'width', // szerokość oprawy
				'thickness', // grubość grzbietu oprawy
				'weight', // waga książki
		);
		
		$this->iSourceId = $this->getSourceId('dictum');
	}

	public function clearProductData() {
		unset($this->aProduct);
	}
	
	public function postParse() {
		
		if (trim($this->aProduct['isbn_plain']) == '') {
			return false;
		}
		
		
		if ($this->aProduct['_status_txt'] == 'Zapowiedź' && $this->aProduct['_release_date'] != '' && $this->checkAddPreviews($this->aProduct['_release_date'])) {
			$this->aProduct['prod_status'] = '3';
			$this->aProduct['_shipment_date'] = $this->aProduct['_release_date'];
		}
    
		if ($this->aProduct['price_brutto'] > 0.00 && $this->aProduct['vat'] > 0) {
			$this->aProduct['price_netto'] = Common::formatPrice2($this->aProduct['price_brutto']/(1+$this->aProduct['vat']/100));
		} else {
			return false;
		}
		return $this->aProduct;
	}
	
	public function setBook(&$oXml) {
		// TYLKO obsługiwanie
		$this->aProduct['_id'] = $oXml->getAttribute('id'); // id książki w źródle
	}
	
	public function setTitle(&$oXml) {
		$this->aProduct['name'] = $this->getTextNodeValue($oXml);
		$this->aProduct['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aProduct['name'], ' ')));
		if (stristr($this->aProduct['name'], '(Audiobook)')) {
			$this->aProduct['type'] = '1';
		}
	}

	public function setOriginaltitle(&$oXml) {
		$this->aProduct['original_title'] = $this->getTextNodeValue($oXml);
	}
	
	public function setAuthor(&$oXml) {
		$this->aProduct['_items_authors'] = $this->getTextNodeValue($oXml);
	}
	
	public function setTranslator(&$oXml) {
		$this->aProduct['translator'] = $this->getTextNodeValue($oXml);
	}
	
	public function setStatus(&$oXml) {
		
		// TODO interpretacja statusów - dodać zapowiedź
		$sStatus = $this->getTextNodeValue($oXml);
		if ($sStatus != '') {
      // @mod 05.08.2013 + "Promocja"
			if ($sStatus == 'W sprzedaży' || 
          $sStatus == 'Promocja' || 
          $sStatus == 'Nowość' || 
          $sStatus == 'Niedostępny' || 
          $sStatus == 'Dodruk' || 
          $sStatus == 'Zapowiedź') {
				// domyślnie niedostępna - status musi zostać zaktualizowany po dodaniu produktu
				
				$this->aProduct['_status_txt'] = $sStatus;
				$this->aProduct['prod_status'] = '0'; 
			} else {
				// nieobsługiwany status produktu
				$this->addErrorLog("Nie obsługiwany status ".$sStatus." produktu o isbn: ".$this->aProduct['isbn']);
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function setEan(&$oXml) {
		// $this->aProduct['id'] = $this->getTextNodeValue($oXml);
		$sEAN13 = $this->getTextNodeValue($oXml);
		$this->aProduct['ean_13'] = sprintf('%013s', $sEAN13);
		if ($this->aProduct['ean_13'] != $sEAN13) {
			$this->aProduct['isbn_13'] = $sEAN13;
		}
	}
	
	public function setIsbn(&$oXml) {
		$sIsbn = $this->getTextNodeValue($oXml);
		
		if ($sIsbn != '') {
			$this->aProduct['isbn'] = isbn2plain($sIsbn);
			$this->aProduct['isbn_plain'] = $this->aProduct['isbn']; // isbn2plain f-cja z functions
		}
	}
	
	public function setPublisher(&$oXml) {
		// czyścimy ze slashy jakiś dziwnych ponieważ:
		//	- http://stackoverflow.com/questions/3235219/urlencoded-forward-slash-is-breaking-url
		$this->aProduct['_publisher'] = str_replace('/', '', $this->getTextNodeValue($oXml));
	}
	
	public function setPurchasegrossprice(&$oXml) {
		// XXX TODO DOPYTAĆ JAK wróci Marcin Chudy
		$this->aProduct['_wholesale_price'] = $this->getTextNodeValue($oXml);
	}
	
	public function setGrossprice(&$oXml) {
		$this->aProduct['price_brutto'] = Common::formatPrice2($this->getTextNodeValue($oXml));
	}
	
	public function setVat(&$oXml) {
		$aMatches = array();
		
		$sVat = $this->getTextNodeValue($oXml);
		preg_match("/^(\d+)/", $sVat, $aMatches);
		$iVatt = intval($aMatches[1]);
		if ($iVatt > 0) {
			$this->aProduct['vat'] = $iVatt;
		} else {
			return false;
		}
	}
	
	public function setReleasedate(&$oXml) {
		$aMatches = array();
		// w Profit nie ma na to pola
		// $this->aProduct['id'] = $this->getTextNodeValue($oXml);
		$bIsMatch = preg_match("/^\d{4}-\d{2}-\d{2}$/", $this->getTextNodeValue($oXml), $aMatches );
		if ($bIsMatch == true && $aMatches[0] != '') {
			$this->aProduct['_release_date'] = $aMatches[0]; // data premiery
		}
	}
	
	public function setDateofsale(&$oXml) {
		// w Profit nie ma na to pola
		//$this->aProduct['id'] = $this->getTextNodeValue($oXml);
	}
	
	public function setIssueyear(&$oXml) {
		$iPYear = intval($this->getTextNodeValue($oXml));
		if ($iPYear > 0) {
			$this->aProduct['publication_year'] = $iPYear;
		}
	}
	
	public function setIssuenumber(&$oXml) {
		
		$iEdition = intval($this->getTextNodeValue($oXml));
		if ($iEdition > 0) {
			$this->aProduct['edition'] = $iEdition;
		}
	}
	
	/**
	 * Metoda ustawia kategorię produktu
	 *
	 * @param type $oXml
	 * @return boolean 
	 */
	public function setCategory(&$oXml) {

		// XXX modyfikacja z dnia 19.10.2012 - usunięcie uwzględniania mapowań kategorii dictum
		// z powodu usunięcia tagu id w kategorii, 
		// Marcin Chudy nie chciał wprowadzać rozróżniania po nazwie
    
		$this->aProduct['_profit_categories'] = array('1'); // TODO todaj kategorie
    
		$sCatName =(string) $this->getTextNodeValue($oXml);
    
    if (preg_match("/^ZABAWKI/", $sCatName)) {
      // nie importujemy jeśli są to zabawki
      unset($this->aProduct);
    }
    
		if ($sCatName != '') {
			if (stristr($sCatName, 'AUDIOBOOKS')) {
				$this->aProduct['type'] = '1';
			}
		} else {
			return false;
		}
	}// end of setCategory() method
	
	public function setSeries(&$oXml) {
		
		$iSeriesId = intval($oXml->getAttribute('id'));
		$sSeriesName = $this->getTextNodeValue($oXml);
		if ($iSeriesId > 0 && $sSeriesName != '') {
			$this->aProduct['_items_series'] = array(
					$iSeriesId => $sSeriesName
			);
		} else {
			return false;
		}
	}
	
	public function setDescription(&$oXml) {
		
		$sDesc = eregi_replace('<a.*</a>','', $this->clearAsciiCrap($this->getTextNodeValue($oXml)));
		$sDesc = eregi_replace('<script.*</script>','', $sDesc);
		$this->aProduct['description'] = $sDesc;
		$this->aProduct['short_description'] = $sDesc;			
	}
	
	public function setCover(&$oXml) {
		
		$sBinding = $this->getTextNodeValue($oXml);
		if ($sBinding != '') {
			$this->aProduct['_binding'] = $sBinding;
		}
	}
	
	public function setSites(&$oXml) {
		
		$iPages = intval($this->getTextNodeValue($oXml));
		if ($iPages > 0) {
			$this->aProduct['pages'] = $iPages;
		}
	}
	
	public function setHeight(&$oXml) {}
	
	public function setWidth(&$oXml) {}
	
	public function setThickness(&$oXml) {}
	
	public function setWeight(&$oXml) {
		
		$iWeight = (double)$this->getTextNodeValue($oXml);
		if ($iWeight > 0.00) {
			$this->aProduct['weight'] = $iWeight;
		}
	}
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Trochę przydługa metoda zapisywania zdjęcia do produktu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iProductId
	 * @param type $iSourceBookId
	 * @param type $aProduct
	 * @return boolean 
	 */
	public function addImage($iProductId, $iSourceBookId, $aProduct) {
		global $aConfig, $pDbMgr;
		
		
		$this->createProductImageDir($iProductId);
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
		$sShortDir = 'okladki/'.$iRange.'/'.$iProductId;
		
		$sDstName = $aProduct['isbn_plain'].".jpg";
		$sSrcFile = $sDestDir."/_".$sDstName;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id=".$iProductId." AND photo='".$sDstName."' AND name='".$sDstName."'";
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if ($iId > 0) {
			// zdjecie juz istnieje w bazie nie dodajemy ponownie
			return true;
		}
		
		$iTimeOut = 0;
		$sURL = "http://www.dictum.pl/images/covers/".$iSourceBookId.".jpg";
		// otwarcie pliku
		if ($rFp = fopen($sSrcFile, "w")) {
			$oCURL = curl_init();
      //curl_setopt($oCURL, CURLOPT_INTERFACE, "188.165.19.148");
			curl_setopt($oCURL, CURLOPT_URL, $sURL);
			curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			if ($iTimeOut > 0) {
				curl_setopt($oCURL, CURLOPT_TIMEOUT, $iTimeOut);
			}
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			//curl_setopt($oCURL, CURLOPT_POSTFIELDS, $sParameters);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFp);

			curl_exec($oCURL);
			curl_close($oCURL);
			fclose ($rFp);
		}
		
		if(!file_exists($sSrcFile))
			return false;

		$aFileInfo = getimagesize($sSrcFile);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
		$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
		if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
			$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
		}
		unlink($sSrcFile);

		$aValues=array(
			'product_id' => $iProductId,
			'directory' => $sShortDir,
			'photo' => $sDstName,
			'mime' => 'image/jpeg',
			'name' => $sDstName,
			'order_by' => '1'
		);
		return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
	}// end of addImage() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sLogin = '0627';
		$sPassword = '0627_3559';
		$sQuery = "http://www.dictum.pl/books/dane.xml?login=".$sLogin."&password=".$sPassword;
		$sFileName = date("siH_dmY").'_products.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/dictum/".$sFileName;
		// otwarcie pliku
		$rFp = fopen($sFilePath, "w");
		if ($rFp) {
			$oCURL = curl_init();
			//curl_setopt($oCURL, CURLOPT_INTERFACE, "188.165.19.148");
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFp);

			curl_exec($oCURL);

			curl_close($oCURL);
			fclose ($rFp);
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	public function proceedInsert(&$oParent, $aProduct, $iSourceBId) {
    $oParent->proceedInsertProduct($aProduct, $iSourceBId, $this->aProduct['_profit_categories']);
	}
	
	public function proceedUpdate(&$oParent, &$aProduct, $iSourceBId, $iProductId) {
		// dodaj bookindeks dla produktu
		$sMD5 = $this->getProductMD5($aProduct);
		$mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);

		if ($aProduct['prod_status'] != '3') {
			// dla zapowiedzi ustawiać status zapowiedź
			unset($aProduct['prod_status']); // to niech się ustawia ze skryptu aktualizacji stanów
		}
		// dodać aktualizację cen
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aProduct, true);
		if ($this->checkUpdateProduct($aProduct, $mRetMD5, $iProductId)) {
			$this->proceedUpdateProduct($aProduct, $iProductId, $iSourceBId, $this->aProduct['_profit_categories']);
		}
	}
	
	public function checkExists(&$aProduct) {
		return $this->refCheckProductExists($aProduct);
	}
}// end of productsElements() Class
?>
