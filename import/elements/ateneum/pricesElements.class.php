<?php
/**
 * Klasa aktualizacji stanów produktu z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-01-31
 * @copyrights Marcin Chudy - Profit24.pl
 */
class pricesElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
  private $aConfig;
  private $pDbMgr;

  function __construct($aConfig, $pDbMgr) {
    $this->aConfig = $aConfig;
    $this->pDbMgr = $pDbMgr;



		$this->sSoruceSymbol = 'ateneum';

		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
    //
    //Ident_ate, Stan_magazynowy, Cena_detaliczna_netto, Cena_detaliczna_brutto, Stawka_VAT
		$this->aSupportedTags = array(
				'source_id',// Ident_ate 1,
				'stock', // Stan_magazynowy 0,
				'price_netto', // Cena_detaliczna_netto 30.38,
				'price_brutto', // Cena_detaliczna_brutto 31.90,
				'vat', // Stawka_VAT 5
		);

		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
  }


  public function setSource_id($sValue) {
    $this->aElement['_id'] = intval($this->getText($sValue));
    $this->aElement['_product_id'] = $this->getProductIdBySourceId($this->aElement['_id'], $this->iSourceId);
  }

  public function setPrice_netto($sValue) {
    $this->aElement['price_netto'] = floatval($this->getText($sValue));
  }

  public function setPrice_brutto($sValue) {
    $this->aElement['price_brutto'] = floatval($this->getText($sValue));
  }

  public function setVat($sValue) {
    $this->aElement['vat'] = intval($this->getText($sValue));
  }

  public function setStock($sValue) {
//    $this->aElement['_stock'] = intval($this->getText($sValue));
  }


	public function clearProductData() {

		unset($this->aElement);
	}



	public function postParse() {

//    dump($this->aElement);die;
		if ($this->aElement['_id'] == '' || $this->aElement['_id'] <= 0) {
			return -1;
		}

		if ($this->aElement['_product_id'] == '' || $this->aElement['_product_id'] <= 0) {
			return -1;
		}

//    if ($this->aElement['_stock'] > 0) {
//      $this->aElement['prod_status'] = '1';
//    } else {
//      $this->aElement['prod_status'] = '0';
//    }
    $this->aElement['shipment_time'] = '1';

		return $this->aElement;
	}


	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aItem, true);
	}

	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		// nie potrzebny tu insert, a nawet nie wskazay
	}

	public function checkExists($aItem) {
		return intval($aItem['_product_id']) > 0 ? intval($aItem['_product_id']) : false;
	}


	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method



	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 *
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;

		$sFileName = date("siH_dmY").'_prices_stock.CSV';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/ateneum/".$sFileName;

		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'dbupdate/stanyceny.php');
		fclose ($rFp);

		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method


	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {

		$sLogin = 'profit';
		$sPassword = 'g8gMKtgp';
		$sQuery = "http://".$sLogin.":".$sPassword."@ateneum.net.pl/".$sFile;

		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}


  public function PrepareSynchro() {}
}

?>
