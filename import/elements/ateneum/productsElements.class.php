<?php

/**
 * Klasa dodawania aktualizacji produktów Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-01-31
 * @copyrights Marcin Chudy - Profit24.pl
 */
class productsElements extends CommonSynchro
{
    public $aElement = array();
    public $aSupportedTags = array();
    public $iSourceId = NULL;
    public $sSoruceSymbol = NULL;
    private $aConfig;
    private $pDbMgr;
    public $_countEmptyPublicationYear;
    protected $sLogin = 'profit';
    protected $sPassword = 'g8gMKtgp';

    function __construct($aConfig, $pDbMgr)
    {
        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->iNextIdSource = 0;


        $this->sSoruceSymbol = 'ateneum';

        // Stan na dzień 31.07.2012 r. - pierwsza implementacja
        $this->aSupportedTags = array(
            'ateneum_id',// 123598,
            'EAN',// "9788364057137",
            'ISBN',// "9788364057137",
            'Tytul',// "Opowie¶ci graficzne TW",
            'Autorzy',// "Bernet Jordi, Abuli E.S.",
            'Wydawca',// "Korporacja Ha!Art",
            'Opis_wydania',// "Strony: 184, Format: 24,3x16,8 cm",
            'Opis_wydania2',// "Rok wydania: 2013, oprawa: twarda",
            'Opis',// "Unikatowe historie obrazkowe, stworzone przez Antonisza na przestrzeni czterdziestu lat w różnych momentach jego życia, stanowi± bezcenne uzupełnienie twórczo¶ci artysty. Ksi±żka odzwierciedla różnorodno¶ć tematów i technik narracyjno-graficznych stosowanych przez autora - poczynaj±c od młodzieńczej, 64-stronicowej próby ""Przygody Hitlera"" z 1954 roku, przez obszerny scenopis obrazkowy do niezrealizowanego filmu ""Pierony"" (lata 70.), aż po licz±cy dwadzie¶cia dwie strony fotokomiks ""Jak powstało Muzeum Filmu Non-Camera"" (1984 r.). Wszystkie wymienione realizacje stanowi± ¶wiadectwo nieprzeciętnej wyobraĽni, znakomitego zmysłu obserwacji i surrealistycznego poczucia humoru, wspartych inwencj± formaln±, widoczn± w wykorzystaniu przez artystę szerokiego arsenału ¶rodków plastycznych. ""Realizacja filmów eksperymentalnych metodami grafiki artystycznej poprzez odbijanie grafik bezpo¶rednio na ta¶mie filmowej z pominięciem kamery filmowej jest szans± na stworzenie filmu będ±cego autentycznym dziełem sztuki"" - pisał artysta w swoim ""Manife¶cie artystycznym grupy twórczej Non-Camera"" z 1977 roku. W wyniku interwencji autora film - jako unikatowy obiekt - zyskuje rangę obiektu sztuki, a sama ta¶ma filmowa nabiera cech ""hipergęstej"" sekwencji grafik, w której rozbity na fazy ruch budzi skojarzenia z narracj± komiksow±. Tego rodzaju podej¶cie do materii filmowej pozwala wpisać Antonisza nie tyle do grona ""filmowców"", co raczej intermedialnych, awangardowych artystów, traktuj±cych swoje kinematograficzne realizacje jako czę¶ć większego, interdyscyplinarnego (lub postdyscyplinarnego) projektu. ",
            'Cena_netto',// 56.19,
            'Podatek_opis',// "Podatek VAT 5%",
            'Cena_brutto',// 59.00,
            'kat1',// "Komiksy",
            'kat2',// "Dla dorosłych",
            'kat3',// "",
            'okl_path',// "123598.jpg",
            'hash',// "6f5f430607b2cc20af1d68c31288cd17",
            'empty',


        );

        /*
//				'book', //  głowny tag książki
				'ident_ate', // id książki w bazie źródła
        'EAN',
        'ISBN',
        'Tytył',
        'autor',
        'wydawnictwo',
        'opis_wydania', // ilosc stron, format
        'rok_wydania',
        'krótka_charakterystyka',
        'cena_detal_netto',
        'stawka_vat', // stawka vat słownie
        'cena_detal_brutto',
        'kategoria_poziom_1',
        'kategoria_poziom_2',
        'kategoria_poziom_3',
        'kategoria_poziom_3',
        'plik_zdjecia',
        'hash',
         */

        $this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
        $this->iNextIdSource = $this->getMaxIdInSource($this->iSourceId);
    }


    public function setEmpty($sValue)
    {
    }// fix na pusty set

    public function setAteneum_id($sValue)
    {
        $this->aElement['_id'] = intval($this->getText($sValue));
    }

    public function setEAN($sValue)
    {
        $sEan13 = $this->getText($sValue);
        if ($sEan13 != '' && preg_match('/^[\dX]{6,15}$/', $sEan13)) {
            $this->aElement['ean_13'] = sprintf('%013s', $sEan13);
        }
    }

    public function setISBN($sValue)
    {
        //$this->aElement['ean_13'] = intval($this->getText($sValue));
        // validator na zabawki - np. 10+
        $sIsbn = $this->getText($sValue);
        if ($sIsbn != '' && preg_match('/^[\dX]{6,15}$/', $sIsbn)) {
            $this->aElement['isbn'] = isbn2plain($sIsbn);
            $this->aElement['isbn_plain'] = $this->aElement['isbn']; // isbn2plain f-cja z functions
        }
    }

    public function setTytul($sValue)
    {
        $this->aElement['name'] = $this->getText($sValue);
        $this->aElement['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aElement['name'], ' ')));
    }

    public function setAutorzy($sValue)
    {
        $this->aElement['_items_authors'] = $this->getText($sValue);
    }

    public function setWydawca($sValue)
    {
        // czyścimy ze slashy jakiś dziwnych ponieważ:
        //	- http://stackoverflow.com/questions/3235219/urlencoded-forward-slash-is-breaking-url
        $this->aElement['_publisher'] = str_replace('/', '', $this->getText($sValue));
    }

    public function setOpis_wydania($sValue)
    {
        // "Strony: 184, Format: 24,3x16,8 cm",
        // $this->aElement['_items_authors'] = $this->getText($sValue);
        $aMatches = array();
        preg_match('/Strony:\s?(\d+)/', $this->getText($sValue), $aMatches);
        if (intval($aMatches[1]) > 0) {
            $this->aElement['pages'] = intval($aMatches[1]);
        }
    }


    public function setOpis_wydania2($sValue)
    {
        // "Rok wydania: 2013, oprawa: twarda",
        // $this->aElement['_items_authors'] = $this->getText($sValue);
        $aMatches = array();
        preg_match('/Rok wydania:\s?(\d+)/', $this->getText($sValue), $aMatches);
        if (intval($aMatches[1]) > 0) {
            $this->aElement['publication_year'] = intval($aMatches[1]);
        }
    }

    public function setOpis($sValue)
    {

        $sDesc = eregi_replace('<a.*</a>', '', $this->clearAsciiCrap($this->getText($sValue)));
        $sDesc = eregi_replace('<script.*</script>', '', $sDesc);
        $this->aElement['description'] = $sDesc;
        $this->aElement['short_description'] = $sDesc;
    }

    public function setCena_netto($sValue)
    {
        $this->aElement['price_netto'] = Common::formatPrice2($this->getText($sValue));
    }

    public function setPodatek_opis($sValue)
    {
        // jakieś netto bardzo dziwne w opisie haha
        // Podatek VAT 5%
    }

    public function setCena_brutto($sValue)
    {
        $this->aElement['price_brutto'] = Common::formatPrice2($this->getText($sValue));
    }


    /**
     * Metoda zwraca maksymalny id kategorii w zrodle
     * @global array $aConfig
     * @global type $pDbMgr
     * @param int $iSourceId
     * @return int
     */
    private function getMaxIdInSource($iSourceId)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT MAX(source_category_id)
						 FROM " . $aConfig['tabls']['prefix'] . "menus_items_mappings_categories
						 WHERE source_id = " . $iSourceId;
        return $pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $sCategoryName
     * @param $iLevel
     * @return mixed
     */
    private function getSoruceCategories($sCategoryName, $iLevel)
    {
        global $pDbMgr;
        $iParentId = $iLevel > 1 ? $this->aElement['_ateneum_cat_id'][$iLevel - 1] : 0;

        $iCatId = $this->checkCategoryExistsByName($sCategoryName, $iParentId);

        if (intval($iCatId) <= 0) {
            $this->iNextIdSource++;
            $aValues = [
                'source_id' => $this->iSourceId,
                'source_category_id' => $this->iNextIdSource,
                'source_category_parent_id' => $iLevel > 1 ? ($this->aElement['_ateneum_cat_id'][$iLevel - 1] > 0 ? $this->aElement['_ateneum_cat_id'][$iLevel - 1] : 'NULL') : 'NULL',
                'name' => $sCategoryName
            ];
            $iCatId = $pDbMgr->Insert('profit24', 'menus_items_mappings_categories', $aValues);
            // nowa kategoria nie może być mapowania
            $this->aElement['_ateneum_cat_id'][$iLevel] = $this->iNextIdSource;
        } else {
            $this->aElement['_ateneum_cat_id'][$iLevel] = $iCatId;
        }
        return $iCatId;
    }


    /**
     * Metoda zwraca id categorii na podstawie nazwy i zrodla
     * @global type $pDbMgr
     * @param string $sCatName - nazwa kategorii
     * @param int $iParent - id rodzica
     * @return mixed
     */
    private function checkCategoryExistsByName($sCatName, $iParent)
    {
        global $pDbMgr;

        $sCond = "AND source_category_parent_id ";
        if ($iParent > 0) {
            $sCond .= " = " . $iParent;
        } else {
            $sCond .= "IS NULL";
        }
        $sSql = "SELECT source_category_id
             FROM menus_items_mappings_categories
             WHERE source_id = " . $this->iSourceId . "
             $sCond
             AND TRIM(name) = '$sCatName'";
        $iCatId = $pDbMgr->GetOne('profit24', $sSql);
        if ($iCatId > 0) {
            return $iCatId;
        } else {
            return false;
        }
    }

    public function setKat1($sValue)
    {
        // Historia
        // onInsert
        $sCatName = $this->getText($sValue);
        if ($sCatName != '') {
            echo '1';
            $iCatId = $this->getSoruceCategories($sCatName, 1);
            if ($iCatId > 0) {
                $this->aElement['_items_category'][$iCatId] = $sCatName;
            }
        }
    }

    public function setKat2($sValue)
    {
        // Sztuki
        // onInsert
        $sCatName = $this->getText($sValue);
        if ($sCatName != '') {
            echo '2';
            $iCatId = $this->getSoruceCategories($sCatName, 2);
            if ($iCatId > 0) {
                $this->aElement['_items_category'][$iCatId] = $sCatName;
            }
        }
    }

    public function setKat3($sValue)
    {
        // Nowoczeznej
        // onInsert
        $sCatName = $this->getText($sValue);
        if ($sCatName != '') {
            echo '3';
            $iCatId = $this->getSoruceCategories($sCatName, 3);
            if ($iCatId > 0) {
                $this->aElement['_items_category'][$iCatId] = $sCatName;
            }
        }
    }

    public function setOkl_path($sValue)
    {
        $img = $this->getText($sValue);
        if ($img != '') {
            $filename = preg_replace('/(.*)\.(.*)/', '$1', $img);
            $this->aElement['_img'] = 'http://' . $this->sLogin . ':' . $this->sPassword . '@ateneum.net.pl/dbupdate/imagelarge.php?id=' . $filename;
        }
    }

    /**
     * Trochę przydługa metoda zapisywania zdjęcia do produktu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param type $iProductId
     * @param type $iSourceBookId
     * @param type $aProduct
     * @return boolean
     */
    public function addImage($iProductId, $iSourceBookId, $aProduct)
    {
        global $aConfig, $pDbMgr;

        $this->createProductImageDir($iProductId);
        $iRange = intval(floor($iProductId / 1000)) + 1;
        $sDestDir = $aConfig['common']['base_path'] . 'images/photos/okladki/' . $iRange . '/' . $iProductId;
        $sShortDir = 'okladki/' . $iRange . '/' . $iProductId;

        $sDstName = $aProduct['isbn_plain'] . ".jpg";
        $sSrcFile = $sDestDir . "/_" . $sDstName;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "products_images
						 WHERE product_id=" . $iProductId . " AND photo='" . $sDstName . "' AND name='" . $sDstName . "'";
        $iId = $pDbMgr->GetOne('profit24', $sSql);
        if ($iId > 0) {
            // zdjecie juz istnieje w bazie nie dodajemy ponownie
            return true;
        }

        $iTimeOut = 0;
        $sURL = $aProduct['_img'];
        // otwarcie pliku
        if ($rFp = fopen($sSrcFile, "w")) {
            $oCURL = curl_init($sURL);
            curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCURL, CURLOPT_HEADER, FALSE);

            curl_setopt($oCURL, CURLOPT_FILE, $rFp);
            curl_setopt($oCURL, CURLOPT_TIMEOUT, 1000);
            curl_setopt($oCURL, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($oCURL, CURLOPT_USERAGENT, 'Mozilla/5.0');

            curl_exec($oCURL);
            curl_close($oCURL);
            fclose($rFp);
        }

        if (!file_exists($sSrcFile))
            return false;

        $aFileInfo = getimagesize($sSrcFile);
        $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_' . $sDstName);
        $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
        $aSmallInfo = getimagesize($sDestDir . '/' . $sDstName);
        if ($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])) {
            $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_' . $sDstName);
        }
        unlink($sSrcFile);

        $aValues = array(
            'product_id' => $iProductId,
            'directory' => $sShortDir,
            'photo' => $sDstName,
            'mime' => 'image/jpeg',
            'name' => $sDstName,
            'order_by' => '1'
        );
        return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix'] . "products_images", $aValues, '', false) != false);
    }// end of addImage() method


    public function setHash($sValue)
    {
    }

    public function clearProductData()
    {
        unset($this->aElement);
    }

    public function postParse()
    {

        if (trim($this->aElement['isbn_plain']) == '' && $this->aElement['ean_13'] != '') {
            $this->aElement['isbn_plain'] = $this->aElement['ean_13'];
            $this->aElement['isbn'] = $this->aElement['ean_13'];
        } else if (trim($this->aElement['isbn_plain']) == '') {
            return -1;
        }
        if (intval($this->aElement['publication_year']) <= 0) {
            $this->_countEmptyPublicationYear++;
        }

        //...

        return $this->aElement;
    }


    /**
     * Metoda pobiera id źródła dla danego importu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param string $sSourceSymbol - symbol źródła
     * @return type
     */
    private function getSourceId($sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "sources
						 WHERE symbol='" . $sSourceSymbol . "'";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceId() method


    /**
     * Metoda importuje XML'a i zapisuje na dysku
     *
     * @return string - adres pliku XML z danymi
     */
    public function importSaveXMLData()
    {
        global $aConfig;

        $sFileName = date("siH_dmY") . '_product.CSV';
        $sFilePath = $aConfig['common']['base_path'] . $aConfig['common']['import_dir'] . "/CSV/ateneum/" . $sFileName;

        $rFp = fopen($sFilePath, "w");
        $bIsErr = $this->getSourceFile($rFp, 'bazaksiazek/baza_ksiazek.csv');
        fclose($rFp);

        if ($bIsErr == true) {
            return $sFilePath;
        } else {
            return false;
        }
    }// end of importSaveXMLData() method


    /**
     * Metoda pobiera ze źródła plik
     *
     * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
     * @param string $sFile - nazwa pliku który chcemy pobrać
     * @return boolean
     */
    protected function getSourceFile(&$rFileHandle, $sFile)
    {


        $sQuery = "http://" . $this->sLogin . ":" . $this->sPassword . "@ateneum.net.pl/" . $sFile;

        if ($rFileHandle) {
            $oCURL = curl_init();
            curl_setopt($oCURL, CURLOPT_URL, $sQuery);
            curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
            curl_setopt($oCURL, CURLOPT_POST, FALSE);
            curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
            curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

            if (curl_exec($oCURL) === false) {
                curl_close($oCURL);
                return false;
            }
            curl_close($oCURL);
            return true;
        }
        return false;
    }


    public function checkExists(&$aElement)
    {
        return $this->refCheckProductExists($aElement);
    }

    public function PrepareSynchro()
    {
    }

    public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId)
    {
        $sMD5 = $this->getProductMD5($aElement);
        $mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);


//        $this->proceedCategoriesMerge($aElement, $iProductId, $this->aElement['_profit_categories']);
//    $this->addImage($iProductId, $iSourceBId, $aElement);

//    if ($this->checkUpdateProduct($aElement, $mRetMD5, $iProductId)) {
//      $this->proceedUpdateProduct($aElement, $iProductId, $iSourceBId, $this->aElement['_profit_categories']);
//    }
        return true;
    }

    public function proceedInsert(&$oParent, $aElement, $iSourceBId)
    {
            $oParent->proceedInsertProduct($aElement, $iSourceBId, $this->aElement['_profit_categories']);
        return true;
    }

}

