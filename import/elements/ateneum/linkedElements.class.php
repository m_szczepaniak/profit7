<?php

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 03.10.16
 * Time: 12:11
 */
class linkedElements extends CommonSynchro
{
    public $aElement = array();
    public $aSupportedTags = array();
    public $iSourceId = NULL;
    public $sSoruceSymbol = NULL;
    private $aConfig;
    private $pDbMgr;

    function __construct($aConfig, $pDbMgr)
    {
        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;


        $this->sSoruceSymbol = 'ateneum';

        // Stan na dzień 31.07.2012 r. - pierwsza implementacja
        //
        //Ident_ate, Stan_magazynowy, Cena_detaliczna_netto, Cena_detaliczna_brutto, Stawka_VAT
        $this->aSupportedTags = array(
            'mainId',
            'linkedId',
        );

        $this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
    }

    /**
     * @return array
     */
    public function postParse()
    {

        $this->aElement['_product_id'] = $this->getProductIdBySourceId($this->aElement['_id'], $this->iSourceId);
        return $this->aElement;
    }


    /**
     * @param $sValue
     */
    public function setMainId($sValue)
    {
        $this->aElement['_id'] = intval($this->getText($sValue));
        $this->aElement['main_id'] = $this->aElement['_id'];
    }

    /**
     * @param $sValue
     */
    public function setLinkedId($sValue)
    {
        $this->aElement['linked_id'] = intval($this->getText($sValue));
    }

    public function clearProductData()
    {

        unset($this->aElement);
    }

    public function checkExists($aItem)
    {
        return false;
    }


    /**
     * Metoda pobiera id źródła dla danego importu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param string $sSourceSymbol - symbol źródła
     * @return type
     */
    private function getSourceId($sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "sources
						 WHERE symbol='" . $sSourceSymbol . "'";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceId() method


    /**
     * Metoda importuje XML'a i zapisuje na dysku
     *
     * @return string - adres pliku XML z danymi
     */
    public function importSaveXMLData()
    {
        global $aConfig;

        $sFileName = date("siH_dmY") . '_linked.CSV';
        $sFilePath = $aConfig['common']['base_path'] . $aConfig['common']['import_dir'] . "/CSV/ateneum/" . $sFileName;

        $rFp = fopen($sFilePath, "w");
        $bIsErr = $this->getSourceFile($rFp, 'dbupdate/powiazane.php');
        fclose($rFp);

        if ($bIsErr == true) {
            return $sFilePath;
        } else {
            return false;
        }
    }// end of importSaveXMLData() method

    /**
     * Metoda pobiera ze źródła plik
     *
     * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
     * @param string $sFile - nazwa pliku który chcemy pobrać
     * @return boolean
     */
    protected function getSourceFile(&$rFileHandle, $sFile)
    {

        $sLogin = 'profit';
        $sPassword = 'g8gMKtgp';
        $sQuery = "http://" . $sLogin . ":" . $sPassword . "@ateneum.net.pl/" . $sFile;

        if ($rFileHandle) {
            $oCURL = curl_init();
            curl_setopt($oCURL, CURLOPT_URL, $sQuery);
            curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
            curl_setopt($oCURL, CURLOPT_POST, FALSE);
            curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
            curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

            if (curl_exec($oCURL) === false) {
                curl_close($oCURL);
                return false;
            }
            curl_close($oCURL);
            return true;
        }
        return false;
    }

    public function PrepareSynchro()
    {
        global $pDbMgr;

        $sSql = 'DELETE FROM products_ateneum_linked WHERE id > 0';
        return $pDbMgr->Query('profit24', $sSql);
    }

    public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId)
    {
        return $this->insertUpdate($aItem);
    }

    public function proceedInsert(&$oParent, $aItem, $iSourceBId)
    {
        return $this->insertUpdate($aItem);
    }

    private function insertUpdate($aItem)
    {
        global $pDbMgr;

        $aValues = [
            'mainId' => $aItem['main_id'],
            'linkedId' => $aItem['linked_id']
        ];
        return $pDbMgr->Insert('profit24', 'products_ateneum_linked', $aValues);
    }
}