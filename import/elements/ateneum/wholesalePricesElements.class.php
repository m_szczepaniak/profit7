<?php
/**
 * Klasa aktualizacji stanów produktu z Ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-01-31 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class wholesalePricesElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
  private $aConfig;
  private $pDbMgr;
  
  function __construct($aConfig, $pDbMgr) {
    $this->aConfig = $aConfig;
    $this->pDbMgr = $pDbMgr;
            
    

		$this->sSoruceSymbol = 'ateneum';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'source_id',
				'wholesale_price_netto',
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
  }
  
  
	public function clearProductData() {
		
		unset($this->aElement);
	}
  
  

	public function postParse() {
		
		if ($this->aElement['_id'] == '' || $this->aElement['_id'] <= 0) {
			return -1;
		}
    
		if ($this->aElement['_product_id'] == '' || $this->aElement['_product_id'] <= 0) {
			return -1;
		}
    
    // pobierzmy Vat i przeliczmy zakup brutto
    $aProductStock = $this->getProductVatStock($this->aElement['_product_id']);
    if ($aProductStock['id'] > 0) {
      // wyliczamy wartość netto ceny zakupu
      $this->aElement['_wholesale_price'] = Common::formatPrice2($this->aElement['_wholesale_price_netto']*(1 + $aProductStock[$this->sSoruceSymbol . '_vat']/100));
      
      // XXX sztuczne podniesienie ceny zakupu o 5%
      //$this->aElement['_wholesale_price'] = Common::formatPrice2($this->aElement['_wholesale_price'] * (0.68)); 
	  //$this->aElement['_wholesale_price'] = $this->aElement['_wholesale_price'] - Common::formatPrice2($this->aElement['_wholesale_price'] * (0.68 / 100));
    }
    
		return $this->aElement;
	}
  
  
  public function setSource_id($sValue) {
    $this->aElement['_id'] = intval($this->getText($sValue));
    $this->aElement['_product_id'] = $this->getProductIdBySourceId($this->aElement['_id'], $this->iSourceId);
  }
  
  public function setWholesale_price_netto($sValue) {
    $this->aElement['_wholesale_price_netto'] = floatval($this->getText($sValue));
  }
  
  
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aItem, true);
	}
  
	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		// nie potrzebny tu insert, a nawet nie wskazay
	}
	
	public function checkExists($aItem) {
		return intval($aItem['_product_id']) > 0 ? intval($aItem['_product_id']) : false;
	}
  
  
	/**
	 * Metoda pobiera status produktu
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iPId
	 * @return array
	 */
	public function getProductVatStock($iPId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id, ateneum_vat
						 FROM ".$aConfig['tabls']['prefix']."products_stock
						 WHERE 
              id = ".$iPId."
              AND ateneum_price_netto > 0
						 LIMIT 1";
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getProductStock() method
  
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
  
  

	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_wholesale_price_netto.CSV';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/ateneum/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'profit/ceny.csv');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
  
  
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {

		$sLogin = 'profit';
		$sPassword = 'g8gMKtgp';
		$sQuery = "http://".$sLogin.":".$sPassword."@ateneum.net.pl/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
  
  
  public function PrepareSynchro() {}
}

?>
