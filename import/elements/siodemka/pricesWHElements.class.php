<?php
/**
 * Klasa parsowania elementu ceny hurtowe super-siodemki Profit24.pl
 * 
 * @version 1.0
 * @author Arkadiusz Golba
 * @copyright Marcin Chudy Profit24.pl - 2013
 */

class pricesWHElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'siodemka';
		
		// Stan na dzień 07.10.2013 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'INDEKS', // numer kartoteki
				'NAZWASKR', // skrót nazwy
                'CENAN', // cena netto
                'VAT', // vat
                'GRUPA_NAZWA', // wydawnictwo
                'EAN', // ean
                'ISBN', // isbn
                'KARTOTEKA_GLOWNA', // ??
                'MAGAZYN'
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
	}

	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {
		
		if ($this->aElement['_wholesale_price_netto'] > 0.00 && $this->aElement['vat'] > 0) {
			// zmienione
			$this->aElement['_wholesale_price'] = Common::formatPrice2($this->aElement['_wholesale_price_netto']*(1+$this->aElement['vat']/100));
		} else {
      return false;
    }
    
		if ($this->aElement['_id'] != '' && $this->aElement['_wholesale_price'] > 0.00) {
			return $this->aElement;
		}
    
		return false; // coś jest nie tak
	}
	
	public function setIndeks($sValue) {
    // w tym przechowujemy id produktu w źródle
    $this->aElement['_id'] = $this->getText($sValue);
  }
  public function setNazwaskr($sValue) {}
  public function setKartoteka_Glowna($sValue) {
    global $pDbMgr;
    
    $sMainIdent = $this->getText($sValue);
    if ($sMainIdent != '') {
          // ok mamy zmienioną kartotekę główną
      if ($sMainIdent != $this->aElement['_id']) {
        // różny ident
        //$this->aElement['_attachment_source_id'] = $sMainIdent;
        $iProductId = $this->getProductIdBySourceId($this->aElement['_id'], $this->iSourceId);
        if ($iProductId > 0) {
          // zmieniamy indeks
        
          // dodajemy główny indeks
          $aValuesPTS = array(
              'product_id' => $iProductId,
              'source_id' => $this->iSourceId,
              'source_index' => $sMainIdent,
              'main_index' => '1'
          );
          $pDbMgr->Insert('profit24', 'products_to_sources', $aValuesPTS);

          // usuwamy ten indeks
          $sSql = "DELETE FROM products_to_sources WHERE 
                   product_id = ".$iProductId." AND
                   source_id = ".$this->iSourceId." AND
                   source_index = '".$this->aElement['_id']."'";
          $pDbMgr->Query('profit24', $sSql);
        } else {
          $iProductId = $this->getProductIdBySourceId($sMainIdent, $this->iSourceId);
        }
        // dodajemy załącznik do produktu jeśli nie istnieje
        $aAttachments = $this->getAttachment($iProductId);
        if (empty($aAttachments)) {
          // dodajemy
          $aAttachment = array(
              'attachment_type' => '6',
              //'price_brutto' => //$this->aElement['price_brutto'],
              //'price_netto' => $this->aElement['price_netto'],
              'vat' => $this->aElement['vat'],
              'siodemka_index' => $this->aElement['_id']
          );
          $this->addAttachment($iProductId, $aAttachment, false);
        }
        // $this->aElement['_attachments']
        
      }
      // jeśli ident załącznika jest inny to dodajemy załącznik lub aktualizujemy
      // jeśli głowny ident źródła jest różny niż wskazany to należy zaktualizować  
    }
    
    
    
    
  }
  public function setGrupa_nazwa($sValue) {}
  
  public function setCenan($sValue) {
    $this->aElement['_wholesale_price_netto'] = Common::FormatPrice2($this->getText($sValue));
  }
  
  public function setVat($sValue) {
    $fVat = Common::FormatPrice2($this->getText($sValue));
    $this->aElement['vat'] = intval($fVat * 100);
  }

  public function setMAGAZYN($sValue) {
      $this->aElement['_location'] = $this->getText($sValue);
  }
  
  public function setEAN($sValue) {
//    $this->aElement['ean_13'] = trim($this->getText($sValue));
  }
  
  public function setIsbn($sValue) {
    /*
		$sIsbn = trim($this->getText($sValue));
    
		if ($sIsbn != '') {
			$this->aElement['isbn'] = isbn2plain($sIsbn);
      $this->aElement['_id'] = $this->aElement['isbn'];
			$this->aElement['isbn_plain'] = $this->aElement['isbn'];
		}
     */
  }
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje CSV'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku CSV z danymi
	 */
	public function importSaveCSVData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_ceny_hurtowe.csv';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/siodemka/".$sFileName;
		
		$bIsErr = $this->getSourceFile($sFilePath, '/PlikiS7/3978.txt');
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveCSVData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param string $sLocalFilePath - sciezka pliku do ktorego zostanie zapisany, plik pobrany
	 * @param string $sRemoteFilePath - sciezka do pliku na serwerze dostawcy
	 * @return boolean 
	 */
	protected function getSourceFile($sLocalFilePath, $sRemoteFilePath) {
		global $aConfig;
    
    set_include_path(get_include_path() . PATH_SEPARATOR . $aConfig['common']['client_base_path'].'omniaCMS/lib/phpseclib/');
    include_once('Net/SFTP.php');
    $sLogin = 'profit_wa';
		$sPassword = 'ProfitFTPwa';
    
    $sftp = new Net_SFTP("213.218.125.80");
    if (!$sftp->login($sLogin, $sPassword)) {
      $sftp->disconnect();
      return false;
    }
    if (!$sftp->get($sRemoteFilePath, $sLocalFilePath)) {
      $sftp->disconnect();
      return false;
    }
    $sftp->disconnect();
    return true;
	}// end of getSourceFile() method

	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		global $aConfig, $pDbMgr;
		// nie potrzebny tu insert, a nawet nie wskazay
	}
	
	
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aItem, true);
	}
  
	public function checkExists($aProduct) {
    return $this->refCheckProductExists($aProduct, TRUE, TRUE);
	}
  
	public function PrepareSynchro() {}
}// end of seriesElements() Class
