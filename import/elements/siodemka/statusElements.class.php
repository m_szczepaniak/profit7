<?php

/**
 * Klasa parsowania elementu stany magazynowe super-siodemki Profit24.pl
 *
 * XXX
 * UWAGA 1 - dodatkowa funkcjonalność w tym imporcie - to ze wzlędu na to iż aktualizowane są pozycje na gównym indeksie źródła,
 *  a główny indeks źródła zmienia się w zależności od tego czy produkt jest dostępny na tym indeksie,
 *  to na końcu importu należy zmienić na "niedostępne w s. siodemce", pozycje które nie zostały zmienione w aktualnym imporcie.
 *
 * UWAGA 2 - dla pewności niedostępne powinny być wszystkie pozycje, z super siodemki które nie zostały w ostatnim imporcie stanów zaktualizowane.
 *
 * UWAGA 3 - update powinien być zabezpieczony minimalną ilością zaktualizowanych produktów
 *
 * @version 1.0
 * @author Arkadiusz Golba
 * @copyright Marcin Chudy Profit24.pl - 2014
 */
class statusElements extends CommonSynchro
{
    public $aElement = array();
    public $aSupportedTags = array();
    public $iSourceId = NULL;
    public $sSoruceSymbol = NULL;
    public $_bSupportMainSourceId = TRUE;
    public $dStartMysqlDateTime = NULL;

    const SIODEMKA_MIN_QUANTITY = 2;

    public function __construct()
    {
        $this->dStartMysqlDateTime = $this->getStartMysqlDateTime();
        $this->sSoruceSymbol = 'siodemka';

        // Stan na dzień 07.02.2013 r. - pierwsza implementacja
        $this->aSupportedTags = array(
            'INDEKS', // indeks siodemki - "KAMA-003"
            'STAN', // stan magazynowy - 7.0
            'CENAN', // cena netto super 7 - 7.07
            'VAT', //  St.VAT - " 5%"
            'MIN', // Stan MIN - 0.0
            'MAX', // Stan MAX - 0.0
            'C_STD_NETTO', // cena sztywna netto - 0.0
            'CENA', // cena det. brutto - 11.0,
            'S' // ?????
        );

        $this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
    }

    private function getStartMysqlDateTime()
    {
        global $pDbMgr;

        $sSql = "SELECT NOW()";
        return $pDbMgr->GetOne('profit24', $sSql);
    }

    public function clearProductData()
    {
        unset($this->aElement);
    }

    public function setIndeks($sValue)
    {

        // w tym przechowujemy id produktu w źródle
        $this->aElement['_id'] = $this->getText($sValue);
    }

    public function setStan($sValue)
    {
        $this->aElement['_stock'] = intval($this->getText($sValue));
    }

    public function setCenan($sValue)
    {
    }

    public function setVat($sValue)
    {
    }

    public function setMin($sValue)
    {
    }

    public function setMax($sValue)
    {
    }

    public function setC_std_netto($sValue)
    {
    }

    public function setCena($sValue)
    {
    }

    public function setS($sValue)
    {
    }

    public function postParse()
    {

        if (($this->aElement['_id'] == '')) {
            return FALSE;
        }

        if ($this->aElement['_stock'] > self::SIODEMKA_MIN_QUANTITY) {
            $this->aElement['prod_status'] = '1';
        } else {
            $this->aElement['prod_status'] = '0';
        }

        $this->aElement['shipment_time'] = '1';
        return $this->aElement;
    }


    /**
     * Metoda pobiera ilość ostatnio zmienonych pozycji,
     * użyte do odpublikowania starych, nie synchronizowanych pozycji
     *
     * @global array $aConfig
     * @global object $pDbMgr
     * @return int
     */
    private function _getCountLastUpdatedItems()
    {
        global $pDbMgr;

        $sSql = "SELECT COUNT(id) "
            . "FROM products_stock "
            . "WHERE " . $this->sSoruceSymbol . "_last_import >=  '" . $this->dStartMysqlDateTime . "'"
            . "LIMIT 40";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of _getCountLastUpdatedItems() method

    /**
     * Metoda odpublikowuje produkty, które nie były synchronizowane wczoraj i dziś
     *
     * @global object $pDbMgr
     * @return bool
     */
    private function _unpublishProducts()
    {
        global $pDbMgr;

        $aValues = array(
            $this->sSoruceSymbol . '_status' => '0',
            $this->sSoruceSymbol . '_stock' => 'NULL'
        );
        $sWhere = $this->sSoruceSymbol . '_last_import < "' . $this->dStartMysqlDateTime . '" AND '
            . $this->sSoruceSymbol . '_status <> "0" ';// stock nie jest tak ważny
        return $pDbMgr->Update('profit24', 'products_stock', $aValues, $sWhere);
    }// end of _unpublishProducts() method


    /**
     * Metoda odpublikowuje produkty, które nie były synchronizowane wczoraj i dziś,
     * jeśli przez te dni zostało zmienione więcej niż 30 produktów
     */
    public function unpublishOutOfDateProducts()
    {

        // sprawdźmy najpierw czy dziś i wczoraj, zmieniono więcej niż 30 pozycji
        if (intval($this->_getCountLastUpdatedItems()) >= 30) {
            if ($this->_unpublishProducts() === FALSE) {
                echo 'ERR';
                $this->addErrorLog("Wystąpił błąd podczas odpublikowywania starych pozycji");
                return false;
            } else {
                echo 'OKI';
            }
        } else {
            echo 'ERR2';
        }
    }// end of unpublishOutOfDateProducts() method


    /**
     * Metoda pobiera id źródła dla danego importu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param string $sSourceSymbol - symbol źródła
     * @return type
     */
    private function getSourceId($sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "sources
						 WHERE symbol='" . $sSourceSymbol . "'";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceId() method


    /**
     * Metoda importuje CSV'a i zapisuje na dysku
     *
     * @return string - adres pliku CSV z danymi
     */
    public function importSaveCSVData()
    {
        global $aConfig;

        $sFileName = date("siH_dmY") . '_stany_n.csv';
        $sFilePath = $aConfig['common']['base_path'] . $aConfig['common']['import_dir'] . "/CSV/siodemka/" . $sFileName;

        $bIsErr = $this->getSourceFile($sFilePath, '/PlikiS7/STD_SKR.TXT');

        if ($bIsErr == true) {
            return $sFilePath;
        } else {
            return false;
        }
    }// end of importSaveCSVData() method


    /**
     * Metoda pobiera ze źródła plik
     *
     * @param string $sLocalFilePath - sciezka pliku do ktorego zostanie zapisany, plik pobrany
     * @param string $sRemoteFilePath - sciezka do pliku na serwerze dostawcy
     * @return boolean
     */
    protected function getSourceFile($sLocalFilePath, $sRemoteFilePath)
    {
        global $aConfig;

        set_include_path(get_include_path() . PATH_SEPARATOR . $aConfig['common']['client_base_path'] . 'omniaCMS/lib/phpseclib/');
        include_once('Net/SFTP.php');
        $sLogin = 'profit_wa';
        $sPassword = 'ProfitFTPwa';

        $sftp = new Net_SFTP("213.218.125.80");
        if (!$sftp->login($sLogin, $sPassword)) {
            $sftp->disconnect();
            return false;
        }
        if (!$sftp->get($sRemoteFilePath, $sLocalFilePath)) {
            $sftp->disconnect();
            return false;
        }
        $sftp->disconnect();
        return true;
    }// end of getSourceFile() method

    public function proceedInsert(&$oParent, $aItem, $iSourceBId)
    {
        global $aConfig, $pDbMgr;
        // nie potrzebny tu insert, a nawet nie wskazay
    }


    public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId)
    {

        if ($iProductId > 0 && $this->aElement['_stock'] > self::SIODEMKA_MIN_QUANTITY) {
            $this->aElement['prod_status'] = '1';
            if ($this->UpdateMainIndex($iProductId) === false) {
                return true;
            }
        }

//        if ($this->aElement['prod_status'] == '1') {
//            if ($iProductId > 0 && false === $this->checkIsBook($iProductId)) {
//                $aItem['prod_status'] = '0';
//                dump($this->aElement);
//            }
//        }

        $this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aItem, true);
    }

    private function checkIsBook($iProductId)
    {
        global $pDbMgr;

        $sSql = 'SELECT product_type 
                 FROM products 
                 WHERE id = ' . $iProductId;
        $pType = $pDbMgr->GetOne('profit24', $sSql);
        if ('K' == $pType) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param int $iProductId
     * @global \DatabaseManager $pDbMgr
     * @return bool
     */
    private function UpdateMainIndex($iProductId)
    {
        global $pDbMgr;

        // update As main
        $aValues = [
            'main_index' => '1'
        ];
        if ($pDbMgr->Update('profit24', 'products_to_sources', $aValues, ' product_id = ' . $iProductId . ' AND 
                                                    source_id = "' . $this->iSourceId . '" AND
                                                    source_index = "' . $this->aElement['_id'] . '"
                                                    ') === false
        ) {
            return false;
        }

        $aValues = [
            'main_index' => '0'
        ];
        return $pDbMgr->Update('profit24', 'products_to_sources', $aValues, ' product_id = ' . $iProductId . ' AND 
                                                    source_id = "' . $this->iSourceId . '" AND
                                                    source_index <> "' . $this->aElement['_id'] . '" ');
    }

    public function checkExists($aProduct)
    {

        if (intval($this->aElement['_stock']) == 0) {
            // jeśli stan aktualnej pozycji to 0
            // to dopasowujemy produkt na głównym indeksie
            // ponieważ będzie zmieniał status na niedostępny
            return $this->refCheckProductExists($aProduct, TRUE, TRUE);
        } else {
            // jeśli stock > 0 to, należy sprawdzać nie tylko głowny,
            // ponieważ może zaistnieć potrzeba zmiany głownego indeksu
            // a aktualny główny index będzie inny
            return $this->refCheckProductExists($aProduct, TRUE);
        }
    }

    public function PrepareSynchro()
    {
    }
}// end of seriesElements() Class
