<?php

/**
 * Klasa parsowania elementów ze źródła Super siódemka do Profit24.pl
 *
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */
class productsElements extends CommonSynchro
{
    public $aProduct = array();
    public $aSupportedTags = array();
    public $iSourceId = NULL;
    public $sSoruceSymbol = NULL;

    public function __construct()
    {

        $this->sSoruceSymbol = 'siodemka';

        // Stan na dzień 31.07.2012 r. - pierwsza implementacja
        $this->aSupportedTags = array(
            'A', // głowny tag
            'aid', // ArticleId
            'e', // Ean
            'i', // ISBN
            'pc', // ProducerCode
            'pn', // ProducerName
            'n', // Name
            'd', // Description
            'c', // grupa kategorii
            'cid', // CategoryId
            'auth', // Authors
            'a', // Author
            'p', // Price
            'pw', // PriceWholeSale
            'v', // Vat
            'u', // Unit
            'q', // Quantity
            'sid', // SrcId
            'nov', // Novelty
            'py', //PublicationYear
            'pa', // Pages
            'f', // Format
            't', // Type
            'w', // Weight
            'b', // Bestseller
            'r', // Recommended
            'co', // Cover
            'img', // FileName

        );

        $this->iSourceId = $this->getSourceId('siodemka');
    }

    public function clearProductData()
    {
        unset($this->aProduct);
    }

    public function postParse()
    {

        if (trim($this->aProduct['isbn_plain']) == '') {
            if ($this->aProduct['_ean'] != '') {
                $this->aProduct['isbn'] = isbn2plain($this->aProduct['_ean']);
                $this->aProduct['isbn_plain'] = $this->aProduct['isbn']; // isbn2plain f-cja z functions
            } else {
                $this->aProduct['_dont_insert'] = '1';
            }
        }
        /*
        if ($this->aProduct['price_brutto'] > 0.00 && $this->aProduct['vat'] > 0) {
            $this->aProduct['price_netto'] = Common::formatPrice2($this->aProduct['price_brutto']/(1+$this->aProduct['vat']/100));
        } else {
            return false;
        }
         */
        if ($this->aProduct['price_netto'] > 0.00 && $this->aProduct['vat'] > 0) {
            $this->aProduct['price_brutto'] = Common::formatPrice2($this->aProduct['price_netto'] * (1 + $this->aProduct['vat'] / 100));
        } else {
            return false;
        }

        return $this->aProduct;
    }

    public function setAid(&$oXml)
    {
//		$this->aProduct['_id'] = $this->getTextNodeValue($oXml);
    }

    public function setI(&$oXml)
    {
        $sIsbn = $this->getTextNodeValue($oXml);
        if ($sIsbn != '') {
            $this->aProduct['isbn'] = isbn2plain($sIsbn);
            $this->aProduct['isbn_plain'] = $this->aProduct['isbn']; // isbn2plain f-cja z functions
        }
    }

    public function setPc(&$oXml)
    {
        // Kod wydawcy - nie potrzebny
    }

    public function setPn(&$oXml)
    {
        // czyścimy ze slashy jakiś dziwnych bo ponieważ iż
        //	- http://stackoverflow.com/questions/3235219/urlencoded-forward-slash-is-breaking-url
        $this->aProduct['_publisher'] = str_replace('/', '', $this->getTextNodeValue($oXml));
    }

    public function setN(&$oXml)
    {
        $this->aProduct['name'] = $this->getTextNodeValue($oXml);
        $this->aProduct['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aProduct['name'], ' ')));
    }

    public function setD(&$oXml)
    {

        $sDesc = eregi_replace('<a.*</a>', '', $this->clearAsciiCrap($this->getTextNodeValue($oXml)));
        $sDesc = eregi_replace('<script.*</script>', '', $sDesc);
        $this->aProduct['description'] = $sDesc;
        $this->aProduct['short_description'] = $sDesc;
    }

    public function setC(&$oXml)
    {
        // grupa kategorii ->setCid
    }

    public function setCid(&$oXml)
    {

        $iCatId = $this->getTextNodeValue($oXml);
        if ($iCatId > 0) {
            $this->aProduct['_items_category'][$iCatId] = '1';
            if ($iCatId == '2') {// kategoria o id 2 na dzien dzisiejszy to audiobook, brak innego oznaczenia
                $this->aProduct['type'] = '1';
                $this->aProduct['product_type'] = \LIB\EntityManager\Entites\Product::TYPE_AUDIO;
            }
        } else {
            return false;
        }
    }

    public function setAuth(&$oXml)
    {
        // grupa kategorii ->setCid
    }

    public function setA(&$oXml, $bMainTagBook)
    {

        if ($bMainTagBook === false) {
            // jeśli nie jest to gówny tag produktów, tylko tag autora
            $sAuthName = $this->getTextNodeValue($oXml);
            if ($sAuthName != '') {
                $this->aProduct['_items_authors'] .= ', ' . $sAuthName;
            } else {
                return false;
            }
        }
    }

    public function setP(&$oXml)
    {
        $this->aProduct['price_netto'] = Common::formatPrice2($this->getTextNodeValue($oXml));
    }

    public function setPw(&$oXml)
    {
        /*
            $fWholesalePrice = (float)$this->getTextNodeValue($oXml);
            if ($fWholesalePrice > 0.00) {
                $this->aProduct['_wholesale_price'] = $fWholesalePrice;
            }
         */
    }

    public function setV(&$oXml)
    {
        $aMatches = array();

        $sVat = $this->getTextNodeValue($oXml);
        preg_match("/^(\d+)/", $sVat, $aMatches);
        $iVatt = intval($aMatches[1]);
        if ($iVatt > 0) {
            $this->aProduct['vat'] = $iVatt;
        } else {
            return false;
        }
    }

    public function setU(&$oXml)
    {
        $sUnit = $this->getTextNodeValue($oXml);
        if ($sUnit != 'szt.') {
            // jeśli nie w sztukach to w czym ??
            $this->addErrorLog("Dziwna jednosta Unit tag 'U' w produkcie o id producenta " . $this->aProduct['_id'] . " wartość: " . $sUnit);
            return false;
        }
    }

    public function setQ(&$oXml)
    {
        // XXX TODO do zaimplementowania ilości produktów

        /*
            Brak - 0 / 1
            Końcówka - poniżej 10
            Średnio - więcej niż 10, poniżej 30
            Dużo - powyżej 30
         */
        /*
         * Stany ilości z statusElements.class.php
         *
            $mQuantity = $this->getTextNodeValue($oXml);
            $this->aProduct['_stock'] = $mQuantity;
            if ($mQuantity == 'brak' || (is_numeric($mQuantity) && $mQuantity == 0) || $mQuantity == '') {
                // brak produktu
                $this->aProduct['prod_status'] = '0';
            } elseif ($mQuantity == 'końcówka') {
                $this->aProduct['prod_status'] = '1';
            } elseif ($mQuantity == 'średnio') {
                $this->aProduct['prod_status'] = '1';
            } elseif ($mQuantity == 'dużo') {
                $this->aProduct['prod_status'] = '1';
            } elseif (intval($mQuantity) > 0) {
                $this->aProduct['prod_status'] = '1';
            } else {
                // jeśli nie w sztukach to w czym ??
                $this->addErrorLog("Dziwna liczeność quantity tag 'Q' w produkcie o id producenta ".$this->aProduct['_id']." wartość: ".$mQuantity);
                return false;
            }
         */
    }

    public function setPy(&$oXml)
    {
        $aMatches = array();

        $sYear = $this->getTextNodeValue($oXml);
        preg_match("/^(\d{4}+)/", $sYear, $aMatches);
        $iPYear = intval($aMatches[1]);
        if ($iPYear > 0 && mb_strlen($iPYear, 'UTF-8') == 4) {
            $this->aProduct['publication_year'] = $iPYear;
        } else {
            $this->addErrorLog("Nieprawidłowy rok w produkcie o id producenta " . $this->aProduct['_id'] . " isbn: " . $this->aProduct['isbn'] . " wartość: " . $sYear);
            return false;
        }
    }

    public function setPa(&$oXml)
    {

        $iPages = intval($this->getTextNodeValue($oXml));
        if ($iPages > 0) {
            $this->aProduct['pages'] = $iPages;
        }
    }

    public function setCo(&$oXml)
    {

        $sBinding = $this->getTextNodeValue($oXml);
        if ($sBinding != '') {
            $this->aProduct['_binding'] = $sBinding;
        }
    }

    public function setT(&$oXml)
    {

        $sType = $this->getTextNodeValue($oXml);
        // importujemy wszystko
    }

    public function setImg(&$oXml)
    {

        $sImg = $this->getTextNodeValue($oXml);
        if ($sImg != '') {
            $this->aProduct['_img'] = $sImg;
        }
    }

    public function setW(&$oXml)
    {

        $iWeight = (double)$this->getTextNodeValue($oXml);
        if ($iWeight) {
            $this->aProduct['weight'] = (float)('0.' . $iWeight);
        }
    }

    public function setF(&$oXml)
    {
        // nic nie rób
    }

    public function setE(&$oXml)
    {
        // nic nie rób
        $sEAN = isbn2plain($this->getTextNodeValue($oXml));
        if (mb_strlen($sEAN, 'UTF-8') < 13) {
            $this->aProduct['ean_13'] = sprintf('%013s', $sEAN);
            $this->aProduct['_ean'] = isbn2plain($sEAN);
        }
        if ($sEAN != '' && strlen($sEAN) == 13) {
            $this->aProduct['ean_13'] = $sEAN;
        } else {
            $this->aProduct['_ean'] = isbn2plain($sEAN);
        }
    }

    public function setR(&$oXml)
    {
        // nic nie rób
    }

    public function setB(&$oXml)
    {
        // nic nie rób
    }

    public function setNov(&$oXml)
    {
        // nic nie rób
    }


    public function setSid(&$oXml)
    {
        // nic nie rób
        $this->aProduct['_id'] = $this->getTextNodeValue($oXml);
    }


    /**
     * Metoda pobiera id źródła dla danego importu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param string $sSourceSymbol - symbol źródła
     * @return type
     */
    private function getSourceId($sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "sources
						 WHERE symbol='" . $sSourceSymbol . "'";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceId() method


    /**
     * Trochę przydługa metoda zapisywania zdjęcia do produktu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param type $iProductId
     * @param type $iSourceBookId
     * @param type $aProduct
     * @return boolean
     */
    public function addImage($iProductId, $iSourceBookId, $aProduct)
    {
        global $aConfig, $pDbMgr;

        // XXX TODO USUNAC
//    return true;

        $this->createProductImageDir($iProductId);
        $iRange = intval(floor($iProductId / 1000)) + 1;
        $sDestDir = $aConfig['common']['base_path'] . 'images/photos/okladki/' . $iRange . '/' . $iProductId;
        $sShortDir = 'okladki/' . $iRange . '/' . $iProductId;

        $sDstName = $aProduct['isbn_plain'] . ".jpg";
        $sSrcFile = $sDestDir . "/_" . $sDstName;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "products_images
						 WHERE product_id=" . $iProductId . " AND photo='" . $sDstName . "' AND name='" . $sDstName . "'";
        $iId = $pDbMgr->GetOne('profit24', $sSql);
        if ($iId > 0) {
            // zdjecie juz istnieje w bazie nie dodajemy ponownie
            return true;
        }

        $rFp = fopen($sSrcFile, "w");
        $bIsErr = !$this->getSourceFile($rFp, $aProduct['_img']);
        fclose($rFp);


        if ($bIsErr == true || !file_exists($sSrcFile))
            return false;

        $aFileInfo = getimagesize($sSrcFile);
        $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_' . $sDstName);
        $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
        $aSmallInfo = getimagesize($sDestDir . '/' . $sDstName);
        if ($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])) {
            $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_' . $sDstName);
        }
        unlink($sSrcFile);

        $aValues = array(
            'product_id' => $iProductId,
            'directory' => $sShortDir,
            'photo' => $sDstName,
            'mime' => 'image/jpeg',
            'name' => $sDstName,
            'order_by' => '1'
        );
        return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix'] . "products_images", $aValues, '', false) != false);
    }// end of addImage() method


    /**
     * Metoda importuje XML'a i zapisuje na dysku
     *
     * @return string - adres pliku XML z danymi
     */
    public function importSaveXMLData()
    {
        global $aConfig;

        $sFileName = date("siH_dmY") . '_products.XML';
        $sFilePath = $aConfig['common']['base_path'] . $aConfig['common']['import_dir'] . "/XML/siodemka/" . $sFileName;

        $rFp = fopen($sFilePath, "w");
        $bIsErr = $this->getSourceFile($rFp, 'AllArticles.xml');
        fclose($rFp);

        if ($bIsErr == true) {
            return $sFilePath;
        } else {
            return false;
        }
    }// end of importSaveXMLData() method


    /**
     * Metoda pobiera ze źródła plik
     *
     * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
     * @param string $sFile - nazwa pliku który chcemy pobrać
     * @return boolean
     */
    protected function getSourceFile(&$rFileHandle, $sFile)
    {

        $sLogin = 's7-PrO31T';
        $sPassword = 'Lo6oW4n13';
        $sQuery = "ftp://" . $sLogin . ":" . $sPassword . "@62.129.242.58/" . $sFile;

        if ($rFileHandle) {
            $oCURL = curl_init();
            curl_setopt($oCURL, CURLOPT_URL, $sQuery);
            curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
            curl_setopt($oCURL, CURLOPT_POST, FALSE);
            curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
            curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

            if (curl_exec($oCURL) === false) {
                curl_close($oCURL);
                return false;
            }
            curl_close($oCURL);
            return true;
        }
        return false;
    }

    public function proceedInsert(&$oParent, $aProduct, $iSourceBId)
    {
        $aAttachment = $this->_getAttachmentBySiodemkaIndex($this->aProduct['_id']);
        if (!empty($aAttachment)) {
            // ok powiązany załącznik
            $this->_checkUpdateAttachment($aAttachment);
        } else {
            // nie chcemy takich które mogą być załącznikiem i nie spełniają podstowych wymagan
            if ($this->aProduct['_dont_insert'] != '1') {
                $oParent->proceedInsertProduct($aProduct, $iSourceBId);
            }
        }
    }

    /**
     * Metoda przeszukuje w poszukiwaniu załącznika o podanym indeksie siodemki
     *
     * @global type $pDbMgr
     * @param type $sSiodemkaIndex
     * @return type
     */
    private function _getAttachmentBySiodemkaIndex($sSiodemkaIndex)
    {
        global $pDbMgr;

        $sSql = 'SELECT * 
             FROM products_attachments
             WHERE siodemka_index = "' . $sSiodemkaIndex . '"';
        return $pDbMgr->GetRow('profit24', $sSql);
    }


    /**
     * Metoda sprawdza czy zaktualizować załącznik, jeśli tak to aktualizje
     *
     * @global type $pDbMgr
     * @param type $aAttachment
     * @return boolean
     */
    private function _checkUpdateAttachment($aAttachment)
    {
        global $pDbMgr;

        // czy jset inna cena netto i brutto lub vat
        if (floatval($aAttachment['price_netto']) != floatval($this->aProduct['price_netto']) ||
            floatval($aAttachment['price_brutto']) != floatval($this->aProduct['price_brutto']) ||
            floatval($aAttachment['vat']) != floatval($this->aProduct['vat'])
        ) {
            $aAttachmentValues = array(
                'price_netto' => $this->aProduct['price_netto'],
                'price_brutto' => floatval($this->aProduct['price_brutto']),
                'vat' => $this->aProduct['vat'],
            );

            $pDbMgr->Update('profit24', 'products_attachments', $aAttachmentValues, ' id = ' . $aAttachment['id']);
        }
        return true;
    }

    public function proceedUpdate(&$oParent, &$aProduct, $iSourceBId, $iProductId)
    {

        $aAtt = $this->getAttachment($iProductId);

        $aAttachment = $this->_getAttachmentBySiodemkaIndex($this->aProduct['_id']);
        if (!empty($aAttachment)) {
            // ok powiązany załącznik
            $this->_checkUpdateAttachment($aAttachment);
        } else {
            if (!empty($aAtt) && $aAtt['price_brutto'] > 0) {
                $this->aProduct['price_brutto'] += $aAtt['price_brutto'];
                $aProduct['price_brutto'] += $aAtt['price_brutto'];
                $this->aProduct['price_netto'] += $aAtt['price_netto'];
                $aProduct['price_netto'] += $aAtt['price_netto'];
            }
            // nie chcemy takich które mogą być załącznikiem i nie spełniają podstowych wymagan
            //if ($this->aProduct['_force_attachment'] != '1') {
            // dodaj bookindeks dla produktu
            $sMD5 = $this->getProductMD5($aProduct);
            $mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);

            unset($this->aProduct['_stock']);
            unset($aProduct['prod_status']); // to niech się ustawia ze skryptu aktualizacji stanów
            // dodać aktualizację cen

            // przed aktualizacją sprawdźmy czy ten produkt jest na głownym indeksie, zmieniamy go jeśli własnie jest to główny indeks

            if ($this->refCheckProductExists($aProduct, TRUE, TRUE) > 0) {

                $this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aProduct, true);
                if ($this->checkUpdateProduct($aProduct, $mRetMD5, $iProductId)) {
                    $this->proceedUpdateProduct($aProduct, $iProductId, $iSourceBId);
                }
            }
            //}
        }
    }

    public function checkExists(&$aProduct)
    {
        return $this->refCheckProductExists($aProduct, TRUE);
    }
}// end of productsElements() Class
