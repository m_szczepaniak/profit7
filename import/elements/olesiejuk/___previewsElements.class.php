<?php
/**
 * UWAGA MODUL NIE JEST UZYWANY PONIEWAZ OLESIEJUK NIE WYSTAWIA STAWKI VAT PRODUKTU
 * 
 * Klasa parsowania elementu Zapowiedź ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class previewsElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'olesiejuk';
		
		// Stan na dzień 04.09.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'tytul',
				'autor',
				'cena',
				'datawyd',
				'wydawnictwo',
				'opis',
				'ean',
				'isbn',
				'okladka'
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
	}

	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {
		
		$this->aElement['vat'] = 5; // TODO USUNAC
		$this->aElement['page_id'] = '1'; // TODO ZAPYTAĆ GDZIE TO DODAWAĆ
		$this->aElement['prod_status'] = '3';
		if ($this->aElement['price_brutto'] > 0.00 && $this->aElement['vat'] > 0) {
			$this->aElement['price_netto'] = Common::formatPrice2($this->aElement['price_brutto']/(1+$this->aElement['vat']/100));
		} else {
			return false;
		}
		
		if (($this->aElement['price_brutto'] != '' || $this->aElement['ean'] != '') &&
				$this->aElement['price_brutto'] > 0.00 && $this->aElement['price_netto']) {
			return $this->aElement;
		}
		
		return false; // coś jest nie tak
	}
	
	public function setTytul($sValue) {
		
		$this->aElement['name'] = $this->getText($sValue);
		$this->aElement['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aElement['name'], ' ')));
	}
	
	public function setEan($sValue) {
		
		$this->aElement['ean_13'] = $this->getText($sValue);
		if ($this->aElement['_id'] == '' && $this->aElement['ean_13'] != '') {
			$this->aElement['_id'] = $this->aElement['ean_13'];
		}
	}
	
	public function setIsbn($sValue) {
		
		$sIsbn = $this->getText($sValue);
		
		if ($sIsbn != '') {
			$this->aElement['isbn'] = isbn2plain($sIsbn);
			$this->aElement['isbn_plain'] = $this->aElement['isbn']; // isbn2plain f-cja z functions
		}
	}
	
	public function setAutor($sValue) {
		
		$this->aElement['_items_authors'] = $this->getText($sValue);
	}
	
	public function setCena($sValue) {
		$this->aElement['price_brutto'] = Common::FormatPrice2($this->getText($sValue));
	}
	
	public function setDatawyd($sValue) {
		
		$aMatches = array();
		// w Profit nie ma na to pola
		$bIsMatch = preg_match("/^\d{4}-\d{2}-\d{2}$/", $this->getText($sValue), $aMatches );
		if ($bIsMatch == true && $aMatches[0] != '') {
			$this->aElement['_shipment_date'] = $aMatches[0]; // data premiery
		}
	}
	
	public function setWydawnictwo($sValue) {
		// czyścimy ze slashy jakiś dziwnych ponieważ:
		//	- http://stackoverflow.com/questions/3235219/urlencoded-forward-slash-is-breaking-url
		$this->aElement['_publisher'] = str_replace('/', '', $this->getText($sValue));
	}
	
	public function setOpis($sValue) {
		
		$sDesc = eregi_replace('<a.*</a>','', $this->clearAsciiCrap($this->getText($sValue)));
		$sDesc = eregi_replace('<script.*</script>','', $sDesc);
		
		$sDescTMP = trim(preg_replace("/^'(.*)'$/", '$1', $sDesc));
		if ($sDescTMP != '')  {
			$sDesc = $sDescTMP;
		}
		$this->aElement['description'] = $sDesc;
		$this->aElement['short_description'] = $sDesc;	
	}
	
	public function setOkladka($sValue) {
		
		$sImg = $this->getText($sValue);
		if ($sImg != '') {
			$this->aElement['_img'] = $sImg;
		}
	}
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Trochę przydługa metoda zapisywania zdjęcia do produktu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iProductId
	 * @param type $iSourceBookId
	 * @param type $aElement
	 * @return boolean 
	 */
	public function addImage($iProductId, $iSourceBookId, $aProduct) {
		global $aConfig, $pDbMgr;
		
		$this->createProductImageDir($iProductId);
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
		$sShortDir = 'okladki/'.$iRange.'/'.$iProductId;
		
		$sDstName = $aProduct['isbn_plain'].".jpg";
		$sSrcFile = $sDestDir."/_".$sDstName;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id=".$iProductId." AND photo='".$sDstName."' AND name='".$sDstName."'";
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if ($iId > 0) {
			// zdjecie juz istnieje w bazie nie dodajemy ponownie
			return true;
		}
		
		/*
		$rFp = fopen($sSrcFile, "w");
		$bIsErr = !$this->getSourceFile($rFp, $aProduct['_img']);
		fclose ($rFp);
		*/
		file_put_contents($sSrcFile, file_get_contents($aProduct['_img']));
		

		if(!file_exists($sSrcFile))
			return false;

		$aFileInfo = getimagesize($sSrcFile);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
		$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
		if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
			$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
		}
		unlink($sSrcFile);

		$aValues=array(
			'product_id' => $iProductId,
			'directory' => $sShortDir,
			'photo' => $sDstName,
			'mime' => 'image/jpeg',
			'name' => $sDstName,
			'order_by' => '1'
		);
		return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
	}// end of addImage() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_zapowiedzi.csv';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/olesiejuk/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'zapowiedzi.csv');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		
		$sLogin = 'ksiegarniaprofit';
		$sPassword = '08profit2012ks';
		$sQuery = "ftp://".$sLogin.":".$sPassword."@195.42.113.31/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aElement, $iSourceBId) {
		return $oParent->proceedInsertProduct($aElement, $iSourceBId);
	}
	
	
	public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId) {
		// aktualizacja
		$sMD5 = $this->getProductMD5($aElement);
		$mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);

		if ($aElement['ean_13'] != '') {
			$this->updateEAN($iProductId, $aElement['ean_13']);
		}
		
		//unset($aElement['prod_status']); - jeśli jest w tym skrypcie to produkt jest dostępny
		// dodać aktualizację cen
		
		// aktualizujemy produkt
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aElement, true);
		if ($this->checkUpdateProduct($aElement, $mRetMD5, $iProductId)) {
			$this->proceedUpdateProduct($aElement, $iProductId, $iSourceBId);
		}
	}
	
	public function checkExists($aElement) {
		return $this->refCheckProductExists($aElement);
	}
	
	public function PrepareSynchro() {
		global $aConfig, $pDbMgr;
	}
}// end of previewsElements() Class
?>
