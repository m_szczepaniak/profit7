<?php
/**
 * Klasa parsowania elementu dostępność ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class availableElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'olesiejuk';
		
		// Stan na dzień 14.08.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'ean',
				'cena_C',
				'biblioteka'
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
	}

	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {
		
    $this->aElement['prod_status'] = '0';
		if ($this->aElement['ean_13'] != '' || $this->aElement['isbn_10'] != '' || $this->aProduct['isbn'] != '') {
			return $this->aElement;
		}
		return false; // coś jest nie tak
	}
	
	public function setEan($sValue) {
		$sEan = $this->getText($sValue);
			
		if (strlen($sEan) == 10) {
			$this->aElement['isbn_10'] = $sEan;
		} elseif (strlen($sEan) == 13) {
			$this->aElement['ean_13'] = $sEan;
		} else {
			$this->aProduct['isbn'] = isbn2plain($sEan);
			$this->aProduct['isbn_plain'] = $this->aProduct['isbn']; 
		}
		$this->aElement['_id'] = $sEan;
	}
	
	public function setCena_C($sValue) {
		$this->aElement['_stock'] = $this->getText($sValue);
	}
	
	public function setBiblioteka($sValue) {
		// UWAGA NIE IMPORTUJEMY JEŚLI JEST TU 1
		if (intval($this->getText($sValue)) >= 1) {
			$this->aElement['prod_status'] = '0';
		}
	}
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_produkty_dostepne.csv';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/olesiejuk/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'produkty_dostepne.csv');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		
		$sLogin = 'ksiegarniaprofit';
		$sPassword = '08profit2012ks';
		$sQuery = "ftp://".$sLogin.":".$sPassword."@195.42.113.31/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aElement, $iSourceBId) {
		global $aConfig, $pDbMgr;
	}
	
	
	public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId) {
		global $aConfig, $pDbMgr;
		
		// aktualizacja
		$sNewStatus = $this->parseProductStock($this->sSoruceSymbol, $this->aElement['_stock']);
		$aValues = array(
				'olesiejuk_last_import' => 'NOW()',
				'olesiejuk_stock' => $sNewStatus != '' ? $sNewStatus : 'NULL'
		);
		return $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aValues, " id=".$iProductId);
	}
	
	public function checkExists($aElement) {
		return $this->refCheckProductExists($aElement);
	}
	
	public function PrepareSynchro() {}
}// end of bindingsElements() Class
?>
