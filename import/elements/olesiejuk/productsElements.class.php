<?php
/**
 * Klasa parsowania elementu Produkt ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class productsElements extends CommonSynchro {
	public $aElement = array();
	public $aProduct = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	protected $aBindingsDB = NULL;
	
	const REDUCTION_RATE = 1.333;
  const ADDITIONAL_DISCOUNT = 8;
	
	final public function __construct() {
		
		$this->sSoruceSymbol = 'olesiejuk';
		
		// Stan na dzień 14.08.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'kod', // to unikalny numer nadawany kolejno przy zapisywaniu książki do bazy
				'autor', // id autora
				'tytul', // tytuł książki
				'wydawca', // id wydawcy
				'rok_wydania', // rok wydania książki
				'cena_zakupu', // cena zakupu
				'cena_hurtowa', // cena hurtowa sprzedaży (netto)
				'cena_detaliczna', // cena detaliczna
				'cena_sprzedazy',
				'VAT', // stawka VAT
				'opis', //opis książki (krótkie streszczenie)
				'stron', // ilość stron
				'format', // id formatu książki
				'WAGA', // waga
				'oprawa', //  id oprawy
				'j_miary', // id jednostki
				'data_wprowadzenia', //  data wprowadzenia książki do bazy
				'dodatki', // wpis w polu "dodatek" odsyła do słownika "dodatki.csv" ale tylko w odniesieniu do dodatków bezpłatnie dodawanych do książek
				'isbn', // numer ISBN
				'okladka', // powiązany plik z okładką książki (okladka.jpg)
				'dzial1', // książka może należeć do określonych działów (np. dla dzieci, powieść)
				'dzial2', // książka może należeć do określonych działów (np. dla dzieci, powieść)
				'dzial3', // książka może należeć do określonych działów (np. dla dzieci, powieść)
				'seria', // id serii; jeżeli książka należy do serii książek
				'uwagi',
				'okazja',
				'nowosc',
				'hit_dnia',
				'polecamy',
				'zapowiedz',
				'NR_KATAL', //numer EAN
				'cena_C', // (mały zapas) flaga "0" w tym polu oznacza produkt dostępny w ilości sztuk > 10, w przeciwnym razie niski zapas magazynowy
				'ile_dni', // ilość dni jakie upłynęły od momentu wprowadzenia książki do bazy
				'barkod', // kod kreskowy
				'PKWIU', // Polska Klasyfikacja Wyrobów i Usług
				'tania',
				'cena_tania',
				'zwrot', //  flaga "1" w tym polu oznacza, że te pozycje oferujemy bez prawa zwrotu
				'ID_ARTYKULU', // identyfiaktor artykułu
				'biblioteka' // flaga "1" w tym polu oznacza, że pozycja oferowana jest wyłącznie bibliotekom
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
	}

	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {
		global $aConfig, $pDbMgr;
		// przygotujmy dane do synchronizacji
		

		if ($this->aElement['_id'] <= 0) return false;
		
		// oprawa
		if (!isset($this->aBindingsDB)) {
			$sSql = "SELECT id, nazwa AS name FROM ".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_bindings";
			$this->aBindingsDB = $pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC, false);
		}
		if (isset($this->aBindingsDB[$this->aElement['_binding']]) && $this->aBindingsDB[$this->aElement['_binding']] != '') {
			$this->aElement['_binding'] = $this->aBindingsDB[$this->aElement['_binding']];
		}
		
		if (intval($this->aElement['_series']) > 0) {
			$sSql = "SELECT nazwa as name FROM ".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_series
							 WHERE id=".$this->aElement['_series'];
			$this->aElement['_items_series'] = $pDbMgr->GetCol('profit24', $sSql);
		}
		
		if (intval($this->aElement['_publisher']) > 0) {
			$sSql = "SELECT nazwa as name FROM ".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_publishers
							 WHERE id=".$this->aElement['_publisher'];
			$this->aElement['_publisher'] = $pDbMgr->GetOne('profit24', $sSql);
		}
		
		// attachments
		$sSql = "SELECT B.* FROM ".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_products_attachments AS A
						 JOIN ".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_attachments AS B
							 ON B.id = A.attachment_id
						 WHERE A.product_id=".$this->aElement['_id'];
		$this->aElement['_attachments'] = $this->parseAttachments($pDbMgr->GetAll('profit24', $sSql));
		
		if ($this->aElement['_img'] != '') {
			// okladki
			$sSql = "SELECT dir, filename FROM ".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_images
							 WHERE filename='".$this->aElement['_img'].'.jpg'."'";
			$aImage = $pDbMgr->GetRow('profit24', $sSql);
			
			if (!empty($aImage) && $this->aElement['_img'] != '') {
				$this->aElement['_img'] = '/OKLADKI/'.$aImage['dir'].'/'.$aImage['filename'];
			} else {
				unset($this->aElement['_img']);
			}
		}
		
		// przelicz cenę detaliczną brutto
		if ($this->aElement['price_brutto'] <= 0.00 && 
				$this->aElement['vat'] > 0 && 
				$this->aElement['_wholesale_price_netto'] > 0.00) {
			$this->aElement['price_brutto'] = $this->calculateDetailPriceBrutto($this->aElement['_wholesale_price_netto'], $this->aElement['vat']);
		}
		
		if ($this->aElement['_wholesale_price_netto'] > 0.00 && $this->aElement['vat'] > 0) {
			// zmienione
			$this->aElement['_wholesale_price'] = Common::formatPrice2($this->aElement['_wholesale_price_netto']*(1+$this->aElement['vat']/100));
      // należy odjąć jeszcze "8%"
      $this->aElement['_wholesale_price'] = Common::formatPrice2($this->aElement['_wholesale_price'] - Common::formatPrice2($this->aElement['_wholesale_price'] * (self::ADDITIONAL_DISCOUNT/100)));
		}
		
		// przelicz cenę detaliczną netto
		if ($this->aElement['price_brutto'] > 0.00 && $this->aElement['vat'] > 0) {
      $this->aElement['price_netto'] = Common::formatPrice2($this->aElement['price_brutto']/(1+$this->aElement['vat']/100));
		} else{
			// brak ceny lub vatu
			return false;
		}
    if ($this->aElement['_stock'] == '2' || $this->aElement['_stock'] == '1') {
      $this->aElement['prod_status'] = '0'; // wszystkie w ole1.csv są dostępne
    } else {
      $this->aElement['prod_status'] = '1'; // wszystkie w ole1.csv są dostępne
    }
		$this->aElement['shipment_time'] = '1';
		if ($this->aElement['name'] != '' && $this->aElement['_id'] > 0 && $this->aElement['price_brutto'] > 0.00) {
			return $this->aElement;
		}
		
		return false; // coś jest nie tak
	}
	
	
	/**
	 * Metoda oblicza cenę detaliczną Netto na podstawie ceny hurtowej netto i wskaźnika redukcji (rabatu)
	 * 
	 * @param float $fWholsalePriceNetto
	 * @return float
	 */
	function calculateDetailPriceNetto($fWholsalePriceNetto) {
		$fWholsalePriceNetto = Common::formatPrice2($fWholsalePriceNetto);
		
		return Common::formatPrice2($fWholsalePriceNetto*self::REDUCTION_RATE);
	}// end of calculateDetailPriceNetto() method
	
	
	/**
	 * Metoda przelicza cenę detaliczną brutto na podstawie ceny hurtowej netto 
	 * i przy użyciu funkcji obliczania ceny detalicznej netto - calculateDetailPriceNetto();
	 * 
	 * 
	 * @param float $fWholsalePriceNetto
	 * @param int $iVat
	 * @return float
	 */
	function calculateDetailPriceBrutto($fWholsalePriceNetto, $iVat) {
		$iVat = intval($iVat);
		$fWholsalePriceNetto = Common::formatPrice2($fWholsalePriceNetto);
		
		$fDetailPriceNetto = $this->calculateDetailPriceNetto($fWholsalePriceNetto);
		$fDetailPriceBrutto = $fDetailPriceNetto * ((100 + $iVat) / 100);
		return number_format((double)$fDetailPriceBrutto, 1, '.', '');
	}// end of calculateDetailPriceBrutto() method
	
	
	/**
	 * Metoda parsuje tablice załączników produktów darmowych i płatnych
	 * 
	 * @param array $aAttachments
	 * @return array
	 */
	public function parseAttachments($aAttachments) {
		$aNewArr = array();
		
		foreach ($aAttachments as $iKey => $aAtt) {
			if ($aAtt['vat'] > 0.00 && $aAtt['cena'] > 0.00) {
				$aNewArr[$iKey]['price_brutto'] = Common::formatPrice2($aAtt['cena'] * (1 + ($aAtt['vat'] / 100)));
			} else {
				$aNewArr[$iKey]['price_brutto'] = 0.00;
			}
			$aNewArr[$iKey]['price_netto'] = $aAtt['cena'];
			$aNewArr[$iKey]['vat'] = $aAtt['vat'];
		}
		return $aNewArr;
	} 
	
	public function setKod($sValue) {
		$this->aElement['_id'] = intval($this->getText($sValue));
	}
	
	public function setAutor($sValue) {
		$sAuthors = $this->getText($sValue);
		if (!empty($sAuthors) && $sAuthors != '') {
			$this->aElement['_items_authors'] = $sAuthors;//= explode(', ', $sAuthors);
		}
	}
	
	public function setTytul($sValue) {
		$this->aElement['name'] = $this->getText($sValue);
    $this->aElement['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aElement['name'], ' ')));
	}
	
	public function setWydawca($sValue) {
		$iPublisher = intval($this->getText($sValue));
		if ($iPublisher > 0) {
			$this->aElement['_publisher'] = $this->getText($sValue);
		}
	}
	
	public function setRok_wydania($sValue) {
		$iPYear = intval($this->getText($sValue));
		if ($iPYear > 0) {
			$this->aElement['publication_year'] = $iPYear;
		}
	}
	
	public function setCena_zakupu($sValue) {}
	
	public function setCena_hurtowa($sValue) {
		// XXX netto - chyba lipa
		$this->aElement['_wholesale_price_netto'] = Common::FormatPrice2($this->getText($sValue));
	}

	public function setCena_detaliczna($sValue) {
		$this->aElement['price_brutto'] = Common::FormatPrice2($this->getText($sValue));
	}
	
	public function setCena_sprzedazy($sValue) {}
	
	public function setVat($sValue) {
		
		$iVatt = intval($this->getText($sValue));
		if ($iVatt > 0) {
			$this->aElement['vat'] = $iVatt;
		} else {
			return false;
		}
	}
	
	public function setOpis($sValue) {
		// TODO tutaj trzeba trochę usunąć te ciapki
		$sDesc = eregi_replace('<a.*</a>','', $this->clearAsciiCrap($this->getText($sValue)));
		$sDesc = eregi_replace('<script.*</script>','', $sDesc);
		$this->aElement['description'] = $sDesc;
		$this->aElement['short_description'] = $sDesc;			
	}
	
	public function setStron($sValue) {
		
		$iPages = intval($this->getText($sValue));
		if ($iPages > 0) {
			$this->aElement['pages'] = $iPages;
		}		
	}
	
	public function setFormat($sValue) {}
	
	public function setWaga($sValue) {
		
		$iWeight = (double)($this->getText($sValue) / 1000);
		if ($iWeight > 0.00) {
			$this->aElement['weight'] = $iWeight;
		}
	}	
	
	public function setOprawa($sValue) {
		$iWeight = intval($this->getText($sValue));
		if ($iWeight > 0) {
			$this->aElement['_binding'] = $iWeight;
		}
	}	
	
	public function setJ_miary($sValue) {}	
	
	public function setData_wprowadzenia($sValue) {}	
	
	public function setDodatki($sValue) {
		// bezpłatne dodatki 
		$aAtrributes = explode(',', $this->getText($sValue));
		if (!empty($aAtrributes) && is_array($aAtrributes) && !empty($sValue))  {
			$this->aElement['_free_attachments'] = $aAtrributes;
		}
	}	
	
	public function setIsbn($sValue) {
		$sIsbn = $this->getText($sValue);
		
		if ($sIsbn != '') {
			$this->aElement['isbn'] = isbn2plain($sIsbn);
			$this->aElement['isbn_plain'] = $this->aElement['isbn']; // isbn2plain f-cja z functions
		}
	}	
	
	public function setOkladka($sValue) {
		
		$sImg = $this->getText($sValue);
		if ($sImg != '') {
			$this->aElement['_img'] = $sImg;
		}
	}	
	
	public function setDzial1($sValue) {
		$iCat = intval($this->getText($sValue));
		if ($iCat > 0) {
			$this->aElement['_items_category'] = array($iCat => '1');
			if ($iCat === 49 || $iCat === 213) {
				$this->aElement['type'] = '1';
			}
      if ($iCat === 199) {
        // nie importujemy zabawek
        unset($this->aElement); 
      }
		}
	}	
	
	public function setDzial2($sValue) {
		$iCat = intval($this->getText($sValue));
		if ($iCat > 0) {
			$this->aElement['_items_category'][$iCat] = '1';
			if ($iCat === 49 || $iCat === 213) {
				$this->aElement['type'] = '1';
			}
      if ($iCat === 199) {
        unset($this->aElement); 
      }
		}
	}	
	
	public function setDzial3($sValue) {
		$iCat = intval($this->getText($sValue));
		if ($iCat > 0) {
			$this->aElement['_items_category'][$iCat] = '1';
			if ($iCat === 49 || $iCat === 213) {
				$this->aElement['type'] = '1';
			}
      if ($iCat === 199) {
        unset($this->aElement); 
      }
		}
	}	
	
	public function setSeria($sValue) {
		$iSeries = intval($this->getText($sValue));
		if ($iSeries > 0) {
			$this->aElement['_series'] = $iSeries;
		}
	}	
	
	public function setUwagi($sValue) {}	
	
	public function setOkazja($sValue) {}	
	
	public function setNowosc($sValue) {}	
	
	public function setHit_dnia($sValue) {}	
	
	public function setPolecamy($sValue) {}	
	
	public function setZapowiedz($sValue) {
		// XXX TODO zapowiedzi
	}	
	
	public function setNr_katal($sValue) {
		$sEan = $this->getText($sValue);
		if ($sEan != '') {
			$this->aElement['ean_13'] = $sEan;
		}
	}	
	
	public function setCena_c($sValue) {
		// tutaj ilość, dostępność
		// (mały zapas) flaga "0" w tym polu oznacza produkt dostępny w ilości sztuk > 10, w przeciwnym razie niski zapas magazynowy
		$this->aElement['_stock'] = $this->getText($sValue);
	}	
	
	public function setIle_dni($sValue) {}	
	
	public function setBarkod($sValue) {}	
	
	public function setPkwiu($sValue) {
    
    $sPKWiU = trim((string) $this->getText($sValue));
    if (intval(preg_match("/^58\..*/", $sPKWiU)) <= 0 && 
        intval(preg_match("/^59\..*/", $sPKWiU)) <= 0 &&
        intval(preg_match("/^22\..*/", $sPKWiU)) <= 0
            ) {
      unset($this->aElement); 
    }
  }	
	
	public function setTania($sValue) {}	
	
	public function setCena_tania($sValue) {}	
	
	public function setZwrot($sValue) {}	
	
	public function setId_artykulu($sValue) {}	
	
	public function setBiblioteka($sValue) {
		// UWAGA NIE IMPORTUJEMY JEŚLI JEST TU 1
		if (intval($this->getText($sValue)) >= 1) {
			unset($this->aElement);
		}
	}
	
	
	/**
	 * Trochę przydługa metoda zapisywania zdjęcia do produktu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iProductId
	 * @param type $iSourceBookId
	 * @param type $aProduct
	 * @return boolean 
	 */
	public function addImage($iProductId, $iSourceBookId, $aProduct) {
		global $aConfig, $pDbMgr;
		
		$this->createProductImageDir($iProductId);
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
		$sShortDir = 'okladki/'.$iRange.'/'.$iProductId;
		
		$sDstName = $aProduct['isbn_plain'].".jpg";
		$sSrcFile = $sDestDir."/_".$sDstName;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id=".$iProductId." AND photo='".$sDstName."' AND name='".$sDstName."'";
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if ($iId > 0) {
			// zdjecie juz istnieje w bazie nie dodajemy ponownie
			return true;
		}
		
		$rFp = fopen($sSrcFile, "w");
		$bIsErr = !$this->getSourceFile($rFp, $aProduct['_img']);
		fclose ($rFp);
		

		if($bIsErr == true || !file_exists($sSrcFile))
			return false;

		$aFileInfo = getimagesize($sSrcFile);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
		$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
		if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
			$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
		}
		unlink($sSrcFile);

		$aValues=array(
			'product_id' => $iProductId,
			'directory' => $sShortDir,
			'photo' => $sDstName,
			'mime' => 'image/jpeg',
			'name' => $sDstName,
			'order_by' => '1'
		);
		return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
	}// end of addImage() method
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_products.csv';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/olesiejuk/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'ole1.csv');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		
		$sLogin = 'ksiegarniaprofit';
		$sPassword = '08profit2012ks';
		$sQuery = "ftp://".$sLogin.":".$sPassword."@195.42.113.31/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aElement, $iSourceBId) {
    return $oParent->proceedInsertProduct($aElement, $iSourceBId);
    return true;
	}
	
	
	public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId) {  
		// dodaj bookindeks dla produktu
		$sMD5 = $this->getProductMD5($aElement);
		$mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);
//		if ($aElement['ean_13'] != '') {
//			$this->updateEAN($iProductId, $aElement['ean_13']);
//		}
		
		//unset($aElement['prod_status']); - jeśli jest w tym skrypcie to produkt jest dostępny
		// dodać aktualizację cen
		
		// aktualizujemy produkt
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aElement, true);
		if ($this->checkUpdateProduct($aElement, $mRetMD5, $iProductId)) {
			$this->proceedUpdateProduct($aElement, $iProductId, $iSourceBId);
		}
	}
	
	public function checkExists(&$aElement) {
		return $this->refCheckProductExists($aElement);
	}
	
	public function PrepareSynchro() {}
	
}// end of seriesElements() Class
?>