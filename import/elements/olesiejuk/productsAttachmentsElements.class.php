<?php
/**
 * Klasa parsowania elementu Załącznik ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class productsAttachmentsElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'olesiejuk';
		
		// Stan na dzień 14.08.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'nr_katalogowy',
				'id_dodatku',
				'ID_ARTYKULU',
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
	}

	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {
		
		if ($this->aElement['_id'] > 0 && $this->aElement['product_id'] > 0) {
			return $this->aElement;
		}
		
		return false; // coś jest nie tak
	}
	
	public function setNr_katalogowy($sValue) {}
	
	public function setId_dodatku($sValue) {
		$this->aElement['attachment_id'] = $this->getText($sValue);
		$this->aElement['_id'] = $this->aElement['attachment_id'];
	}
	
	public function setID_ARTYKULU($sValue) {
		$this->aElement['product_id'] = intval($this->getText($sValue));
	}
	
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_wiazanie_dodatkowi.csv';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/CSV/olesiejuk/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'wiazanie_dodatkowi.csv');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		
		$sLogin = 'ksiegarniaprofit';
		$sPassword = '08profit2012ks';
		$sQuery = "ftp://".$sLogin.":".$sPassword."@195.42.113.31/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aElement, $iSourceBId) {
		global $aConfig, $pDbMgr;
		
		// wyczyśćmy śmieci
		$aValues = $this->clearProductVars($aElement);
		return $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_products_attachments", $aValues, '', false);
	}
	
	
	public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId) {
		// aktualizacja
	}
	
	public function checkExists($aElement) {
		
		return false;
	}
	
	public function PrepareSynchro() {
		global $aConfig, $pDbMgr;
		
		$sSql = "DELETE FROM 
						".$aConfig['tabls']['prefix']."_".$this->sSoruceSymbol."_products_attachments";
		return $pDbMgr->Query('profit24', $sSql);
	}
}// end of authorsElements() Class
?>
