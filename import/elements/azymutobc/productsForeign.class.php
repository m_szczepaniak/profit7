<?php
/**
 * Klasa dodawania aktualizacji produktów Ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-01-31 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class productsElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
  private $aConfig;
  private $pDbMgr;
  public $_countEmptyPublicationYear;
  
  function __construct($aConfig, $pDbMgr) {
    $this->aConfig = $aConfig;
    $this->pDbMgr = $pDbMgr;
    
		$this->sSoruceSymbol = 'azymyut';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
      'ateneum_id',// 123598,
      'EAN',// "9788364057137",
      'ISBN',// "9788364057137",
      'Tytul',// "Opowie¶ci graficzne TW",
      'Autorzy',// "Bernet Jordi, Abuli E.S.",
      'Wydawca',// "Korporacja Ha!Art",
      'Opis_wydania',// "Strony: 184, Format: 24,3x16,8 cm",
      'Opis_wydania2',// "Rok wydania: 2013, oprawa: twarda",
      'Opis',// "Unikatowe historie obrazkowe, stworzone przez Antonisza na przestrzeni czterdziestu lat w różnych momentach jego życia, stanowi± bezcenne uzupełnienie twórczo¶ci artysty. Ksi±żka odzwierciedla różnorodno¶ć tematów i technik narracyjno-graficznych stosowanych przez autora - poczynaj±c od młodzieńczej, 64-stronicowej próby ""Przygody Hitlera"" z 1954 roku, przez obszerny scenopis obrazkowy do niezrealizowanego filmu ""Pierony"" (lata 70.), aż po licz±cy dwadzie¶cia dwie strony fotokomiks ""Jak powstało Muzeum Filmu Non-Camera"" (1984 r.). Wszystkie wymienione realizacje stanowi± ¶wiadectwo nieprzeciętnej wyobraĽni, znakomitego zmysłu obserwacji i surrealistycznego poczucia humoru, wspartych inwencj± formaln±, widoczn± w wykorzystaniu przez artystę szerokiego arsenału ¶rodków plastycznych. ""Realizacja filmów eksperymentalnych metodami grafiki artystycznej poprzez odbijanie grafik bezpo¶rednio na ta¶mie filmowej z pominięciem kamery filmowej jest szans± na stworzenie filmu będ±cego autentycznym dziełem sztuki"" - pisał artysta w swoim ""Manife¶cie artystycznym grupy twórczej Non-Camera"" z 1977 roku. W wyniku interwencji autora film - jako unikatowy obiekt - zyskuje rangę obiektu sztuki, a sama ta¶ma filmowa nabiera cech ""hipergęstej"" sekwencji grafik, w której rozbity na fazy ruch budzi skojarzenia z narracj± komiksow±. Tego rodzaju podej¶cie do materii filmowej pozwala wpisać Antonisza nie tyle do grona ""filmowców"", co raczej intermedialnych, awangardowych artystów, traktuj±cych swoje kinematograficzne realizacje jako czę¶ć większego, interdyscyplinarnego (lub postdyscyplinarnego) projektu. ",
      'Cena_netto',// 56.19,
      'Podatek_opis',// "Podatek VAT 5%",
      'Cena_brutto',// 59.00,
      'kat1',// "Komiksy",
      'kat2',// "Dla dorosłych",
      'kat3',// "",
      'okl_path',// "123598.jpg",
      'hash',// "6f5f430607b2cc20af1d68c31288cd17",
      'empty'
		);
		
        /*
//				'book', //  głowny tag książki
				'ident_ate', // id książki w bazie źródła
        'EAN',
        'ISBN',
        'Tytył',
        'autor',
        'wydawnictwo',
        'opis_wydania', // ilosc stron, format
        'rok_wydania',
        'krótka_charakterystyka',
        'cena_detal_netto',
        'stawka_vat', // stawka vat słownie
        'cena_detal_brutto',
        'kategoria_poziom_1',
        'kategoria_poziom_2',
        'kategoria_poziom_3',
        'kategoria_poziom_3',
        'plik_zdjecia',
        'hash',
         */    
    
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
  }
}
