<?php
/**
 * Klasa parsowania elementów ze źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class categoriesElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	public $iNextIdSource = 0;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'azymutobc';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'dictTematyka', // w elemencie jest atrybut code - symbol w źródle
				'p', // w elemencie jest atrybut language - język, interesuje nas tylko PL
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
		$this->iNextIdSource = $this->getMaxIdInSource($this->iSourceId) + 1;
	}

	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {
		$this->aElement['source_id'] = $this->iSourceId;
		
		if ($this->aElement['source_category_id'] > 0 && $this->aElement['name'] != '') {
			return $this->aElement;
		}
		return false;
	}
	
	
	public function setP(&$oXml) {
    $this->aElement['source_symbol'] = $oXml->getAttribute('s');
    $this->aElement['name'] = $oXml->getAttribute('o');
    $this->aElement['_id'] = ($this->iNextIdSource++);
    $this->aElement['source_category_id'] = $this->aElement['_id'];
    $this->postParse();
    if (!$this->checkExists($this->aElement)) {
      $oParent = new stdClass();
      $this->proceedInsert($oParent, $this->aElement, $this->aElement['_id']);
    }
	}	
	
	
	/**
	 * Metoda pobiera id rodzica w źródle na podstawie symbolu
	 * 
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iSourceId
	 * @param type $sSourceSymbol
	 * @return type
	 */
	private function getSourceCategoryIdBySymbol($iSourceId, $sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT source_category_id FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_id = ".$iSourceId." AND
									 source_symbol = '".$sSourceSymbol."'
						 GROUP BY source_category_id
						 LIMIT 1";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceCategoryIdBySymbol() method
	
	
	/**
	 * Metoda konwertuje string na liczby, separator to zero
	 * 
	 * @param string $sStr
	 * @return int
	 */
	private function getMaxIdInSource($iSourceId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT MAX(source_category_id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_id = ".$iSourceId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of convertToInt() method
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
    
    include_once ('import_func.inc.php');
		
		$sFileName = 'azymut_'.date("siH_dmY").'_subjects.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sFileName;
		
    if(!downloadAzymutXML("getCats","azymut_cats.xml")){
      return false;
    } else {
      return $sFilePath;
    }
	}// end of importSaveXMLData() method
	
	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		global $aConfig, $pDbMgr;
		
		$aItem = $this->clearProductVars($aItem);
		// dodawanie
    dump($aItem);
		return $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."menus_items_mappings_categories", $aItem);
	}
	
	
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
		// nie potrzebna tu aktualizacja, a nawet nie wskazana
	}
	
	public function checkExists($aItem) {
		return $this->getSourceCategoryIdBySymbol($this->iSourceId, $aItem['source_symbol']);
	}
}// end of categoriesElements() Class
?>
