<?php
/**
 * Klasa parsowania elementów ze źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class categoriesElements extends CommonSynchro {
	public $aProduct = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'panda';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'GROUP', // głowny tag kategorii
		);
		
		$this->iSourceId = $this->getSourceId('panda');
	}

	public function clearProductData() {
		
		unset($this->aProduct);
	}
	
	public function postParse() {

		$this->aProduct['source_id'] = $this->iSourceId;
		if ($this->aProduct['source_category_id'] >= 0 && $this->aProduct['name'] != '') {
			return $this->aProduct;
		}
		return false;
	}

	/**
	 * @param XMLReader $oXml
	 */
	public function setGROUP(&$oXml) {
		global $pDbMgr;

		// nic nie robimy
		$itemId = $oXml->getAttribute('item_id');
		$this->aProduct['source_category_id'] = $itemId;
		$this->aProduct['_id'] = $itemId;

		$parentId = $oXml->getAttribute('parent_id');
		if ($parentId == '-1') {
			$this->aProduct['source_category_parent_id'] = 'NULL';
		} else {
			$this->aProduct['source_category_parent_id'] = $parentId;
		}


		$name = $oXml->getAttribute('name');
		if ($name != '') {
			$this->aProduct['name'] = $name;
		}

		$this->aProduct['source_id'] = $this->iSourceId;
		if ($this->aProduct['source_category_id'] >= 0 && $this->aProduct['name'] != '') {

			if ($this->checkCategoryExists($this->aProduct) <= 0) {
				var_dump($this->aProduct);
				$this->aProduct = $this->clearProductVars($this->aProduct);
				// dodawanie
				return $pDbMgr->Insert('profit24', "menus_items_mappings_categories", $this->aProduct);
			}

		}
		unset($this->aProduct);
	}

	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_categories.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/panda/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, '_plik_grupy.xml');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		
		$sQuery = 'http://panda.biuro.un.pl:81/XMLDoPobrania.aspx?sciezka=E://XML_v2//'.$sFile.'&login=Profit&haslo=A9C3686021FFB74F1111921774797368';
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		global $aConfig, $pDbMgr;
//		$aItem = $this->clearProductVars($aItem);
		// dodawanie
//		return $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."menus_items_mappings_categories", $aItem);
	}
	
	
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
		// nie potrzebna tu aktualizacja, a nawet nie wskazana
	}
	
	public function checkExists($aItem) {
		return ($this->checkCategoryExists($aItem) > 0);
	}
}// end of categoriesElements() Class
?>
