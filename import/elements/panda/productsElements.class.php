<?php
use LIB\Helpers\PriceHelper;

/**
 * Klasa dodawania aktualizacji produktów Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-01-31
 * @copyrights Marcin Chudy - Profit24.pl
 */
class productsElements extends CommonSynchro
{
    public $aElement = array();
    public $aSupportedTags = array();
    public $iSourceId = NULL;
    public $sSoruceSymbol = NULL;
    private $aConfig;
    private $pDbMgr;
    private $publisers;
    public $_countEmptyPublicationYear;

    function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->iNextIdSource = 0;


        $this->sSoruceSymbol = 'panda';

        // Stan na dzień 31.07.2012 r. - pierwsza implementacja
        $this->aSupportedTags = array(
            'PRODUCTS',
            'PRODUCT',
            'PRODUCTS_DESCRIPTION',
            'PRODUCTS_IMAGE'
        );

        $this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
        $this->iNextIdSource = $this->getMaxIdInSource($this->iSourceId);
        $this->markupWholesalePrice = $this->getMarkupWholesalePrice($this->iSourceId);
    }

    /**
     * @param $sourceId
     * @return mixed
     */
    private function getMarkupWholesalePrice($sourceId)
    {

        $sSql = 'SELECT markup_wholesale_price FROM products_general_discount WHERE source = ' . $sourceId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * Metoda zwraca maksymalny id kategorii w zrodle
     * @global array $aConfig
     * @global type $pDbMgr
     * @param int $iSourceId
     * @return int
     */
    private function getMaxIdInSource($iSourceId)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT MAX(source_category_id)
						 FROM " . $aConfig['tabls']['prefix'] . "menus_items_mappings_categories
						 WHERE source_id = " . $iSourceId;
        return $pDbMgr->GetOne('profit24', $sSql);
    }


    public function setPRODUCTS($sValue)
    {
    }// fix na pusty set


    /**
     * @param $sEan13
     * @return string
     */
    private function setEan($sEan13)
    {

        if ($sEan13 != '' && preg_match('/^[\dX]{6,15}$/', $sEan13)) {
            $this->aElement['isbn_plain'] = $sEan13; // do isbn'a idzie przed zmianą
            $sEAN13 = sprintf('%013s', $sEan13);
            $this->aElement['isbn'] = $sEan13;
            return $sEAN13;
        }
    }

    /**
     * @param $wholesalePriceNetto
     * @param $vat
     */
    private function setPrices($wholesalePriceNetto, $vat)
    {

        $markup = 40;
        $this->aElement['_wholesale_price'] = Common::formatPrice2($wholesalePriceNetto * (1 + $vat / 100));
        $this->aElement['price_brutto'] = Common::formatPrice2($this->aElement['_wholesale_price'] * (1 + $markup / 100));
        $this->aElement['price_netto'] = PriceHelper::priceNettoByBrutto($this->aElement['price_brutto'], $vat);
        $this->aElement['vat'] = $vat;
    }


    /**
     * @param XMLReader $oXml
     */
    public function setPRODUCT(&$oXml)
    {
        $productsName = $oXml->getAttribute('PRODUCTS_NAME');
        if ('' !== $productsName) {
            $this->aElement['name'] = $productsName;
            $this->aElement['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aElement['name'], ' ')));
        }
        // todo dopełniaj zerami
        $ean = $this->setEan($oXml->getAttribute('EAN'));
        if ($ean != '') {
            $this->aElement['ean_13'] = $ean;
        }

//        $quantity = $oXml->getAttribute('PRODUCTS_QUANTITY');
//        $jm = $oXml->getAttribute('JM');
        $wholesalePriceNetto = $oXml->getAttribute('NET_PRICE');
        $vat = $oXml->getAttribute('VAT');
        if ($wholesalePriceNetto > 0 && $vat > 0) {
            $this->setPrices($wholesalePriceNetto, $vat);
        }

        $kod = $oXml->getAttribute('towar_id');
        if ('' != $kod) {
            $this->aElement['_id'] = $kod;
        }

        $producer = $oXml->getAttribute('PRODUCENT');
        if ('' != $producer) {
            $this->aElement['_publisher'] = str_replace('/', '', $producer);
            if (!isset($this->publisers[$this->aElement['_publisher']])) {
                $this->publisers[$this->aElement['_publisher']] = 1;
            } else {
                $this->publisers[$this->aElement['_publisher']]++;
            }
        }

        $series = $oXml->getAttribute('MARKA');
        if ('' != $series) {
            $this->aElement['_items_series'] = [str_replace('/', '', $series)];
        }

        $categoryId = $oXml->getAttribute('category_id');
        if (isset($categoryId) && $categoryId != '') {
            $this->aElement['_items_category'] = [$categoryId => ''];
        }
    }

    /**
     * @param $oXml
     */
    public function setPRODUCTS_DESCRIPTION(&$oXml)
    {
        $sDesc = eregi_replace('<a.*</a>', '', $this->clearAsciiCrap($this->getTextNodeValue($oXml)));
        $sDesc = eregi_replace('<script.*</script>', '', $sDesc);
        $this->aElement['description'] = $sDesc;
        $this->aElement['short_description'] = $sDesc;
    }

    public function setPRODUCTS_IMAGE(&$oXml)
    {
        $image = $this->getTextNodeValue($oXml);
        if ($image != '') {
            $this->aElement['_img'] = $image;
        }
    }

    /**
     * Trochę przydługa metoda zapisywania zdjęcia do produktu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param type $iProductId
     * @param type $iSourceBookId
     * @param type $aProduct
     * @return boolean
     */
    public function addImage($iProductId, $iSourceBookId, $aProduct)
    {
        global $aConfig, $pDbMgr;

        $this->createProductImageDir($iProductId);
        $iRange = intval(floor($iProductId / 1000)) + 1;
        $sDestDir = $aConfig['common']['base_path'] . 'images/photos/okladki/' . $iRange . '/' . $iProductId;
        $sShortDir = 'okladki/' . $iRange . '/' . $iProductId;

        $sDstName = $aProduct['isbn_plain'] . ".jpg";
        $sSrcFile = $sDestDir . "/_" . $sDstName;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "products_images
						 WHERE product_id=" . $iProductId . " AND photo='" . $sDstName . "' AND name='" . $sDstName . "'";
        $iId = $pDbMgr->GetOne('profit24', $sSql);
        if ($iId > 0) {
            // zdjecie juz istnieje w bazie nie dodajemy ponownie
            return true;
        }

        $iTimeOut = 0;
        $sURL = $aProduct['_img'];
        // otwarcie pliku
        if ($rFp = fopen($sSrcFile, "w")) {
            $oCURL = curl_init($sURL);
            curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCURL, CURLOPT_HEADER, FALSE);

            curl_setopt($oCURL, CURLOPT_FILE, $rFp);
            curl_setopt($oCURL, CURLOPT_TIMEOUT, 1000);
            curl_setopt($oCURL, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($oCURL, CURLOPT_USERAGENT, 'Mozilla/5.0');

            curl_exec($oCURL);
            curl_close($oCURL);
            fclose($rFp);
        }

        if (!file_exists($sSrcFile))
            return false;

        $aFileInfo = getimagesize($sSrcFile);
        $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_' . $sDstName);
        $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
        $aSmallInfo = getimagesize($sDestDir . '/' . $sDstName);
        if ($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])) {
            $this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_' . $sDstName);
        }
        unlink($sSrcFile);

        $aValues = array(
            'product_id' => $iProductId,
            'directory' => $sShortDir,
            'photo' => $sDstName,
            'mime' => 'image/jpeg',
            'name' => $sDstName,
            'order_by' => '1'
        );
        return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix'] . "products_images", $aValues, '', false) != false);
    }// end of addImage() method

    public function clearProductData()
    {
        unset($this->aElement);
    }

    public function postParse()
    {

        if (trim($this->aElement['isbn_plain']) == '' && $this->aElement['ean_13'] != '') {
            $this->aElement['isbn_plain'] = $this->aElement['ean_13'];
            $this->aElement['streamsoft_indeks'] = $this->aElement['ean_13'];
        } else if (trim($this->aElement['isbn_plain']) == '') {
            return -1;
        }
        return $this->aElement;
    }


    /**
     * Metoda pobiera id źródła dla danego importu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param string $sSourceSymbol - symbol źródła
     * @return type
     */
    private function getSourceId($sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "sources
						 WHERE symbol='" . $sSourceSymbol . "'";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceId() method


    /**
     * Metoda importuje XML'a i zapisuje na dysku
     *
     * @return string - adres pliku XML z danymi
     */
    public function importSaveXMLData()
    {
        global $aConfig;

        $sFileName = date("siH_dmY") . '_product.XML';
        $sFilePath = $aConfig['common']['base_path'] . $aConfig['common']['import_dir'] . "/XML/panda/" . $sFileName;

        $bIsErr = $this->getSourceFile($sFilePath, '_plik_longPROFIT M_7002.xml');

        if ($bIsErr == true) {
            return $sFilePath;
        } else {
            return false;
        }
    }// end of importSaveXMLData() method


    /**
     * Metoda pobiera ze źródła plik
     *
     * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
     * @param string $sFile - nazwa pliku który chcemy pobrać
     * @return boolean
     */
    protected function getSourceFile($sFilePath, $sFile)
    {

        $getdata = http_build_query(
            array(
                'sciezka' => 'E://XML_v2//' . $sFile,
                'login' => 'Profit',
                'haslo' => 'A9C3686021FFB74F1111921774797368',
            )
        );
        $sQuery = 'http://panda.biuro.un.pl:81/XMLDoPobrania.aspx?' . $getdata;

        $opts = array('http' =>
            array(
                'method' => 'GET',
                'content' => $getdata
            )
        );

        $context = stream_context_create($opts);

        if (false === file_put_contents($sFilePath, file_get_contents($sQuery, false, $context))) {
            return false;
        }
        return true;
    }


    public function checkExists(&$aElement)
    {
        return $this->refCheckProductExists($aElement);
    }

    public function PrepareSynchro()
    {
    }

    /**
     * @param \CommonSynchro $oParent
     * @param $aElement
     * @param $iSourceBId
     * @param $iProductId
     * @return bool
     */
    public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId)
    {
        $sMD5 = $this->getProductMD5($aElement);
        $mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5, true);
        $this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aElement, true);
        if ('' != $aElement['_img']) {
            $this->addImage($iProductId, $iSourceBId, $aElement);
        }
        return true;
    }

    /**
     * @param $productId
     */
    private function clearPhotoPanda($productId) {
        global $aConfig;

        $sSql = '
SELECT *, PI.id AS id_image, P.id AS p_product_id
FROM products_images AS PI
JOIN products AS P
  ON PI.product_id = P.id
WHERE 
PI.created > "2018-11-16 12:00:00"
AND P.id = '.$productId.'
ORDER BY P.id DESC
';
        $row = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($row)) {
            $photoDir = $aConfig['common']['client_base_path'] . 'images/photos/' . $row['directory'];
            if ('' == file_get_contents($photoDir . '/' . $row['photo'])) {
                $this->removeImageFile($row);
                $sSql = 'DELETE FROM products_images WHERE id = ' . $row['id_image'];
                Common::Query($sSql);
                var_dump($productId);
            }
        }
    }

    function removeImageFile($aValue)
    {
        global $aConfig;

        $photoDir = $aConfig['common']['client_base_path'] . 'images/photos/' . $aValue['directory'];

        if (file_exists($photoDir . '/__b_' . $aValue['photo'])) {
            unlink($photoDir . '/__b_' . $aValue['photo']);
            echo 'del: ' . $photoDir . '/__b_' . $aValue['photo'] . "\n";
        }
        if (file_exists($photoDir . '/' . $aValue['photo'])) {
            unlink($photoDir . '/' . $aValue['photo']);
            echo 'del: ' . $photoDir . '/' . $aValue['photo'] . "\n";
        }
        if (file_exists($photoDir . '/__t_' . $aValue['photo'])) {
            unlink($photoDir . '/__t_' . $aValue['photo']);
            echo 'del: ' . $photoDir . '/__t_' . $aValue['photo'] . "\n";
        }
    }


    /**
     * @param \CommonSynchro $oParent
     * @param $aElement
     * @param $iSourceBId
     * @return bool
     */
    public function proceedInsert(&$oParent, $aElement, $iSourceBId)
    {
        $oParent->proceedInsertProduct($aElement, $iSourceBId, $this->aElement['_profit_categories'], NULL, NULL, true);
        usleep(50);
        return true;
    }

}

