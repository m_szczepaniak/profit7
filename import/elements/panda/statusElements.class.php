<?php
/**
 * Klasa parsowania elementów ze źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class statusElements extends CommonSynchro {
	public $aProduct = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;

	const MIN_QUANTITY_AVAILABLE = 3;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'panda';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'PRODUCT', // głowny tag kategorii
		);
		
		$this->iSourceId = $this->getSourceId('panda');
	}

	public function clearProductData() {
		
		unset($this->aProduct);
	}
	
	public function postParse() {

		return false;
	}

	/**
	 * @param XMLReader $oXml
	 */
	public function setPRODUCT(&$oXml) {
		global $pDbMgr;

		unset($this->aElement);
		// nic nie robimy
		$this->aElement['_id'] = $oXml->getAttribute('towar_id');
		$kod = $oXml->getAttribute('KOD');

		$this->aElement['_product_id'] = $this->getProductIdBySourceId($this->aElement['_id'], $this->iSourceId, true);
		$this->aElement['shipment_time'] = '1';
		if ($this->aElement['_product_id'] > 0 ) {
			$this->aElement['_stock'] = intval(\Common::formatPrice2($oXml->getAttribute('PRODUCTS_QUANTITY')));
			if ($this->aElement['_stock'] >= self::MIN_QUANTITY_AVAILABLE) {
				$this->aElement['prod_status'] = '1';
			} else {
				$this->aElement['prod_status'] = '0';
			}
			$sMD5 = $this->getProductMD5($this->aElement);
			$this->addBookIndeks($this->aElement['_product_id'], $kod, $this->iSourceId, $sMD5, 'K');
			$this->proceedProductStock($this->sSoruceSymbol, $this->aElement['_product_id'], $this->aElement, true);
		}
		unset($this->aElement);
	}

	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_categories.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/panda/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, '_plik_short.xml');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {

		$sQuery = 'http://panda.biuro.un.pl:81/XMLDoPobrania.aspx?sciezka=E://XML_v2//'.$sFile.'&login=Profit&haslo=A9C3686021FFB74F1111921774797368';
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
	}
	
	
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
	}
	
	public function checkExists($aItem) {

	}
}// end of categoriesElements() Class
?>
