<?php
/**
 * Klasa parsowania elementów ze źródła Platon do Profit24.pl
 * 
 * @author Marcin Janowski
 * @created 2015-06-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */

class productsElements extends CommonSynchro {
	public $aProduct = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	public $iNextIdSource = 0;
  public $iUpdate = false;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'platon';
	
		$this->aSupportedTags = array(
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'r',
        'rp',
        's',
        't',
        'u',
        'w',
        'x',
        'y',
        'z',
        'DataUtworzenia', 
        'URLOkladkaLink',
        'PRODUCTS'
		);
    
/*
  a-symbol(ID) towaru;
  b-kod kreskowy towaru;
  c-ISBN;
  d-Nazwa towaru;
  e-Data premiery;
  f-Status towaru;

  g-parametr dla linka do pobrania okładki;
  h-Autor;
  i-Seria;
  j-Tłumacz;
  k-Kategoria;
  l-Wydawnictwo;
  m-Opis;
  n-Rok wydania;
  o-Oprawa;

  p-Liczba stron;
  r-Wysokość;
  s-Szerokość;
  t-Wydanie;
  u-Waga;
  w-VAT;
  x-Cena detaliczna brutto;
  y-Rodzaj
*/
		
		$this->iSourceId = $this->getSourceId('platon');
		$this->iNextIdSource = $this->getMaxIdInSource($this->iSourceId); 
	}

	public function clearProductData() {
		unset($this->aProduct);
	}
	
	public function postParse() {
		
		if (trim($this->aProduct['isbn_plain']) == '') {
			return false;
		}
    
		if ($this->aProduct['price_brutto'] > 0.00 && $this->aProduct['vat'] > 0) {
			$this->aProduct['price_netto'] = Common::formatPrice2($this->aProduct['price_brutto']/(1+$this->aProduct['vat']/100));
		} else {
			return false;
		}
//    dump($this->aProduct);
		return $this->aProduct;
	}
  
  public function setPRODUCTS() {}
  
	public function setS(&$oXml) {}
  public function setDataUtworzenia(&$oXml) {}
  public function setURLOkladkaLink(&$oXml) {}
  public function setR(&$oXml) {}
  public function setRp(&$oXml) {}
  
	public function setA(&$oXml) {
		$this->aProduct['_id'] = $this->getTextNodeValue($oXml);
	}
  
	public function setB(&$oXml) {
		// $this->aProduct['id'] = $this->getTextNodeValue($oXml);
        $sEAN13 = $this->getTextNodeValue($oXml);
        $this->aProduct['ean_13'] = sprintf('%013s', $sEAN13);
		if ($sEAN13 != '') {
			$this->aProduct['isbn'] = isbn2plain($sEAN13);
			$this->aProduct['isbn_plain'] = $this->aProduct['isbn']; // isbn2plain f-cja z functions
		}
	}

	public function setZ(&$oXml) {}
  
  public function setC(&$oXml) {
		// $this->aProduct['id'] = $this->getTextNodeValue($oXml);
		$sIsbn10 = str_replace('-', '', $this->getTextNodeValue($oXml));
    if (mb_strlen($sIsbn10) == 10) {
      $this->aProduct['isbn_10'] = $sIsbn10;
    }
	}
	
	public function setD(&$oXml) {
		$this->aProduct['name'] = $this->getTextNodeValue($oXml);
		$this->aProduct['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aProduct['name'], ' ')));
		if (stristr($this->aProduct['name'], 'CD MP3')) {
			$this->aProduct['type'] = '1';
		}
	}
  
  public function setE(&$oXml) {}
  
	public function setX(&$oXml) {
		$this->aProduct['price_brutto'] = Common::formatPrice2($this->getTextNodeValue($oXml));
	}
  
	public function setF(&$oXml) {
		
		// TODO interpretacja statusów - dodać zapowiedź
//		$sStatus = $this->getTextNodeValue($oXml);
//		if ($sStatus != '') {
//      switch($sStatus) {
//        case 'JEST':
//          $this->aProduct['_status_txt'] = $sStatus;
//          $this->aProduct['prod_status'] = '1'; 
//        break;
//        
//        case 'Nowość': 
//          $this->aProduct['_status_txt'] = $sStatus;
//          $this->aProduct['prod_status'] = '1'; 
//          break;
//        
//        case 'DODRUK': 
//          $this->aProduct['_status_txt'] = $sStatus;
//          $this->aProduct['prod_status'] = '1'; 
//          break;
//        
//        case 'ZAPO': 
//          $this->aProduct['_status_txt'] = $sStatus;
//          $this->aProduct['prod_status'] = '0'; // nie ma daty wydania zapowiedzi 
//          break;
//        
//        case 'Wyczerpany': 
//          $this->aProduct['_status_txt'] = $sStatus;
//          $this->aProduct['prod_status'] = '0'; 
//          break;
//        default: 
//          $this->addErrorLog("Nie obsługiwany status ".$sStatus." produktu o isbn: ".$this->aProduct['isbn']);
//        	return false;  
//      }
//		} else {
//			return false;
//		}
	}
	
  public function setImagesmall() {}
  
  public function setG(&$oXml) {
    $sImg = $this->getTextNodeValue($oXml);
    if ($sImg) {
      $this->aProduct['_img'] = 'https://b2b.commersoft.pl/GetData/CommS_GetDataExecute.aspx?CPG=B193A8D8-C264-4DC9-9683-12BDCD7F6DAE&csAttachmentsG='.$sImg;
    }
  }
	
  
	public function setH(&$oXml) {
    $sAuthor = $this->getTextNodeValue($oXml);
    if($sAuthor != "-") {
      $this->aProduct['_items_authors'] = $sAuthor;
    }
	}
	
	
	public function setL(&$oXml) {
		// czyścimy ze slashy jakiś dziwnych ponieważ:
		//	- http://stackoverflow.com/questions/3235219/urlencoded-forward-slash-is-breaking-url
		$this->aProduct['_publisher'] = str_replace('/', '', $this->getTextNodeValue($oXml));
	}
  
  
	public function setM(&$oXml) {
		$sDesc = eregi_replace('<a.*</a>','', $this->clearAsciiCrap($this->getTextNodeValue($oXml)));
		$sDesc = eregi_replace('<script.*</script>','', $sDesc);
		$this->aProduct['description'] = $sDesc;
		
	} 
  
	
	public function setO(&$oXml) {
		
		$sBinding = $this->getTextNodeValue($oXml);
		if ($sBinding != '' && $sBinding != '-') {
			$this->aProduct['_binding'] = $sBinding;
		}
	}
	
	public function setP(&$oXml) {
		
		$iPages = intval($this->getTextNodeValue($oXml));
		if ($iPages > 0) {
			$this->aProduct['pages'] = $iPages;
		}
	}
  
  public function setU(&$oXml) {
		$fWeight = intval($this->getTextNodeValue($oXml))/1000;
    $this->aProduct['weight'] = $fWeight;

  }
  
	public function setN(&$oXml) {
		$iPYear = intval($this->getTextNodeValue($oXml));
		if ($iPYear > 0) {
			$this->aProduct['publication_year'] = $iPYear;
		}
	}
	
	public function setT(&$oXml) {
		
		$iEdition = intval($this->getTextNodeValue($oXml));
		if ($iEdition > 0) {
			$this->aProduct['edition'] = $iEdition;
		}
	}
  
	public function setI(&$oXml) {
		
		$sSeriesName = $this->getTextNodeValue($oXml);
		if ($sSeriesName != '') {
			$this->aProduct['_items_series'] = $sSeriesName;
		}
	}
  
	public function setJ(&$oXml) {
    $sTranslator = $this->getTextNodeValue($oXml);
    if($sTranslator != "-") {
      $sTranslatorClr = $this->getTranslatorsList($sTranslator);
      $this->aProduct['translator'] = $sTranslatorClr;
    }
	}
  
	public function setY(&$oXml) {

        $sType = $this->getTextNodeValue($oXml);
        $sType = mb_strtolower($sType, 'UTF-8');
        switch ($sType) {
          case 'książka':
          case 'książki':
              $this->aProduct['type'] = '0';
              $this->aProduct['type'] = '0';
          break;
          case 'audiobook':
            $this->aProduct['type'] = '1';
            break;
          default :
              $this->aProduct['type'] = '0';
              $this->aProduct['product_type'] = 'Z';
          break;
        }
	}
  
  	
	/**
	 * Metoda ustawia kategorię produktu
	 *
	 * @param type $oXml
	 * @return boolean 
	 */
	public function setK(&$oXml) {

		$sCatStr =(string) $this->getTextNodeValue($oXml);
    $aCategoriesNames = explode('\\', $sCatStr);
    foreach ($aCategoriesNames as $iKey => $sCatName) {
      $aCategoriesNames[$iKey] = trim($sCatName);
    }
    
    $iLastCatKey = count($aCategoriesNames)-1;
    $sCatName = $aCategoriesNames[$iLastCatKey];
    $iCatId = $this->getLastCatId($aCategoriesNames);
		$this->aProduct['_items_category'][$iCatId] = $sCatName; 
    if (preg_match("/^zabawki/", $sCatName)) {
      // nie importujemy jeśli są to zabawki
      unset($this->aProduct);
    }
    
		if ($sCatName != '') {
			if (stristr($sCatName, 'CD MP3')) {
				$this->aProduct['type'] = '1';
			}
		} else {
			return false;
		}
	}// end of setCategory() method
	
  /**
   * Metoda zwraca oczyszczoną listę tłumaczy ksiązki
   * @param string $sTranString
   */
  private function getTranslatorsList($sTranString) {
    
    $sTranslator = str_replace(range(0,9),'',$sTranString);
    $sTranslator = str_replace(';','',$sTranslator);
    $sTranslator = trim($sTranslator,',');
    return $sTranslator;
  }

  /**
   * Metoda zwraca maksymalny id kategorii w zrodle 
   * @global array $aConfig
   * @global type $pDbMgr
   * @param int $iSourceId
   * @return int
   */
	private function getMaxIdInSource($iSourceId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT MAX(source_category_id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_id = ".$iSourceId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}
  
  /**
   * Metoda zwraca id kategorii w zrodle
   * @param array $aCategories
   * @return int -- id kategorii w zrodle
   */
  private function getLastCatId($aCategories) {
    
    $iMaxIteration = count($aCategories) - 1;
    $iParent = false;
    
    foreach($aCategories as $sCatName) {
      $iCatId = $this->checkCategoryExistsByName($sCatName, $iParent);
      if($iCatId === false) {
        $this->iNextIdSource++;
        $this->insertNewCategory($this->iNextIdSource, $iParent, $sCatName);
        if(!$iMaxIteration) {
          return $this->iNextIdSource;
        } else {
          $iParent = $this->iNextIdSource;
        }
      } 
      elseif(!$iMaxIteration) {
        return $iCatId;
      } else {
        $iParent = $iCatId;
      }
      $iMaxIteration--;
    }
  }
  /**
   * Metoda zwraca id categorii na podstawie nazwy i zrodla 
   * @global type $pDbMgr
   * @param string $sCatName - nazwa kategorii
   * @param int $iParent - id rodzica
   * @return mixed
   */
  private function checkCategoryExistsByName($sCatName, $iParent) {
    global $pDbMgr;
    
    $sCond = "AND source_category_parent_id ";
    if($iParent>0) {
      $sCond .= " = ".$iParent;
    } else {
      $sCond .= "IS NULL";
    }
    $sSql = "SELECT source_category_id 
             FROM menus_items_mappings_categories
             WHERE source_id = ".$this->iSourceId."
             $sCond
             AND TRIM(name) = '$sCatName'";
    $iCatId = $pDbMgr->GetOne('profit24',$sSql);
    if($iCatId>0) {
      return $iCatId;
    } else {
      return false;
    }
  }
  
  /**
   * Metoda dodaje nowa kategorie w zrodle
   * @global type $pDbMgr
   * @param int $iCatId
   * @param int $iParent
   * @param string $sCatName
   */
  private function insertNewCategory($iCatId, $iParent, $sCatName) {
    global $pDbMgr;
    $aValues = array (
      'source_id' => $this->iSourceId,
      'source_category_id' => $iCatId,
      'name' => $sCatName
    );
    if($iParent !== false) {
        $aValues['source_category_parent_id'] = $iParent;
    }
    $pDbMgr->Insert('profit24', "menus_items_mappings_categories", $aValues);
    echo "Dodano nowa kategorie \n";
  }
  
  
	public function setW(&$oXml) {
		$aMatches = array();
		
		$sVat = $this->getTextNodeValue($oXml);
		preg_match("/^(\d+)/", $sVat, $aMatches);
		$iVatt = intval($aMatches[1]);
		if ($iVatt > 0) {
			$this->aProduct['vat'] = $iVatt;
		} else {
			return false;
		}
	}
  
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Trochę przydługa metoda zapisywania zdjęcia do produktu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iProductId
	 * @param type $iSourceBookId
	 * @param type $aProduct
	 * @return boolean 
	 */
	public function addImage($iProductId, $iSourceBookId, $aProduct) {
		global $aConfig, $pDbMgr;
		
		$this->createProductImageDir($iProductId);
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
		$sShortDir = 'okladki/'.$iRange.'/'.$iProductId;
		
		$sDstName = $aProduct['isbn_plain'].".jpg";
		$sSrcFile = $sDestDir."/_".$sDstName;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id=".$iProductId." AND photo='".$sDstName."' AND name='".$sDstName."'";
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if ($iId > 0) {
			// zdjecie juz istnieje w bazie nie dodajemy ponownie
			return true;
		}
		
		$iTimeOut = 0;
		$sURL = $aProduct['_img'];
		// otwarcie pliku
		if ($rFp = fopen($sSrcFile, "w")) {
			$oCURL = curl_init($sURL);
			curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
      
			curl_setopt($oCURL, CURLOPT_FILE, $rFp);
      curl_setopt($oCURL, CURLOPT_TIMEOUT, 1000); 
      curl_setopt($oCURL, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($oCURL, CURLOPT_USERAGENT, 'Mozilla/5.0');

			curl_exec($oCURL);
			curl_close($oCURL);
			fclose ($rFp);
		}
		
		if(!file_exists($sSrcFile))
			return false;
    
		$aFileInfo = getimagesize($sSrcFile);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
		$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
		if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
			$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
		}
		unlink($sSrcFile);

		$aValues=array(
			'product_id' => $iProductId,
			'directory' => $sShortDir,
			'photo' => $sDstName,
			'mime' => 'image/jpeg',
			'name' => $sDstName,
			'order_by' => '1'
		);
		return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
	}// end of addImage() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = 'products_'.date("siH_dmY").'.xml';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/platon/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp);
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
  
  
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle) {

		$sQuery = "https://b2b.commersoft.pl/GetData/CommS_GetDataExecute.aspx?CPG=B193A8D8-C264-4DC9-9683-12BDCD7F6DAE&csB2BReports4Customers=DBB0E46D-7E9E-43F9-BA90-A7F70D841F58";
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}

    /**
     * @param $aProduct
     */
    private function changeBeforeInsertProductName($aProduct) {
        $aProduct['name'] = $this->UcFirst($aProduct['name']);
        return $aProduct;
    }

    private function mb_ucfirst($string, $encoding)
    {
        $string = mb_strtolower($string, $encoding);
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    /**
     * @param $sName
     * @return mixed
     */
    private function UcFirst($sName) {
        return $this->mb_ucfirst($sName, "utf8");
    }
	
	public function proceedInsert(&$oParent, $aProduct, $iSourceBId) {
     echo "Insert ".$aProduct['id']."\r";

     $aProduct = $this->changeBeforeInsertProductName($aProduct);
     return $oParent->proceedInsertProduct($aProduct, $iSourceBId, $this->aProduct['_profit_categories']);
	}
	
	public function proceedUpdate(&$oParent, &$aProduct, $iSourceBId, $iProductId) {
    echo "Update ".$aProduct['id']."\r";
		// dodaj bookindeks dla produktu
		$sMD5 = $this->getProductMD5($aProduct);
		$mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);

//		if ($aProduct['type'] == '0' && 'Z' != $aProduct['product_type']) {
//		    $sSql = 'SELECT `type`, product_type, modiefied_by_service, name, id FROM products WHERE modiefied_by_service = "0" AND id = '.$iProductId;
//		    $aCurrProductData = Common::GetRow($sSql);
//		    if (!empty($aCurrProductData)) {
//                if ('0' == $aCurrProductData['modiefied_by_service'] && 'K' != $aCurrProductData['product_type']) {
//                    $line = print_r($aCurrProductData, true);
//                    file_put_contents('products_platon.log', $line, FILE_APPEND);
//                }
//            }
//        }

		// dodać aktualizację cen
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aProduct, true);
		if ($this->checkUpdateProduct($aProduct, $mRetMD5, $iProductId)) {
			$this->proceedUpdateProduct($aProduct, $iProductId, $iSourceBId, $this->aProduct['_profit_categories']);
		}
	}
	
	public function checkExists(&$aProduct) {
		return $this->refCheckProductExists($aProduct);
	}
  
  
  /**
 * Removes invalid XML
 *
 * @access public
 * @param string $value
 * @return string
 */
  public function stripInvalidXml($value) {
      $ret = "";
      $current;
      if (empty($value)) 
      {
          return $ret;
      }

      $length = strlen($value);
      for ($i=0; $i < $length; $i++)
      {
          $current = ord($value{$i});
          if (($current == 0x9) ||
              ($current == 0xA) ||
              ($current == 0xD) ||
              (($current >= 0x20) && ($current <= 0xD7FF)) ||
              (($current >= 0xE000) && ($current <= 0xFFFD)) ||
              (($current >= 0x10000) && ($current <= 0x10FFFF)))
          {
              $ret .= chr($current);
          }
          else
          {
              $ret .= " ";
          }
      }
      return $ret;
  }
}// end of productsElements() Class
