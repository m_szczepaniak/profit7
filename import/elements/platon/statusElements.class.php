<?php
/**
 * Klasa aktualizacji stanów produktu z Ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-01-31 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class statusElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
  private $aConfig;
  private $pDbMgr;
  
  function __construct() {
    global $aConfig, $pDbMgr;
    $this->aConfig = $aConfig;
    $this->pDbMgr = $pDbMgr;
            
    

		$this->sSoruceSymbol = 'platon';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'a',
				'b',
        'c',
        'd',
        'e',
        'f',
        'r'
		);
 /*   
a-kod kreskowy towaru; 
b-symbol(ID) towaru; 
c-stan, opisowo; 
d-cena netto po rabacie; 
e-cena brutto po rabacie; 
f-rabat(%)
	*/	
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
  }
  
  
	public function clearProductData() {
		
		unset($this->aElement);
	}
  
  

	public function postParse() {
    $aMatches = array();
    
		if ($this->aElement['_id'] == '' || $this->aElement['_id'] <= 0) {
			return -1;
		}
    preg_match('/(\d+)/', $this->aElement['_stock'], $aMatches);
    if (intval($aMatches[1]) > 0) {
      $this->aElement['prod_status'] = '1';
    } else {
      $this->aElement['prod_status'] = '0';
    }
    if ($this->aElement['_stock'] == '1-2') {
      $this->aElement['prod_status'] = '0';
    }
    $this->aElement['shipment_time'] = '1';
		return $this->aElement;
	}
  
  /**
   * 
   * @param type $oXml
   */
	public function setA(&$oXml) {
		$this->aElement['ean_13'] = $this->getTextNodeValue($oXml);
	}
  
  /**
   * 
   * @param type $oXml
   */
	public function setB(&$oXml) {
		$this->aElement['_id'] = $this->getTextNodeValue($oXml);
	}
  
  /**
   * 
   * @param type $oXml
   */
	public function setC(&$oXml) {
		$cStatus = $this->getTextNodeValue($oXml);
    $this->aElement['_stock'] = html_entity_decode($cStatus);
	}
  
  /**
   * 
   * @param type $oXml
   */
	public function setD(&$oXml) {}

  /**
   * 
   * @param type $oXml
   */
	public function setR(&$oXml) {}
  
  /**
   * 
   * @param type $oXml
   */
	public function setF(&$oXml) {}
  
  /**
   * 
   * @param type $oXml
   */
	public function setE(&$oXml) {
		$this->aElement['_wholesale_price'] = $this->getTextNodeValue($oXml);
	}
  
  
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
    $this->addBookIndeks($iProductId, $aItem['_id'], $this->iSourceId);
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aItem, true);
	}
  
	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		// nie potrzebny tu insert, a nawet nie wskazay
	}
	
	public function checkExists(&$aProduct) {
		return $this->refCheckProductExists($aProduct);
	}
  
  
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
  
  

	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = 'stock_'.date("siH_dmY").'.xml';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/platon/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp);
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
  
  
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle) {

		$sQuery = "https://b2b.commersoft.pl/GetData/CommS_GetDataExecute.aspx?CPG=B193A8D8-C264-4DC9-9683-12BDCD7F6DAE&csB2BReports4Customers=90ACF6EB-5897-4785-9087-E7775E706643";
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
  
  
  public function PrepareSynchro() {}
}

?>
