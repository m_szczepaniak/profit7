<?php

/**
 * Klasa parsowania elementów
 *
 * @author Arkadiusz Golba
 */
class dzialyElements extends CommonSynchro
{
    public $aElement = array();
    public $aSupportedTags = array();
    public $iSourceId = NULL;
    public $sSoruceSymbol = NULL;
    public $iNextIdSource = 0;

    public function __construct()
    {

        $this->sSoruceSymbol = 'azymut';

        // Stan na dzień 2016-06-14
        $this->aSupportedTags = array(
            'dzialy',
            'd',
            'nazwa',
            'kategorie',
            'k',
        );

        $this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
//		$this->iNextIdSource = $this->getMaxIdInSource($this->iSourceId) + 1;
    }

    public function clearProductData()
    {
        unset($this->aElement);
    }

    public function postParse()
    {
        $this->aElement['source_id'] = $this->iSourceId;
        if ($this->aElement['section'] == 'KD') {
            return $this->aElement;
        }
        return false;
    }


    public function setDzialy(&$oXml) {}

    public function setD(&$oXml) {
        $this->aElement['section'] = $oXml->getAttribute('kategoryzacja');
        $this->aElement['id'] = $oXml->getAttribute('id');
        $this->aElement['_id'] = $this->aElement['id'];
    }

    public function setNazwa(&$oXml) {
        $this->aElement['section_name'] = $this->getTextNodeValue($oXml);
    }

    public function setKategorie(&$oXml) {
        $this->aElement['section_categories'] = [];
    }

    public function setK(&$oXml) {
        $this->aElement['section_categories'][] =
            [
                'id' => $oXml->getAttribute('id'),
                'parent_id' => $oXml->getAttribute('parent_id'),
                'name' => $this->getTextNodeValue($oXml)
           ];
    }

    /**
     * Metoda pobiera id rodzica w źródle na podstawie symbolu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param type $iSourceId
     * @param type $sSourceSymbol
     * @return type
     */
    private function getSourceCategoryIdBySymbol($iSourceId, $sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT source_category_id FROM " . $aConfig['tabls']['prefix'] . "menus_items_mappings_categories
						 WHERE source_id = " . $iSourceId . " AND
									 source_symbol = '" . $sSourceSymbol . "'
						 GROUP BY source_category_id
						 LIMIT 1";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceCategoryIdBySymbol() method


    /**
     * Metoda pobiera id źródła dla danego importu
     *
     * @global type $aConfig
     * @global type $pDbMgr
     * @param string $sSourceSymbol - symbol źródła
     * @return type
     */
    private function getSourceId($sSourceSymbol)
    {
        global $aConfig, $pDbMgr;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "sources
						 WHERE symbol='" . $sSourceSymbol . "'";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of getSourceId() method


    /**
     * Metoda importuje XML'a i zapisuje na dysku
     *
     * @return string - adres pliku XML z danymi
     */
    public function importSaveXMLData()
    {
        global $aConfig;

        include_once('import_func.inc.php');

        $sFileName = 'azymut_' . date("siH_dmY") . '_dzialy.XML';
        $sFilePath = $aConfig['common']['base_path'] . $aConfig['common']['import_dir'] . "/XML/" . $sFileName;

        if (!downloadAzymutXML("getDzialy", "azymut_dzialy.xml")) {
            return false;
        } else {
            return $sFilePath;
        }
    }// end of importSaveXMLData() method

    public function proceedInsert(&$oParent, $aItem, $iSourceBId)
    {
        global $aConfig, $pDbMgr;
        $aItem = $this->clearProductVars($aItem);
        $iCategoryNumber = $this->toNumber($aItem['id']);
        $iExistedCategoryId = $this->checkCategorySourceSymbol($this->iSourceId, $aItem['id'], $iCategoryNumber);
        if ($iExistedCategoryId <= 0) {

            $aValues = [
                'name' => $aItem['section_name'],
                'source_category_id' => $iCategoryNumber,
                'source_symbol' => $aItem['id'],
                'source_id' => $this->iSourceId
            ];
            $iExistedCategoryId = $pDbMgr->Insert('profit24', "menus_items_mappings_categories", $aValues);
        }

        // lecimy po dzieciach
        $this->proceedSubcategories($iCategoryNumber, $aItem['section_categories']);
    }


    /**
     * @param $iCategoryNumber
     * @param $aSectionSubcategories
     */
    private function proceedSubcategories($iCategoryNumber, $aSectionSubcategories) {
        global $pDbMgr;

        dump($aSectionSubcategories);
        foreach($aSectionSubcategories as $aSubcategory) {
            $aData['source_id'] = $this->iSourceId;
            $aData['source_category_id'] =  $aSubcategory['id'];
            $iExistedSubCategoryId = $this->checkCategoryExists($aData);
            if ($iExistedSubCategoryId <= 0) {
                $aValues = [
                    'name' => $aSubcategory['name'],
                    'source_category_id' =>  $aData['source_category_id'],
                    'source_category_parent_id' => (isset($aSubcategory['parent_id']) && $aSubcategory['parent_id'] > 0 ? $aSubcategory['parent_id'] : $iCategoryNumber),
                    'source_id' => $this->iSourceId
                ];
                $iExistedSubCategoryId = $pDbMgr->Insert('profit24', "menus_items_mappings_categories", $aValues);
            }
        }
    }

    private function toNumber($word)
    {
        $return = '';

        $aWordArray = str_split($word);
        foreach ($aWordArray as $dest) {
                $return .= ord(strtolower($dest)) + 200;
        }
        return $return;
    }

    /**
     * @param $iSourceId
     * @param $sSourceCategorySymbol
     * @return mixed
     */
    private function checkCategorySourceSymbol($iSourceId, $sSourceCategorySymbol, $iCategoryNumber) {
        global $pDbMgr;


        $sSql = 'SELECT id FROM menus_items_mappings_categories WHERE source_id = "'.$iSourceId.'"
                    AND source_category_id = '.$iCategoryNumber.'
                    AND source_symbol = "'.$sSourceCategorySymbol.'" ';
        return $pDbMgr->GetOne('profit24', $sSql);
    }


    public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId)
    {
        // brak
    }

    public function checkExists($aElement) {
        return false;
    }

    public function PrepareSynchro() {}
}// end of categoriesElements() Class
?>
