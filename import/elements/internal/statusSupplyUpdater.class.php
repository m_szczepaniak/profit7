<?php

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use orders_alert\OrdersAlert;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
/**
 * Description of statusSupplyUpdater
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class statusSupplyUpdater {

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var array
   */
  private $aElement;
  
  /**
   *
   * @var string
   */
  private $sNow;
  
  /**
   * @var DataProvider
   */
  private $dataProvider;
  
  private $iSourceId;

  const MIN_COUNT_TO_DELETE_NOT_UPDATED = 1;
  
  public function __construct(DatabaseManager $pDbMgr, $sNow) {
    
    $this->pDbMgr = $pDbMgr;
    $this->sNow = $sNow;
    $this->dataProvider = new DataProvider($this->pDbMgr);
  }
  
  /**
   * 
   * @param int $iSourceId
   */
  public function setSourceId($iSourceId) {
    $this->iSourceId = $iSourceId;
  }
  
  /**
   * 
   * usuwamy brakujące dostawy
   * przypisujemy rezerwacje
   * 
   * @throws Exception
   */
  public function doRecountProductStockSupply() {
    
    if ($this->getCountProductNowUpdated() > self::MIN_COUNT_TO_DELETE_NOT_UPDATED) {
      $aAllSuppliesToDelete = $this->getProductSupplies();
      if (!empty($aAllSuppliesToDelete)) {
        $this->prepareProductsToDelete($aAllSuppliesToDelete);
      }
    } else {
      throw new Exception('liczba dostaw jest zbyt mała, może nie udało się pobrać dostawy...');
    }
  }
  
  /**
   * 
   * @param array $aAllSuppliesToDelete
   */
  private function prepareProductsToDelete($aAllSuppliesToDelete) {
    $productStockService = new LIB\Supplies\ProductStockService($this->pDbMgr);
    
    foreach ($aAllSuppliesToDelete as $iKey => $aSupply) {
      $aDebugInfo = array();
      $this->pDbMgr->BeginTransaction('profit24');

      // jezeli istnieje rezerwacja
      if ($aSupply['reservation'] > 0) {
        // jak np ejst 3 reservation_to _reduce a reservation = 2
        if ($aSupply['reservation'] <= $aSupply['reservation_to_reduce']) {
          $aDebugInfo['type'] = 'delete';
          if ($this->deleteSupply($aSupply['id'], false) === false) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
          }
        }
        // jak rezerwacji jest wiecej niz reservation_to_reduce
        else {
          // sprawdzamy czy są inne rezerwacje 
          $aSuppliesToRewriteReservation = $this->getSuppliesProductPriceASC($aSupply['product_id'], $aSupply['id']);
          $aSupplyReservations = $this->getReservationsSupply($aSupply['id']);
          $aPreparedRewriteToReservations = $this->prepareRewriteSupplyReservations($aSupplyReservations, $aSuppliesToRewriteReservation);

          // probojemy przepisac rezerwacje
          if (!empty($aSupplyReservations) && !empty($aPreparedRewriteToReservations) && is_array($aPreparedRewriteToReservations)) {
            $aDebugInfo['type_1'] = 'rewrite';
            $aDebugInfo['data'] = $aPreparedRewriteToReservations;
            if ($this->RewriteReservations($aPreparedRewriteToReservations) === false ) {
              $this->pDbMgr->RollbackTransaction('profit24');
              return false;
            }
            $aDebugInfo['type_2'] = 'delete';
            if ($this->deleteSupply($aSupply['id'], false) === false) {
              $this->pDbMgr->RollbackTransaction('profit24');
              return false;
            }
            Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'a.golba@profit24.pl;l.jaskiewicz@profit24.pl', 'Uwaga brak dostawy, na której była rezerwacja statusSupplyUpdater, przepisujemy', print_r($aSuppliesToRewriteReservation, true).print_r($aSupplyReservations, true).print_r($aDebugInfo, true), true);
          }
          // wywalamy rezerwacje i przywracamy do kresek
          else {
            $aDebugInfo['delete_supplies'] = 'delete';
            $aDebugInfo['data'] = $aSupply;
//            Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'a.golba@profit24.pl;l.jaskiewicz@profit24.pl', 'Uwaga brak dostawy, na której była rezerwacja statusSupplyUpdater', print_r($aDebugInfo, true), true);
            
            if ($aSupply['source'] == '0' || $aSupply['source'] == '1') {
              // możemy wycofać do kresek tylko produkty z E i z J
              $aDebugInfo['type_1'] = 'go_to_new';
              // wycofujemy do '---'
              if ($this->goToNewOrderItem($aSupply['id']) === false) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
              }
              $aDebugInfo['type_2'] = 'delete';
              if ($this->deleteSupply($aSupply['id'], true) === false) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
              }
              
              $productStockService->updateStockByProductsIds([$aSupply['product_id']]);
            }
          }
        }
      } else {
        //zwykle usuwanie jezeli niema zadnej rezewaji na ten produkt
        $aDebugInfo['type'] = 'delete';
        if ($this->deleteSupply($aSupply['id'], true) === false) {
          $this->pDbMgr->RollbackTransaction('profit24');
          return false;
        }
        $productStockService->updateStockByProductsIds([$aSupply['product_id']], false, true);
      }
      $this->pDbMgr->CommitTransaction('profit24');
      $debug = [
          'before' => $aSupply,
          'update' => $aDebugInfo
      ];
      $this->dataProvider->insertProductsStockSuppliesDebug($aSupply['id'], $debug);
    }
    return true;
  }

    /**
     *
     * @param int $iId
     * @return bool
     */
  public function goToNewOrderItem($iId) {
    
    $sSql = 'SELECT PSR.orders_items_id, PSR.order_id
      FROM products_stock_reservations AS PSR
      JOIN products_stock_supplies_to_reservations AS PSSTR
        ON PSR.id = PSSTR.products_stock_reservations_id
      JOIN products_stock_supplies AS PSS
        ON PSS.id = PSSTR.products_stock_supplies_id
      WHERE PSS.id = ' . $iId;
    $aOrders = $this->pDbMgr->GetAll('profit24', $sSql);
    
    foreach ($aOrders as $aOrderRow) {
      
      $iOIId = $aOrderRow['orders_items_id'];
      $iOId = $aOrderRow['order_id'];
      
      if ($iOIId > 0 && $iOId > 0) {
        $this->goToLineOrderItemAndOrder($iOId, $iOIId);
      }
    }
    return true;
  }
  
  /**
   * 
   * @param int $iOId
   * @param int $iOIId
   * @return boolean
   */
  private function goToLineOrderItemAndOrder($iOId, $iOIId) {

    if ($this->pDbMgr->Update('profit24', 'orders', ['internal_status' => '1'], ' id = '.$iOId.' LIMIT 1') === false) {
      return false;
    }

    if ($this->pDbMgr->Update('profit24', "orders_items", ['status' => 0], ' id = ' . $iOIId.' LIMIT 1') === false) {
      return false;
    }
    return true;
  }
  
  
  /**
   * 
   * @param array $aPreparedRewriteToReservations
   * @return bool
   */
  private function RewriteReservations($aPreparedRewriteToReservations) {
    
    // recount products_stock
    foreach ($aPreparedRewriteToReservations as $aToReservation) {
      if ($this->updateReservation($aToReservation) === false) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * 
   * @param array $aToReservation
   */
  private function updateReservation($aToReservation) {
    
    $sSql = 'UPDATE products_stock_supplies 
             SET reservation = reservation + '.$aToReservation['reservation_quantity'].'
             WHERE id = '.$aToReservation['supply_to_id'];
    
    if ($this->pDbMgr->Query('profit24', $sSql) === false) {
      return false;
    }
    
    $aValuesS = [
      'reservated_quantity' => $aToReservation['reservation_quantity'],
      'products_stock_supplies_id' => $aToReservation['supply_to_id'],
      'products_stock_reservations_id' => $aToReservation['products_stock_reservations_id_from']
    ];
    if ($this->pDbMgr->Insert('profit24', 'products_stock_supplies_to_reservations', $aValuesS) === false) {
      return false;
    }
    
    $debug = [
        'before' => $aToReservation,
        'update' => [
            'res' => $sSql,
            'to_res' => $aValuesS
        ]
    ];
    $this->dataProvider->insertProductsStockSuppliesDebug($aToReservation['current_reservation_reservation'], $debug);
    return true;
  }
  
  /**
   * 
   * @param array $aSupplyReservations
   * @param array $aSuppliesToRewriteTo
   * @return boolean|array
   */
  public function prepareRewriteSupplyReservations($aSupplyReservations, $aSuppliesToRewriteTo) {
    $aPreparedRewriteToReservations = array();
    
    foreach ($aSupplyReservations as $aSupplyRewriteFrom) {
      if ($this->prepareRewriteReservation($aPreparedRewriteToReservations, $aSupplyRewriteFrom, $aSuppliesToRewriteTo) === false) {
        return false;
      }
    }
    return $aPreparedRewriteToReservations;
  }
  
  /**
   * 
   * @param array $aSupplyRewriteFrom
   * @param array $aSuppliesToRewriteTo
   * @return array $aPreparedRewriteToReservations
   */
  public function prepareRewriteReservation(&$aPreparedRewriteToReservations, $aSupplyRewriteFrom, $aSuppliesToRewriteTo) {
    if ($aSupplyRewriteFrom['products_stock_reservations_id'] <= 0) {
      return false;
    }
    
    $iQuantyToRewrite = $aSupplyRewriteFrom['reservated_quantity'];
    
    foreach ($aSuppliesToRewriteTo as $aSupplyRewriteTo) {
      $iQuantityFree = $aSupplyRewriteTo['quantity'] - $aSupplyRewriteTo['reservation'];
      
      if ($iQuantityFree >= $iQuantyToRewrite) {
        
        $aPreparedRewriteToReservations[] = [
            'supply_to_id' => $aSupplyRewriteTo['id'],
            'supply_from_id' => $aSupplyRewriteFrom['id'],
            'products_stock_reservations_id_from' => $aSupplyRewriteFrom['products_stock_reservations_id'],
            'reservation_quantity' => $iQuantyToRewrite,
            'current_reservation_reservation' => ($iQuantyToRewrite + $aSupplyRewriteTo['reservation'])
            ];
        return $aPreparedRewriteToReservations;
        
      } elseif ($iQuantityFree > 0) {
        $aPreparedRewriteToReservations[] = [
            'supply_to_id' => $aSupplyRewriteTo['id'],
            'supply_from_id' => $aSupplyRewriteFrom['id'],
            'products_stock_reservations_id_from' => $aSupplyRewriteFrom['products_stock_reservations_id'],
            'reservation_quantity' => $iQuantityFree,
            'current_reservation_reservation' => ($iQuantityFree + $aSupplyRewriteTo['reservation'])// całą wolną ilość rezerwujemy
            ];
        $iQuantyToRewrite -= $iQuantityFree;
      }
    }
    return false;
  }
  
  /**
   * 
   * @param int $iProductId
   * @param int $iId
   * @return array
   */
  private function getSuppliesProductPriceASC($iProductId, $iId) {
    
    $sSql = 'SELECT PSS.*, SUM(PSS.reservation) as reservation, PSSTR.products_stock_reservations_id
      FROM products_stock_supplies AS PSS
      LEFT JOIN products_stock_supplies_to_reservations AS PSSTR
        ON PSSTR.products_stock_supplies_id = PSS.id
      WHERE PSS.product_id = ' . $iProductId . '
            AND PSS.last_update = "'.$this->sNow.'"
            AND PSS.id <> ' . $iId . '
            AND PSS.source = "'.$this->iSourceId.'"
      GROUP BY PSS.id
      ORDER BY PSS.wholesale_price ASC
      ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iSupplyId
   * @return array
   */
  private function getReservationsSupply($iSupplyId) {
    
    $sSql = '
      SELECT *
      FROM products_stock_supplies_to_reservations AS PSSTR
      JOIN products_stock_supplies AS PSS
        ON PSSTR.products_stock_supplies_id = PSS.id
      WHERE PSS.id = '.$iSupplyId;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iId
   * @return bool
   */
  private function deleteSupply($iId, $bDeleteProductsStockReservations = false) {
    
    $sSql = 'DELETE FROM products_stock_supplies WHERE id = '.$iId.' LIMIT 1';

    if (true === $bDeleteProductsStockReservations) {
      $this->onDeleteSupply($iId);
    }

    $res = $this->pDbMgr->Query('profit24', $sSql);

    return $res;
  }

  /**
   * Nie uzywac bezposrednio, metoda zostaje wywolywana tylko z deleteSupply
   */
  private function onDeleteSupply($iId)
  {
      $sql = "select DISTINCT PSR.id
        from products_stock_supplies_to_reservations AS PSTR
        join products_stock_supplies AS PSS ON PSTR.products_stock_supplies_id = PSS.id
        join products_stock_reservations AS PSR ON PSTR.products_stock_reservations_id = PSR.id AND PSR.move_to_erp = '0'
        where PSS.id = $iId";

      $res = $this->pDbMgr->GetCol('profit24', $sql);

      if (false == empty($res)) {
          foreach($res as $iReservationId) {
              $this->pDbMgr->Query('profit24', "DELETE from products_stock_reservations where id = $iReservationId LIMIT 1");
          }
      }
  }
  
  /**
   * 
   * @param int $iProductId
   * @return array
   */
  private function getProductSupplies() {
    
    $sSql = 'SELECT * 
              FROM products_stock_supplies 
              WHERE last_update <> "'.$this->sNow.'" AND
                    source = "'.$this->iSourceId.'" ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @return int
   */
  private function getCountProductNowUpdated() {
    
    $sSql = 'SELECT count(1) 
             FROM products_stock_supplies
             WHERE last_update = "'.$this->sNow.'" AND
                   source = "'.$this->iSourceId.'" ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aElement
   */
  public function updateSupply($aElement) {
    
    $this->aElement = $aElement;
    $aRowDB = $this->getEqualElement($aElement);
    if ($aRowDB['id'] > 0) {
      if ($this->UpdateSupplyData($aRowDB, $aElement) === false) {
        return false;
      }
    } else {
      if ($this->InsertSupplyData($aElement) === false) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * 
   * @param array $aRow
   * @param array $aElement
   * @return bool
   */
  private function checkUpdateSupply($aRow, $aElement) {
    return (intval($aRow['quantity']) !== intval($aElement['_stock']));
  }
  
  /**
   * 
   * @param array $aElement
   * @return int
   */
  private function getEqualElement($aElement) {
    
    $sSql = 'SELECT id, quantity, reservation_to_reduce, price_netto, price_brutto, vat
             FROM products_stock_supplies 
             WHERE product_id = '.$aElement['_product_id'].' AND
                   type = "'.$aElement['type'].'" AND 
                   wholesale_price = "'.$aElement['_wholesale_price'].'" AND
                   source = "'.$aElement['_source_id'].'"  ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aElement
   * @return bool
   */
  private function InsertSupplyData($aElement) {

    $aValues = array(
      'product_id' => $aElement['_product_id'],
      'source' => $aElement['_source_id'],
      'quantity' => $aElement['_stock'],
      'reservation_erp' => $aElement['_REZ'],
      'type' => $aElement['type'],
      'wholesale_price' => $aElement['_wholesale_price'],
      'price_netto' => $aElement['price_netto'],
      'price_brutto' => $aElement['price_brutto'],
      'vat' => $aElement['vat'],
      'last_update' => $this->sNow
    );
    return $this->pDbMgr->Insert('profit24', 'products_stock_supplies', $aValues);
  }
  
  /**
   * 
   * @param int $iId
   * @param array $aElement
   * @return bool
   */
  private function UpdateSupplyData($aRowDB, $aElement) {
    
    $aValues = array(
      'last_update' => $this->sNow,
      'reservation_erp' => $aElement['_REZ']
    );

    if ($this->checkUpdateSupply($aRowDB, $aElement)) {
      $aValues['quantity'] = $aElement['_stock'];
    }
    if ($this->checkUpdatePrices($aRowDB, $aElement)) {
      $aValues['price_netto'] = $aElement['price_netto'];
      $aValues['price_brutto'] = $aElement['price_brutto'];
      $aValues['vat'] = $aElement['vat'];
    }

    return $this->pDbMgr->Update('profit24', 'products_stock_supplies', $aValues, ' id = '.$aRowDB['id']);
  }
  
  /**
   * 
   * @param array $aRowDB
   * @param array $aElement
   * @return boolean
   */
  private function checkUpdatePrices($aRowDB, $aElement) {
    if ((float)$aRowDB['price_netto'] !== (float)$aElement['price_netto']){
      return true;
    }
    if ((float)$aRowDB['price_brutto'] !== (float)$aElement['price_brutto']){
      return true;
    }
    if ((float)$aRowDB['vat'] !== (float)$aElement['vat']){
      return true;
    }
    return false;
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @return array
   */
  private function getSubstractReservationOnStockSupplies($productIds = []) {
    global $pDbMgr;
    
    $sSql = 'SELECT id, product_id, reservation_to_reduce, reservation
             FROM products_stock_supplies
             WHERE reservation_to_reduce > 0
             ';
    if (!empty($productIds)) {
        $sSql .= ' AND product_id IN ('.implode(',', $productIds).') ';
    }
    return $pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @return bool
   */
  public function substractReservationOnStockSupplies($productIds = []) {
    
    $oAlert = new OrdersAlert();
    $aSupplies = $this->getSubstractReservationOnStockSupplies($productIds);
    
    foreach ($aSupplies as $aSupply) {
      if ($aSupply['reservation_to_reduce'] > $aSupply['reservation']) {
        $oAlert->createAlert('NULL', 'reservation_to_reduce', sprintf(_('Rezerwacja %s %s %s'), $aSupply['product_id'], $aSupply['reservation'], $aSupply['reservation_to_reduce']));
        if ($this->zeroSupplies($aSupply) === false) {
          return false;
        }
      } else {
        if ($this->reduceSupplies($aSupply) === false) {
          return false;
        }
      }
    }
    
    $oAlert->addAlerts();
    return true;
  }
  
  /**
   * 
   */
  private function zeroSupplies($aSupply) {
    global $pDbMgr;
    
    $aValues = [
        'reservation' => '0',
        'reservation_to_reduce' => '0' 
    ];
    
    $debug = [
        'zeroSupplies' => $aValues,
        'supply_in' => $aSupply
    ];
    $this->dataProvider->insertProductsStockSuppliesDebug($aSupply['id'], $debug);

    return $pDbMgr->Update('profit24', 'products_stock_supplies', $aValues, ' id = '.$aSupply['id']);
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param array $aSupply
   * @return bool
   */
  private function reduceSupplies($aSupply) {
//    global $pDbMgr;
//
//
//    $aValues = [
//        'reservation' => $aSupply['reservation'] - $aSupply['reservation_to_reduce'],
//        'reservation_to_reduce' => '0'
//    ];
//
//    $sId = $aSupply['id'];
//
//    $sqlData = "
//                SELECT sum(pstr.reservated_quantity) as reservation
//                FROM `products_stock_supplies` AS pss
//                JOIN products_stock_supplies_to_reservations AS pstr ON pss.id = pstr.products_stock_supplies_id
//                WHERE pss.id = $sId
//    ";
//
//    $data = $this->pDbMgr->GetRow('profit24', $sqlData);
//
//    if ($aValues['reservation'] != $data['reservation']) {
//      $aValues['reservation'] = $data['reservation'];
//    }
//
//    $debug = [
//        'reduceSupplies' => $aValues,
//        'supply_in' => $aSupply
//    ];
//    $this->dataProvider->insertProductsStockSuppliesDebug($aSupply['id'], $debug);
//
//    return $pDbMgr->Update('profit24', 'products_stock_supplies', $aValues, ' id = '.$aSupply['id']);

    global $pDbMgr;

    $aValues = [
        'reservation' => $aSupply['reservation'] - $aSupply['reservation_to_reduce'],
        'reservation_to_reduce' => '0'
    ];

    $debug = [
        'reduceSupplies' => $aValues,
        'supply_in' => $aSupply
    ];
    $this->dataProvider->insertProductsStockSuppliesDebug($aSupply['id'], $debug);

    return $pDbMgr->Update('profit24', 'products_stock_supplies', $aValues, ' id = '.$aSupply['id']);
  }
}
