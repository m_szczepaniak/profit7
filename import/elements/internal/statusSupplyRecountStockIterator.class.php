<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\Supplies\ProductStockService;

/**
 * Description of statusSupplyIteratior
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class statusSupplyRecountStockIterator {

  /**
   * @var statusSupplyUpdater
   */
  private $statusSupplyUpdater;
  
  /**
   *
   * @var array
   */
  private $aProductsSupplies = array();

  /**
   * @var ProductStockService
   */
  private $productStockService;

  /**
   * @param statusSupplyUpdater $statusSupplyUpdater
   * @param ProductStockService $productStockService
   */
  public function __construct(statusSupplyUpdater $statusSupplyUpdater, ProductStockService $productStockService) {

      global $pDbMgr;

      $this->statusSupplyUpdater = $statusSupplyUpdater;
      $this->productStockService = $productStockService;
      $this->dataProvider = new DataProvider($pDbMgr);
  }

  
  /**
   * 
   * @param array $aCurrentElement
   */
  public function nextElement($aCurrentElement) {

    if ($this->checkCurrentElementIsDiffrentPrevious($aCurrentElement) === true) {
      // inny element przeliczamy 
      $this->doRecountStock($this->aProductsSupplies);

      // deklarujemy nową tablicę z produktami
      $this->aProductsSupplies = array();
      $this->aProductsSupplies[] = $aCurrentElement;
      $this->aPreviousElement = $aCurrentElement;
    } else {
      // ten sam produkt
      $this->aProductsSupplies[] = $aCurrentElement;
      $this->aPreviousElement = $aCurrentElement;
    }
  } 
  
  /**
   * 
   * @param array $aProductsSupplies
   */
  private function doRecountStock($aProductsSupplies) {
    /**
     *     
      [0] => Array
        (
            [_id] => KAL022015
            [ean_13] => KAL022015
            [isbn_plain] => KAL022015
            [type] => FA
            [_stock] => 2.00
            [_wholesale_price_netto] => 21.95
            [vat] => 23
            [price_netto] => 36.59
            [price_brutto] => 45.00
            [_depot] => 02
            [_source_id] => 1
            [_wholesale_price] => 27.00
        )
      [1] => Array
        (
            [_id] => KAL022015
            [ean_13] => KAL022015
            [isbn_plain] => KAL022015
            [type] => FA
            [_stock] => 5.00
            [_wholesale_price_netto] => 21.95
            [vat] => 23
            [price_netto] => 36.59
            [price_brutto] => 45.00
            [_depot] => 02
            [_source_id] => 1
            [_wholesale_price] => 27.00
        )
     */
    
    if (true === empty($aProductsSupplies)) {
        return null;
    }

    $this->productStockService->updateProductStock($aProductsSupplies, true);
    
//    $this->checkReservations($aProductsSupplies);
  }
  
  /**
   * 
   * @param array $aProductsSupplies
   */
  private function checkReservations($aProductsSupplies) {
    foreach ($aProductsSupplies as $stockSupply) {
      if ($stockSupply['_source_id'] == '10') {
        $data = $this->dataProvider->getSupplyByProductIdSourceAndWhPrice($stockSupply['_product_id'], $stockSupply['_wholesale_price'], $stockSupply['_source_id']);
        $iRez = (intval($data['reservation']) - $data['reservation_to_reduce']);
        if($iRez == intval($stockSupply['_REZ'])) {
          echo 'OK rezerwacja';
        } else {
          $sMsg = 'ERR rezerwacja '.$iRez.' : '.$stockSupply['_REZ'].' '.print_r($data, true).print_r($stockSupply, true);
          Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'a.golba@profit24.pl;l.jaskiewicz@profit24.pl', 'Uwaga rozjazd w rezerwacjach', $sMsg, false);
          echo $sMsg;
        }
      }
    }
  }
  

  /**
   * 
   * @param array $aCurrentElement
   * @return boolean
   */
  public function checkCurrentElementIsDiffrentPrevious($aCurrentElement) {

    if (true === $this->isFirst($aCurrentElement)){
        return false;
    }
    
    if ($aCurrentElement['_id'] !== $this->aPreviousElement['_id']) {
      return true;
    } else {
      return false;
    }
  }

  private function isFirst($aCurrentElement)
  {
      // pierwszy element
      if (true === empty($this->aPreviousElement) && false === empty($aCurrentElement)) {
          return true;
      }

      return false;
  }
}
