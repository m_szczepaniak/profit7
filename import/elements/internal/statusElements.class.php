<?php

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\EntityManager\Entites\StockLocation;
use LIB\Supplies\ProductStockService;
use omniaCMS\lib\Products\ProductsStockReservations;

include_once(__DIR__.'/../../CommonSynchro.class.php');
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class statusElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
  public $pProductsCommon = NULL;
  private $sEAN13 = '';
  private $sDepot;
  private $sNow;
  public $toProceedAtTheEndOfScript = [];
  
  
  /**
   *
   * @var date
   */
  private $sUpdateTime;
  
  private $aMappedInternalSources = array(
//    '02' => array('symbol' => 'profit_e'),
    'O98' => array('symbol' => 'profit_j'),
    '07' => array('symbol' => 'profit_g'),
  );
  
  /**
   *
   * @var statusSupplyRecountStockIterator 
   */
  private $oStatusSupplyIterator;
  
  /**
   *
   * @var statusSupplyUpdater 
   */
  public $oStatusSupplyUpdater;
  
  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

    private $stockLocation;
  
  function __construct() {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    
    $this->sNow = date("Y-m-d H:i:s");
    // dodanie do shadowa
    include_once('modules/m_oferta_produktowa/Module_Common.class.php');
    $this->pProductsCommon = new Module_Common();

    $this->stockLocation = new StockLocation($this->pDbMgr);
    
    // zależny od nr magazynu
		//$this->sSoruceSymbol = 'profit24';
		
		// Stan na dzień 16.12.2013 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'STANY', // głowny tag
				'STAN',
				'MAGAZYN',
				'INDEKS',
        'RODZAJ',
        'ILOSC',
        'CENA_ZAKUPU',
        'STAWKA_VAT',
        'CENA_SPRZ_NETTO',
        'CENA_SPRZ_BRUTTO',
        'REZ',
        'POREZ'
		);
		
		$this->iSourceId = $this->getSourceId('profit24');
    
    include_once('statusSupplyUpdater.class.php');
    $this->oStatusSupplyUpdater = new statusSupplyUpdater($pDbMgr, $this->sNow);

    $productStockService = new ProductStockService($pDbMgr);
    $this->productStockService = $productStockService;

    include_once('statusSupplyRecountStockIterator.class.php');
    $this->oStatusSupplyIterator = new statusSupplyRecountStockIterator($this->oStatusSupplyUpdater, $productStockService);
    
    $this->aMappedInternalSources = $this->getSourcesDataByMappedInternalSources($this->aMappedInternalSources);
    
    $this->dataProvider = new DataProvider($this->pDbMgr);
    $this->mappedSources = $this->dataProvider->getMappedSourceByStockPrefix();
  }

  public function postParseElement($book)
  {
    global $pDbMgr;

    // sprawdzamy tak poniewaz niewiadomo czy gowniany parser przekaze nam tablice czy cokolwiek innego.
    if (!is_array($book)) {
      return false;
    }

    if (!isset($book['_source_id']) || $book['_source_id'] == '') {

      return false;
    }

    if ($book['pid'] === false || !isset($book['_POREZ'])) {
      return;
    }

    $pid = $book['pid'];
    $whPrice = $book['_wholesale_price'];

    if ($book['_POREZ'] < 1 && $book['_source_id'] == 10) {

      //wyrezerwowujemy dostawy i ustawiamy status na stock na 0

      $sql = 'UPDATE products_stock_supplies
                 SET reservation = quantity
                 WHERE product_id = '.$pid. ' AND source = 10 AND wholesale_price = '.$whPrice.' AND type = "'.$book['type'].'"';

      $pDbMgr->Query('profit24', $sql);

      $stockSupplies = $pDbMgr->GetAll('profit24', "SELECT * FROM products_stock_supplies WHERE source = 10 AND product_id = $pid");

      if (empty($stockSupplies)) {

        $pDbMgr->Query('profit24', "UPDATE products_stock SET profit_g_status = 0, profit_g_act_stock = 0, profit_g_reservations = 0 WHERE id = $pid LIMIT 1");
      } else {

        $tempValues = [
          'reservated' => 0,
          'total' => 0,
        ];

        foreach($stockSupplies as $supply) {

          $tempValues['reservated'] += $supply['reservation'];
          $tempValues['total'] += $supply['quantity'];
        }

        $tempValues['avail'] = $tempValues['total'] - $tempValues['reservated'];

        $reees = $pDbMgr->Query('profit24', 'UPDATE products_stock SET profit_g_status = '.$tempValues['avail'].', profit_g_act_stock = '.$tempValues['total'].', profit_g_reservations = '.$tempValues['reservated'].' WHERE id = '.$pid.' LIMIT 1');
      }
    }
  }

  /**
   * 
   * @param array $aMappedInternalSources
   * @return array
   */
  private function getSourcesDataByMappedInternalSources($aMappedInternalSources) {
    foreach ($aMappedInternalSources as $iKey => $aSource) {
      $aMappedInternalSources[$iKey]['id'] = $this->getSourceBySymbol($aSource['symbol']);
    }
    return $aMappedInternalSources;
  }
  
  
  /**
   * 
   * @param string $sSymbol
   * @return int
   *
   */
  private function getSourceBySymbol($sSymbol) {
    
    $sSql = ' SELECT id 
              FROM sources 
              WHERE column_prefix_products_stock = "'.$sSymbol.'" ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param string $sUpdateTime
   */
  public function setUpdateTime($sUpdateTime) {
    $this->sUpdateTime = $sUpdateTime;
  }
  
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
  
  
	public function clearProductData() {
		unset($this->aElement);
	}
  
  /**
   * 
   * @param string $sDepot
   * @return boolean
   */
  private function getSourceIdByDepot($sDepot) {
    
    if (isset($this->aMappedInternalSources[$sDepot]) && isset($this->aMappedInternalSources[$sDepot]['id'])) {
      return $this->aMappedInternalSources[$sDepot]['id'];
    } else {
      return false;
    }
  }
	
  /**
   * 
   * @return array
   */
	public function postParse() {
    
    $this->sSoruceSymbol = $this->_getStockPrefixByDepot($this->sDepot);
    $this->aElement['_depot'] = $this->sDepot;
    $this->aElement['_source_id'] = $this->getSourceIdByDepot($this->sDepot);
    $this->oStatusSupplyUpdater->setSourceId($this->aElement['_source_id']);
    
    if ($this->aElement['_reservation'] > 0) {
      $this->aElement['_stock'] = intval($this->aElement['_stock']) - intval($this->aElement['_reservation']);
    }
    $this->aElement['_wholesale_price'] = Common::formatPrice2($this->aElement['_wholesale_price_netto']*(1 + $this->aElement['vat']/100));
     

    return $this->aElement;
	}
  
  public function setSTAN($sValue) {}
  public function setRODZAJ($sValue) {
    $sType = $this->getTextNodeValue($sValue);
    $this->aElement['type'] = $sType;
  }
  
	public function setINDEKS(&$sValue) {
		$sEAN13 = $this->getTextNodeValue($sValue);
    $this->sEAN13 = $sEAN13;
    $this->aElement['_id'] = $sEAN13;
    $this->aElement['ean_13'] = $sEAN13;
    $this->aElement['isbn_plain'] = $sEAN13;
	}
  
	public function setILOSC(&$sValue) {
		$this->aElement['_stock'] = Common::formatPrice2($this->getTextNodeValue($sValue));
	}
  
	public function setMAGAZYN(&$sValue) {
		$this->sDepot = $this->getTextNodeValue($sValue);
	}
  
	public function setCENA_SPRZ_NETTO(&$sValue) {
		$this->aElement['price_netto'] = Common::formatPrice2($this->getTextNodeValue($sValue));
	}
  
  public function setCENA_SPRZ_BRUTTO(&$sValue) {
		$this->aElement['price_brutto'] = Common::formatPrice2($this->getTextNodeValue($sValue));
	}
  
  public function setSTAWKA_VAT(&$sValue) {
    $this->aElement['vat'] = str_replace('%', '', $this->getTextNodeValue($sValue));
  }
  
  public function setREZ(&$sValue) {
    $this->aElement['_REZ'] = Common::formatPrice($this->getTextNodeValue($sValue));
  }
  
  public function setPOREZ(&$sValue) {
    $this->aElement['_POREZ'] = Common::formatPrice($this->getTextNodeValue($sValue));
  }
    
    
  public function setCENA_ZAKUPU(&$sValue) {
    $this->aElement['_wholesale_price_netto'] = Common::formatPrice2($this->getTextNodeValue($sValue));
  }
  
  
  /**
   * Metoda pobiera prefix magazynu w tabeli products_stock 
   *  na podstawie id magazynu
   * 
   * @param int $iDepot
   * @return string|boolean
   */
  public function _getStockPrefixByDepot($iDepot) {
    
    if (isset($this->aMappedInternalSources[$iDepot])) {
      return $this->aMappedInternalSources[$iDepot]['symbol'];
    }
    return false;
  }// end of _getStockPrefixByDepot() method
  
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV 
   *       prawdopodobnie plik będzie wysyłany na serwer
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		
		$sFilePath = $aConfig['common']['base_path'].'/LIB/communicator/sources/Internal/streamsoft/import_files/xml/Stany/';
    foreach (glob($sFilePath."*.xml") as $filename) {
        return $filename;
    }
  	return false;
	}// end of importSaveXMLData() method
  
	
  /**
   * 
   * @global array $aConfig
   * @global DatabaseManager $pDbMgr
   * @param type $oParent
   * @param array $aElement
   * @param int $iSourceBId
   * @param int $iProductId
   * @return boolean
   */
	public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId) {
      $aElement['_product_id'] = $iProductId;
      $this->aElement = $aElement;
      if ($this->sSoruceSymbol !== FALSE) {
          if ($this->aElement['_id'] != '') {
              $this->oStatusSupplyIterator->nextElement($this->aElement);
          }
      }
      if ($iProductId > 0) {
        $this->oStatusSupplyUpdater->updateSupply($aElement);
      }
	}
	
  
	/**
	 * Metoda dopasowuje produkt po ISBNach, pobiera także zadane kolumny
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param array $aProduct - dane produktu
	 * @param array $aColsSelect - wybierane kolumny z tabeli products
   * @param bool  $bSearchByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym
   * @param bool $bSearchOnlyByMainSourceIndex - czy szukany indeks produktu w źródle ma być głównym i wyszukujemy tylko po tym indeksie dostawcy
	 * @return array
	 */
	public function refMatchProductByISBNs($aProduct, $aColsSelect = array('id')){
		global $pDbMgr;
		$aDBProduct = array();
    
		$sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    streamsoft_indeks = "%1$s"
                    ';
		
		if (empty($aDBProduct) && $aProduct['ean_13'] != '') {
			$sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
			$aDBProduct = $pDbMgr->GetRow('profit24', $sSql);
		}

		if (empty($aDBProduct) && $aProduct['ean_13'] != '') {
            $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    ean_13 = "%1$s"
                    ';
            $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
            $satesErr = $pDbMgr->GetRow('profit24', $sSql);
            if (!empty($satesErr)) {
                dump($aProduct['ean_13']);
                dump($satesErr);
            }
        }

        if (empty($aDBProduct) && empty($satesErr) && $aProduct['ean_13'] != '') {
            $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    isbn_13 = "%1$s"
                    ';
            $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
            $satesErr = $pDbMgr->GetRow('profit24', $sSql);
            if (!empty($satesErr)) {
                dump($aProduct['ean_13']);
                dump($satesErr);
            }
        }

        if (empty($aDBProduct) && empty($satesErr) && $aProduct['ean_13'] != '') {
            $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    isbn_10 = "%1$s"
                    ';
            $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
            $satesErr = $pDbMgr->GetRow('profit24', $sSql);
            if (!empty($satesErr)) {
                dump($aProduct['ean_13']);
                dump($satesErr);
            }
        }

        if (empty($aDBProduct) && empty($satesErr) && $aProduct['ean_13'] != '') {
            $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    isbn_plain = "%1$s"
                    ';
            $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
            $satesErr = $pDbMgr->GetRow('profit24', $sSql);
            if (!empty($satesErr)) {
                dump($aProduct['ean_13']);
                dump($satesErr);
            }
        }

		/*
		 * NIE JEST TO POTRZEBNE
		if (isset($aProduct[$sMatchedType]) && $aProduct[$sMatchedType] != '') {
			$aProduct['_matched_type'] = $sMatchedType;
			$aProduct['_matched_product'] = $aDBProduct;
		}
		 */
		return $aDBProduct;
	}// end of refMatchProductByISBNs() method
  
  
	public function proceedInsert(&$oParent, &$aElement, $iSourceBId) {
   // można by było tutaj dodawać kartotekę
	}
  
  /**
   * 
   * @param array $aElement
   * @return int
   */
	public function checkExists($aElement) {
    $aItem = $this->refMatchProductByISBNs($aElement, array('id'));
    if (!empty($aItem)) {
      return $aItem['id'];
    }
    return false;
	}
	
  public function doUpdateAll() {
		global $aConfig, $pDbMgr;
    
    /*
    foreach ($this->aElements as $aItem) {
      if ($aItem['_product_id'] > 0) {
        // jesli poprzedni to to samo id to, nie zmieniamy cen
        // aktualizacja
        $iStock = intval($aItem['_stock']);
        $sSql = 'UPDATE products_stock
          SET ';
        $sSql .= $this->sSoruceSymbol.'_last_import = NOW(), ';
        $sSql .= $this->sSoruceSymbol.'_location = '.
            ' (IF ('.$this->sSoruceSymbol.'_status > 0 AND ' . $iStock . ' = 0 , ' .
            ' , "" '.
            ' , '.$this->sSoruceSymbol.'_location) ),';
        $sSql .= $this->sSoruceSymbol.'_status = ' . $iStock . ' - '.$this->sSoruceSymbol.'_reservations , ';
        $sSql .= $this->sSoruceSymbol.'_act_stock = "'.$iStock. '", ';
        $sSql .= $this->sSoruceSymbol.'_shipment_time = "0", ';
        $sSql .= $this->sSoruceSymbol.'_wholesale_price = "' . $aItem['_wholesale_price'] . '",';
        $sSql .= $this->sSoruceSymbol.'_price_netto = "' . ($aItem['price_netto'] != '' ? $aItem['price_netto'] : '0').'", ';
        $sSql .= $this->sSoruceSymbol.'_price_brutto = "' . ($aItem['price_brutto'] != '' ? $aItem['price_brutto'] : '0').'", ';
        $sSql .= $this->sSoruceSymbol.'_vat = "' . ($aItem['vat'] != '' ? $aItem['vat'] : '0') . '" ';
        $sSql .= ' WHERE id = '.$aItem['_product_id'];
        if ($pDbMgr->Query('profit24', $sSql) === FALSE) {
          return false;
        }
      }
    }
    */
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   */
  public function trySoledOutProducts($sDataUpdate) {

    $iMinQuantityToUpdate = 150;
    foreach ($this->aMappedInternalSources as $iSourceId => $aSource) {
      $sSourceColName = $aSource['symbol'];
      if ($this->checkProductSourceUpdated($sSourceColName, $iMinQuantityToUpdate, $sDataUpdate) === TRUE) {
        
//        $aIdsSoldOut = $this->getProductsIdsToSoledOut($sSourceColName, $sDataUpdate);
//        $iProviderId = $this->mappedSources[$sSourceColName];
//        $this->deleteProductsSourceSupplies($aIdsSoldOut, $iProviderId);
        $this->doSoledOutProductsStock($sSourceColName, $sDataUpdate);
      }
    }
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param array $aIdsSoldOut
   * @param int $iProviderId
   */
  private function deleteProductsSourceSupplies($aIdsSoldOut, $iProviderId) {
    global $pDbMgr;
    
    foreach ($aIdsSoldOut as $iProductId) {
      $sSql = 'DELETE FROM products_stock_supplies 
               WHERE product_id = '.$iProductId.' AND source = "'.$iProviderId.'"';
      $pDbMgr->Query('profit24', $sSql);
    }
  }
  
  private function doSoledOutProductsStock($sSourceColName, $sDataUpdate) {
    global $pDbMgr;
    
    $sSql = "
    UPDATE products_stock
    SET
      ".$sSourceColName."_act_stock = '0',
      ".$sSourceColName."_reservations = '0',
      ".$sSourceColName."_status = '0'
    WHERE ".$this->getWhereSoledOut($sSourceColName, $sDataUpdate);
    return $pDbMgr->Query('profit24', $sSql);
  }
  
  /**
   * 
   * @param string $sSourceColName
   * @param string $sDataUpdate
   * @return string
   */
  private function getProductsIdsToSoledOut($sSourceColName, $sDataUpdate) {
    global $pDbMgr;
    
    $sSql = 'SELECT id 
             FROM products_stock
             WHERE '. $this->getWhereSoledOut($sSourceColName, $sDataUpdate);
    return $pDbMgr->GetCol('profit24', $sSql);
  }
  
  /**
   * 
   * @param string $sSourceColName
   * @param string $sDataUpdate
   * @return string
   */
  private function getWhereSoledOut($sSourceColName, $sDataUpdate) {
    
    $sSql = $sSourceColName."_act_stock > 0
          AND ".$sSourceColName."_last_import < '".$sDataUpdate."' ";
    return $sSql;
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iMinQuantity
   * @return bool
   */
  private function checkProductSourceUpdated($sSourceColName, $iMinQuantity, $sDataUpdate) {
    global $pDbMgr;
    
    $sSql = 'SELECT count(id)
      FROM products_stock
      WHERE '.$sSourceColName.'_act_stock > 0
        AND '.$sSourceColName.'_last_import > "'.$sDataUpdate.'"
      ';
    return ($pDbMgr->GetOne('profit24', $sSql) > $iMinQuantity) ? TRUE : FALSE;
  }
  
	public function PrepareSynchro() {}
  
  /**
   * 
   */
  public function doMovedToErpUPDATE() {
    global $pDbMgr;
    
    $oReservations = new ProductsStockReservations($pDbMgr);
    $aMoveERPReservations = $this->getMoveToERPReservation();
    if (!empty($aMoveERPReservations)) {
      foreach ($aMoveERPReservations as $aReservation) {
        // delete reservation 
        $oReservations->deleteReservationBySourceId($aReservation['source_id'], $aReservation['order_id'], $aReservation['orders_items_id']);
        $this->stockLocation->unreserveStockLocation($aReservation['order_id'], $aReservation['orders_items_id']);
      }
    }
    
    // tutaj należy zwolnić także products_stock_supplies
    $this->oStatusSupplyUpdater->substractReservationOnStockSupplies();
  }
  
  /**
   * 
   */
  public function doRecountProductStockSupply() {
    $this->oStatusSupplyUpdater->doRecountProductStockSupply();
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @return array
   */
  private function getMoveToERPReservation() {
    global $pDbMgr;
    
    $sSql = 'SELECT OSR.*
             FROM products_stock_reservations AS OSR
             JOIN orders AS O
              ON O.id = OSR.order_id
             WHERE OSR.move_to_erp = "2" OR
( OSR.move_to_erp = "1" 
AND O.erp_export IS NOT NULL)';
    return $pDbMgr->GetAll('profit24', $sSql);
  }
}
