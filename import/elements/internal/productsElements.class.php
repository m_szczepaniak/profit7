<?php
/**
 * Klasa dodawania nowych produktów w pliku CSV
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class productsElements extends CommonSynchro  {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
  public $pProductsCommon = NULL;
  
  function __construct() {
    // dodanie do shadowa
    include_once('modules/m_oferta_produktowa/Module_Common.class.php');
    $this->pProductsCommon = new Module_Common();
    
		// Stan na dzień 16.12.2013 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'KARTOTEKA', // głowny isbn
				'INDEKS', // EAN13
				'IDENTYFIKATOR',
        'NAZWA',
        'STAWKAVAT', // na podstawie grupy określamy kat głowną i kat nowości oraz zapisujemy źródło pochodzenia
				'CENAKATALOGOWANETTO',
				'CENAKATALOGOWABRUTTO',
        'PKWIU',
        'JM'
		);
  }
  
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
  
  
  
	public function clearProductData() {
		unset($this->aElement);
	}
	
	public function postParse() {

    if (empty($this->aElement['_identyfikator']) && empty($this->aElement['name'])) {
        unset($this->aElement);
        return FALSE;
    }
    
    if ($this->aElement['_id'] != '') {
      return $this->aElement;
    } else {
      return FALSE;
    }
		return FALSE; // coś jest nie tak
	}
  
  public function setKARTOTEKA(&$sValue) { }
	public function setIDENTYFIKATOR(&$sValue) {
        $ident = $this->getTextNodeValue($sValue);
        $this->aElement['_identyfikator'] = $ident;
    }
  public function setJM(&$sValue) { }
  
	public function setINDEKS(&$sValue) {
		$sISBN = $this->getTextNodeValue($sValue);
    
		$this->aElement['isbn_plain'] = $sISBN;
    $this->aElement['ean_13'] = $sISBN;
		$this->aElement['_id'] = $sISBN;
	}
  
  public function setNAZWA(&$sValue) {
		$this->aElement['name'] = $this->getTextNodeValue($sValue);
	}
  
  
  public function setSTAWKAVAT(&$sValue) {
		$this->aElement['vat'] = str_replace('%', '', $this->getTextNodeValue($sValue));
	}
  

	public function setCENAKATALOGOWANETTO(&$sValue) {
		$this->aElement['price_netto'] = $this->getTextNodeValue($sValue);
	}
  
  public function setCENAKATALOGOWABRUTTO(&$sValue) {
		$this->aElement['price_brutto'] = $this->getTextNodeValue($sValue);
	}
  
  public function setPKWIU(&$sValue) {
		$this->aElement['pkwiu'] = $this->getTextNodeValue($sValue);
	}
  
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * @TODO do zaimplementowania, aktualnie brak danych o tym jak pobierac CSV 
   *       prawdopodobnie plik będzie wysyłany na serwer
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		
		$sFilePath = $aConfig['common']['base_path'].'/LIB/communicator/sources/Internal/streamsoft/import_files/xml/Kartoteki/';
    foreach (glob($sFilePath."*.xml") as $filename) {
        return $filename;
    }
  	return false;
	}// end of importSaveXMLData() method

	public function proceedInsert(&$oParent, $aElement, $iSourceBId) {
		global $aConfig, $pDbMgr;
    
      $aValues = array(
        'page_id' => '1',
        'name' => $aElement['name'],
        'plain_name' => $aElement['name'],
        'description' => '',
        'pkwiu' => $aElement['pkwiu'],
        'published' => '0',
        'reviewed' => '1',
        'isbn' => $aElement['isbn_plain'],
        'isbn_plain' => $aElement['isbn_plain'],
        'ean_13' => $aElement['ean_13'],
        'price_brutto' => $aElement['price_brutto'],
        'price_netto' => $aElement['price_netto'],
        'vat' => $aElement['vat'],
        'last_import' => 'NOW()',
        'created' => 'NOW()',
        'created_by' => 'auto_streamsoft'
      );
      if (($PId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products", $aValues)) === false) {
        $this->addErrorLog('Wystąpił błąd podczas dodawania nowego produktu o id : '.$PId.' ISBN: '.$aElement['isbn_plain']);
        return FALSE;
      } else {
        return TRUE;
      }
	}
  
	public function proceedUpdate(&$oParent, &$aElement, $iSourceBId, $iProductId) {
		global $aConfig, $pDbMgr;
    // myślę że tu narazie nie będziemy nic synchronizować
    // ale może w przyszłości tytył, opis, pkwiu
      /*
    $sSql = 'SELECT published FROM products WHERE id = ' . $iProductId;
    $cPublished = $pDbMgr->GetOne('profit24', $sSql);
    if ($cPublished == '1') {
      try {
        $oAddProductsSS = new \LIB\communicator\sources\Internal\streamsoft\synchroProduct($pDbMgr);
        $oAddProductsSS->trySynchronizeProduct($iProductId, true);
      } catch (Exception $ex) {
        echo "\n".$iProductId."\n";
        echo $ex->getMessage();
        echo 'TUUU';
        die;
        $this->addErrorLog($ex->getMessage().$ex->getTraceAsString());
      }
    }
      */
  }
  
  
	public function checkExists($aElement) {
        print_r($aElement);
        print_r($this->refCheckProductExists($aElement))."\n"."\n";
        var_dump($this->refCheckProductExists($aElement));
		return $this->refCheckProductExists($aElement);
	}
	
	public function PrepareSynchro() {}
  
  
}

