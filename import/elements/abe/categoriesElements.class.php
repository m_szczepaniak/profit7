<?php
/**
 * Klasa parsowania elementów ze źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class categoriesElements extends CommonSynchro {
	public $aElement = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	public $iNextIdSource = 0;
	
	public function __construct() {
		
		$this->sSoruceSymbol = 'abe';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
				'item', // w elemencie jest atrybut code - symbol w źródle
				'heading', // w elemencie jest atrybut language - język, interesuje nas tylko PL
		);
		
		$this->iSourceId = $this->getSourceId('abe');
		$this->iNextIdSource = $this->getMaxIdInSource($this->iSourceId);
	}

	public function clearProductData() {
		
		unset($this->aElement);
	}
	
	public function postParse() {
		
		$this->aElement['source_id'] = $this->iSourceId;
		
		// sprwdźmy czy kategoria może mieć rodzica
		if (mb_strlen($this->aElement['source_symbol'], 'UTF-8') > 1) {
			// ma rodziców, pobierzmy sobie ojca
			$sParentSymbol = mb_substr($this->aElement['source_symbol'], 0, -1, 'UTF-8');
			$iPSId = $this->getSourceCategoryIdBySymbol($this->iSourceId, $sParentSymbol);
			if ($iPSId > 0) {
				$this->aElement['source_category_parent_id'] = $iPSId;
			}
		}

		if ($this->aElement['source_category_id'] > 0 && $this->aElement['name'] != '') {
			return $this->aElement;
		}
		return false;
	}
	
	
	public function setItem(&$oXml) {
		$sCode = $oXml->getAttribute('code');

		if ($sCode != '') {
			$this->iNextIdSource++;
			$this->aElement['_id'] = $this->iNextIdSource;
			$this->aElement['source_category_id'] = $this->aElement['_id'];
			$this->aElement['source_symbol'] = $sCode;
		} else {
			$this->addErrorLog("Wystąpił błąd pobierania kodu kategorii, pusty kod w kategorii ");
		}
	}
	
	
	public function setHeading(&$oXml) {
		$sLanguage = $oXml->getAttribute('language');
		
		if ($sLanguage != '') {
			if ($sLanguage == 'pl') {
				// interesuje nas wyłącznie polski
				$this->aElement['name'] = $this->getTextNodeValue($oXml);
			}
		} else {
			$this->addErrorLog("Pusty język w kategorii o kodzie ".$this->aElement['source_symbol']);
		}
	}	
	
	
	/**
	 * Metoda pobiera id rodzica w źródle na podstawie symbolu
	 * 
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iSourceId
	 * @param type $sSourceSymbol
	 * @return type
	 */
	private function getSourceCategoryIdBySymbol($iSourceId, $sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT source_category_id FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_id = ".$iSourceId." AND
									 source_symbol = '".$sSourceSymbol."'
						 GROUP BY source_category_id
						 LIMIT 1";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceCategoryIdBySymbol() method
	
	
	/**
	 * Metoda konwertuje string na liczby, separator to zero
	 * 
	 * @param string $sStr
	 * @return int
	 */
	private function getMaxIdInSource($iSourceId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT MAX(source_category_id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_id = ".$iSourceId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of convertToInt() method
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData() {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_categories.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/abe/".$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'bic_translations.php');
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		global $aConfig;
		
		$sQuery = "http://b2b.abe.pl/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_USERPWD, $aConfig['import']['abe_login'].':'.$aConfig['import']['abe_password']);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aItem, $iSourceBId) {
		global $aConfig, $pDbMgr;
		
		$aItem = $this->clearProductVars($aItem);
		// dodawanie
		return $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."menus_items_mappings_categories", $aItem);
	}
	
	
	public function proceedUpdate(&$oParent, &$aItem, $iSourceBId, $iProductId) {
		// nie potrzebna tu aktualizacja, a nawet nie wskazana
	}
	
	public function checkExists($aItem) {
		return $this->getSourceCategoryIdBySymbol($this->iSourceId, $aItem['source_symbol']);
	}
}// end of categoriesElements() Class
?>
