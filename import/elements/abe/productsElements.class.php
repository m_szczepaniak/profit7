<?php
/**
 * Klasa parsowania elementów ze źródła ABE do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

class productsElements extends CommonSynchro {
	public $aProduct = array();
	public $aSupportedTags = array();
	public $iSourceId = NULL;
	public $sSoruceSymbol = NULL;
	private $aContenerField = array();
	private $iAuthor = 0;
	private $iStock = 1;
					
	public function __construct() {
		
		$this->sSoruceSymbol = 'abe';
		
		// Stan na dzień 31.07.2012 r. - pierwsza implementacja
		$this->aSupportedTags = array(
			'Header',
			'FromCompany',
			'FromEmail',
			'ToCompany',
			'SentDate',
			'Product',
			'RecordReference',
			'NotificationType',
			'ProductIdentifier',
			'ProductIDType',
			'IDValue',
			'ProductForm',
      'ProductClassification',
      'ProductClassificationType',
      'ProductClassificationCode',
			'Title',
			'TitleType',
			'TitleText',
			'Contributor',
			'SequenceNumber',
			'ContributorRole',
			'PersonName',
			'KeyNames',
      'Language',
      'LanguageRole',
      'LanguageCode',
			'EditionNumber',
			'NumberOfPages',
			'BICMainSubject',
			'BICVersion',
			'OtherText',
			'TextTypeCode',
			'Text',
			'MediaFile',
			'MediaFileTypeCode',
			'MediaFileFormatCode',
			'MediaFileLinkTypeCode',
			'MediaFileLink',
			'Publisher',
			'PublishingRole',
			'NameCodeType',
			'NameCodeValue',
			'PublisherName',
			'PublicationDate',
			'Measure',
			'MeasureTypeCode',
			'Measurement',
			'MeasureUnitCode',
			'SupplyDetail',
			'SupplierName',
			'ProductAvailability',
			'Stock',
			'LocationIdentifier',
			'LocationIDType',
			'StockQuantityCoded',
			'StockQuantityCodeType',
			'StockQuantityCode',
			'Price',
			'PriceTypeCode',
			'PriceAmount',
			'CurrencyCode',
      'DiscountCoded',
      'DiscountCodeType',
      'DiscountCode',
			'Price',
			'PriceTypeCode',
			'PriceAmount',
			'CurrencyCode',
			'TaxRateCode1',
			'TaxRatePercent1',
			'TaxableAmount1',
			'TaxAmount1',
      'AudienceCode'
		);
		
		$this->iSourceId = $this->getSourceId($this->sSoruceSymbol);
	}
	
	
	public function setHeader(&$oXml) {}
	public function setFromcompany(&$oXml) {}
	public function setFromemail(&$oXml) {}
	public function setTocompany(&$oXml) {}
	public function setSentdate(&$oXml) {}
	public function setProduct(&$oXml) {}
  public function setAudienceCode(&$oXml) {}
  
  public function setLanguage(&$oXml) {}
  public function setLanguageRole(&$oXml) {}
  public function setLanguageCode(&$oXml) {
    $sLangCode = $this->getTextNodeValue($oXml);
    if ($sLangCode != '') {
      $this->aProduct['_language_code'] = $sLangCode;
    }
  }
	
	public function setRecordreference(&$oXml) {
		$sIsbn = $this->getTextNodeValue($oXml);
		
		if ($sIsbn != '') {
			$this->aProduct['isbn'] = isbn2plain($sIsbn);
			$this->aProduct['isbn_plain'] = $this->aProduct['isbn']; // isbn2plain f-cja z functions
			$this->aProduct['_id'] = $this->aProduct['isbn'];
		}
	}
	
	public function setNotificationtype(&$oXml) {
		
		$iType = $this->getTextNodeValue($oXml);
		if ($iType != '03') {
			$this->addErrorLog('Nie wspierany typ powiadomienia');
			unset($this);
			return false;
		}
	}
	
	public function setProductidentifier(&$oXml) {
		$this->aContenerField = array(); // na wstępie czyścimy
		$this->aContenerField['ident'] = $oXml->name;
	}
	
	public function setProductidtype(&$oXml) {
		
		$iTagType = $this->getTextNodeValue($oXml);
		if ($iTagType == '02') {
			// isbn 10
			$this->aContenerField['type'] = $iTagType;
		} elseif ($iTagType == '15') {
			// isbn 13
			$this->aContenerField['type'] = $iTagType;
		} else {
			$this->addErrorLog('Nie wspierany typ tagu: '.$iTagType);
			unset($this);
			return false;
		}
		
	}
	
	public function setIdvalue(&$oXml) {
		
		$sValue = $this->getTextNodeValue($oXml);
		if ($sValue != '') {
			switch ($this->aContenerField['ident']) {
				case 'ProductIdentifier': 
					if ($this->aContenerField['type'] == '02') {
						$this->aProduct['isbn_10'] = $sValue;
					}
					elseif ($this->aContenerField['type'] == '15') {
						$this->aProduct['isbn_13'] = $sValue;
					} else {
						$this->addErrorLog('Nie znacznik ISBN "ProductIdentifier" : '.$sValue);
						unset($this);
						return false;
					}
				break;
				
				case 'Stock': 
					$this->aProduct['_stock_abe'][$this->iStock]['value'] = $sValue;
				break;

				default:
					$this->addErrorLog('Nie wspierany tag wewnętrzny : '.$this->aContenerField['ident']);
					unset($this);
					return false;
				break;
			}
		}
		// czyścimy kontener
		unset($this->aContenerField);
	}
	
	public function setProductform(&$oXml) {
		
		$sBCode = $this->getTextNodeValue($oXml);
		$sBinding = $this->ProductformMapper($sBCode);
		if ($sBinding != '') {
			$this->aProduct['_binding'] = $sBinding;
		}
		if (substr($sBCode, 0, 1) == 'A') {
			$this->aProduct['type'] = '1';
		}
	}
  
  public function setProductClassification(&$oXml) {} // XXX TODO
  public function setProductClassificationType(&$oXml) {} // XXX TODO
  public function setProductClassificationCode(&$oXml) {} // XXX TODO
          
	
	public function setTitle(&$oXml) {}
	
	public function setTitletype(&$oXml) {
		$this->aContenerField = array();
		$this->aContenerField['type'] = $this->getTextNodeValue($oXml);
	}
	
	public function setTitletext(&$oXml) {
		
		switch ($this->aContenerField['type']) {
			case '01':
				$this->aProduct['name'] = $this->getTextNodeValue($oXml);
				$this->aProduct['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($this->aProduct['name'], ' ')));
			break;
			default:
				$this->addErrorLog('Nie wspierany typ "Titletype" : '.$this->aContenerField['type']);
				unset($this);
				return false;
			break;
		}
	}
	
	
	public function setContributor(&$oXml) {}
	public function setSequencenumber(&$oXml) {}
	public function setContributorrole(&$oXml) {
		$this->aContenerField = array();
		$this->aContenerField['type'] = $this->getTextNodeValue($oXml);
	}
	
	public function setPersonname(&$oXml) {
		
		
		switch ($this->aContenerField['type']) {
			case 'A01':
				$this->iAuthor++;
				$this->aProduct['_items_authors'][$this->iAuthor] = $this->getTextNodeValue($oXml);
			break;
			default:
				$this->addErrorLog('Nie wspierany typ autora "Contributorrole" : '.$this->aContenerField['type']);
				unset($this);
				return false;
			break;
		}
	}
	
	public function setKeynames(&$oXml) {
		
		
		$this->aProduct['_items_authors_keynames'][$this->iAuthor] = $this->getTextNodeValue($oXml);
		
		if (strstr($this->aProduct['_items_authors'][$this->iAuthor], $this->aProduct['_items_authors_keynames'][$this->iAuthor])) {
			$sAutor = str_replace($this->aProduct['_items_authors_keynames'][$this->iAuthor], '', $this->aProduct['_items_authors'][$this->iAuthor]);
			$sAutor = $this->aProduct['_items_authors_keynames'][$this->iAuthor].' '.$sAutor;
			$sAutor = trim($sAutor);
			$this->aProduct['_items_authors'][$this->iAuthor] = $sAutor;
		}
	}
	
	public function setEditionnumber(&$oXml) {
		$iEdition = intval($this->getTextNodeValue($oXml));
		if ($iEdition > 0) {
			$this->aProduct['edition'] = $iEdition;
		}
	}
	
	public function setNumberofpages(&$oXml) {
		$iPages = intval($this->getTextNodeValue($oXml));
		if ($iPages > 0) {
			$this->aProduct['pages'] = $iPages;
		}
	}
	
	public function setBicmainsubject(&$oXml) {
		
		$sCatCode = $this->getTextNodeValue($oXml);
		$aMapping = $this->getABEMapping($sCatCode, $this->iSourceId);
		
		if ($aMapping['name'] != '' && $aMapping['source_category_id'] != '') {
			$this->aProduct['_items_category'] = array(
				$aMapping['source_category_id'] => $aMapping['name']
			);
		} else {
			return false;
		}
	}
	
	public function setBicversion(&$oXml) {
		
		$iVersion = $this->getTextNodeValue($oXml);
		if ($iVersion != '2') {
			$this->addErrorLog('UWAGA zmieniona została wersja ONIX : '.$this->aContenerField['type']);
			unset($this);
			return false;
		}
	}
	
	public function setOthertext(&$oXml) {}
	public function setTexttypecode(&$oXml) {
		$this->aContenerField = array();
		$this->aContenerField['type'] = $this->getTextNodeValue($oXml);
	}
	
	public function setText(&$oXml) {
		
		$sTextFormat = $oXml->getAttribute('textformat');
		if ($sTextFormat == '01') {
			switch ($this->aContenerField['type']) {
				case '01':
					$sDesc = eregi_replace('<a.*</a>','', $this->clearAsciiCrap($this->getTextNodeValue($oXml)));
					$sDesc = eregi_replace('<script.*</script>','', $sDesc);
					$this->aProduct['description'] = $sDesc;
					$this->aProduct['short_description'] = $sDesc;
				break;
				case '04':
					$sDesc = eregi_replace('<a.*</a>','', $this->clearAsciiCrap($this->getTextNodeValue($oXml)));
					$sDesc = eregi_replace('<script.*</script>','', $sDesc);
					if ($sDesc != '') {
						$this->aProduct['description'] = $this->aProduct['description'].'<br /><br /><strong>Spis treści:</strong><br />'.$sDesc;
					}
				break;
				default:
					$this->addErrorLog('Nie wspierany typ autora "Contributorrole" : '.$this->aContenerField['type']);
					unset($this);
					return false;
				break;
			}
		} else {
			$this->addLog("Nie obsługiwany format tekstu ".$sTextFormat." w tagu text");
		}
	}
	
	public function setMediafile(&$oXml) {}
	public function setMediafiletypecode(&$oXml) {
		$this->aContenerField = array();
		$this->aContenerField['type'] = $this->getTextNodeValue($oXml);
	}
	public function setMediafileformatcode(&$oXml) {
		$this->aContenerField['format'] = $this->getTextNodeValue($oXml);
	}
	public function setMediafilelinktypecode(&$oXml) {
		$this->aContenerField['link_type'] = $this->getTextNodeValue($oXml);
	}
	public function setMediafilelink(&$oXml) {
		
		if ($this->aContenerField['type'] == '04' &&
				$this->aContenerField['format'] == '03' && 
				$this->aContenerField['link_type'] == '01') {
			
			// adres okładki, +fix
			$this->aProduct['_img'] = str_replace('html/b2b/', '', $this->getTextNodeValue($oXml));
			$this->aProduct['_img'] = str_replace('http://b2b.abe.pl/', '', $this->aProduct['_img']);
		} else {
			unset($this->aContenerField);
			$this->addErrorLog("Nie osługiwany typy tagów".
												 " MediaFileTypeCode:".$this->aContenerField['type'].
												 " MediaFileFormatCode:".$this->aContenerField['format'].
												 " MediaFileLinkTypeCode:".$this->aContenerField['link_type']
												);
		}
	}
	
	public function setPublisher(&$oXml) {}
	public function setPublishingrole(&$oXml) {
		$this->aContenerField = array();
		$this->aContenerField['role'] = $this->getTextNodeValue($oXml);
	}
	public function setNamecodetype(&$oXml) {
		$this->aContenerField['type'] = $this->getTextNodeValue($oXml);
	}
	public function setNamecodevalue(&$oXml) {
		$this->aContenerField['value'] = $this->getTextNodeValue($oXml);
	}
	public function setPublishername(&$oXml) {
		/*
     * Wydawcy zaczeli być dziwnie opisywani bez podanych tagów namecodevalue i namecodetype i publishing role
     * $this->aContenerField['role'] == '01' && 
				$this->aContenerField['type'] == '01' &&
				$this->aContenerField['value'] != ''
     */
    $sPublisher = $this->getTextNodeValue($oXml);
		if ( $sPublisher != '') {
			// czyścimy ze slashy jakiś dziwnych ponieważ:
			//	- http://stackoverflow.com/questions/3235219/urlencoded-forward-slash-is-breaking-url
			$this->aProduct['_publisher'] = str_replace('/', '', $sPublisher);
		} else {
			$this->addErrorLog("Nie osługiwany typy tagów".
												 " Publishingrole:".$this->aContenerField['role'].
												 " Namecodetype:".$this->aContenerField['type'].
												 " Namecodevalue:".$this->aContenerField['value']
												);
      unset($this->aContenerField);
			return false;
		}
	}
	public function setPublicationdate(&$oXml) {
		$aMatches = array();
		
		$sPub = $this->getTextNodeValue($oXml);
		if ($sPub != '') {
			preg_match("/(\d{4})/", $sPub, $aMatches);
			if ($aMatches[1] > 1) {
				$this->aProduct['publication_year'] = intval($aMatches[1]);
			}
		}
	}
	
	public function setMeasure(&$oXml) {}
	public function setMeasuretypecode(&$oXml) {
		$this->aContenerField = array();
		$this->aContenerField['type'] = $this->getTextNodeValue($oXml);
	}
	public function setMeasurement(&$oXml) {
		$this->aContenerField['value'] = $this->getTextNodeValue($oXml);
	}
	public function setMeasureunitcode(&$oXml) {
		
		$sUnit = $this->getTextNodeValue($oXml);
		if ($sUnit == 'kg' &&
				$this->aContenerField['type'] == '08' &&
				$this->aContenerField['value'] != '') {
			$this->aProduct['weight'] = $this->aContenerField['value'];
		} else {
			unset($this->aContenerField);
			$this->addErrorLog("Nie obsługiwane typy tagów ".
												 " Measuretypecode:".$this->aContenerField['type'].
												 " Measurement:".$this->aContenerField['value'].
												 " Measureunitcode:".$sUnit
												);
			return false;
		}
	}
	
	public function setSupplydetail(&$oXml) {}
	public function setSuppliername(&$oXml) {}
	
	public function setProductavailability(&$oXml) {
		global $pDbMgr;
    
		$sPAval = $this->getTextNodeValue($oXml);
		if ($sPAval == '20' || $sPAval == '21') {
			
			$this->aProduct['prod_status'] = '1';
		} elseif ($sPAval == '22') {
			
			$this->aProduct['prod_status'] = '1';
			$this->aProduct['shipment_time'] = '3';
      
      $this->aProduct['published'] = isset($this->aProduct['published']) ? $this->aProduct['published'] : '1';
      
      // uwaga wyłączone zostały produkty na zamówienie
      //$iPId = $this->checkExists($this->aProduct);
      $aProductItemData = $this->refMatchProductByISBNs($this->aProduct, array('id', 'source'));
      if ($aProductItemData['id'] > 0 && $aProductItemData['source'] == '3') {
        // dodanie do shadowa
        include_once('modules/m_oferta_produktowa/Module_Common.class.php');
        $pProductsCommon = new Module_Common();
        
        $aValues = array('published' => '0', 'shipment_time' => '3', 'prod_status' => '0');
        $pDbMgr->Update('profit24', 'products', $aValues, ' id = ' . $aProductItemData['id'].' LIMIT 1');
        $pProductsCommon->deleteShadow($aProductItemData['id']);
        $pProductsCommon->unpublishProductTags($aProductItemData['id']);
      }
      unset($this->aProduct);
      return false;
		} elseif ($sPAval == '31' || $sPAval == '40' || $sPAval == '99'){
			$this->aProduct['prod_status'] = '0';
		} else {
			$this->aProduct['prod_status'] = '0';
//			$this->addErrorLog("Produkt nie obsługiwany kod dostępności 'Productavailability' : ".$sPAval);
		}
	}
	
	// ---------------------------------------------------------------------------
	/**
<Stock>
	<LocationIdentifier>
		<LocationIDType>01</LocationIDType>
		<IDValue>ABE</IDValue>
	</LocationIdentifier>
	<StockQuantityCoded>
		<StockQuantityCodeType>01</StockQuantityCodeType>
		<StockQuantityCode>JEST</StockQuantityCode>
	</StockQuantityCoded>
</Stock>
	 */
	public function setStock(&$oXml) {
		
		$this->aContenerField['ident'] = $oXml->name;
		$this->iStock++;
		$this->aProduct['_stock_abe'] = array(
				$this->iStock => array()
		); // na wstępie czyścimy
	}
	
	public function setLocationidentifier(&$oXml) {}
	
	public function setLocationidtype(&$oXml) {
		
		$iTagType = $this->getTextNodeValue($oXml);
		if ($iTagType == '01') {
			$this->aProduct['_stock_abe'][$this->iStock]['type'] = $iTagType;
		} else {
			$this->addErrorLog('Nie wspierany typ tagu "Locationidtype" : '.$iTagType);
			unset($this);
			return false;
		}
	}
	
	public function setStockquantitycoded(&$oXml) {}
	
	public function setStockquantitycodetype(&$oXml) {
		
		$iTagType = $this->getTextNodeValue($oXml);
		if ($iTagType == '01') {
			$this->aProduct['_stock_abe'][$this->iStock]['code_type'] = $iTagType;
		} else {
			$this->addErrorLog('Nie wspierany typ tagu "TextNodeValue" : '.$iTagType);
			unset($this);
			return false;
		}
	}
	
	public function setStockquantitycode(&$oXml) {
		
		$sTagType = $this->getTextNodeValue($oXml);
		if ($sTagType == 'JEST') {
			$this->aProduct['_stock_abe'][$this->iStock]['quantity_type'] = $sTagType;
		} else {
			$this->addErrorLog('Nie wspierany typ tagu "Stockquantitycode" : '.$sTagType);
			unset($this);
			return false;
		}
	}
	// ---------------------------------------------------------------------------
	
	
	public function setPrice(&$oXml) {}
	
	public function setPricetypecode(&$oXml) {
		$this->sContenerPriceType = $this->getTextNodeValue($oXml);
		$this->aContenerPrice[$this->sContenerPriceType] = array();
		$this->aContenerPrice[$this->sContenerPriceType]['type'] = $this->sContenerPriceType;
	}
	public function setPriceamount(&$oXml) {
		$this->aContenerPrice[$this->sContenerPriceType]['amount'] = $this->getTextNodeValue($oXml);
	}
	public function setCurrencycode(&$oXml) {
		$this->aContenerPrice[$this->sContenerPriceType]['currency'] = $this->getTextNodeValue($oXml);
	}
	
  public function setDiscountCoded(&$oXml) {}
  public function setDiscountCodeType(&$oXml) {}
  public function setDiscountCode(&$oXml) {}
  
	public function setTaxratecode1(&$oXml) {}
	
	public function setTaxratepercent1(&$oXml) {
		$this->aContenerPrice[$this->sContenerPriceType]['vat'] = $this->getTextNodeValue($oXml);
	}
	public function setTaxableamount1(&$oXml) {}
	public function setTaxamount1(&$oXml) {}

	public function clearProductData() {
		unset($this->aProduct);
		unset($this->iAuthor);
		unset($this->iStock);
		unset($this->aContenerField);
	}
	
	public function postParse() {

		/*
		 * XXX TODO odkomentować jeśli ABE poprawi autorów
		if (!empty($this->aProduct['_items_authors']) && is_array($this->aProduct['_items_authors'])) {
			$this->aProduct['_items_authors'] = implode(',', $this->aProduct['_items_authors']);
		}
		*/
		$this->aProduct['_items_authors'] = $this->aProduct['_items_authors'][1];
		if (trim($this->aProduct['isbn_plain']) == '') {
			return false;
		}
		
		// status
		if (!empty($this->aProduct['_stock_abe']) && (!isset($this->aProduct['shipment_time']) || $this->aProduct['shipment_time'] == '')) {
			foreach ($this->aProduct['_stock_abe'] as $aStock) {
				if ($aStock['value'] == 'ABE') {
					$this->aProduct['shipment_time'] = '1'; // głowna
				} elseif (substr($aStock['value'], 0, 1) == 'F') {
					$this->aProduct['shipment_time'] = '2'; // filia
				} else {
					$this->addErrorLog("Wystąpił błąd definiowania czasu wysyłki - 'idValue', 'Stock', wartość : ".$aStock['value']);
					unset($this);
					return false;
				}
			}
			if (empty($this->aProduct['_stock_abe'])) {
				$this->aProduct['prod_status'] = '0';
				$this->aProduct['shipment_time'] = '3';
        // uwaga wyłączone zostały produkty na zamówienie
        unset($this->aProduct);
        return false;
			}
			unset($this->aProduct['_stock_abe']);// nie potrzebne
		}
		
		// cena
		if (isset($this->aContenerPrice['02']) && 
				!empty($this->aContenerPrice['02']) &&
				$this->aContenerPrice['02']['currency'] == 'PLN' &&
				$this->aContenerPrice['02']['amount'] > 0.00 &&
				$this->aContenerPrice['02']['vat'] > 0) {
			
			$this->aProduct['price_brutto'] = $this->aContenerPrice['02']['amount'];
			$this->aProduct['vat'] = $this->aContenerPrice['02']['vat'];
		} else {
			$this->addErrorLog("Wystąpił błąd podczas definiowania cen");
			unset($this);
			return false;
		}
		
		if ($this->aProduct['price_brutto'] > 0.00 && $this->aProduct['vat'] > 0) {
			$this->aProduct['price_netto'] = Common::formatPrice2($this->aProduct['price_brutto']/(1+$this->aProduct['vat']/100));
		} else {
			return false;
		}
		return $this->aProduct;
	}
	
	
	/**
	 * Metoda pobiera symbole kategorii w źródle
	 * 
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param int $iSourceId
	 * @return array
	 */
	public function getSourceSymbolCategories($iSourceId) {
		global $aConfig, $pDbMgr;
    
    $sSql = "SELECT IF( LENGTH( source_symbol ) = 1, CONCAT( source_symbol,  '$' ) , source_symbol ) 
              FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
              WHERE source_id = ".$iSourceId."
              AND source_symbol <>  ''
              AND source_symbol IS NOT NULL 
              AND LENGTH( source_symbol ) <= 2";
		return $pDbMgr->GetCol('profit24', $sSql);
	}// end of getSourceSymbolCategories() method
	
	
	/**
	 * Metoda pobiera id źródła dla danego importu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sSourceSymbol - symbol źródła
	 * @return type 
	 */
	private function getSourceId($sSourceSymbol) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE symbol='".$sSourceSymbol."'";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getSourceId() method
	
	
	/**
	 * Trochę przydługa metoda zapisywania zdjęcia do produktu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iProductId
	 * @param type $iSourceBookId
	 * @param type $aProduct
	 * @return boolean 
	 */
	public function addImage($iProductId, $iSourceBookId, $aProduct) {
		global $aConfig, $pDbMgr;
		
		
		$this->createProductImageDir($iProductId);
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
		$sShortDir = 'okladki/'.$iRange.'/'.$iProductId;
		
		$sDstName = $aProduct['isbn_plain'].".jpg";
		$sSrcFile = $sDestDir."/_".$sDstName;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id=".$iProductId." AND photo='".$sDstName."' AND name='".$sDstName."'";
		$iId = $pDbMgr->GetOne('profit24', $sSql);
		if ($iId > 0) {
			// zdjecie juz istnieje w bazie nie dodajemy ponownie
			return true;
		}
		
		$iTimeOut = 0;
		
		$rFp = fopen($sSrcFile, "w");
		$bIsErr = !$this->getSourceFile($rFp, $aProduct['_img']);
		fclose ($rFp);
		
		if($bIsErr == true || !file_exists($sSrcFile))
			return false;

		$aFileInfo = getimagesize($sSrcFile);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
		$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
		$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
		if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
			$this->resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
		}
		unlink($sSrcFile);


		$aValues=array(
			'product_id' => $iProductId,
			'directory' => $sShortDir,
			'photo' => $sDstName,
			'mime' => 'image/jpeg',
			'name' => $sDstName,
			'order_by' => '1'
		);
		return ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
	}// end of addImage() method
	
	
 /**
	* Pobiera datę o 7 dni wcześniejszą od ostatniego pobrania
	* (7 dni wprowadzone zeby wyeliminowac mozliwe problemy z zgubieniem produktu
  * 
	* @return date
	*/
	public function getLastAbeUpdate() {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT DATE_SUB(last_new_abe_import, INTERVAL 2 DAY )
						 FROM ".$aConfig['tabls']['prefix']."products_import
						 LIMIT 0,1";	
		return $pDbMgr->GetOne('profit24', $sSql);
	} // end of getLastAbeUpdate() function
	
	
	/**
	 * Zapisuje czas ostatniego pobrania produktów z ABE
	 * 
	 * @param date $sLast - poprzednia data ostatniego pobrania produktów z abe 
	 * @return bool - success
	 */
	public function setLastAbeUpdate($sLast) {
		global $aConfig, $pDbMgr;

		if(empty($sLast)){
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."products_import (last_new_abe_import)
							 VALUES ( NOW())";
		} else {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_import
						 SET last_new_abe_import = NOW()
						 WHERE 1=1";
		}
		return $pDbMgr->Query('profit24', $sSql);
	} // end of setLastAbeUpdate() function
	
	
	/**
	 * Metoda zamienia kod oprawy na tekst opisujący oprawę
	 * 
	 * @param type $sCode
	 * @return string
	 */
	protected function ProductformMapper($sCode){
		$aMapper = array(
			'AA' => '',
			'AB' => '',
			'AC' => '',
			'AD' => '',
			'AE' => '',
			'AF' => '',
			'AG' => '',
			'AH' => '',
			'AI' => '',
			'AJ' => '',
			'AK' => '',
			'AL' => '',
			'AZ' => '',
			'DA' => '',
			'DB' => '',
			'DC' => '',
			'BA' => '',// 	Book 	Book – detail unspecified.
			'BB' => 'oprawa twarda',// 	Hardback 	Hardback or cased book.
			'BC' => 'oprawa miękka',// 	Paperback / softback 	Paperback or other softback book.
			//'BD' => '',// 	Loose-leaf 	Loose-leaf book.
			'BE' => 'spirala',// 	Spiral bound 	Spiral, comb or coil bound book.
			'BF' => 'broszura',// 	Pamphlet 	Pamphlet or brochure, stapled; German ‘geheftet’.
			'BG' => 'skóra',// 	Leather / fine binding 	
			//'BH' => '',// 	Board book 	Child’s book with all pages printed on board.
			'BI' => 'płucienna',// 	Rag book 	Child’s book with all pages printed on textile.
			'BJ' => 'drukowana na wodoodpornym materiale',// 	Bath book 	Child’s book printed on waterproof material.
			//'BK' => '',// 	Novelty book 	A book whose novelty consists wholly or partly in a format which cannot be described by any other available code – a ‘conventional’ format code is always to be preferred; one or more Product Form Detail codes, eg from the B2nn group, should be used whenever possible to provide additional description.
			//'BL' => '',// 	Slide bound 	Slide bound book.
			//'BM' => '',// 	Big book 	Extra-large format for teaching etc; this format and terminology may be specifically UK; required as a top-level differentiator.
			//'BN' => '',// 	Part-work (fascículo) 	A part-work issued with its own ISBN and intended to be collected and bound into a complete book.
			//'BO' => '',// 	Fold-out book or chart 	Concertina-folded book or chart, designed to fold to pocket or regular page size: use for German ‘Leporello’.
			'BP' => 'wykonana z pianki',// 	Foam book 	A children’s book whose cover and pages are made of foam.
			'BZ' => ''// 	Other book format 	Other book format or binding not specified by BB to BP.
		);
		if (isset($aMapper[$sCode])) {
			return $aMapper[$sCode];
		} else {
			$this->addLog('Produkt posiada kod okładki "ProductForm"
										 który nie został przetłumaczony, produkt został prawidłowo 
										 dodany z brakiem informacji o oprawie :'.$sCode);
		}
		return '';
	}// end of ProductformMapper() method
	
	
	/**
	 * Metoda pobiera kategorie ABE
	 * 
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param string $sCatCode
	 * @param integer $iSourceId
	 * @return array
	 */
	protected function getABEMapping($sCatCode, $iSourceId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_symbol = '".$sCatCode."' AND
									 source_id = ".$iSourceId;
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getABEMapping() method
	
	
	/**
	 * Metoda importuje XML'a i zapisuje na dysku
	 * 
	 * @return string - adres pliku XML z danymi
	 */
	public function importSaveXMLData($sCatBic, $sLastUpdate) {
		global $aConfig;
		
		$sFileName = date("siH_dmY").'_'.$sCatBic.'_books.XML';
		$sFilePath = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/abe/".date("Ymd");
		
		// dodajemy katalog z datą
		if (!file_exists($sFilePath) || !is_dir($sFilePath)) {
			if (mkdir($sFilePath) === false) {
				return false;
			}
		}
		$sFilePath .= '/'.$sFileName;
		
		$rFp = fopen($sFilePath, "w");
		$bIsErr = $this->getSourceFile($rFp, 'books.php?format=onix2&last_update='.$sLastUpdate.'&bic='.$sCatBic);
		fclose ($rFp);
		
		if ($bIsErr == true) {
			return $sFilePath;
		} else {
			return false;
		}
	}// end of importSaveXMLData() method
	
	
	/**
	 * Metoda pobiera ze źródła plik
	 *
	 * @param handle $rFileHandle - uchwyt pliku do którego chcemy zapisywać
	 * @param string $sFile - nazwa pliku który chcemy pobrać
	 * @return boolean 
	 */
	protected function getSourceFile(&$rFileHandle, $sFile) {
		global $aConfig;
		
		$sQuery = "http://b2b.abe.pl/".$sFile;
		
		if ($rFileHandle) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sQuery);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_USERPWD, $aConfig['import']['abe_login'].':'.$aConfig['import']['abe_password']);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFileHandle);

			if (curl_exec($oCURL) === false) {
				curl_close($oCURL);
				return false;
			}
			curl_close($oCURL);
			return true;
		}
		return false;
	}
	
	public function proceedInsert(&$oParent, $aProduct, $iSourceBId) {
		$oParent->proceedInsertProduct($aProduct, $iSourceBId);
	}
	
	public function proceedUpdate(&$oParent, &$aProduct, $iSourceBId, $iProductId) {
		// dodaj bookindeks dla produktu
		$sMD5 = $this->getProductMD5($aProduct);
		$mRetMD5 = $this->addBookIndeks($iProductId, $iSourceBId, $this->iSourceId, $sMD5);

    unset($this->aProduct['_language_code']);
		//unset($aProduct['prod_status']); // to niech się ustawia ze skryptu aktualizacji stanów
		// dodać aktualizację cen
		$this->proceedProductStock($this->sSoruceSymbol, $iProductId, $aProduct, true);
		if ($this->checkUpdateProduct($aProduct, $mRetMD5, $iProductId)) {
			$this->proceedUpdateProduct($aProduct, $iProductId, $iSourceBId);
		}
	}
	
	public function checkExists(&$aProduct) {
		
		return $this->refCheckProductExists($aProduct);
	}
}// end of productsElements() Class
?>