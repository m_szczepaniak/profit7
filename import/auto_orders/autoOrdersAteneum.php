<?php
/**
 * Test klasy Communicator
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */

$aConfig['config']['project_dir'] = 'import/auto_orders/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

use communicator\sources\Ateneum\Ateneum;
global $pDbMgr;

//TRYB TESTOWY 
if ($aConfig['common']['status'] == 'development') {
  $iTestMode = 2;
} else {
  $iTestMode = 0;
}

//MAKSYMALNA LICZBA ITERACJI!
$iMaxIteration = 4;

// wysyłka zamówień
$iteration=0;
$result = true;
while(($result !== false) && ($iteration < $iMaxIteration)) {
    $pConnectorAteneum = new communicator\Communicator('Ateneum', 1, $pDbMgr, $iTestMode);
    $result = $pConnectorAteneum->sendCMD('putOrder', array());
    $iteration++;
    unset($pConnectorAteneum);
}