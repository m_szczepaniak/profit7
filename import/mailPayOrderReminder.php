<?php
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['use_db_manager'] = '1';
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'] . '/lang/pl.php');

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

//ini_set('memory_limit', '128M');

// dolaczenie biblioteki funkcji modulu
include_once('modules/m_oferta_produktowa/client/Common.class.php');

$pSmarty->template_dir = $aConfig['common']['client_base_path'] . $aConfig['smarty']['template_dir'];
$pSmarty->compile_dir = $aConfig['common']['client_base_path'] . $aConfig['smarty']['compile_dir'];
$pSmarty->config_dir = $aConfig['common']['client_base_path'] . $aConfig['smarty']['config_dir'];
$pSmarty->cache_dir = $aConfig['common']['client_base_path'] . $aConfig['smarty']['cache_dir'];


function getDateAfterWorkingDays($iDate, $iDays, &$aFreeDays)
{
    global $aConfig;
    $iDaysCount = 0;
    // dopóki nie osiągniemy potrzebnej ilości dni
    while ($iDaysCount < $iDays) {
        // dodaj dzień
        $iDate += (24 * 60 * 60);
        $aDate = getdate($iDate);
        // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
        if ($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0, 0, 0, $aDate['mon'], $aDate['mday'], $aDate['year']), $aFreeDays)) {
            $iDaysCount++;
        }
    }
    return $iDate;
}

/**
 * Metoda ustawia konfigurację serwisu
 *
 * @global array $aConfig
 */
function getWebsiteSettingsS($sWebsite)
{
    global $aConfig, $pDbMgr;

    // pobieramy ustawienia
    $sSql = "SELECT *
						FROM " . $aConfig['tabls']['prefix'] . "website_settings
						LIMIT 1";
    return $pDbMgr->GetRow($sWebsite, $sSql);
}// end of getWebsiteSettingsS() method


/**
 * Metoda ustawia konfigurację serwisu
 *
 * @param string $sWebsite
 * @global array $aConfig
 * @global \DatabaseManager $pDbMgr
 */
function getSellerData($sWebsite)
{
    global $aConfig, $pDbMgr;

    // pobieramy ustawienia
    $sSql = "SELECT *
						FROM " . $aConfig['tabls']['prefix'] . "orders_seller_data
						LIMIT 1";
    return $pDbMgr->GetRow($sWebsite, $sSql);
}// end of getWebsiteSettingsS() method


/**
 * Metoda pobiera symbol serwisu na podstawie id serwisu
 *
 * @global type $aConfig
 * @param type $iWebsiteId
 * @return type
 */
function getWebsiteSymbol($iWebsiteId)
{
    global $aConfig;

    $sSql = "SELECT code FROM " . $aConfig['tabls']['prefix'] . "websites
						 WHERE id=" . $iWebsiteId;
    return Common::GetOne($sSql);
}// end of getWebsiteSymbol() method


/**
 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
 *
 * @global type $aConfig
 * @param type $iOId
 * @return type
 */
function getWebsiteId($iOId)
{
    global $aConfig;

    $sSql = "SELECT website_id FROM " . $aConfig['tabls']['prefix'] . "orders
						WHERE id=" . $iOId;
    return Common::GetOne($sSql);
}// end of getWebsiteId() method


/**
 * Zmienia dane z DB do edycji emaila
 *
 * @param array $aArray
 * @return array
 */
function formSellerData($aArray)
{

    foreach ($aArray as $key => $value) {
        $aArray['{' . $key . '}'] = $value;
        unset($aArray[$key]);
    }
    return $aArray;
}


$sSql = "SELECT *
				 	 FROM " . $aConfig['tabls']['prefix'] . "orders_mails_times
							 WHERE id = " . getModuleId('m_zamowienia');
$aMailSettingsServise['it'] =& $pDbMgr->GetRow('it', $sSql);
$aMailSettingsServise['profit24'] =& $pDbMgr->GetRow('profit24', $sSql);
$aMailSettingsServise['np'] =& $pDbMgr->GetRow('np', $sSql);
$aMailSettingsServise['mestro'] =& $pDbMgr->GetRow('mestro', $sSql);
$aMailSettingsServise['smarkacz'] =& $pDbMgr->GetRow('smarkacz', $sSql);
$aMailSettingsServise['nb'] =& $pDbMgr->GetRow('nb', $sSql);

// pobieramy tresc maila

$aBankMailServise['it'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'it');
$aBankMailServise['profit24'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'profit24');
$aBankMailServise['np'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'np');
$aBankMailServise['mestro'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'mestro');
$aBankMailServise['smarkacz'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'smarkacz');
$aBankMailServise['nb'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'nb');


// dni wolne
$sSql = "SELECT UNIX_TIMESTAMP(free_day)
							 FROM " . $aConfig['tabls']['prefix'] . "orders_free_days";
$aFreeDaysServise['it'] = $pDbMgr->GetCol('it', $sSql);
$aFreeDaysServise['profit24'] = $pDbMgr->GetCol('profit24', $sSql);
$aFreeDaysServise['np'] = $pDbMgr->GetCol('np', $sSql);
$aFreeDaysServise['mestro'] = $pDbMgr->GetCol('mestro', $sSql);
$aFreeDaysServise['smarkacz'] = $pDbMgr->GetCol('smarkacz', $sSql);
$aFreeDaysServise['nb'] = $pDbMgr->GetCol('nb', $sSql);


$aSellerDataServise['profit24'] = getSellerData('profit24');
$aSellerDataServise['it'] = getSellerData('it');
$aSellerDataServise['np'] = getSellerData('np');
$aSellerDataServise['mestro'] = getSellerData('mestro');
$aSellerDataServise['smarkacz'] = getSellerData('smarkacz');
$aSellerDataServise['nb'] = getSellerData('nb');


// pobieramy zamowienia z nierozeslnym opineo
$sSql = "SELECT DISTINCT O.id, O.*, DATE_FORMAT(O.order_date, '" . $aConfig['common']['sql_date_format'] . "') AS order_date, 
								UNIX_TIMESTAMP(O.status_1_update) AS status_1_update_stamp
					FROM " . $aConfig['tabls']['prefix'] . "orders AS O
          LEFT JOIN orders_items AS OI_TEST
          ON OI_TEST.order_id = O.id
            AND IF (OI_TEST.source = '51', OI_TEST.status <> '3' AND OI_TEST.status <> '4', OI_TEST.status <> '4')
            AND OI_TEST.deleted = '0'
            AND (OI_TEST.item_type = 'I' OR OI_TEST.item_type = 'P')
            AND OI_TEST.packet = '0'
					WHERE 
              OI_TEST.id IS NULL
            AND O.payment_status = '0'
            AND (O.payment_type = 'bank_transfer' OR O.payment_type = 'platnoscipl' OR O.payment_type = 'card') 
            AND (O.order_status = '1' OR O.order_status = '2')
            AND O.reminder_sent IS NULL
          ";
$aOrders = Common::GetAll($sSql);

if (!empty($aOrders)) {
    foreach ($aOrders as $iKey => $aOrder) {
        $sWebsite = getWebsiteSymbol(getWebsiteId($aOrder['id']));
        $aSettings =& getWebsiteSettingsS($sWebsite);
        $aConfig['_settings']['site'] =& $aSettings;

        $aBankMail = $aBankMailServise[$sWebsite];
        $aFreeDays = $aFreeDaysServise[$sWebsite];
        $aMailSettings = $aMailSettingsServise[$sWebsite];
        $aSellerData = formSellerData($aSellerDataServise[$sWebsite]);

        if ($aOrder['payment_type'] == 'bank_transfer') {
            if (getDateAfterWorkingDays($aOrder['status_1_update_stamp'], $aMailSettings['pay_reminder_days'], $aFreeDays) <= time()) {
                $aCols = array('{nr_zamowienia}', '{data_zamowienia}', '{dni_do_anulowania}', '{do_zaplaty}');
                $aVals = array($aOrder['order_number'], $aOrder['order_date'], $aMailSettings['cancel_days'] - $aMailSettings['pay_reminder_days'], $aOrder['to_pay']);

                $aCols = array_merge(array_keys($aSellerData), $aCols);
                $aVals = array_merge(array_values($aSellerData), $aVals);

                $sContent = str_replace($aCols, $aVals, $aBankMail['content']);

                echo $aOrder['email'];
                Common::sendWebsiteMail($aBankMail, $aSettings['name'], $aSettings['email'], $aOrder['email'], $aBankMail['content'], $aCols,
                    $aVals, array(), array(), $sWebsite);

                $aValues = array(
                    'reminder_sent' => 'NOW()'
                );
                Common::Update($aConfig['tabls']['prefix'] . "orders", $aValues, "id = " . $aOrder['id']);
            }
        }
    }
}
