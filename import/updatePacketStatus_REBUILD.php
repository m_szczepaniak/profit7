<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
/**
 * Skrypt Aktualizuje status Pakietu na podstawie kolumny upd_packet_navailable w tabeli products_packet_items
 * Jeśli upd_packet_navailable = '0' wtedy zmień prod_status = '0' w tabeli products dla tego produktu/pakietu
 * I ponownie zmień upd_packet_navailable = '1' aby nastepne wywołanie skryptu nie musiało aktualizować pakietu
 * 
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);
$bIsErr = false;

include_once ('import_func.inc.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module_Common.class.php');
$oCommon = new Module_Common();

/**
 * Funkcja sprawdza czy wszystkie składowe pakietu są opublikowane
 *
 * @global array $aConfig
 * @param type $iPacketId
 * @return boolean 
 */
function checkPacketItemsAvailable($iPacketId) {
	global $aConfig;
	
	// pobierz id składowych
	$sSql = "SELECT B.id FROM ".$aConfig['tabls']['prefix']."products_packets_items AS A
					 JOIN ".$aConfig['tabls']['prefix']."products AS B
						 ON A.product_id=B.id
					 WHERE A.packet_id=".$iPacketId." AND (prod_status<>'1' OR published<>'1')";
	$aPacketItems = Common::GetCol($sSql);
	
	if (!empty($aPacketItems)) {
		return false;
	}
	return true;
}// end of checkPacketItemsAvailable() function



/** PAKIETY KTÓRE MOGĄ ZOSTAĆ ZMIENIONE NA NIEDOSTĘPNE **/

$sSql = "SELECT DISTINCT packet_id FROM ".$aConfig['tabls']['prefix']."products_packets_items
				";// WHERE upd_packet_navailable='0'
$aPIds = Common::GetCol($sSql);

$iCount = 0;
foreach($aPIds as $iId){
	// zmieniamy status na niedostępny dla takiej pozycji
	$sSql="UPDATE ".$aConfig['tabls']['prefix']."products
				SET prod_status = '0'
				WHERE id = ".$iId;
	if(Common::Query($sSql) === false){
		$bIsErr = true;
		$sLogInfo .= "blad aktualizacji statusu pakietu na niedostępny dla pozycji [".$iId."] \n";
		echo "blad aktualizacji statusu pakietu na niedostępny dla pozycji [".$iId."] \n";
		break;
	}

	// ustawiamy ponownie flagę aktualizacji niedostepnych na 1 dla wszystkich elementow pakietu w products_packets_items
	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_packets_items
					 SET upd_packet_navailable = '1'
					 WHERE packet_id=".$iId;
	if(Common::Query($sSql) === false){
		$bIsErr = true;
		$sLogInfo .= "blad zmiany flagi aktualizacji niedostępnego pakietu [".$iId."] \n";
		echo "blad zmiany flagi aktualizacji niedostępnego pakietu [".$iId."] \n";
		break;
	}
	else {
		$sLogInfo .= "Zmieniono pakiet o id  ".$iId." na niedostepny \n";
		echo "Zmieniono pakiet o id  ".$iId." na niedostepny \n";
		$iCount++;
	}
	
	// aktualizacja cienia
	if ($oCommon->existShadow($iId)) {
		if ($oCommon->updateShadow($iId) === false) {
			$sLogInfo .= "blad aktualizacji shadowa pakietu [".$iId."] \n";
			echo "blad aktualizacji shadowa pakietu [".$iId."] \n";
			$bIsErr = true;
		}
	} 
}





/** PAKIETY KTÓRE MOGĄ ZOSTAĆ ZMIENIONE NA DOSTĘPNE **/
$sSql = "SELECT DISTINCT packet_id FROM ".$aConfig['tabls']['prefix']."products_packets_items
				 ";//WHERE upd_packet_navailable='2'";
$aPIds = Common::GetCol($sSql);

foreach($aPIds as $iId){
	// sprawdźmy czy pakiet posiada wszystkie składowe dostępne i opublikowane
	if (checkPacketItemsAvailable($iId) !== false) {
		// zmieniamy status na niedostępny dla takiej pozycji
		$sSql="UPDATE ".$aConfig['tabls']['prefix']."products
					SET prod_status = '1', published='1'
					WHERE id = ".$iId;
		if(Common::Query($sSql) === false){
			$bIsErr = true;
			$sLogInfo .= "blad aktualizacji statusu pakietu na dostepny dla pozycji [".$iId."] \n";
			echo "blad aktualizacji statusu pakietu na dostepny dla pozycji [".$iId."] \n";
			break;
		}

		// ustawiamy ponownie flagę aktualizacji niedostepnych na 1 dla wszystkich elementow pakietu w products_packets_items
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_packets_items
						SET upd_packet_navailable = '1'
						WHERE packet_id=".$iId;
		if(Common::Query($sSql) === false){
			$bIsErr = true;
			$sLogInfo .= "blad zmiany flagi aktualizacji dostepnego pakietu [".$iId."] \n";
			echo "blad zmiany flagi aktualizacji dostepnego pakietu [".$iId."] \n";
			break;
		}
		else {
			$sLogInfo .= "Zmieniono pakiet o id  ".$iId." na dostepny \n";
			echo "Zmieniono pakiet o id  ".$iId." na dostepny \n";
			$iCount++;
		}

		// aktualizacja cienia
		if (!$oCommon->existShadow($iId)) {
			if ($oCommon->addShadow($iId) === false) {
				$sLogInfo .= "blad dodawania shadowa pakietu [".$iId."] \n";
				echo "blad dodawania shadowa pakietu [".$iId."] \n";
				$bIsErr = true;
			}
		}  else {
			if ($oCommon->updateShadow($iId) === false) {
				$sLogInfo .= "blad aktualizacji shadowa pakietu [".$iId."] \n";
				echo "blad aktualizacji shadowa pakietu [".$iId."] \n";
				$bIsErr = true;
			}
		}
	}
}


echo "<b>Zmienionych pakietów ".$iCount."</b><br>\n";
$sLogInfo.="Zmienionych pakietów ".$iCount."  \n";

if($iCount>0 || $bIsErr == true){
	sendInfoMail("Odbudowanie pakietów",$sLogInfo);
}

?>