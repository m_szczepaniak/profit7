<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

$aParms = $argv; // parametr
$bLast = ($aParms[1] == 'last' ? true : false);

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
// zmienne
$iMId = 5;
$iPublisherID = 1;
$iDevLimit=0;
$sLogInfo='';
$bError=false;


//liczba znalezionych ksiazek
$iBooksFound=0;

include_once ('import_func.inc.php');
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
$oCommonSynchro->iSourceId = '2';

// dodanie do shadowa
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
$GLOBALS['pProductsCommon'] = new Module_Common();


/**
 * Funkcja pobiera id serii wydawniczej
 * 
 * @global array $aConfig
 * @param int $iProductId
 * @return int
 */
function getSeriesProductMapped($iProductId) {
	global $aConfig;
	
	$sSql = "SELECT series_id FROM ".$aConfig['tabls']['prefix']."products_to_series 
					 WHERE product_id = ".$iProductId;
	return Common::GetOne($sSql);
}// end of getSeriesProductMapped() method


/**
 * dodaje podukt do bufora importu azymut
 * @param $aValues - tablica z danymi produktu
 * @return void
 */
function addToAzymutBuffer($aValues){
	global $aConfig, $bError, $sLogInfo, $iBooksFound;
	
	Common::Insert($aConfig['tabls']['prefix']."products_azymut_buffer_copy", $aValues);
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_azymut_buffer", $aValues)) !== false) {
			// DODANO PRODUKT
			//dump( 'dodano ['.$iId.'] ['.$aValues['azymut_index'].'] '.$aValues['title']."\n");
			$sLogInfo.="Dodano [".$iId."] [".$aValues['azymut_index']."] - ".$aValues['title']."\n";
			++$iBooksFound;
			$bError=true;
	}
	else{
		// blad dodawania
		dump( 'blad dodawania: ['.$iId.'] ['.$aValues['azymut_index'].'] '.$aValues['title']."<br>\n");
		$sLogInfo.='blad dodawania: ['.$iId.'] ['.$aValues['azymut_index'].'] '.$aValues['title']."\n";
	}
} // end of addToAzymutBuffer() function


/**
 * parsuje xml dla dodania nowego produktu
 * @param $oXml
 * @return unknown_type
 */
function parseAzymutBook(&$oXml,$sAzymutId, $sPublicationType) {
global $sLogInfo, $oCommonSynchro;
	$sHTMLBeginDescription = '';
	
	$aValues = array();
	$aValues['azymut_index'] = $sAzymutId;
	$aValues['dzial'] = $sPublicationType;
	
	$aValues['created']='NOW()';
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'book':
						// ksiazka - KONIEC - dodanie do bazy
						$aValues['description'] = $sHTMLBeginDescription.$aValues['description'];
						$sHTMLBeginDescription = '';
						$sLogInfo.="Nie istnieje w buforze [0] [".$aValues['azymut_index']."] - ".$aValues['title']."\n";
            var_dump($aValues);die;
						addToAzymutBuffer($aValues);
						//dump($aValues);
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'autorzy':
						$aValues['authors'] = getTextNodeValue($oXml);
						break;
					case 'tytul':
						$aValues['title'] = getTextNodeValue($oXml);
					case 'wydawca':
						$aValues['publisher'] = getTextNodeValue($oXml);			
						break;
					case 'podtytul':
						$aValues['subtitle'] = getTextNodeValue($oXml);	
						break;
					case 'liczbatomo':
						$aValues['volumes'] = (int) getTextNodeValue($oXml);			
						break;
					case 'nrkolejnyt':
						$aValues['volume_nr'] =(int)  getTextNodeValue($oXml);			
						break;
					case 'tytultomu':
						$aValues['volume_title'] = getTextNodeValue($oXml);			
						break;
					case 'jezykoryg':
						$aValues['original_language'] = getTextNodeValue($oXml);
						break;
					case 'tytuloryg':
						$aValues['original_title'] = getTextNodeValue($oXml);
						break;
					case 'tlumacze':
						$aValues['translator'] = getTextNodeValue($oXml);
						break;
					case 'jezyki':
						$aValues['language'] = getTextNodeValue($oXml);
						break;
					case 'seriacykl':
						$aValues['seria'] = getTextNodeValue($oXml);
						break;
					case 'tematyka':
						$aValues['subject'] = getTextNodeValue($oXml);
						break;
					case 'kodwydawcy':
						$aValues['publisher_code'] = getTextNodeValue($oXml);			
						break;
					case 'wydanie':
						$aValues['edition'] = getTextNodeValue($oXml);			
						break;
					case 'rokwyd':
						$aValues['publication_year'] = (int) getTextNodeValue($oXml);
						break;
					case 'objetosc':
						$aValues['pages'] = (int) getTextNodeValue($oXml);
						break;
					case 'format':
						$aValues['dimension'] = getTextNodeValue($oXml);
						break;	
					case 'oprawa':
						$aValues['binding'] = getTextNodeValue($oXml);
						break;
					case 'ciezar':
						$aValues['weight'] = getTextNodeValue($oXml);
						break;
					case 'isbn':
						$aValues['isbn'] = getTextNodeValue($oXml);		
						$aValues['isbn_plain'] = isbn2plain($aValues['isbn']);		
            var_dump($aValues);
						break;	
					case 'opis':
						$sDesc = eregi_replace('<a.*</a>','', clearAsciiCrap(getTextNodeValue($oXml)));
						$sDesc = eregi_replace('<script.*</script>','', $sDesc);
						$aValues['description'] = $sDesc;
						break;
					case 'miejscowosc':
						$aValues['city'] = getTextNodeValue($oXml);
						break;
					case 'kod_paskowy':
						// dodane 20.07.2012 @Arkadiusz Golba 
						// na zlecenie Marcin Chudy podczas rozmowy skype 19.07.2012
						// 
						// modyfikacja ean_13 @Arkadiusz Golba 11.09.2012
						// rozbicie pól
						// 
						// jeśli nie wystąpił wcześniej isbn
						$aValues['ean_13'] = isbn2plain(getTextNodeValue($oXml));
						if (empty($aValues['isbn'])) {
							// to w pole isbn, isbn_plain ladują dane z kodu paskowego
							$aValues['isbn'] = $aValues['ean_13'];		
							$aValues['isbn_plain'] = $aValues['ean_13'];	
						}
						break;
					case 'typ_pub':
						$aValues['publication_type'] = getTextNodeValue($oXml);
						break;
					case 'image':
						$aValues['image'] = (int) getTextNodeValue($oXml);
						break;
					case 'kompl1':
						$aValues['komplet'] = getTextNodeValue($oXml);
						break;	
					case 'atrybuty':
						$aAttribs = parseAttribs($oXml);
						$sHTMLBeginDescription = getAudiobookHTML($aAttribs);
            var_dump($aValues);die;
						/*
						$oCommonSynchro->sendInfoMail("IMPROT AZYMUT OPIS PRODUKTU HTMLOWY ", "dodany html: ".$sHTMLBeginDescription
																																									." tablica atrybutów produktu ".print_r($aAttribs, true)
																																									." tablica danych produktu ".print_r($aValues, true));
						 */
					break;
				}
			break;
		}
	}
} // end of parseAddProduct() function



/**
 * Metoda generuje kod html audiobooka
 * 
 * @param array $aAttribs
 * @return string - HTML
 */
function getAudiobookHTML($aAttribs) {
	
	/*
	 Może się komuś przyda:
	    [czas_trwania] => 9:22:00
			[nosnik_glowny] => CD-MP3
			[nosnik_ilosc] => 8
			[items] => Array
				(
					[0] => Array
							(
								[lp] => 1
								[typ] => CD-MP3
								[name] => Harry Potter i kamieĹ? filozoficzny CD1
								[poz] => Array
									(
									[0] => Array
											(
													[nazwa] => RozdziaĹ? 1
											)

									[1] => Array
											(
													[nazwa] => RozdziaĹ? 2
											)

									[2] => Array
											(
													[nazwa] => RozdziaĹ? 3
											)
									)
							)
	*/
	$sHTML = '';
	if (!empty($aAttribs) && is_array($aAttribs)) {
		$sHTML = 
			'<div class="audiobook">
					<h2>Zawartość audiobooka:</h2>';
		if ($aAttribs['nosnik_glowny'] != '') {
			$sHTML .= '<span class="info">Rodzaj nośnika: </span><span class="info2">'.$aAttribs['nosnik_glowny'].'</span>';
		}
		if (intval($aAttribs['nosnik_ilosc']) > 0) {
			if ($aAttribs['nosnik_glowny'] != '') {
				$sHTML .= '<span class="sep">&nbsp;|</span> ';
			}
			$sHTML .= '<span class="info">Ilość nośników: </span><span class="info2">'.intval($aAttribs['nosnik_ilosc']).'</span>';
		}
		if ($aAttribs['czas_trwania'] != '') {
			if (intval($aAttribs['nosnik_ilosc']) > 0) {
				$sHTML .= '<span class="sep">&nbsp;|</span> ';
			}
			$sHTML .= '<span class="info">Czas trwania: </span><span class="info2">'.$aAttribs['czas_trwania'].'</span> ';
		}
		
		if (!empty($aAttribs['items']) && is_array($aAttribs['items'])) {
			$sHTML .= '<ul>';

			foreach ($aAttribs['items'] as $aItem) {
				$sHTML .= '<li>';
				
				if ($aItem['name'] != '') {
					$sHTML .= '<div>Nośnik '.($aItem['lp']).': <strong>'.($aItem['name']).'</strong></div>';
				}
				if ($aItem['typ'] != '') {
					$sHTML .= '<div>Typ ścieżki: <strong>'.($aItem['typ']).'</strong></div>';
				}
				
				if (!empty($aItem['poz']) && is_array($aItem['poz'])) {
					$sHTML .= '<ul>';
					foreach ($aItem['poz'] as $aPoz) {
						if ($aPoz['nazwa'] != '') {
							$sHTML .= '<li>'.$aPoz['nazwa'].'</li>';
						}
					}
					$sHTML .= '</ul>';
				}
				
				$sHTML .= '</li>';
			}
			$sHTML .= '</ul>';
		}
		
		$sHTML .= '</div>';
	}
	return $sHTML;
}// end of getAudiobookHTML() function


/**
 * Metoda parsuje atrybuty produktu
 * 	$aAttribs['nosnik_glowny'];
 *  $aAttribs['czas_trwania'];
 *  $aAttribs['nosnik_ilosc'];
 * 
 * @param type $oXml
 * @return array
 */
function parseAttribs(&$oXml) {
	
	$aAttribs = array();
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'atrybuty':
						// ksiazka - KONIEC - dodanie do bazy
						return $aAttribs;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'czas_trwania':
						$aAttribs['czas_trwania'] = getTextNodeValue($oXml);
					break;
					case 'nosnik_glowny':
						$aAttribs['nosnik_glowny'] = getTextNodeValue($oXml);
					break;
					case 'nosnik_ilosc':
						$aAttribs['nosnik_ilosc'] = (int) getTextNodeValue($oXml);
					break;
				
					case 'nosniki':
						$aAttribs['items'] = parseAttribsItems($oXml);
					break;
				
					default:
					break;
				}
		}
	}
} // end of parseAttribs() method

/**
 * Metoda parsuje nośniki 
 * 	$aAttribsItems['lp'];
 *  $aAttribsItems['typ'];
 *  $aAttribsItems['nazwa'];
 * 
 * @param type $oXml
 * @return array
 */
function parseAttribsItems(&$oXml) {
	$aAttribsItems = array();
	
	$iCountNośnik = 0;
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'nosniki':
						// ksiazka - KONIEC - dodanie do bazy
						return $aAttribsItems;
					break;
					case 'nosnik':
						$iCountNośnik++;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'nosnik':
						$aAttribsItems[$iCountNośnik]['lp'] = $oXml->getAttribute('lp');
						$aAttribsItems[$iCountNośnik]['typ'] = getNosnikTypeString($oXml->getAttribute('typ'));
					break;
					case 'nazwa':
						$aAttribsItems[$iCountNośnik]['name'] = getTextNodeValue($oXml);
					break;
				
					case 'poz':
						$aAttribsItems[$iCountNośnik]['poz'] = parseAttribsItemsPoz($oXml);
					break;
				
					default:
					break;
				}
		}
	}
}// end of parseAttribsItems() method


/**
 * Metoda parsuje rozdziały nośnika
 * $aAttribsItemsPoz['nazwa'];
 * 
 * @param type $oXml
 * @return array
 */
function parseAttribsItemsPoz(&$oXml) {

	$aAttribsItemsPoz = array();
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'poz':
						// ksiazka - KONIEC - dodanie do bazy
						return $aAttribsItemsPoz;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'nazwa':
						$aAttribsItemsPoz[]['nazwa'] = getTextNodeValue($oXml);
					break;
				
					default:
					break;
				}
		}
	}
}// end of parseAttribsItemsPoz() method


/**
 * Funkcja zwraca nazwe nośnika na podstawie typu
 * 
 * @param type $sType
 * @return string|null
 */
function getNosnikTypeString($sType) {

	$aTypes = array(
		'AB' => 'Kaseta audio',
		'AC' => 'CD-Audio',
		'ACM' => 'CD-MP3',
		'AG' => 'MiniDisc',
		'AH' => 'CD-Extra',
		'AI' => 'DVD Audio',
		'AIM' => 'DVD-MP3',
		'DB' => 'CD-ROM',
		'DC' => 'CD-I',
		'DI' => 'DVD-ROM',
		'DM' => 'Pendrive',
		'VI' => 'DVD Video',
		'VJ' => 'VHS Video',
		'VL' => 'VCD',
		'VM' => 'SVCD',
		'VN' => 'HD DVD',
		'VO' => 'Blu-ray',
	);
	if ($aTypes[$sType] != '') {
		return $aTypes[$sType];
	}
	return null;
}// end of getNosnikTypeString() function 


/**
 * Funkcja parsuje listę książek <book></book>
 * 
 * @global type $sLogInfo
 * @param type $oXml
 * @return int
 */
function parseBookList(&$oXml) {
	global $sLogInfo;	
	$iBooksFound = 0;

	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'books':
						// koniec listingu ksiazek
						$sLogInfo.='Koniec wezla books'."\n";
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'book': {
							// Index azymut
							$sAzymutId = $oXml->getAttribute('indeks');
							$sPublicatonType = $oXml->getAttribute('dzial');
							
							// jesli juz nie dodana ani nie ma jej w buforze
							//if(!existAzymutBook($sAzymutId) && !existAzymutBookInBuffer($sAzymutId))
							$sLogInfo.="Znaleziono [0] [".$sAzymutId."]\n";
			
							if(!existAzymutBookInBuffer($sAzymutId)) {
								parseAzymutBook($oXml,$sAzymutId, $sPublicatonType);
								$iBooksFound++;
							} else {
                echo 'istnieje w bufforze azymut';
              }
					}
					break;
				}
			break;
		}
	}
	return $iBooksFound;
}
// end of parseBookList() function

/**
 * Funkcja parsuje listę zapowiedzi <zapowiedzi></zapowiedzi>
 * 
 * @global array $aConfig
 * @global type $sLogInfo
 * @param type $oXml
 * @return boolean|int
 */
function parsePrevList(&$oXml) {
global $aConfig, $sLogInfo, $pDbMgr, $oCommonSynchro, $pProductsCommon;

	$iBooksFound = 0;
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'zapowiedzi':
						// koniec listingu ksiazek
						$sLogInfo .= 'Koniec wezla zapowiedzi'."\n";
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'zap': {
							$aMatches = array();
							// Index azymut
							$sAzymutId = $oXml->getAttribute('ind');
							$sDate = $oXml->getAttribute('data');
							preg_match("/^\d{4}-\d{2}-\d{2}$/", $sDate, $aMatches);
							// jesli juz nie dodana ani nie ma jej w buforze
							$sLogInfo .= "Znaleziono zapowiedź [".$iBooksFound."] [".$sAzymutId."]\n";
							unset($aPublisherCats, $iNewsMainCategory, $aSeriesCategoryTMP, $iPreviewsMainCategory);
							$iNewsMainCategory = 423; // domyslna wartosc
              $iPreviewsMainCategory = 2337;

							$aStatus = getAzymutStock($sAzymutId, 'A.azymut_shipment_date, B.shipment_time, B.publisher_id ');
							if (!empty($aStatus) && $aMatches[0] != '') {
								$aValues = array(); // czyścimy tutaj na wstępie
								$aValuesStatus = array();
								
								if ($aStatus['azymut_status'] != '3' && $aStatus['prod_status'] != '1' && $aStatus['shipment_time'] != '0') {
									
									// inny status, zmieniamy status
									$aValuesStatus = array(
											'azymut_status' => '3',
											'azymut_shipment_time' => '1'
									);
									
                  // jeśli nie ustawiono kategorii głownych
									if ($aStatus['publisher_id'] > 0) {
										$aPublisherCats = $oCommonSynchro->getPublisherCategories($aStatus['publisher_id']);
									}
									if (!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0) {
										// nadpisanie id głównej kategorii nowości
										$iNewsMainCategory = $aPublisherCats['news_category'];
										// wybranie id zapowiedzi
										$iPreviewsMainCategory = $oCommonSynchro->getPreviewPageIdByNewsPageId($iNewsMainCategory);
									}
									
									$iTmpSeriesId = getSeriesProductMapped($aStatus['id']);//getSeriesId($aStatus['id']);
									if (intval($iTmpSeriesId) > 0) {
										$aSeriesCategoryTMP = $oCommonSynchro->getSeriesCategories($iTmpSeriesId);
										if (intval($aSeriesCategoryTMP['news_category']) > 0) {
											// zmieniamy kategorię główną nowości
											$iNewsMainCategory = $aSeriesCategoryTMP['news_category'];
											$iPreviewsMainCategory = $oCommonSynchro->getPreviewPageIdByNewsPageId($iNewsMainCategory);
										}
									}
                  
									// to jest zapowiedź wywalamy z nowości
									$aPrevs = $oCommonSynchro->getPreviews($aStatus['id']);
									if (empty($aPrevs)) {
										$oCommonSynchro->addPreviews($aStatus['id'], $iPreviewsMainCategory);
									}
									
									// to jest zapowiedź wywalamy z nowości
									$aNews = $oCommonSynchro->getNews($aStatus['id']);
									if (!empty($aNews)) {
										$oCommonSynchro->delNews($aStatus['id']);
									}
								}
								
								if ($aStatus['azymut_shipment_date'] != $aMatches[0]) {
									$iDateDB = preg_replace('/^(\d{4})-(\d{2})-(\d{2})$/', '$1$2$3', $aStatus['azymut_shipment_date']);
									$iDateXML = preg_replace('/^(\d{4})-(\d{2})-(\d{2})$/', '$1$2$3', $aMatches[0]);
									if ($iDateDB == '' || $iDateXML != $iDateDB) {
										// data z XML jest wcześniejsza -> update
                    // data z XML jest różna
										$aValuesStatus['azymut_shipment_date'] = $aMatches[0];
									}
								}
								if (!empty($aValuesStatus)) {
									$aValues['last_import'] = 'NOW()';
									if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products", $aValues, ' id='.$aStatus['id']) === false) {
										$sLogInfo .= 'WYSTĄPIŁ BŁĄD KRYTYCZNY AKTUALIZACJI PRODUKTU '.$aStatus['id']."\n";
										echo 'WYSTĄPIŁ BŁĄD KRYTYCZNY AKTUALIZACJI PRODUKTU  '.$aStatus['id'];
										return false;
									}
								}
								if (!empty($aValuesStatus)) {
									$aValuesStatus['azymut_last_import'] = 'NOW()';
									if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aValuesStatus, ' id='.$aStatus['id']) === false) {
										$sLogInfo .= 'WYSTĄPIŁ BŁĄD KRYTYCZNY PODCZAS ZMIANY STANU NA ZAPOWIEDŹ '.$aMatches[0].' PRODUKTU '.$aStatus['id']."\n";
										echo 'WYSTĄPIŁ BŁĄD KRYTYCZNY PODCZAS ZMIANY STANU NA ZAPOWIEDŹ '.$aMatches[0].' PRODUKTU '.$aStatus['id'];
										return false;
									}
									
									// Aktualizacja shadowa
									if ($pProductsCommon->updateShadow($aStatus['id']) === false) {
										$sLogInfo .= "Wystąpił błąd podczas aktualizacji produktu w shadow, id = ".$aStatus['id']."\n";
										echo "Wystąpił błąd podczas aktualizacji produktu w shadow, id = ".$aStatus['id'];
										return false;
									}
								}
								
								$iBooksFound++;
							} else {
								$sLogInfo .= 'nie znalazłem produktu w bazie lub data jest nieprawidłowa bookindeks: '.$sAzymutId.', data: '.$sDate."\n";
								//echo 'nie znalazłem produktu w bazie lub data jest nieprawidłowa bookindeks: '.$sAzymutId.', data: '.$sDate."\n";
							}
					}
					break;
				}
			break;
		}
	}
	return $iBooksFound;
} // end of parsePrevList() function



/**
 * Funkcja parsuje listę nowosci <new></new>
 * 
 * @global array $aConfig
 * @global type $sLogInfo
 * @param type $oXml
 * @return boolean|int
 */
function parseNewsList(&$oXml) {
global $aConfig, $sLogInfo, $pDbMgr, $oCommonSynchro, $pProductsCommon;

	$iBooksFound = 0;
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'new':
						// koniec listingu ksiazek
						$sLogInfo .= 'Koniec wezla nowosci'."\n";
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'item': {
							$aMatches = array();
							// Index azymut
							$sAzymutId = $oXml->getAttribute('indeks');
							$sLogInfo .= "Znaleziono news [".$iBooksFound."] [".$sAzymutId."]\n";
							unset($aPublisherCats, $iNewsMainCategory, $aSeriesCategoryTMP, $iPreviewsMainCategory);
							$iNewsMainCategory = 423; // domyslna wartosc

							$aStatus = getAzymutStock($sAzymutId, ' B.publisher_id ');
							if (!empty($aStatus) && intval($aStatus['id']) > 0) {
								
								$aPreview = $oCommonSynchro->getPreviews($aStatus['id']);
								if (!empty($aPreview)) {
									// jeśli nie ustawiono kategorii głownych
									if ($aStatus['publisher_id'] > 0) {
										$aPublisherCats = $oCommonSynchro->getPublisherCategories($aStatus['publisher_id']);
									}
									if (!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0) {
										// nadpisanie id głównej kategorii nowości
										$iNewsMainCategory = $aPublisherCats['news_category'];
										// wybranie id zapowiedzi
										//$iPreviewsMainCategory = $oCommonSynchro->getPreviewPageIdByNewsPageId($iNewsMainCategory);
									}
									
									$iTmpSeriesId = getSeriesProductMapped($aStatus['id']);//getSeriesId($aStatus['id']);
									if (intval($iTmpSeriesId) > 0) {
										$aSeriesCategoryTMP = $oCommonSynchro->getSeriesCategories($iTmpSeriesId);
										if (intval($aSeriesCategoryTMP['news_category']) > 0) {
											// zmieniamy kategorię główną nowości
											$iNewsMainCategory = $aSeriesCategoryTMP['news_category'];
											//$iPreviewsMainCategory = $oCommonSynchro->getPreviewPageIdByNewsPageId($iNewsMainCategory);
										}
									}

									
									$oCommonSynchro->delPreviews($aStatus['id']);
									$aNews = $oCommonSynchro->getNews($aStatus['id']);
									if (empty($aNews)) {
										$oCommonSynchro->addNews($aStatus['id'], $iNewsMainCategory);
									}
								}
								
								if ($aStatus['azymut_status'] != '1') {
									// inny status, zmieniamy status
									$aValuesStatus = array(
											'azymut_status' => '1'
									);
								}

								if (!empty($aValuesStatus)) {
									$aValuesStatus['azymut_last_import'] = 'NOW()';
									if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."products_stock", $aValuesStatus, ' id='.$aStatus['id']) === false) {
										$sLogInfo .= 'WYSTĄPIŁ BŁĄD KRYTYCZNY PODCZAS ZMIANY STANU NA ZAPOWIEDŹ '.$aMatches[0].' PRODUKTU '.$aStatus['id']."\n";
										echo 'WYSTĄPIŁ BŁĄD KRYTYCZNY PODCZAS ZMIANY STANU NA ZAPOWIEDŹ '.$aMatches[0].' PRODUKTU '.$aStatus['id'];
										return false;
									}
								}
								
								// Aktualizacja shadowa
								if ($pProductsCommon->updateShadow($aStatus['id']) === false) {
									$sLogInfo .= "Wystąpił błąd podczas aktualizacji produktu w shadow, id = ".$aStatus['id']."\n";
									echo "Wystąpił błąd podczas aktualizacji produktu w shadow, id = ".$aStatus['id'];
									return false;
								}

								$iBooksFound++;
							} else {
								$sLogInfo .= 'nie znalazłem produktu w bazie '.$sAzymutId."\n";
								//echo 'nie znalazłem produktu w bazie lub data jest nieprawidłowa bookindeks: '.$sAzymutId.', data: '.$sDate."\n";
							}
					}
					break;
				}
			break;
		}
	}
	return $iBooksFound;
}
// end of parseNewsList() function



if (file_exists($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/once_75631803649KS.XML")) {
  $oXml = new XMLReader();
  $oXml->open($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/once_75631803649KS.XML");

  $sLogInfo.='START'."\n";

  while(@$oXml->read()) {
    switch ($oXml->nodeType) {
      case XMLReader::END_ELEMENT :
        switch ($oXml->name) {
          case 'stuff':
            // KONIEC XMLa
            $oXml->close();
            $sLogInfo.='STOP'."\n";
          break;
        }
      break;

      case XMLReader::ELEMENT :
        switch ($oXml->name) {
          case 'books':
            $sLogInfo.='Poczatek wezla books'."\n";
            $iBooksCounter = parseBookList($oXml);
          break;
        }
      break;
    }
  }
  // usuniecie pliku
  //@unlink('XML/azymut_towary.xml');
  //koniec pojedynczego kroku
  echo $sLogInfo;
} else {
  echo 'brak pliku';
}


?>