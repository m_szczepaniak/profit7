<?php
/**
 * Skrypt synchronizacji cen hurtowych źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importCSVController.class.php');	


$bTestMode = false; //  czy test mode
$iLimitIteration = 9999999;
$sSource = 'siodemka';
$sElement = 'pricesWH';
$oImportCSVController = new importCSVController($sSource, $sElement, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveCSVData();
}
else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'import/CSV/siodemka/324012_29052017_ceny_hurtowe.csv';
}

if ($sFilename !== false && $sFilename != '') {
    $aSupportedTags = array(
        'INDEKS', // numer kartoteki
        'NAZWASKR', // skrót nazwy
        'CENAN', // cena netto
        'VAT', // vat
        'GRUPA_NAZWA', // wydawnictwo
        'EAN', // ean
        'ISBN', // isbn
        'KARTOTEKA_GLOWNA', // ??
        'MAGAZYN'
    );

	// mamy CSV'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, "\t", true, $aSupportedTags);
  if (!empty($oImportCSVController->aErrorLog) || !empty($oImportCSVController->oSourceElements->aErrorLog)) {
    $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElement." ERR", print_r($oImportCSVController->aErrorLog, true).
                                                          print_r($oImportCSVController->oSourceElements->aErrorLog, true));
  }
} else {
	// TODO komunikat błędu pobierania/zapisu pliku CSV
	echo 'brak CSV';
}
	 
// TODO mail z informacją o imporcie

echo 'koniec';
