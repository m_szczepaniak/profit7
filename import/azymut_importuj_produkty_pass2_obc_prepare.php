<?php

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
// zmienne
$iMId = 5;
$iPublisherID = 1;
$iDevLimit=0;
$sLogInfo='';

$sTransactionCode='';

$iAzymutOldAge=14; // czas po ktorym produkty z azymuta bez kategorii sa dodawane jako 
									// nieposegregowane


include_once ('import_func.inc.php');
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
$oCommonSynchro->iSourceId = '2';

// dodanie do shadowa
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
$GLOBALS['pProductsCommon'] = new Module_Common();

$GLOBALS['iAzymutOBCSource'] = getSourceId('azymutobc');
$GLOBALS['iAzymutSource'] = getSourceId('azymut');

$changedProducts = [
	'updated' => [],
	'inserted' => []
];

$GLOBALS['aAzymutAllowedProductTypes'] = ['KS', 'CD', 'ZA', 'GR', 'AK', 'CZ', 'DV', 'GR', 'ME', 'NT'];
$GLOBALS['aAzymutAllowedDzial'] = ['AU', 'GPL', 'GK', 'MM', 'KC', 'GP', 'PU', 'ZBW'];

/**
 * Funkcja aktualizuje indeks azymutu 
 * 
 * Jeśli istnieje książka z podanym ISBN oraz różnym bookindeksem aktualizacja 
 * kolumny z bookindeksami oraz dopisanie nowego bookindeksu do wcześniej 
 * dodanej tabeli z listą bookindeksów książek.
 * 
 * Podczas realizacji zamówienia używany będzie tylko ten bookindeks który jest 
 * zdefiniowany przy produkcie, aktualizowany za każdym razem kiedy przyjdzie 
 * z azymutu z listą statusów książek. 
 * 
 * Tabela z historią bookindeksów będzie używana wyłącznie do sytuacji 
 * spornych w celu określenia czy dana książka występowała także 
 * pod innymi bookindeksami.
 *
 * @global boolean $bError
 * @global string $sLogInfo
 * @global int $iBooksFound
 * @param string $sISBN
 * @param array $aValues 
 */
function updateAzymutIndex($sPlainISBN, $sAzymutbookindex, $aBookChange) {
	global $bError,$sLogInfo, $aConfig;
	
	$aValues['azymut_index'] = $sAzymutbookindex;
	if (Common::Update($aConfig['tabls']['prefix']."products", $aValues, " id=".$aBookChange['id']) === false) {
		return false;
	}
	
	// stary niech pobierze i zapisze a nie nowy, nowy będzie w tabeli products
	$aValues = array();
	$aValues['product_id'] = $aBookChange['id'];
	$aValues['azymut_index'] = $aBookChange['azymut_index'];
	@Common::Insert($aConfig['tabls']['prefix']."products_azymut_bookindex", $aValues);
	return true;
}

/**
 * Metoda sprawdza czy przy podanym isbn istnieje inny bookindex, 
 *	jeśli tak należy zaktualizować bookindex na aktualny i dodać do tabeli z bookindexami ksiazek
 *
 * @global array $aConfig
 * @param string $sPlainISBN
 * @param string $sAzymutbookindex
 * @return bool 
 */
function azymutBookindexChangedId($sPlainISBN, $sAzymutbookindex) {
	global $aConfig;
	
	$sSql = "SELECT id, azymut_index FROM ".$aConfig['tabls']['prefix']."products
					 WHERE isbn_plain = '".$sPlainISBN."' AND azymut_index<>'".$sAzymutbookindex."'";
	return Common::GetRow($sSql);
}// end of azymutBookindexChanged() method


/**
 * Pobiera id serii odanej nazwie i wydawnictwie,
 * jesli nie istnieje dodaje ja do tabeli products_series
 * @param string $sSeries - nazwa poszukiwanej serii
 * @param int $iPublisherId - id wydawnictwa poszukiwanejc serii
 * @return int - id poszukiwanej serii/false
 */
function getSeriesId($sSeries,$iPublisherId){
	global $aConfig,$pDB,$sLogInfo;
	
	$sSql="SELECT id FROM products_series
				 WHERE publisher_id = ".$iPublisherId."
				 AND name = ".$pDB->quoteSmart(stripslashes($sSeries));
	$iId=Common::GetOne($sSql);
	if($iId > 0)
		return $iId;
	$aValues=array(
		'name'=>$sSeries,
		'publisher_id' => $iPublisherId,
		'created'=> 'NOW()',
		'created_by' => 'import'
	);
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_series", $aValues)) === false) {
		echo "error adding Series ".$sSeries."\n";
		$sLogInfo.= "error adding Series ".$sSeries."\n";
		dump($aValues);
		return false;
	}
	else {
		$sLogInfo.= "dodano serie ".$sSeries."\n";
		return $iId;
	}			
} // end of getSeriesId() function

/**
 * Dodaje powiązanie serii do produktu
 * @param int $PId - id produktu
 * @param string $sSeries - nazwa serii
 * @param int $iPublisherId - id wydawnictwa
 * @return bool - success
 */
function addSeries($PId,$sSeries,$iPublisherId){
global $aConfig,$sLogInfo;
	if(!empty($sSeries)){
			$aValues=array(
				'product_id'=>$PId,
				'series_id'=>getSeriesId($sSeries,$iPublisherId)
			);
			if (Common::Insert($aConfig['tabls']['prefix']."products_to_series", $aValues,'',false) === false) {
				echo "błąd dodawania serii do produktu ".$PId."<br>\n";
				$sLogInfo.= "błąd dodawania serii do produktu ".$PId."\n";
			dump($aValues);
				return false;
			}
	}
	return true;
} // end of addSeries() function


/**
 * Dodaje autorów do produktu
 * @param int $PId - id produktu
 * @param string $sAuthors - string z autorami otrzymany z azymut
 * @return bool - success
 */
function addAuthors($PId,$sAuthors){
	global $aConfig,$sLogInfo;
	$iAuthorRoleId=1;
	if(!empty($sAuthors)&&($sAuthors != '.')){
		$aAuthors=split(',',$sAuthors);
		foreach($aAuthors as $iKey=>$sAuth){
			$sAuth=trim($sAuth);
			if(!empty($sAuth)){
				$aValues=array(
					'product_id'=>$PId,
					'author_id'=>getAuthorId($sAuth),
					'role_id'=>$iAuthorRoleId,
					'order_by'=>$iKey
				);
				if (Common::Insert($aConfig['tabls']['prefix']."products_to_authors", $aValues,'',false) === false) {
					dump($aValues);
					dump($sAuth);
					echo "błąd dodawania autorów do produktu ".$PId."<br>\n";
					$sLogInfo.= "błąd dodawania autorów do produktu ".$PId." \n";
					//dump($aValues);
					return false;
				}
			}
		}
	}
	return true;
} // end of addAuthors() function


/**
 * Dodaje załącznik do produktu
 * 
 * @param int $PId - id produktu
 * @param string $sAttachmentAzymutIdx - indeks załącznika w azymut
 * @return bool - success
 */
function addAzymutAttachment($PId,$sAttachmentAzymutIdx){
	global $aConfig,$sLogInfo;
	$aBook=getAzymutBookFromBufferByIndex($sAttachmentAzymutIdx);
	if(!empty($aBook)){
		$iAttTypeId=getAttachmentTypeId(convertAzymutPublType($aBook['publication_type']));
		if($iAttTypeId > 0){
			$aValues=array(
				'attachment_type' => $iAttTypeId,
			  'product_id' => $PId,
				'azymut_index' => $sAttachmentAzymutIdx
			);
			if (($AId = Common::Insert($aConfig['tabls']['prefix']."products_attachments", $aValues)) === false) {
				echo "błąd dodawania załącznika dla produktu ".$PId."<br>\n";
				$sLogInfo.= "błąd dodawania załącznika dla produktu ".$PId."\n";
				//dump($aValues);
				return false;
			}
			else{
				echo "added attachment ".$AId." - ".$aBook['title']." for product ".$PId."\n";
				$sLogInfo.= "dodano załącznik ".$AId." - ".$aBook['title']." dla produktu ".$PId."\n";
			}
			 deleteFromBuffer($aBook['id']);
			return true;
		}
	}
	else return false;
} // end of addAuthors() function

/**
 * Dodaje produkt z bufora azymut do bazy
 * @param int $iId - id w buforze azymut
 * @param $aCategories
 * @param array $changedProducts
 * @internal param ref $array $aCategories - tabela z id kategorii w księgarni
 */
function addAzymutProductFromBuffer($iId,&$aCategories, array &$changedProducts){
global $aConfig, $sLogInfo, $oCommonSynchro;
	$aBook=getAzymutBookFromBuffer($iId);
	
	if(!empty($aBook['komplet'])){
		if($aBook['publication_type'] != 'KS' && $aBook['publication_type'] != 'CD'){
			//echo "ommiting ".$aBook['title']." - attachment <br>\n";
			$sLogInfo.="book ".$aBook['azymut_index']." will be ommited \n";
			return;
		}
	}

	$iPublisherId = getPublisherId($aBook['publisher']);
	
	$aValues=array(
		'page_id' => getPageId($aCategories),
  	'publisher_id' => $iPublisherId>0?$iPublisherId:'NULL',
		'azymut_index' => $aBook['azymut_index'],
	  'producer_code' => $aBook['publisher_code'],
	  'name' => $aBook['title'],
	  'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($aBook['title'], ' '))),
	  'name2' =>  !empty($aBook['subtitle'])?$aBook['subtitle']:'NULL',
	  'description' => $aBook['description'],
        'short_description' => trimString(strip_tags(br2nl($aBook['description'],' ')),200),
	  'published' => '1',
		'reviewed' => '1',
	  'weight' => !empty($aBook['weight'])?$aBook['weight']:'NULL', 
	  'isbn' => $aBook['isbn'], 
	  'isbn_plain' => $aBook['isbn_plain'],
		'ean_13' => $aBook['ean_13'] != '' ? $aBook['ean_13'] : 'NULL',
	  'publication_year' => !empty($aBook['publication_year'])?$aBook['publication_year']:'NULL',
	  'pages' => !empty($aBook['pages'])?$aBook['pages']:'NULL',
	  'binding' => getBindingId($aBook['binding']),
	  'language' => getLanguageId($aBook['language']),
	  'translator' => !empty($aBook['translator'])?$aBook['translator']:'NULL',
	  'volumes' => (int)$aBook['volumes'],
	  'volume_nr' => (int)$aBook['volume_nr'],
	  'edition' => (int)$aBook['edition'],
	  'volume_name' => !empty($aBook['volume_title'])?$aBook['volume_title']:'NULL',
	  'city' => !empty($aBook['city'])?$aBook['city']:'NULL',
	  'original_language' => getLanguageId($aBook['original_language']),
	  'original_title' => !empty($aBook['original_title'])?$aBook['original_title']:'NULL',
	  'created' => 'NOW()',
	  'created_by' => 'Azymut',
		'price_brutto' => '0.0',
		'source' => ($aBook['foreign_language_book'] == '1' ? '12' : '2'),
		'last_import' => 'NOW()',
		'prod_status'=> '0',
		'shipment_time' => $aBook['foreign_language_book'] == '1' ? '2' : '1',
    'azymut_foreign_language_book' => $aBook['foreign_language_book'] == '1' ? '1' : '0',
		'type' => $aBook['dzial'] == 'AU' ? '1' : '0' // AU to audiobook
	);
	$aValues = $oCommonSynchro->refProceedIsbn($aValues, true);
	
	$aPublisherCats = array();
	if($iPublisherId>0){
		$aPublisherCats = getPublisherCategories($iPublisherId);
		if(!empty($aPublisherCats) && $aPublisherCats['main_category'] > 0){
			$aValues['page_id'] = $aPublisherCats['main_category'];
		}
	}
	
	//kategorie powiazane do serii
	$aSeriesCats = array();
	if($iPublisherId > 0 && !empty($aBook['seria'])) {
		$iSeriesId = getSeriesId($aBook['seria'], $iPublisherId);
		$aSeriesCats = getSeriesCategories($iSeriesId);
		if(!empty($aSeriesCats) && $aSeriesCats['main_category'] > 0){
			// nadpisanie zeby nie bylo problemow dalej
			$aPublisherCats['main_category'] = $aSeriesCats['main_category'];
			if(!empty($aPublisherCats) && $aPublisherCats['main_category'] > 0){
				$aValues['page_id'] = $aPublisherCats['main_category'];
			}
		}
		if(!empty($aSeriesCats) && $aSeriesCats['news_category'] > 0){
			//nadpisanie zeby nie bylo problemow dalej
			$aPublisherCats['news_category'] = $aSeriesCats['news_category'];
		}	
	}
	
	if($aValues['page_id'] == $aConfig['import']['unsorted_category']) { //  mod 06.06.2012 || !checkLevel2Cats($aCategories)
		$aValues['published'] = '0';
	}
	
	if ($oCommonSynchro->checkUniqueRef($aValues)) {
    if (($PId = Common::Insert($aConfig['tabls']['prefix']."products", $aValues)) === false) {
      echo "błąd dodawania produktu <br>\n";
      $sLogInfo.= "błąd dodawania produktu ".$aValues['azymut_index']." ".$aValues['name']." \n";
      //dump($aValues);
      return false;
    }
    else{
      //echo "dodano produkt ".$PId." - ".$aValues['name']."<br>\n";
      $sLogInfo.= "dodano produkt ".$PId." - ".$aValues['azymut_index']." ".$aValues['name']."\n";
      if(!empty($aBook['publication_year'])){
        addYearToDict(intval($aBook['publication_year']));
      }

      if(!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0){
        $iNewsMainCategory = $aPublisherCats['news_category'];
      } else {
        $iNewsMainCategory = 423;
      }
      $oCommonSynchro->proceedProductFlags('azymut', $PId, $aValues['publication_year'], false, $aValues['prod_status'], NULL, $iNewsMainCategory);



      addAuthors($PId,$aBook['authors']);
      if($aBook['image']=='1'){
        if(!addAzymutImage($PId,$aBook['azymut_index'],$aBook['isbn_plain']))
          $sLogInfo.= "nie udało się pobrać okładki produktu ".$PId." - ".$aValues['name']."\n";
      }
      if($iPublisherId>0 && !empty($aBook['seria'])) {
        addSeries($PId,$aBook['seria'],$iPublisherId);
      }
      addExtraCategories($PId,$aCategories);
      if(!empty($aBook['komplet']) && ($aBook['publication_type']=='KS')){
        addAzymutAttachment($PId,$aBook['komplet']);
      }

		addUpdateGameAttribs($PId, $aBook['product_age'], $aBook['sex']);
		$changedProducts['inserted'][] = $PId;
    }
  } else {
    $sLogInfo .= "Nie unikatowy: ".$aValues['azymut_index'];
  }
  deleteFromBuffer($iId);
	
} // end of addAzymutProductFromBuffer()


/**
 * @param $iProductId
 * @param $age
 * @param $sex
 * @return mixed
 */
function addUpdateGameAttribs($iProductId, $age, $sex) {
	global $aConfig;

	$aValues = [
		'product_id' => $iProductId,
		'sex' => $sex,
		'age' => $age,
		'modified' => 'NOW()',
		'modified_by' => 'auto-azymut'
	];

	$sSql = 'SELECT id FROM products_game_attributes WHERE product_id = '.$iProductId;
	$iGId = Common::GetOne($sSql);

	if ($iGId > 0 ) {
		return Common::Update('products_game_attributes', $aValues, ' id = '.$iGId);
	} else {
		return Common::Insert('products_game_attributes', $aValues);
	}
}

/**
 * 
 * @global type $pDbMgr
 * @global type $oCommonSynchro
 * @param string $sCategoryCode
 * @return array
 */
function getProductCategoryMappingsBySubject($sCategoryCode) {
  global $pDbMgr, $iAzymutOBCSource;

  $sSql = 'SELECT page_id 
           FROM menus_items_mappings AS MIM
           JOIN menus_items_mappings_categories AS MIMC
            ON MIM.source_id = MIMC.source_id 
            AND MIMC.source_category_id = MIM.source_category_id
            AND MIMC.source_id = '.$iAzymutOBCSource.'
            AND MIMC.source_symbol = "'.$sCategoryCode.'"
            ';
  return $pDbMgr->GetCol('profit24', $sSql);
}

/**
 *
 * @global type $pDbMgr
 * @global type $oCommonSynchro
 * @param string $sCategoryCode
 * @return array
 */
function getProductCategoryMappingsById($iCategoryId) {
	global $pDbMgr, $oCommonSynchro;

	$sSql = 'SELECT page_id
           FROM menus_items_mappings AS MIM
           WHERE
            MIM.source_category_id = "'.$iCategoryId.'"
            AND MIM.source_id = '.$oCommonSynchro->iSourceId.'
            ';
	return $pDbMgr->GetCol('profit24', $sSql);
}

/**
 * @param $sAttributes
 * @return array
 */
function getCategoriesFromAttributes($sAttributes) {

	$aCategoriesMapped = [];
	$aCategories = unserialize(base64_decode($sAttributes));
	if (!empty($aCategories)) {
		foreach ($aCategories as $aCategory) {
			$aCats = getProductCategoryMappingsById($aCategory['id']);
			if (!empty($aCats)) {
				$aCategoriesMapped = array_merge($aCategoriesMapped, $aCats);
			}
		}
	}
	return $aCategoriesMapped;
}
/**
 * Dodaje produkt aktualizuje z bufora azymut do bazy
 * @param int $iId - id w buforze azymut
 * @param $PId
 * @param $sAzymutIndex
 * @param $aFound
 * @param $changedProducts
 * @internal param ref $array $aCategories - tabela z id kategorii w księgarni
 */
function updateAzymutProductFromBuffer($iId, $PId, $sAzymutIndex, $aFound, &$changedProducts){
global $aConfig, $sLogInfo, $oCommonSynchro, $pProductsCommon, $aAzymutAllowedProductTypes;

	
	$aCategories = array();
	// jesli sa kategorie azymut pobierz ich mapowania

	$aBook = getAzymutBookFromBuffer($iId);

	if ($aBook['publication_type'] != 'KS' && $aBook['publication_type'] != 'AU' && $aBook['attributes'] != '') {
		// probujemy mapować wyłącznie po kategoriach z Attr
		$aCategories = getCategoriesFromAttributes($aBook['attributes']);

	}

	if (empty($aCategories)) {
		if ($aBook['foreign_language_book'] == '1') {
			$aCategories = getProductCategoryMappingsBySubject($aBook['subject']);
		} else {
			$aCats = getAzymutCats($sAzymutIndex);
			if (!empty($aCats)) {
				$aCategories = getAzymutMappings($aCats);
			}
		}
	}

	if (empty($aBook)) {
		return false;
	}
	
	if(!empty($aBook['komplet'])){
		if (!in_array($aBook['publication_type'], $aAzymutAllowedProductTypes)) {
			echo "ommiting ".$aBook['title']." - attachment <br>\n";
			$sLogInfo.="book ".$aBook['azymut_index']." will be ommited \n";
			return;
		}
	}

	$iPublisherId = getPublisherId($aBook['publisher']);
	$aValues = array(
		//'page_id' => getPageId($aCategories),
  	//'publisher_id' => $iPublisherId>0?$iPublisherId:'NULL',
		'azymut_index' => $aBook['azymut_index'],
	  'producer_code' => $aBook['publisher_code'],
	  'name' => $aBook['title'],
	  'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($aBook['title'], ' '))),
	  'name2' =>  !empty($aBook['subtitle'])?$aBook['subtitle']:'NULL',
	  'description' => $aBook['description'],
        'short_description' => trimString(strip_tags(br2nl($aBook['description'],' ')),200),
		'reviewed' => '1',
	  'weight' => !empty($aBook['weight'])?$aBook['weight']:'NULL', 
	  'isbn' => $aBook['isbn'], 
		'ean_13' => $aBook['ean_13'] != '' ? $aBook['ean_13'] : 'NULL', 
	  'isbn_plain' => $aBook['isbn_plain'],
	  'publication_year' => !empty($aBook['publication_year'])?$aBook['publication_year']:'NULL',
	  'pages' => !empty($aBook['pages'])?$aBook['pages']:'NULL',
	  'binding' => getBindingId($aBook['binding']),
	  'language' => getLanguageId($aBook['language']),
	  'translator' => !empty($aBook['translator'])?$aBook['translator']:'NULL',
	  'volumes' => (int)$aBook['volumes'],
	  'volume_nr' => (int)$aBook['volume_nr'],
	  'edition' => (int)$aBook['edition'],
	  'volume_name' => !empty($aBook['volume_title'])?$aBook['volume_title']:'NULL',
	  'city' => !empty($aBook['city'])?$aBook['city']:'NULL',
	  'original_language' => getLanguageId($aBook['original_language']),
	  'original_title' => !empty($aBook['original_title'])?$aBook['original_title']:'NULL',
		'last_import' => 'NOW()',
		'type' => $aBook['dzial'] == 'AU' ? '1' : '0', // AU to audiobook,
    'azymut_foreign_language_book' => $aBook['foreign_language_book'] == '1' ? '1' : '0'
	);
	$aValues = $oCommonSynchro->refProceedIsbn($aValues, false);
  if ($oCommonSynchro->checkUniqueRef($aValues, $PId)) {
    if (Common::Update($aConfig['tabls']['prefix']."products", $aValues, ' id='.$PId) === false) {
      $oCommonSynchro->onUpdateErr($PId, $aValues);
      echo "błąd zmiany produktu <br> ".$PId.' '.$aValues['azymut_index']." ".$aValues['name']." \n";
      $sLogInfo.= "błąd zmiany produktu ".$PId.' '.$aValues['azymut_index']." ".$aValues['name']." \n";
      //dump($aValues);
      return false;
    }
    else{
      echo "zmieniono produkt ".$PId." - ".$aValues['name']."<br>\n";
      $sLogInfo.= "zmieniono produkt ".$PId." - ".$aValues['azymut_index']." ".$aValues['name']."\n";
      if (!empty($aBook['publication_year'])) {
        addYearToDict(intval($aBook['publication_year']));
      }

      $aPublisherCats = array();
      if($iPublisherId > 0){
        $aPublisherCats = getPublisherCategories($iPublisherId);
      }

      //kategorie powiazane do serii
      $aSeriesCats=array();
      if($iPublisherId>0 && !empty($aBook['seria'])) {
        $iSeriesId=getSeriesId($aBook['seria'],$iPublisherId);
        $aSeriesCats = getSeriesCategories($iSeriesId);
        if(!empty($aSeriesCats) && $aSeriesCats['news_category'] > 0){
          //nadpisanie zeby nie bylo problemow dalej
          $aPublisherCats['news_category'] = $aSeriesCats['news_category'];
        }	
      }

      if (!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0) {
        $iNewsMainCategory = $aPublisherCats['news_category'];
      } else {
        $iNewsMainCategory = 423;
      }
      $oCommonSynchro->proceedProductFlags('azymut', $PId, $aValues['publication_year'], true, $aValues['prod_status'], $aFound['shipment_date'], $iNewsMainCategory);

      if (trim(getAuthors($PId)) == '') {
        addAuthors($PId,$aBook['authors']);
      }
      if ($aBook['image'] == '1') {
        if (!addAzymutImage($PId, $aBook['azymut_index'], $aBook['isbn_plain']))
          $sLogInfo.= "nie udało się pobrać okładki produktu ".$PId." - ".$aValues['name']."\n";
      }
      if ($iPublisherId > 0 && !empty($aBook['seria'])) {
        addSeries($PId, $aBook['seria'], $iPublisherId);
      }
      // dodawanie kategorii
      if (!empty($aCategories)) {
        addExtraCategories($PId, $aCategories);
      }

      if (!empty($aBook['komplet']) && ($aBook['publication_type']=='KS')) {
        addAzymutAttachment($PId, $aBook['komplet']);
      }

      if ($aFound['published'] == '1') {

        // Aktualizacja shadowa
        if ($pProductsCommon->updateShadow($PId) === false) {
          $sLogInfo .= "Wystąpił błąd podczas aktualizacji produktu w shadow, ".$aValues['azymut_index'];
          return false;
        }
        if ($pProductsCommon->updateShadowAuthors($PId) === false) {
          $sLogInfo .= "Wystąpił błąd podczas aktualizacji autorów produktu w shadow, ".$aValues['azymut_index'];
          return false;
        }
        if ($pProductsCommon->updateShadowTags($PId) === false) {
          $sLogInfo .= "Wystąpił błąd podczas aktualizacji tagów produktu w shadow, ".$aValues['azymut_index'];
          return false;
        }
      }
		addUpdateGameAttribs($PId, $aBook['age'], $aBook['sex']);
		$changedProducts['updated'][] = $PId;
    }
  } else {
    $sLogInfo .= "Nie unikatowy: ".$aValues['azymut_index'];
  }
  deleteFromBuffer($iId);
} // end of updateAzymutProductFromBuffer()



createAzymutImagesDir();
$oBuffer=getAzymutBuffer();

$iItems=0;
$iDevLimiterC=0;
$iDevLimiter=100;
$iAdded=0;
$sLogInfo.='START'."\n";
while ($aBufferItem =& $oBuffer->fetchRow(DB_FETCHMODE_ASSOC)) {
  
	if ((!in_array($aBufferItem['publication_type'], $aAzymutAllowedProductTypes)) ||
		(!in_array($aBufferItem['dzial'], $aAzymutAllowedDzial))
		  ) {
		// nie importujemy innych niż audiobooki i ksiażki i czasopisma
		deleteFromBuffer($aBufferItem['id']);
	} else {
		//$aBookChange = array();
		// czy juz przypadkiem nie zostala dodana z azymuta
		$sLogInfo.='book form bufor: '.$aBufferItem['azymut_index'].' isbn:'.$aBufferItem['isbn_plain']."\n";
		if (!existAzymutBook($aBufferItem['azymut_index'])) {

			// dopasujmy produkt
			$aFound = $oCommonSynchro->refMatchProductByISBNs($aBufferItem, array('id', 'azymut_index', 'prod_status', 'published', 'source', 'publisher_id', 'shipment_date'));
			$sLogInfo.="book ".$aBufferItem['azymut_index']." is not in azymut table\n";
			// wyslij maila co 10000 dodanych produktów
			if($iItems > 10000) {
				 sendInfoMail("Wykonano import produktow z bufora Azymut - testowe logi dodatkowe",$sLogInfo);
				 $sLogInfo='';
				 $iItems=0;
			}
		//		if($iDevLimiterC>$iDevLimiter) {
		//			sendInfoMail("Wykonano import produktow z bufora Azymut",$sLogInfo);
		//			die("dev limiter");
		//		}

			if (!empty($aBufferItem['isbn_plain'])) {

				if ($aFound['id'] > 0) {

					if (empty($aFound['azymut_index'])) {

						// jesli tak - dodaj do niej kod produktu azymut
						$sLogInfo.="book ".$aBufferItem['azymut_index']." - inserted azymut code\n";
						addAzymutIndexToBook($aFound['id'],$aBufferItem['azymut_index'],$aBufferItem['id']);

						if ($oCommonSynchro->checkUpdateProduct($aFound, 2)) {
							updateAzymutProductFromBuffer($aBufferItem['id'], $aFound['id'], $aBufferItem['azymut_index'], $aFound, $changedProducts);
						}
					} elseif ($aBufferItem['azymut_index'] != $aFound['azymut_index']) {

						// zmien azymut bookindex
						if (updateAzymutIndex($aBufferItem['isbn_plain'], $aBufferItem['azymut_index'], $aFound) === false) {
							// błąd aktualizacji azymut bookindexu
							$sLogInfo .= 'Wystąpił błąd podczas aktualizacji azymutbookindexu: '.print_r($aBufferItem, $aFound, true);
						} else {

							$sLogInfo .= 'Zmieniono azymutbookindex: '.$aBufferItem['isbn_plain']." - ".$aBufferItem['azymut_index'];
						}
						updateLastImport(getProductIdByBookindex($aBufferItem['azymut_index']));
						// nic więcej nie rób w tej pętli
						deleteFromBuffer($aBufferItem['id']);

					} else {

						// ksiazka z tym isbn powtarza sie w azymut (czasopismo) - usuwamy z bufora
						deleteFromBuffer($aBufferItem['id']);
						// aktualizuj 
						updateLastImport($aFound['id']);
						//echo "book ".$aBufferItem['azymut_index']." repeats isbn ".$aBufferItem['isbn_plain']." of book ".$aFound['id']."<br>\n";
						$sLogInfo.="book ".$aBufferItem['azymut_index']." repeats isbn ".$aBufferItem['isbn_plain']." of book ".$aFound['id']."\n";
					}
				 }
				else {

					// nie ma ksiazki w bazie
					// pobierz kategorie azymuta dla niej

					$sLogInfo.="book ".$aBufferItem['azymut_index']." is new\n";
					$aMappings=array();
					// jesli sa kategorie azymut pobierz ich mapowania
          if ($aBufferItem['foreign_language_book'] == '1') {
            $aMappings = getProductCategoryMappingsBySubject($aBufferItem['subject']);
          } else {
            $aCats=getAzymutCats($aBufferItem['azymut_index']);
            if(!empty($aCats)){
              $aMappings=getAzymutMappings($aCats);
              if(empty($aMappings)){
                $aMappings[]=$aConfig['import']['unsorted_category'];
                $sLogInfo.="book ".$aBufferItem['azymut_index']." mapping is unsorted\n";
              }
            }
            // jesli nie ma kategorii i ksiazka lezy w buforze powyzej $iAzymutOldAge dni
            // przypisz jej kategorie 'nieuporzadkowane'
            elseif($aBufferItem['age'] > $iAzymutOldAge){
              $aMappings[]=$aConfig['import']['unsorted_category'];
            }
          }

					// jesli sa mapowania dodaj do bazy
					if(!empty($aMappings)){
						$iItems++;
						$iAdded++;
						$iDevLimiterC++;

						$sLogInfo.="adding book ".$aBufferItem['azymut_index']."\n";
						addAzymutProductFromBuffer($aBufferItem['id'], $aMappings, $changedProducts);
					} else {
						$sLogInfo.=" brak mapowań produktu \n";
					}
				}
			}
			else
				$sLogInfo.="book ".$aBufferItem['azymut_index']." have empty isbn:".$aBufferItem['isbn_plain']."\n";
		} else {
			$aFound = $oCommonSynchro->refMatchProductByISBNs($aBufferItem, array('id, modiefied_by_service', 'shipment_date', 'published'));
			if ($aFound['id'] > 0 && $oCommonSynchro->checkUpdateProduct($aFound, 2)) {
				updateAzymutProductFromBuffer($aBufferItem['id'], $aFound['id'], $aBufferItem['azymut_index'], $aFound, $changedProducts);
			}
			// aktualizuj produkt dla podanego bookindexu
			updateLastImport(getProductIdByBookindex($aBufferItem['azymut_index']));
			//echo "book ".$aBufferItem['azymut_index']." alreday in table<br>\n";
			deleteFromBuffer($aBufferItem['id']);
			$sLogInfo.="book ".$aBufferItem['azymut_index']." alreday in table\n";
		}
	}
}
unset($oBuffer);
echo "<b>Dodano ".$iAdded."</b><br/>\n";
$sLogInfo.="Dodano ".$iAdded."\n";
if(!empty($sLogInfo)){
	dump($sLogInfo);
	sendInfoMail("Wykonano import produktow z bufora Azymut - testowe logi dodatkowe",$sLogInfo);
}

?>