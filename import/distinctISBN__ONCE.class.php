<?php
/**
 * Skrypt porządkujący ISBN'y
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');	
$oCommonSynchro = new CommonSynchro();

// Sprawdzenie duplikatów
/*

SELECT COUNT( isbn_plain ) , isbn_plain, GROUP_CONCAT(id SEPARATOR  ',') 
FROM  `products` 
WHERE packet = '0'
GROUP BY isbn_plain
HAVING COUNT( isbn_plain ) >1

-- dlugosic isbnow, nie potrzebne przy synchro
SELECT LENGTH( isbn_plain ) 
FROM  `products`
WHERE 
isbn_10 IS NULL AND
isbn_13 IS NULL AND
ean_13 IS NULL
ORDER BY id DESC

**** 13 **
 
SELECT * 
FROM products
WHERE isbn_10 IS NULL 
AND isbn_13 IS NULL 
AND ean_13 IS NULL 
AND LENGTH( isbn_plain ) =13
AND isbn_plain
REGEXP  '^[0-9]{12}[0-9X]{1}$'
AND packet = '0'

// update
UPDATE products
SET isbn_13 = isbn_plain
WHERE
isbn_10 IS NULL 
AND isbn_13 IS NULL 
AND ean_13 IS NULL 
AND LENGTH( isbn_plain ) = 13
AND isbn_plain
REGEXP  '^[0-9]{13}$'
AND packet = '0'

**** 13 **

SELECT * 
FROM products
WHERE isbn_10 IS NULL 
AND isbn_13 IS NULL 
AND ean_13 IS NULL 
AND LENGTH( isbn_plain ) = 10
AND isbn_plain
REGEXP  '^[0-9]{9}[0-9X]{1}$'
AND packet = '0'


// zmiana

UPDATE products
SET isbn_10 = isbn_plain
WHERE isbn_10 IS NULL 
AND isbn_13 IS NULL 
AND ean_13 IS NULL 
AND LENGTH( isbn_plain ) = 10
AND isbn_plain
REGEXP  '^[0-9]{9}[0-9X]{1}$'
AND packet = '0'




ANALOGICZNIE W SHADOW

UPDATE products_shadow
SET isbn_10 = isbn_plain
WHERE isbn_10 IS NULL 
AND isbn_13 IS NULL 
AND ean_13 IS NULL 
AND LENGTH( isbn_plain ) = 10
AND isbn_plain
REGEXP  '^[0-9]{9}[0-9X]{1}$'
AND packet = '0'

UPDATE products_shadow
SET isbn_13 = isbn_plain
WHERE
isbn_10 IS NULL 
AND isbn_13 IS NULL 
AND ean_13 IS NULL 
AND LENGTH( isbn_plain ) = 13
AND isbn_plain
REGEXP  '^[0-9]{13}$'
AND packet = '0'
 
-- usunięcei duplikatów z shadowa
DELETE FROM products_shadow
WHERE id IN (
SELECT id FROM
(
	SELECT id
	FROM  `products_shadow` 
	WHERE packet =  '0'
	GROUP BY isbn_plain
	HAVING COUNT( isbn_plain ) >1
	) AS TMP
)
*/

?>