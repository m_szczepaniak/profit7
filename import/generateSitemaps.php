<?php
use LIB\Assets\sitemapGenerateListing;

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
$aConfig['_tmp']['location_prefix']='../';


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';

$fPriority = 0.8; // priorytet pozycji produktu
$sUpdate = 'daily'; // częstotliwośc aktualizacji
$iMaxPerFile = 45000; // ilosc pozycji w jednym pliku sitemapy
$bGzip = true; // czy gzipować pliki sitemapy
global $pDbMgr;

include_once ('import_func.inc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/inc/functions.inc.php');

/**
 * Otwiera xml sitemapy i wpisuje nagłówek
 * @param string $sName - nazwa pliku xml
 * @return file pointer
 */
function initFile($sName){
global $aConfig;
if (!($fp = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$sName, 'w'))) {
   echo "Nie mogę otworzyc pliku ".$sName." do zapisu";
   die();
	} 
	
	$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
';
	fwrite($fp,$sXml);
	return $fp;
} // end of initFile() function

/**
 * dopisuje stopke i zamyka xml sitemapy
 * @param $fp - file pointer
 * @return void
 */
function closeFile($fp){
global $aConfig;
	$sXml = '</urlset>
	';
	fwrite($fp,$sXml);
	fclose($fp);
} // end of closeFile() function

/**
 * gzipuje plik xml sitemapy
 * @param $iNr - nr pliku sitemapy
 * @return void
 */
function gzipSitemap($iNr){
	global $aConfig;
	if(system("gzip -c ".$_SERVER['DOCUMENT_ROOT'].'/sitemap'.$iNr.'.xml > '.$_SERVER['DOCUMENT_ROOT'].'/sitemap'.$iNr.'.xml.gz') === false){
		die('gzip fail!');
	}
} // end of gzipSitemap() function

/**
 * Utwórz link do produktu
 * 
 * @global array $aConfig
 * @param int $iId
 * @param string $sName
 * @return string
 */
function createFixedProductLink($iId, $sName='') {
	global $aConfig;
	
	$sPageLink .= '/';
	if($sName != ''){
		if(mb_strlen($sName, 'UTF-8') > 210) {
			$sPageLink .= mb_substr(link_encode($sName),0,210, 'UTF-8').'%2C';
		} else {
			$sPageLink .= link_encode($sName).'%2C';
		}
	} else {
		$sPageLink .= '%2C';
	}
	$sPageLink .= 'product'.$iId;
	$sPageLink .= '.html';
	return $sPageLink; 
} // end of createFixedProductLink() function


/**
 * dodaj wpis sitemapy do xmla indeksu sitemap
 * @param $fpIdx - file pointer
 * @param $iNr - nr pliku sitemapy
 * @return void
 */
function addSitemap($fpIdx,$iNr){
	global $aConfig,$bGzip;
	fwrite($fpIdx, "	<sitemap>\n");
	fwrite($fpIdx, "		<loc>".$aConfig['common']['base_url_http'].'sitemap'.$iNr.'.xml'.($bGzip?'.gz':'')."</loc>\n");
	fwrite($fpIdx, "	</sitemap>\n");
} // end of addSitemap() function


	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, IFNULL(IF(last_import > DATE(modified), last_import, DATE(modified)),DATE(created)) AS lastmod
					 FROM ".$aConfig['tabls']['prefix']."products A
					 WHERE A.published = '1'
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
		
	if (!($fpIdx = fopen($_SERVER['DOCUMENT_ROOT'].'/sitemaps.xml', 'w'))) {
   echo "Nie mogę otworzyc pliku sitemaps.xml do zapisu";
   die();
	} else {
	
	$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
';
	fwrite($fpIdx,$sXml);
	addSitemap($fpIdx,1);
	
	$iCount = 0;
	$iFileCount = 1;
	$fp = initFile(_('sitemap').$iFileCount.'.xml');
	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
			
		fwrite($fp, "	<url>\n");
		fwrite($fp, "		<loc>".$aConfig['common']['base_url_http_no_slash'].createFixedProductLink($aItem['id'], $aItem['plain_name'])."</loc>\n");
		fwrite($fp, "		<lastmod>".$aItem['lastmod']."</lastmod>\n");
		fwrite($fp, "		<changefreq>".$sUpdate."</changefreq>\n");
		fwrite($fp, "		<priority>".$fPriority."</priority>\n");
		fwrite($fp, "	</url>\n");
		fflush($fp);
		if(++$iCount >= $iMaxPerFile){
			$iCount = 0;
			closeFile($fp);
			// gzip
			if($bGzip){
				gzipSitemap($iFileCount);
			}
			++$iFileCount;
			$fp = initFile('sitemap'.$iFileCount.'.xml');
			addSitemap($fpIdx,$iFileCount);
		}
	}


	$sitemapGenerateListing = new sitemapGenerateListing($pDbMgr);
	$itemsListing = $sitemapGenerateListing->getItems();
	foreach ($itemsListing as $aItem) {

		fwrite($fp, "	<url>\n");
		fwrite($fp, "		<loc>".$aConfig['common']['base_url_http_no_slash'].$aItem['link']."</loc>\n");
		fwrite($fp, "		<lastmod>".date('Y-m-d')."</lastmod>\n");
		fwrite($fp, "		<changefreq>".$sUpdate."</changefreq>\n");
		fwrite($fp, "		<priority>".$fPriority."</priority>\n");
		fwrite($fp, "	</url>\n");
		fflush($fp);
		if(++$iCount >= $iMaxPerFile){
			$iCount = 0;
			closeFile($fp);
			// gzip
			if($bGzip){
				gzipSitemap($iFileCount);
			}
			++$iFileCount;
			$fp = initFile('sitemap'.$iFileCount.'.xml');
			addSitemap($fpIdx,$iFileCount);
		}
	}

	closeFile($fp);
	// gzip
	if($bGzip){
		gzipSitemap($iFileCount);
	}
	addSitemap($fpIdx,$iFileCount);
	$sXml = '</sitemapindex>
';
	fwrite($fpIdx,$sXml);
	fclose($fpIdx);
	
	}
	

?>
