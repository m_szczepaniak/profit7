<?php
/**
 * Skrypt i klasa importu paczkomatów do tabeli paczkomaty_machines
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-22 
 * @copyrights Marcin Chudy - Profit24.pl
 */

header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/paczkomaty/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(90800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '1G');

/**
 * Klasa importu paczkomatów do tabeli paczkomaty_machines
 * 
 */
class importPaczkomaty {
  const sPaczkomatyLibFilePath = 'paczkomaty/inpost.php';
  const sPaczkomatyCacheFilePath = 'omniaCMS/lib/paczkomaty/data/cache1.dat';

  function __construct() {}
  
  
  /**
   * Metoda pobiera listę paczkomatów
   * 
   * @return boolean
   * @throws Exception
   */
  public function downloadMachines() {
    
    include_once(self::sPaczkomatyLibFilePath);
    if (inpost_get_machine_list() == false){
      throw new Exception("Wystąpił błąd podczas pobierania listy paczkomatów");
    }
    return true;
  }// end of downloadMachines() method
  
  
  /**
   * Metoda dodaje paczkomaty do bazy danych, a stare usuwa
   * 
   * @throws Exception
   */
  public function insertMachines() {
    
    $aMachines = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].self::sPaczkomatyCacheFilePath));
    if (!empty($aMachines) && is_array($aMachines)) {
      if ($this->_truncateMachine() === false) {
        throw new Exception('Wystąpił błąd podczas czyszczenia tabeli paczkomatów');
      }
      foreach ($aMachines as $aMachine) {
        if ($this->_insertMachine($aMachine) === false) {
          throw new Exception('Wystąpił błąd podczas dodawania paczkomatu');
        }
      }
    } else {
      throw new Exception('Wystąpił błąd podczas pobierania listy paczkomatów - pusta tablica paczkomatów');
    }
  }// end of  insertMachines(0 method
  
  
  /**
   * Metoda czyści tabelę paczkomatow
   * 
   * @global object $pDbMgr
   * @return boolean
   * @throws Exception
   */
  private function _truncateMachine() {
    global $pDbMgr;
    
    $sSql = "TRUNCATE TABLE `paczkomaty_machines`";
    if (@$pDbMgr->Query('profit24', $sSql) === false) {
      $sSql = "DELETE FROM `paczkomaty_machines`";
      if ($pDbMgr->Query('profit24', $sSql) === false) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }// end of _truncateMachine(0 method
  
  
  /**
   * Dodaje paczkomat do tabeli
   * 
   * @global object $pDbMgr
   * @param type $aMachine
   */
  private function _insertMachine($aMachine) {
    global $pDbMgr;
    
	if (count($aMachine) < 9) {
		// bledny paczkomat
		return true;
	}
    $aValues = array(
        '`code`' => $aMachine[0],
        '`desc`' => $aMachine[3].' '.$aMachine[4].' '.$aMachine[1].' '.$aMachine[2],
        'postal' => intval(str_replace('-', '', $aMachine[3])),
        'data' => serialize($aMachine)
    );
    if ($pDbMgr->Insert('profit24', 'paczkomaty_machines', $aValues, '', false) === false) {
      return false;
    }
    return true;
  }// end of _insertMachine() method
}// end of Class


$oPaczkomaty = new importPaczkomaty();
// na wstępie aktualuzujemy paczkomaty
try {
  $oPaczkomaty->downloadMachines();
  $oPaczkomaty->insertMachines();
} catch (Exception $exc) {
  sendInfoMail('importPaczkomaty',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." wystąpił błąd podczas importu paczkomatów ".$exc->getTraceAsString());
  echo $exc->getTraceAsString();
}
