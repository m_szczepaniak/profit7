<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
global $pDbMgr;

$transportFedEx = 1;

$sSql = 'SELECT O.transport_number
         FROM orders_transport_buffer AS OTB
         JOIN orders AS O
            ON O.id = OTB.order_id
         WHERE OTB.transport_id = '.$transportFedEx.'
         ';
$transportNumbersFedEx = $pDbMgr->GetCol('profit24', $sSql);
$transportNumbersFedEx[] = '6231286361262';
$oShipment = new \orders\Shipment($transportFedEx, $pDbMgr);
$result = $oShipment->doSprawdzStatusyPrzesylek('Deliver', $transportNumbersFedEx);
if (!empty($result)) {
    echo 'Błędne statusy przesyłek do nadania - '.print_r($result, true);
}