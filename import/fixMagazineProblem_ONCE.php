<?php
/**
 * Description of fixReservations
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');
global $pDbMgr;

$sSql = '
SELECT OILI.ean_13, GROUP_CONCAT(SL.container_id," - ",SL.quantity SEPARATOR \'\r\n\') AS sl_locations
FROM stock_location_orders_items_lists_items AS SLOILI
JOIN orders_items_lists_items AS OILI
  ON OILI.id = SLOILI.orders_items_lists_items_id
JOIN stock_location AS SL
  ON SLOILI.stock_location_id = SL.id AND SLOILI.stock_location_magazine_type = SL.magazine_type AND SL.magazine_type = "SH"
JOIN sell_predict AS SP
  ON SP.orders_items_lists_items_id = OILI.id
JOIN products_stock AS PS
  ON SL.products_stock_id = PS.id
WHERE 
                (
               SELECT IFNULL(SUM(SL2.quantity), 0) 
               FROM stock_location AS SL2
               WHERE SL2.products_stock_id = OILI.product_id
               )
               <> PS.profit_g_act_stock AND
               (
               SELECT id
               FROM stock_location_orders_items_lists_items AS SLOILI2
               WHERE OILI.id = SLOILI2.orders_items_lists_items_id AND SLOILI2.confirmed_quantity <> SLOILI2.reserved_quantity 
               LIMIT 1
               ) IS NOT NULL
               
GROUP BY SP.id
HAVING count(SLOILI.id) > 1
ORDER BY OILI.id DESC
';
$data = $pDbMgr->GetAll('profit24', $sSql);
foreach ($data as $item) {

}