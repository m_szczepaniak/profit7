<?php
/**
 * Skrypt sztucznej rezerwacji towaru, wszystkie pozycje składowe zamówień nie usunięte, nie zrealizowane, 
 * które są w statusie 'jest nie', skrypt dodaje do tabeli rezerwacji
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);


$sSql = '
  INSERT INTO products_stock_reservations 
  SELECT 0 as id, OI.product_id, OI.order_id, OI.id AS orders_items_id, OI.quantity
  FROM orders_items AS OI
  JOIN orders AS O
    ON O.id = OI.order_id
  WHERE 
    OI.source = "51" AND
    OI.status = "3" AND
    OI.deleted = "0" AND
    OI.get_ready_list = "0" AND
    OI.deleted = "0" AND
    (OI.item_type = "I" OR OI.item_type = "P") AND
    OI.packet = "0" AND
    O.order_status <> "4" AND 
    O.order_status <> "5"
  ';
/*
		INSERT INTO products_stock_reservations 
		SET
			products_stock_id = NEW.product_id,
			order_id = NEW.order_id,
			orders_items_id = NEW.id,
			quantity = NEW.quantity;
 */