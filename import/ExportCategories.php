<?php
/**
 * Skrypt exportu kategorii
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-10-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
//error_reporting(E_ALL); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 

function getCat($iCatId) {
  
  $sSql = "SELECT name, id "
          . " FROM menus_items "
          . " WHERE published = '1' "
          . " AND id = ". $iCatId;
  return Common::GetRow($sSql);
}
function getCategories($iCatId) {
  
  $sSql = "SELECT name, id "
          . " FROM menus_items "
          . " WHERE published = '1' "
          . " AND parent_id = ". $iCatId;
  return Common::GetAll($sSql);
}

$aMainCategoriesOffer = array(
    '1', // dzial ogolny
    '261', // dzial naukowy
    '147' // dział językowy
);
$aMenusItems = array();
// pobieramy najpierw po rodzicu
foreach ($aMainCategoriesOffer as $iMainCat) {
  $aRow = getCat($iMainCat);
  // pobierzmy dzieci
  echo $aRow['name'].'<br />';
  $a1Cats = getCategories($aRow['id']);
  foreach ($a1Cats as $a1Cat) {
    echo $aRow['name'].'/'.$a1Cat['name'].'<br />';
    $a2Cats = getCategories($a1Cat['id']);
    foreach ($a2Cats as $a2Cat) {
      echo $aRow['name'].'/'.$a1Cat['name'].'/'.$a2Cat['name'].'<br />';
      $a3Cats = getCategories($a2Cat['id']);
      foreach ($a3Cats as $a3Cat) {
        echo $aRow['name'].'/'.$a1Cat['name'].'/'.$a2Cat['name'].'/'.$a3Cat['name'].'<br />';
      }
    }
  }
}

/*
echo '<?xml version="1.0" encoding="utf-8"?>';
echo '<menus>';
  echo '<menu>';
  echo '</menu>';     
echo '</menus>';
*/

?>
