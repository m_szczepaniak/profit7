<?php
/**
 * Skrypt importu produktów ze źródła Panda do Profit24.pl
 *
 * @author Marcin Janowski
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(21600); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');

if ($aConfig['common']['status']	!= 'development') {
    $bTestMode = false; //  czy test mode
} else {
    $bTestMode = true; //  czy test mode
}
$iLimitIteration = 500000;
$sSource = 'panda';
$sElement = 'products';
$oImportXMLController = new importXMLController($sSource, $sElement, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
  //XML Spakowany rarem
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportXMLController->oSourceElements->importSaveXMLData();
  // $sFilename = __DIR__.'/XML/panda/products_302508_19102015.xml';
} else {
	// link testowy na sztywno aby nie pobierać
  $sFilename = 'XML/panda/085315_22082018_product.XML';
}

if ($sFilename !== false && $sFilename != '') {

    // mamy XML'a przechodzimy do parsowania
    $oImportXMLController->parseXMLFile($sFilename, 'PRODUCTS', 'PRODUCT', 'UTF-8');
} else {
    // TODO komunikat błędu pobierania/zapisu pliku XML

}

$oImportXMLController->sendInfoMail("IMPORT '.$sSource.' OK", print_r($oImportXMLController->aLog, true).
    print_r($oImportXMLController->oSourceElements->aLog, true));
$oImportXMLController->sendInfoMail("IMPORT '.$sSource.' ERR", print_r($oImportXMLController->aErrorLog, true).
    print_r($oImportXMLController->oSourceElements->aErrorLog, true));
echo "\n KONIEC \n";