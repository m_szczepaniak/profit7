<?php
/**
 * 
 * Skrypt pobiera i dopisuje brakujące okładki z HELION'u, dodatkowo sprawdza 
 *	czy pobrana okładka jest prawidłowym obrazkiem, wcześniej należy wykonać 
 *	skrypt sprawdzający poprawność wszystkich obarazków w tabeli products_images
 * 
 * @author    Arkadiusz Golba <a.golba@omnia.pl>
 * @version   1.0 - 2012
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);


include_once ('import_func.inc.php');

$iDevLimit=0;


/**
 * Funkcja sprawdza czy produkt posiada zdefiniowaną okładkę
 *
 * @global array $aConfig
 * @param integer $iProdId
 * @return bool
 */
function checkProductImage($iProdId) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images 
					 WHERE product_id=".$iProdId." LIMIT 1";
	return (Common::GetOne($sSql) > 0? true : false);
}// end of checkProductImage() function 


/**
 * parsuje xml - produkt
 * @param $oXml
 * @return unknown_type
 */
function parseHelionProduct(&$oXml,$sBookstore,$sHelionIdent, &$aValues) {
global $aConfig;

	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'ksiazka':
						$aBookStatus=getBookStockWithISBN($aValues['product']['isbn_plain']);
						// sprawdź czy produkt posiada okładkę
						if ($aValues['okladka'] != '' && $aBookStatus['id'] > 0 && $aValues['product']['isbn_plain'] != '') {
							if (checkProductImage($aBookStatus['id']) == false) {
								addHelionImage($aBookStatus['id'],$aValues['okladka'],$aValues['product']['isbn_plain'],strtolower($aValues['helion_id']));
								echo 'dodaliśmy okładkę do produktu o isbn '.$aValues['product']['isbn_plain'].' <br />';
							} else {
								// echo 'produkt o isbn '.$aValues['product']['isbn_plain'].' posiada już okładkę <br />';
							}
						} else {
							// echo 'produkt o isbn '.$aValues['product']['isbn_plain'].' nie istnieje <br />';
						}
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'ident':
						break;
					case 'tytul':
						break;
					case 'autor':
						// ID AUTORA
						break;
					case 'opis':
						// opis
						break;
					case 'serietematyczne':
						// kategorie
						break;
					case 'seriewydawnicze':
						break;
					case 'powiazane':
						break;
					//  ------------------ atrybuty ----------------------
					case 'isbn':
						// ID AUTORA
						$aValues['product']['isbn'] = getTextNodeValue($oXml);		
						$aValues['product']['isbn_plain'] = isbn2plain($aValues['product']['isbn']);		
						break;	
					case 'liczbastron':
						break;
					case 'nosnik':
						break;
					case 'datawydania':
						break;
					case 'online':
						break;
					case 'okladka':
						// ID AUTORA
						$aValues['okladka'] = getTextNodeValue($oXml);
						break;
					case 'ksiegarnie_nieinter':
						break;
				}
			break;
		}
	}
} // end of parseAddProduct()

function parseProductsFile($sFilename,$sBookstore, &$aValues) {
global $aConfig;
	if (file_exists($sFilename)) {
	$oXml = new XMLReader();
	$oXml->open($sFilename);

		while(@$oXml->read()) {
			switch ($oXml->nodeType) {
				case XMLReader::END_ELEMENT :
					switch ($oXml->name) {
						case 'lista':
							// KONIEC LISTINGU PRODUKTOW
							$oXml->close();
						break;
					}
				break;
			
				case XMLReader::ELEMENT :
					switch ($oXml->name) {
						case 'ksiazka':
							// dodawanie produktu
							$sHelionIdent = $oXml->getAttribute('ident');
							parseHelionProduct($oXml,$sBookstore,$sHelionIdent, $aValues);
							break;
					}
				break;
			}
		}
	} else {
		dump('missing file '.$sFilename);
	}
}



parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/helion-produkty.xml",'helion', $aValues);
dump('helion');
dump($aValues);
$aValues = array();

parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/onepress-produkty.xml",'onepress', $aValues);
dump('onepress');
dump($aValues);
$aValues = array();

parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/sensus-produkty.xml",'sensus', $aValues);
dump('sensus');
dump($aValues);
$aValues = array();

parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/septem-produkty.xml",'septem', $aValues);
dump('septem');
dump($aValues);

?>