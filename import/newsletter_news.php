<?php
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
//$aConfig['err_handler']['use_error_handler'] = FALSE;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
ini_set("memory_limit", '512M');

//ini_set('memory_limit', '128M');
if ($aConfig['common']['status'] == 'development') {
	$aConfig['common']['base_url_http'] = 'http://profit24.serwer/';
	$aConfig['common']['base_url_http_no_slash'] = 'http://profit24.serwer';
	$aConfig['common']['client_base_url_http'] = 'http://profit24.serwer/';
	$aConfig['common']['client_base_url_http_no_slash'] = 'http://profit24.serwer';
} else {
	$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
	$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';
	$aConfig['common']['client_base_url_http'] = 'https://www.profit24.pl/';
	$aConfig['common']['client_base_url_http_no_slash'] = 'https://www.profit24.pl';
}
// liczba maili w wysylanym pakiecie
$iMails = 500;
// czas w sekundach przerwy pomiedzy pakietami
$iSleep = 5;

//$bTest=true;

$iMaxExecTime = ini_get('max_execution_time');
// czas rozpoczecia wykonywania skryptu
$iTime = microtime();
$iTime = explode(' ', $iTime);
$iTime = $iTime[1] + $iTime[0];
$iStartTime = $iTime;

function getRemainingTime() {
	global $iStartTime, $iMaxExecTime;
	
	$iTime = microtime();
	$iTime = explode(" ", $iTime);
	$iTime = $iTime[1] + $iTime[0];
	$iEndTime = $iTime;
	return $iMaxExecTime - ceil($iEndTime - $iStartTime);
}

// dolaczenie biblioteki funkcji modulu
include_once('modules/m_newsletter/internals/functions.inc.php');
include_once('modules/m_oferta_produktowa/client/Common.class.php');
include_once($aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].'modules/m_newsletter/lang/pl_news.php');
include_once($aConfig['common']['client_base_path'].$aConfig['smarty']['template_dir'].'/modules/m_newsletter/newsletters/auto_newsletter.php');

	
/**
	* Methoda pobiera zdjęcia newslettera
	*
	* @global array $aConfig 
	* @return array
	*/
function getNewsletterAutoImagesDyn() {
	global $aConfig;

	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."newsletters_images";
	$aImgs =& Common::GetAll($sSql);

	$aNewImgs = array();
	if (!empty($aImgs)) {
		foreach ($aImgs as $iKey => &$aImg) {
			$sPhotoFile = $aConfig['common']['client_base_path'].'/'.$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/'.$aImg['photo'];
			$aImage = getimagesize($sPhotoFile);
			$aNewImgs[] = array($sPhotoFile, $aImage['mime']);
			unset($aImg);
		}
	}
	return $aNewImgs;
}// end of getNewsletterAutoImagesDyn() method



/**
	* Metoda sprawdza czy można dokodać automatycznej rozsyłki newslettera
	*
	* @global array $aConfig
	* @return mixed - false - nie można dokonać rozsyłki
	*								 string - HTML z opisem - można dokonać rozsyłki
	*/
function &getDescriptionNewsletter() {
	global $aConfig;

	$sSql = "SELECT auto_description 
						FROM ".$aConfig['tabls']['prefix']."newsletters_settings
						LIMIT 1";
	$sDescriptionHTML = Common::GetOne($sSql);
	$sTMPDesc = trim($sDescriptionHTML);
	if (empty($sTMPDesc)) {
		// opis pusty, nie wysyłamy newslettera AUTO
		return false;
	} else {
		// w opsie coś istnieje - wysyłamy newsletter AUTO
		return $sDescriptionHTML;
	}
}// end of getDescriptionNewsletter() method


/**
 * Funkcja zapisuje logi rozsyłki newslettera, i zwiększa licznik auto newslettera
 * 
 * @global array $aConfig
 * @param integer $iNId - id auto newslettera
 * @param string $sEmail
 */
function writeEmail($iNId, $sEmail) {
	global $aConfig;
	
	$sSql="UPDATE ".$aConfig['tabls']['prefix']."newsletters_auto SET sent_count=sent_count+1 WHERE id = ".$iNId;
	Common::Query($sSql);
	/*
	$sLogFile = $aConfig['common']['client_base_path'].'omniaCMS/logs/newsletter_auto_'.$iNId.'.txt';
	if ($rHandle = fopen($sLogFile, "a")) {
		fwrite($rHandle, "[ ".date('Y-m-d H:i:s')." ]\t".$sEmail."\t".memory_get_usage()."\t".memory_get_usage(true)."\t".memory_get_peak_usage()."\t".memory_get_peak_usage(true)."\t".getRemainingTime()."\r\n");
		fclose($rHandle);
	}
	 */
	//sleep(1);
} // end of writeEmail() function


/**
 * Funkcja usuwa odbiorce newslettera z automatycznej rozsyłki
 * 
 * @global array $aConfig
 * @param int $iNId - id auto newslettera
 * @param type $iId - id odbiorcy newslettera
 * @return bool
 */
function deleteRecipient($iNId, $iId) {
	global $aConfig;
	
	$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_auto_jobs
					 WHERE recipient_id = ".$iId." AND
					 			 auto_newsletter_id = ".$iNId;
	return Common::Query($sSql);
}// end of deleteRecipient() function


/**
 * Funkcja pobiera kategorie odbiorcy newslettera
 * 
 * @global array $aConfig
 * @param type $iRecpId
 * @return type
 */
function getReciptientCats($iRecpId){
	global $aConfig;
	$sSql = "SELECT A.category_id
								 	FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS A
								 	JOIN ".$aConfig['tabls']['prefix']."newsletters_categories AS B
								 	ON B.id=A.category_id
								 	WHERE A.recipient_id = ".$iRecpId."
								 	AND B.auto_send='1'";
	return Common::GetCol($sSql);
}// end of getReciptientCats() method

/**
 * Metoda pobiera autorów produktu o podanym id
 * 
 * @global array $aConfig
 * @param integer $iId
 * @return string
 */ 
function getAuthors($iId) {
	global $aConfig;
	$aAuthors = array();
	
	// wybiera grupy kategorii dla tego produktu
	$sSql = "SELECT B.id as authorid, CONCAT('<b>',B.surname,'</b> ',B.name) AS name, C.name AS role
					 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
					 JOIN ".$aConfig['tabls']['prefix']."products_authors B
					 ON A.author_id=B.id
					 JOIN ".$aConfig['tabls']['prefix']."products_authors_roles C
					 ON A.role_id=C.id
					 WHERE A.product_id =".$iId."
					ORDER BY C.order_by ASC, A.id ASC, A.order_by ASC 
					LIMIT 1";
	$aItems =& Common::GetAll($sSql);
	foreach ($aItems as $iKey => $aItem) {
			// utworzenie linku do rekordu
			$aAuthors[$aItem['role']][$iKey]['name'] = $aItem['name'];
	}
	return serialize($aAuthors);
}// end of getAuthors() method
	
/**
 * Dodaje ksiązki do bufora newsletera
 * 
 * @global array $aConfig
 * @param int $iNewsletterCatId
 * @param int $iProductCatId
 * @param string $sCatName
 * @param int $iCatPriority
 * @param int $iLimit
 */
function addBooksFromCategory($iNewsletterCatId,$iProductCatId,$sCatName,$iCatPriority,$iLimit){
	global $aConfig;

	$oCommon = new Common_m_oferta_produktowa();
	//include($aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].'modules/m_newsletter/lang/pl_news.php');
	$sSql = "SELECT DISTINCT A.id AS product_id, A.price, A.price_brutto, A.order_by, 
									A.isbn, A.title, A.publisher, A.discount_limit, A.packet,
									A.is_news, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_previews AS P
								 ON A.id = P.product_id
							 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories AS C
								 ON C.product_id=A.id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id 
						 	 WHERE C.page_id = ".$iProductCatId.
							 		" AND (A.shipment_time = '1' OR A.shipment_time = '0')
							 		AND (A.prod_status = '1' OR A.prod_status = '3')
							 		AND I.id IS NOT NULL
							 		AND A.price_brutto >= 30
									AND (B.id IS NOT NULL OR P.id IS NOT NULL)
							 GROUP BY A.id
							 ORDER BY A.order_by DESC
							 LIMIT 0,".$iLimit;
	$aData = Common::GetAll($sSql);
	if (!empty($aData)) {
		foreach($aData as $iKey => $aValue){

			//pobranie cennika
			$aTarrif = $oCommon->getTarrif($aValue['product_id']);
			$aTarrif['packet'] = ($aValue['packet']=='1');
			
			// przeliczenie i formatowanie cen
			$aData[$iKey]['price'] = Common::formatPrice($oCommon->getPromoPrice($aData[$iKey]['price_brutto'], $aTarrif, $sPromoText, $aValue['discount_limit'], $oCommon->getSourceDiscountLimit($aValue['product_id'])));
			$aData[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);

			$aData[$iKey]['authors'] = getAuthors($aValue['product_id']);
			// okladka
			$aImage = getItemImage('products_images',array('product_id'=> $aValue['product_id']),'__t_');
			$aData[$iKey]['image_link'] = $aConfig['common']['base_url_http_no_slash'].$aImage['src'];
			// link do produktu
			$aData[$iKey]['product_link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aValue['product_id'],$aValue['title']);

			$aData[$iKey]['category_priority'] = $iCatPriority;
			$aData[$iKey]['category_id'] = $iNewsletterCatId;
			unset($aData[$iKey]['discount_limit']);
			unset($aData[$iKey]['packet']);
			if (($iNId = Common::Insert($aConfig['tabls']['prefix']."newsletters_books_buffer", $aData[$iKey])) === false) {
				echo "error adding auto newsletter book to buffer<br>\n";
			}
		}
	}
}// end of addBooksFromCategory() method

/**
 * Funkcja pobiera tresc newslettera z buffora na podstawie hasha
 * 
 * @global array $aConfig
 * @param string $sHash
 * @return string - html
 */
function getFromNewslettersBuffer($sHash){
	global $aConfig;
	
	$sSql="SELECT html
				FROM ".$aConfig['tabls']['prefix']."newsletters_buffer
				WHERE categories_hash = '".$sHash."'";
	return Common::GetOne($sSql);
}// end of getFromNewslettersBuffer() method

/**
 * Funkcja pobiera id newslettera do rozsyłki auto
 * 
 * @global array $aConfig
 * @param type $iAutoId
 * @return integer id newslettera
 */
function getNewsletterIdForAuto($iAutoId){
	global $aConfig;
	
	$sSql="SELECT id
				FROM ".$aConfig['tabls']['prefix']."newsletters
				WHERE auto_id = ".$iAutoId;
	return Common::GetOne($sSql);
}// end of getNewsletterIdForAuto() method


/**
 * Funkcja generuje tresc newslettera
 * 
 * @global array $aConfig
 * @param integer $iNewsId
 * @param string $sCats
 * @param int $iLimit
 * @param array $aImages
 * @param string $sDescriptionHTML
 * @return string HMTL
 */
function &getAutoNewsletterForCategory($iNewsId,&$sCats,$iLimit, &$aImages, &$sDescriptionHTML){
	global $aConfig;
	$sHash = md5($sCats);
	$sHtml = getFromNewslettersBuffer($sHash);
	if(empty($sHtml)){
		$sSql = "SELECT *
								 FROM ".$aConfig['tabls']['prefix']."newsletters_books_buffer
							 	 WHERE category_id IN (".$sCats.")
								 GROUP BY product_id
								 ORDER BY category_priority ASC,order_by DESC
								 LIMIT 0,".$iLimit;
		$aData = Common::GetAll($sSql);
		if (!empty($aData)) {
			foreach($aData as $Key => $Value)
			{
				$aData[$Key]['authors'] = unserialize($Value['authors']);
			}

			$sHtml=getNewsletterHtml($aConfig['common']['base_url_http_no_slash'],
																$aConfig['lang']['mod_m_newsletter_nowosci'],
																$aData,
																$aImages,
																$sDescriptionHTML);

			// dodanie do wszystkich linkow zrodla przejscia do sklepu - link sledzacy
			// wraz z Id newslettera
			$sTxt = '';
			extractLinks($iNewsId, $sTxt, $sHtml, true);

			$aValues = array(
				'categories_hash' => $sHash,
				'html' => $sHtml
			);
			if((Common::Insert($aConfig['tabls']['prefix']."newsletters_buffer", $aValues,'',false)) === false) {
				echo "error adding newsletter to buffer<br>\n";
				dump($aValues);
			}
		}
		unset($aData);
	}
	return $sHtml;
}// end of getAutoNewsletterForCategory() method


/**
 * Wysyla newsletter o danym id
 * 
 * @param $iAutoId
 * @param string $sDescriptionHTML - treść maila
 * @return void
 */
function sendNewsletter($iAutoId, $sDescriptionHTML = '') {
	global $aConfig,$iMails,$iSleep,$aNewsletter;

	include_once('MailNewsletter.class.php');
	$oMailsNewsletter = new MailNewsletter();

	//$aDynImages
	$iNewsletterId = getNewsletterIdForAuto($iAutoId);
	$aNewsletterImages = getNewsletterImages();
	$aNewsletterImagesDyn = getNewsletterAutoImagesDyn();
	if (!empty($aNewsletterImagesDyn)) {
		$aNewsletterImages = array_merge($aNewsletterImages, $aNewsletterImagesDyn);
	}
	if ($aConfig['common']['status'] == 'development') {
		$sDescriptionHTML = str_replace('src="/images/', 'src="http://profit24.serwer/images/', $sDescriptionHTML);
		//$sDescriptionHTML = str_replace('src="http://profit24.serwer/images/', 'src="/images/', $sDescriptionHTML);
	} else {
		$sDescriptionHTML = str_replace('src="/images/', 'src="https://www.profit24.pl/images/', $sDescriptionHTML);
		//$sDescriptionHTML = str_replace('src="/images/', $sDescriptionHTML);
	}
	$aAdditionalImages = extractHTMLImages($sDescriptionHTML);
	if (!empty($aAdditionalImages) && is_array($aAdditionalImages)) {
		$aNewsletterImages = array_merge($aNewsletterImages, $aAdditionalImages);
	}
	// ustaw na początku zdjęcia dla maila
	$oMailsNewsletter->addHTMLImage($aNewsletterImages);
	$oMailsNewsletter->setBackend();
	// ukoczone wysylanie
	$bFinished = true;
	$sSql = "SELECT *
					FROM ".$aConfig['tabls']['prefix']."newsletters_auto_jobs
					WHERE auto_newsletter_id = ".$iAutoId;
	$oRecipients = Common::PlainQuery($sSql);
	while ($aRecipient =& $oRecipients->fetchRow(DB_FETCHMODE_ASSOC)) {
			if (getRemainingTime() < 10) {
				// pozostalo mniej niz 10 sekund do przekroczenia maksymalnego czasu
				// wykonania skryptu
				// ustawienie statusu na 2 i przerwanie rozsylki
				$aValues = array(
					'status' => '2'
				);
				Common::Update($aConfig['tabls']['prefix']."newsletters_auto", $aValues, "id = ".$iAutoId);// nie oznaczaj jako wyslany
				$bFinished = false;
				break;
			}
			else {

				$aCats = getReciptientCats($aRecipient['recipient_id']);

				if(!empty($aCats)){
					$sCats = implode(',',$aCats);
					$sHtml = getAutoNewsletterForCategory($iNewsletterId,$sCats,$aNewsletter['auto_send_number'], $aNewsletterImagesDyn, $sDescriptionHTML);
					if(!empty($sHtml)){
						// wysylka maila
						echo $aRecipient['email'].' '.$sCats."<br>\n";
						// dodanie adresu email do linek sledzacych
						$sTxt='';
						$sHtml2='';
						$sDeleteLink = $aConfig['common']['base_url_http'].$aNewsletter['symbol'].'/delete,'.md5($aRecipient['recipient_id'].':'.$aRecipient['email']).'.html';
						addEmailToLinks($aRecipient['email'], '', $sHtml, $sTxt, $sHtml2);
						// zamiana znacznikow na linki w HTML
						$sHtml2 =& str_replace('{delete_link}', $sDeleteLink, $sHtml2);

						$oMailsNewsletter->setHTMLBody($sHtml2);
						$oMailsNewsletter->setGetParams();
						$oMailsNewsletter->setHeaderData($aNewsletter['auto_sender'],$aNewsletter['auto_sender_email'], $aNewsletter['auto_send_subject']);

						if ($aConfig['common']['status'] == 'development') {
							$oMailsNewsletter->sendNewsletterMail('arekgolba@gmail.com');
						} else {
							// TODO XXX ODKOMENTOWAC - po przetestowaniu newslettera
							$oMailsNewsletter->sendNewsletterMail($aRecipient['email']);
						}

						//Common::sendTxtHtmlMail($aNewsletter['auto_sender'], $aNewsletter['auto_sender_email'], $aRecipient['email'], $aNewsletter['auto_send_subject'], '', $sHtml2, '', '', $aNewsletterImages);
						// zapis do pliku
						writeEmail($iAutoId, $aRecipient['email']);
					}
					unset($sHtml);
				}
				unset($aCats);
				// usuwanie maila z zadan
				deleteRecipient($iAutoId, $aRecipient['recipient_id']);
				// jezeli wyslano 500 - 5 sekund przerwy
				if ((++$iCounter) % $iMails == 0) {
					sleep($iSleep);
				}
			}
		}
		if ($bFinished) {
			// nie ma juz do kogo wysylac, ustawienie statusu na 3 i czasu wysylki
			$aValues = array(
				'status' => '4'
			);
			Common::Update($aConfig['tabls']['prefix']."newsletters_auto", $aValues, "id = ".$iAutoId);
		}
	unset($oRecipients);
}// end of sendNewsletter() method


// sprawdzamy czy dzisiajszy dzien tygodnia jest dniem wysylki newslettera nowosci i pobieramy jego ustawienia
$sSql="SELECT A.*, B.symbol
				FROM ".$aConfig['tabls']['prefix']."newsletters_settings AS A
				 JOIN ".$aConfig['tabls']['prefix']."menus_items AS B
				 ON B.id = A.page_id
				WHERE auto_sending_day+1 = DAYOFWEEK(CURDATE())
			LIMIT 1";
$aNewsletter=Common::GetRow($sSql);
if(!empty($aNewsletter))	{
	// sprawdzamy czy zostal utworzony newsletter dla dnia dzisiejszego
	$sSql= "SELECT * 
					FROM ".$aConfig['tabls']['prefix']."newsletters_auto
					WHERE send_date = CURDATE()";
	$aAutoNewsletter = Common::GetRow($sSql);

	// tu sprawdzamy czy newsletter można wysłać
	$sDescriptionHTML = getDescriptionNewsletter();
	if(empty($aAutoNewsletter)){
		/*
		if ($sDescriptionHTML === false) {
			// nie wysyłamy newslettera
			//dump('nie wysyłamy newslettera');
		} else {
			*/
			// nie ma - tworzymy wpis	
			$aValues = array(
				'send_date' => 'NOW()',
				'status' => '0'
			);

			if(($iAutoId=Common::Insert($aConfig['tabls']['prefix']."newsletters_auto", $aValues)) === false) {
				echo "error adding auto newsletter<br>\n";
				dump($aValues);
			}
			// czyszczenie bufora książek newslettera
			$sSql="TRUNCATE TABLE ".$aConfig['tabls']['prefix']."newsletters_books_buffer";
			if(Common::Query($sSql)===false){
				echo "error clearing newsletter books buffer<br>\n";
			}
			// czyszczenie bufora html newsletterów
			$sSql="TRUNCATE TABLE ".$aConfig['tabls']['prefix']."newsletters_buffer";
			if(Common::Query($sSql)===false){
				echo "error clearing newsletter buffer<br>\n";
			}

			// pobieranie kategorii dla ktorych trzeba wygenerowac automatycznie newslettery

			$sSql="SELECT A.id, A.name, A.send_categories, B.priority, A.page_id
							FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
							JOIN ".$aConfig['tabls']['prefix']."menus_items B
						ON B.id = A.send_categories
							WHERE A.auto_send = '1' 
							AND A.send_categories IS NOT NULL
							AND B.priority > 0";

			$aAutoCategories=Common::GetAll($sSql);
			// dodanie ksiązek do bufora
			foreach($aAutoCategories as $aAutoCat){
				addBooksFromCategory($aAutoCat['id'],$aAutoCat['send_categories'],$aAutoCat['name'],$aAutoCat['priority'],$aNewsletter['auto_send_number']);
			}
			// dodanie nowego wpisu w tablicy newsletterów na poptrzeby zliczania kliknięć
			$aValues = array(
				'page_id' => $aAutoCategories[0]['page_id'],
				'subject' => 'nowości '.date('d-m-Y'),
				'created' => 'NOW()',
				'created_by' => 'auto',
				'auto_id' => $iAutoId
			);
			if (($iNId = Common::Insert($aConfig['tabls']['prefix']."newsletters", $aValues)) === false) {
				echo "error adding newsletter<br>\n";
				dump($aValues);
				die();
			}

			// poczatek rozsylki tego newslettera - dodanie subskrybentow
			// do tablicy newsletters_jobs
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."newsletters_auto_jobs
							(SELECT ".$iAutoId.", B.id, B.email
								FROM ".$aConfig['tabls']['prefix']."newsletters_recipients AS B
								WHERE B.confirmed = '1'".($aConfig['common']['newsletter_test']?" AND B.test='1'":" AND B.test='0'").")";
			if (Common::Query($sSql) === false) {
				echo "error adding newsletter job<br>\n";
				dump($aValues);
				die();
			}

			// ustawienie stnau 'gotowy'
			$aValues = array(
				'status' => '1'
			);
			Common::Update($aConfig['tabls']['prefix']."newsletters_auto", $aValues, "id = ".$iAutoId);
			// wysylaj
			sendNewsletter($iAutoId, $sDescriptionHTML);
		//}
	} else {
		// jesli wysylanie nie jest zakonczone - przerwany - wznow i wysylaj
		if($aAutoNewsletter['status']!='4'){
			sendNewsletter($aAutoNewsletter['id'], $sDescriptionHTML);
		}
	}
}


?>