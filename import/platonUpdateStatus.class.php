<?php
/**
 * Skrypt importu produktów ze źródła Platon do Profit24.pl
 * 
 * @author Marcin Janowski
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(900);
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');	
$bTestMode = false; //  czy test mode
$iLimitIteration = 2000000;
$sSource = 'platon';
$sElement = 'status';
$oImportXMLController = new importXMLController($sSource, $sElement, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportXMLController->oSourceElements->importSaveXMLData();
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'XML/platon/stock_013919_14092015.xml';
}

if ($sFilename !== false && $sFilename != '') {
	// mamy XML'a przechodzimy do parsowania
	$oImportXMLController->parseXMLFile($sFilename, 'STOCK', 'r');
	/*
	dump($oImportXMLController->aErrorLog);
	dump($oImportXMLController->oSourceElements->aErrorLog);
	 */
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
  $oImportXMLController->addErrorLog(_('Brak pliku'));
}


$sSql = 'UPDATE `products_stock` 
SET platon_status = "0" 
WHERE 
platon_status = "1" 
AND 
platon_last_import <= DATE_SUB( CURDATE( ) , INTERVAL 1 DAY )
';
Common::Query($sSql);

$oImportXMLController->sendInfoMail("IMPORT PLATON OK", print_r($oImportXMLController->aLog, true).
																											print_r($oImportXMLController->oSourceElements->aLog, true));
$oImportXMLController->sendInfoMail("IMPORT PLATON ERR", print_r($oImportXMLController->aErrorLog, true).
																											print_r($oImportXMLController->oSourceElements->aErrorLog, true));
 //TODO mail z informacją o imporcie

echo "\n KONIEC \n";

?>
