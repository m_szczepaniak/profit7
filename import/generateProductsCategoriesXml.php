<?php

use PriceUpdater\Buffor\ProductBuffor;
use PriceUpdater\Tarrifs\TarrifsUpdater;
use PriceUpdater\Buffor\CyclicBuffor;

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
ini_set('memory_limit', '2048M');

global $pDbMgr;

$sSql = "SELECT MI.id, MI.parent_id as pid, MI.name as content
         FROM menus_items AS MI
         ORDER BY MI.parent_id ASC, MI.id ASC";

$data = $pDbMgr->GetAll("profit24", $sSql);

$sXMLFilename = $_SERVER['DOCUMENT_ROOT'].'/categories_export.json';

file_put_contents($sXMLFilename, json_encode($data));
