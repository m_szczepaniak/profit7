<?php
/**
 * Kontroler importu CSV elementów z dowolnego źródła do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 * 
 * @property sourceElements $oSourceElements
 * @property Module_Common $oProductsCommon
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');	
class importCSVController extends CommonSynchro {

	private $iLimitIteration;
	public $bTestMode;
	public $aLog = array();
	public $aErrorLog = array();

	public $oSourceElements = NULL;
	public $oProductsCommon = NULL;
  	public $sFile;
	private $sSource;

	/**
	 * @var bool
	 */
	private $bIgnoreConvertCP1250;


	public function __construct($sSource, $sElementName, $bTestMode, $iLimitIteration = 0, $bIgnoreConvertCP1250 = false) {
    global $pDbMgr, $aConfig;
		$this->iLimitIteration = $iLimitIteration;
		$this->bTestMode = $bTestMode;
		
		$sSourceElementsClassName = $sElementName."Elements";
		include_once($_SERVER['DOCUMENT_ROOT'].'/import/elements/'.$sSource.'/'.$sSourceElementsClassName.".class.php");
		$this->oSourceElements = new $sSourceElementsClassName($pDbMgr, $aConfig);
		
		include_once('modules/m_oferta_produktowa/Module_Common.class.php');
		$this->oProductsCommon = new Module_Common();
		$this->sSource = $sSource;
		$this->bIgnoreConvertCP1250 = $bIgnoreConvertCP1250;
	}
  
  public function __destruct() {
    
    if ($this->bTestMode == false && isset($this->sFile) && $this->sFile != '') {
      @unlink($this->sFile);
    }
  }
  
  private function setFile($sFile) {
    $this->sFile = $sFile;
    return $this;
  }

	/**
	 * Metoda parsuje plik XML
	 *
	 * @param string $sCSVFile
	 * @return boolean 
	 */
	public function parseCSVFile($sCSVFile, $sSep = ';', $bUseFgetcsv = FALSE, $aColsToFgetcsv = array()) {

    $this->setFile($sCSVFile);
		$iIteration = 0;
		
		if (file_exists($sCSVFile)) {

			$handle = fopen($sCSVFile, "rb");
			if ($handle) {
        
        if ($bUseFgetcsv === TRUE) {
          $aCols = $aColsToFgetcsv;
          $this->oSourceElements->PrepareSynchro();
          while (($aItem = fgetcsv($handle, 0, $sSep, '"', '"')) !== FALSE) {
              $this->parseItem($aItem, $aCols);
							if ($this->iLimitIteration <= $iIteration) {
								return;
							}
							$iIteration++;
          }
        } else {
          $aCols = array();
					while (($sLine = fgets($handle, 4096)) !== false) {
							// cała linia

							$aElements = explode($sSep, $sLine);
							if ($iIteration > 0) {
								// nastepne iteracje po kolumnach
								$this->parseItem($aElements, $aCols);
							}
							else {
								$this->oSourceElements->PrepareSynchro();
								
								// kolumny, pierwsza iteracja
								$aCols = $aElements;
							}
							
							// $this->bTestMode == true && 
							if ($this->iLimitIteration <= $iIteration) {
								return;
							}
							$iIteration++;
					}
        
					if (!feof($handle)) {
							echo "Błąd: niespodziewany błąd fgets()\n";
							$this->addErrorLog("Błąd: niespodziewany błąd fgets() w pliku - '".$sCSVFile."'");
							return false;
					}
        }
        
				fclose($handle);
			}
		} else {
			$this->addErrorLog("Brak pliku XML '".$sCSVFile."'");
			// brak XML'a
			return false;
		}
	}// end of parseCSVFile() method
	
	
	/**
	 * Metoda parsuje książki po jednej
	 *
	 * @param type $oXml
	 * @return type 
	 */
	private function parseItem($aElements, $aCols) {
		$sSourceBId = '';
		
		// czyścimy na wstępnie dane ksiażki
		$this->oSourceElements->clearProductData();
		
		// lecimy po elementach
		foreach ($aElements as $iKey => $sElementValue) {
			$sColName = trim($aCols[$iKey]); // nazwa aktualnie przetważanego pola
			if ($this->bIgnoreConvertCP1250 === false) {
				$sElementValue = iconv("CP1250", "UTF-8", trim($sElementValue));
			}
			
			if ($this->oSourceElements->aElement['_id'] != '' && empty($sSourceBId)) {
				$sSourceBId = $this->oSourceElements->aElement['_id'];
			}
			// wyłuskaj z danych z XML dane książki
			if ($this->shellItemData($sElementValue, $sSourceBId, $sColName) === false) {

				unset($sSourceBId);
				$this->oSourceElements->clearProductData();
				//continue;
			}
		}

		if ($sSourceBId != '') {
			// ksiazka - KONIEC - dodanie do bazy
			$aElement = $this->oSourceElements->postParse(); // dorzuca dodatkowe dane po wybraniu innych

			if (is_array($aElement) && !empty($aElement) && $aElement !== false) {
				$iPId = $this->oSourceElements->checkExists($aElement);
				if ($iPId <= 0) {
					// element nie istnieje
					//echo 'INSERT - '.$aElement['_id'];
					$this->oSourceElements->proceedInsert($this, $aElement, $sSourceBId);
				} else {
					// element istnieje
					// XXX TODO update - następna iteracja
					//echo 'UPDEJT - '.$iPId. ' - '.$aElement['_id'];
					$this->oSourceElements->proceedUpdate($this, $aElement, $sSourceBId, $iPId);
				}

				unset($sSourceBId);
				$this->oSourceElements->clearProductData();
			} elseif ($aElement === -1) {
				unset($sSourceBId);
				$this->oSourceElements->clearProductData();
			} else {
        $this->oSourceElements->clearProductData();
				$this->addErrorLog("element o id źródła: ".$sSourceBId." nie spełnia podstawowych warunków");
				unset($sSourceBId);
      }
		}
	}// end of parseItem() method
	
	
	/**
	 * Metoda wyłusuje dane z CSV'a
	 *
	 * @param type $oXml 
	 */
	private function shellItemData($sElementValue, $sSourceBId, $sColName) {
		
		$sTagName = $sColName;
		if(isset($sTagName) && $sTagName != '' && in_array($sTagName, $this->oSourceElements->aSupportedTags)) {
			
			// tag jest wspierany
			$sSetMethodName = 'set'.ucfirst(mb_strtolower($sTagName, 'UTF-8'));
			// XXX TODO sprawdzić jeszcze czy metoda nie istnieje jeśli nie to wywalić błąd
			if (method_exists($this->oSourceElements, $sSetMethodName) === true) {
				if ($this->oSourceElements->$sSetMethodName($sElementValue) === false){
					$this->addErrorLog("Wystąpił błąd w metodzie : ".$sSetMethodName.
														" dla elementu o id : ".$sSourceBId);
					// uciekamy, nie dodajemy tego elementu coś jest z nim nie tak
					return false;
				}
			} else {
				$this->addErrorLog("Brak metody : ".$sSetMethodName.
														" w objekcie oSourceElements ".
														" dla elementu o id : ".$sSourceBId);
				// uciekamy, nie dodajemy tego elementu coś jest z nim nie tak
				return false;
			}
		} else {
			$this->addErrorLog("Nieobsługiwany tag '".$sTagName."' elementu o id ".$sSourceBId);
			// element posiada nieobsługiwany TAG
			return false;
		}
	}// end of shellItemData() method
}// end of dictumImport() class
?>
