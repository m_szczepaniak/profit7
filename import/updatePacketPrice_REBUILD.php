<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
/**
 * Skrypt Aktualizuje cenę Pakietu na podstawie kolumny last_modified_price w tabeli products
 * Jeśli last_modified_price = '1' wtedy zmień cenę pakietu
 * I ponownie zmień last_modified_price = '0' aby nastepne wywołanie skryptu nie musiało aktualizować pakietu
 * 
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);
$bIsErr = false;

include_once ('import_func.inc.php');

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module_Common.class.php');
$oCommon = new Module_Common();

$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_packets_items";
$aPIds = Common::GetCol($sSql);

$iCount = 0;
foreach($aPIds as $iId){
	// przeliczamy pakiety
	// pobierz pakiety w ktorych wystepuje dany produkt
	$aPacketsProduct = $oCommon->getPacketsProduct($iId);
	if (!empty($aPacketsProduct) && is_array($aPacketsProduct)) {
		foreach ($aPacketsProduct as $iPacketProduct) {
			// zmień ceny składowych pakietu
			if ($oCommon->updateProductPacketPrice($iId, $iPacketProduct) === false) {
				$bIsErr = true;
				$sLogInfo .= "1 - blad zmiany cen dla pakietu [".$iPacketProduct."] dla pozycji [".$iId."] \n";
				echo "1 - blad zmiany cen dla pakietu [".$iPacketProduct."] dla pozycji [".$iId."] \n";
				break;
			}
			// zmień ceny pakietu bez aktualizacji autorów
			if ($oCommon->updatePacketAuthorsPrices($iPacketProduct, false) === false) {
				$bIsErr = true;
				$sLogInfo .= "2 - blad zmiany cen dla pakietu [".$iPacketProduct."] dla pozycji [".$iId."] \n";
				echo "2 - blad zmiany cen dla pakietu [".$iPacketProduct."] dla pozycji [".$iId."] \n";
				break;
			}
			// jeśli powiodły sie updaty to zwiększ licznik
			$iCount++;
		}
	}
	
	// ustawiamy ponownie flagę aktualizacji niedostepnych na 1 dla wszystkich elementow pakietu w products_packets_items
	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
					 SET last_modified_price = '0'
					 WHERE id=".$iId;
	if(Common::Query($sSql) === false){
		$bIsErr = true;
		$sLogInfo .= "3 - blad zmiany flagi 'last_modified_price' dla pozycji [".$iId."] \n";
		echo "3 - blad zmiany flagi 'last_modified_price' dla pozycji [".$iId."] \n";
		break;
	}
}

echo "<b>Zmienionych pakietów ".$iCount."</b><br>\n";
$sLogInfo.="Zmienionych pakietów ".$iCount."  \n";

if($iCount>0 || $bIsErr == true){
	sendInfoMail("Zmiana cen pakietów",$sLogInfo);
}

?>