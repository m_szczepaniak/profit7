<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'http://www.profit24.com.pl';


include_once ('import_func.inc.php');

function getProductCats($iId) {
	global $aConfig;
	$sSql = "SELECT A.id, A.priority
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId;
	return Common::GetAll($sSql);
}

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="bez_kategorii_2_poziomu.xls"');
header("Pragma: no-cache");
header("Expires: 0");
echo	"Id\tNazwa\tIsbn\r\n";

	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn
					 FROM ".$aConfig['tabls']['prefix']."products A
					 WHERE A.published = '1'
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		$aCats = getProductCats($aItem['id']);
		if(!empty($aCats)){
			$bHasL2Cats = false;
			foreach($aCats as $aCat){
				if($aCat['priority']>0){
					$bHasL2Cats = true;
				}
			}
			if(!$bHasL2Cats){
				echo $aItem['id']."\t".$aItem['plain_name']."\t".$aItem['isbn']."\r\n";
			}
		}
	}
	
	
?>
