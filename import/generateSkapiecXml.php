<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');

function getProductAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.', ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-2);
  }
  return $sAuthors;
}
	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	} // end of getTarrifPrice()
	
	function getProductCat($iId) {
	global $aConfig;
	$sSql = "SELECT A.id
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";
	return Common::GetOne($sSql);
}

function &getCats() {
	global $aConfig;
	$sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
					 WHERE A.priority > 0
					ORDER BY A.priority";
	return Common::GetAll($sSql);
}


function philsXMLClean($strin) {
        $strout = null;

        for ($i = 0; $i < strlen($strin); $i++) {
                $ord = ord($strin[$i]);

                if (($ord > 0 && $ord < 32) || ($ord >= 127)) {
                        $strout .= "&amp;#{$ord};";
                }
                else {
                        switch ($strin[$i]) {
                                case '<':
                                        $strout .= '&lt;';
                                        break;
                                case '>':
                                        $strout .= '&gt;';
                                        break;
                                case '&':
                                        $strout .= '&amp;';
                                        break;
                                case '"':
                                        $strout .= '&quot;';
                                        break;
                                default:
                                        $strout .= $strin[$i];
                        }
                }
        }

        return $strout;
}

function getProductImg($iId) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
	
		if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
			return $sDir.'/__b_'.$aImg['photo'];
		}
		else {
			if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
				return $sDir.'/'.$aImg['photo'];
			}
		}
	}
	return '';
} // end of getItemImage() function

/* Convert Extended ASCII Characters to HTML Entities */
function ascii2entities($string){
    for($i=128;$i<=255;$i++){
        $entity = htmlentities(chr($i), ENT_QUOTES, 'cp1252');
        $temp = substr($entity, 0, 1);
        $temp .= substr($entity, -1, 1);
        if ($temp != '&;'){
            $string = str_replace(chr($i), '', $string);
        }
        else{
            $string = str_replace(chr($i), $entity, $string);
        }
    }
    return $string;
}

/**
 * Metoda pobiera opisy dodawane do tytułu produktu - dostępność
 * 
 * @return type
 */
function getShipmentTimes(){

  $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
  return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
}// end of getShipmentTimes() method


$aShipmentTimes = getShipmentTimes();

	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 LIMIT 1";
	$aSite =& Common::GetRow($sSql);
	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn_plain, A.shipment_time, A.price_brutto, A.shipment_time, A.description, B.name AS publisher_name
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 WHERE A.published = '1'
					 			AND (A.prod_status = '1' OR A.prod_status = '3')
					 			AND (A.shipment_time = '0' OR A.shipment_time='1')
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
		
	if (!($fp = fopen($_SERVER['DOCUMENT_ROOT'].'/skapiec.xml', 'w'))) {
   echo "Nie mogę otworzyc pliku skapiec.xml do zapisu";
   die();
	} else {
	
	$sXml = '<?xml version="1.0" encoding="ISO-8859-2"?>
<xmldata>
	<version>12.0</version>
	<header>
		<name>'.iconv('utf-8','iso-8859-2',$aSite['title']).'</name>
		<www>'.$aConfig['common']['base_url_http_no_slash'].'</www>
		<time>'.date('Y-m-d').'</time>
	</header>
	<category>
';
	fwrite($fp,$sXml);
	
	$aCats = getCats();
	foreach($aCats as $aCat){
		fwrite($fp, "		<catitem>\n");
		fwrite($fp, "			<catid>".$aCat['id']."</catid>\n");
		fwrite($fp, "			<catname>".iconv('utf-8','iso-8859-2',htmlspecialchars($aCat['name'],ENT_QUOTES,'UTF-8',false))."</catname>\n");
		fwrite($fp, "		</catitem>\n");
	}
	
	
	fwrite($fp,"	</category>\n	<data>\n");
	fflush($fp);
	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		$iCatId = getProductCat($aItem['id']);
		if(!empty($iCatId)) {
			$fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
			$sAuthors = getProductAuthors($aItem['id']);
			$sImg = getProductImg($aItem['id']);
			switch($aItem['shipment_time']){
				case '0':
					$iShipment = 1;
				break;
				case '1':
					$iShipment = 2;
				break;
				case '2':
					$iShipment = 9;
				break;
				case '3':
					$iShipment = -1;
				break;
				default:
					$iShipment = 0;
				break;
			}
	
			fwrite($fp, "		<item>\n");
			fwrite($fp, "			<compid>".$aItem['id']."</compid>\n");
			if(!empty($aItem['publisher_name'])){
				fwrite($fp, "			<vendor>".iconv('utf-8','iso-8859-2',htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false))."</vendor>\n");
			}
			fwrite($fp, "			<name>".iconv('utf-8','iso-8859-2',htmlspecialchars(trimString($aItem['plain_name'],200),ENT_QUOTES,'UTF-8',false).$aShipmentTimes[$aItem['shipment_time']])."</name>\n");
			fwrite($fp, "			<price>".$fTarrifPrice."</price>\n");
			fwrite($fp, "			<catid>".$iCatId."</catid>\n");
			if(!empty($sImg)){
				fwrite($fp, "			<foto>".$aConfig['common']['base_url_http'].$sImg."</foto>\n");
			}
			//fwrite($fp, "			<desclong><![CDATA[".philsXMLClean(iconv('utf-8','iso-8859-2',stripslashes(strip_tags($aItem['description']))))."]]></desclong>\n");
			fwrite($fp, "			<desclong><![CDATA[".clearAsciiCrap(ascii2entities(htmlspecialchars(iconv('utf-8','iso-8859-2',stripslashes(strip_tags($aItem['description']))),ENT_QUOTES,'UTF-8',false)))."]]></desclong>\n");
			if($iShipment != 0){
				fwrite($fp, "			<availability>".$iShipment."</availability>\n");
			}
			fwrite($fp, "			<ean>".$aItem['isbn_plain']."</ean>\n");
			fwrite($fp, "			<url>".$aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name'])."</url>\n");
	
			if(!empty($sAuthors)){
				fwrite($fp, '			<attribute name="autor">'.iconv('utf-8','iso-8859-2',htmlspecialchars($sAuthors,ENT_QUOTES,'UTF-8',false))."</attribute>\n");
			}
			fwrite($fp, "		</item>\n");
			fflush($fp);
		}
	}
	
	$sXml = '	</data>
</xmldata>';
	fwrite($fp,$sXml);
	fclose($fp);
	}
	
/*
<?xml version="1.0" encoding="ISO-8859-2"?>
<xmldata>
	<version>12.0</version> <!-- wersja szablonu -->
	<header>
		<name>nazwa sklepu</name> <!-- nazwa sklepu partnera -->
		<www>www sklepu</www> <!-- adres internetowy sklepu: http://www.sklep.pl -->
		<time>2004-11-01</time> <!-- data z ktorej pochodza dane rrrr-mm-dd -->
	</header>
	<category> <!-- zawiera opis kategorii do ktorych przydzielone sa poszczegolne towary. Rozbicie na kategorie w sklepie nie jest uzaleznione od rozbicia na kategorie w serwisie Skapiec.pl ale zalecane jest aby bylo mozliwe stworzenie relacji n:1 (kategorie partnera:kategorie skapiec.pl). Optymalnie podzial powinien odpowiadac temu ze skapiec.pl-->
		<catitem> 
			<catid>45katd</catid> <!-- identyfikator kategorii -->
			<catname>nazwa kategorii</catname> <!-- nazwa kategorii -->
		</catitem>
		<catitem>
			<catid>45katd</catid>
			<catname>monitory LCD</catname>
		</catitem>
		.
		.
		.
	</category>
	<data> <!-- dane towarow -->
		<item> <!-- pojedynczy towar -->
			<compid>0123abc</compid> <!-- identyfikator towaru w sklepie, musi to być coś co pozwala zidentyfikować towar na stronie, a więc część linka do towaru -->
			<vendor>Sapphire</vendor> <!-- producent - jezeli nie da sie go wyodrebnic z nazwy towaru, to pole moze pozostac puste-->
			<name>Sapphire Radeon 9600 Pro 128bitów 128MB</name> <!-- nazwa towaru maksimum 200 znaków, opis widoczny na naszych stronach -->
			<price>240.20</price> <!-- cena brutto - kropka dziesietna (moze nie byc gdy cena jest wart. calkowita) -->
			<partnr>APED:2030-53PLVFY</partnr> <!-- tzw PartNumber czyli unikalny kod nadany przez producenta danemu towarowi, pole nie jest obowiazkowe; wartość może byc umieszczona takze w znaczniku NAME-->
			<catid>45katd</catid> <!-- identyfikator kategorii (z sekcji category) -->
			<foto>http://www.sklep.pl/zdjecie_produktu.jpg</foto> <!-- link do zdjecia produktu -->
			<desclong>pelny opis towaru w sklepie</desclong> <!-- pole nie jest obowiazkowe -->
			<availability>2</availability> <!-- ilosc dni od zlozenia zamowienia do czasu wyslania towaru, (-1  towar na zamowienie), pole nie jest obowiazkowe -->
			<ean>1234567890123</ean> <!-- kod EAN/ISBN produktu (pole obowiazkowe dla ksiazek oraz plyt z muzyka) -->
			<url>http://www.sklep.pl/0123abc.php</url> <!-- URL do widoku produktu, pole nie jest obowiazkowe, obecnie url generowany jest dynamicznie na podstawie <compid> -->
			<delivery> <!-- cala sekcja delivery nie jest obowiazkowa, bedzie wykorzystana w przyszlosci - moze zostac pominieta, lub wypelniona jedynie czesciowo (zgodnie z metodami platnosci/dostawy dostepnymi w sklepie -->		
				<pcash>	</pcash> <!-- koszt dostawy przy platnosci za pobraniem i wysylce poczta -->
				<ptransfer> </ptransfer>  <!-- koszt dostawy przy platnosci przelewem i wysylce poczta -->
				<pcard>	</pcard> <!-- koszt dostawy przy platnosci karta i wysylce poczta -->
				<ccash>	</ccash> <!-- koszt dostawy przy platnosci za pobraniem i wysylce kurierem -->
				<ctransfer> </ctransfer> <!-- koszt dostawy przy platnosci przelewem i wysylce kurierem -->
				<ccard>	</ccard> <!-- koszt dostawy przy platnosci karta i wysylce kurierem -->
			</delivery>
			<attribute name="nazwa">tutaj mozemy umiescic dowolne dodatkowe atrybuty towarow, ktore pomoga w opisie/identyfikacji produktu</attribute> <!-- dodatkowe atrybuty sa opcjonalne -->
		</item>
		<item>
			<compid>123abc</compid>				
			<vendor>LG</vendor>
			<name>LG L175</name>
			<price>240.20</price>
			<partnr>L175</partnr>
			<catid>45katd</catid>
			<foto>http://www.sklep.pl/zdjecie_produktu.jpg</foto>
			<desclong>Ultraszybki czas reakcji, wysoki kontrast, szeroki kąt widzenia obrazu to niewątpliwe atuty tego monitora. Funkcja f-ENGINE Innowacyjny układ scalony doskonalący jakość kolorów, obrazu i pola widzenia.</desclong>
			<availability>1</availability>
			<ean>1234567890123</ean>
			<url>http://www.sklep.pl/123abc.php</url>
			<delivery>		
				<pcash>15</pcash>
				<ptransfer>20</ptransfer>
				<pcard>25</pcard>
				<ccash>15</ccash>
				<ctransfer>20</ctransfer>
				<ccard>25</ccard>
			</delivery>
			<attribute name="przekatna ekranu">17</attribute>
			<attribute name="czas reakcji">8 ms</attribute>
		</item>
		.
		.
		.
	</data>
</xmldata>

*/
?>
