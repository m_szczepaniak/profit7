<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-23 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Description of fillExternalProviderReservations
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');

// 1) ustawić source
/**
 * @var $pDbMgr \DatabaseManager
 */
$sSql = 'SELECT PSR.id, S.id AS source_id
         FROM products_stock_reservations AS PSR
         JOIN orders_items AS OI
          ON OI.id = PSR.orders_items_id
         JOIN sources AS S
          ON OI.source = S.provider_id';
$aPSRs = $pDbMgr->GetAll('profit24', $sSql);
foreach ($aPSRs as $aProductItemReservation) {
  $aValue = array(
      'source_id' => $aProductItemReservation['source_id']
  );
  $pDbMgr->Update('profit24', 'products_stock_reservations', $aValue, ' id='.$aProductItemReservation['id']);
}

//1) założyć rezerwację, na J i E
$sSql = '
  SELECT OI.source, OI.product_id, OI.order_id, OI.id AS orders_items_id, OI.quantity
  FROM orders_items AS OI
  JOIN orders AS O
    ON O.id = OI.order_id
  WHERE 
    (OI.source = "1" OR OI.source = "0") AND
    OI.status = "3" AND
    OI.deleted = "0" AND
    OI.get_ready_list = "0" AND
    OI.deleted = "0" AND
    (OI.item_type = "I" OR OI.item_type = "P") AND
    OI.packet = "0" AND
    O.order_status <> "4" AND 
    O.order_status <> "5"
    ';
$aOrdersItems = $pDbMgr->GetAll('profit24', $sSql);
$oReservations = new \omniaCMS\lib\Products\ProductsStockReservations($pDbMgr);
foreach ($aOrdersItems as $aOrder) {
  $oReservations->createReservation($aOrder['source'], $aOrder['product_id'], $aOrder['order_id'], $aOrder['orders_items_id'], $aOrder['quantity']);
}
