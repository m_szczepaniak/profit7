<?php
/**
 * Klasa importu danych z magazynów wewnetrznych
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED); 
//error_reporting(E_ALL); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(900); // maksymalny czas wykonywania 3 godziny - 
//ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');

if ($aConfig['common']['status']	!= 'development') {
    $bTestMode = false; //  czy test mode
} else {
    $bTestMode = true; //  czy test mode
}
$iLimitIteration = 3000000000;
$sSource = 'internal';
$sElementName = 'status';
$sDataUpdate = date("Y-m-d H:i:s");

$n = 0;
$elementsToProceedAtTheEnd = [];
// pobieramy na dzień dobry wszystkie produkty
for ($i = 0; $i < 8; $i++) {
  $oImportCSVController = new importXMLController($sSource, $sElementName, $bTestMode, $iLimitIteration);
  if ($bTestMode == true) {
//    $sFilename = $_SERVER['DOCUMENT_ROOT'].'/LIB/communicator/sources/Internal/streamsoft/import_files/xml/Stany/07_20150629103009.xml';
    $sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
  } else {
    $sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
  }

  $code = strpos($sFilename, "Stany/07_") != false ? 7 : 0;
  /*
  if ($bTestMode == false) {


  } else {
    // link testowy na sztywno aby nie pobierać
    $sFilename = 'CSV/internal/myfile.csv';
  }

  */
  $sBaseFilename = basename($sFilename, '.xml');
  preg_match('/^(\w+)_/', $sBaseFilename, $aMatched);
  if ($oImportCSVController->oSourceElements->_getStockPrefixByDepot($aMatched[1]) !== false) {
    if ($sFilename !== false && $sFilename != '') {

      // mamy XML'a przechodzimy do parsowania
      $oImportCSVController->parseXMLFile($sFilename, 'STANY', 'STAN');

      dump($oImportCSVController->aErrorLog);
      dump($oImportCSVController->oSourceElements->aErrorLog);
    } else {
      // TODO komunikat błędu pobierania/zapisu pliku XML
    }
    
    $oImportCSVController->oSourceElements->aElement['_id'] = '19';
    // opierdala ostani element
    $oImportCSVController->oSourceElements->postParse();
    try {
      $oImportCSVController->oSourceElements->doRecountProductStockSupply();
    } catch (Exception $ex) {
      $oImportCSVController->addErrorLog($ex->getMessage());
    }

    if (!empty($oImportCSVController->aErrorLog) || !empty($oImportCSVController->oSourceElements->aErrorLog)) {
      $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
                                                            print_r($oImportCSVController->oSourceElements->aErrorLog, true));
    }	
  }
  $sBaseFilename = basename($sFilename);
  $sBasePathOldFiles = str_replace($sBaseFilename, '', $sFilename).'/old/';
  @rename($sFilename, $sBasePathOldFiles.$sBaseFilename);

  if ($code == 7) {
    $elementsToProceedAtTheEnd = $oImportCSVController->oSourceElements->temptoProceedAtTheEndOfScript;
  }

  unset($oImportCSVController);
  $n++;
}

// jak wszystko zostalo opierdolone
if ($n >= 7) {
  $oImportCSVController = new importXMLController($sSource, $sElementName, $bTestMode, $iLimitIteration);
  $oImportCSVController->oSourceElements->doMovedToErpUPDATE();
  $oImportCSVController->oSourceElements->trySoledOutProducts($sDataUpdate);
}
// TODO mail z informacją o imporcie


if (!empty($elementsToProceedAtTheEnd)) {

  $oImportCSVController = new importXMLController($sSource, $sElementName, $bTestMode, $iLimitIteration);

  foreach($elementsToProceedAtTheEnd as $element) {

    $oImportCSVController->oSourceElements->postParseElement($element);

  }
}

while (false !== ($entry = $oImportCSVController->oSourceElements->importSaveXMLData())) {
    // czyszczenie pozostałych w folderze plików XML
    unlink($entry);
}

exec('/opt/php-5.4.45/bin/php -f /var/www/vhosts/profit24.pl/httpdocs/LIB/FixAutomats/CheckInvaildProducts.php');
echo 'koniec';