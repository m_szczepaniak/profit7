<?php
/**
 * Skrypt importu produktów ze źródła Panda do Profit24.pl
 * 
 * @author Arkadiusz Golba
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');

$bTestMode = false; //  czy test mode
$iLimitIteration = 10000000;
$sSource = 'panda';
$sElement = 'status';
$oImportXMLController = new importXMLController($sSource, $sElement, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
    $sFilename = $oImportXMLController->oSourceElements->importSaveXMLData();

} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'XML/panda/_plik_short.xml';
}

if ($sFilename !== false && $sFilename != '') {

    // mamy XML'a przechodzimy do parsowania
    $oImportXMLController->parseXMLFile($sFilename, 'PRODUCTS', 'PRODUCT');
    /*
    dump($oImportXMLController->aErrorLog);
    dump($oImportXMLController->oSourceElements->aErrorLog);
     */
    // hotfix wyłączamy produkty które są niedostępne przez 1 dzień w pliku !!!
    $sSql = '
UPDATE `products_stock` 
SET panda_status = "0",
    panda_stock = "0"
WHERE 
panda_status = "1" 
AND 
panda_last_import <= DATE_SUB( CURDATE( ) , INTERVAL 1 DAY )
';
    Common::Query($sSql);

    $oImportXMLController->sendInfoMail("IMPORT ".$sSource." ".$sElement." - OK", print_r($oImportXMLController->aLog, true).
        print_r($oImportXMLController->oSourceElements->aLog, true));
    $oImportXMLController->sendInfoMail("IMPORT ".$sSource." ".$sElement." ERR", print_r($oImportXMLController->aErrorLog, true).
        print_r($oImportXMLController->oSourceElements->aErrorLog, true));
} else {
    // TODO komunikat błędu pobierania/zapisu pliku XML

}


// TODO mail z informacją o imporcie

echo 'koniec';

?>
;