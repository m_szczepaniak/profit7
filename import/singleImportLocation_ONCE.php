<?php
/**
 * Skrypt importu produktów ze źródła Super siódemka do Profit24.pl
 *
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');
global $pDbMgr;

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_raporty/Module_inventory_shelf_not_equal.class.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/Admin.class.php');

$smarty = new \Smarty();
$std = new \Admin($smarty);
$oinve = new \omniaCMS\modules\m_raporty\Module__raporty__inventory_shelf_not_equal($std);
$oinve->doMove_products_to_locations();