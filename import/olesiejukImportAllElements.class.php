<?php
/**
 * Skrypt importu produktów ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importCSVController.class.php');	

// ---
// CONFIG
$bTestMode = false; //  czy test mode
$iLimitIteration = 10000000000;
$sSource = 'olesiejuk';

/*
 * UWAGA MODUL NIE JEST UZYWANY PONIEWAZ OLESIEJUK NIE WYSTAWIA STAWKI VAT PRODUKTU
 * 
 * 
$sElementName = 'previews'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/zapowiedzi.csv';
}
	 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	dump($oImportCSVController->aErrorLog);
	dump($oImportCSVController->oSourceElements->aErrorLog);
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);
*/

// -------------------------------------------------
// AUTORZY

$sElementName = 'authors'; // najpiew autorzy
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/autorzy.csv';
}
	 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);


// -------------------------------------------------
// OPRAWY


$sElementName = 'bindings';
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/oprawy.csv';
}
	 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);


// -------------------------------------------------
// SERIE

$sElementName = 'series';
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/serie.csv';
}
	 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);
	 

// -------------------------------------------------
// WYDAWCY

$sElementName = 'publishers'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);
if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/wydawcy.csv';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);


// -------------------------------------------------
// KATEGORIE

$sElementName = 'categories'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);
if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/dzialy.csv';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);

// -------------------------------------------------
// DODATKI

$sElementName = 'attachments'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/dodatki.csv';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);

// -------------------------------------------------
// WIĄZANIE PRODUKTY -> ZAŁĄCZNIKI PRODUKTOW

$sElementName = 'productsAttachments'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/wiazanie_dodatkowi.csv';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);
	 
// TODO mail z informacją o imporcie

// -------------------------------------------------
// DODANIE PRODUKTOW

$sElementName = 'products'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/products.csv';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																												print_r($oImportCSVController->oSourceElements->aLog, true));
	$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																												print_r($oImportCSVController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
	 
unset($oImportCSVController);
	 
// TODO mail z informacją o imporcie


echo 'koniec';
