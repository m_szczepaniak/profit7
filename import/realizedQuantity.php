<?php

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['common']['ommit_trigger_database'] = true;

include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
ini_set("memory_limit", '3G');



$sSql = 'SELECT O.id, OI.quantity, CONCAT(YEAR(O.order_date), MONTH(O.order_date)) as `month`
FROM orders_items AS OI
JOIN orders AS O 
  ON OI.order_id = O.id AND 
  O.order_status = "4" AND 
  DATE(O.order_date) >= "2017-10-01"
  AND DATE(O.order_date) <= "2018-01-01"
  AND O.website_id = "5"
';
$data = Common::GetAll($sSql);
$ordersMonth = [];
foreach ($data as $row) {
    $ordersMonth[$row['month']][$row['id']] += $row['quantity'];
}

// dump($ordersMonth);
$ordersStatistic = [];
foreach ($ordersMonth as $month => $orders) {
    foreach ($orders as $orderNumber => $ordersQuantities) {
        $ordersStatistic[$month][$ordersQuantities] ++;
    }
    $ordersMonthStats[$month] = count($orders);
    arsort($ordersStatistic[$month], SORT_NUMERIC);
}
echo 'MIESIĄC;ILOŚĆ EGZ. PRODUKTÓW;ILOŚĆ PRODUKTÓW W ZAMÓWIENIACH; OGÓLNA ILOŚĆ PRODUKTÓW W MIESIĄCU'."\n";
foreach ($ordersStatistic as $month => $rows) {
    echo ';;;'. $ordersMonthStats[$month]. "\n";
    foreach ($rows as $quantityProducts => $quantityProductsOrders) {
        echo $month . ';' . $quantityProducts . ';' . $quantityProductsOrders . "; \n";
    }
}
