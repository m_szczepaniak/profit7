<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');



$sSql = '
        UPDATE products_stock AS PS
        SET 
          profit_g_location = "",
          profit_x_location = ""
         WHERE 
            PS.profit_g_act_stock_unavailable_last_update < DATE_SUB(NOW(), INTERVAL 20 HOUR)
            AND PS.profit_g_act_stock_unavailable_last_update IS NOT NULL
            AND PS.profit_g_act_stock = 0
            AND (PS.profit_g_location <> "" OR PS.profit_x_location <> "")
             
         ';
$pDbMgr->Query('profit24', $sSql);