<?php
/**
 * Skrypt importu produktów ze źródła ABE do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
// set_time_limit(90800); // maksymalny czas wykonywania 3 godziny - 
set_time_limit(272400);
ini_set("memory_limit", '1G');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');	


$bTestMode = false; //  czy test mode
$iLimitIteration = 50000000000;
$sSource = 'abe';
$sElement = 'products';
$oImportXMLController = new importXMLController($sSource, $sElement, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
  $aCatBId = $oImportXMLController->oSourceElements->getSourceSymbolCategories($oImportXMLController->oSourceElements->iSourceId);

  //----------------------------------------------------------------------------
  $sLastUpdate = $oImportXMLController->oSourceElements->getLastAbeUpdate();

  foreach ($aCatBId as $sCatBIdId) {

    // pobieramy na dzień dobry wszystkie produkty
    $sFilename = $oImportXMLController->oSourceElements->importSaveXMLData($sCatBIdId, $sLastUpdate);

    if ($sFilename !== false && $sFilename != '') {

      // mamy XML'a przechodzimy do parsowania
      $oImportXMLController->parseXMLFile($sFilename, 'ONIXMessage', 'Product');
    } else {
      // TODO komunikat błędu pobierania/zapisu pliku XML
      break;
    }
    $oImportXMLController->oSourceElements->clearProductData();
    //----------------------------------------------------------------------------
  }
  $oImportXMLController->oSourceElements->setLastAbeUpdate($sLastUpdate);
} else {
  // link testowy na sztywno aby nie pobierać
  $sFilename = 'XML/abe/books.XML';
  
  if ($sFilename !== false && $sFilename != '') {

    // mamy XML'a przechodzimy do parsowania
    $oImportXMLController->parseXMLFile($sFilename, 'ONIXMessage', 'Product');
  } else {
    // TODO komunikat błędu pobierania/zapisu pliku XML
    //continue;
  }
  $oImportXMLController->oSourceElements->clearProductData();
}

$oImportXMLController->sendInfoMail("IMPORT ABE OK", print_r($oImportXMLController->aLog, true).
																											print_r($oImportXMLController->oSourceElements->aLog, true));
$oImportXMLController->sendInfoMail("IMPORT ABE ERR", print_r($oImportXMLController->aErrorLog, true).
																											print_r($oImportXMLController->oSourceElements->aErrorLog, true));
// TODO mail z informacją o imporcie

echo 'koniec';

?>
