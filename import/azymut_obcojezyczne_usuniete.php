<?php

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);


include_once ('import_func.inc.php');
// zmienne
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
$oCommonSynchro->iSourceId = '12';


function unavailableProduct($iId) {
  global $pDbMgr;
  
  $aValues = ['prod_status' => '0'];
  return $pDbMgr->Update('profit24', 'products', $aValues, ' id = '.$iId);
}

// zmienne
$sFilesToParse = getAzymutXMLFTPFile('deleted', "AzymutDeleted.xml");
if($sFilesToParse === false){
  die("brak pliku lub błąd");
}

$sLocalFile = $sFilesToParse;
if (file_exists($sLocalFile)) {
  /* var $xml SimpleXMLElement  */
  $xml = simplexml_load_file($sLocalFile);
  foreach ($xml->book as $item) {
    $sId = (string)$item->attributes()['indeks'];
    $aProduct = getAzymutStatus($sId);
    if (!empty($aProduct)) {
      unavailableProduct($aProduct['id']);
    }
  }
}
@rename($sFilesToParse,
        $aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/old/AzymutDeleted.xml_'.date('d_m_Y').'.xml');
