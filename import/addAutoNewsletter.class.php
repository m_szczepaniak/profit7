<?php
/**
 * Klasa automatycznego dodawania, użytkownika do newslettera, na podstawie zakupionych produktów
 * 
 * Dodawane będą w pierwszej kolejności kategorie produktu, najdroższego, limit 5 kategorii
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class addAutoNewsletterRecipients {
  private $pDbMgr;
  private $bTestMode;

  function __construct($pDbMgr, $bTestMode) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }
  
  /**
   * Metoda dodaje nowych subskrybentów newslettera, 
   * na podstawie złożonych przez nich zamówień
   */
  public function doAddSubscribents() {
    // dolaczenie klasy newslettera
    // pijany drwal to wymyślił
    $_GET['lang_id'] = '1';
    $iPId = getModulePageId('m_newsletter');
    include_once('modules/m_newsletter/client/Newsletter.class.php');
		$oNewsletter = new Newsletter('',$iPId,array(),'');
    $aRecipients = $this->_getUnsubscribedRecipents();
    foreach ($aRecipients as $aUserData) {
      $aCategories = $this->_getCategoryFromOrders($aUserData['id']);
      if ($this->bTestMode === FALSE) {
        if (!empty($aCategories)) {
          $oNewsletter->addRecipient($aUserData['email'], $aCategories, true);
        }
      }
    }
  }// end of doAddSubscribents() method
  
  
  /**
   * Pobieramy zrealizowanych uzytkowników, który mają zgodę na otrzymywanie newslettera,
   * nie są już zapisani na newsletter,
   * mają zrealizowane zamówienie,
   * od realizacji zamówienie zostało zrealizowane maksymalnie jeden dzien, ponieważ:
   *  jeśli ktoś dzisiaj zrealizował zamówienie i dzisiaj się wypisał z newslettera
   *  to nie powinien następnego dnia zostać ponownie dopisany.
   * 
   * @return array
   * 
   */
  private function _getUnsubscribedRecipents() {
    
    $sSql = " 
SELECT UA.email, O.id
FROM  users_accounts AS UA 
JOIN orders AS O
  ON O.user_id = UA.id
LEFT JOIN newsletters_recipients AS NR 
  ON UA.email = NR.email 
WHERE 
UA.promotions_agreement = '1' AND 
NR.id IS NULL AND 
O.website_id = '1' AND
O.order_status = '4' AND
O.status_4_update > DATE_SUB(CURDATE(), INTERVAL 1 DAY)
GROUP BY UA.email
ORDER BY O.value_brutto DESC
LIMIT 5

             ";
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of _getUnsubscribedRecipents() method
  
  
  /**
   * Metoda wybiera kategorię newslettera, dla danego zamówienia,
   * maksymalnie 5 ze względów wydajnościowych
   * 
   * @param int $iOId
   * @return array
   */
  private function _getCategoryFromOrders($iOId) {
    
    $sSql = '
SELECT NC.id, "1" AS confirm
FROM orders AS O
JOIN orders_items AS OI
  ON OI.order_id = O.id
JOIN products_extra_categories AS PEC
  ON OI.product_id = PEC.product_id
JOIN newsletters_categories AS NC
  ON (NC.send_categories = PEC.page_id AND NC.public = "1" AND NC.unregistred_cat = "0")
JOIN newsletters_categories_groups AS NCG
  ON NCG.id = NC.group_id 
WHERE O.id = '.$iOId.'
GROUP BY NC.send_categories
LIMIT 5
      ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql);
  }// end of _getCategoryFromOrders() method
  
  
}// end of Recipients() Class


error_reporting(E_ERROR | E_WARNING | E_PARSE); 
//error_reporting(E_ALL); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 

$bTestMode = FALSE;
$oAutoAddNewsletterRecipients = new addAutoNewsletterRecipients($pDbMgr, $bTestMode);
$oAutoAddNewsletterRecipients->doAddSubscribents();

