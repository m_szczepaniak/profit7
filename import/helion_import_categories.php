<?php
/**
 * Przetwarzanie kategorii heliona wrzucenie do tabeli products_helion_categories, 
 *	dodanie parent_id w przypadku kiedy jest zdefiniowany
 */
ob_start();
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

include_once ('import_func.inc.php');


function checkAddCategory($iHelionCatId, $sHelionCatName, $sBookstore, $iHelionParentId = 0) {
	global $aConfig;
	
	if (intval($iHelionCatId) <=0 && trim($sHelionCatName) == '') {
		// brak danych
		return true;
	}
	
	
	$sSql = "SELECT id, name FROM ".$aConfig['tabls']['prefix']."products_helion_categories
					 WHERE id=".$iHelionCatId." AND source='".$sBookstore."'
					 LIMIT 1";
	$aCatData = Common::GetRow($sSql);
	if (intval($aCatData['id']) <= 0) {
		// brak kategorii, dodajemy nową kategorię
		$aValues = array(
				'id' => $iHelionCatId,
				'name' => $sHelionCatName,
				'source' => $sBookstore,
		);
		if ($iHelionParentId > 0) {
			$aValues['parent_id'] = $iHelionParentId;
		}
		echo 'dodawanie<br />';
		dump($aValues);
		if (Common::Insert($aConfig['tabls']['prefix']."products_helion_categories", $aValues) === false) {
			return false;
		}
	} else {
		// istnieje kategoria o tym id i źródle
		// sprawdźmy czy ma taką samą nazwę i rodzica
		$sSql = "SELECT id, name FROM ".$aConfig['tabls']['prefix']."products_helion_categories
					 WHERE id=".$iHelionCatId." AND name='".$sHelionCatName."' AND source='".$sBookstore."' ". ($iHelionParentId > 0 ? "AND parent_id=".$iHelionParentId : "").
					 " LIMIT 1";
		$aCatData = Common::GetRow($sSql);
		if (empty($aCatData)) {
			// nie istnieje nadpisujemy nazwę kategorii i parent_id
			$aValues = array(
					'name' => $sHelionCatName
			);
			if ($iHelionParentId > 0) {
				$aValues['parent_id'] = $iHelionParentId;
			}
			echo 'aktualizacja<br />';
			dump($aValues);
			if (Common::Update($aConfig['tabls']['prefix']."products_helion_categories", $aValues, " id=".$iHelionCatId." AND source='".$sBookstore."' LIMIT 1") === false) {
				return false;
			}
		} else {
			// istnieje zwracamy true i do domu ...
			return true;
		}
		return true;
	}
	return true;
}

/**
 * parsuje xml z kategoriami
 * @param $oXml
 * @return unknown_type
 */
function parseHelionCategories($sFilename, $sBookstore) {
	$oXml = new XMLReader();
	$oXml->open($sFilename);
	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'details':
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'item':
						$aCatItem = array(
								'grupa_nad' => $oXml->getAttribute('grupa_nad'),
								'id_nad' => $oXml->getAttribute('id_nad'),
								'grupa_pod' => $oXml->getAttribute('grupa_pod'),
								'id_pod' => $oXml->getAttribute('id_pod'),
						);
						Common::BeginTransaction();
						dump($aCatItem);
						if (checkAddCategory($aCatItem['id_nad'], $aCatItem['grupa_nad'], $sBookstore) === false) {
							echo ' błąd NAD kategorii : <br />';
							dump($aCatItem);
							Common::RollbackTransaction();
							die;
						}
						if (checkAddCategory($aCatItem['id_pod'], $aCatItem['grupa_pod'], $sBookstore, $aCatItem['id_nad']) === false) {
							echo ' błąd POD kategorii : <br />';
							dump($aCatItem);
							Common::RollbackTransaction();
							die;
						}
						
						echo ' KONIEC ';
						Common::CommitTransaction();
					break;
				}
		}
	}
}

echo ' helion kategorie : <br />';
if(!downloadHelionXML("http://helion.pl/plugins/new/xml/lista-katalog.cgi","helion-kategorie.xml")){
	sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z helion.pl");
	die("błąd pobierania xml produktow");
}
parseHelionCategories($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/helion-kategorie.xml",'helion');


echo ' septem kategorie : <br />';
if(!downloadHelionXML("http://septem.pl/plugins/new/xml/lista-katalog.cgi","septem-kategorie.xml")){
	sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z helion.pl");
	die("błąd pobierania xml produktow");
}
parseHelionCategories($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/septem-kategorie.xml",'septem');


echo ' sensus kategorie : <br />';
if(!downloadHelionXML("http://sensus.pl/plugins/new/xml/lista-katalog.cgi","sensus-kategorie.xml")){
	sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z helion.pl");
	die("błąd pobierania xml produktow");
}
parseHelionCategories($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/sensus-kategorie.xml",'sensus');


echo ' onepress kategorie : <br />';
if(!downloadHelionXML("http://onepress.pl/plugins/new/xml/lista-katalog.cgi","onepress-kategorie.xml")){
	sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z helion.pl");
	die("błąd pobierania xml produktow");
}
parseHelionCategories($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/onepress-kategorie.xml",'onepress');


$bufferContent = ob_get_contents();
ob_end_clean();
sendInfoMail("Aktualizacja kategorii Helion","Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." zakończono aktualizacje kategorii Helion\n".$bufferContent);
echo $bufferContent;
?>
