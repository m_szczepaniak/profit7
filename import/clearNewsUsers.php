<?php
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');

$_GET['lang_id']=1;
$aConfig['_settings']['site'] =& getSiteSettings();


$productsStats = getCountProductsInOrders();

// czyszczenie nowosci po ustalonym czasie
$iClearAfter=$aConfig['_settings']['site']['clear_news_after'];
if($iClearAfter > 0){
	
	$sSql="SELECT * FROM ".$aConfig['tabls']['prefix']."products_news 
					WHERE DATE_ADD(news_from, INTERVAL ".$iClearAfter." DAY) < NOW()";
	$aToDelete = Common::GetAll($sSql);
	
	foreach($aToDelete as $aNews) {
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='0'
							WHERE id = ".$aNews['product_id'];
		if(Common::Query($sSql) === false) {
			echo "Error updating product table <br>\n";
			dump($sSql);
		}
		
		if(existShadow($aNews['product_id'])) {
			$aShadow = getProductShadow($aNews['product_id']);
			$aShadow['is_news'] = 0; 
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news = '0',
							order_by = ".getOrderByNr($aShadow, $productsStats[$aItem['id']]).",
							order_by_status_old_style = ".getOrderByStatusOldStyle($aShadow)."
							WHERE id = ".$aNews['product_id'];
			if(Common::Query($sSql) === false) {
				echo "Error updating product_shadow table <br>\n";
				dump($sSql);
			}
		}
				
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_news
						 WHERE product_id = ".$aNews['product_id'];
		if(Common::Query($sSql) === false) {
			echo "Error deleting from news <br>\n";
			dump($sSql);
		}
		
	}
	
}

	if($aConfig['_settings']['site']['delete_inactive_accounts_after'] > 0){
    /*
		// czyszczenie niepotwierdzonych kont
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."users_accounts
					 WHERE active = '0' AND deleted = '0' AND
					 			 (created IS NULL OR DATE_ADD(created, INTERVAL ".$aConfig['_settings']['site']['delete_inactive_accounts_after']." DAY) < NOW())";
		if(Common::Query($sSql) === false) {
			echo "Error clearing users accounts <br>\n";
			dump($sSql);
		}
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
					 WHERE confirmed = '0' AND
					 			 (DATE_ADD(added, INTERVAL ".$aConfig['_settings']['site']['delete_inactive_accounts_after']." DAY) < NOW())";
		if(Common::Query($sSql) === false) {
			echo "Error clearing not confirmed newsletter reciptients <br>\n";
			dump($sSql);
		}
    */
	}

	
?>