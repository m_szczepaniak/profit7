<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module_Common.class.php');
$oCommon = new Module_Common();

//$sItestIdSQLA = ' AND A.id = 520289 ';//NULL
//$sItestIdSQLB = ' AND B.id = 792812 ';//NULL


	$sSql="SELECT source, discount, mark_up 
				 FROM ".$aConfig['tabls']['prefix']."products_general_discount
				 WHERE recount = '1'";
	$aSources = Common::GetAll($sSql);
	if(!empty($aSources)){
		foreach($aSources as $aSource){
			dump($aSource);
			
			$sUSql= "UPDATE products_general_discount 
								SET recount = '0'
								WHERE source='".$aSource['source']."'";
			Common::Query($sUSql);
			
			// update - limity nie uwzględniają rabatu pakietu
			$sSql="SELECT A.id, A.price_brutto, A.vat, A.wholesale_price,
										IFNULL(B.discount_limit,0) AS discount_limit, 
										IFNULL(PS.discount_limit,0) AS series_discount_limit, 
										IFNULL( C.discount_limit, 0 ) AS first_source_discount_limit,
                    PA.price_netto AS att_price_netto
						 FROM ".$aConfig['tabls']['prefix']."products A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
							ON B.id = A.publisher_id
						 LEFT JOIN products_source_discount AS C 
							ON C.first_source = A.created_by
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series PTS
							ON A.id = PTS.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_series PS
							ON PTS.series_id = PS.id AND PS.publisher_id=A.publisher_id
						 LEFT JOIN products_attachments AS PA
							ON PA.product_id = A.id
						 WHERE A.source='".$aSource['source']."' AND A.packet='0'".$sItestIdSQLA;
			$oToRecount = Common::PlainQuery($sSql);
			
			while ($aItem =& $oToRecount->fetchRow(DB_FETCHMODE_ASSOC)) {
        $fGeneralSourceDiscount = $aSource['discount'];
				// jesli ustawiony limit dla wydawnictwa, i rabat jest od niego wiekszy,
				// jako wartosc rabatu przyjmujemy limit
        
        // tutaj rabat obliczamy na podstawie Narzutu
        if (floatval($aItem['att_price_netto']) == 0.00 && $aSource['mark_up'] > 0.00 && $aItem['wholesale_price'] > 0) {
           $fGeneralSourceDiscount = Common::formatPrice2(100 - (((($aItem['wholesale_price'] * ($aSource['mark_up'] / 100)) + $aItem['wholesale_price']) / $aItem['price_brutto']) * 100));
        }
        
        
				if(($aItem['discount_limit'] > 0) && ($fGeneralSourceDiscount > $aItem['discount_limit'])){
					$fDiscount = $aItem['discount_limit'];
				} else {
					$fDiscount = $fGeneralSourceDiscount;
				}
        
				// jesli ustawiony limit dla wydawnictwa, i rabat jest od niego wiekszy,
				// jako wartosc rabatu przyjmujemy limit
        // @mod 28.06.2013 - dodanie ignorowania limitu rabatu wydawcy w promocji
        // @mod 17.03.2014 - ignorowanie limitu rabatu obejmuje limit serii wydawniczej oraz samego wydawnictwa
        if ($aItem['ignore_publisher_limit'] != '1') {
          
          if(($aItem['discount_limit'] > 0) && ($fDiscount > $aItem['discount_limit'])){
            $fDiscount = $aItem['discount_limit'];
          }
          
          if(($aItem['series_discount_limit'] > 0) && ($fDiscount > $aItem['series_discount_limit'])){
            $fDiscount = $aItem['series_discount_limit'];
          }
        }
        
				/*
				if(($aItem['first_source_discount_limit'] > 0) && ($fDiscount > $aItem['first_source_discount_limit'])){
					$fDiscount = $aItem['first_source_discount_limit'];
				}
				
				if(($aItem['series_discount_limit'] > 0) && ($fDiscount > $aItem['series_discount_limit'])){
					$fDiscount = $aItem['series_discount_limit'];
				}
				*/
        
				$fDiscountValue = Common::formatPrice2($aItem['price_brutto'] * ($fDiscount / 100));			
				$fNewBrutto = $aItem['price_brutto'] - $fDiscountValue;
				$fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $aItem['vat']/100));
				
				$aValues = array(
					'discount' => $fDiscount,
					'discount_value' => $fDiscountValue,
					'price_brutto' => $fNewBrutto,
					'price_netto' => $fNewNetto
				);
				if(Common::Update($aConfig['tabls']['prefix'].'products_tarrifs',
													$aValues,
													"product_id = ".$aItem['id']." AND general_discount = '1'") === false){
						// error report
						echo "error chaniging tarrif for ".$aItem['id']."\n";
						dump($aValues);								
				}
				
				// przeliczamy pakiety
				// pobierz pakiety w ktorych wystepuje dany produkt
				$aPacketsProduct = $oCommon->getPacketsProduct($aItem['id']);
				if (!empty($aPacketsProduct) && is_array($aPacketsProduct)) {
					foreach ($aPacketsProduct as $iPacketProduct) {
						// zmień ceny składowych pakietu
						if ($oCommon->updateProductPacketPrice($aItem['id'], $iPacketProduct) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
						// zmień ceny pakietu bez aktualizacji autorów
						if ($oCommon->updatePacketAuthorsPrices($iPacketProduct, false) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
					}
				}
			}
			unset($oToRecount);
		}
	}
	
	// przeliczamy limity wg wydawcy
	$sSql="SELECT id, discount_limit 
				 FROM ".$aConfig['tabls']['prefix']."products_publishers
				 WHERE recount_limit = '1'";
	$aPublishers = Common::GetAll($sSql);
	if(!empty($aPublishers)){
		foreach($aPublishers as $aPublisher){
			
			$sUSql= "UPDATE products_publishers 
								SET recount_limit = '0'
								WHERE id = ".$aPublisher['id'];
			Common::Query($sUSql);
			
			// update - limity nie uwzględniają rabatu pakietu
			$sSql="SELECT A.id, B.price_brutto, B.vat, A.discount, B.wholesale_price,
										IFNULL(A.promotion_id,0) AS promotion_id, 
										IFNULL(C.discount,0) AS promotion_discount, 
                    IFNULL(PS.discount_limit,0) AS series_discount_limit, 
										IFNULL(D.discount,0) AS global_discount, 
                    IFNULL(D.mark_up,0) AS mark_up, 
										A.general_discount,
                    C.ignore_publisher_limit,
                    PA.price_netto AS att_price_netto
						 FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
						 JOIN ".$aConfig['tabls']['prefix']."products B
              ON B.id=A.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_promotions C
              ON C.id = A.promotion_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series PTS
							ON A.product_id = PTS.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_series PS
							ON PTS.series_id = PS.id AND PS.publisher_id = B.publisher_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_general_discount D
              ON D.source=B.source
             LEFT JOIN products_attachments AS PA
							ON PA.product_id = A.product_id
						 WHERE B.publisher_id = ".$aPublisher['id']."
						 AND A.isdefault = '0'
						 AND B.packet = '0'".$sItestIdSQLB;
							
			$oToRecount = Common::PlainQuery($sSql);
			
			while ($aItem =& $oToRecount->fetchRow(DB_FETCHMODE_ASSOC)) {
        
        if (floatval($aItem['att_price_netto']) == 0.00 && $aItem['mark_up'] > 0.00 && $aItem['wholesale_price'] > 0) {
           $aItem['global_discount'] = Common::formatPrice2(100 - (((($aItem['wholesale_price'] * ($aItem['mark_up'] / 100)) + $aItem['wholesale_price']) / $aItem['price_brutto']) * 100));
        }
        
				if($aItem['general_discount'] == '1'){
					$fDiscount = $aItem['global_discount'];
				}
				else {
					$fDiscount = $aItem['discount'];
				}
				
				// jesli ustawiony limit dla wydawnictwa, i rabat jest od niego wiekszy,
				// jako wartosc rabatu przyjmujemy limit
        // @mod 28.06.2013 - dodanie ignorowania limitu rabatu wydawcy w promocji
        // @mod 17.03.2014 - ignorowanie limitu rabatu obejmuje limit serii wydawniczej oraz samego wydawnictwa
        if ($aItem['ignore_publisher_limit'] != '1') {
          
          if(($aPublisher['discount_limit'] > 0) && ($fDiscount > $aPublisher['discount_limit'])){
            $fDiscount = $aPublisher['discount_limit'];
          }
          
          if(($aItem['series_discount_limit'] > 0) && ($fDiscount > $aItem['series_discount_limit'])){
            $fDiscount = $aItem['series_discount_limit'];
          }
        }
				
				$fDiscountValue = Common::formatPrice2($aItem['price_brutto'] * ($fDiscount / 100));			
				$fNewBrutto = $aItem['price_brutto'] - $fDiscountValue;
				$fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $aItem['vat']/100));
				
				$aValues = array(
					'discount' => $fDiscount,
					'discount_value' => $fDiscountValue,
					'price_brutto' => $fNewBrutto,
					'price_netto' => $fNewNetto
				);
				if(Common::Update($aConfig['tabls']['prefix'].'products_tarrifs',
													$aValues,
													"id = ".$aItem['id']) === false){
						// error report
						echo "error chaniging tarrif ".$aItem['id']."\n";
						dump($aValues);								
				}
				
				// przeliczamy pakiety
				// pobierz pakiety w ktorych wystepuje dany produkt
				$aPacketsProduct = $oCommon->getPacketsProduct($aItem['id']);
				if (!empty($aPacketsProduct) && is_array($aPacketsProduct)) {
					foreach ($aPacketsProduct as $iPacketProduct) {
						// zmień ceny składowych pakietu
						if ($oCommon->updateProductPacketPrice($aItem['id'], $iPacketProduct) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
						// zmień ceny pakietu bez aktualizacji autorów
						if ($oCommon->updatePacketAuthorsPrices($iPacketProduct, false) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
					}
				}
			}
			unset($oToRecount);
		}
	}

	
	// przeliczamy limity wg serii wydawniczej
	$sSql="SELECT id, discount_limit, publisher_id
				 FROM ".$aConfig['tabls']['prefix']."products_series
				 WHERE recount_limit = '1'";
	$aSeries = Common::GetAll($sSql);
	if(!empty($aSeries)){
		foreach($aSeries as $aSerie){
			
			$sUSql= "UPDATE products_series 
								SET recount_limit = '0'
								WHERE id = ".$aSerie['id'];
			Common::Query($sUSql);
			
			// update - limity nie uwzględniają rabatu pakietu
			$sSql="SELECT A.id, B.price_brutto, B.vat, A.discount, B.wholesale_price,
										IFNULL(A.promotion_id,0) AS promotion_id, 
										IFNULL(C.discount,0) AS promotion_discount, 
										IFNULL(PS.discount_limit,0) AS series_discount_limit, 
										IFNULL(D.discount,0) AS global_discount, 
                    IFNULL(D.mark_up,0) AS mark_up, 
										A.general_discount,
                    PA.price_netto AS att_price_netto
						 FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id=A.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_promotions C
						 ON C.id = A.promotion_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_general_discount D
						 ON D.source=B.source
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series PTS
							ON B.id = PTS.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_series PS
							ON PTS.series_id = PS.id AND PS.publisher_id=B.publisher_id
             LEFT JOIN products_attachments AS PA
							ON PA.product_id = A.product_id
						 WHERE PS.id = ".$aSerie['id']." AND PS.publisher_id=".$aSerie['publisher_id']."
						 AND A.isdefault = '0'
						 AND B.packet = '0'".$sItestIdSQLB;
			var_dump($sSql);
			$oToRecount = Common::PlainQuery($sSql);
			while ($aItem =& $oToRecount->fetchRow(DB_FETCHMODE_ASSOC)) {
        
        if (floatval($aItem['att_price_netto']) == 0.00 && $aItem['mark_up'] > 0.00 && $aItem['wholesale_price'] > 0) {
          // (100 - ((((7,12 * (15 / 100)) + 7,12) / 8,9) * 100));
           $aItem['global_discount'] = Common::formatPrice2(100 - (((($aItem['wholesale_price'] * ($aItem['mark_up'] / 100)) + $aItem['wholesale_price']) / $aItem['price_brutto']) * 100));
        }
        
				if($aItem['general_discount'] == '1'){
					$fDiscount = $aItem['global_discount'];
				}
				else {
					$fDiscount = $aItem['discount'];
				}
				
				// jesli ustawiony limit na serie
				if(($aSerie['discount_limit'] > 0) && ($fDiscount > $aSerie['discount_limit'])){
					$fDiscount = $aSerie['discount_limit'];
				}

				
				$fDiscountValue = Common::formatPrice2($aItem['price_brutto'] * ($fDiscount / 100));			
				$fNewBrutto = $aItem['price_brutto'] - $fDiscountValue;
				$fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $aItem['vat']/100));
				
				$aValues = array(
					'discount' => $fDiscount,
					'discount_value' => $fDiscountValue,
					'price_brutto' => $fNewBrutto,
					'price_netto' => $fNewNetto
				);
				if(Common::Update($aConfig['tabls']['prefix'].'products_tarrifs',
													$aValues,
													"id = ".$aItem['id']) === false){
						// error report
						echo "error chaniging tarrif ".$aItem['id']."\n";
						dump($aValues);								
				}
				
				// przeliczamy pakiety
				// pobierz pakiety w ktorych wystepuje dany produkt
				$aPacketsProduct = $oCommon->getPacketsProduct($aItem['id']);
				if (!empty($aPacketsProduct) && is_array($aPacketsProduct)) {
					foreach ($aPacketsProduct as $iPacketProduct) {
						// zmień ceny składowych pakietu
						if ($oCommon->updateProductPacketPrice($aItem['id'], $iPacketProduct) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
						// zmień ceny pakietu bez aktualizacji autorów
						if ($oCommon->updatePacketAuthorsPrices($iPacketProduct, false) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
					}
				}
			}
			unset($oToRecount);
		}
	}
	
	
	
	// limit wg pierwotnego źródła
	$sSql="SELECT id, discount_limit, first_source
				 FROM ".$aConfig['tabls']['prefix']."products_source_discount
				 WHERE recount_limit = '1'";
	$aSources = Common::GetAll($sSql);
	if(!empty($aSources)){
		foreach($aSources as $aSource){
			
			$sUSql= "UPDATE products_source_discount 
								SET recount_limit = '0'
								WHERE id = ".$aSource['id'];
			Common::Query($sUSql);
			
			$sSql = "SELECT A.id, B.price_brutto, B.vat, A.discount, B.wholesale_price,
                    IFNULL(A.promotion_id,0) AS promotion_id, 
                    IFNULL(C.discount,0) AS promotion_discount, 
                    IFNULL(D.discount,0) AS global_discount, 
                    IFNULL(D.mark_up,0) AS mark_up, 
                    A.general_discount,
                    PA.price_netto AS att_price_netto
						 FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id=A.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_promotions C
						 ON C.id = A.promotion_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_general_discount D
						 ON D.source=B.source
             LEFT JOIN products_attachments AS PA
							ON PA.product_id = A.product_id
						 WHERE B.created_by = '".$aSource['first_source']."'
						 AND A.isdefault = '0'
						 AND B.packet = '0'".$sItestIdSQLB;
			
			
			$oToRecount = Common::PlainQuery($sSql);
			
			while ($aItem =& $oToRecount->fetchRow(DB_FETCHMODE_ASSOC)) {
        
        if (floatval($aItem['att_price_netto']) == 0.00 && $aItem['mark_up'] > 0.00 && $aItem['wholesale_price'] > 0) {
           $aItem['global_discount'] = Common::formatPrice2(100 - (((($aItem['wholesale_price'] * ($aItem['mark_up'] / 100)) + $aItem['wholesale_price']) / $aItem['price_brutto']) * 100));
        }
        
				if($aItem['general_discount'] == '1'){
					$fDiscount = $aItem['global_discount'];
				}
				else {
					$fDiscount = $aItem['discount'];
				}
				
				// jesli ustawiony limit dla wydawnictwa, i rabat jest od niego wiekszy,
				// jako wartosc rabatu przyjmujemy limit
				if(($aSource['discount_limit'] > 0) && ($fDiscount > $aSource['discount_limit'])){
					$fDiscount = $aSource['discount_limit'];
				}
				
				$fDiscountValue = Common::formatPrice2($aItem['price_brutto'] * ($fDiscount / 100));			
				$fNewBrutto = $aItem['price_brutto'] - $fDiscountValue;
				$fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $aItem['vat']/100));
				
				$aValues = array(
					'discount' => $fDiscount,
					'discount_value' => $fDiscountValue,
					'price_brutto' => $fNewBrutto,
					'price_netto' => $fNewNetto
				);
				if(Common::Update($aConfig['tabls']['prefix'].'products_tarrifs',
													$aValues,
													"id = ".$aItem['id']) === false){
						// error report
						echo "error chaniging tarrif ".$aItem['id']."\n";
						dump($aValues);
				} else {
					echo "changed tarrif ".$aItem['id']."\n<br />";
				}
				
				// przeliczamy pakiety
				// pobierz pakiety w ktorych wystepuje dany produkt
				$aPacketsProduct = $oCommon->getPacketsProduct($aItem['id']);
				if (!empty($aPacketsProduct) && is_array($aPacketsProduct)) {
					foreach ($aPacketsProduct as $iPacketProduct) {
						// zmień ceny składowych pakietu
						if ($oCommon->updateProductPacketPrice($aItem['id'], $iPacketProduct) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
						// zmień ceny pakietu bez aktualizacji autorów
						if ($oCommon->updatePacketAuthorsPrices($iPacketProduct, false) === false) {
							// error report
							echo "error chaniging tarrif for packet of product ".$aItem['id']."\n";
							dump($aValues);		
						}
					}
				}
			}
			unset($oToRecount);
		}
	}
  
  echo 'KONIEC';