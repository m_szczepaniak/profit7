<?php

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);


include_once ('import_func.inc.php');
// zmienne
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
$oCommonSynchro->iSourceId = '12';

// zmienne

//if ($aConfig['common']['status']	!= 'development') {
  $aFilesToParse = getAzymutXMLFTPFile('prices', "AzymutPrices.xml");
	if($aFilesToParse === false){
    sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml cen z Azymut");
    die("błąd pobierania xml cen");
  }
//}

if (file_exists($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/AzymutPrices.xml")) {
//	sendInfoMail('Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto aktualizacje cen azymut");
	$oXml = new XMLReader();
	$oXml->open($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/AzymutPrices.xml");
	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'prices':
						// KONIEC LISTINGU KATEGORII
						$oXml->close();
					break;
				}
			break;
		
		case XMLReader::ELEMENT :
			switch ($oXml->name) {
				case 'book':
					$sId=$oXml->getAttribute('indeks');
					$fPrice=(double)$oXml->getAttribute('detal');
					$fPriceWH = (double)$oXml->getAttribute('cena');
					$iVat=(int)$oXml->getAttribute('vat');
					$fKomplet=(double)$oXml->getAttribute('kplD');
					$f=(double)$oXml->getAttribute('kplD');
					$fKompletWH = (double)$oXml->getAttribute('kplH');
          $sStatusQuantity = $oXml->getAttribute('p');
          $sNewStatus = $oCommonSynchro->parseProductStock('azymutobc', $sStatusQuantity);
          
					// aktualizuj cene zalacznika
					if (($fKomplet > 0) && (substr($sId,-2) != 'KS')) {
						$aPrice = getAzymutAttachmentPrice($sId);
						if(!empty($aPrice)&&($fPrice > 0)&&(($fPrice!=$aPrice['price_brutto']) || $iVat!=$aPrice['vat'])) {
							$iVat=vatMapper($iVat);
							$aValues=array(
								'price_netto' => Common::formatPrice2($fPrice/(1+$iVat/100)),
								'price_brutto' => Common::formatPrice2($fPrice),
								'vat' => (int)$iVat,
							);
							if ((Common::Update($aConfig['tabls']['prefix']."products_attachments", $aValues,"id = ".$aPrice['id'])) === false) {
								echo("error updating ".$aPrice['id']."\n");
							}
						}
					} else { // aktualizuj cene ksiazki
						$aStatus = getAzymutobcStock($sId, 'azymutobc_wholesale_price');

						if($fKomplet > 0){
							$fNewPrice=$fKomplet;
						} else {
							$fNewPrice=$fPrice;
						}

						if ($fKompletWH > 0.00) {
							$fNewWHPriceNetto = Common::formatPrice2($fKompletWH);
						} else {
							$fNewWHPriceNetto = Common::formatPrice2($fPriceWH);
						}
            
            // zmiana ceny hurtowej z Netto na brutto
            $iVat=vatMapper($iVat);

//            if ($fNewWHPriceNetto > 0.00) {
//              $iDiscount = 1;// UWAGA tutaj tylko ustawiamy rabat dodatkowy
//              $fNewWHPriceNetto = $fNewWHPriceNetto - Common::formatPrice2($fNewWHPriceNetto * ($iDiscount / 100));
//              $fNewWHPriceBrutto = Common::formatPrice2($fNewWHPriceNetto*(1+$iVat/100));
//            }
            
            if ($fKomplet > 0) {
              $aAtt = getAttachment($aStatus['id']);
              if(empty($aAtt)){
                var_dump($aStatus['id']);
              }
            }
						if(!empty($aStatus)&&($fNewPrice!=$aStatus['azymutobc_price_brutto'] || $iVat != $aStatus['azymutobc_vat'] || $fNewWHPriceBrutto != $aStatus['azymutobc_wholesale_price'])){
							$fAttCorrection = 0;
							if(!($fKomplet > 0)){
								$aAtt = getAttachment($aStatus['id']);
								if(!empty($aAtt) && $aAtt['price_brutto'] > 0){
									$fAttCorrection = $aAtt['price_brutto'];
								}
							}
							$iVat=vatMapper($iVat);
							$aStatusValues = array(
								'azymutobc_wholesale_price' => $fNewWHPriceBrutto > 0.00 ? $fNewWHPriceBrutto : 'NULL',
								'azymutobc_price_netto' => Common::formatPrice2(($fPrice-$fAttCorrection)/(1+$iVat/100)),
								'azymutobc_price_brutto' =>  Common::formatPrice2($fNewPrice),
								'azymutobc_vat' => (int)$iVat,
                'azymutobc_status' => '1',
                'azymutobc_shipment_time' => '2',
                'azymutobc_stock' => $sNewStatus != '' ? $sNewStatus : 'NULL'
							);
							
							
							//dump($aStatusValues);
							if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aStatusValues,"id = ".$aStatus['id'])) === false) {
													echo("błąd aktualizacji ceny produktu ".$aStatus['id']."<br/>\n");
							}
						}
            // UWAGA optymalizacja z 06.12.2012 - zmiana stanu produktu przy cenach nie jest uwzględniana
						// tylko dla ksiazek
						// zmiana last import we wszystkich produktach które zostały zaktualizowane
						//updateLastImport(getProductIdByBookindex($sId));
					}
				break;
			}
		}
	}
  // usuniecie pliku
//  sendInfoMail('Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." zakończono aktualizacje cen azymut");
	@rename($aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/AzymutPrices.xml',
					$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/old/AzymutPrices.xml_'.date('d_m_Y').'.xml');
}

?>