<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();





dump('NOWOSCI KTÓRE POWINNY BYĆ ZAPOWIEDZIAMI WG STATUSU');
// -- tutaj część poprawiająca zapowiedzi w statusie nowości
// prawdopodobnie błąd pojawia się w triggerze podczas zmiany stanu z nowość na zapowiedź
// i podczas aktualizacji produktu w azymut
$sSql = "
  SELECT P.id, P.shipment_date, PN.page_id AS news_main_category, S.symbol AS source_symbol
  FROM  products AS P
  JOIN products_news AS PN 
    ON P.id = PN.product_id
  JOIN sources AS S 
    ON P.source = S.id
  WHERE P.prod_status =  '3'";
$aProds = Common::GetAll($sSql);
dump($aProds);
foreach ($aProds as $aProd) {
  // wywalamy z nowości
  $oCommonSynchro->delNews($aProd['id']);
  // dodajemy do zapowiedzi
  if ($aProd['shipment_date'] != '') {
    // wrzucamy do zapowiedzi
    $iPreviewsMainCategory = $oCommonSynchro->getPreviewPageIdByNewsPageId($aProd['news_main_category']);
    $oCommonSynchro->addPreviews($aProd['id'], $iPreviewsMainCategory);
  } elseif ($aProd['source_symbol'] != '1') {
    // status różny od profit24 - źródło wewnętrzne
    // nie mamy daty publikacji w tabeli produktu, wydobywamy z tabeli stanów
    $sSql = 'SELECT '.$aProd['source_symbol'].'_shipment_date 
             FROM products_stock
             WHERE id = '.$aProd['id'];
    $fShipmentDate = Common::GetOne($sSql);
    
    $aMatches = array();
    $bIsMatch = preg_match("/^\d{4}-\d{2}-\d{2}$/", $fShipmentDate, $aMatches);
    if ($bIsMatch === 1 && $fShipmentDate != '0000-00-00') {
      $bIsErr = false;
      // mamy datę podanę w stocku, ustawiamy w tabeli produktu i dodajemy do zapowiedzi
      $aValues = array(
          'shipment_date' => $fShipmentDate
      );
      Common::BeginTransaction();
      if (Common::Update('products', $aValues, 'id = '.$aProd['id']) === false) {
        $bIsErr = true;
      }
      if (Common::Update('products_shadow', $aValues, 'id = '.$aItem['id']) === false) {
        $bIsErr = true;
      }
    
      $iPreviewsMainCategory = $oCommonSynchro->getPreviewPageIdByNewsPageId($aProd['news_main_category']);
      if ($oCommonSynchro->addPreviews($aProd['id'], $iPreviewsMainCategory) === false) {
        $bIsErr = true;
      }
      if ($bIsErr === false) {
        // poszło
        Common::CommitTransaction();
        echo 'Wprowadzone datę wydania oraz dodano do zapowiedzi :'.$aProd['id']."<br />";
      } else {
        Common::RollbackTransaction();
        echo 'Wystąpił błąd podczas ustawiania daty dodania lub podcza dodawania do zapowiedzi :'.$aProd['id']."<br />";
      }
    }
  } else {
    echo 'brak daty wydania produktu : '.$aProd['id'];
  }
}
unset($aProd, $aProds);



dump('ZAPOWIEDZI BEZ DAT WYDANIA');
// poprawiamy produkty w statusie zapowiedzi z pustymi datami wydania
$sSql = "SELECT P.id, S.symbol as source_symbol, PP.id AS preview_id, P.publisher_id
         FROM products AS P
         JOIN sources AS S
          ON P.source = S.id
         LEFT JOIN products_previews AS PP
          ON PP.product_id = P.id
         WHERE P.prod_status =  '3'
           AND P.shipment_date IS NULL";
$aItems = Common::GetAll($sSql);
dump($aItems);
foreach ($aItems as $aItem) {
  // pobierzmy datę z tabeli ze stanami
  $sSql = 'SELECT '.$aItem['source_symbol'].'_shipment_date 
           FROM products_stock
           WHERE id = '.$aItem['id'];
  $fShipmentDate = Common::GetOne($sSql);
  
  $aMatches = array();
  $bIsMatch = preg_match("/^\d{4}-\d{2}-\d{2}$/", $fShipmentDate, $aMatches);
  if ($bIsMatch === 1 && $fShipmentDate != '0000-00-00') {
    $bIsErr = false;

    Common::BeginTransaction();
    // aktualizujemy datę w tabeli produktu
    $aValues = array(
        'shipment_date' => $fShipmentDate
    );
    if (Common::Update('products', $aValues, 'id = '.$aItem['id']) === false) {
      $bIsErr = true;
    }
    if (Common::Update('products_shadow', $aValues, 'id = '.$aItem['id']) === false) {
      $bIsErr = true;
    }

    if (intval($aItem['preview_id']) <= 0) {
      // nie jest zapowiedzią jeszcze
      
      $iPreviewsMainCategory = $oCommonSynchro->getPreviewMainCategory($aItem['source_symbol'], $aItem['id'], $aItem['publisher_id']);
      if ($oCommonSynchro->addPreviews($aItem['id'], $iPreviewsMainCategory) === false) {
        $bIsErr = true;
      }
    }
    
    if ($bIsErr === false) {
      // poszło
      Common::CommitTransaction();
      echo 'Wprowadzone datę wydania oraz dodano do zapowiedzi :'.$aItem['id']."<br />";
    } else {
      Common::RollbackTransaction();
      echo 'Wystąpił błąd podczas ustawiania daty dodania lub podcza dodawania do zapowiedzi :'.$aItem['id']."<br />";
    }
  } else {
    // produkt jest niedostępny, jeśli jest inaczej w nocy zostanie zmieniony na status dostępny
    $aValues = array(
        'prod_status' => '0'
    );
    if (Common::Update('products', $aValues, 'id = '.$aItem['id']) === false) {
      $bIsErr = true;
    }
    if (Common::Update('products_shadow', $aValues, 'id = '.$aItem['id']) === false) {
      $bIsErr = true;
    }
    if (intval($aItem['preview_id']) > 0) {
      // wywalamy tez z zapowiedzi
      $oCommonSynchro->delPreviews($aItem['id']);
    }
  }
}

?>
