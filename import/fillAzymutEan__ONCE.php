<?php
/**
 * Skrypt aktualizujący ISBN azymut - skrypt jednorazowo uruchamiany
 * 
 * @author Arkadiusz Golba - omnia.pl
 * @copyright 2012.06.23
 */

$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
set_time_limit(20800);

$handle = @fopen("bookindex_ean.csv", "r");
if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
        $aRow = explode(',', $buffer);
				// linia pojedyncza
				if ($aRow[0] != '' && $aRow[1] != '') {
					$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_azymut_buffer
									 WHERE azymut_index='".$aRow[0]."'";
					$aDbRow = Common::GetRow($sSql);
					if (!empty($aDbRow)) {
						$aValues = array(
								'isbn' => $aRow[1],
								'isbn_plain' => $aRow[1]
						);
						if (Common::Update($aConfig['tabls']['prefix'].'products_azymut_buffer', $aValues, " azymut_index='".$aRow[0]."'") === false) {
							echo 'BRAK BOOKINDEKSU '.$aRow[0];
							die;
						} else {
							echo 'Zaktualizowano ISBN '.$aRow[1].' na podstawie bookindeksu '.$aRow[0];
						}
					} else {
						echo 'BRAK BOOKINDEKSU '.$aRow[0];
						die;
					}
					//dump($sSql);
					//dump($aDbRow);
				}
    }
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    fclose($handle);
}

echo 'koniec';
?>