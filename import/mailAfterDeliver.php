<?php
/**
 * Skrypt przypomina o nieaktywnych zamówieniach użytkowników, którzy nie dokonali aktywacji po 24 godzinach od złożenia zamówienia
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-08-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
global $pDbMgr;
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['use_db_manager'] = '1';
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'http://www.profit24.com.pl';
$aConfig['common']['client_base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['client_base_url_http_no_slash'] = 'http://www.profit24.com.pl';
//
//// dolaczenie biblioteki funkcji modulu
//include_once('modules/m_oferta_produktowa/client/Common.class.php');

$pSmarty->template_dir 	= $aConfig['common']['client_base_path'].$aConfig['smarty']['template_dir'];
$pSmarty->compile_dir 	= $aConfig['common']['client_base_path'].$aConfig['smarty']['compile_dir'];
$pSmarty->config_dir 		= $aConfig['common']['client_base_path'].$aConfig['smarty']['config_dir'];
$pSmarty->cache_dir 		= $aConfig['common']['client_base_path'].$aConfig['smarty']['cache_dir'];



/**
 * Metoda ustawia konfigurację serwisu
 *
 * @global array $aConfig 
 */
function getWebsiteSettingsS($sWebsite) {
  global $aConfig, $pDbMgr;

  // pobieramy ustawienia
  $sSql = "SELECT *
          FROM ".$aConfig['tabls']['prefix']."website_settings
          LIMIT 1";
  return $pDbMgr->GetRow($sWebsite, $sSql);
}// end of getWebsiteSettingsS() method


/**
 * Metoda pobiera symbol serwisu na podstawie id serwisu
 *
 * @global type $aConfig
 * @param type $iWebsiteId
 * @return type 
 */
function getWebsiteSymbol($iWebsiteId) {
  global $aConfig;

  $sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
           WHERE id=".$iWebsiteId;
  return Common::GetOne($sSql);
}// end of getWebsiteSymbol() method


/**
 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
 *
 * @global type $aConfig
 * @param type $iOId
 * @return type 
 */
function getWebsiteId($iOId) {
  global $aConfig;

  $sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
          WHERE id=".$iOId;
  return Common::GetOne($sSql);
}// end of getWebsiteId() method

/**
 * 
 * @param int $iId
 * @return bool
 */
function deleteFromOrdersTranspoortBuffer($iId){
  
  $sSql = 'DELETE FROM orders_transport_buffer WHERE id = ' . $iId . ' LIMIT 1';;
  return Common::Query($sSql);
}


include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/Module.class.php');  
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');  
$oParent = new stdClass();

$oOrderModule = new Module($pSmarty, $oParent, TRUE);
$oOrderModule->sModule = 'm_zamowienia';

$sSql = 'SELECT OTB.id AS OTB_ID, OTB.order_id
         FROM orders_transport_buffer AS OTB
         WHERE OTB.get_deliver = "1"
         ';
$aOrders = Common::GetAll($sSql);
if(!empty($aOrders))	{
  foreach($aOrders as $iKey => $aOrder){
    $oOrderModule->sendOrderStatusMail($aOrder['order_id']);
    deleteFromOrdersTranspoortBuffer($aOrder['OTB_ID']);
  }
}
