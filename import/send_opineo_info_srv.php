<?php
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['use_db_manager'] = '1';
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(32400);
ini_set('memory_limit', '128M');

$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';
$aConfig['common']['client_base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['client_base_url_http_no_slash'] = 'https://www.profit24.pl';

/**
 * Funkcja wysyła maila - jest to zmodyfikowana wersja metody Common::sendWebsiteMail()
 * bez uzywania Smarty (ze Smarty cieknie pamiec przy duzej liczbie maili)
 * 
 * @param	string	$sTemplate	- szablon maila
 * @param string	$sSender - nadawca
 * @param string	$sSenderEmail - mail nadawcy
 * @param string	$sTargetEmail - mail odiorcy
 * @param	string	$sSubject	- temat maila
 * @param string	$sContent - treść
 * @param array	$aSymbols - symbole do podmiany ({symbol1}, {symbol2}, ....)
 * @param array	$aValues - wartosći do podmiany za symbole
 * @param	array $aAdditionalImages	- dodatkowe obrazki do dolaczenia jako zalaczniki
 * @return bool
 */
function sendOkazjeMail($sTemplate, $sSender, $sSenderEmail, $sTargetEmail, $sSubject, $sContent, $aSymbols=array(), $aValues=array(), $aAdditionalImages=array(),$iOid, $sWebsite) {
	global $aConfig, $pSmarty;
	$aHtmlImages = array();
	$aImgs = array();

	/*
	$sContent = str_replace($aSymbols, $aValues, $sContent);
	$sFooter = (!empty($aConfig['default']['website_mail_footer'])?$aConfig['default']['website_mail_footer']:$aConfig['_settings']['site']['mail_footer']);
	// wprowadzanie zmiennych do szablonu
	$sContent = str_replace('{$sContent}', $sContent, $sTemplate);
	if ($sWebsite == 'profit24') {
		$sContent = str_replace('{$sLink}', 'https://www.profit24.pl/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink2}', 'https://www.profit24.pl/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="https://www.profit24.pl/redirector.php?id='.($iOid).'&type=O">http://www.okazje.info.pl/</a>', $sContent);
	} elseif ($sWebsite == 'it') {
		$sContent = str_replace('{$sLink}', 'http://ksiegarnia.it/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink2}', 'http://ksiegarnia.it/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="http://ksiegarnia.it/redirector.php?id='.($iOid).'&type=O">http://www.okazje.info.pl/</a>', $sContent);
	} elseif ($sWebsite == 'np') {
		$sContent = str_replace('{$sLink}', 'http://nieprzeczytane.pl/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink2}', 'http://nieprzeczytane.pl/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="http://nieprzeczytane.pl/redirector.php?id='.($iOid).'&type=O">http://www.okazje.info.pl/</a>', $sContent);
	} elseif ($sWebsite == 'mestro') {
		$sContent = str_replace('{$sLink}', 'http://mestro.pl/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink2}', 'http://mestro.pl/redirector.php?id='.($iOid).'&type=O', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="http://mestro.pl/redirector.php?id='.($iOid).'&type=O">http://www.okazje.info.pl/</a>', $sContent);
	}

	if(!empty($aConfig['common']['email_logo_file'])){
		$aImgs[] = $aConfig['common']['email_logo_file'];
	}
	if(!empty($aAdditionalImages)){
		$aImgs = $aAdditionalImages;
	}
	if(!empty($aImgs)){
		foreach($aImgs as $sImg){
			$sImgF = $aConfig['common']['client_base_path'].$sImg;
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
	}
	*/
	
	$sSubject = '=?UTF-8?B?'.base64_encode($sSubject).'?=';
	dump($sSubject);
	return Common::sendTxtHtmlMail($sSender, $sSenderEmail, $sTargetEmail, $sSubject, '', $sTemplate, '', '', $aHtmlImages);
} // end of sendOkazjeMail() method

/**
 * Funkcja wysyła maila - jest to zmodyfikowana wersja metody Common::sendWebsiteMail()
 * bez uzywania Smarty (ze Smarty cieknie pamiec przy duzej liczbie maili)
 * 
 * @param	string	$sTemplate	- szablon maila
 * @param string	$sSender - nadawca
 * @param string	$sSenderEmail - mail nadawcy
 * @param string	$sTargetEmail - mail odiorcy
 * @param	string	$sSubject	- temat maila
 * @param string	$sContent - treść
 * @param array	$aSymbols - symbole do podmiany ({symbol1}, {symbol2}, ....)
 * @param array	$aValues - wartosći do podmiany za symbole
 * @param	array $aAdditionalImages	- dodatkowe obrazki do dolaczenia jako zalaczniki
 * @return bool
 */
function sendCeneoMail($sTemplate, $sSender, $sSenderEmail, $sTargetEmail, $sSubject, $sContent, $aSymbols=array(), $aValues=array(), $aAdditionalImages=array(),$iOid, $sWebsite) {
	global $aConfig, $pSmarty;
	$aHtmlImages = array();
	$aImgs = array();

	/*
	$sContent = str_replace($aSymbols, $aValues, $sContent);
	$sFooter = (!empty($aConfig['default']['website_mail_footer'])?$aConfig['default']['website_mail_footer']:$aConfig['_settings']['site']['mail_footer']);
	// wprowadzanie zmiennych do szablonu
	$sContent = str_replace('{$sContent}', $sContent, $sTemplate);
	if ($sWebsite == 'profit24') {
		$sContent = str_replace('{$sLink}', 'https://www.profit24.pl/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink2}', 'https://www.profit24.pl/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="https://www.profit24.pl/redirector.php?id='.($iOid).'&type=C">http://www.ceneo.pl</a>', $sContent);
	} elseif ($sWebsite == 'it') {
		$sContent = str_replace('{$sLink}', 'http://ksiegarnia.it/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink2}', 'http://ksiegarnia.it/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="http://ksiegarnia.it/redirector.php?id='.($iOid).'&type=C">http://www.ceneo.pl</a>', $sContent);
	} elseif ($sWebsite == 'np') {
    $sContent = str_replace('{$sLink}', 'http://nieprzeczytane.pl/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink2}', 'http://nieprzeczytane.pl/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="http://nieprzeczytane.pl/redirector.php?id='.($iOid).'&type=C">http://www.ceneo.pl</a>', $sContent);
  } elseif ($sWebsite == 'mestro') {
    $sContent = str_replace('{$sLink}', 'http://mestro.pl/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink2}', 'http://mestro.pl/redirector.php?id='.($iOid).'&type=C', $sContent);
		$sContent = str_replace('{$sLink3}', '<a href="http://mestro.pl/redirector.php?id='.($iOid).'&type=C">http://www.ceneo.pl</a>', $sContent);
  }
  

	if(!empty($aConfig['common']['email_logo_file'])){
		$aImgs[] = $aConfig['common']['email_logo_file'];
	}
	if(!empty($aAdditionalImages)){
		$aImgs = $aAdditionalImages;
	}
	if(!empty($aImgs)){
		foreach($aImgs as $sImg){
			$sImgF = $aConfig['common']['client_base_path'].$sImg;
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
	}
	*/

	$sSubject = '=?UTF-8?B?'.base64_encode($sSubject).'?=';
	return Common::sendTxtHtmlMail($sSender, $sSenderEmail, $sTargetEmail, $sSubject, '', $sTemplate, '', '', $aHtmlImages);
} // end of sendCeneoMail() method

// dolaczenie biblioteki funkcji modulu
include_once('modules/m_newsletter/internals/functions.inc.php');
include_once('modules/m_oferta_produktowa/client/Common.class.php');
include_once($aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].'modules/m_newsletter/lang/pl_news.php');
include_once($aConfig['common']['client_base_path'].$aConfig['smarty']['template_dir'].'/modules/m_newsletter/newsletters/auto_newsletter.php');

$pSmarty->template_dir 	= $aConfig['common']['client_base_path'].$aConfig['smarty']['template_dir'];
$pSmarty->compile_dir 	= $aConfig['common']['client_base_path'].$aConfig['smarty']['compile_dir'];
$pSmarty->config_dir 		= $aConfig['common']['client_base_path'].$aConfig['smarty']['config_dir'];
$pSmarty->cache_dir 		= $aConfig['common']['client_base_path'].$aConfig['smarty']['cache_dir'];

$aMailImages['it'] = array(			//obrazki dla okazje
	'images/gfx/mailing_okazje/it/logo.gif',
	'images/gfx/mailing_okazje/it/slucham-swoich-klientow.gif',
	'images/gfx/mailing_okazje/it/wystaw-opinie-na.gif',
	'images/gfx/mailing_okazje/it/facebook.gif'
);

$aMailImagesC['it'] = array(		//obrazki dla ceneo
	'images/gfx/mailing_ceneo/it/logo.gif',
	'images/gfx/mailing_ceneo/it/slucham-swoich-klientow.gif',
	'images/gfx/mailing_ceneo/it/wystaw-opinie-na.gif',
	'images/gfx/mailing_ceneo/it/facebook.gif'
);


$aMailImages['np'] = array(			//obrazki dla okazje
	'images/gfx/mailing_okazje/np/logo.gif',
	'images/gfx/mailing_okazje/np/slucham-swoich-klientow.gif',
	'images/gfx/mailing_okazje/np/wystaw-opinie-na.gif',
);

$aMailImagesC['np'] = array(		//obrazki dla ceneo
	'images/gfx/mailing_ceneo/np/logo.gif',
	'images/gfx/mailing_ceneo/np/slucham-swoich-klientow.gif',
	'images/gfx/mailing_ceneo/np/wystaw-opinie-na.gif',
);

$aMailImages['mestro'] = array(			//obrazki dla okazje
	'images/gfx/mailing_okazje/mestro/logo.gif',
	'images/gfx/mailing_okazje/mestro/slucham-swoich-klientow.gif',
	'images/gfx/mailing_okazje/mestro/wystaw-opinie-na.gif',
);

$aMailImagesC['mestro'] = array(		//obrazki dla ceneo
	'images/gfx/mailing_ceneo/mestro/logo.gif',
	'images/gfx/mailing_ceneo/mestro/slucham-swoich-klientow.gif',
	'images/gfx/mailing_ceneo/mestro/wystaw-opinie-na.gif',
);

$aMailImages['profit24'] = array(			//obrazki dla okazje
	'images/gfx/mailing_okazje/profit24/logo.gif',
	'images/gfx/mailing_okazje/profit24/slucham-swoich-klientow.gif',
	'images/gfx/mailing_okazje/profit24/wystaw-opinie-na.gif',
	'images/gfx/mailing_okazje/profit24/facebook.gif',
//  'images/gfx/mailing_okazje/profit24/Baner_Profit24.png'
);

$aMailImagesC['profit24'] = array(		//obrazki dla ceneo
	'images/gfx/mailing_ceneo/profit24/logo.gif',
	'images/gfx/mailing_ceneo/profit24/slucham-swoich-klientow.gif',
	'images/gfx/mailing_ceneo/profit24/wystaw-opinie-na.gif',
	'images/gfx/mailing_ceneo/profit24/facebook.gif',
//  'images/gfx/mailing_okazje/profit24/Baner_Profit24.png'
);

	function getDateAfterWorkingDays($iDate,$iDays,&$aFreeDays){
	global $aConfig;
		$iDaysCount=0;
		// dopóki nie osiągniemy potrzebnej ilości dni
		while($iDaysCount < $iDays){
			// dodaj dzień
			$iDate += (24 * 60 * 60);
			$aDate = getdate($iDate);
			// jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
			if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
				$iDaysCount++;
			}
		}
		return $iDate;
	}
	
	
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
	
	
	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getWebsiteId($iOId) {
		global $aConfig;
		
		$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id=".$iOId;
		return Common::GetOne($sSql);
	}// end of getWebsiteId() method
	
	
	/**
	 * Metoda ustawia konfigurację serwisu
	 *
	 * @global array $aConfig 
	 */
	function getWebsiteSettingsS($sWebsite) {
		global $aConfig, $pDbMgr;
		
		// pobieramy ustawienia
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."website_settings
						LIMIT 1";
		return $pDbMgr->GetRow($sWebsite, $sSql);
	}// end of getWebsiteSettingsS() method
	

/*
// pobieramy ustawienia
	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 LIMIT 1";
	$aSettings =& Common::GetRow($sSql);
	$aConfig['_settings']['site'] =& $aSettings; 
	*/
// pobieramy tresc maila
	
  $aMailServise['np'] = Common::getWebsiteMail('zamowienia_opineo', 'np');
//	$aMailServise['it'] = Common::getWebsiteMail('zamowienia_opineo', 'it');
  $aMailServise['mestro'] = Common::getWebsiteMail('zamowienia_opineo', 'mestro');
	$aMailServise['profit24'] = Common::getWebsiteMail('zamowienia_opineo', 'profit24');
	$aMailServise['smarkacz'] = Common::getWebsiteMail('zamowienia_opineo', 'smarkacz');
	
	// dni wolne
	$sSql = "SELECT UNIX_TIMESTAMP(free_day)
							 FROM ".$aConfig['tabls']['prefix']."orders_free_days";
  $aFreeDaysServise['np'] = $pDbMgr->GetCol('np', $sSql);
	$aFreeDaysServise['it'] = $pDbMgr->GetCol('it', $sSql);
  $aFreeDaysServise['mestro'] = $pDbMgr->GetCol('mestro', $sSql);
	$aFreeDaysServise['profit24'] = $pDbMgr->GetCol('profit24', $sSql);
	$aFreeDaysServise['smarkacz'] = $pDbMgr->GetCol('smarkacz', $sSql);

// pobieramy zamowienia z nierozeslnym okazje
	$sSql="SELECT A.*, DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_format']."') AS order_date, 
								DATE_FORMAT(status_4_update, '".$aConfig['common']['sql_date_format']."') AS status_4_update_, 
								UNIX_TIMESTAMP(A.status_4_update) AS status_4_update_stamp
					FROM ".$aConfig['tabls']['prefix']."orders AS A
					WHERE order_status = '4'
	 				AND opineo_dont_send = '0'
					AND opineo_sent IS NULL
					AND (website_id = 1 OR website_id = 3 OR website_id = 4 OR website_id = 5)
					AND IF(website_id = 5, DATE(A.order_date) >= '2016-10-31' AND DATE(A.status_4_update) <= DATE(A.shipment_date_expected), 1 = 1)
					AND id >= 424824
					"; /// XX USUNAC
	$oOrders =& Common::PlainQuery($sSql);

	if($oOrders !== false) {
		//ile maili do wyslania
		$iCount=count($oOrders);
		//polowa do ceneo
		$iCount=floor($iCount/2);
		//$iCount=0;
		// pobranie szablonu maila
    $aTemplate['np'] = file_get_contents($aConfig['common']['client_base_path'].'smarty/templates/mails/np/'.$aMailServise['np']['template']);
    $aTemplate['mestro'] = file_get_contents($aConfig['common']['client_base_path'].'smarty/templates/mails/mestro/'.$aMailServise['mestro']['template']);
		$aTemplate['it'] = file_get_contents($aConfig['common']['client_base_path'].'smarty/templates/mails/it/'.$aMailServise['it']['template']);
		$aTemplate['profit24'] = file_get_contents($aConfig['common']['client_base_path'].'smarty/templates/mails/profit24/'.$aMailServise['profit24']['template']);
		$aTemplate['smarkacz'] = file_get_contents($aConfig['common']['client_base_path'].'smarty/templates/mails/smarkacz/'.$aMailServise['smarkacz']['template']);
		
		while ($aOrder =& $oOrders->fetchRow(DB_FETCHMODE_ASSOC)) {
			$sWebsite = getWebsiteSymbol(getWebsiteId($aOrder['id']));
			$sTemplate = $aTemplate[$sWebsite];
			//dump($sWebsite);
			//dump($aTemplate);

			$aSettings =& getWebsiteSettingsS($sWebsite);
			$aConfig['_settings']['site'] =& $aSettings; 
			$aFreeDays = $aFreeDaysServise[$sWebsite];
			$aMail = $aMailServise[$sWebsite];
			if (getDateAfterWorkingDays($aOrder['status_4_update_stamp'],$aSettings['send_opineo_after'],$aFreeDays) <= time()) {
				// logujemy
				echo "[".date('Y-m-d H:i:s')."] ".$aOrder['order_number']." | ".$aOrder['email']." | ".$aOrder['status_4_update']." | ".($iCount?'O':'C')."\r\n";
				//okazje/ceneo
				$aValues=array(
					'email'=>$aOrder['email'],
					'user_id'=>$aOrder['user_id'],
					'send_date'=>date('Y-m-d'),
					'type'=>'C'
				);
				$iOid=Common::Insert($aConfig['tabls']['prefix']."orders_opinions_sent", $aValues);
				sendCeneoMail($sTemplate, $aSettings['name'], $aSettings['email'], $aOrder['email'], $aMail['subject'], $aMail['content'], array('{nr_zamowienia}','{data_zamowienia}','{data_realizacji}'), array($aOrder['order_number'],$aOrder['order_date'],$aOrder['status_4_update']), $aaMailImagesC,$iOid, $sWebsite);

				//zmiana statusu
				$iCount=!$iCount;

				$aValues = array(
					'opineo_sent' => 'NOW()'
				);
				Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$aOrder['id']);
			}
		}
	}
?>
