<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 26.09.16
 * Time: 15:21
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['common']['ommit_trigger_database'] = true;

include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

/**
 * @param $aProduct
 */
function changeBeforeInsertProductName($name) {
    return mb_ucfirst($name, 'utf8');
}

function mb_ucfirst($string, $encoding)
{
    $string = mb_strtolower($string, $encoding);
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

$sSql = 'SELECT id, name FROM products WHERE created_by = "platon" AND modiefied_by_service = "0" AND modified IS NULL ORDER BY id DESC ';
$aProducts = Common::GetAll($sSql);

foreach ($aProducts as $aProduct) {
    $aProduct['name'] = changeBeforeInsertProductName($aProduct['name']);
    $aValues = [
        'name' => $aProduct['name'],
        'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($aProduct['name'], ' ')))
    ];
    Common::Update('products', $aValues, ' id = '.$aProduct['id']);
    $aValues = [
        'title' => $aProduct['name']
    ];
    Common::Update('products_shadow', $aValues, ' id = '.$aProduct['id']);
}


echo 'stop';