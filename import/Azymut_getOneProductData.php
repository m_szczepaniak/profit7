<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);

include_once ('import_func.inc.php');

ini_set('display_errors', 'On');
error_reporting(E_ALL);

//$sAzymutbookindex = '53448801622KS';
$sAzymutbookindex = $_GET['index'];
$sDestFile = './once_'.$sAzymutbookindex.'.XML';
downloadAzymutXML('getItem', $sDestFile, '&ind='.$sAzymutbookindex);
echo file_get_contents($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile);