<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');

function getProductAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.', ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-2);
  }
  return $sAuthors;
}
	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	} // end of getTarrifPrice()
	
	function getProductCat($iId) {
	global $aConfig;
	$sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";
	return Common::GetRow($sSql);
}

function getCategoryParent($iId) {
	global $aConfig;
	
	$sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return (!empty($aItem['name'])?$aItem['name'].'/':'');
}



function getShipmentTime($iShipment){
	switch(intval($iShipment)){
		case 0:
			return '24 godziny';
			break;
		case 1:
			return '2 dni';
			break;
		case 2:
			return '9 dni';
			break;
		case 3:
			return '';
			break;
	}
}

function getProductImg($iId) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
	
		if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
			return $sDir.'/__b_'.$aImg['photo'];
		}
		else {
			if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
				return $sDir.'/'.$aImg['photo'];
			}
		}
	}
	return '';
} // end of getItemImage() function


/**
 * Metoda pobiera opisy dodawane do tytułu produktu - dostępność
 * 
 * @return type
 */
function getShipmentTimes(){

  $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
  return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
}// end of getShipmentTimes() method


$aShipmentTimes = getShipmentTimes();


	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 LIMIT 1";
	$aSite =& Common::GetRow($sSql);
	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn, A.shipment_time, A.price_brutto, A.shipment_time, A.description, B.name AS publisher_name
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 WHERE A.published = '1'
					 			AND (A.prod_status = '1' OR A.prod_status = '3')
					 			AND (A.shipment_time = '0' OR A.shipment_time = '1')
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
		
	if (!($fp = fopen($_SERVER['DOCUMENT_ROOT'].'/tanio.xml', 'w'))) {
   echo "Nie mogę otworzyc pliku tanio.xml do zapisu";
   die();
	} else {
	
	$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<wpoffers xmlns="http://zakupy.wp.pl/WPzakupySchema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://zakupy.wp.pl/ http://zakupy.wp.pl/WPzakupySchema.xsd">

';
	fwrite($fp,$sXml);

	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		$aCat = getProductCat($aItem['id']);
		if(!empty($aCat['name'])){
			$sCatFullPath = getCategoryParent($aCat['id']).str_replace('/ ','',$aCat['name']);
			$fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
			$sAuthors = getProductAuthors($aItem['id']);
			$sImg = getProductImg($aItem['id']);

	
			fwrite($fp, "	<offer>\n");
			fwrite($fp, "		<src_id>".$aItem['id']."</src_id>\n");
			$sName = $aItem['plain_name'];
			if(!empty($sAuthors)){
				$sName .= ' - '.$sAuthors;
			}
			fwrite($fp, "		<name><![CDATA[".html_entity_decode(trimString($sName,100),ENT_QUOTES,'UTF-8').$aShipmentTimes[$aItem['shipment_time']]."]]></name>\n");
			fwrite($fp, "		<description><![CDATA[".html_entity_decode(strip_tags(clearAsciiCrap($aItem['description'])),ENT_QUOTES,'UTF-8')."]]></description>\n");
			fwrite($fp, "		<url><![CDATA[".html_entity_decode($aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name']),ENT_QUOTES,'UTF-8')."]]></url>\n");
			fwrite($fp, "		<category><![CDATA[".html_entity_decode($sCatFullPath,ENT_QUOTES,'UTF-8')."]]></category>\n");
			fwrite($fp, "		<price>".$fTarrifPrice."</price>\n");
			if(!empty($sImg)){
				fwrite($fp, "		<pic_url><![CDATA[".html_entity_decode($aConfig['common']['base_url_http'].$sImg,ENT_QUOTES,'UTF-8')."]]></pic_url>\n");
			}
			fwrite($fp, "		<ean>".$aItem['isbn']."</ean>\n");
			if(!empty($aItem['publisher_name'])){
				fwrite($fp, "		<producer><![CDATA[".html_entity_decode($aItem['publisher_name'],ENT_QUOTES,'UTF-8')."]]></producer>\n");
			}
			
			fwrite($fp, "		<delivery>\n");
      fwrite($fp, "			<cost>0</cost>\n");
      fwrite($fp, "			<time><![CDATA[".getShipmentTime($aItem['shipment_time'])."]]></time>\n");
    	fwrite($fp, "		</delivery>\n");
		
			fwrite($fp, "	</offer>\n");
			fflush($fp);
		}
	}
	
	$sXml = '</wpoffers>';
	fwrite($fp,$sXml);
	fclose($fp);
	}
	
/*
<?xml version="1.0" encoding="UTF-8"?>
<wpoffers xmlns="http://zakupy.wp.pl/WPzakupySchema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://zakupy.wp.pl/ http://zakupy.wp.pl/WPzakupySchema.xsd">

  <offer>
    <src_id>Identyfikator produktu 1 max. 200 znaków</src_id>
    <name><![CDATA[Nazwa produktu 1 max. 100 znaków]]></name>
    <description><![CDATA[Opis produktu, bez ograniczeń ]]></description>
    <url><![CDATA[http://www.nazwasklepu.pl/¶cieżkadoproduktu.bez_ograniczenia_dlugo¶ci]]></url>
    <category><![CDATA[Kategoria/podkategoria/bez_ograniczeń_długo¶ci]]></category>
    <price>0.02</price>
    <pic_url><![CDATA[http://www.nazwasklepu.pl/¶cieżkadoproduktu.bez_ograniczenia_dlugo¶ci]]></pic_url>
    <bdk>1</bdk>
    <ean>Kod EAN, max. 100 znaków</ean>
    <color>Kolor max. 30 znaków</color>
    <producer><![CDATA[Nazwa producenta, brak ograniczeń w długo¶ci]]></producer>
    <delivery>
      <cost>18.30</cost>
      <time><![CDATA[Czas dostarcz. max. 30znaków]]></time>
    </delivery>
    <attributes>
	<attribute>
	   <name><![CDATA[Nazwa Atrybutu 1, bez ograniczenia długo¶ci ]]></name>
	   <value><![CDATA[Warto¶ć atrybutu 1, bez ograniczenia długo¶ci]]></value>
	</attribute>
	<attribute>
	   <name><![CDATA[Nazwa Atrybutu 2, bez ograniczenia długo¶ci ]]></name>
	   <value><![CDATA[Warto¶ć atrybutu 2, bez ograniczenia długo¶ci]]></value>
	</attribute>
    </attributes>
  </offer> 

  <offer>
    <src_id>Identyfikator produktu 2 max. 200 znaków</src_id>
    <name><![CDATA[Nazwa produktu 2 max. 100 znaków]]></name>
    <description><![CDATA[Opis produktu, bez ograniczeń ]]></description>
    <url><![CDATA[http://www.nazwasklepu.pl/¶cieżkadoproduktu.bez_ograniczenia_dlugo¶ci]]></url>
    <category><![CDATA[Kategoria/podkategoria/bez_ograniczeń_długo¶ci]]></category>
    <price>0.02</price>
    <pic_url><![CDATA[http://www.nazwasklepu.pl/¶cieżkadoproduktu.bez_ograniczenia_dlugo¶ci]]></pic_url>
    <bdk>1</bdk>
    <ean>Kod EAN, max. 100 znaków</ean>
    <color>Kolor max. 30 znaków</color>
    <producer><![CDATA[Nazwa producenta, brak ograniczeń w długo¶ci]]></producer>
    <delivery>
      <cost>18.30</cost>
      <time><![CDATA[Czas dostarcz. max. 30znaków]]></time>
    </delivery>
    <attributes>
	<attribute>
	   <name><![CDATA[Nazwa Atrybutu 1, bez ograniczenia długo¶ci ]]></name>
	   <value><![CDATA[Warto¶ć atrybutu 1, bez ograniczenia długo¶ci]]></value>
	</attribute>
	<attribute>
	   <name><![CDATA[Nazwa Atrybutu 2, bez ograniczenia długo¶ci ]]></name>
	   <value><![CDATA[Warto¶ć atrybutu 2, bez ograniczenia długo¶ci]]></value>
	</attribute>
    </attributes>
  </offer>

</wpoffers>

*/
?>