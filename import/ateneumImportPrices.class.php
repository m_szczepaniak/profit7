<?php
/**
 * Skrypt importu produktów ze źródła Ateneum do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importCSVController.class.php');	

$bTestMode = FALSE; //  czy test mode
$iLimitIteration = 10000000000;
$sSource = 'ateneum';

$aColsToFgetcsv = array(
				'source_id',// Ident_ate 1,
				'stock', // Stan_magazynowy 0,
        'price_netto', // Cena_detaliczna_netto 30.38,
        'price_brutto', // Cena_detaliczna_brutto 31.90,
        'vat', // Stawka_VAT 5
);

$sElementName = 'prices'; // najpiew autorzy
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/ateneum/stanyceny.csv';
}
	 
if ($sFilename !== false && $sFilename != '') {

	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ',', TRUE, $aColsToFgetcsv);
	
  if (!empty($oImportCSVController->aLog) || !empty($oImportCSVController->oSourceElements->aLog)) {
    $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
                                                          print_r($oImportCSVController->oSourceElements->aLog, true));
  }
  if (!empty($oImportCSVController->aErrorLog) || !empty($oImportCSVController->oSourceElements->aErrorLog)) {
    $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
                                                          print_r($oImportCSVController->oSourceElements->aErrorLog, true));
  }
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
dump($oImportCSVController->oSourceElements->_countEmptyPublicationYear);
	 
unset($oImportCSVController);