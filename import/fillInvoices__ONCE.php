<?php
/**
 * Skrypt przenoszący informacje o fakturach zamówienia z folderu invoices oraz 
 * proforma do bazy danych
 * 
 */
$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
set_time_limit(20800);

function getOrderIdByNumber($sONr) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders WHERE order_number='".$sONr."'";
	return Common::GetOne($sSql);
}

function getOrderIdByInvoiceId($sONr) {
	global $aConfig;
	
	$sSql = "SELECT id, invoice_date FROM ".$aConfig['tabls']['prefix']."orders WHERE invoice_id='".$sONr."'";
	$aData = Common::GetRow($sSql);
	if (!empty($aData)) {
		return $aData;
	} else {
		$sSql = "SELECT id, invoice_date FROM ".$aConfig['tabls']['prefix']."orders WHERE invoice2_id='".$sONr."'";
		$aData = Common::GetRow($sSql);
		$aData['invoice2'] = '1';
		return $aData;
	}
	return false;
}

function fileExists($iOId, $bInvoice) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."invoices 
					 WHERE order_id=".$iOId." AND proforma='".($bInvoice==true?'0':'1')."' ";
	return (Common::GetOne($sSql) > 0? true : false);
}

/*
//$sPath = '../proforma';
$sPath =  '../'.$aConfig['common']['pro_forma_invoice_dir'];
if ($handle = opendir($sPath)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
						$file_source = file_get_contents($sPath.'/'.$entry);
						$sONr = str_ireplace('faktura_proforma', '', $entry);
						$sONr = str_replace('_', '/', $sONr);
						$sONr = str_ireplace('.pdf', '', $sONr);
						$iOId = getOrderIdByNumber($sONr);
						//$file_source = addslashes($file_source);
						if (intval($iOId) > 0 ) {
							$aValues = array(
									'order_id' => $iOId,
									'invoice_name' => $entry,
									'data' => 'NULL',
									'invoice2' => '0',
									'proforma' => '1',
									'content' => $file_source
							);
							if (Common::Insert($aConfig['tabls']['prefix']."invoices", $aValues) === false) {
								echo 'błąd podczas importu faktury '.$entry.'<br />';
								die;
							}
						} else {
							echo 'brak zamowienia o nr '.$entry.'<br />';
							//die;
						}
        }
    }
    closedir($handle);
}
*/
/*
DELETE FROM invoices WHERE id IN (
SELECT id FROM (
SELECT id FROM invoices
GROUP BY invoice_name
HAVING count(id) >1
				) AS TMP
				);
*/			
//$sPath = '../omniaCMS/invoices';
$sPath = '../../invoices';
echo 'start';
if ($handle = opendir($sPath)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != ".." && $entry != "faktura.pdf" ) {
						$sONr = str_ireplace('faktura', '', $entry);
						$sONr = str_replace('_', '/', $sONr);
						$sONr = str_ireplace('.pdf', '', $sONr);
						$aData = getOrderIdByInvoiceId($sONr);
						if (($aData['id']) > 0 && fileExists($aData['id'], true) == false) {
							$file_source = file_get_contents($sPath.'/'.$entry);
							$file_source = addslashes($file_source);
							if (intval($aData['id']) > 0 ) {
								$aValues = array(
										'order_id' => $aData['id'],
										'invoice_name' => $entry,
										'data' => $aData['invoice_date'],
										'proforma' => '0',
										'invoice2' => ($aData['invoice2'] == '1' ? '1' : '0'),
										'content' => $file_source
								);
							
								if (Common::Insert($aConfig['tabls']['prefix']."invoices", $aValues) === false) {
									echo 'błąd podczas importu faktury '.$entry.'<br />';
									//die;
								}
							}  else {
								echo 'brak zamowienia o nr '.$entry.'<br />';
								//die;
							}
						}
        }
				die;
    }
    closedir($handle);
}
echo 'koniec';
?>
