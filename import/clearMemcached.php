<?php
/**
 * Skrypt przeczyszczający memcached - kliencki codziennie, po nocnych importach
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-06-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/Memcache_adapter.class.php');
ini_set("memory_limit", '512M');

$oMem = new Memcache_adapter_Cache_Lite(null, true);
$oMem->clean();
?>