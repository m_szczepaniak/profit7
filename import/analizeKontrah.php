<?php
/**
 * Skrypt importu produktów ze źródła Ateneum do Profit24.pl
 *
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');

$newArray = [];

$row = 1;
if (($handle = fopen("kontrah2.txt", "r")) !== FALSE) {
    while (($row = fgetcsv($handle, 1000, "\t")) !== FALSE) {
        $newArray[$row[1]] =
            [
                'company' => trim($row[25]),
                'nip' => trim($row[26]),
                'city' => trim($row[27]),
                'postal' => trim($row[28]),
                'street' => trim($row[30]),
                'number' => trim($row[31]),
            ];
//        $row[2] // streamsoft_id
//        $row[25] // company
//        $row[26]// nip
//        $row[27]// city
//        $row[29]// postal
//        $row[30]// street
//        $row[31]// number
    }
    fclose($handle);
}
global $pDbMgr;
$sSql = 'SELECT * 
         FROM users_accounts_address
         WHERE streamsoft_id <> "" AND streamsoft_id IS NOT NULL ';
$usersAccountsAddress = $pDbMgr->GetAll('profit24', $sSql);
foreach ($usersAccountsAddress as $address) {
    if (isset($newArray[$address['streamsoft_id']])) {
        $streamData = $newArray[$address['streamsoft_id']];
        if (trim(str_replace('-', '', $address['nip'])) <> str_replace('-', '', $streamData['nip'])) {
            echo 'NIEZGODNY NIP ID '.$address['streamsoft_id'].' OMNIA: '.print_r($address, true).' STREAM: '.print_r($streamData, true) .' '."\n";
            // poprawimy

            $sSql = 'UPDATE users_accounts_address 
                     SET 
                        streamsoft_id = NULL, 
                        address_checksum = NULL 
                     WHERE id = '.$address['id'].'
                     LIMIT 1';
            dump($sSql);
//            $pDbMgr->Query('profit24', $sSql);


            continue;
        }
//        else if (trim($address['company']) <> $streamData['company']) {
//            echo 'NIEZGODNA NAZWA FIRMY ID '.$address['streamsoft_id'].' OMNIA: '.print_r($address, true).' STREAM: '.print_r($streamData, true) .' '."\n";
//            continue;
//        }
//        else if (trim($address['city']) <> $streamData['city']) {
//            echo 'NIEZGODNY CITY ID '.$address['streamsoft_id'].' OMNIA: '.print_r($address, true).' STREAM: '.print_r($streamData, true) .' '."\n";
//            continue;
//        }
//        else if (trim($address['postal']) <> $streamData['postal']) {
//            echo 'NIEZGODNY postal ID '.$address['streamsoft_id'].' OMNIA: '.print_r($address, true).' STREAM: '.print_r($streamData, true) .' '."\n";
//            continue;
//        }
//        else if (trim($address['street']) <> $streamData['street']) {
//            echo 'NIEZGODNY street ID '.$address['streamsoft_id'].' OMNIA: '.print_r($address, true).' STREAM: '.print_r($streamData, true) .' '."\n";
//            continue;
//        }
//        else if (trim($address['number']) <> $streamData['number']) {
//            echo 'NIEZGODNY number ID '.$address['streamsoft_id'].' OMNIA: '.print_r($address, true).' STREAM: '.print_r($streamData, true) .' '."\n";
//            continue;
//        }
        else {
            echo 'WSZYSTKO ZGODNE : '.$address['streamsoft_id'].' '."\n";
        }
    } else {
        echo 'BRAK STRAMSOFT ID '.print_r($address, true)."\n";
    }
}
//