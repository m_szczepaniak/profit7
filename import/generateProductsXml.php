<?php

use PriceUpdater\Buffor\ProductBuffor;
use PriceUpdater\Tarrifs\TarrifsUpdater;
use PriceUpdater\Buffor\CyclicBuffor;

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
ini_set('memory_limit', '2048M');

//
//$sSql = "SELECT A.id, A.plain_name, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.pages, A.price_brutto, A.description,
//									A.shipment_time, B.name AS publisher_name,
//									C.binding AS binding_name, D.dimension AS dimension_name,
//									A.packet, A.weight, A.description_on_create


global $pDbMgr;

$sSql = "SELECT      A.id, A.name, A.short_description, A.description, A.name2 as subtitle, A.description_on_create,
                     A.ean_13, A.isbn, A.isbn_10, A.isbn_13, C.binding, PL.language, PLA.language as original_language, D.dimension as book_format,
                     B.name as product_publisher, A.publication_year, A.producer_code, A.pkwiu, A.weight, A.pages,
                     A.volumes, A.volume_nr, A.edition, A.volume_name, A.city, A.legal_status_date, A.translator, A.is_news,
                     A.is_previews, A.is_bestseller, A.product_type,
                     PGA.sex, PGA.age, PGA.game_time, PGA.number_of_players, PGA.content_of_box, PGA.count_elements,
                     PGA.lego_code, A.modiefied_by_service, PA.attachment_type


					 FROM products A
					 LEFT JOIN products_attachments PA ON PA.product_id=A.id
					 LEFT JOIN products_languages PL ON PL.id=A.language
					 LEFT JOIN products_languages PLA ON PLA.id=A.original_language
					 LEFT JOIN products_publishers B ON B.id=A.publisher_id
					 LEFT JOIN products_bindings C ON C.id=A.binding
					 LEFT JOIN products_dimensions D ON C.id=A.dimension
					 LEFT JOIN products_game_attributes PGA ON A.id=PGA.product_id
					 GROUP BY A.id
                     ;
                     ";

/** @var DatabaseManager $data */
$data = $pDbMgr->PlainQuery("profit24", $sSql);

$tagsSql = "
    SELECT PT.id from products_to_tags AS PTT
    JOIN products_tags PT ON PTT.tag_id = PT.id
    WHERE PTT.product_id = %s
";

$seriesSql = "
    SELECT PS.name from products_to_series AS PTS
    JOIN products_series PS ON PTS.series_id = PS.id
    WHERE PTS.product_id = %s
";

$authorsSql = "
    SELECT PA.name, PA.surname from products_to_authors AS PTA
    JOIN products_authors PA ON PTA.author_id = PA.id
    WHERE PTA.product_id = %s
";

$categoriesSql = "
    SELECT PEC.page_id from products_extra_categories PEC
    WHERE PEC.product_id = %s
";

$sXMLFilename = $_SERVER['DOCUMENT_ROOT'].'/products_export.json';
file_put_contents($sXMLFilename, "[");

$count = $data->numRows();

$i = 1;
while ($value =& $data->fetchRow(DB_FETCHMODE_ASSOC)) {

    $tags = $pDbMgr->GetCol("profit24", sprintf($tagsSql, $value['id']));
    $series = $pDbMgr->GetCol("profit24", sprintf($seriesSql, $value['id']));
    $authors = $pDbMgr->GetAll("profit24", sprintf($authorsSql, $value['id']));
    $categories = $pDbMgr->GetCol("profit24", sprintf($categoriesSql, $value['id']));

    $finalAuthors = [];

    $producerCode = $value['producer_code'];
    $legoCode = $value['lego_code'];

    if (!empty($legoCode)) {

        $producerCode = $legoCode;
    }

    unset($value['lego_code']);

    foreach($authors as $author) {

        $finalAuthors[] = trim($author['name'].' '.$author['surname']);
    }

    $ean = $value['ean_13'];
    $isbnClean = $value['isbn'];
    $isbn10 = $value['isbn_10'];
    $isbn13 = $value['isbn_13'];

    if (empty($ean)) {

        if (!empty($isbnClean)) {

            $ean = $isbnClean;
        }
        else if (!empty($isbn13)) {

            $ean = $isbn13;
        } else {

            $ean = $isbn10;
        }
    }

    $child = null;

    // attachments
    if ($value['attachment_type'] != null) {

        $atchId = $value['id'].'_atch';

        $attachment = [
            'id' => $atchId,
            'child' => null,
            'is_attachment' => 1,
            'attachment_type' => $value['attachment_type'].'_atch'
        ];

        $child = $atchId;

        file_put_contents($sXMLFilename, json_encode($attachment) .','.PHP_EOL, FILE_APPEND);
    }

    $value['is_attachment'] = 0;
    $value['child'] = $child;
    $value['ean_13'] = $ean;
    $value['producer_code'] = $producerCode;
    $value['tags'] = $tags;
    $value['series'] = $series;
    $value['authors'] = $finalAuthors;
    $value['categories'] = $categories;

    $extra = $count == $i ? '' : ','.PHP_EOL;

    $created = new DateTime($value['created']);
    $modified = new DateTime($value['modified']);
    $created_year = $created->format('Y');
    $modified_year = $modified->format('Y');

    if ( $value['packet'] == 0 && ( $value['published'] == 1 || ( $value['published'] == 0 && ( $created_year > 2015 || $modified_year > 2015 ) ) ) )
    {
        echo "Produkt dodany - ".$value['id'];
        file_put_contents($sXMLFilename, json_encode($value) . $extra, FILE_APPEND);
    }
    else
    {
        echo "Produkt nie spełnia kryteriów ".$value['id'];
    }

    $i++;
}

file_put_contents($sXMLFilename, ']', FILE_APPEND);

