<?php
use \LIB\EntityManager\Entites\OrdersMailsSended;


//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL );
$aConfig['use_db_manager'] = '1';
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'] . '/lang/pl.php');

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(32400);
ini_set('memory_limit', '128M');

global $pDbMgr;

if ($aConfig['common']['status'] == 'development') {
    echo 'nie wykonujemy lokalnie !';
    die;
}

/**
 * Metoda ustawia konfigurację serwisu
 *
 * @global array $aConfig
 */
function getWebsiteSettingsS($sWebsite)
{
    global $aConfig, $pDbMgr;

    // pobieramy ustawienia
    $sSql = "SELECT *
          FROM " . $aConfig['tabls']['prefix'] . "website_settings
          LIMIT 1";
    return $pDbMgr->GetRow($sWebsite, $sSql);
}// end of getWebsiteSettingsS() method


$sWebsite = 'np';
$sMailSymbol = 'add_review';
$aMail = Common::getWebsiteMail($sMailSymbol, $sWebsite);

$aSettings = getWebsiteSettingsS($sWebsite);
$ordersMailsSended = new OrdersMailsSended($pDbMgr);

if ($aMail['content'] != '') {

    // TODO ZMIENIĆ ILOŚĆ DNI NA 30 i USUNĄĆ ID i zmienić limit na 200
    $sSql = 'SELECT O.id
             FROM orders AS O
             JOIN users_accounts AS UA
                ON O.user_id = UA.id 
                  AND UA.promotions_agreement = "1"
                  AND UA.deleted = "0"
                  AND UA.active = "1"
             LEFT JOIN orders_mails_sended AS OMS
                ON O.id = OMS.order_id
                  AND OMS.mail_type = "'.OrdersMailsSended::TYPE_PLEASE_REVIEW.'"
             WHERE O.order_status = "4" AND 
                   DATE(O.status_4_update) > "2018-01-01" AND 
                   DATE(O.status_4_update) < DATE_SUB(NOW(), INTERVAL 30 DAY) 
                   AND O.website_id = "3"
                   AND OMS.id IS NULL
             ORDER BY O.id ASC
             LIMIT 1000
                   ';
    $ordersIds = $pDbMgr->GetCol('profit24', $sSql);

    foreach ($ordersIds as $orderId) {
        if (intval($ordersMailsSended->checkExists($orderId, OrdersMailsSended::TYPE_PLEASE_REVIEW)) <= 0) {
            $orderData = $pDbMgr->getTableRow('orders', $orderId, ['user_id', 'order_number', 'email', 'check_status_key']);

            $sMailTo = $orderData['email'];
            $key = md5($orderData['order_number'] . '_' . $orderData['check_status_key']);
            $sAddReviewLink = $aConfig['common'][$sWebsite]['client_base_url_http_no_slash'] . '/recenzje?order_number=' . $orderData['order_number'] . '&key=' . $key;
            $sDeleteCodeSubscribe = $aConfig['common'][$sWebsite]['client_base_url_http_no_slash'] . '/newsletter/simple_unsubscribe,'.md5($orderData['user_id'].':'.$orderData['email']).'.html';


            $aCols = array('{link_recenzje}', '{link_zrezygnuj}');
            $aVals = array($sAddReviewLink, $sDeleteCodeSubscribe);
            $sContent = str_replace($aCols, $aVals, $aMail['content']);

            // TODO odkomentować !!!
            $ordersMailsSended->add($orderId, OrdersMailsSended::TYPE_PLEASE_REVIEW);
            Common::sendWebsiteMail($aMail, $aSettings['name'], $aSettings['email'], $sMailTo, $sContent, $aCols,
                $aVals, array(), array(), $sWebsite);
        }
    }
}