<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);


include_once ('import_func.inc.php');

// zmienne

	sendInfoMail('Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto pobieranie i aktualizację powiązań indeksów azymut");
	
if ($aConfig['common']['status']	!= 'development') {
  if(!downloadAzymutXML("getInd2Cat","azymut_ind2cat.xml")){
    sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml powiazan kategorii z Azymut");
    die("błąd pobierania xml powiązań kategorii");
  }
}

if (file_exists($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/azymut_ind2cat.xml")) {
	$oXml = new XMLReader();
	$oXml->open($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/azymut_ind2cat.xml");
	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'ind2cat':
						// KONIEC LISTINGU POWIĄZAŃ KATEGORII I INDEKSÓW
						$oXml->close();
					break;
				}
			break;
		
		case XMLReader::ELEMENT :
			switch ($oXml->name) {
				case 'cat':
					$iId=$oXml->getAttribute('id');
					$sInd=$oXml->getAttribute('ind');
					if(!existAzymutInd2Cat($iId,$sInd)){
						$aValues = array (
								'azymut_category' => $iId,
								'azymut_index' => $sInd
							);
				
							//dump($aValues);
							if (Common::Insert($aConfig['tabls']['prefix']."products_azymut_ind2cat", $aValues,'',false) === false) {
								// blad
								echo "<strong>Blad!</strong> - dodawanie indeksu do kategorii <strong>".$sInd.' - '.$iId."</strong><br />";
								sendInfoMail('Błąd importu Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się dodać indeksu do kategorii : ".$sInd.' - '.$iId);
							}
							
						//$sLogInfo.="Dodano kategorię ".$sName."\n";
					}
				break;
			}
		}
	}
	sendInfoMail('Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." zakończono pobieranie i aktualizację powiązań indeksów azymut");
	
	// usuniecie pliku
	@unlink($aConfig['common']['base_path'].$aConfig['common']['import_dir'].'XML/azymut_ind2cat.xml');
}
?>