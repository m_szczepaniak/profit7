<?php
/*
// nadpisanie pol opisowych

UPDATE 
products, products_shadow
SET
products_shadow.title = products.name,
products_shadow.name2 = products.name2,
products_shadow.edition = products.edition,
products_shadow.publication_year = products.publication_year
WHERE 
products_shadow.id=products.id


UPDATE 
products, products_publishers, products_shadow
SET
products_shadow.publisher_id = products.publisher_id,
products_shadow.publisher = products_publishers.name
WHERE 
products_shadow.id=products.id AND
products_publishers.id=products.publisher_id AND
(
products_shadow.publisher_id <> products.publisher_id OR
products_shadow.publisher <> products_publishers.name
);
UPDATE 
products, products_shadow
SET
products_shadow.prod_status = products.prod_status
WHERE 
products_shadow.id=products.id
 * 

UPDATE 
products, products_shadow
SET
products_shadow.is_news = products.is_news
WHERE 
products_shadow.id=products.id
 
*/

use LIB\EntityManager\Entites\Product;

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

echo 'start';
include_once ('import_func.inc.php');


$productsStats = getCountProductsInOrders();
// aktualizujemy Mapowania produktu
$sSql = 'UPDATE products AS P
         JOIN products_types_mapping AS PTM
          ON P.main_category_id = PTM.menus_items_id
         SET P.product_type = PTM.product_type
         WHERE P.modiefied_by_service = "0" AND P.product_type <> PTM.product_type
          ';
Common::Query($sSql);

// niedostępne przeterminowane zapowiedzi
$sSql = "UPDATE `products`
SET prod_status = '0'
WHERE  `shipment_date` < CURDATE( )
AND prod_status =  '3'
AND  `shipment_date` <>  ''
AND  `shipment_date` IS NOT NULL";
Common::Query($sSql);

$sSql = "UPDATE `products_shadow`
SET prod_status = '0'
WHERE  `shipment_date` < CURDATE( )
AND prod_status =  '3'
AND  `shipment_date` <>  ''
AND  `shipment_date` IS NOT NULL";
Common::Query($sSql);

// niedostępne zapowiedzi > 7 dni
$sSql = "UPDATE products AS P
SET
  P.shipment_time = '3'
WHERE
  P.shipment_date > DATE_ADD(CURDATE(), INTERVAL ".Product::PREVIEW_DAYS_AVAILABLE." DAY)
  AND P.prod_status =  '3'
  AND P.shipment_time <> '3'
  AND  P.shipment_date <>  ''
  AND  P.shipment_date IS NOT NULL";
Common::Query($sSql);

$sSql = "UPDATE products_shadow AS PS
SET
  PS.shipment_time = '3'
WHERE
  PS.shipment_date > DATE_ADD(CURDATE(), INTERVAL ".Product::PREVIEW_DAYS_AVAILABLE." DAY)
  AND PS.prod_status =  '3'
  AND PS.shipment_time <> '3'
  AND  PS.shipment_date <>  ''
  AND  PS.shipment_date IS NOT NULL";
Common::Query($sSql);

// dostępne zapowiedzi <= 7 dni
$sSql = "UPDATE products AS P
SET
  P.shipment_time = '0'
WHERE
  P.shipment_date <= DATE_ADD(CURDATE(), INTERVAL ".Product::PREVIEW_DAYS_AVAILABLE." DAY)
  AND ommit_auto_preview = '0'
  AND P.prod_status = '3'
  AND P.shipment_time = '3'
  AND  P.shipment_date <>  ''
  AND  P.shipment_date IS NOT NULL";
Common::Query($sSql);

$sSql = "UPDATE products_shadow AS PS
SET
  PS.shipment_time = '0'
WHERE
  PS.shipment_date <= DATE_ADD(CURDATE(), INTERVAL ".Product::PREVIEW_DAYS_AVAILABLE." DAY)
  AND (SELECT ommit_auto_preview FROM products AS P WHERE P.id = PS.id) = '0'
  AND PS.prod_status = '3'
  AND PS.shipment_time = '3'
  AND  PS.shipment_date <>  ''
  AND  PS.shipment_date IS NOT NULL";
Common::Query($sSql);

/**
 * Metoda pobiera limity piewotnego źródła pochodzenia
 * 
 * @global array $aConfig
 * @return array
 */
function getAllFirstSourceDictountLimit() {
  
  $sSql = "SELECT first_source, IFNULL( discount_limit, 0 ) AS first_source_discount_limit 
           FROM products_source_discount ";
  $aFSource = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
  $aNewFSource = array();
  foreach ($aFSource as $sSource => $iLimit) {
    $aNewFSource[strtolower($sSource)] = $iLimit;
  }
  return $aNewFSource;
}// end of getAllFirstSourceDictountLimit() method

/**
 * Metoda pobiera autorów produktu
 * 
 * @global array $aConfig
 * @param integer $iId
 * @return array
 */
function getProductAuthors($iId) {
	global $aConfig;
  
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.' ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-1);
  }
  return $sAuthors;
}// end of getProductAuthors() method


/**
 * Metoda pobiera tagi rzeczowe produktu o podanym Id
 * 
 * @param	integer	$iId	- Id produktu
 * @return	string
 */
function &getProductTags($iId) {
	global $aConfig;
	
	// pobranie indeksow rzeczowych produktu
	$sSql = "SELECT B.tag
					 FROM ".$aConfig['tabls']['prefix']."products_to_tags A
					 JOIN ".$aConfig['tabls']['prefix']."products_tags B
					 ON B.id=A.tag_id
					 WHERE A.product_id = ".$iId;
	return @implode(', ',Common::GetCol($sSql));
} // end of getProductTags() method


/**
 * Metoda pobiera cennik dla przekazanego produktu
 * 
 * @global array $aConfig
 * @param type $iId
 * @return float
 */
function getTarrifPrice($iId) {
  global $aConfig;
  
  $sSql = "SELECT price_brutto
          FROM ".$aConfig['tabls']['prefix']."products_tarrifs
          WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
          ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
  return Common::GetOne($sSql);
} // end of getTarrifPrice()


/**
 * Metoda pobiera kategorie pierwszego poziomu dla przekazanego produktu
 * 
 * @global array $aConfig
 * @param type $iId
 * @return type
 */
function getProductTopCats($iId) {
  global $aConfig;

  $sSql = "SELECT A.id
            FROM menus_items A
            JOIN products_extra_categories B
            ON A.id=B.page_id
           WHERE B.product_id = ".$iId.
            " AND A.parent_id IS NULL";
  return Common::GetCol($sSql);
}// end of getProductTopCats() method


/**
 * Metoda pobiera ilość rekordów do zwrócenia
 *
 * @global array $aConfig
 * @return integer
 */
function getCountResults () {
  global $aConfig;

  $sSql = "SELECT COUNT( DISTINCT A.id)
           FROM ".$aConfig['tabls']['prefix']."products A
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
            ON B.id=A.publisher_id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series C
            ON A.id = C.product_id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_series D
            ON C.series_id = D.id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings E
           ON E.id=A.binding
           WHERE A.published = '1' AND A.product_type IN (\"".implode('", "', $aConfig['common']['allowed_product_type'])."\") AND A.page_id <> ".$aConfig['import']['unsorted_category'];
  return Common::GetOne($sSql);
}// end of getCountResults() function 

/**
 * Funkcja sprawdza czy aktualizować produkt w shadow
 *
 * @param type $aItem
 * @param type $aShadow
 * @param type $fTarrifPrice
 * @param type $sOrdBy
 * @param string $orderByStatusOldStyle
 * @return bool
 */
function checkUpdateShadow($aItem, $aShadow, $fTarrifPrice, $sOrdBy, $orderByStatusOldStyle) {
/*
   * Stare warunki
   * 
  $aItem['prod_status'] != $aShadow['prod_status'] ||
  $aItem['shipment_time'] != $aShadow['shipment_time'] ||
  $fTarrifPrice != $aShadow['price'] ||
  $aItem['price_brutto'] != $aShadow['price_brutto'] ||
  $aItem['shipment_date'] != $aShadow['shipment_date'] || 
  $aShadow['order_by'] != $sOrdBy || 
  intval($aShadow['publication_year']) != intval($aItem['publication_year']) || 
  $aShadow['discount_limit'] != $aItem['discount_limit'] || 
  $aShadow['is_news'] != $aItem['is_news'] ||
  $aShadow['is_bestseller'] != $aItem['is_bestseller'] || 
  $aShadow['is_previews'] != $aItem['is_previews']
  */
  
  if (intval($aItem['prod_status']) != intval($aShadow['prod_status'])) {
//    echo 'inny prod_status';
    return true;
  }
  if (intval($aItem['shipment_time']) != intval($aShadow['shipment_time'])) {
//    echo 'inny shipment_time';
    return true;
  }
  if (floatval($fTarrifPrice) != floatval($aShadow['price'])) {
//    echo 'inny shipment_time';
    return true;
  }
  if (floatval($aItem['price_brutto']) != floatval($aShadow['price_brutto'])) {
//    echo 'inny price_brutto';
    return true;
  }
  if (intval(str_replace('-', '', $aItem['shipment_date'])) != intval(str_replace('-', '', $aShadow['shipment_date']))) {
//    echo 'inny shipment_date';
    return true;
  }
  if ($sOrdBy !== $aShadow['order_by']) {
//    echo 'inny order_by';
    return true;
  }
  if ($orderByStatusOldStyle !== $aShadow['order_by_status_old_style']) {
//      echo 'inny order_by_status_old_style';
      return true;
  }
  if (intval($aItem['publication_year']) != intval($aShadow['publication_year'])) {
//    echo 'inny publication_year';
    return true;
  }
  if (floatval($aItem['discount_limit']) != floatval($aShadow['discount_limit'])) {
//    echo 'inny discount_limit';
    return true;
  }
  if (intval($aItem['is_news']) != intval($aShadow['is_news'])) {
//    echo 'inny is_news';
    return true;
  }
  if (intval($aItem['is_bestseller']) != intval($aShadow['is_bestseller'])) {
//    echo 'inny is_news';
    return true;
  }
  if (intval($aItem['is_previews']) != intval($aShadow['is_previews'])) {
//    echo 'inny is_previews';
    return true;
  }
  if ($aItem['type'] !== $aShadow['type']) {
//    echo 'inny types';
    return true;
  }
  return false; // nie trzeba aktualizować
} // end of checkUpdateShadow() function
$n = 0;

$aLimitFirstSource = getAllFirstSourceDictountLimit();
  
  $sLog='';
  //sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
  $sSql = "SELECT DISTINCT A.id, A.publisher_id, C.series_id, A.publication_year, A.isbn, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.prod_status, A.edition,
                  A.shipment_time, A.price_brutto, A.shipment_date, A.name AS title, A.name2, B.name AS publisher, B.discount_limit, D.discount_limit AS series_discount_limit, 
                  D.name AS series, E.binding, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.created_by, A.show_packet_year, A.type,
                  (SELECT GROUP_CONCAT(PS.website_id SEPARATOR ',' ) FROM products_websites AS PS WHERE PS.product_id = A.id) AS products_websites,
                  A.published
           FROM ".$aConfig['tabls']['prefix']."products A
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
            ON B.id=A.publisher_id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series C
            ON A.id = C.product_id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_series D
            ON C.series_id = D.id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings E
           ON E.id=A.binding
           WHERE A.published = '1' AND A.product_type IN (\"".implode('", "', $aConfig['common']['allowed_product_type'])."\") AND A.page_id <> ".$aConfig['import']['unsorted_category'];
  $oProducts = Common::PlainQuery($sSql);

  while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
    if (empty($aItem)) {
      break;
    }
    $n++;

      if ($aItem['published'] == '1' && $aItem['products_websites'] != '') {
          $productIds = explode(',', $aItem['products_websites']);
          $bookstoreCode = $aConfig[$aConfig['common']['bookstore_code'].'_website_id'];
          if (!in_array($bookstoreCode, $productIds)) {
              echo "TUUUU - ".print_r($aItem, true);
              continue;
          }
      }
      unset($aItem['products_websites']);
      unset($aItem['published']);

    if (isset($aLimitFirstSource[strtolower($aItem['created_by'])])) {
      $aItem['first_source_discount_limit'] = floatval($aLimitFirstSource[strtolower($aItem['created_by'])]);
    } else {
      $aItem['first_source_discount_limit'] = 0;
    }
    
    $aItem['isbn_10'] = (empty($aItem['isbn_10'])) ? 'NULL' : $aItem['isbn_10'];
    $aItem['isbn_13'] = (empty($aItem['isbn_13'])) ? 'NULL' : $aItem['isbn_13'];
    $aItem['ean_13'] = (empty($aItem['ean_13'])) ? 'NULL' : $aItem['ean_13'];

    if (($aItem['discount_limit'] <= 0.00) || (($aItem['series_discount_limit'] > 0) && $aItem['series_discount_limit'] < $aItem['discount_limit'])) {
      $aItem['discount_limit'] = $aItem['series_discount_limit'];
    }
    if (($aItem['discount_limit'] <= 0.00) || ($aItem['first_source_discount_limit'] > 0) && $aItem['first_source_discount_limit'] < $aItem['discount_limit']) {
      $aItem['discount_limit'] = $aItem['first_source_discount_limit'];
    }

    $aShadow = getProductShadow($aItem['id']);
    if($aItem['price_brutto'] == 0 && $aItem['prod_status'] == '1') {
      $aItem['prod_status'] = '0';
      echo 'odpublikowujemy <br />'."\r\n";
      /*
       * XXX TODO odkomentować
       * dump('ODPUBLIKOWANIE PRODUKTU');
      */
      $sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
                SET prod_status = '0',
                modified = NOW(),
                modified_by = 'import'
                WHERE id = ".$aItem['id'];
      if (Common::Query($sSql) === false) {
          echo "błąd zmiany stanu produktu ".$aItem['title']." - ".$aItem['id']."<br>\n";
          $sLog.="błąd zmiany stanu produktu ".$aItem['title']." - ".$aItem['id']."<br>\n";
          dump($aItem);
      }
    }
    $fTarrifPrice = getTarrifPrice($aItem['id']);

    if(empty($aShadow)) {
      unset($aItem['first_source_discount_limit']);
      unset($aItem['series_discount_limit']);
      unset($aItem['show_packet_year']);
      unset($aItem['created_by']);
      $aItem['authors'] = getProductAuthors($aItem['id']);
      $aItem['tags'] = getProductTags($aItem['id']);
      $aItem['price'] = $fTarrifPrice;
      $aTopCats=getProductTopCats($aItem['id']);

      $aItem['order_by'] = getOrderByNr($aItem, $productsStats[$aItem['id']]);
      $aItem['order_by_status_old_style'] = getOrderByStatusOldStyle($aItem);

      $aItem['page_id_1'] = $aTopCats[0];
      $aItem['page_id_2'] = $aTopCats[1];
      $aItem['page_id_3'] = $aTopCats[2];
      if($aItem['page_id_1'] == $aConfig['import']['unsorted_category'] ||
          $aItem['page_id_2'] == $aConfig['import']['unsorted_category'] || 
          $aItem['page_id_3'] == $aConfig['import']['unsorted_category']
      ) {
        //echo "produkt [".$aItem['id']."] ".$aItem['title']." posiada kategorie nieposortowana - pomijam<br>\n";
        //$sLog.="produkt [".$aItem['id']."] ".$aItem['title']." posiada kategorie nieposortowana - pomijam<br>\n";
      }
      else {
        /*
         * XXX TODO - odkomentować
         * dump('Insert');
         * dump($aItem);
        */
        if (Common::Insert($aConfig['tabls']['prefix']."products_shadow", $aItem,'',false) === false) {
          echo "błąd dodawania cienia produktu ".$aItem['title']."<br>\n";
          $sLog.="błąd dodawania cienia produktu ".$aItem['title']."<br>\n";
          dump($aItem);
        }
      }
    } else {
      $sOrdBy = getOrderByNr($aItem, $productsStats[$aItem['id']]);
      $orderByStatusOldStyle = getOrderByStatusOldStyle($aItem);
      if ($aItem['packet'] == '1' && $aItem['show_packet_year'] == '0') {
        unset($aItem['publication_year']);
      }
      if( checkUpdateShadow($aItem, $aShadow, $fTarrifPrice, $sOrdBy, $orderByStatusOldStyle) ) {
        //$aShadow['prod_status'] = $aItem['prod_status'];
        //$aShadow['shipment_time'] = $aItem['shipment_time'];

        $aValues = array(
          'publication_year' => $aItem['publication_year'],
          'prod_status' => $aItem['prod_status'],
          'shipment_time' => $aItem['shipment_time'],
          'shipment_date' => $aItem['shipment_date'],
          'price' => $fTarrifPrice,
          'price_brutto' => $aItem['price_brutto'],
          'order_by' => $sOrdBy,
          'order_by_status_old_style' => $orderByStatusOldStyle,
          'discount_limit' => $aItem['discount_limit'],
          'is_news' => $aItem['is_news'],
          'is_bestseller' => $aItem['is_bestseller'],
          'is_previews' => $aItem['is_previews'],
          'type' => $aItem['type'],
        );
        /*
         * XXX TODO - odkomentować
         * dump('Update');
         * dump($aValues);
         */
        if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues, 'id = '.$aItem['id']) === false) {
          echo "błąd aktualizacji cienia produktu ".$aItem['title']." - ".$aItem['id']."<br>\n";
          $sLog.="błąd aktualizacji cienia produktu ".$aItem['title']." - ".$aItem['id']."<br>\n";
          dump($aItem);
        }
        echo 'zmieniono produkt :'.$aItem['id'].'<br />';
      }
    }
  }

  $oProducts->free();
  unset($oProducts);

//echo 'stop';
//sendInfoMail('updateShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." zakończono tworzenie tablicy pomocniczej wyszukiwarki \n".$sLog);
?>