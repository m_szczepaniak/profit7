<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
global $pDbMgr;

$aAllProductWebsites = $sSql = 'SELECT * FROM websites WHERE url LIKE "http%" AND code <> "profit24" ';
$aWebsites = $pDbMgr->GetAll('profit24', $sSql);



$sDBPR24 = $aConfig['db']['name'];

$sSqlDelProducts = '
DELETE FROM %1$s.products
WHERE id IN
(
  SELECT id FROM
    (
      SELECT PM.id FROM %1$s.products AS PM
      LEFT JOIN %2$s.products AS P
      ON PM.profit24_id = P.id
      WHERE P.id is null
    ) AS TMP
)';

$sSqlDelShadow = '
DELETE FROM %1$s.products_shadow
WHERE id IN (
    SELECT id FROM (
        SELECT PS.id FROM %1$s.products_shadow AS PS
        LEFT JOIN %1$s.products AS P
        ON P.id = PS.id
        WHERE P.id IS NULL
    ) AS TMP
)';

foreach ($aWebsites as $aWebsite) {
    $sSql = sprintf($sSqlDelProducts, $aWebsite['db_name'], $sDBPR24);
    var_dump($sSql);
    $pDbMgr->Query($aWebsite['code'], $sSql);
    sleep(5);
    $sSql = sprintf($sSqlDelShadow, $aWebsite['db_name']);
    var_dump($sSql);
    $pDbMgr->Query($aWebsite['code'], $sSql);
    sleep(5);
}