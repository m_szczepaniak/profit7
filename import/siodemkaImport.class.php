<?php
/**
 * Skrypt importu produktów ze źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');	


$bTestMode = false; //  czy test mode
$iLimitIteration = 50000000000;
$sSource = 'siodemka';
$sElement = 'products';
$oImportXMLController = new importXMLController($sSource, $sElement, $bTestMode, $iLimitIteration);
if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportXMLController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'XML/siodemka/082508_29072014_products.XML';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportXMLController->parseXMLFile($sFilename, 'Super7', 'A');
	/*
	dump($oImportXMLController->aErrorLog);
	dump($oImportXMLController->oSourceElements->aErrorLog);
	*/
  dump(print_r($oImportXMLController->aLog, true));
  dump(print_r($oImportXMLController->oSourceElements->aLog, true));
  dump(print_r($oImportXMLController->aErrorLog, true));
  dump(print_r($oImportXMLController->oSourceElements->aErrorLog, true));
	$oImportXMLController->sendInfoMail("IMPORT ".$sSource." ".$sElement." - OK", print_r($oImportXMLController->aLog, true).
																												print_r($oImportXMLController->oSourceElements->aLog, true));
	$oImportXMLController->sendInfoMail("IMPORT ".$sSource." ".$sElement." ERR", print_r($oImportXMLController->aErrorLog, true).
																												print_r($oImportXMLController->oSourceElements->aErrorLog, true));
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
	
}
	 
	 
// TODO mail z informacją o imporcie

echo 'koniec';

?>
