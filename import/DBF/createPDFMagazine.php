<?php
/**
 * Tworzenie DBF skryptem
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-07-26 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['config']['project_dir'] = 'import/DBF';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/Module_stock_report.class.php');


//$sStartDate = '23-05-2014';
if (isset($argv[1]) && $argv[1] != '') {
  $sStartDate = $argv[1];
} else {
  $sStartDate = date("d-m-Y", strtotime('-1 day', strtotime(date('Y-m-d H:i:s'))));
}
$_SESSON['user']['name'] = 'auto-pdf';
$_POST['end_date'] = $sStartDate;
$_POST['start_date'] = $sStartDate;
$oModule = new Module($pSmarty, TRUE);
$oModule->sTemplatePath = $_SERVER['DOCUMENT_ROOT'].'omniaCMS/smarty/templates/';
$oModule->sModule = 'm_zamowienia_stock_report';

$aConfig['common']['client_base_path'] = $_SERVER['DOCUMENT_ROOT'];
$aGeneretedFiles = $oModule->showStats($pSmarty);
