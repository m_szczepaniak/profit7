<?php
/**
 * Generowanie XML dla porownywarki cen Nokaut.pl
 * 
 * @author	Maciej Strączkowski omnia.pl
 * @since	27.12.2011
 * @version	2.0
 */

// Ustawienia
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';
include_once ('import_func.inc.php');

// Kodowanie
$sEncoding = 'UTF-8';

// Utworzenie obiektu klasy Nokaut_XML_Creator();
include_once('Nokaut.class.php');
$oNokaut = new Nokaut_XML_Creator($_SERVER['DOCUMENT_ROOT'].'nokaut_v2.xml', $sEncoding);

// --------------------------------------------------------------------

	/**
	 * Funkcja getProductAuthors();
	 * 
	 * Funkcja pobiera autorow ksiazki
	 * Na podstawie przekazanego ID produktu
	 * 
	 * @param	integer	$iId	- ID produktu
	 * @return	string	- Autorzy 
	 */
	function getProductAuthors($iId) {
		global $aConfig;
		
		$sAuthors='';
		$sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
					FROM ".$aConfig['tabls']['prefix']."products_to_authors A
					JOIN ".$aConfig['tabls']['prefix']."products_authors B
						ON A.author_id=B.id
					WHERE A.product_id =".$iId."
					ORDER BY A.order_by";
		$aItems =& Common::GetCol($sSql);
		
		foreach($aItems as $sItem){
			$sAuthors .= $sItem.', ';
		}
		if(!empty($sAuthors)){
			$sAuthors = substr($sAuthors,0,-2);
		}
		return $sAuthors;
	}//end of getProductAuthors() function
	
// --------------------------------------------------------------------
	
	/**
	 * Funkcja getTarrifPrice();
	 * 
	 * Funkcja pobiera aktualna cene brutto z cennika
	 * Na podstawie przekazanego ID produktu
	 * 
	 * @param	integer	$iId	- ID produktu
	 * @return	decimal	- Cena brutto
	 */
	function getTarrifPrice($iId) {
		global $aConfig;
		
		$sSql = "SELECT price_brutto
					FROM ".$aConfig['tabls']['prefix']."products_tarrifs
					WHERE product_id = ".$iId." 
						AND start_date <= UNIX_TIMESTAMP() 
						AND end_date >= UNIX_TIMESTAMP()
					ORDER BY type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	}//end of getTarrifPrice() function
	
// --------------------------------------------------------------------
	
	/**
	 * Funkcja getProductCat();
	 * 
	 * Funkcja pobiera kategorie produktu
	 * Na podstawie przekazanego ID produktu
	 * 
	 * @param	integer	$iId	- ID produktu
	 * @return	array 
	 */
	function getProductCat($iId) {
		global $aConfig;
		
		$sSql = "SELECT A.id, A.name
					FROM ".$aConfig['tabls']['prefix']."menus_items A
					JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					WHERE B.product_id = ".$iId."
					ORDER BY A.priority DESC
					LIMIT 1";
    $aCat = Common::GetRow($sSql);
		return $aCat;
	}//end of getProductCat() function
	
// --------------------------------------------------------------------

	/**
	 * Funkcja getCategoryParent();
	 * 
	 * Funkcja pobiera nazwe kategorii ojca produktu
	 * Na podstawie przekazanego ID produktu
	 * 
	 * @param	integer	$iId	- ID produktu
	 * @return	string 
	 */
	function getCategoryParent($iId) {
		global $aConfig;

		$sSql="SELECT B.name
				 FROM ".$aConfig['tabls']['prefix']."menus_items A
				 JOIN ".$aConfig['tabls']['prefix']."menus_items B
					ON B.id=A.parent_id
				 WHERE A.id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return (!empty($aItem['name']) ? $aItem['name'].'/' : '');
	}//end of getCategoryParent() function

// --------------------------------------------------------------------
	
	/**
	 * Funkcja getProductImg();
	 * 
	 * Funkcja zwraca sciezke do okladki produktu
	 * Ktorego ID zostalo przekazane jako parametr $iId
	 * 
	 * @param	integer	$iId	- ID produktu
	 * @return	string	- Sciezka do okladki
	 */
	function getProductImg($iId) {
		global $aConfig;

		$sSql = "SELECT directory, photo, mime
					FROM ".$aConfig['tabls']['prefix']."products_images
					WHERE product_id = ".$iId."
					ORDER BY order_by LIMIT 0, 1";
		$aImg =& Common::GetRow($sSql);

		if(!empty($aImg)){
			$sDir = $aConfig['common']['photo_dir'];

			if(!empty($aImg['directory'])){
				$sDir .= '/'.$aImg['directory'];
			}

			if($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])){
				return $sDir.'/__b_'.$aImg['photo'];
			}
			else {
				if($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])){
					return $sDir.'/'.$aImg['photo'];
				}
			}
		}
		return '';
	}//end of getItemImage() function
  

  /**
   * Metoda pobiera opisy dodawane do tytułu produktu - dostępność
   * 
   * @return type
   */
  function getShipmentTimes(){

    $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
    return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
  }// end of getShipmentTimes() method
	
  
	$aShipmentTimes = getShipmentTimes();
	// Pobranie produktow z bazy danych
	$sSql = "SELECT
					A.id, A.plain_name, A.isbn, A.isbn_plain, A.ean_13, A.azymut_index,  A.pages, A.edition, 
					A.shipment_time, A.price_brutto, A.shipment_time, A.description, 
					B.name AS publisher_name, L.binding, I.language, J.language AS original_language, 
					A.city, A.volume_name, A.volume_nr, A.publication_year, A.name2, E.name AS series_name
				FROM ".$aConfig['tabls']['prefix']."products A
				LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					ON B.id=A.publisher_id
				LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages I
					ON A.language=I.id
				LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages J
					ON A.original_language=J.id
				LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings L
					ON A.binding=L.id
				LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series D
					ON D.product_id=A.id
	  			LEFT JOIN ".$aConfig['tabls']['prefix']."products_series E
	  				ON E.id=D.series_id
				WHERE A.published = '1'
					AND (A.prod_status = '1' OR A.prod_status = '3') 
					AND (A.shipment_time = '0' OR A.shipment_time='1')
					AND A.page_id <> ".$aConfig['import']['unsorted_category']."
				GROUP BY A.id ";
	$oProducts = Common::PlainQuery($sSql);

	// Petla po wszystkich produktach
	while($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC))
	{
		// Pobranie kategorii
		$aCat = getProductCat($aItem['id']);
		
		if(!empty($aCat['name']))
		{
			// Pobranie pelnej sciezki kategorii danego produktu example: (Kategoria/Podkategoria)
			$sCatFullPath = getCategoryParent($aCat['id']).str_replace('/ ', '', $aCat['name']);
			
			// Pobranie ceny brutto
			$fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
			
			// Pobranie autorow oddzielonych przecinkiem
			$sAuthors = getProductAuthors($aItem['id']);
			
			// Pobranie sciezki okladki
			$sImg = getProductImg($aItem['id']);

			// Przygotowanie danych do XML
			// TAGI GLOWNE <tag></tag>
			$aXMLItem = array(
				 'id' => $aItem['id'],
				 'name' => htmlspecialchars($aItem['plain_name'], ENT_QUOTES, $sEncoding, false).$aShipmentTimes[$aItem['shipment_time']],
				 'description' => htmlspecialchars(strip_tags(clearAsciiCrap($aItem['description'])), ENT_QUOTES, $sEncoding, false),
				 'url' => htmlspecialchars($aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name']), ENT_QUOTES, $sEncoding, false),
				 'price' => $fTarrifPrice,
				 'category' => htmlspecialchars($sCatFullPath, ENT_QUOTES, $sEncoding, false),
				 'availability' => $aItem['shipment_time']
			);
			!empty($sImg) ? $aXMLItem['image'] = htmlspecialchars($aConfig['common']['base_url_http'].$sImg, ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['publisher_name']) ? $aXMLItem['publisher_name'] = htmlspecialchars($aItem['publisher_name'], ENT_QUOTES, $sEncoding, false) : '';

			// Przygotowanie danych do XML
			// TAGI PROPERTY <property name="tag"></property>
			!empty($aItem['ean_13']) ? $aXMLItemProperties['ean'] = $aItem['ean_13'] : '';
			!empty($aItem['isbn']) ? $aXMLItemProperties['isbn'] = $aItem['isbn'] : '';
			!empty($aItem['azymut_index']) ? $aXMLItemProperties['osdw'] = $aItem['azymut_index'] : '';
			!empty($sAuthors) ? $aXMLItemProperties['autor'] = htmlspecialchars($sAuthors, ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['pages']) ? $aXMLItemProperties['liczba stron'] = $aItem['pages'] : '';
			!empty($aItem['edition']) ? $aXMLItemProperties['numer wydania'] = $aItem['edition'] : '';
			!empty($aItem['publication_year']) ? $aXMLItemProperties['rok wydania'] = $aItem['publication_year'] : '';
			!empty($aItem['name2']) ? $aXMLItemProperties['podtytuł'] = htmlspecialchars($aItem['name2'], ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['binding']) ? $aXMLItemProperties['oprawa'] = htmlspecialchars($aItem['binding'], ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['language']) ? $aXMLItemProperties['język'] = htmlspecialchars($aItem['language'], ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['original_language']) ? $aXMLItemProperties['język oryginału'] = htmlspecialchars($aItem['original_language'], ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['city']) ? $aXMLItemProperties['miejsce wydania'] = htmlspecialchars($aItem['city'], ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['volume_name']) ? $aXMLItemProperties['tytuł tomu'] = htmlspecialchars($aItem['volume_name'], ENT_QUOTES, $sEncoding, false) : '';
			!empty($aItem['volume_nr']) ? $aXMLItemProperties['numer kolejnego tomu'] = $aItem['volume_nr'] : '';
			!empty($aItem['series_name']) ? $aXMLItemProperties['seria wydawnicza'] = $aItem['series_name'] : '';
			
			// Dodanie produktu do XML
			// Usuniecie zawartosci tablicy
			$oNokaut->addItem($aXMLItem, $aXMLItemProperties);
			unset($aXMLItem, $aXMLItemProperties);
		}
	}
?>