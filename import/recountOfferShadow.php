<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 07.10.16
 * Time: 11:00
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
include_once('import_func.inc.php');


global $pDbMgr;


include_once('modules/m_oferta_produktowa/Module_Common.class.php');
$GLOBALS['pProductsCommon'] = new Module_Common();


$sSql = 'SELECT id, published FROM products WHERE published = "1" ORDER BY id DESC';
$aRows = $pDbMgr->GetAll('profit24', $sSql);
$i = 0;
$iCount = count(array_keys($aRows));
foreach ($aRows as $aRow) {

    // Aktualizacja shadowa
    if ($pProductsCommon->updateShadow($aRow['id']) === false) {
        $sLogInfo .= "Wystąpił błąd podczas aktualizacji produktu w shadow, " . $aRow['id'];
    }
    if ($pProductsCommon->updateShadowAuthors($aRow['id']) === false) {
        $sLogInfo .= "Wystąpił błąd podczas aktualizacji autorów produktu w shadow, " . $aRow['id'];
    }
    if ($pProductsCommon->updateShadowTags($aRow['id']) === false) {
        $sLogInfo .= "Wystąpił błąd podczas aktualizacji tagów produktu w shadow, " . $aRow['id'];
    }
    echo 'Zmieniono ' . $aRow['id'] . ' - ' . $i . ' z ' . $iCount . "\r\n";
    $i++;
}
echo $sLogInfo;