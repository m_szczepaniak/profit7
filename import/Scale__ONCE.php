<?php


use Scale\ScaleLoader;

$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once('../LIB/autoloader.php');

$scaleValue = $_GET['scale_value'];
$scaleId = $_GET['scale_id'];

$scaleLoader = new ScaleLoader();
$scaleLoader->updateScaleValue($scaleId, $scaleValue);

die;

