<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);


include_once ('import_func.inc.php');
include_once ('CommonFunctionsHelion.php');

include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
$oCommonSynchro->iSourceId = '4';

$iDevLimit=0;


$sLogInfo='';

/**
 * Metoda dodaje powiązania kategorii produktu
 *
 * @global array $aConfig
 * @param string $sBookstore
 * @param string $sHelionIdent
 * @param array $aCatIds
 * @return bool 
 */
function addHelionInd2Cat($sBookstore, $sHelionIdent, $aCatIds) {
	global $aConfig;
	
	foreach ($aCatIds as $iCatId) {
		$aValues = array(
				'helion_id' => $sHelionIdent, // id produktu w helion
				'helion_category' => $iCatId, // kategoria w tabeli products_helion_categories
				'helion_source' => $sBookstore // źródło kategorii czyli sklep
		);
		// istniała książka która posiadala kategorie która nie była na liście dostepnych kategorii
		@Common::Insert($aConfig['tabls']['prefix']."products_helion_ind2cat", $aValues, '', false);
	}
	
	return true;
}// end of addHelionInd2Cat() method

/**
 * Metoda sprawdza czy produkt o podanym isbn istnieje w bufforze
 *
 * @global array $aConfig
 * @param string $sPlainISBN
 * @return bool 
 */
function checkProductExistsBuffer($sPlainISBN) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_helion_buffer
					 WHERE isbn_plain='".$sPlainISBN."'";
	return (intval(Common::GetOne($sSql))>0 ? true : false );
}// end of checkProductExistsBuffer() method


	
function addHelionBuffer($aValues, $sBookstore, $sHelionIdent) {
	global $aConfig, $sLogInfo, $oCommonSynchro;
	if (empty($aValues['categories'])) return true; // brak kategorii nie ma sensu tu dodawać 
		
	if (checkProductExistsBuffer($aValues['product']['isbn_plain']) === true) {
		return true;
	}
	
	unset($aValues['product']['created']);
	unset($aValues['product']['created_by']);
	Common::BeginTransaction();

	
	/*
	$aValues['product']['page_id'] = getPageId($aCats);
	if($aValues['product']['page_id'] == $aConfig['import']['unsorted_category']) {
		$aValues['product']['published'] = '0';
	} else {
		$aValues['product']['published'] = '1';
	}
	*/
	$aValues['product']['helion_id'] = $sHelionIdent;
	/* marka wg XML:
	  		<marka>:
			1 - helion
			2 - onepress
			3 - editio
			4 - sensus
			5 - septem
			6 - bezdroza
			8 - podrecznik
			11 - bezdroża obce
	 */
	$aValues['product']['source_type'] = $aValues['marka'];
	$aValues['product']['reviewed'] = '1';
	$aValues['product']['source'] = $sBookstore;
	$aValues['product']['shipment_time'] = '1';
	// opis
	$aValues['product']['description']= eregi_replace('<a.*</a>','', $aValues['product']['description']);
	$aValues['product']['description']= eregi_replace('<script.*</script>','', $aValues['product']['description']);

	$aValues['product']['description'] = trim($aValues['product']['description']);
	$aValues['product']['short_description'] = strip_tags(br2nl(trimString($aValues['product']['description'],200),' '));

	$aValues['product']['series_helion_id'] = $aValues['series'][0];
	// dodawanie kategorii
	$aValues['product']['authors'] = $aValues['authors'];
	// dodawanie kategorii
	$aValues['product']['translations'] = $aValues['translations'];
	if ($aValues['binding'] != '') {
		// oprawa
		$iBindingId = getBindingId($aValues['binding']);
		$aValues['binding'] = ( $iBindingId > 0 ? $iBindingId : 'NULL' );
	}
	
	$aValues['product']['cover'] = $aValues['okladka'];
	
	if (!empty($aValues['linked'])) {
		$aValues['product']['linked'] = "'".implode("','", $aValues['linked'])."'";
	}
					
	$aValuesInsert = $oCommonSynchro->clearProductVars($aValues['product']);
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_helion_buffer", $aValuesInsert)) !== false) {
		// DODANO PRODUKT
		$sLogInfo.= "dodano produkt do buffora helion ".$iId." - ".$aValues['product']['name']."\n";
		
		addHelionInd2Cat($sBookstore, $sHelionIdent, $aValues['categories']);
			
		Common::CommitTransaction();
	} else {
		Common::RollbackTransaction();
		dump($aValues['product']);
		dump(mysql_error());
		die("problem z dodaniem produktu");
	}
}
	
	
/**
 * Funkcja sprawdza czy produkt jest w Helionie z innego wydawnictwa,
 * nie ma znajdować się w naszej ofercie jako produkt wyd. Helion
 * 
 * @param string $sHelionIdent
 * @return bool
 */
function checkHelionOtherPublisher($sHelionIdent) {
  return preg_match('/^A_.+/', $sHelionIdent);
}// end of checkHelionOtherPublisher() method
	
/**
 * dodaje nowy produkt do bazy
 * @param $aValues
 * @return unknown_type
 */
function checkAddProduct(&$aValues, $sBookstore, $sHelionIdent, $aMatchedProductDB){
	global $aConfig, $iDevLimit, $iPublisherID, $sLogInfo, $oCommonSynchro;
  
	// uwaga, nie synchronizujemy produktów, które nie są heliona
  if (checkHelionOtherPublisher($sHelionIdent)) {
    return true;
  }
  
	$aValues['product']['_stock'] = '>70';// to taka wartość domyslna
	$aValues['helion_id'] = $sHelionIdent;
	switch($aValues['status']){
		case 1:
			$aValues['product']['prod_status'] = '1';
		break;
		case 2:
			// na_ukonczeniu
			$aValues['product']['prod_status'] = '1';
			$aValues['product']['_stock'] = 'na_ukonczeniu';
		break;
		case 7:
			// przedsprzedaz
			$aValues['product']['prod_status'] = '0';
		break;
		default:
			$aValues['product']['prod_status'] = '0';
		break;
	}
	$aMatches = array();
	if (($aValues['status'] == '6' || $aValues['status'] == '7') && 
			isset($aValues['przedsprzedazdo']) && 
			preg_match("/^\d{4}-\d{2}-\d{2}$/", $aValues['przedsprzedazdo'], $aMatches) &&
			!empty($aMatches) &&
			$aValues['przedsprzedazdo'] != '') {
		// spełnia warunki przedsprzedazy
		$aValues['product']['prod_status'] = '3';
		$aValues['product']['_shipment_date'] = $aValues['przedsprzedazdo'];
	}
  
	// jesli jest w promocji przedsprzedaz
	// i rok wydania jest pusty, należy ustawić rok wydania na aktualny
	if ($aValues['przedsprzedaz'] == '1' && intval($aValues['product']['publication_year']) <= 0) {
		$aValues['product']['publication_year'] = date('Y');
	}
 

	// dodaj, lub zaktualizuj bookindeks dla produktu
	$mRetMD5 = $oCommonSynchro->addBookIndeks($aMatchedProductDB['id'], $sHelionIdent, $oCommonSynchro->iSourceId, $aValues['md5']);

	// echo 'produkt: '.$aValues['product']['isbn_plain']." <br />";
	if (isset($aMatchedProductDB['id']) && intval($aMatchedProductDB['id']) > 0) {
		
		$aBookStatus = $oCommonSynchro->getProductStock($aMatchedProductDB['id']);
		if (!empty($aBookStatus) && !empty($aMatchedProductDB)) {
			$aBookStatus = array_merge($aBookStatus, $aMatchedProductDB);
		} elseif (!empty($aMatchedProductDB)) {
			$aBookStatus = $aMatchedProductDB;
		}
		if ($oCommonSynchro->checkUpdateProduct($aMatchedProductDB, $mRetMD5, $aMatchedProductDB['id'])) {
					//echo 'produkt: '.$aValues['product']['isbn_plain']." aktualizacja <br />";


					//// XXXX poczatek
					// Aktualizacja
					$aProfitCategories = getHelionMappings($sBookstore, $aValues['categories']);
					if (empty($aProfitCategories) && $aValues['editio'] == '1') {
						// seria lub marka editio
						//$aCats[] = '1'; // powinno ma wpaść do działu ogólnego
						$aValues['categories'][] = '9999';// kategoria helion mapowana na sztywno (brak tej kategorii w rzeczywistym helion)
					}
					$aProfitCategories = getHelionMappings($sBookstore, $aValues['categories']);
					if (empty($aProfitCategories)) return true; // brak kategororii

					//dodawanie powiazanych z heliona--------------------
					if(!empty($aValues['_linked'])) {
						$aValues['product']['_linked']=findBookByHelionId($aValues['linked']);
					}
					unset($aValues['linked']);

					$aValues['product']['_items_authors'] = $aValues['authors'];
					$aValues['product']['_binding'] = $aValues['binding'];
          if (checkHelionOtherPublisher($sHelionIdent) && $aValues['publisher'] != '') {
            $aValues['product']['publisher_id'] = getPublisherId($aValues['publisher']);
          } else {
            $aValues['product']['publisher_id'] = getHelionPublisherIdByBrand($aValues['marka']);
          }

					$iNewsMainCategory = NULL;
					$iPreviewsMainCategory = NULL;
					switch($sBookstore){
							case 'helion':
							case 'onepress':
								$iNewsMainCategory = 425;
								$iPreviewsMainCategory = 2338;
							break;
							case 'sensus':
							case 'septem':
								$iNewsMainCategory = 423;
								$iPreviewsMainCategory = 2337;
							break;
						}

          $fWHDiscount = getHelionDiscountByBrand($aValues['marka']);
          $aValues['product']['_wholesale_price'] = Common::formatPrice2($aValues['product']['price_brutto'] -  Common::formatPrice2($aValues['product']['price_brutto'] * ($fWHDiscount / 100)));
					if ($oCommonSynchro->proceedUpdateProduct($aValues['product'], $aMatchedProductDB['id'], $sHelionIdent, $aProfitCategories, $iNewsMainCategory, $iPreviewsMainCategory) > 0) {
						$iId = $aMatchedProductDB['id'];

						if (!empty($aValues['series'])) {
							addSeries($iId, $aValues['series'][0], $sBookstore, $aValues['product']['publisher_id']);
						}


						if ($aValues['okladka'] != '') {
							addHelionImage($iId,$aValues['okladka'],$aValues['product']['isbn_plain'],strtolower($aValues['helion_id']));
						}
					} else {
						dump($oCommonSynchro->aErrorLog);
						dump($oCommonSynchro->aLog);
					}

					///\\\ XXX koniec
		} else {
			$iId = $aMatchedProductDB['id'];
			// aktualizacja stanów
			//
			// stan produktu
			$iNewsMainCategory = NULL;
			$iPreviewsMainCategory = NULL;
			switch($sBookstore){
				case 'helion':
				case 'onepress':
					$iNewsMainCategory = 425;
					$iPreviewsMainCategory = 2338;
				break;
				case 'sensus':
				case 'septem':
					$iNewsMainCategory = 423;
					$iPreviewsMainCategory = 2337;
				break;
			}

			$aValues['product']['shipment_time'] = '1';
      $fWHDiscount = getHelionDiscountByBrand($aValues['marka']);
      $aValues['product']['_wholesale_price'] = Common::formatPrice2($aValues['product']['price_brutto'] -  Common::formatPrice2($aValues['product']['price_brutto'] * ($fWHDiscount / 100)));

			if ($oCommonSynchro->proceedProductStock('helion', $iId, $aValues['product'], true, $iNewsMainCategory, $iPreviewsMainCategory) === false) {
				dump($aValues['product']);
				echo 'WYSTĄPIŁ BŁĄD PODCZAS AKTUALIZACJI STATNÓW I CEN '.$iId."\r\n";
				$sLogInfo .= 'WYSTĄPIŁ BŁĄD PODCZAS AKTUALIZACJI STATNÓW I CEN '.$iId."\r\n";
			} else {
				//dump('Zaktualizowano stany produktu');
				//dump($aValues['product']);
			}
			
			//echo "Nie trzeba aktualizować nie został zmieniony lub został zmodyfikowany przez obsługę w CMS ".$aBookStatus['id']." isbn ".$aBookStatus['isbn_plain']." \n";
			$sLogInfo.= "Nie trzeba aktualizować nie został zmieniony lub został zmodyfikowany przez obsługę w CMS ".$aBookStatus['id']." isbn ".$aBookStatus['isbn_plain']." \n";
			
			/*
			$aStatusValues = array(
				'helion_status' => $aValues['product']['prod_status'],
				'helion_price_brutto' => $aValues['product']['price_brutto'],
				'helion_price_netto' => $aValues['product']['price_netto'],
				'helion_vat' => $aValues['product']['vat'],
				'helion_shipment_time' => $aValues['product']['shipment_time'],
				'helion_shipment_date' => $aValues['product']['_shipment_date'],
				'helion_last_import' => 'NOW()'
			);
			*/
			/*
			//dump($aStatusValues);
			if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aStatusValues,"id = ".$iId)) === false) {
									echo("błąd aktualizacji stanu produktu ".$iId."<br/>\n");
									$sLogInfo.= "błąd aktualizacji stanu produktu ".$iId."\n";
			}
		*/
			
		}
	} else {
		
		// dodawanie nowego
		$aCats = getHelionMappings($sBookstore,$aValues['categories']);
		if (empty($aCats) && $aValues['editio'] == '1') {
			// seria lub marka editio
			//$aCats[] = $aConfig['import']['unsorted_category_editio'];
			$aValues['categories'][] = '9999';// kategoria helion mapowana na sztywno (brak tej kategorii w rzeczywistym helion)
		}
		$aCats = getHelionMappings($sBookstore,$aValues['categories']);
		if (!empty($aCats)){
		Common::BeginTransaction();

			$aValues['product']['page_id'] = getPageId($aCats);
			if($aValues['product']['page_id'] == $aConfig['import']['unsorted_category'] || $aValues['product']['page_id'] == $aConfig['import']['unsorted_category_editio']) {
				// tu już nie powinno wpaść .. kategoria nieposrtowane
				$aValues['product']['published'] = '0';
			} else {
				$aValues['product']['published'] = '1';
			}
			$aValues['product']['reviewed'] = '1';
			$aValues['product']['created'] = 'NOW()';
			$aValues['product']['created_by'] = 'Helion';
			$aValues['product']['source'] = '4';
			$aValues['product']['last_import'] = 'NOW()';
			$aValues['product']['shipment_time'] = '1';
			$aValues['product']['helion_index'] = $aValues['helion_index'];

      if (checkHelionOtherPublisher($sHelionIdent) && $aValues['publisher'] != '') {
        $aValues['product']['publisher_id'] = getPublisherId($aValues['publisher']);
      } else {
        $aValues['product']['publisher_id'] = getHelionPublisherIdByBrand($aValues['marka']);
      }
			// opis
			$aValues['product']['description']= eregi_replace('<a.*</a>','', $aValues['product']['description']);
			$aValues['product']['description']= eregi_replace('<script.*</script>','', $aValues['product']['description']);
						
			$aValues['product']['description'] = trim($aValues['product']['description']);
			$aValues['product']['short_description'] = strip_tags(br2nl(trimString($aValues['product']['description'],200),' '));
			
			if (intval($aValues['product']['publication_year']) <= 0) {
				unset($aValues['product']['publication_year']); // brak roku wydania
			}
			$aValues['product'] = $oCommonSynchro->refProceedIsbn($aValues['product'], true);
			$aValuesInsert = $oCommonSynchro->clearProductVars($aValues['product']);
			var_dump($aValues);
			if (($iId = Common::Insert($aConfig['tabls']['prefix']."products", $aValuesInsert)) !== false) {
				// DODANO PRODUKT
				$sLogInfo.= "dodano produkt ".$iId." - ".$aValues['product']['name']."\n";
				
				
				//dodawanie powiazanych z heliona--------------------
				if(!empty($aValues['linked'])) {
					$aLinkedBooks=findBookByHelionId($aValues['linked']);
					}
				if(!empty($aLinkedBooks)) {
					addLinking($aLinkedBooks, $iId);
					}	
				unset($aValues['linked']);
				//koniec dodawania powiazanych z heliona-------------	
				
				
			  $aValues['product']['vat']=vatMapper($aValues['product']['vat']);
				/*
				// aktualizacja stanów
				$aStatusValues = array(
					'helion_status' => $aValues['product']['prod_status'],
					'helion_price_brutto' => $aValues['product']['price_brutto'],
					'helion_price_netto' => $aValues['product']['price_netto'],
					'helion_vat' => $aValues['product']['vat'],
					'helion_shipment_time' => '1',
					'helion_shipment_date' => $aValues['product']['_shipment_date'],
					'helion_last_import' => 'NOW()'
				);
				//dump($aStatusValues);
				if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aStatusValues,"id = ".$iId)) === false) {
										echo("błąd aktualizacji stanu produktu ".$iId."<br/>\n");
										$sLogInfo.= "błąd aktualizacji stanu produktu ".$iId."\n";
				}
				*/
				$iNewsMainCategory = NULL;
				$iPreviewsMainCategory = NULL;
				switch($sBookstore){
					case 'helion':
					case 'onepress':
						$iNewsMainCategory = 425;
						$iPreviewsMainCategory = 2338;
					break;
					case 'sensus':
					case 'septem':
						$iNewsMainCategory = 423;
						$iPreviewsMainCategory = 2337;
					break;
				}
        
        $fWHDiscount = getHelionDiscountByBrand($aValues['marka']);
        $aValues['product']['_wholesale_price'] = Common::formatPrice2($aValues['product']['price_brutto'] -  Common::formatPrice2($aValues['product']['price_brutto'] * ($fWHDiscount / 100)));
				$oCommonSynchro->proceedProductStock('helion', $iId, $aValues['product'], false, $iNewsMainCategory, $iPreviewsMainCategory);
				if(!empty($aValues['product']['publication_year'])){
					addYearToDict(intval($aValues['product']['publication_year']));
				}
				// dodawanie serii
				if (!empty($aValues['series'])) {
					addSeries($iId, $aValues['series'][0], $sBookstore, $aValues['product']['publisher_id']);
				}
				// dodawanie kategorii
				addExtraCategories($iId, $aCats);
	
				// dodawanie autorów
				if (!empty($aValues['authors'])) {
					addHelionAuthors($iId, $aValues['authors']);
				}
				if (!empty($aValues['translations'])) {
					addHelionTranslations($iId, $aValues['translations']);
				}
				if ($aValues['binding'] != '') {
					// oprawa
					$iBindingId = getBindingId($aValues['binding']);
					$aValues['product']['binding'] = ( $iBindingId > 0 ? $iBindingId : 'NULL' );
				}
				if ($aValues['okladka'] != '') {
					addHelionImage($iId,$aValues['okladka'],$aValues['product']['isbn_plain'],strtolower($aValues['helion_id']));
				}
				
				if ($aValues['product']['published'] == '1') {
					// dodanie do shadowa
					include_once('modules/m_oferta_produktowa/Module_Common.class.php');
					$pProductsCommon = new Module_Common();
					if ($pProductsCommon->existShadow($iId) == false) {
						if ($pProductsCommon->addShadow($iId) === false) {
							echo 'wystąpił błąd podczas dodawania produktu do shadowa ,'.$iId;
							die();
						}
					}
				}
	//			if($iDevLimit++>150) {
	//				Common::RollbackTransaction();
	//				die('devlimit');
	//			}
				Common::CommitTransaction();
			} else {
				Common::RollbackTransaction();
				die("problem z dodaniem produktu");
			}
		} else {
			// brak kategorii
			// dodanie do buffora produktów
			
			if (addHelionBuffer($aValues, $sBookstore, $sHelionIdent) === false) {
				echo 'błąd dodawania do tabeli bufforowej książki dla źródła - '.$sBookstore.' dane: '.print_r($aValues, true);
				die();
			}
		}
	}
}

function parseProductCategories(&$oXml, &$aValues){
	global $iMId;
	$aValues['categories'] = array();
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'serietematyczne':
						// KONIEC kategorii
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {					
					case 'seriatematyczna':
							$aValues['categories'][]=(int)$oXml->getAttribute('id');
					break;
				}
			break;
		}
	}
}

function parseProductSeries(&$oXml, &$aValues){
	global $iPublisherID;
	$aValues['series'] = array();
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'seriewydawnicze':
						// KONIEC kategorii
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {					
					case 'seriawydawnicza':
						$aValues['series'][]=(int)$oXml->getAttribute('id');
						$sEditio = getTextNodeValue($oXml);
						if ($sEditio == 'Editio') {
							$aValues['editio'] = 1;
						}
					break;
				}
			break;
		}
	}
}

function parseLinked(&$oXml, &$aValues){
	global $iPublisherID;
	$aValues['linked'] = array();
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'powiazane':
						// KONIEC powiazanych
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'ident_powiazany':
						$aValues['linked'][]=getTextNodeValue($oXml);
					break;
				}
			break;
		}
	}
}

function parseKsiegarnieNieinter(&$oXml, &$aValues){
	global $iPublisherID;
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'ksiegarnie_nieinter':
						// KONIEC kategorii
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {					
					case 'status2':
						$aValues['status']=intval(getTextNodeValue($oXml));
					break;
					case 'cena_netto':
						$aValues['product']['price_netto'] = Common::formatPrice2(getTextNodeValue($oXml));
					break;
					case 'cena_brutto':
						$aValues['product']['price_brutto'] = Common::formatPrice2(getTextNodeValue($oXml));
					break;
					case 'vat_procent':
						$aValues['product']['vat'] = intval(getTextNodeValue($oXml));
					break;
				}
			break;
		}
	}
}


/**
 * parsuje xml dla dodania nowego produktu
 * @param $oXml
 * @return unknown_type
 */
function parseHelionProduct(&$oXml,$sBookstore,$sHelionIdent) {
global $aConfig, $sLogInfo, $oCommonSynchro;
	$aValues = array();
	$aValues['product']['created'] = 'NOW()';
	$aValues['product']['created_by'] = 'import';
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'ksiazka':
						// autor - KONIEC - dodanie do bazy
						// $iHelionProductId = getBookIdWithISBN($aValues['product']['isbn_plain']);
						
						// zabezpiecznie jeśli nie ma isbn_plain a jest ean_13
						if (empty($aValues['product']['isbn_plain']) && $aValues['product']['ean_13'] != '') {
							$aValues['product']['isbn'] = $aValues['product']['ean_13'];		
							$aValues['product']['isbn_plain'] = $aValues['product']['ean_13'];		
						}
						$aMatchedProductDB = $oCommonSynchro->refMatchProductByISBNs($aValues['product'], array('id', 'prod_status', 'published', 'source', 'publisher_id', 'modiefied_by_service'));
						updateLastImport($aMatchedProductDB['id']);
						checkAddProduct($aValues, $sBookstore, $sHelionIdent, $aMatchedProductDB);
						//dump($aValues);
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'ident':
						// ID Helion
						$aValues['helion_index'] = getTextNodeValue($oXml);
						break;
					case 'tytul':
						// Tytuł
						if($oXml->getAttribute('language')=='polski'){
							$aValues['product']['name'] = getTextNodeValue($oXml);
							$aValues['product']['plain_name'] = preg_replace('/ +/', ' ', strip_tags(br2nl($aValues['product']['name'], ' ')));
						}
						else
							$aValues['product']['original_title'] = getTextNodeValue($oXml);
						break;
					case 'autor':
						// ID AUTORA
						$aValues['authors'] = getTextNodeValue($oXml);			
						break;
					case 'tlumacz':
						// ID AUTORA
						$aValues['translations'] = getTextNodeValue($oXml);			
					break;
					
					/*case 'status':
						// ID AUTORA
						$aValues['prod_status'] = (int)getTextNodeValue($oXml);
						break;
					case 'cenadetaliczna':
						// cena
						$aValues['product']['price_brutto'] = Common::formatPrice2(getTextNodeValue($oXml));
						break;*/
					case 'przedsprzedazDO':
						// ID AUTORA
						$sSellDate = getTextNodeValue($oXml);
						if ($sSellDate != '') {
							preg_match("/^\d{4}-\d{2}-\d{2}$/", $sSellDate, $aMatches);
							$aValues['przedsprzedazdo'] = $aMatches[0];			
						}
					break;
					case 'opis':
						// opis
						$aValues['product']['description'] = strip_tags(getCDATAValue($oXml),'<p><i><u><b><ul><li><br><h4><strong><br />');
						break;
					case 'serietematyczne':
						// kategorie
						if (!$oXml->isEmptyElement) {
							parseProductCategories($oXml, $aValues);
						}
						break;
					case 'seriewydawnicze':
						// serie
						if (!$oXml->isEmptyElement) {
							parseProductSeries($oXml, $aValues);
						}
						break;
					case 'powiazane': 
						// serie
						if (!$oXml->isEmptyElement) {
							parseLinked($oXml, $aValues);
						}
						break;
					//  ------------------ atrybuty ----------------------
					case 'isbn':
						$aValues['product']['isbn'] = isbn2plain(getTextNodeValue($oXml));		
						$aValues['product']['isbn_plain'] = $aValues['product']['isbn'];		
					break;
				
					case 'ean':
						$aValues['product']['ean_13'] = isbn2plain(getTextNodeValue($oXml));
					break;	
					
					case 'liczbastron':
						// stron
						$aValues['product']['pages'] = getTextNodeValue($oXml);
						break;
					
					case 'nosnik':
						// nosnik
						$aValues['attachment'] = getTextNodeValue($oXml);
						break;
					case 'oprawa':
						// nosnik
						$aValues['binding'] = getTextNodeValue($oXml);
						break;
					case 'wydawca':
            // @date 16.01.2014
						// dodane pole wydawca, oczywiście nie informując nas
						$aValues['publisher'] = getTextNodeValue($oXml);
					break;
					case 'datawydania':
						// ID AUTORA
							preg_match("/^[0-9]{4}/", getTextNodeValue($oXml), $year);
							$aValues['product']['publication_year'] = $year[0];
						break;
					case 'online':
						// ID AUTORA
						$aValues['attributes']['online'] = getTextNodeValue($oXml);
						break;
					case 'marka':
						// marka
						$aValues['marka'] = getTextNodeValue($oXml);
						if ($aValues['marka'] == '3') {
							$aValues['editio'] = 1;
						}
					break;
					case 'okladka':
						// ID AUTORA
						$aValues['okladka'] = getTextNodeValue($oXml);
					break;
					case 'promocja':
						// ID AUTORA
						if( getTextNodeValue($oXml) == 'Przedsprzedaż') {
							$aValues['przedsprzedaz'] = '1';
						}
					break;
					case 'ksiegarnie_nieinter':
						parseKsiegarnieNieinter($oXml, $aValues);
						break;
					case 'typ':
						// typ
						$aValues['typ'] = getTextNodeValue($oXml);
						// jeśli typ to '2' oznacz to że jest to ebook
						if ($aValues['typ'] == '2') {
							unset($aValues);
							//echo 'pomijamy '.$aValues['helion_index'].' <br />';
							$sLogInfo .= 'pomijamy '.$aValues['helion_index'].' <br />';
							//echo 'pomijamy '.$aValues['helion_index'].' <br />';
							return; // XXX PRZERYWAMY IMPORT TAKIEGO PRODUKTU
						} elseif ($aValues['typ'] == '3' || $aValues['typ'] == '4') {
              $aValues['product']['type'] = '1'; // oznaczam audiobook
            }
					break;
					case 'md5':
						// md5
						$aValues['md5'] = trim(getTextNodeValue($oXml));
					break;
				}
			break;
		}
	}
} // end of parseAddProduct()

function parseProductsFile($sFilename,$sBookstore) {
global $aConfig;
	if (file_exists($sFilename)) {
	$oXml = new XMLReader();
	$oXml->open($sFilename);

		while(@$oXml->read()) {
			switch ($oXml->nodeType) {
				case XMLReader::END_ELEMENT :
					switch ($oXml->name) {
						case 'lista':
							// KONIEC LISTINGU PRODUKTOW
							$oXml->close();
						break;
					}
				break;
			
				case XMLReader::ELEMENT :
					switch ($oXml->name) {
						case 'ksiazka':
							// dodawanie produktu
							$sHelionIdent = $oXml->getAttribute('ident');
							parseHelionProduct($oXml,$sBookstore,$sHelionIdent);
							break;
					}
				break;
			}
		}
	} else {
		dump('missing file '.$sFilename);
	}
}



if (!is_dir($aConfig['common']['base_path']."images/photos/helion_okladki")) {
		mkdir($aConfig['common']['base_path']."images/photos/helion_okladki");
	}
	//sendInfoMail('Helion',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto aktualizacje Helion");
	// usuniecie pliku
	//@unlink('XML/lista-katalog.xml');
	//dump($aCats);
	
	echo "Onepress\n";
	/*
	 * TODO ODKOMENTOWAĆ !!!
	 */
	if(!downloadHelionXML("http://onepress.pl/plugins/xml/lista2.xml","onepress-produkty.xml")){
		sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z onepress.pl");
		die("błąd pobierania xml produktow");
	}
	parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/onepress-produkty.xml",'onepress');

	
	echo "Sensus\n";
	/*
	 * TODO ODKOMENTOWAĆ !!!
	 */
	if(!downloadHelionXML("http://sensus.pl/plugins/xml/lista2.xml","sensus-produkty.xml")){
		sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z sensus.pl");
		die("błąd pobierania xml produktow");
	}
	parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/sensus-produkty.xml",'sensus');

	echo "Septem\n";
	
	
	/*
	 * TODO ODKOMENTOWAĆ !!!
	 */
	if(!downloadHelionXML("http://septem.pl/plugins/xml/lista2.xml","septem-produkty.xml")){
		sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z septem.pl");
		die("błąd pobierania xml produktow");
	}
	parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/septem-produkty.xml",'septem');
	
	echo "Helion\n";
	/*
	 * TODO ODKOMENTOWAĆ !!!
	 */
	if(!downloadHelionXML("http://helion.pl/plugins/xml/lista2.xml","helion-produkty.xml")){
		sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z helion.pl");
		die("błąd pobierania xml produktow");
	}
	parseProductsFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/helion-produkty.xml",'helion');
	
dump($oCommonSynchro->aErrorLog);
dump($oCommonSynchro->aLog);
	//if(!empty($sLogInfo))
		sendInfoMail("Aktualizacja produktów Helion","Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." zakończono aktualizacje Helion\n".$sLogInfo.print_r($oCommonSynchro->aErrorLog,true).print_r($oCommonSynchro->aLog,true));
echo 'KONIEC';
?>