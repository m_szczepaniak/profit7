<?php
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');

	$sSql = "SELECT phrase, counter 
					 FROM ".$aConfig['tabls']['prefix']."search_phrases 
					 ORDER BY counter DESC LIMIT 0,1000"; 
	$aPhrases =& Common::GetAll($sSql);
	
	if(!empty($aPhrases)){
		Common::BeginTransaction();
		$sSql="TRUNCATE TABLE ".$aConfig['tabls']['prefix']."search_suggestions";
		Common::Query($sSql);
		foreach($aPhrases as $key => $aValue){
			Common::Insert($aConfig['tabls']['prefix']."search_suggestions", $aValue, '', false);
		}
		Common::CommitTransaction();
	}
	
?>