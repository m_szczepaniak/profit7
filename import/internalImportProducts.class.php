<?php
/**
 * Klasa importu danych z magazynów wewnetrznych
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
//error_reporting(E_ALL); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');	


$bTestMode = true; //  czy test mode
$iLimitIteration = 1000000000;
$sSource = 'internal';
$sElementName = 'products';
$oImportCSVController = new importXMLController($sSource, $sElementName, $bTestMode, $iLimitIteration);

// pobieramy na dzień dobry wszystkie produkty
$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
/*  
if ($bTestMode == false) {
	
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/internal/modified_products.csv';
}
	*/ 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseXMLFile($sFilename, 'Kartoteki', 'KARTOTEKA');
	
	dump($oImportCSVController->aErrorLog);
	dump($oImportCSVController->oSourceElements->aErrorLog);
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}
@unlink($sFilename);

if (!empty($oImportCSVController->aErrorLog) || !empty($oImportCSVController->oSourceElements->aErrorLog)) {
  $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
                                                        print_r($oImportCSVController->oSourceElements->aErrorLog, true));
}
	 
// TODO mail z informacją o imporcie

echo 'koniec';

