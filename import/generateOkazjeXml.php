<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');

function getProductAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.', ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-2);
  }
  return $sAuthors;
}
	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	} // end of getTarrifPrice()
	
	function getProductCat($iId) {
	global $aConfig;
	$sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";
	return Common::GetRow($sSql);
}

function getCategoryParent($iId) {
	global $aConfig;
	
	$sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return (!empty($aItem['name'])?$aItem['name'].'/':'');
}


function getProductImg($iId) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
	
		if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
			return $sDir.'/__b_'.$aImg['photo'];
		}
		else {
			if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
				return $sDir.'/'.$aImg['photo'];
			}
		}
	}
	return '';
} // end of getItemImage() function


  /**
   * Metoda pobiera opisy dodawane do tytułu produktu - dostępność
   * 
   * @return type
   */
  function getShipmentTimes(){

    $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
    return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
  }// end of getShipmentTimes() method


  $aShipmentTimes = getShipmentTimes();

	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 LIMIT 1";
	$aSite =& Common::GetRow($sSql);
	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn, A.azymut_index,  A.pages, A.edition, A.shipment_time, A.price_brutto, A.shipment_time, A.description, B.name AS publisher_name,
									A.city, A.volume_name, A.volume_nr, A.publication_year
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id

					 WHERE A.published = '1'
					 			AND (A.prod_status = '1' OR A.prod_status = '3')
					 			AND (A.shipment_time = '0' OR A.shipment_time='1')
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
		
	if (!($fp = fopen($_SERVER['DOCUMENT_ROOT'].'/okazje.xml', 'w'))) {
   echo "Nie mogę otworzyc pliku okazje.xml do zapisu";
   die();
	} else {
	
	$sXml = '<?xml version="1.0" encoding="UTF-8" ?>
<okazje>
	<offers>
';
	fwrite($fp,$sXml);

	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		$aCat = getProductCat($aItem['id']);
		if(!empty($aCat['name'])){
			$sCatFullPath = getCategoryParent($aCat['id']).str_replace('/ ','',$aCat['name']);
			$fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
			$sAuthors = getProductAuthors($aItem['id']);
			$sImg = getProductImg($aItem['id']);

	
			fwrite($fp, "	<offer>\n");
			fwrite($fp, "		<id>".$aItem['id']."</id>\n");
			fwrite($fp, "		<name><![CDATA[".htmlspecialchars($aItem['plain_name'],ENT_QUOTES,'UTF-8',false).$aShipmentTimes[$aItem['shipment_time']]."]]></name>\n");
			fwrite($fp, "		<description><![CDATA[".htmlspecialchars(strip_tags(clearAsciiCrap($aItem['description'])),ENT_QUOTES,'UTF-8',false)."]]></description>\n");
			fwrite($fp, "		<category><![CDATA[".htmlspecialchars($sCatFullPath,ENT_QUOTES,'UTF-8',false)."]]></category>\n");
			fwrite($fp, "		<price>".$fTarrifPrice."</price>\n");
			fwrite($fp, "		<url><![CDATA[".htmlspecialchars($aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name']),ENT_QUOTES,'UTF-8',false)."]]></url>\n");
			if(!empty($sImg)){
				fwrite($fp, "		<image><![CDATA[".htmlspecialchars($aConfig['common']['base_url_http'].$sImg,ENT_QUOTES,'UTF-8',false)."]]></image>\n");
			}
			
			if(!empty($aItem['publisher_name'])){
				fwrite($fp, "		<attribute name=\"wydawnictwo\">".htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false)."</attribute>\n");
			}
			
			fwrite($fp, "		<attribute name=\"ISBN\">".$aItem['isbn']."</attribute>\n");
			
			if(!empty($aItem['azymut_index'])){
				fwrite($fp, "		<attribute name=\"osdw\">".$aItem['azymut_index']."</attribute>\n");
			}
			if(!empty($sAuthors)){
				fwrite($fp, "		<attribute name=\"autor\">".htmlspecialchars($sAuthors,ENT_QUOTES,'UTF-8',false)."</attribute>\n");
			}
			if(!empty($aItem['pages'])){
				fwrite($fp, "		<attribute name=\"ilosc_stron\">".$aItem['pages']."</attribute>\n");
			}
			if(!empty($aItem['publication_year'])){
				fwrite($fp, "		<attribute name=\"rok_wydania\">".$aItem['publication_year']."</attribute>\n");
			}
			if(!empty($aItem['binding'])){
				fwrite($fp, "		<attribute name=\"oprawa\">".htmlspecialchars($aItem['binding'],ENT_QUOTES,'UTF-8',false)."</attribute>\n");
			}

			fwrite($fp, "	</offer>\n");
			fflush($fp);
		}
	}
	
	$sXml = '	</offers>
</okazje>';
	fwrite($fp,$sXml);
	fclose($fp);
	}
	
/*
<?xml version="1.0" encoding="UTF-8" ?>
<okazje>
	<offers>
		<offer>
			<id>73</id>
			<name><![CDATA[Kolorowy świat przedszkolaka]]></name>
			<description><![CDATA[Jeśli chcesz dowiedzieć się, kto wyruszył w podróż za trzy grosze, jakie warzywa
			spotkały się na urodzinach marchewki, kto porwał czapkę lalce i co zgubiła w lesie wiewiórka, sięgnij po
			Kolorowy świat przedszkolaka.]]></description>
			<category><![CDATA[Ksiazki/informatyka]]></category>
			<price>12.50</price>
			<url>http://www.twoj_sklep.pl/products_id=73</url>
			<attribute name=”autor”>Czyżewska Małgorzata (oprac.)</attribute>
			<attribute name=”ISBN”>9788374354271</attribute>
			<attribute name=”ilosc_stron”>48</attribute>
			<attribute name=”wydawnictwo”>Znak</attribute>
			<image>http://www.twoj_sklep.pl/images/73.jpg</image>
		</offer>
	</offers>
</okazje>

*/
?>