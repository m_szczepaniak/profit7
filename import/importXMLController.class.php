<?php
/**
 * Kontroler importu XML z dowolnego źródła do Profit24.pl
 *
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 *
 * @property sourceElements $oSourceElements
 * @property Module_Common $oProductsCommon
 */

include_once($_SERVER['DOCUMENT_ROOT'] . '/import/CommonSynchro.class.php');

class importXMLController extends CommonSynchro
{
    private $iLimitIteration;
    public $bTestMode;
    public $aLog = array();
    public $aErrorLog = array();

    public $oSourceElements = NULL;
    public $oProductsCommon = NULL;
    public $sFile;


    public function __construct($sSource, $sElementName, $bTestMode, $iLimitIteration = 0)
    {
        $this->iLimitIteration = $iLimitIteration;
        $this->bTestMode = $bTestMode;

        $sSourceElementsClassName = $sElementName . "Elements";
        include_once($_SERVER['DOCUMENT_ROOT'] . '/import/elements/' . $sSource . '/' . $sSourceElementsClassName . ".class.php");
        $this->oSourceElements = new $sSourceElementsClassName();

        include_once('modules/m_oferta_produktowa/Module_Common.class.php');
        $this->oProductsCommon = new Module_Common();
    }

    public function __destruct()
    {

        if ($this->bTestMode == false && isset($this->sFile) && $this->sFile != '') {
            @unlink($this->sFile);
        }
    }

    private function setFile($sFile)
    {
        $this->sFile = $sFile;
        return $this;
    }

    /**
     * Metoda parsuje plik XML
     *
     * @param string $sXMLFile
     * @return boolean
     */
    public function parseXMLFile($sXMLFile, $sBooksTagName, $sBookTagName, $sXMLEncode = 'UTF-8')
    {
        $this->setFile($sXMLFile);

        if (file_exists($sXMLFile)) {
            $oXml = new XMLReader();
            $oXml->open($sXMLFile, $sXMLEncode, LIBXML_PARSEHUGE);

            while ($oXml->read()) {
                switch ($oXml->nodeType) {
                    case XMLReader::END_ELEMENT :
                        switch ($oXml->name) {
                            case $sBooksTagName:
                                // KONIEC XMLa
                                $oXml->close();
                                break;
                        }
                        break;

                    case XMLReader::ELEMENT :
                        switch ($oXml->name) {
                            case $sBooksTagName:
                                // lecimy po książkach
                                $this->parseItem($oXml, $sBookTagName);
                                break;
                        }
                        break;
                }
            }
        } else {
            $this->addErrorLog("Brak pliku XML '" . $sXMLFile . "'");
            // brak XML'a
            return false;
        }
    }// end of parseXMLFile() method


    /**
     * Metoda parsuje książki po jednej
     *
     * @param type $oXml
     * @return type
     */
    private function parseItem(&$oXml, $sBookTagName)
    {
        $aValues = array();
        $iIteration = 0;
        $iAdded = 0;
        $iUpdated = 0;
        $iSourceBId = ''; // okazało się że lepiej jeśli będzie to string

        $elementsParserHasPostParseMethod =  method_exists($this->oSourceElements, 'postParseElement');

        // czyścimy na wstępnie dane ksiażki
        $this->oSourceElements->clearProductData();
        while (@$oXml->read()) {
            switch ($oXml->nodeType) {
                case XMLReader::END_ELEMENT:
                    switch ($oXml->name) {
                        case $sBookTagName:
                            $iPId = null;
                            if ($iSourceBId != '') {
                                // ksiazka - KONIEC - dodanie do bazy
                                $aProduct = $this->oSourceElements->postParse(); // dorzuca dodatkowe dane po wybraniu innych

                                if (is_array($aProduct) && !empty($aProduct) && $aProduct !== false) {
                                    $iPId = $this->oSourceElements->checkExists($aProduct);
                                    if ($iPId <= 0) {
                                        // Produkt nie istnieje
                                        // echo 'INSERT - '.$aProduct['_id'];
                                        // dump($aProduct);
                                        $bRes = $this->oSourceElements->proceedInsert($this, $aProduct, $iSourceBId);
                                        if ($bRes == true) {
                                            $iAdded++;
                                        }
                                    } else {
                                        // Produkt istnieje
                                        // XXX TODO update - następna iteracja
                                        // echo 'UPDEJT - '.$iPId. ' - '.$aProduct['_id'];
                                        $this->oSourceElements->proceedUpdate($this, $aProduct, $iSourceBId, $iPId);
                                        $iUpdated++;
                                    }

                                    $this->tryAddWeightBySource($iPId, $aProduct, $this->oSourceElements->iSourceId);

                                    unset($iSourceBId);
                                    $this->oSourceElements->clearProductData();
                                } elseif ($aProduct === -1) {
                                    // wygłuszona informacja
                                } else {
                                    // echo 'uciekamy - produkt nie spełnia podstawowych warunków';
                                    $this->oSourceElements->clearProductData();
                                    $this->addErrorLog("Produkt o id źródła: " . $iSourceBId . " nie spełnia podstawowych warunków");
                                    unset($iSourceBId);
                                }

                                // koniec parsowania ksiazki
                                if (true == $elementsParserHasPostParseMethod) {

                                    $aProduct['pid'] = $iPId;

                                    $this->oSourceElements->temptoProceedAtTheEndOfScript[] = $aProduct;
                                }
                            }
                            if ($this->bTestMode == true) {
                                print "\r Przetworzono " . ($iIteration + 1) . " produktów | Dodano do bazy: $iAdded | Zaktualizowano: $iUpdated";
                            }
                            $iIteration++; // licznik iteracji
                            if ($this->bTestMode == true && $this->iLimitIteration <= $iIteration) {
                                return;
                            }
                            break;
                    }
                    break;

                case XMLReader::ELEMENT:

                    if (!empty($this->oSourceElements->aProduct['_id']) != '' && empty($iSourceBId)) {
                        $iSourceBId = $this->oSourceElements->aProduct['_id'];
                    }
                    if (!empty($this->oSourceElements->aElement['_id']) != '' && empty($iSourceBId)) {
                        $iSourceBId = $this->oSourceElements->aElement['_id'];
                    }

                    // wyłuskaj z danych z XML dane książki
                    if ($this->shellItemData($oXml, $iSourceBId, ($sBookTagName === $oXml->name)) === false) {
                        unset($iSourceBId);
                        $this->oSourceElements->clearProductData();
                        continue;
                    }
                    break;
            }
        }
    }// end of parseItem() method

    private function tryAddWeightBySource($pId, $aProduct, $sourceId)
    {
        global $pDbMgr;

        $weight = $aProduct['weight'];

        if (null == $pId || empty($pId)) {

            $pId = $this->oSourceElements->checkExists($aProduct);

            if (null == $pId) {

                return;
            }
        }

        if (null == $sourceId || empty($aProduct) || empty($weight) || null == $weight) {
            return;
        }

        if(!in_array($sourceId, [7, 11, 8])) {
            return;
        }

        $existId = $pDbMgr->GetOne('profit24', "SELECT id FROM product_weights WHERE source_id = $sourceId AND product_id = $pId");

        if (null != $existId) {
            return;
        }

        $pDbMgr->Insert('profit24', 'product_weights', [
            'source_id' => $sourceId,
            'weight' => $weight,
            'product_id' => $pId,
        ]);
    }

    /**
     * Metoda wyłusuje dane z XML'a
     *
     * @param type $oXml
     */
    private function shellItemData(&$oXml, $iSourceBId, $bMainTag)
    {

        $sTagName = $oXml->name;
        if (isset($sTagName) && $sTagName != '' && in_array($sTagName, $this->oSourceElements->aSupportedTags)) {

            // tag jest wspierany
            $sSetMethodName = 'set' . ucfirst(mb_strtolower(str_replace('-', '', $sTagName), 'UTF-8'));
            // XXX TODO sprawdzić jeszcze czy metoda nie istnieje jeśli nie to wywalić błąd
            if (method_exists($this->oSourceElements, $sSetMethodName) === true) {
                if ($this->oSourceElements->$sSetMethodName($oXml, $bMainTag) === false) {
                    $this->addErrorLog("Wystąpił błąd w metodzie : " . $sSetMethodName .
                        " dla produktu o id : " . $iSourceBId .
                        " o ISBN : " . $this->oSourceElements->aProduct['isbn']);
                    // uciekamy, nie dodajemy tego produktu coś jest z nim nie tak
                    return false;
                }
            } else {
                $this->addErrorLog("Brak metody : " . $sSetMethodName .
                    " w objekcie oSourceElements " .
                    " dla produktu o id : " . $iSourceBId .
                    " o ISBN : " . $this->oSourceElements->aProduct['isbn']);
                // uciekamy, nie dodajemy tego produktu coś jest z nim nie tak
                return false;
            }
        } else {
            $this->addErrorLog("Nieobsługiwany tag '" . $sTagName . "' produktu o id " . $iSourceBId);
            // produkt posiada nieobsługiwany TAG
            return false;
        }
    }// end of shellItemData() method
}// end of importXMLController() class
