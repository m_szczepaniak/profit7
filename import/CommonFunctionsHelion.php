<?php
/**
 * Zbór wspólnych funkcji dla Heliona
 * 
 */

/**
 * Funkcja pobiera id serii
 *
 * @global type $aConfig
 * @param type $sHelionSeriesName
 * @param type $iPublisherId
 * @return type 
 */
function getOneSeries($sHelionSeriesName, $iPublisherId) {
	global $aConfig,$pDB;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_series
					 WHERE name=".$pDB->quoteSmart(stripslashes($sHelionSeriesName)). " AND publisher_id=".$iPublisherId;
	return Common::GetOne($sSql);
}// end of checkSeries() method


/**
 * Returns series id for given helion series id
 */
function getHelionSeries($iId,$sBookstore,$iPublisherId) {
	global $aConfig,$pDB;
	
	$sSql = "SELECT name
					 FROM ".$aConfig['tabls']['prefix']."products_helion_series
					 WHERE source = '".$sBookstore."'
					 AND helion_id = ".$iId;	
	$sHelionSeriesName =  Common::GetOne($sSql);
	if($sHelionSeriesName != '') {
		if ($iPublisherId > 0) {
			$sSql="SELECT id FROM products_series_import
					 WHERE publisher_id = ".$iPublisherId."
					 AND name = ".$pDB->quoteSmart(stripslashes($sHelionSeriesName));
			$iId=Common::GetOne($sSql);
		}
		
		if ($iId > 0) {
			$sSql='SELECT series_id FROM '.$aConfig['tabls']['prefix'].'products_series_mappings WHERE series_import_id='.$iId;
			$iId=Common::GetOne($sSql);
		} else {
			unset($iId);
		}
		
		if($iId > 0)
			return $iId;
		
		
		$aValues=array(
			'name'=>$sHelionSeriesName,
			'publisher_id' => $iPublisherId,
			'created'=> 'NOW()',
			'created_by' => 'import'
		);
		$iSeriesId = getOneSeries($aValues['name'], $aValues['publisher_id']);
		if (intval($iSeriesId) > 0) {
			return $iSeriesId;
		} else {
			if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_series", $aValues)) === false) {
				echo date('d.m.Y')." ".date('H:i:s')." error adding Series ".$sHelionSeriesName."\n";
				$sLogInfo.= "error adding Series ".$sHelionSeriesName."\n";
				dump($aValues);
				return false;
			}
			else {
				$sLogInfo.= "dodano serie ".$sHelionSeriesName."\n";
				return $iId;
			}	
		}
	}
} // end of getCategoriesMappings() function

/**
 * Metoda sprawdza istnienie identycznego mapowania
 *
 * @global type $aConfig
 * @param type $iPId
 * @param type $iSId
 * @return type 
 */
function checkMappingExists($iPId, $iSId) {
	global $aConfig;
	
	$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_to_series
					 WHERE product_id=".$iPId." AND series_id=".$iSId;
	return (intval(Common::GetOne($sSql)) > 0 ? true : false);
}// end of checkMappingExists() function

/**
 * Funkcja dodaje serie oraz mapowanie produkt - seria
 *
 * @global type $aConfig
 * @global string $sLogInfo
 * @param type $iPId
 * @param type $iHelionSeriesId
 * @param type $sBookstore
 * @param type $iPublisherId
 * @return boolean 
 */
function addSeries($iPId,$iHelionSeriesId,$sBookstore,$iPublisherId){
global $aConfig,$sLogInfo;

	/*
		// XXX TODO usunac;
	$sSql = "DELETE FROM products_to_series WHERE product_id=".$iPId;
	Common::Query($sSql);
	*/

	$aValues=array(
		'product_id' => $iPId,
		'series_id' => getHelionSeries($iHelionSeriesId,$sBookstore,$iPublisherId)
	);
	if (intval($aValues['series_id']) <= 0) {
		dump('brak serii w produkcie '.$iPId);
		return true;
	}
	if (checkMappingExists($aValues['product_id'], $aValues['series_id']) == false) {
		if (Common::Insert($aConfig['tabls']['prefix']."products_to_series", $aValues,'',false) === false) {
			echo date('d.m.Y')." ".date('H:i:s')." błąd dodawania serii do produktu ".$iPId."<br>\n";
			$sLogInfo.= "błąd dodawania serii do produktu ".$iPId."\n";
			dump($aValues);
			return false;
		}
		return true;
	} else {
		// mamy mapowania
		return true;
	}
}
?>