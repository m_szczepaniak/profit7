<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');

function getProductAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.', ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-2);
  }
  return $sAuthors;
}
	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	} // end of getTarrifPrice()
	
	function getProductCat($iId) {
	global $aConfig;
	$sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";
	return Common::GetRow($sSql);
}

function getCategoryParent($iId) {
	global $aConfig;
	
	$sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return (!empty($aItem['name'])?$aItem['name'].'/':'');
}



function getShipmentTime($iShipment){
	switch(intval($iShipment)){
		case 0:
			return '24 godziny';
			break;
		case 1:
			return '2 dni';
			break;
		case 2:
			return '9 dni';
			break;
		case 3:
			return '';
			break;
	}
}

function getProductImg($iId) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
	
		if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
			return $sDir.'/__b_'.$aImg['photo'];
		}
		else {
			if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
				return $sDir.'/'.$aImg['photo'];
			}
		}
	}
	return '';
} // end of getItemImage() function



	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 LIMIT 1";
	$aSite =& Common::GetRow($sSql);
	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn, A.azymut_index,  A.pages, A.edition, A.shipment_time, A.price_brutto, A.shipment_time, A.description, B.name AS publisher_name,
									L.binding, I.language, J.language AS original_language, A.city, A.volume_name, A.volume_nr, A.publication_year, A.name2, E.name AS series_name
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 	LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages I
						 ON A.language=I.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages J
						 ON A.original_language=J.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings L
						 ON A.binding=L.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series D
						 ON D.product_id=A.id
	  					LEFT JOIN ".$aConfig['tabls']['prefix']."products_series E
	  					 ON E.id=D.series_id
					 WHERE A.published = '1'
					 			AND (A.prod_status = '1' OR A.prod_status = '3') 
					 			AND (A.shipment_time = '0' OR A.shipment_time='1')
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
		
	if (!($fp = fopen($_SERVER['DOCUMENT_ROOT'].'/nokaut.xml', 'w'))) {
   echo "Nie mogę otworzyc pliku nokaut.xml do zapisu";
   die();
	} else {
	
	$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE nokaut SYSTEM "http://www.nokaut.pl/integracja/nokaut.dtd">
<nokaut>
	<offers>
';
	fwrite($fp,$sXml);

	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		$aCat = getProductCat($aItem['id']);
		if(!empty($aCat['name'])){
			$sCatFullPath = getCategoryParent($aCat['id']).str_replace('/ ','',$aCat['name']);
			$fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
			$sAuthors = getProductAuthors($aItem['id']);
			$sImg = getProductImg($aItem['id']);

	
			fwrite($fp, "	<offer>\n");
			fwrite($fp, "		<id>".$aItem['id']."</id>\n");
			fwrite($fp, "		<name><![CDATA[".htmlspecialchars($aItem['plain_name'],ENT_QUOTES,'UTF-8',false)."]]></name>\n");
			fwrite($fp, "		<description><![CDATA[".htmlspecialchars(strip_tags(clearAsciiCrap($aItem['description'])),ENT_QUOTES,'UTF-8',false)."]]></description>\n");
			fwrite($fp, "		<url><![CDATA[".htmlspecialchars($aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name']),ENT_QUOTES,'UTF-8',false)."]]></url>\n");
			if(!empty($sImg)){
				fwrite($fp, "		<image><![CDATA[".htmlspecialchars($aConfig['common']['base_url_http'].$sImg,ENT_QUOTES,'UTF-8',false)."]]></image>\n");
			}
			fwrite($fp, "		<price>".$fTarrifPrice."</price>\n");
			fwrite($fp, "		<category><![CDATA[".htmlspecialchars($sCatFullPath,ENT_QUOTES,'UTF-8',false)."]]></category>\n");
			if(!empty($aItem['publisher_name'])){
				fwrite($fp, "		<producer><![CDATA[".htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false)."]]></producer>\n");
			}
			
			fwrite($fp, "		<property name=\"ean\">".$aItem['isbn']."</property>\n");
			if(!empty($aItem['azymut_index'])){
				fwrite($fp, "		<property name=\"osdw\">".$aItem['azymut_index']."</property>\n");
			}
			if(!empty($sAuthors)){
				fwrite($fp, "		<property name=\"autor\"><![CDATA[".htmlspecialchars($sAuthors,ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			if(!empty($aItem['pages'])){
				fwrite($fp, "		<property name=\"liczba stron\">".$aItem['pages']."</property>\n");
			}
			if(!empty($aItem['edition'])){
				fwrite($fp, "		<property name=\"numer wydania\">".$aItem['edition']."</property>\n");
			}
			if(!empty($aItem['publication_year'])){
				fwrite($fp, "		<property name=\"rok wydania\">".$aItem['publication_year']."</property>\n");
			}
			if(!empty($aItem['name2'])){
				fwrite($fp, "		<property name=\"podtytuł\"><![CDATA[".htmlspecialchars($aItem['name2'],ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			if(!empty($aItem['binding'])){
				fwrite($fp, "		<property name=\"oprawa\"><![CDATA[".htmlspecialchars($aItem['binding'],ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			if(!empty($aItem['language'])){
				fwrite($fp, "		<property name=\"język\"><![CDATA[".htmlspecialchars($aItem['language'],ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			if(!empty($aItem['original_language'])){
				fwrite($fp, "		<property name=\"język oryginału\"><![CDATA[".htmlspecialchars($aItem['original_language'],ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			
			if(!empty($aItem['city'])){
				fwrite($fp, "		<property name=\"miejsce wydania\"><![CDATA[".htmlspecialchars($aItem['city'],ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			if(!empty($aItem['volume_name'])){
				fwrite($fp, "		<property name=\"tytuł tomu\"><![CDATA[".htmlspecialchars($aItem['volume_name'],ENT_QUOTES,'UTF-8',false)."]]></property>\n");
			}
			if(!empty($aItem['volume_nr'])){
				fwrite($fp, "		<property name=\"numer kolejnego tomu\">".$aItem['volume_nr']."</property>\n");
			}
			if(!empty($aItem['series_name'])){
				fwrite($fp, "		<property name=\"seria wydawnicza\"><![CDATA[".$aItem['series_name']."]]></property>\n");
			}
			
      fwrite($fp, "		<availability>".$aItem['shipment_time']."</availability>\n");
		
			fwrite($fp, "	</offer>\n");
			fflush($fp);
		}
	}
	
	$sXml = '</wpoffers>';
	fwrite($fp,$sXml);
	fclose($fp);
	}
	
/*
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE nokaut SYSTEM "http://www.nokaut.pl/integracja/nokaut.dtd">
<nokaut generator=”sote” ver=”5.0”>
	<offers>
		<offer>
			<id>1</id>
			<name><![CDATA[Canon EOS 500D]]></name>
			<description><![CDATA[Doskonały następca modelu Canon 450D. Model 500D wyposażony został w nowoczesną matrycę o rozdzielczości 15.1mln, rozszerzony zakres czułości 100-3200 (12.800) oraz możliwośd nagrywania filmów w rozdzielczości Full HD.]]></description>
			<url><![CDATA[http://www.example.org/produkt1.html]]></url>
			<image><![CDATA[http://www.example.org/img/produkt1.jpg]]></image>
			<price>2425.90</price>
			<category><![CDATA[Aparaty cyfrowe / Lustrzanki cyfrowe]]></category>
			<producer><![CDATA[Canon]]></producer>
			<property name=”EAN”> 871 4574 535326</property>
			<property name=”rozdzielczośd”><![CDATA[15 Mpx]]></property>
			<promo><![CDATA[Zamawiając ten produkt przed 31.12.2010 dostaniesz 2 bilety do kina gratis]]></promo>
			<availability><![CDATA[dostępny od ręki]]></availability>
			<warranty><![CDATA[Gwarancja producenta 2 lata od daty zakupu]]></warranty>
		</offer>
		<offer>
			<id>2</id>
			<name><![CDATA[Canon IP 4200]]></name>
			<description><![CDATA[Nowa drukarka Canon PIXMA iP4200: doskonała jakośd, doskonała cena]]></description>
			<url><![CDATA[http://www.example.org/produkt2.html]]></url> <image><![CDATA[http://www.example.org/img/produkt2.jpg]]></image> <price>400.45</price>
			<category><![CDATA[Drukarki / Drukarki atramentowe]]></category>
			<producer><![CDATA[Canon]]></producer>
			<promo><![CDATA[Do każdej drukarki zakupionej do kooca marca - ryza papieru gratis]]></promo>
			<availability>1</availability>
			<warranty><![CDATA[Gwarancja producenta 12 miesięcy od daty zakupu]]></warranty>
		</offer> <offer>
			<id>3</id> <name><![CDATA[Nokia 6610i]]></name>
			<description><![CDATA[<strong>Telefon Nokia 6610i</strong> przeznaczony dla biznesu i wymagających, który posiada funkcje aparatu i wysyłania MMS]]></description>
			<url><![CDATA[http://www.example.org/produkt3.html]]></url>
			<image><![CDATA[http://www.example.org/img/produkt3.jpg]]></image> <price>15.97</price>
			<category><![CDATA[Telefony / Telefony komórkowe]]></category>
			<producer><![CDATA[Nokia]]></producer>
			<promo><![CDATA[Do każdego telefonu Nokia dołączamy smycz z logo naszego sklepu.]]></promo>
			<availability>0</availability>
			<warranty><![CDATA[24 miesiące]]></warranty>
		</offer>
	</offers>
</nokaut>

*/
?>