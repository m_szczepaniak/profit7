<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 18.08.17
 * Time: 13:49
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');

/**
 *
 * Do dom i ogród: - zaczynające się od:
 * Artykuły dekoracyjne
 * Przemysłowe, Wyposażenie
 * Artykuły domowe
 * Spożywcze, Higiena
 *
 *
 * Do Sport i turystyka - zaczynające się od:
 * Squash
 *
 *
 * Do Artykuly szkolne i biurowe: - zaczynające się od:
 * Akcesoria Biurowe
 * Artykuły Papiernicze
 * Artykuły piśmiennicze
 * Artykuły plastyczne
 * Artykuły szkolne
 * Galanteria szkolna
 * Papeteria
 * Przechowywanie dokumentów
 * Sprzęt Biurowy
 * Wyposażenie biura
 *
 */

$aMove['2646'] = [
    '2797',
    '2424',
    '2796',
    '2423',
];

$aMove['2657'] = [
    '2462',
];

$aMove['2656'] = [
    '2422',
    '2420',
    '2802',
    '2798',
    '496',
    '2803',
    '462',
    '2799',
    '2419',
    '2800',
];

function slug($z){
    $z = strtolower($z);
    $z = preg_replace('/[^a-z0-9 -]+/', '', $z);
    $z = str_replace(' ', '-', $z);
    return trim($z, '-');
}

function addCategory($menuId, $parentId, $sName)
{
    $sSql = "SELECT IFNULL(MAX(order_by), 0) + 1
							 FROM menus_items
							 WHERE language_id = 1 AND
							 			 menu_id = ".$menuId." AND
							 			 parent_id ".($parentId == 0 ? "IS NULL" : "= ".$parentId);
    $iOrderBy = Common::GetOne($sSql);
    if ($iOrderBy > 65535) {
        $iOrderBy = 65535;
    }

    $sSymbol = slug($sName);
    $aValues = array(
        'id' => '#id#',
        'menu_id' => $menuId,
        'language_id' => 1,
        'parent_id' => $parentId > 0 ? $parentId : 'NULL',
        'module_id' => '26',
        'link_to_id' => 'NULL',
        'symbol' => $sSymbol,
        'name' => $sName,
        'mtype' => '0',
        'menu' => '0',
        'template' => 'NULL',
        'description' => 'NULL',
        'seo_title' => 'NULL',
        'seo_keywords' => 'NULL',
        'url' => 'NULL',
        'new_window' => '0',
        'restricted' => '0',
        'order_by' => $iOrderBy,
        'moption' => 'NULL',
        'published' => '1',
        'created' => 'NOW()',
        'created_by' => 'automat',
    );
    if (($iNewId = Common::Insert("menus_items",
            $aValues,
            "language_id = 1")) === false) {
        return false;
    }

    $a = 1;
    return $iNewId;
}

/**
 * Metoda dodaje mapowania dla źródła
 *
 * @global array $aConfig
 * @param integer $iPageId
 * @param integer $iSourceId
 * @param integer $iSCId
 * @return boolean
 */
function AddSourceMapping($iPageId, $iSourceId, $iSCId) {
    global $aConfig;

    $aValues=array(
        'page_id' => $iPageId,
        'source_id' => $iSourceId,
        'source_category_id' => $iSCId
    );
    if ((Common::Insert($aConfig['tabls']['prefix']."menus_items_mappings", $aValues, '', false)) === false) {
        return false;
    }
    return true;
}// end of AddSourceMapping() method

function getChilds(&$categories, $iPageIdParent, $menuId)
{
    foreach ($categories as $iKey => $category) {
        $iPageIdParent1 = addCategoryAndMapping($category, $menuId, $iPageIdParent);

        $sSql = 'SELECT * FROM menus_items_mappings_categories WHERE source_id = 8 AND source_category_parent_id = ' . $category['source_category_id'];
        $childs = Common::GetAll($sSql);
        if (!empty($childs)) {
            foreach ($childs as $iBKey => $child) {
                $iPageIdParent2 = addCategoryAndMapping($child, $menuId, $iPageIdParent1);


                $sSql = 'SELECT * FROM menus_items_mappings_categories WHERE source_id = 8 AND source_category_parent_id = ' . $child['source_category_id'];
                $nextChilds = Common::GetAll($sSql);
                if (!empty($nextChilds)) {
                    foreach ($nextChilds as $iNewKey => $nextChild) {
                        $iPageIdParent3 = addCategoryAndMapping($nextChild, $menuId, $iPageIdParent2);


                        $sSql = 'SELECT * FROM menus_items_mappings_categories WHERE source_id = 8 AND source_category_parent_id = ' . $nextChild['source_category_id'];
                        $lastChilds = Common::GetAll($sSql);
                        if (!empty($lastChilds)) {
                            foreach ($lastChilds as $iDDKey => $aaLastChild) {
                                $iPageIdParent4 = addCategoryAndMapping($aaLastChild, $menuId, $iPageIdParent3);


                                $sSql = 'SELECT * FROM menus_items_mappings_categories WHERE source_id = 8 AND source_category_parent_id = ' . $aaLastChild['source_category_id'];
                                $aaLastChilds = Common::GetAll($sSql);
                                if (!empty($aaLastChilds)) {
                                    foreach ($aaLastChilds as $aaLastChilddddd) {
                                        $iPageIdParent5 = addCategoryAndMapping($aaLastChilddddd, $menuId, $iPageIdParent4);
                                    }

                                    $lastChilds[$iDDKey]['childs'] = $aaLastChilds;
                                }
                            }
                            $nextChilds[$iNewKey]['childs'] = $lastChilds;
                        }
                    }
                    $childs[$iBKey]['childs'] = $nextChilds;
                }
            }
            $categories[$iKey]['childs'] = $childs;
        }
    }
    return $categories;
}

function addCategoryAndMapping($categorySource, $menuId, $parentIdMenusItems) {

    $iPageId = addCategory($menuId, $parentIdMenusItems, $categorySource['name']);
    AddSourceMapping($iPageId, 8, $categorySource['source_category_id']);
    return $iPageId;
}

foreach ($aMove as $moveToCategory => $categories) {
    $sSql = 'SELECT * 
             FROM menus_items_mappings_categories AS DD 
             WHERE 
                source_id = 8 AND 
                source_category_parent_id IS NULL AND
                (
                  SELECT MM.id 
                  FROM menus_items_mappings_categories AS MM 
                  WHERE MM.source_category_parent_id = DD.source_category_id 
                  LIMIT 1
                ) IS NOT NULL AND 
                DD.source_category_id IN ("' . implode('", "', $categories) . '")';
    $categoriesSource = Common::GetAll($sSql);
    foreach ($categoriesSource as $parentIdMenusItems => $categorySource) {
        $sSql = 'SELECT menu_id FROM menus_items WHERE id = '.$moveToCategory;
        $menuId = Common::GetOne($sSql);
        $iPageId = addCategoryAndMapping($categorySource, $menuId, $moveToCategory);


        $sSql = 'SELECT * FROM menus_items_mappings_categories WHERE source_id = 8 AND source_category_parent_id = ' . $categorySource['source_category_id'];
        $categoriesSource[$iKey]['childs'] = Common::GetAll($sSql);

        if (!empty($categoriesSource[$iKey]['childs'])) {
            getChilds($categoriesSource[$iKey]['childs'], $iPageId, $menuId);
        }
    }


    dump($categories);
    dump($categoriesSource);
}