<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once('import_func.inc.php');


function removeImageFile($aValue)
{
    global $aConfig;

    $photoDir = $aConfig['common']['client_base_path'] . 'images/photos/' . $aValue['directory'];

    if (file_exists($photoDir . '/__b_' . $aValue['photo'])) {
        unlink($photoDir . '/__b_' . $aValue['photo']);
        echo 'del: ' . $photoDir . '/__b_' . $aValue['photo'] . "\n";
    }
    if (file_exists($photoDir . '/' . $aValue['photo'])) {
        unlink($photoDir . '/' . $aValue['photo']);
        echo 'del: ' . $photoDir . '/' . $aValue['photo'] . "\n";
    }
    if (file_exists($photoDir . '/__t_' . $aValue['photo'])) {
        unlink($photoDir . '/__t_' . $aValue['photo']);
        echo 'del: ' . $photoDir . '/__t_' . $aValue['photo'] . "\n";
    }
}

function checkIsImage($aValue)
{
    global $aConfig;

    $filename = $aConfig['common']['client_base_path'] . 'images/photos/' . $aValue['directory'] . '/' . $aValue['photo'];

    $imagesizedata = getimagesize($filename);
    if ($imagesizedata === FALSE) {
        return false;
    }
    return true;
}

$sSql = '
SELECT *, PI.id AS id_image, P.id AS p_product_id
FROM products_images AS PI
JOIN products AS P
  ON PI.product_id = P.id
WHERE 
P.created_by = "panda"
AND DATE(PI.created) = "2018-11-16"
ORDER BY P.id DESC
';


$records = Common::PlainQuery($sSql);

// Petla po wszystkich produktach
$i = 0;
while($row =& $records->fetchRow(DB_FETCHMODE_ASSOC)) {
//    if (false === checkIsImage($row)) {
    echo $i."\n";
    $i++;
    removeImageFile($row);
    $sSql = 'DELETE FROM products_images WHERE id = ' . $row['id_image'];
    Common::Query($sSql);
//    }
}

