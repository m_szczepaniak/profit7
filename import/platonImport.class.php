<?php
/**
 * Skrypt importu produktów ze źródła Platon do Profit24.pl
 * 
 * @author Marcin Janowski
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(21600); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importXMLController.class.php');	

$bTestMode = false; //  czy test mode
$iLimitIteration = 200000000000;
$sSource = 'platon';
$sElement = 'products';
$oImportXMLController = new importXMLController($sSource, $sElement, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
  //XML Spakowany rarem
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportXMLController->oSourceElements->importSaveXMLData();
  // $sFilename = __DIR__.'/XML/platon/products_302508_19102015.xml';
} else {
	// link testowy na sztywno aby nie pobierać
  $sFilename = 'XML/platon/products_315209_19102015.xml';
}

if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportXMLController->parseXMLFile($sFilename, 'PRODUCTS', 'rp', 'UTF-8');
	/*
	dump($oImportXMLController->aErrorLog);
	dump($oImportXMLController->oSourceElements->aErrorLog);
	 */
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
	
}

//$sFileName = __DIR__.'/elements/platon/reports/report_err.txt';
//$sData = print_r($oImportXMLController->aErrorLog, true) . print_r($oImportXMLController->oSourceElements->aErrorLog, true);
//file_put_contents($sFileName, $sData);
//
//$sFileName = __DIR__.'/elements/platon/reports/report_ok.txt';
//$sData = print_r($oImportXMLController->aLog, true) . print_r($oImportXMLController->oSourceElements->aLog, true);
//file_put_contents($sFileName, $sData);


$oImportXMLController->sendInfoMail("IMPORT PLATON OK", print_r($oImportXMLController->aLog, true).
																											print_r($oImportXMLController->oSourceElements->aLog, true));
$oImportXMLController->sendInfoMail("IMPORT PLATON ERR", print_r($oImportXMLController->aErrorLog, true).
																											print_r($oImportXMLController->oSourceElements->aErrorLog, true));
 //TODO mail z informacją o imporcie

echo "\n KONIEC \n";

?>
