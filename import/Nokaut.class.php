<?php
/**
 * Klasa Nokaut_XML_Creator
 * 
 * Klasa sluzy do szybkiego i latwego generowania plikow XML
 * Dla porownywarki cen Nokaut.pl, dzieki przemyslanemu mechanizmowi
 * Generowanie pliku XML jest bardzo uniwersalne i proste
 * 
 * @author		Maciej Strączkowski <m.straczkowski@omnia.pl>
 * @package		Exports
 * @copyright	Maciej Strączkowski omnia.pl
 * @version		1.0 <15.10.2011>
 */
class Nokaut_XML_Creator {

	
	private $sFilePath;
	
	/**
	 * Metoda __construct();
	 * 
	 * Metoda zapisuje sciezke do pliku jako wlasciwosc,
	 * Dodatkowo zapisuje do wskazanego pliku naglowki XML
	 * Istnieje takze mozliwosc zmiany kodowania pliku XML
	 * Poprzez podanie kodowania w drugim parametrze
	 * 
	 * @access	public
	 * @param	string	$sFilePath	- Do jakiego pliku ma zostac zapisany xml
	 * @param	string	$sCharset	- Kodowanie pliku xml
	 * @return	boolean
	 */
	public function __construct($sFilePath, $sCharset = 'UTF-8')
	{
		$this->sFilePath = $sFilePath;
		if(file_exists($this->sFilePath)){
			unlink($this->sFilePath);
		}
		$sXmlStart = '
			<?xml version="1.0" encoding="'.$sCharset.'"?>
			<!DOCTYPE nokaut SYSTEM "http://www.nokaut.pl/integracja/nokaut.dtd">
			<nokaut generator="sote" ver="5.0">
			<offers>';
		if(!file_put_contents($this->sFilePath, trim($sXmlStart))){
			die('Nie mozna utworzyc pliku XML - Nie mozna wykonac zapisu');
		}
		return true;
	}//end of __construct() method
	
// --------------------------------------------------------------------
	
	/**
	 * Metoda addItem();
	 * 
	 * Metoda dodaje produkt do generowanego pliku XML
	 * Tworzy segment <offer></offer> oraz wypelnia go przekazanymi danymi
	 * Klucz tablicy $aItems jest nazwa tagu, co zapewnia wysoka uniwersalnosc
	 * 
	 * @example
	 * Array(
	 *		[name] => Prawo i zarzadzanie
	 *		[price] => 105.50
	 * )
	 * Utworzy tagi:
	 * <name><![CDATA[Prawo i zarzadzanie]]></name>
	 * <price>105.50</price>
	 * 
	 * Dodatkowo istnieje mozliwosc podania atrybutow produktu
	 * (Znaczniki <property name=""></property>)
	 * 
	 * @example
	 * Array(
	 *		[isbn] => 98-125-55-41
	 *		[ilość_stron] => 547
	 * )
	 * Utworzy tagi:
	 * <property name="isbn"><![CDATA[98-125-55-41]]></property>
	 * <property name="ilość_stron">547</property>
	 * 
	 * Jezeli wartosc jest stringiem - automatycznie umieszczana jest w <![CDATA[]]>
	 * 
	 * @access	public
	 * @param	array		$aItem			- Tablica przedmiotu
	 * @param	array		$aProporties	- Tablica atrybutow
	 * @return	boolean
	 */
	public function addItem($aItem, $aProporties = null)
	{
		$sOneItem = '<offer>'."\r\n";
		foreach($aItem as $sKey => $sValue){
			if($sValue != ''){
				if(is_numeric($sValue)){
					$sOneItem .= '<'.$sKey.'>'.$sValue.'</'.$sKey.'>'."\r\n";
				} else {
					$sOneItem .= '<'.$sKey.'><![CDATA['.$sValue.']]></'.$sKey.'>'."\r\n";
				}
			}
		}
		if(!is_null($aProporties)){
			foreach($aProporties as $sKey => $sValue){
				if($sValue != ''){
					if(is_numeric($sValue)){
						$sOneItem .= '<property name="'.$sKey.'">'.$sValue.'</property>'."\r\n";
					} else {
						$sOneItem .= '<property name="'.$sKey.'"><![CDATA['.$sValue.']]></property>'."\r\n";
					}
				}
			}
		}
		$sOneItem .= '</offer>'."\r\n";
		file_put_contents($this->sFilePath, $sOneItem, FILE_APPEND);
		return true;
	}//end of addItem() method
	
// --------------------------------------------------------------------
	
	/**
	 * Metoda __destruct();
	 * 
	 * Destruktor konczy plik XML dodajac na koniec pliku tagi zamykajace
	 * Wyswietlana jest sciezka oraz waga pliku na ekran
	 * 
	 * @access	public
	 * @return	boolean
	 */
	public function __destruct()
	{
		if(!file_put_contents($this->sFilePath, '</offers></nokaut>', FILE_APPEND)){
			die('Nie mozna utworzyc pliku XML - Nie mozna wykonac zapisu');
		}
		if(file_exists($this->sFilePath)){
			echo '<p>Generowanie XML zakonczone sukcesem: <br />
					<strong>Plik:</strong> '.$this->sFilePath.'<br />
					<strong>Waga:</strong> '.round(filesize($this->sFilePath)/1024, 2) . ' KB<br />
					<strong>Uzyta pamiec:</strong> '.round(memory_get_usage()/1024, 2).' KB</p>';
		}
		return true;
	}//end of __destruct() method
	
}//end of Nokaut_XML_Creator Class
	
?>
