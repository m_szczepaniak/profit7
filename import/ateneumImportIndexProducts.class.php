<?php
/**
 * Skrypt importu produktów ze źródła Ateneum do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importCSVController.class.php');	

$aColsToFgetcsv = array(
  'ateneum_id',// 123598,
  'EAN',// "9788364057137",
  'ISBN',// "9788364057137",
  'Tytul',// "Opowie¶ci graficzne TW",
  'Autorzy',// "Bernet Jordi, Abuli E.S.",
  'Wydawca',// "Korporacja Ha!Art",
  'Opis_wydania',// "Strony: 184, Format: 24,3x16,8 cm",
  'Opis_wydania2',// "Rok wydania: 2013, oprawa: twarda",
  'Opis',// "Unikatowe historie obrazkowe, stworzone przez Antonisza na przestrzeni czterdziestu lat w różnych momentach jego życia, stanowi± bezcenne uzupełnienie twórczo¶ci artysty. Ksi±żka odzwierciedla różnorodno¶ć tematów i technik narracyjno-graficznych stosowanych przez autora - poczynaj±c od młodzieńczej, 64-stronicowej próby ""Przygody Hitlera"" z 1954 roku, przez obszerny scenopis obrazkowy do niezrealizowanego filmu ""Pierony"" (lata 70.), aż po licz±cy dwadzie¶cia dwie strony fotokomiks ""Jak powstało Muzeum Filmu Non-Camera"" (1984 r.). Wszystkie wymienione realizacje stanowi± ¶wiadectwo nieprzeciętnej wyobraĽni, znakomitego zmysłu obserwacji i surrealistycznego poczucia humoru, wspartych inwencj± formaln±, widoczn± w wykorzystaniu przez artystę szerokiego arsenału ¶rodków plastycznych. ""Realizacja filmów eksperymentalnych metodami grafiki artystycznej poprzez odbijanie grafik bezpo¶rednio na ta¶mie filmowej z pominięciem kamery filmowej jest szans± na stworzenie filmu będ±cego autentycznym dziełem sztuki"" - pisał artysta w swoim ""Manife¶cie artystycznym grupy twórczej Non-Camera"" z 1977 roku. W wyniku interwencji autora film - jako unikatowy obiekt - zyskuje rangę obiektu sztuki, a sama ta¶ma filmowa nabiera cech ""hipergęstej"" sekwencji grafik, w której rozbity na fazy ruch budzi skojarzenia z narracj± komiksow±. Tego rodzaju podej¶cie do materii filmowej pozwala wpisać Antonisza nie tyle do grona ""filmowców"", co raczej intermedialnych, awangardowych artystów, traktuj±cych swoje kinematograficzne realizacje jako czę¶ć większego, interdyscyplinarnego (lub postdyscyplinarnego) projektu. ",
  'Cena_netto',// 56.19,
  'Podatek_opis',// "Podatek VAT 5%",
  'Cena_brutto',// 59.00,
  'kat1',// "Komiksy",
  'kat2',// "Dla dorosłych",
  'kat3',// "",
  'okl_path',// "123598.jpg",
  'hash',// "6f5f430607b2cc20af1d68c31288cd17",
  'empty'
);
// ---
// CONFIG
if ($aConfig['common']['status']	!= 'development') {
    $bTestMode = false; //  czy test mode
    $iLimitIteration = 20000000000;
} else {
    $bTestMode = true; //  czy test mode
    $iLimitIteration = 20;
}
$sSource = 'ateneum';

$sElementName = 'products'; // najpiew autorzy
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration, true);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/ateneum/214111_02092016_product.CSV';
}

if ($sFilename !== false && $sFilename != '') {
	$sFilenameReplace = preg_replace('/(.*)\./', '$1', $sFilename);
    $sNewFilename = $sFilenameReplace.'_utf8.csv';
    exec('iconv -f ISO8859-2 -t UTF-8 -o "'.$sNewFilename.'" '.$sFilename);
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sNewFilename, ',', TRUE, $aColsToFgetcsv);

  if (!empty($oImportCSVController->aLog) || !empty($oImportCSVController->oSourceElements->aLog)) {
    $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
                                                          print_r($oImportCSVController->oSourceElements->aLog, true));
  }
  if (!empty($oImportCSVController->aErrorLog) || !empty($oImportCSVController->oSourceElements->aErrorLog)) {
    $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
                                                          print_r($oImportCSVController->oSourceElements->aErrorLog, true));
  }
  if ($bTestMode == false) {
    @unlink($sFilename);
  }
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}

dump($oImportCSVController->oSourceElements->_countEmptyPublicationYear);
	 
unset($oImportCSVController);