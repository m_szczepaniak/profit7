<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
ini_set('memory_limit', '512M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');

/**
 * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
 *
 * @return string
 */
function libxml_display_error($error)
{
    $return = "<br/>\n";
    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }
    $return .= trim($error->message);
    if ($error->file) {
        $return .=    " in <b>$error->file</b>";
    }
    $return .= " on line <b>$error->line</b>\n";

    return $return;
}


/**
 * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
 *
 * @return string
 */
function libxml_display_errors() {
		$sErr = '';
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        $sErr .= libxml_display_error($error);
    }
    libxml_clear_errors();
		return $sErr;
}// end of libxml_display_errors() function


function getProductAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.', ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-2);
  }
  return $sAuthors;
}
	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto, vat
						FROM ".$aConfig['tabls']['prefix']."products
						WHERE id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getTarrifPrice()
	
	function getProductCats($iId) {
	global $aConfig;
	$sSql = "SELECT A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority";
	return Common::GetCol($sSql);
}

function getCategoryParent($iId) {
	global $aConfig;
	
	$sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return (!empty($aItem['name'])?$aItem['name'].'/':'');
}

function &getProductImg($iId) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
	
		if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
			return $sDir.'/__b_'.$aImg['photo'];
		}
		else {
			if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
				return $sDir.'/'.$aImg['photo'];
			}
		}
	}
	return '';
} // end of getItemImage() function

function getShipmentTime($iShipment){
	switch(intval($iShipment)){
		case 0:
			return 'wysyłamy od ręki';
			break;
		case 1:
			return 'wysyłamy 1-2 dni';
			break;
		case 2:
			return 'wysyłamy 7-9 dni';
			break;
		case 3:
			return 'tylko na zamówienie';
			break;
	}
}
/**
 * Funkcja pobiera czas realizacji i konwertuje na wartości CENEO
 *
 * @param type $iShipment
 * @return type 
 */
function getShipmentTimeCeneo($iShipment){
	/*
	 * 
	 * wartość z Ksiegarni :
		['shipment_0'] = "Od ręki";
		['shipment_1'] = "1 - 2 dni";
		['shipment_2'] = "7 - 9 dni";
		['shipment_3'] = "tylko na zamówienie";
	 * 
	 * 
	 * wartości z Ceneo:
	  1 – dostępny, sklep posiada produkt, 
	  3 – sklep będzie posiadał produkt do 3 dni, 
	  7 – sklep będzie posiadał produkt w ciągu tygodnia, 
	  14 – produkt dostępny nie wcześniej niż za tydzień, 
	  99 lub brak wartości – link prowadzący do informacji w sklepie. Opcjonalnie
	 */
  return $iShipment;
}



function generateElement(&$aItem, $aCat) {
	global $aConfig;
	
	$sElement = '';
	$sCatFullPath = htmlspecialchars(implode('/', $aCat), ENT_QUOTES,'UTF-8',false);
  $aPrice = getTarrifPrice($aItem['id']);
	$fTarrifPrice = Common::formatPrice2($aPrice['price_brutto']);
  $iVat = intval($aPrice['vat']);
			
	$sAuthors = getProductAuthors($aItem['id']);
	
	$sImgSrc = getProductImg($aItem['id']);
	if ($sImgSrc!='') {
		$sImg = $aConfig['common']['base_url_http'].$sImgSrc;
	}
	
	$sPlainName = trimString(htmlspecialchars($aItem['plain_name'],ENT_QUOTES,'UTF-8',false), 150);
	
	// TODO SPRAWDZIĆ set='0'
	$sXML .= '<Product id="'.$aItem['id'].'" '.
							'url="'.$aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name']).'" '.
					    'price="'.$fTarrifPrice.'" '.
              'vat="'.$iVat.'" '.
							'avail="'.getShipmentTimeCeneo($aItem['shipment_time']).'" '.
							($aItem['weight']!=''? ' weight="'.$aItem['weight'].'"' : '') .'>'."\n";
	
	$sXML .= 	'	<cats>'.
							'<![CDATA['.htmlspecialchars($sCatFullPath,ENT_QUOTES,'UTF-8',false).']]>'.
						'</cats>'."\n";
	
	$sXML .= 	'	<name>'.
							'<![CDATA['.$sPlainName.']]>'.
						'</name>'."\n";
	
	if (!empty($sImg)) {
    $sXML .=	'	<img url="'.$sImg.'" />';
	}
	
	$sXML	.=	'	<desc>'.
							'<![CDATA['.htmlspecialchars(stripslashes(strip_tags(clearAsciiCrap($aItem['description']))),ENT_QUOTES,'UTF-8',false).']]>'.
						'</desc>'."\n";
	
	$sXML .=	'	<attrs>'."\n".
							(!empty($sAuthors)?
							'<a name="Autor">'."\n". //<!--name: tekst (długość 512) wymagany-->          
								'<![CDATA['.trimString(htmlspecialchars($sAuthors,ENT_QUOTES,'UTF-8',false), 512).']]>'."\n".
							'</a>'."\n"
							:'').
							'<a name="ISBN">'."\n".
								'<![CDATA['.$aItem['isbn'].']]>'."\n".
							'</a>'."\n".
							(!empty($aItem['pages'])?
							'<a name="Ilosc_stron">'."\n".
								'<![CDATA['.$aItem['pages'].']]>'."\n".
							'</a>'."\n"
							:'').
							(!empty($aItem['publication_year'])?
							'<a name="Rok_wydania">'."\n".
								'<![CDATA['.$aItem['publication_year'].']]>'."\n".
							'</a>'."\n"
							:'').
              (!empty($aItem['language'])?
							'<a name="Jezyk">'."\n".
								'<![CDATA['.$aItem['language'].']]>'."\n".
							'</a>'."\n"
							:'').
              (!empty($aItem['volume_nr'])?
							'<a name="Tom_nr">'."\n".
								'<![CDATA['.$aItem['volume_nr'].']]>'."\n".
							'</a>'."\n"
							:'').
              (!empty($aItem['edition'])?
							'<a name="Wydanie">'."\n".
								'<![CDATA['.$aItem['edition'].']]>'."\n".
							'</a>'."\n"
							:'').
							(!empty($aItem['publisher_name'])?
							'<a name="Wydawnictwo">'."\n".
								'<![CDATA['.htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false).']]>'."\n".
							'</a>'."\n"
							:'').
							(!empty($aItem['binding_name'])?
							'<a name="Oprawa">'."\n".
								'<![CDATA['.$aItem['binding_name'].']]>'."\n".
							'</a>'."\n"
							: '').
							(!empty($aItem['dimension_name'])?
							'<a name="Format">'."\n".
								'<![CDATA['.$aItem['dimension_name'].']]>'."\n".
							'</a>'."\n"
							:'').
						'	</attrs>'."\n";
	$sXML .= '</Product>'."\n";
	return $sXML;
}
        
	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn, A.pages, A.price_brutto, A.description, 
									A.shipment_time, B.name AS publisher_name, 
									C.binding AS binding_name, D.dimension AS dimension_name,
									A.packet, A.weight, A.publication_year, A.volume_nr, A.edition, PL.language
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings C
					 	ON C.id=A.binding
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_dimensions D
					 	ON C.id=A.dimension
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages PL
             ON PL.id = A.language
					 WHERE A.published = '1'
            AND A.packet = '0'
            AND (A.prod_status = '1' OR A.prod_status = '3')
            AND (A.shipment_time = '0' OR A.shipment_time='1')
            AND A.page_id <> ".$aConfig['import']['unsorted_category']."
            GROUP BY A.id
           ";
	$oProducts = Common::PlainQuery($sSql);
		
	$sXMLFilename = $_SERVER['DOCUMENT_ROOT'].'/ABCKsiegarnia_221092.xml';
	if (!($fp = fopen($sXMLFilename, 'w'))) {
   echo "Nie mogę otworzyc pliku ceneo.xml do zapisu";
   die();
	} else {
    stream_set_write_buffer($fp, 81920);
		$sXml = '<?xml version="1.0" encoding="utf-8"?>
	<Products xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">
	';
		fwrite($fp,$sXml);
		$sXml='';

		$i=0;
		while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
			$aCat = getProductCats($aItem['id']);
			if(!empty($aCat)){
				$sXml .= generateElement($aItem, $aCat);
				fwrite($fp, $sXml);
//				fflush($fp);
				$sXml='';
			}
			if ($i==10) {
				//dump($aItem);
				//be;
			}
			$i++;
		}

		$sXml = '		'.
	'</Products>';

		fwrite($fp,$sXml);
		fclose($fp);
		$sXml='';
		
		
		// Enable user error handling
		libxml_use_internal_errors(true);
 /*
		// VALIDACJA
		$tempDom = new DOMDocument();
		$tempDom->load($sXMLFilename);
   
		if ($tempDom->schemaValidate($_SERVER['DOCUMENT_ROOT'].'import/XSD/Offer_ceneo_v2.xsd') === false) {
				echo 'BŁĄÐ';
				$sErr = libxml_display_errors();

				// XXX błąd walidacji pliku
				// XXX mail 
				$sContent = "BŁĄÐ podczas walidacji pliku: <br />log: <br />".$sErr;
				echo $sContent;
				//simpleSendMail('Błąd eksportu do Virtualo', $sContent, true);
				exit('Błąd generowania XML Ceneo');
		} else {
				echo 'OK';
		}
		*/
    echo 'KONIEC';
	}
  exit(0);
/*


<?xml version="1.0" encoding="utf-8"?>
<offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1"><!--version (wersja szablonu): liczba całkowita, wymagany-->  
  <group name="books"><!--Oferty zgrupowane wg zdefiniowanych przez nas branż (odpowiednich predefiniowanych teraz XML'i). 
                      Grupa musi być jedną z: books, computers, tires, perfumes, music, games, movies, medicines, rims(felgi), grocery(delikatesy). 
                      Możemy rozszerzać tą listę.-->     
    <o id="151" url="http://www.sklep.tel.pl/id=158" price="980" avail="1" set="0"><!--Oferta sklepu;
                                                                                                                      id: tekst (długość 100), wymagany; 
                                                                                                                      url: tekst (długość 2048), wymagany;
                                                                                                                      price: liczba zmiennoprzecinkowa, wymagany, separator: kropka (przecinek spowoduje błąd walidacji); 
                                                                                                                      avail (dostępność): liczba, opcjonalny, dostępne wartości [1,3,7,14,99], domyślnie 99;
                                                                                                                      
                                                                                                                      set (czy zestaw): 0/1, opcjonalny, domyślnie 0-->
      <name><!--Nazwa oferty; tekst (długość 150), wymagany-->
        <![CDATA[Wspaniała książka]]>
      </name>
<cat><![CDATA[Książki/Wspaniałe książki]]></cat><!--cat (kategoria w sklepie): tekst (długość 255), wymagany-->
      <imgs><!--Zdjęcia oferty, opcjonalny-->
        <main url="http://www.sklep.tel.pl/jpg=158"/><!--zdjęcie główne, wymagany;
                                                        url (długość 2048), wymagany;-->
        <i url="http://www.sklep.tel.pl/jpg=158"/><!--zdjęcie dodatkowe, opcjonalny;
                                                        url (długość 2048), wymagany;-->

      </imgs>
      <desc><!--Opis oferty, opcjonalny-->
        <![CDATA[Opis]]>
      </desc>
      <attrs><!--Atrybuty oferty, wymagany-->
        <a name="Autor"><!--name: tekst (długość 512) wymagany-->          
          <![CDATA[Radek Niejadek]]>
        </a>
        <a name="ISBN">

          <![CDATA[832400782]]>
        </a>
        <a name="Ilosc_stron">
          <![CDATA[10]]>
        </a>        
      </attrs>      
    </o>
  </group>
</offers>


*/