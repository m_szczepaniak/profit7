<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');

function getProductAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.', ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-2);
  }
  return $sAuthors;
}
	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	} // end of getTarrifPrice()
	
	function getProductCat($iId) {
	global $aConfig;
	$sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";
	return Common::GetRow($sSql);
}

function getCategoryParent($iId) {
	global $aConfig;
	
	$sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return (!empty($aItem['name'])?$aItem['name'].'/':'');
}

function getShipmentTime($iShipment){
	switch(intval($iShipment)){
		case 0:
			return 'od ręki';
			break;
		case 1:
			return '1-2 dni';
			break;
		case 2:
			return '7-9 dni';
			break;
		case 3:
			return 'na zamówienie';
			break;
	}
}


function getProductImg($iId) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
	
		if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
			return $sDir.'/__b_'.$aImg['photo'];
		}
		else {
			if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
				return $sDir.'/'.$aImg['photo'];
			}
		}
	}
	return '';
} // end of getItemImage() function


  /**
   * Metoda pobiera opisy dodawane do tytułu produktu - dostępność
   * 
   * @return type
   */
  function getShipmentTimes(){

    $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
    return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
  }// end of getShipmentTimes() method


  $aShipmentTimes = getShipmentTimes();
  
	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 LIMIT 1";
	$aSite =& Common::GetRow($sSql);
	$sLog='';
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn, A.azymut_index,  A.pages, A.edition, A.shipment_time, A.price_brutto, A.shipment_time, A.description, B.name AS publisher_name,
									L.binding, I.language, J.language AS original_language, A.city, A.volume_name, A.volume_nr, A.publication_year, A.name2, E.name AS series_name
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 	LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages I
						 ON A.language=I.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages J
						 ON A.original_language=J.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings L
						 ON A.binding=L.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series D
						 ON D.product_id=A.id
	  					LEFT JOIN ".$aConfig['tabls']['prefix']."products_series E
	  					 ON E.id=D.series_id
					 WHERE A.published = '1'
					 			AND (A.prod_status = '1' OR A.prod_status = '3')
					 			AND (A.shipment_time = '0' OR A.shipment_time='1')
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category'];
	$oProducts = Common::PlainQuery($sSql);
		
	if (!($fp = fopen($_SERVER['DOCUMENT_ROOT'].'/radar.xml', 'w'))) {
   echo "Nie mogę otworzyc pliku radar.xml do zapisu";
   die();
	} else {
	
	$sXml = '<?xml version="1.0" encoding="UTF-8"?>
<radar wersja="1.0">
	<oferta>
';
	fwrite($fp,$sXml);

	
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		$aCat = getProductCat($aItem['id']);
		if(!empty($aCat['name'])){
			$sCatFullPath = getCategoryParent($aCat['id']).str_replace('/ ','',$aCat['name']);
			$fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
			$sAuthors = getProductAuthors($aItem['id']);
			$sImg = getProductImg($aItem['id']);
			
			$sName = $aItem['plain_name'];
			if(!empty($sAuthors)){
				$sName .= ' - '.$sAuthors;
			}

	
			fwrite($fp, "	<produkt>\n		<grupa1>\n");
			fwrite($fp, "		<nazwa><![CDATA[".htmlspecialchars($sName,ENT_QUOTES,'UTF-8',false).$aShipmentTimes[$aItem['shipment_time']]."]]></nazwa>\n");
			if(!empty($aItem['publisher_name'])){
				fwrite($fp, "		<producent>".htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false)."</producent>\n");
			}
			fwrite($fp, "		<opis><![CDATA[".htmlspecialchars(strip_tags(clearAsciiCrap($aItem['description'])),ENT_QUOTES,'UTF-8',false)."]]></opis>\n");
			fwrite($fp, "		<id>".$aItem['id']."</id>\n");
			fwrite($fp, "		<url><![CDATA[".htmlspecialchars($aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name']),ENT_QUOTES,'UTF-8',false)."]]></url>\n");
			if(!empty($sImg)){
				fwrite($fp, "		<foto><![CDATA[".htmlspecialchars($aConfig['common']['base_url_http'].$sImg,ENT_QUOTES,'UTF-8',false)."]]></foto>\n");
			}
			fwrite($fp, "		<kategoria><![CDATA[".htmlspecialchars($sCatFullPath,ENT_QUOTES,'UTF-8',false)."]]></kategoria>\n");
			fwrite($fp, "		<cena>".$fTarrifPrice."</cena>\n");
			
			fwrite($fp, "		<dostepnosc>".getShipmentTime($aItem['shipment_time'])."</dostepnosc>\n");
			fwrite($fp, "		<promocja></promocja>\n");

			fwrite($fp, "		</grupa1>\n	</produkt>\n");
			fflush($fp);
		}
	}
	
	$sXml = '		</oferta>
</radar>';
	fwrite($fp,$sXml);
	fclose($fp);
	}
	
/*
<?xml version="1.0" encoding="UTF-8"?>
<radar wersja="1.0">
	<oferta>
		<produkt>
			<grupa1>
				<nazwa><![CDATA[F.E.A.R.]]></nazwa>
				<producent><![CDATA[CD PROJEKT]]></producent>
				<opis><![CDATA[F.E.A.R. to doskonałe połączenie gry
				akcji z intrygującą fabułą rodem z najpopularniejszych
				azjatyckich horrorów(...)]]>
				</opis>
				<id>1234</id>
				<url>http://sklep.pl/oferta1234.html</url>
				<foto>http://sklep.pl/zdjecia/1234.jpg</foto>
				<kategoria>
				Gry i Konsole / Komputerowe PC / Akcja
				</kategoria>
				<cena>52.00</cena>
				<dostepnosc>1-2 dni</dostepnosc>
				<promocja></promocja>
			</grupa1>
		</produkt>
		<produkt>
			<grupa1>
				<nazwa><![CDATA[MYST V: KONIEC WIEKÓW (GRA PC)]]></nazwa>
				<producent><![CDATA[CENEGA]]></producent>
				<opis><![CDATA[Zdecyduj o losie cywilizacji w ostatnim,
				niepowtarzalnym rozdziale sagi Myst(...)]]>
				</opis>
				<id>1235</id>
				<url>http://sklep.pl/oferta1235.html</url>
				<foto>http://sklep.pl/zdjecia/1235.jpg</foto>
				<kategoria>
				Gry i Konsole / Komputerowe PC / Akcja
				</kategoria>
				<cena>44.00</cena>
				<dostepnosc>1-2 dni</dostepnosc>
				<promocja>Mousepad gratis!</promocja>
			</grupa1>
		</produkt>
	</oferta>
</radar>


*/
?>