<?php
/**
 * Skrypt porządkujący ISBN'y
 *
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

use LIB\EntityManager\Entites\StockLocation;

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');

$stockLocation = new StockLocation($pDbMgr);


$sSql = '
SELECT id, available
FROM stock_location 
WHERE container_id LIKE "17%" 
  AND quantity > 0
';

$stockLocationRows = $pDbMgr->GetAll('profit24', $sSql);
foreach ($stockLocationRows as $row) {
    $locationId = $row['id'];

    $sSql = '
        UPDATE stock_location
        SET 
            quantity = 0,
            reservation = 0,
            available = 0
        WHERE id = ' . $locationId . '
        LIMIT 1 ';
    $result = $pDbMgr->Query('profit24', $sSql);

    if (false === $stockLocation->recountProductStock($locationId)) {
        $result = false;
    }
}
