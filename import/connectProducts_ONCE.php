<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 20.09.17
 * Time: 11:24
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');

$stdClass = new stdClass();
$oParent = new stdClass();
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module.class.php');
$module = new \Module($stdClass, $oParent, true);

$sSql = "
SELECT P2.id as toDelId, P.id as toMainId
FROM `products` AS P
JOIN products AS P2
ON P.id <> P2.id AND CONCAT('0', P.isbn_plain) = P2.isbn_plain
WHERE LENGTH(P.isbn_plain) = 12
";

//$sSql = "
//SELECT P2.id as toDelId, P.id as toMainId
//FROM `products` AS P
//JOIN products AS P2
//ON P.id <> P2.id AND CONCAT('00', P.isbn_plain) = P2.isbn_plain
//WHERE LENGTH(P.isbn_plain) = 11
//AND P.isbn_plain <> '00000000000'
//AND P.packet <> '1'
//";


$productsToConnect = Common::GetAll($sSql);
foreach ($productsToConnect as $products) {
    $bIsErr = false;
    $reason = '';

    $toDelId = $products['toDelId'];
    $toMainId = $products['toMainId'];
    $result = $module->doConnectProducts($reason, $toMainId, $toDelId, false);
    if ($module::ERR_CONNECT_PRODUCTS_PRICE === $result) {
        $sMsg = _('Produkty mają różną cenę brutto');
        $bIsErr = true;
    }
    elseif ($module::ERR_CONNECT_PRODUCTS_INDEKS === $result) {
        $sMsg = _('W produktach występuje zbyt wiele różnych indeksów, nie można połączyć takich produktów. Indeksy: ');
        $bIsErr = true;
    }
    elseif (false === $result) {
        $sMsg = sprintf(_('Wystąpił błąd podczas łączenia produktów z powodu %s'), $reason);
        $bIsErr = true;
    }

    if (false === $bIsErr) {
        $sMsg = sprintf(_('Produkt "%s" został zaimportowany do produktu "%s"'), $toDelId, $module->getProductAttrib($iId, ' name '));
    }
    echo ':'.$toMainId.':'.$toDelId.':'.$sMsg."\n";
}
