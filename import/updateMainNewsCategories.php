<?php
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);


include_once ('import_func.inc.php');
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
//$oCommonSynchro->iSourceId = '1';// tak okazyjnie

/*
	$aHelionBrans = array(
			'Helion Wydawnictwo', // - helion
			'Helion - Onepress', //- onepress
			'Helion - Editio', //- editio
			'Helion - Sensus',//- sensus
			'Helion - Septem',//- septem
			'Bezdroża',//- bezdroza
			'Helion - Edukacja',//- podrecznik
			'Bezdroża'//- bezdroża obce
	);
*/

/**
 * Metoda aktualizuje kategorie dla elementów
 * 
 * @param array $aElements - tablica elementów
 * @param string $sTable - nazwa tabeli do aktualizacji
 */
function updateItems(&$aElements, $sTable, $sColName) {
	
	foreach ($aElements as $iProductId => $iCatVal) {
		$aValues = array(
				'page_id' => $iCatVal
		);
		dump($sColName.' = '.$iProductId.' na : '.$sTable);
		dump($aValues);
		if (Common::Update($sTable, $aValues, $sColName.' = '.$iProductId) === false) {
			echo 'Błąd zmiany kategorii głownej produktu';
			die;
		}
	}
}// end of updateItems() method

$sSql = "
SELECT A.id, A.prod_status
FROM products AS A
JOIN products_previews AS B ON A.id = B.product_id
WHERE A.prod_status <>  '3' ";
$aErrPreviews = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
foreach ($aErrPreviews as $iProductId => $iProdStatus) {
	if ($iProdStatus == '1') {
		$aValues = array('product_id' => $iProductId);
		Common::Insert('products_news_become', $aValues, '', false);
	}
	$oCommonSynchro->delPreviews($iProductId);
}


$aNewsBecome = getNewsBecome();
if (!empty($aNewsBecome)) {
	
	// są produkty które powinny stać się nowościami
	foreach ($aNewsBecome as $aItem) {
		$PId = $aItem['product_id'];

		// dodaj do nowosci 
		$aNews = getNews($PId);
		$aPreviews = getPreviews($PId);
		// sprawdz czy przypadkiem już nie istnieje
		// jeśli nie jest to zapowiedź !!
		if (empty($aNews) && empty($aPreviews)) {
			$aPublisherCats = array();
			if($aItem['publisher_id'] != 'NULL' && intval($aItem['publisher_id']) > 0){
				$aPublisherCats = getPublisherCategories($aItem['publisher_id']);
			}
			//kategorie powiazane do serii
			$aSeriesCats=array();
			if($aItem['publisher_id']>0) {
				$iSeriesId = getSeries($PId);
				if ($iSeriesId > 0 ) {
					$aSeriesCats = getSeriesCategories($iSeriesId);
					if(!empty($aSeriesCats) && $aSeriesCats['main_category'] > 0){
							//nadpisanie zeby nie bylo problemow dalej
							$aPublisherCats['main_category'] = $aSeriesCats['main_category'];
					}
					if(!empty($aSeriesCats) && $aSeriesCats['news_category'] > 0){
							//nadpisanie zeby nie bylo problemow dalej
							$aPublisherCats['news_category'] = $aSeriesCats['news_category'];
					}	
				}
			}

			// produkt nie jest obecnie nowością
			if(!empty($aPublisherCats) && $aPublisherCats['news_category'] > 0){
				if (addNews($PId,$aPublisherCats['news_category']) === false) {
					echo "Error adding to news ".$PId." <br>\n";
					dump($sSql);
				}
			} else {
				if (addNews($PId,423) === false) {
					echo "Error adding to news ".$PId." <br>\n";
					dump($sSql);
				}
			}
		}
		// usun z tabeli rekord
		$sSql = "DELETE FROM products_news_become WHERE product_id=".$PId;
		if (Common::Query($sSql) === false) {
				echo "Error deleting from products_news_become table <br>\n";
				dump($sSql);
		}
	}
}



// ----- pobieramy do pamieci błędne serie
$sSql = "SELECT P.id, S.news_category
FROM  `products` AS P
JOIN products_to_series AS PS ON P.id = PS.product_id
JOIN products_series AS S ON S.id = PS.series_id
JOIN products_news AS PN ON PN.product_id = P.id
WHERE S.news_category > 0
AND PN.page_id <> S.news_category
AND P.modiefied_by_service = '0'
";
$aSeriesUpdateNews = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);

$sSql = "SELECT P.id, S.main_category
FROM  `products` AS P
JOIN products_to_series AS PS ON P.id = PS.product_id
JOIN products_series AS S ON S.id = PS.series_id
AND S.main_category > 0 
AND P.page_id <> S.main_category
AND P.modiefied_by_service = '0'
";
$aSeriesUpdateCategories = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
/*
if (!empty($aSeriesUpdateNews)) {
	$sAddNewsSQL = " AND A.product_id NOT IN (".implode(',' , array_keys($aSeriesUpdateNews)).")";
}
*/
/*
if (!empty($aSeriesUpdateCategories)) {
	$sAddCatSQL = " AND A.id NOT IN (".implode(',' , array_keys($aSeriesUpdateCategories)).")";
}
*/
// ----- pobieramy do pamieci błędnych wydawców
$sSql = " SELECT B.id, C.news_category
	FROM products_news AS A
	JOIN products AS B 
		ON A.product_id = B.id
	JOIN products_publishers AS C 
		ON B.publisher_id = C.id
	WHERE C.news_category > 0 
  AND A.page_id <> C.news_category 
  AND B.modiefied_by_service = '0' ".$sAddNewsSQL;
$aPublisherUpdateNews = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);


$sSql = "SELECT A.id, B.main_category
FROM products AS A
JOIN products_publishers AS B ON A.publisher_id = B.id
WHERE B.main_category > 0 
AND A.page_id <> B.main_category 
AND A.modiefied_by_service = '0' ".$sAddCatSQL;
$aPublisherUpdateCategories = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);



// usuwamy z wydawców id z serii
foreach ($aPublisherUpdateNews as $iPId => $iVal) {
	if (isset($aSeriesUpdateNews[$iPId])) {
		unset($aPublisherUpdateNews[$iPId]);
	}
}

// usuwamy z wydawców id z serii
foreach ($aPublisherUpdateCategories as $iPId => $iVal) {
	if (isset($aSeriesUpdateCategories[$iPId])) {
		unset($aPublisherUpdateCategories[$iPId]);
	}
}
//dump($aSeriesUpdateNews);
//dump($aSeriesUpdateCategories);
//dump($aPublisherUpdateCategories);
//dump($aPublisherUpdateCategories);
//die;
// -- właściwa część zabawy




dump('Wydawcy - kategoria nowości');
dump($aPublisherUpdateNews);
updateItems($aPublisherUpdateNews, "products_news", 'product_id');
unset($aPublisherUpdateNews);


dump('Wydawcy - kategoria głowna');
dump($aPublisherUpdateCategories);
updateItems($aPublisherUpdateCategories, "products", 'id');
unset($aPublisherUpdateCategories);


// ----- pobieramy do pamieci błędne serie
$sSql = "SELECT P.id, S.news_category
FROM  `products` AS P
JOIN products_to_series AS PS ON P.id = PS.product_id
JOIN products_series AS S ON S.id = PS.series_id
JOIN products_news AS PN ON PN.product_id = P.id
WHERE S.news_category > 0
AND PN.page_id <> S.news_category
AND P.modiefied_by_service = '0' ";
$aSeriesUpdateNews = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);

$sSql = "SELECT P.id, S.main_category
FROM  `products` AS P
JOIN products_to_series AS PS ON P.id = PS.product_id
JOIN products_series AS S ON S.id = PS.series_id
AND S.main_category > 0 AND P.page_id <> S.main_category
AND P.modiefied_by_service = '0' ";
$aSeriesUpdateCategories = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);

dump('SERIE - kategoria nowości');
dump($aSeriesUpdateNews);
updateItems($aSeriesUpdateNews, "products_news", 'product_id');
unset($aSeriesUpdateNews);


dump('SERIE - kategoria głowna');
dump($aSeriesUpdateCategories);
updateItems($aSeriesUpdateCategories, "products", 'id');
unset($aSeriesUpdateCategories);


// porządek z zapowiedziami
$sSql = "SELECT A.id, A.page_id
FROM  `products` AS A
JOIN products_previews AS B ON A.id = B.product_id
WHERE B.page_id <> IF( A.page_id =1, 2337, IF( A.page_id =147, 2339, IF( A.page_id =261, 2338, NULL ) ) ) ";
$aPreviewsUpdateCategories = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
foreach ($aPreviewsUpdateCategories as $iPreviews => $iPageId) {
	$aPreviewsUpdateCategories[$iPreviews] = $oCommonSynchro->getPreviewPageIdByProductPageId($iPageId);
}
dump($aPreviewsUpdateCategories);
updateItems($aPreviewsUpdateCategories, "products_previews", 'product_id');

// porządek z bestsellerami
$sSql = "SELECT A.id, A.page_id
FROM  `products` AS A
JOIN products_bestsellers AS B ON A.id = B.product_id
WHERE B.page_id <> IF( A.page_id = 1, 500, IF( A.page_id = 147, 499, IF( A.page_id = 261, 498, NULL ) ) ) ";
$aBestselleryUpdateCategories = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
foreach ($aBestselleryUpdateCategories as $iBests => $iPageId) {
	$aBestselleryUpdateCategories[$iBests] = $oCommonSynchro->getBestsellerPageIdByProductPageId($iPageId);
}
dump($aBestselleryUpdateCategories);
updateItems($aBestselleryUpdateCategories, "products_bestsellers", 'product_id');

// porządek z nowosciami
$sSql = "SELECT A.id, A.page_id
FROM  `products` AS A
JOIN products_news AS B ON A.id = B.product_id
WHERE B.page_id <> IF( A.page_id =1, 423, IF( A.page_id =147, 424, IF( A.page_id =261, 425, NULL ) ) )  ";
$aNewsUpdateCategories = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
foreach ($aNewsUpdateCategories as $iNews => $iPageId) {
	$aNewsUpdateCategories[$iNews] = $oCommonSynchro->getNewsPageIdByProductPageId($iPageId);
}
dump($aNewsUpdateCategories);
updateItems($aNewsUpdateCategories, "products_news", 'product_id');
?>