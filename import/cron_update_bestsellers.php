<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

$aConfig['common']['use_session'] = true;
$aConfig['use_db_manager']=true;
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/config.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module_Common.class.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lang/pl.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/lang/admin_pl.php');


class update_bestsellers {

	function update_bestsellers() {}
	
	function update() {
		
		$fF=fopen($_SERVER['DOCUMENT_ROOT'].'import/bestselers_log.txt','a');
		fputs($fF, date('Y-m-d H:i:s').' Rozpoczecie pracy'."\r\n");
		
		$iN=500;			//do ustawienia
		$sDate=date('Y-m-d H:i:s', strtotime('-1 month'));

		fputs($fF, date('Y-m-d H:i:s').' Pobieram zamowienia od '.$sDate."\r\n");
		
		
		$aPageIds = array(261, 147, 1);
		//pobranie wartości limitów
		$aLimits = getModulePages('m_powiazane_oferta_produktowa','1',true,'','bestsellery');
		dump($aLimits);

		// pobranie N najczęściej kupowanych książek drozszych niz limit w każdym z działów z osobna
		$aBooks=array();
		$bIsErr=0;
		
		/*
		Common::BeginTransaction();
		*/
		
		//truncate na tabele z bestsellerami
		$sSql='SELECT product_id
					 FROM '.$aConfig['tabls']['prefix'].'products_bestsellers
					 WHERE automatic = 1';
		$aToDelete=Common::GetAll($sSql);

		// usuniecie
		foreach($aToDelete as $aItem)
			$this->deleteBestseller($aItem['product_id']);
		
		for($i=0; $i<3; $i++) {
			fputs($fF, date('Y-m-d H:i:s').' Zamowienia ksiazek dla dzialu  '.$aPageIds[$i]."\r\n");

			$sSql='SELECT A.product_id, C.page_id_1, COUNT(A.id) AS sum 
			FROM '.$aConfig['tabls']['prefix'].'products_shadow C
			JOIN '.$aConfig['tabls']['prefix'].'orders_items A ON C.id= A.product_id
			JOIN '.$aConfig['tabls']['prefix'].'orders B ON A.order_id=B.id
			JOIN '.$aConfig['tabls']['prefix'].'products_publishers P ON C.publisher_id=P.id
			WHERE B.order_date>\''.$sDate.'\' AND ((P.main_category IS NULL AND C.page_id_1='.$aPageIds[$i].')  OR P.main_category='.$aPageIds[$i].') AND A.`promo_price_brutto`>='.Common::GetOne('SELECT bestsLimit FROM '.$aConfig['tabls']['prefix'].'menus_items WHERE id='.$aLimits[$i+1]['value']).' 
			GROUP BY product_id 
			ORDER BY sum DESC 
			LIMIT '.$iN;			
			$aBooks = Common::GetAll($sSql);
            dump($aLimits);
            dump($aBooks);

			echo $sSql."<br /><br />";
			dump($aBooks);
	
			//wpisanie do tabeli bestsellerów
			foreach($aBooks as $iKey => $aBook) {
				//$aValues=array('product_id'=>$aBook['product_id'], 'page_id'=>$aLimits[$i+1]['value'], 'placeOnMain'=>'1', 'automatic'=>1);
				if(Common::GetOne('SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'products_bestsellers WHERE product_id='.$aBook['product_id'])==0){
					if ($this->addBestseller($aBook['product_id'], $aLimits[$i+1]['value'])===false)
						$bIsErr=true;		
					}
				fputs($fF, date('Y-m-d H:i:s').' Dodaje ksiazke o id '.$aBook['product_id']."\r\n");
			}
		}
		if($bIsErr) {
			/*
			Common::RollbackTransaction();
			 */
			echo'Błąd dodawania bestsellerów';
			fputs($fF, date('Y-m-d H:i:s').' BLAD'."\r\n");
		}
		else {
			/*
			Common::CommitTransaction();
			 */
			echo 'OK';
			fputs($fF, date('Y-m-d H:i:s').' OK'."\r\n");
		}
		fclose($fF);
	}
	
	/**
	 * Metoda dodaje produkt do tabeli bestsellerow
	 */
	 function addBestseller($iId, $iPage){
	 	global $aConfig;
	 	
	 	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_bestseller='1'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_bestseller='1'
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				
				$bIsError = true;
			}
		}
	 	
	 	$aValues = array (
	 		'product_id' => $iId,
	 		'page_id' => $iPage,
	 		'order_by' => '#order_by#',
	 		'placeOnMain'=>'1', 
	 		'automatic'=>1
	 	);
	 	
	 	if(Common::Insert($aConfig['tabls']['prefix']."products_bestsellers",
	 										$aValues,'page_id = '.intval($iPage),false) === false) {
				$bIsError = true;
			}
			return !$bIsError;

	 } // end of addBestseller method
	 
	 

	function existShadow($iId) {
		global $aConfig;
		// usuwanie cienia
		$sSql = "SELECT count(id) FROM ".$aConfig['tabls']['prefix']."products_shadow
							 WHERE id = ".$iId;
		return (Common::GetOne($sSql) > 0);
	}
	 
	
	 /**
	  * Metoda usuwa produkt z tabeli bestsellerow
	  */
	  function deleteBestseller($iId){
	  	global $aConfig;
	  	
	  	
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_bestseller='0'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_bestseller='0'
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_bestsellers
						 WHERE product_id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		return !$bIsError;
	  	
		} // end of deleteBestseller method
};
$oUp = new update_bestsellers();
if ($oUp->update() === false) {
	echo "Wystąpił błąd podczas aktualizacji bestsellerów";
	exit();
}
	
?>