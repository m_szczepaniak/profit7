<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);

include_once ('import_func.inc.php');

// zmienne

if ($aConfig['common']['status']	!= 'development') {
  if(!downloadAzymutXML("getCats","azymut_cats.xml")){
    sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml kategorii z Azymut");
    die("błąd pobierania xml kategorii");
  }
}

if (file_exists($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/azymut_cats.xml")) {
	$oXml = new XMLReader();
	$oXml->open($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/azymut_cats.xml");
	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'cats':
						// KONIEC LISTINGU KATEGORII
						$oXml->close();
					break;
				}
			break;
		
		case XMLReader::ELEMENT :
			switch ($oXml->name) {
				case 'cat':
					$iId=$oXml->getAttribute('id');
					if(!existAzymutCategory($iId)){
						$sPid=$oXml->getAttribute('pid');
						$sName=getTextNodeValue($oXml);
						$aValues = array (
								'id' => $iId,
								'parent_id' => (!empty($sPid)?$sPid:'NULL'),
								'name' => $sName
							);
							
						$sParent='';
						// znalezienie rodzica
						if(!empty($sPid)){
							$sParent = getAzymutParentPath($sPid);
						}
				
							//dump($aValues);
							if (($iCat = Common::Insert($aConfig['tabls']['prefix']."products_azymut_categories", $aValues)) === false) {
								// blad
								echo "<strong>Blad!</strong> - dodawanie kategorii <strong>".$sParent.$sName."</strong><br />";
								sendInfoMail('Błąd importu Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się dodać kategorii : ".$sParent.$sName);
							} else {
								$sLogInfo.="Dodano kategorię ".$sParent.$sName."\n";
							}
					}
				break;
			}
		}
	}
	if(!empty($sLogInfo))
		sendInfoMail("Aktualizacja kategorii Azymut",$sLogInfo);
	// usuniecie pliku
	@unlink('XML/azymut_cats.xml');
}
?>