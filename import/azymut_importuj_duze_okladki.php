<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-07-27
 * Time: 11:19
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(1080000);
include_once('import_func.inc.php');
include_once('CommonSynchro.class.php');


/**
 * funkcja dodaje okładkę do produktu azymut
 * @param int $iProductId - id produktu
 * @param string $sAzymutId - indeks azymut
 * @param string $sIsbn - isbn
 * @return bool - success
 */
function addAzymutImageDeleteBefore($iProductId, $sAzymutId, $sIsbn)
{
    global $aConfig, $pDbMgr;
    // jeśli istniej w plikach z płyty
    // jeśli nie pobierz z serwera azymut

    $sDstName = $sIsbn . ".jpg";
    $sSrcFile = $aConfig['common']['base_path'] . 'images/photos/azymut_okladki/' . $sAzymutId . ".jpg";

    $sSql = "SELECT * FROM " . $aConfig['tabls']['prefix'] . "products_images
					 WHERE product_id=" . $iProductId;
    $aImages = $pDbMgr->GetAll('profit24', $sSql);
    foreach ($aImages as $aImage) {
        if ($aImage['id'] > 0) {
            // zdjecie juz istnieje w bazie nie usuwamy
            $sBigFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/__b_' . $aImage['photo'];
            if (file_exists($sBigFile)) {
                return false;
            }
        }
    }

    //http://services.azymut.pl/oferta/servlet/?mode=getImg&indeks=55803302629ZA&id=10831&p=ncYX8PwJ&size=hq
    usleep(20000);
    if (!downloadAzymutImage($sAzymutId, '&size=hq')) {
        usleep(20000);
        if (!downloadAzymutImage($sAzymutId, '')) {
            return false;
        }
    }

    createProductImageDir($iProductId);
    $iRange = intval(floor($iProductId / 1000)) + 1;
    $sDestDir = $aConfig['common']['base_path'] . 'images/photos/okladki/' . $iRange . '/' . $iProductId;
    $sShortDir = 'okladki/' . $iRange . '/' . $iProductId;

    if (!file_exists($sSrcFile))
        return false;


    foreach ($aImages as $aImage) {
        if ($aImage['id'] > 0) {
            // zdjecie juz istnieje w bazie nie usuwamy

            $sBaseFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/' . $aImage['photo'];
            $sThumbFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/__t_' . $aImage['photo'];
            $sBigFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/__b_' . $aImage['photo'];
            unlink($sBaseFile);
            unlink($sThumbFile);
            unlink($sBigFile);
            var_dump($sBaseFile);
            echo "\n";
            $sSql = 'DELETE FROM products_images WHERE id = ' . $aImage['id'];
            $pDbMgr->Query('profit24', $sSql);
        }
    }


    $aFileInfo = getimagesize($sSrcFile);

    resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_' . $sDstName);
    resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
    $aSmallInfo = getimagesize($sDestDir . '/' . $sDstName);
    if ($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])) {
        resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_' . $sDstName);
    }
    unlink($sSrcFile);

    $aValues = array(
        'product_id' => $iProductId,
        'directory' => $sShortDir,
        'photo' => $sDstName,
        'mime' => 'image/jpeg',
        'name' => $sDstName,
        'order_by' => '1'
    );
    print_r($aValues);
    return (Common::Insert($aConfig['tabls']['prefix'] . "products_images", $aValues, '', false) != false);

    return true;
} // end of addAzymutImage() function


$sSql = "SELECT P.id, azymut_index, isbn_plain
					 FROM products AS P
					 JOIN products_images AS PI
					  ON PI.product_id = P.id
					 WHERE
                     product_type <> 'K'
					  AND azymut_index <> ''
					  AND PI.created < '2016-07-20'
					 ";
$products = Common::PlainQuery($sSql);
while ($aBook =& $products->fetchRow(DB_FETCHMODE_ASSOC)) {
    addAzymutImageDeleteBefore($aBook['id'], $aBook['azymut_index'], $aBook['isbn_plain']);
}
