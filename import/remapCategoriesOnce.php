<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
include_once ('import_func.inc.php');


global $pDbMgr;
/*


$sSql = 'SELECT P.id
         FROM products AS P
         JOIN products_publishers AS PP
          ON P.publisher_id = PP.id AND PP.name = "LEGO"
';

$aProducts = $pDbMgr->GetCol('profit24', $sSql);


foreach ($aProducts as $iId) {
    $aValues = [
        'product_type' => 'L',
        'page_id' => '2348',
    ];
    $pDbMgr->Update('profit24', 'products', $aValues, ' id = '.$iId.' LIMIT 1');

    $aCategories = [ 2348, 2352 ];
    addExtraCategories($iId, $aCategories);
}
*/


$sSql = 'SELECT P.id
         FROM products AS P
         JOIN products_publishers AS PP
          ON P.publisher_id = PP.id AND PP.name IN ("Bona", "Tatarak")
';

$aProducts = $pDbMgr->GetCol('profit24', $sSql);

foreach ($aProducts as $iId) {
    $aCategories = [2379];
    addExtraCategories($iId, $aCategories);
}

/*
$sSql = 'SELECT P.id
         FROM products AS P
         JOIN products_publishers AS PP
          ON P.publisher_id = PP.id AND PP.name IN ("BRIGHT JUNIOR MEDIA")
';

$aProducts = $pDbMgr->GetCol('profit24', $sSql);

foreach ($aProducts as $iId) {
    $aCategories = [2381];
    addExtraCategories($iId, $aCategories);
}
*/