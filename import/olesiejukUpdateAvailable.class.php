<?php
/**
 * Skrypt aktualizacji dostępności ze źródła Olesiejuk do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importCSVController.class.php');	

global $pDbMgr, $aConfig;
// ---
// CONFIG
$bTestMode = false; //  czy test mode
$iLimitIteration = 99999999;
$sSource = 'olesiejuk';


// -------------------------------------------------
// DODANIE PRODUKTOW

$sElementName = 'available'; 
$oImportCSVController = new importCSVController($sSource, $sElementName, $bTestMode, $iLimitIteration);

if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveXMLData();
	 
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/olesiejuk/produkty_dostepne.csv';
}
	 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ';');
	
	dump($oImportCSVController->aErrorLog);
	dump($oImportCSVController->oSourceElements->aErrorLog);
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
}

$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." - OK", print_r($oImportCSVController->aLog, true).
																											print_r($oImportCSVController->oSourceElements->aLog, true));
$oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElementName." ERR", print_r($oImportCSVController->aErrorLog, true).
																											print_r($oImportCSVController->oSourceElements->aErrorLog, true));
unset($oImportCSVController);

// TODO mail z informacją o imporcie

echo 'koniec';