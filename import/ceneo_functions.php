<?php

/**
 * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
 *
 * @return string
 */

include_once ('import_func.inc.php');

function libxml_display_error($error)
{
    $return = "<br/>\n";
    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }
    $return .= trim($error->message);
    if ($error->file) {
        $return .=    " in <b>$error->file</b>";
    }
    $return .= " on line <b>$error->line</b>\n";

    return $return;
}


/**
 * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
 *
 * @return string
 */
function libxml_display_errors() {
    $sErr = '';
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        $sErr .= libxml_display_error($error);
    }
    libxml_clear_errors();
    return $sErr;
}// end of libxml_display_errors() function


function getProductAuthors($iId) {
    global $aConfig;
    $sAuthors='';
    // wybiera grupy kategorii dla tego produktu
    $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
    $aItems =& Common::GetCol($sSql);
    foreach ($aItems as $sItem) {
        $sAuthors .= $sItem.', ';
    }
    if(!empty($sAuthors)) {
        $sAuthors = substr($sAuthors,0,-2);
    }
    return $sAuthors;
}
function getTarrifPrice($iId) {
    global $aConfig;
    $sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
    return Common::GetOne($sSql);
} // end of getTarrifPrice()

function getProductCat($iId) {
    global $aConfig;
    $sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					 AND A.parent_id IS NOT NULL
					ORDER BY A.priority
					LIMIT 1";

    $res = Common::GetRow($sSql);;

    if (!empty($res)) {

        return $res;
    }

    $sSql2 = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";

    return Common::GetRow($sSql2);
}

function getCategoryParent($iId) {
    global $aConfig;

    $sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
    $aItem = Common::GetRow($sSql);
    return (!empty($aItem['name'])?$aItem['name'].'/':'');
}

function getProductImg($iId) {
    global $aConfig;

    $sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
    $aImg =& Common::GetRow($sSql);

    if (!empty($aImg)) {
        $sDir = $aConfig['common']['photo_dir'];
        if (!empty($aImg['directory'])) {
            $sDir .= '/'.$aImg['directory'];
        }

        if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
            return $sDir.'/__b_'.$aImg['photo'];
        }
        else {
            if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
                return $sDir.'/'.$aImg['photo'];
            }
        }
    }
    return '';
} // end of getItemImage() function

function getShipmentTimes(){

    $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
    return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
}


/**
 * Funkcja pobiera czas realizacji i konwertuje na wartości CENEO
 *
 * @param type $iShipment
 * @return int
 */
function getShipmentTimeCeneo($iShipment, $productStatus){
    /*
     *
     * wartość z Ksiegarni :
        ['shipment_0'] = "Od ręki";
        ['shipment_1'] = "1 - 2 dni";
        ['shipment_2'] = "7 - 9 dni";
        ['shipment_3'] = "tylko na zamówienie";
     *
     *
     * wartości z Ceneo:
      1 – dostępny, sklep posiada produkt,
      3 – sklep będzie posiadał produkt do 3 dni,
      7 – sklep będzie posiadał produkt w ciągu tygodnia,
      14 – produkt dostępny nie wcześniej niż za tydzień,
      99 lub brak wartości – link prowadzący do informacji w sklepie. Opcjonalnie
     */
    if ($productStatus == '3') {
        return 110;
    }
    switch(intval($iShipment)){
        case 0:
        case 1:
            return 1;
            break;
        case 2:
        case 3:
            return 3;
            break;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
            return 7;
            break;
        default:
            return 99;
            break;
    }
}



function generateElement(&$aItem, $sIsbnEan, $aCat, $aShipmentTimes, $bFirst, $bIsCeneo) {
    global $aConfig;
    $sXML = '';
    $sElement = '';
    $sCatFullPath = htmlspecialchars(getCategoryParent($aCat['id']).str_replace('/ ','',$aCat['name']), ENT_QUOTES,'UTF-8',false);

    if ($bIsCeneo === true) {
        $fTarrifPrice = Common::formatPrice2(getTarrifPrice($aItem['id']));
    } else {
        $fTarrifPrice = Common::formatPrice2(getTarrifBuyBoxPrice($aItem['id'], $aItem['price_brutto']));
    }

    $sAuthors = getProductAuthors($aItem['id']);

    $sImgSrc = getProductImg($aItem['id']);
    if ($sImgSrc!='') {
        $sImg = $aConfig['common']['base_url_http'].$sImgSrc;
    }

    $sPlainName = trimString(htmlspecialchars($aItem['plain_name'],ENT_QUOTES,'UTF-8',false).$aShipmentTimes[$aItem['shipment_time']], 150);

    // TODO SPRAWDZIĆ set='0'
    $sXML .= '<o id="'.$aItem['id'].'" '.
        'url="'.$aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['plain_name'].'-'.$sIsbnEan).'?cdn=1" '.
        'price="'.$fTarrifPrice.'" '.
        'avail="'.getShipmentTimeCeneo($aItem['shipment_time'], $aItem['prod_status']).'" '.
        'set="'.$aItem['packet'].'" '.
        ($aItem['weight']!=''? ' weight="'.$aItem['weight'].'"' : '') .'>'."\n";

    $sXML .= 	'	<cat>'.
        '<![CDATA['.htmlspecialchars($sCatFullPath,ENT_QUOTES,'UTF-8',false).']]>'.
        '</cat>'."\n";

    $sXML .= 	'	<name>'.
        '<![CDATA['.$sPlainName.']]>'.
        '</name>'."\n";

    if (!empty($sImg)) {
        $sXML .=	'	<imgs>'.
            '<main url="'.$sImg.'"/>'.
            '</imgs>'."\n";
    }

    $sXML	.=	'	<desc>'.
        '<![CDATA['.htmlspecialchars(stripslashes(strip_tags(clearAsciiCrap(($aItem['description_on_create'])))),ENT_QUOTES,'UTF-8',false).']]>'.
        '</desc>'."\n";

    $sXML .=	'	<attrs>'."\n".
        (!empty($sAuthors)?
            '<a name="Autor">'."\n". //<!--name: tekst (długość 512) wymagany-->
            '<![CDATA['.trimString(htmlspecialchars($sAuthors,ENT_QUOTES,'UTF-8',false), 512).']]>'."\n".
            '</a>'."\n"
            :'').
        '<a name="' . (strlen($sIsbnEan) == 13 && !preg_match('/^978.+/', $sIsbnEan) ? 'EAN' : 'ISBN') . '">'."\n".
        '<![CDATA['.$sIsbnEan.']]>'."\n".
        '</a>'."\n".
        (!empty($aItem['pages'])?
            '<a name="Ilosc_stron">'."\n".
            '<![CDATA['.$aItem['pages'].']]>'."\n".
            '</a>'."\n"
            :'').
        (!empty($aItem['publisher_name'])?
            '<a name="Wydawnictwo">'."\n".
            '<![CDATA['.htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false).']]>'."\n".
            '</a>'."\n"
            :'').
        (!empty($aItem['binding_name'])?
            '<a name="Oprawa">'."\n".
            '<![CDATA['.$aItem['binding_name'].']]>'."\n".
            '</a>'."\n"
            : '').
        (!empty($aItem['dimension_name'])?
            '<a name="Format">'."\n".
            '<![CDATA['.$aItem['dimension_name'].']]>'."\n".
            '</a>'."\n"
            :'').
        '	</attrs>'."\n";
    $sXML .= '</o>'."\n";
    return $sXML;
}

function getTarrifBuyBoxPrice($iId, $fPriceBrutto) {
    $aTarrif = getTarrif($iId);
    $markUpCookie = new \orders\MarkUpCookie\BuyBox();
    $aNewTarrif = $markUpCookie->getMarkup($fPriceBrutto, $aTarrif, $aTarrif['discount'], false);
    return $aNewTarrif['price_brutto'];
}

/**
 * Metoda zwraca aktualny cennik dla produktu
 * @param $iId - id produktu
 * @return array cennik
 */
function getTarrif($iId) {
    global $aConfig;
    $sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
    return Common::GetRow($sSql);
} // end of getTarrif()


/**
 * Metoda pobiera unikatowe indentyfikatory
 *
 * @param array $aItemData
 * @return array
 */
function getUniqueIdents($aItemData) {

    if ($aItemData['isbn_plain'] != '') {
        $aIdents[] = $aItemData['isbn_plain'];
    }
    if ($aItemData['isbn_10'] != '') {
        $aIdents[] = $aItemData['isbn_10'];
    }
    if ($aItemData['isbn_13'] != '') {
        $aIdents[] = $aItemData['isbn_13'];
    }
    if ($aItemData['ean_13'] != '') {
        $aIdents[] = $aItemData['ean_13'];
    }
    $aIdents = array_unique($aIdents);
    return $aIdents;
}
