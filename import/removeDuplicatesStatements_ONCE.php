<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');
global $pDB;



$sSql = '
SELECT OBS.id, OBS.title, OBS.paid_amount
FROM  `orders_bank_statements` AS OBS
LEFT JOIN orders_payments_list AS OPL
  ON OBS.id = OPL.statement_id
LEFT JOIN orders AS O
  ON O.id = OPL.order_id
WHERE DATE( OBS.created ) IN ("2017-01-05", "2017-01-09") 
GROUP BY CONCAT(OBS.title, "-", OBS.paid_amount)
HAVING COUNT( CONCAT(OBS.title, "-", OBS.paid_amount)) > 1
ORDER BY  OBS.`id` DESC


';

$aOBS = Common::GetAll($sSql);
foreach ($aOBS as $aItem) {
//    $sSql = 'DELETE FROM orders_bank_statements WHERE id = '.$aItem['id'] .' LIMIT 1';
//    Common::Query($sSql);
    $sSql = 'SELECT * FROM orders_bank_statements WHERE title = '.$pDB->quoteSmart($aItem['title']).' AND paid_amount = "'.$aItem['paid_amount'].'"
    ORDER BY id DESC
    LIMIT 1';
    $aNewtestObs = Common::GetRow($sSql);
    if ($aNewtestObs['finished'] == '0') {
        $sSql = 'DELETE FROM orders_bank_statements WHERE id = '.$aNewtestObs['id'] .' LIMIT 1';
        Common::Query($sSql);
    }
}