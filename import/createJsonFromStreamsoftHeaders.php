<?php

use FtpClient\FtpClient;

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

class MerlinIntegration
{

    var $db = '213.216.85.50:/var/lib/firebird/data/PROFIT.gdb';
    var $username = 'SYSDBA';
    var $password = 'ytrewq54321';
    var $pDbMgr;
    var $location;
    var $fv = array();
    var $suppliers = array();
    var $products_to_mm = array();
    var $json_output_directory = '../files/merlin_json/';//'/outputMerlin/'


    function __construct()
    {
        global $pDbMgr,$aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->json_output_directory = $aConfig['common']['client_base_path'] . $this->json_output_directory;
        $this->connectToFireBirdAndQuery();
    }

    function connectToFireBirdAndQuery()
    {
        $dbh = ibase_connect($this->db, $this->username, $this->password);

        echo "Polaczono z firebirdem".PHP_EOL;

        $sSql = 'SELECT * FROM merlin_sent_documents WHERE status = "NOWY" ';

        $documents = $this->pDbMgr->getAll('profit24',$sSql);

        foreach($documents as $document)
        {
            unset($this->suppliers);
            unset($this->fv);
            $this->location = $document['location'];
            $sql = 'SELECT m.oznnrwydr as MAGAZYN,
                     k.indeks as INDEKS,
                     case when (nagl_dost.id_defdok = 10001) then \'MM\' else \'FA\' end AS RODZAJ,
                     sum(case when (dd.ID_SPISDOK != 10)
                         then (case when (d.blokada = 0) then rd.ILOSCROZCH end)
                        else rd.ILOSCROZCH END) AS ILOSC,
                     d.cenazakdost AS CENA_ZAKUPU,
                     sv.ident AS STAWKA_VAT,
                     c.cenan AS CENA_SPRZ_NETTO,
                     c.cenab AS CENA_SPRZ_BRUTTO,
                        nagl_dost.NRDOKWEW AS NRDOK,
                        kon.NAZWASKR AS DOSTAWCA,
                        nagl_dost.DATADOK AS DATADOK,
                        kon.NRKONTRAH AS NRKONTRAH,
                        nagl_dost.ID_NAGL
                    from nagl n
                    join defdok dd on dd.id_defdok = n.id_defdok
                    join poz p on n.id_nagl = p.id_nagl
                    join rozchdost rd ON rd.id_poz = p.id_poz
                    join dostawa d ON rd.ID_DOSTAWA = d.ID_DOSTAWA
                    join poz AS poz_dost ON d.ID_POZZRODLA = poz_dost.ID_POZ
                    JOIN nagl AS nagl_dost ON nagl_dost.ID_NAGL = poz_dost.ID_NAGL 
                    LEFT join kontrah kon ON kon.ID_KONTRAH = nagl_dost.ID_KONTRAH
                    join cennik c on c.id_kartoteka = p.id_kartoteka and c.id_defceny = 10001
                    join stawkavat sv on sv.id_stawkavat = p.id_stawkavat
                    join kartoteka k on k.id_kartoteka = p.id_kartoteka
                    join magazyn m on m.id_magazyn = d.id_magazyn
                    where n.NRDOKZEW = \''.$document['streamsoft_header'].'\' AND dd.ID_DEFDOK IN(420,10012)
                    group by 1,2,3,5,6,7,8,9,10,11,12,13
                    order by k.indeks, d.cenazakdost DESC';

            $rc = ibase_query($dbh, $sql);

            if ($rc === false)
            {
                echo "Brak rezultatów - prawdopodobnie nieprawidłowy header".PHP_EOL;
                continue;
            }
            // Get the result row by row as object
            while ($row = ibase_fetch_object($rc)) {
                $this->addToJson($row);
                $this->markRecord($document['streamsoft_header']);
            }

            $this->createFiles();
        }

        // Release the handle associated with the result of the query
        ibase_free_result($rc);
        // Release the handle associated with the connection
        ibase_close($dbh);
    }
    // Connect to database

    public function markRecord($header)
    {
        $sSql = 'UPDATE merlin_sent_documents 
                 SET status = "DONE" WHERE streamsoft_header = "'.$header.'" ';

        if ($this->pDbMgr->query('profit24',$sSql) === false)
        {
            echo "Nie udało się oznaczyć nagłówków".PHP_EOL;
            return false;
        }
        else
        {
            echo "Udało się oznaczyć nagłówki".PHP_EOL;
            return true;
        }
    }

    public function addToJson($product)
    {
        $jsondata = array();
        $jsondata['merlin_sku'] = $this->findMerlinSku($product->INDEKS);
        $jsondata['streamsoft_indeks'] = $product->INDEKS;
        $jsondata['quantity'] = $product->ILOSC;
        $jsondata['shelf_id'] = $this->location;
        $jsondata['purchase_price'] = $product->CENA_ZAKUPU;
        $jsondata['supplier_id'] = $product->NRKONTRAH;
        //$jsondata['supplier'] = $product->DOSTAWCA;
        $jsondata['document_number'] = $product->NRDOK;
        $jsondata['document_date'] = $product->DATADOK;
        $jsondata['document_type'] = $product->RODZAJ;

        if ($product->RODZAJ == 'MM') {
            $this->suppliers[$product->NRKONTRAH][] = $jsondata;
        } else if ($product->RODZAJ == 'FA') {
            $this->fv[] = $jsondata;
        }
    }

    public function findMerlinSku($product_indeks)
    {
        $sSql = 'SELECT merlin_sku FROM products_merlin_mappings PMM 
                 JOIN products P
                 ON streamsoft_indeks = "' . $product_indeks . '" AND PMM.product_id = P.id';
        $result = $this->pDbMgr->GetOne('profit24', $sSql);

        if (empty($result)) {
            throw new \Exception('Produkt nie posiada mapowania SKU -> ' . $product_indeks.' <br>');
            echo "BRAK:".$product_indeks.PHP_EOL;
        } else {
            return $result;
        }
    }

    public function createFiles()
    {
        $file_list = array();
        //FA
        $json_content = json_encode($this->fv);

        if (!file_exists($this->json_output_directory)) {
            //mkdir($this->json_output_directory,0766,true);
        }

        //FA
        if (!empty($this->fv))
        {
            file_put_contents($this->json_output_directory . 'FA_'.$this->fv[0]['shelf_id'].'.json', $json_content);
            $file_list[] = 'FA_'.$this->fv[0]['shelf_id'].'.json';
            echo "Plik stworzony FA".PHP_EOL;
        }

        //MM
        if (!empty($this->suppliers))
        {
            foreach ($this->suppliers as $supplier => $data) {
                //echo $supplier;
                $json_content = json_encode($data);
                file_put_contents($this->json_output_directory . 'MM_' . $supplier.'_'.$data[0]['shelf_id'].'.json', $json_content);
                $file_list[] = 'MM_' . $supplier . '_' . $data[0]['shelf_id']. '.json';
                echo "Plik stworzony MM".PHP_EOL;
            }
        }

        if (!empty($file_list))
        {
            $this->sendFilesToFtp($file_list);
        }
    }

    /**
     * @param $file_list
     * @throws Exception
     */
    private function sendFilesToFtp($file_list)
    {
        $host = 'ftp.merlin.pl';
        $login = 'migracja_profit';
        $password = 'xiKDmsS6BgILs0S7ZYDM';

        $migration_dir = $this->location;

        $ftp = new FtpClient();
        $ftp->connect($host);
        $ftp->login($login, $password);
        $ftp->pasv(true);

        $isDir = $ftp->isDir($migration_dir);

        if (false == $isDir) {
            $mkDirRes = $ftp->mkdir($migration_dir, true);
            if ($mkDirRes === false) {
                throw new \Exception('Nie można utworzyć folderu prawdopodobnie brak uprawnień' . '<br>');
            }
        }

        foreach ($file_list as $filename) {
            $uploadRes = $ftp->put($migration_dir . DIRECTORY_SEPARATOR . $filename, $this->json_output_directory . $filename, FTP_ASCII);
            if ($uploadRes === false) {
                throw new \Exception('Nie można utworzyć wysłać plików' . '<br>');
            }
            else
            {
                echo "Plik wysłany na ftp ".$filename.PHP_EOL;
            }
        }



        //$ftp->close();
    }

}

$merlinIntegration = new MerlinIntegration();

die('czesc');