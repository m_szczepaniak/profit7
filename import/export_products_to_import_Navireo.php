<?php
/**
 * Skrypt importu produktów do zaimportowania do Navireo
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

// zapytanko
$sSql = "
SELECT 
	P.name as nazwa, 
	P.isbn_plain AS symbol, 
	IF(P.page_id = 1 OR P.page_id = 422, 'Ogolny', IF(P.page_id = 425 OR P.page_id = 261, 'Naukowy', IF(P.page_id = 424 OR P.page_id = 147, 'Jezykowy', 'BRAK'))) AS id_grupy, 
	P.price_netto, 
	P.price_brutto, 
  P.vat, 
  PP.name as Wydawnictwo, 
	GROUP_CONCAT(DISTINCT CONCAT(PA.name, ' ', PA.surname) SEPARATOR ', ') AS autorzy,
	CONCAT('https://www.profit24.pl/product', P.id, '.html') AS www,
	GROUP_CONCAT(DISTINCT MI.name SEPARATOR ' / ') AS kategorie,
	GROUP_CONCAT(DISTINCT PS.name SEPARATOR ' / ') AS serie_wydawnicze,
	P.isbn_10 AS add_isbn_10,
	P.isbn_13 AS add_isbn_13,
	P.ean_13 AS add_ean_13,
	P.publication_year as rok_wydania,
	P.edition AS wydanie,
	P.legal_status_date AS stan_prawny_na,
	P.city AS miejsce_wydania,
	P.pages AS liczba_stron,
	PB.binding AS oprawa,
	PD.dimension AS format_,
	PL.`language` AS język,
	P.translator AS tłumacz,
	PDO.`language` AS jezyk_oryginalny,
	P.original_title AS tytul_oryginalny,
	P.weight AS waga,
	P.volumes AS tomów, P.volume_nr AS tom,P.volume_name AS nazwa_tomu, P.id AS id_produktu_w_internecie
FROM `products` AS P
LEFT JOIN products_extra_categories AS PEC
	ON PEC.product_id = P.id
LEFT JOIN menus_items AS MI
	ON MI.id = PEC.page_id
LEFT JOIN products_publishers AS PP
	ON P.publisher_id = PP.id
LEFT JOIN products_to_authors AS PTA
	ON P.id = PTA.product_id
LEFT JOIN products_authors AS PA
	ON PTA.author_id = PA.id
LEFT JOIN products_to_series AS PTS
	ON PTS.product_id = P.id
LEFT JOIN products_series AS PS
	ON PS.id = PTS.series_id
LEFT JOIN products_bindings AS PB
	ON PB.id = P.binding
LEFT JOIN products_dimensions AS PD
	ON PD.id = P.dimension
LEFT JOIN products_languages AS PL
	ON PL.id = P.`language`
LEFT JOIN products_languages AS PDO
	ON PDO.id = P.original_language
WHERE P.id = 737414
GROUP BY P.id
ORDER BY P.id DESC
";
//P.published = '1' AND P.prod_status = '2'

$oProducts = Common::PlainQuery($sSql);
$bFirst = TRUE;
while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
  if ($bFirst === TRUE) {
    file_put_contents("produkty1.csv", '"'.implode('","', array_keys($aItem)).'"'."\n", FILE_APPEND | LOCK_EX);
    $bFirst = FALSE;
  }
  file_put_contents("produkty1.csv", '"'.implode('","', $aItem).'"'."\n", FILE_APPEND | LOCK_EX);
}