<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.10.17
 * Time: 12:45
 */

use LIB\EntityManager\Entites\Magazine;

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');


$sSql = '
         SELECT PS.id, PS.profit_g_location, PS.profit_x_location, PS.profit_g_act_stock
         FROM products_stock AS PS
         LEFT JOIN containers AS C1
          ON PS.profit_g_location != "" AND C1.id = PS.profit_g_location
         LEFT JOIN containers AS C2
          ON PS.profit_x_location != "" AND C2.id = PS.profit_x_location
         WHERE 
            PS.profit_g_act_stock > 0 AND
             (
                PS.profit_g_location != "" OR 
                PS.profit_x_location != ""
             )
             AND
             (
                C1.id IS NOT NULL OR 
                C2.id IS NOT NULL
             )
         ';
$aRows = Common::GetAll($sSql);
foreach ($aRows as $item) {
    $values = [
        'magazine_type' => ($item['profit_g_location'] != '' ? Magazine::TYPE_LOW_STOCK_SUPPLIES : Magazine::TYPE_HIGH_STOCK_SUPPLIES),
        'products_stock_id' => $item['id'],
        'container_id' => ($item['profit_g_location'] != '' ? $item['profit_g_location'] : $item['profit_x_location']),
        'quantity' => $item['profit_g_act_stock'],
        'available' => $item['profit_g_act_stock']
    ];
    Common::Insert('stock_location', $values);
}