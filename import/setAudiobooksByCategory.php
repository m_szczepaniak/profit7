<?php
/**
 * Skrypt oznacza, jako audiobooki, produkty które znajdują się w kategorii Audiobook / CD
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-05-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

$sSql = "
UPDATE products
SET type = '1'
WHERE id IN (
  SELECT id FROM (
    SELECT P.id
    FROM products_extra_categories AS EC
    JOIN products AS P ON EC.product_id = P.id
    WHERE EC.page_id =51
    AND P.type <>  '1'
  ) AS TMP
);";
Common::Query($sSql);

$sSql ="
UPDATE 
products, products_shadow
SET
products_shadow.type = products.type
WHERE 
products_shadow.id=products.id AND
products_shadow.type <> products.type;";
Common::Query($sSql);        
        
