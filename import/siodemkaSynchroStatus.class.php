<?php
/**
 * Skrypt importu produktów ze źródła Super siódemka do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Korecki Omnia.pl - 2012
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');

include_once($_SERVER['DOCUMENT_ROOT'].'/import/importCSVController.class.php');	


$bTestMode = FALSE; //  czy test mode
$iLimitIteration = 900000000;
$sSource = 'siodemka';
$sElement = 'status';
$aColsToFgetcsv = array(
  'INDEKS', // indeks siodemki - "KAMA-003"
  'STAN', // stan magazynowy - 7.0
  'CENAN', // cena netto super 7 - 7.07
  'VAT', //  St.VAT - " 5%"
  'MIN', // Stan MIN - 0.0
  'MAX', // Stan MAX - 0.0
  'C_STD_NETTO', // cena sztywna netto - 0.0
  'CENA', // cena det. brutto - 11.0
  'S' // ????? 
);

$oImportCSVController = new importCSVController($sSource, $sElement, $bTestMode, $iLimitIteration);
if ($bTestMode == false) {
	// pobieramy na dzień dobry wszystkie produkty
	$sFilename = $oImportCSVController->oSourceElements->importSaveCSVData();
	
} else {
	// link testowy na sztywno aby nie pobierać
	$sFilename = 'CSV/siodemka/STD_SKR_18022014.TXT';
}
	 
if ($sFilename !== false && $sFilename != '') {
	
	// mamy XML'a przechodzimy do parsowania
	$oImportCSVController->parseCSVFile($sFilename, ",", TRUE, $aColsToFgetcsv);
  $oImportCSVController->oSourceElements->unpublishOutOfDateProducts();
  /*
	dump($oImportCSVController->aLog);
	dump($oImportCSVController->oSourceElements->aLog);
	dump($oImportCSVController->aErrorLog);
	dump($oImportCSVController->oSourceElements->aErrorLog);
  */
  if (!empty($oImportCSVController->aErrorLog) || !empty($oImportCSVController->oSourceElements->aErrorLog)) {
    $oImportCSVController->sendInfoMail("IMPORT ".$sSource." ".$sElement." ERR", print_r($oImportCSVController->aErrorLog, true).
                                                          print_r($oImportCSVController->oSourceElements->aErrorLog, true));
  }
} else {
	// TODO komunikat błędu pobierania/zapisu pliku XML
	
}
	 
	 
// TODO mail z informacją o imporcie

echo 'koniec';
