<?php
/**
 * Skrypt importu produktów ze źródła Ateneum do Profit24.pl
 * 
 * @author Arkadiusz Golba
 * @copyright Marcin Chudy - 2015
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny - 
ini_set("memory_limit", '512M');


include_once('modules/m_oferta_produktowa/Module.class.php');
$stdClass = new stdClass();
$oModule = new Module($stdClass, $stdClass, TRUE);


/* coś tu nie działa za dobrze
$sSql = 'SELECT PS.id
FROM products_stock AS PS
JOIN products AS P
    ON P.id = PS.id
LEFT JOIN orders_items AS OI
    ON OI.product_id = PS.id
LEFT JOIN orders AS O
    ON O.id = OI.order_id
    AND (
        O.order_status = "-1"
     OR O.order_status = "0"
     OR O.order_status = "1"
     OR O.order_status = "2"
     OR O.order_status = "3"
    )
WHERE 
  O.id IS NULL AND
  (azymut_status = "0" OR azymut_status = "") AND
  (helion_status = "0" OR helion_status = "") AND
  (profit_e_status = "0" OR profit_e_status = "") AND
  (profit_j_status = "0" OR profit_j_status = "") AND
  (profit_g_status = "0" OR profit_g_status = "") AND
  (dictum_status = "0" OR dictum_status = "") AND
  (siodemka_status = "0" OR siodemka_status = "") AND
  (olesiejuk_status = "0" OR olesiejuk_status = "") AND
  (ateneum_status = "0" OR ateneum_status = "") AND
  (platon_status = "0" OR platon_status = "") AND
  created_by = "abe"
ORDER BY PS.id ASC
LIMIT 15000
';
$aProductIds = Common::GetCol($sSql);
echo implode(', ', $aProductIds);
include_once('modules/m_oferta_produktowa/Module.class.php');
$stdClass = new stdClass();
$oModule = new Module($stdClass, $stdClass, TRUE);
foreach ($aProductIds as $iId) {
  $oModule->deleteItem($iId);
}
