<?php
/**
 * Przetwarzanie kategorii heliona wrzucenie do tabeli products_helion_categories,
 *	dodanie parent_id w przypadku kiedy jest zdefiniowany
 */
ob_start();
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

include_once ('import_func.inc.php');


include_once('CommonSynchro.class.php');


$aProducts = [
    '48154750116',
    '123065090916',
    '8371877560',
    '8387148547',
    '8387148601',
    '8388904929',
    '8390373114',
    '9770481547608',
    '9771896194609',
    '9772082121003',
    '9780194205153',
    '9780194387576',
    '9780194442305',
    '9780194501613',
    '9780194502115',
    '9780194598668',
    '9780194806046',
    '9780521544603',
    '9780521649971',
    '9780582403994',
    '9780582825284',
    '9781107466081',
    '9781107482975',
    '9781292134796',
    '9781337273985',
    '9781405009904',
    '9781405076128',
    '9781408259481',
    '9781408267172',
    '9781408276105',
    '9781408288580',
    '9781471508363',
    '9781471526886',
    '9781471567056',
    '9781565812314',
    '9781843255093',
    '9782011551320',
    '9782011558732',
    '9783194567894',
    '9783197116747',
    '9788301142087',
    '9788301149451',
    '9788301157180',
    '9788301174293',
    '9788301179977',
    '9788301196066',
    '9788302161476',
    '9788320819731',
    '9788320822212',
    '9788325511159',
    '9788325518967',
    '9788325519360',
    '9788325528331',
    '9788325538064',
    '9788325539993',
    '9788325543129',
    '9788325561369',
    '9788325561864',
    '9788325569495',
    '9788325590369',
    '9788326415920',
    '9788326432897',
    '9788326440748',
    '9788326440823',
    '9788326480195',
    '9788326480546',
    '9788326492556',
    '9788326493966',
    '9788326496479',
    '9788326497377',
    '9788326953262',
    '9788326955679',
    '9788327801128',
    '9788360229415',
    '9788360415894',
    '9788360550380',
    '9788360562529',
    '9788360562789',
    '9788360833858',
    '9788361243489',
    '9788362008490',
    '9788362008773',
    '9788362482139',
    '9788362813636',
    '9788363962012',
    '9788364512995',
    '9788365049193',
    '9788365049285',
    '9788365611062',
    '9788365611192',
    '9788365789211',
    '9788365817167',
    '9788365884961',
    '9788372283849',
    '9788372516558',
    '9788372516633',
    '9788372856555',
    '9788373224696',
    '9788373838949',
    '9788373960879',
    '9788374406734',
    '9788374407175',
    '9788376210537',
    '9788376210735',
    '9788376214313',
    '9788376415819',
    '9788376664545',
    '9788377350652',
    '9788377882979',
    '9788377885239',
    '9788377887592',
    '9788378043379',
    '9788378064930',
    '9788378431688',
    '9788378491217',
    '9788378923237',
    '9788378985914',
    '9788379730728',
    '9788380192225',
    '9788380850545',
    '9788380851849',
    '9788380854352',
    '9788380922617',
    '9788380924031',
    '9788380924888',
    '9788381072229',
    '9788381076142',
    '9788381230254',
    '9788381241908',
    '9788389635532',
    '9788394367121',
    '9788394833121',
    '9788394969202',
    '9788477115182',
    '9788498489361'
];
global $pDbMgr;

$commonSynchro = new CommonSynchro();
foreach ($aProducts as $productEan) {
    $sSqlSelect = 'SELECT id, wholesale_price
									 FROM products
									 WHERE 
										isbn_plain LIKE "'.$productEan.'" OR 
										isbn_10 LIKE "'.$productEan.'" OR 
										isbn_13 LIKE "'.$productEan.'" OR 
										ean_13 LIKE "'.$productEan.'" OR 
                    streamsoft_indeks LIKE "'.$productEan.'" OR 
                    standard_code LIKE "'.$productEan.'"
                    LIMIT 1
                    ';
    $productData = $pDbMgr->GetRow('profit24', $sSqlSelect);

    if (!empty($productData)) {
        if ($productData['wholesale_price'] <= 0.00) {
            $sSql = 'SELECT 
            azymut_wholesale_price,
            profit_e_wholesale_price,
            profit_g_wholesale_price, 
            dictum_wholesale_price,
            siodemka_wholesale_price, 
            ateneum_wholesale_price,
            platon_wholesale_price,
            panda_wholesale_price
        FROM products_stock
        WHERE id = '.$productData['id'].'
        ';
            $result = $pDbMgr->GetRow('profit24', $sSql);
            asort($result);
            foreach ($result as $key => $value) {
                if ($value > 0.00) {
                    $productData['wholesale_price'] = $value;
                    break;
                }
            }
        }

        echo $productEan.';'.Common::formatPrice($productData['wholesale_price'])."\r\n";
    } else {
        echo $productEan.';NIE ODNALEZIONO PRODUKTU'."\r\n";
    }
}