<?php
/**
 * Skrypt przypomina o nieaktywnych zamówieniach użytkowników, którzy nie dokonali aktywacji po 24 godzinach od złożenia zamówienia
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-08-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
global $pDbMgr;
//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL ); 
$aConfig['use_db_manager'] = '1';
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

//ini_set('memory_limit', '128M');
$aConfig['common']['base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'http://www.profit24.com.pl';
$aConfig['common']['client_base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['client_base_url_http_no_slash'] = 'http://www.profit24.com.pl';

// dolaczenie biblioteki funkcji modulu
include_once('modules/m_oferta_produktowa/client/Common.class.php');

$pSmarty->template_dir 	= $aConfig['common']['client_base_path'].$aConfig['smarty']['template_dir'];
$pSmarty->compile_dir 	= $aConfig['common']['client_base_path'].$aConfig['smarty']['compile_dir'];
$pSmarty->config_dir 		= $aConfig['common']['client_base_path'].$aConfig['smarty']['config_dir'];
$pSmarty->cache_dir 		= $aConfig['common']['client_base_path'].$aConfig['smarty']['cache_dir'];



	/**
	 * Metoda ustawia konfigurację serwisu
	 *
	 * @global array $aConfig 
	 */
	function getWebsiteSettingsS($sWebsite) {
		global $aConfig, $pDbMgr;
		
		// pobieramy ustawienia
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."website_settings
						LIMIT 1";
		return $pDbMgr->GetRow($sWebsite, $sSql);
	}// end of getWebsiteSettingsS() method

	
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
	
	
	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getWebsiteId($iOId) {
		global $aConfig;
		
		$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id=".$iOId;
		return Common::GetOne($sSql);
	}// end of getWebsiteId() method
	
	
	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."orders_mails_times
							 WHERE id = ".getModuleId('m_zamowienia');
//	$aMailSettingsServise['it'] =& $pDbMgr->GetRow('it', $sSql);
	$aMailSettingsServise['profit24'] =& $pDbMgr->GetRow('profit24', $sSql);
	// pobieramy tresc maila
	
//	$aBankMailServise['it'] = Common::getWebsiteMail('zamowienia_przelew_przyp', 'it');
//	$aPlatnosciMailServise['it'] = Common::getWebsiteMail('zamowienia_platnosci_przyp', 'it');
  $aEmail['profit24'] = array();
	$aEmail['profit24']['zamowienie_nieaktywne'] = Common::getWebsiteMail('zamowienie_nieaktywne', 'profit24');
  $aEmail['profit24']['zamowienie_i_konto_nieaktywne'] = Common::getWebsiteMail('zamowienie_i_konto_nieaktywne', 'profit24');
	
  // pobieramy zamowienia z nierozeslnym opineo
	$sSql = "SELECT O.*, UA.registred
					FROM ".$aConfig['tabls']['prefix']."orders AS O
          LEFT JOIN orders_mails_send AS OMS
            ON OMS.order_id = OMS.order_id AND O.order_status = OMS.order_status
          JOIN users_accounts AS UA
            ON UA.id = O.user_id
					WHERE OMS.id IS NULL 
            AND O.active = '0' 
            AND O.order_status = '0'
            AND O.internal_status = '0' 
            AND O.order_date < DATE_SUB(CURDATE(), INTERVAL ".$aMailSettingsServise['profit24']['unactive_reminder']." DAY)
            ";
	$aOrders = Common::GetAll($sSql);
	if(!empty($aOrders))	{
		foreach($aOrders as $iKey=>$aOrder){

      $aValues = array(
        'order_status' => $aOrder['order_status'],
        'internal_status' => $aOrder['internal_status'],
        'order_id' => $aOrder['id']
      );
      if (Common::Insert($aConfig['tabls']['prefix']."orders_mails_send", $aValues) !== FALSE) {

        
        $sWebsite = getWebsiteSymbol(getWebsiteId($aOrder['id']));
        $aSettings =& getWebsiteSettingsS($sWebsite);
        /*
        $aConfig['_settings']['site'] =& $aSettings; 

        $aBankMail = $aBankMailServise[$sWebsite];
        $aPlatnosciMail = $aPlatnosciMailServise[$sWebsite];
        $aFreeDays = $aFreeDaysServise[$sWebsite];
        $aMailSettings = $aMailSettingsServise[$sWebsite];
         */
        if ($aOrder['registred'] == '1') {
          // z rejestracją
          $aContentEmail = $aEmail[$sWebsite]['zamowienie_i_konto_nieaktywne'];

          $sOrderNumber = rawurldecode(htmlspecialchars(str_replace('/', '-', $aOrder['order_number'])));
          $sToken = md5($aOrder['order_date'].':'.$aOrder['id'].':'.'order_register_active');
          $sActivationLink = 'https://www.profit24.pl/'.createLink('koszyk', 'order_register_active', '', array('on' => $sOrderNumber, 'token' => $sToken));
        } else {
          // bez rejestracji
          $aContentEmail = $aEmail[$sWebsite]['zamowienie_nieaktywne'];

          $sOrderNumber = rawurldecode(htmlspecialchars(str_replace('/', '-', $aOrder['order_number'])));
          $sToken = md5($aOrder['order_date'].':'.$aOrder['id'].':'.'order_active');
          $sActivationLink = 'https://www.profit24.pl/'.createLink('koszyk', 'order_active', '', array('on' => $sOrderNumber, 'token' => $sToken));
        }

        Common::sendWebsiteMail($aContentEmail, $aSettings['name'], $aSettings['email'], $aOrder['email'], $aContentEmail['content'],
                        array('{nr_zamowienia}', '{link_aktywacyjny}'),
                        array($aOrder['order_number'], $sActivationLink ),
                        array(), array(), $sWebsite);
        dump($aOrder['email']);
      }
		}
	}

	
?>