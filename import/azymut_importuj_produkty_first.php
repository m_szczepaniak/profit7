<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
// zmienne
$iMId = 5;
$iPublisherID = 1;
$iDevLimit=0;
$sLogInfo='';
$bError=false;

$sAzymutLogin='10831';
$sAzymutPasswd='ncYX8PwJ';
$sTransactionCode='';

include_once ('import_func.inc.php');

sendInfoMail("rozpoczęto import",$sLogInfo);

function addToAzymutBuffer($aValues){
	global $bError,$sLogInfo,$iDevLimit;
	
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_azymut_buffer", $aValues)) !== false) {
			// DODANO PRODUKT
			//dump( 'dodano ['.$iId.'] ['.$aValues['azymut_index'].'] '.$aValues['title']."\n");
			$sLogInfo.="Dodano [".$iId."] [".$aValues['azymut_index']."] - ".$aValues['title']."\n";
			$bError=true;
	}
	else{
		// blad dodawania
		dump( 'blad dodawania: ['.$iId.'] ['.$aValues['azymut_index'].'] '.$aValues['title']."<br>\n");
		$sLogInfo.='blad dodawania: ['.$iId.'] ['.$aValues['azymut_index'].'] '.$aValues['title']."\n";
	}
}


/**
 * parsuje xml dla dodania nowego produktu
 * @param $oXml
 * @return unknown_type
 */
function parseAzymutBook(&$oXml,$sAzymutId) {
global $aConfig;
	$aValues = array();
	$aValues['azymut_index']=$sAzymutId;
	$aValues['created']='2009-08-01 12:00:00';
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'book':
						// ksiazka - KONIEC - dodanie do bazy
						addToAzymutBuffer($aValues);
						//dump($aValues);
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'autorzy':
						$aValues['authors'] = getTextNodeValue($oXml);
						break;
					case 'tytul':
						$aValues['title'] = getTextNodeValue($oXml);
					case 'wydawca':
						$aValues['publisher'] = getTextNodeValue($oXml);			
						break;
					case 'podtytul':
						$aValues['subtitle'] = getTextNodeValue($oXml);	
						break;
					case 'liczbatomo':
						$aValues['volumes'] = (int) getTextNodeValue($oXml);			
						break;
					case 'nrkolejnyt':
						$aValues['volume_nr'] =(int)  getTextNodeValue($oXml);			
						break;
					case 'tytultomu':
						$aValues['volume_title'] = getTextNodeValue($oXml);			
						break;
					case 'jezykoryg':
						$aValues['original_language'] = getTextNodeValue($oXml);
						break;
					case 'tytuloryg':
						$aValues['original_title'] = getTextNodeValue($oXml);
						break;
					case 'tlumacze':
						$aValues['translator'] = getTextNodeValue($oXml);
						break;
					case 'jezyki':
						$aValues['language'] = getTextNodeValue($oXml);
						break;
					case 'seriacykl':
						$aValues['seria'] = getTextNodeValue($oXml);
						break;
					case 'tematyka':
						$aValues['subject'] = getTextNodeValue($oXml);
						break;
					case 'kodwydawcy':
						$aValues['publisher_code'] = getTextNodeValue($oXml);			
						break;
					case 'wydanie':
						$aValues['edition'] = getTextNodeValue($oXml);			
						break;
					case 'rokwyd':
						$aValues['publication_year'] = (int) getTextNodeValue($oXml);
						break;
					case 'objetosc':
						$aValues['pages'] = (int) getTextNodeValue($oXml);
						break;
					case 'format':
						$aValues['dimension'] = getTextNodeValue($oXml);
						break;	
					case 'oprawa':
						$aValues['binding'] = getTextNodeValue($oXml);
						break;
					case 'ciezar':
						$aValues['weight'] = getTextNodeValue($oXml);
						break;
					case 'isbn':
						$aValues['isbn'] = getTextNodeValue($oXml);		
						$aValues['isbn_plain'] = isbn2plain($aValues['isbn']);		
						break;	
					case 'opis':
						$sDesc = eregi_replace('<a.*</a>','', clearAsciiCrap($oXml));
						$sDesc = eregi_replace('<script.*</script>','', $sDesc);
						$aValues['description'] = $sDesc;
						break;
					case 'miejscowosc':
						$aValues['city'] = getTextNodeValue($oXml);
						break;
					case 'kod_paskowy':
						// dodane 20.07.2012 @Arkadiusz Golba 
						// na zlecenie Marcin Chudy podczas rozmowy skype 19.07.2012
						// 
						// jeśli nie wystąpił wcześniej isbn
						if (empty($aValues['isbn'])) {
							// to w pole isbn, isbn_plain ladują dane z kodu paskowego
							$aValues['isbn'] = getTextNodeValue($oXml);		
							$aValues['isbn_plain'] = isbn2plain($aValues['isbn']);	
						}
						break;
					case 'typ_pub':
						$aValues['publication_type'] = getTextNodeValue($oXml);
						break;
					case 'image':
						$aValues['image'] = (int) getTextNodeValue($oXml);
						break;
					case 'kompl1':
						$aValues['komplet'] = getTextNodeValue($oXml);
						break;	
				}
			break;
		}
	}
} // end of parseAddProduct()




/*
 * parsuje listę książek <book></book>
 */
function parseBookList(&$oXml) {
global $aConfig;
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT:
				switch ($oXml->name) {
					case 'books':
						// koniec listingu ksiazek
						return;
					break;
				}
			break;
			
			case XMLReader::ELEMENT:
				switch ($oXml->name) {
					case 'book':
							// Index azymut
							$sAzymutId=$oXml->getAttribute('indeks');
							// jesli juz nie dodana ani nie ma jej w buforze
							//if(!existAzymutBook($sAzymutId) && !existAzymutBookInBuffer($sAzymutId))
							if(!existAzymutBookInBuffer($sAzymutId))
								parseAzymutBook($oXml,$sAzymutId);
					break;
				}
			break;
		}
	}
}
// end of parseBookList()


if (file_exists($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/first_azymut_towary.xml")) {
	$oXml = new XMLReader();
	$oXml->open($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/first_azymut_towary.xml");

	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'stuff':
						// KONIEC XMLa
						$oXml->close();
					break;
				}
			break;
		
			case XMLReader::ELEMENT :
				switch ($oXml->name) {
					case 'stuff':
						$sTransactionCode=$oXml->getAttribute('transactionId');
					break;
					case 'books':
						parseBookList($oXml);
						break;
				}
			break;
		}
	}
	//kopia pliku dla testów
	copy($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/first_azymut_towary.xml", $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/oldTowary/first_azymut_towary".time().".xml");
		
	sendInfoMail("Pomyślnie dodano produkty do bufora importu azymut",$sLogInfo);
	
} else {
	sendInfoMail("Brak pliku /XML/first_azymut_towary.xml ",$sLogInfo);
}
?>