<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-08-10
 * Time: 15:01
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');
global $pDbMgr;

$sSql = 'SELECT code FROM websites WHERE url LIKE "http%"';
$aWebsites = $pDbMgr->GetCol('profit24', $sSql);
foreach ($aWebsites as $sWebsite) {
    $sSql = 'SELECT P.id FROM products AS P
             JOIN products_news AS PN
              ON PN.product_id = P.id
             WHERE P.is_news  = "0" ';
    $aProductsIds = $pDbMgr->GetCol($sWebsite, $sSql);
    foreach ($aProductsIds AS $iProdId) {
            $sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_news
                             WHERE product_id = ".$iProdId;
            if($pDbMgr->Query($sWebsite, $sSql) === false) {
                echo "Error deleting from news <br>\n";
                dump($sSql);
            }
    }
}