<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);

include_once ('import_func.inc.php');
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();

$sLogInfo='';

$sIsbnField='CERTYFOPIS'; // pole z ktoregfo pobierany jest isbn
$iUnsortedCategoryID=$aConfig['import']['unsorted_category'];

$iParseCount = 0;
$aMemDB = array();

/**
 * Przetwarza plik dbf i wrzuca efekt do tablicy w pamięci
 * 
 * @param string $sDbFile - plik bazy
 * @return void
 */
function parseDBFfile($sDbFile, $sSource){
  global $aConfig,$sLogInfo,$sIsbnField, $iParseCount, $aMemDB;


  if (file_exists($sDbFile)){
    // cześcimy tabelę na początku
    $aMemDB[$sSource] = array();

    /** DB CONFIG **/
    $sDSN2 = 'dbase:///'.$sDbFile;
    $pDB2 =& DB::connect($sDSN2);
    if (DB::isError($pDB2)) {
  //		sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się otworzyć pliku wewnętrznej bazy danych ".$sDbFile.$pDB2->getMessage().$pDB2->getUserInfo());
      die("Could not connect to database! ".$pDB2->getMessage().$pDB2->getUserInfo());
    }
            echo 'update';
    $sSQL="SELECT *";
    $pResult =& $pDB2->query($sSQL);
    if (DB::IsError($pResult)) {
      TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
    }
    else {
      while ($aResultRow =& $pResult->fetchRow(DB_FETCHMODE_ASSOC)) {
        $iParseCount++;
        $iYear = 0;
        //dump($aResultRow);
        $sRawIsbn=trim($aResultRow[$sIsbnField]);
        $aIsbn=split(' ',$sRawIsbn,2);
        $sIsbn=isbn2plain(trim($aIsbn[0]));
        if(strlen($sIsbn) < 2){
          $sIsbn='';
        }
        if(!empty($sIsbn) && !empty($aResultRow['NAZWA_ART']) && $aResultRow['deleted'] != '1'){
          $sName=iconv("CP852","UTF-8", $aResultRow['NAZWA_ART']);
          $fPriceBrutto=Common::formatPrice2($aResultRow['CENA_AX']);
          $fPriceNetto=Common::formatPrice2($aResultRow['CENA_A']);
          $fWholesalePriceBrutto=Common::formatPrice2($aResultRow['CENAX']);
          $iVat = intval($aResultRow['STAWKA_VAT']);
          $iDataPub = intval($aResultRow['CERTYFDATA']);
          $iTMPYear = intval(substr($iDataPub, 0, 4));
          if ($iTMPYear > 0) {
            $iYear = $iTMPYear;
          }

          $sLocation=trim(iconv("CP852","UTF-8", $aResultRow['LOKACJA']));

          $aBuff = array();
          if (isset($aMemDB[$sSource][$sIsbn])) {
            $aBuff = $aMemDB[$sSource][$sIsbn];
          }
          if(empty($aBuff)){
            // dodajemy nowa do bufora
            $aValues=array(
              'internal_source' => $sSource,
              'isbn' => $sIsbn,
              'name' => $sName,
              'price_brutto' => $fPriceBrutto,
              'price_netto' => $fPriceNetto,
              'wholesale_price' => $fWholesalePriceBrutto,
              'vat' => $iVat,
              'stock' => (int)$aResultRow['STAN_FAKT'],
              'location' => $sLocation,
              'year' => $iYear
            );
            $aMemDB[$sSource][$sIsbn] = $aValues;
          } else {
            // zwiekszamy stan w buforze
            if((int)$aResultRow['STAN_FAKT'] > 0){
              if($fPriceBrutto > $aBuff['price_brutto']){
                $aBuff['price_brutto'] = $fPriceBrutto;
                $aBuff['price_netto'] = $fPriceNetto;
                $aBuff['vat'] = $iVat;
                $aBuff['wholesale_price'] = $fWholesalePriceBrutto;
              }
              $aMemDB[$sSource][$sIsbn]['stock'] += $aResultRow['STAN_FAKT'];
              $aMemDB[$sSource][$sIsbn]['price_brutto'] = $aBuff['price_brutto'];
              $aMemDB[$sSource][$sIsbn]['price_netto'] = $aBuff['price_netto'];
              $aMemDB[$sSource][$sIsbn]['wholesale_price'] = $aBuff['wholesale_price'];
              if (!empty($sLocation)) {
                if (!empty($aBuff['location'])) {
                  $aMemDB[$sSource][$sIsbn]['location'] = $aBuff['location'].',';
                }
                $aMemDB[$sSource][$sIsbn]['location'] .= $aBuff['location'];
              }
            }
          }
        }
      }
      unset($pResult);
    } 
    @unlink($sDbFile);
  }else {
    echo "missing file ".$sDbFile."\n <br>";
  }
} // end of parseDBFfile() function


/**
 * Metoda aktualizuje ostatnią aktualizację stanu produktu w źródle
 * 
 * @global array $aConfig
 * @param string $sFileCode
 * @param integer $iId
 * @return bool
 */
function updateStockLastImport($sFileCode, $iId) {
	global $aConfig;
	
	if ($iId > 0) {
		$aValues = array(
				'profit_'.$sFileCode.'_last_import' => 'NOW()'
		);
		return Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues, "id = ".$iId);
	}
	return true;
}// end of updateStockLastImport() method


/**
 * Prazetwarza tabele z buforem importu wewnetrznego i dodaje/aktualizuje produkty
 * @param string $sSource - opis źródła importu
 * @param int $iNewsCategory - id kategorii nowości
 * @param int $iCat - id kategorii głównej produktu
 * @param char $sFileCode - kod pliku źrodła (j/e/m)
 * @return void
 */
function processInternalBuffer($sSource, $iNewsCategory,$iCat,$sFileCode){
  global $aConfig, $sLogInfo, $iUnsortedCategoryID, $oCommonSynchro, $aMemDB;

  if (empty($aMemDB[$sFileCode])) {
    echo "PUSTY ".$sFileCode."\n";
  }
  foreach ($aMemDB[$sFileCode] as $aBufferItem) {
		$aProductStock = array();
		$aBookStock = array();
		
		$sIsbn=isbn2plain(trim($aBufferItem['isbn']));
		
		$aProductPattern = array(
				'isbn_plain' => $aBufferItem['isbn']
		);
		$aProduct = $oCommonSynchro->refMatchProductByISBNs($aProductPattern, array('id', 'prod_status, published, source, publisher_id'));
		
		if (intval($aProduct['id']) > 0) {
			$aProductStock = $oCommonSynchro->getProductStock($aProduct['id']);
		}
		if (!empty($aProductStock) && !empty($aProduct) && is_array($aProduct)) {
			$aBookStock = array_merge($aProductStock, $aProduct);
		} 
		/*
		// aktualizacja last import
		if (updateLastImport($aBookStock['id']) === false) {
			sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import - id produktu '.$aBookStock['id']);
			echo("Błąd zmiany last_import ".$aBookStock['id']."<br/>\n");
			$sLogInfo.= "Błąd zmiany last_import ".$aBookStock['id']."\n";
		}
		
		// aktualizacja last_improt w products_stock @Arkadiusz Golba 02.10.2012
		if (updateStockLastImport($sFileCode, $aBookStock['id']) === false) {
			sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import w products_stock - id produktu '.$aBookStock['id']);
			echo("Błąd zmiany last_import w products_stock ".$aBookStock['id']."<br/>\n");
			$sLogInfo .= "Błąd zmiany wewnetrzny last_import w products_stock ".$aBookStock['id']."\n";
		}
		*/
    
		if($aBufferItem['stock'] > 0){
  			$iStatus='1';
  	}	else{
  			$iStatus='0';
  	}
  	// dodajemy nową książkę 
		if(empty($aBookStock)){
			$aCats=array();
			$aCats[]=$iCat;
			
			$aValues=array(
				'page_id' => $iCat,
		    'name' => $aBufferItem['name'],
			  'plain_name' => $aBufferItem['name'],
			  'published' => '0',
				'reviewed' => '1',
			  'isbn' => $sIsbn, 
			  'isbn_plain' => $sIsbn,
  			'price_brutto' => $aBufferItem['price_brutto'],
				'price_netto' => $aBufferItem['price_netto'],
				'vat' => $aBufferItem['vat'],
				'last_import' => 'NOW()',
			  'created' => 'NOW()',
			  'created_by' => $sSource
			);
  		if (($PId = Common::Insert($aConfig['tabls']['prefix']."products", $aValues)) === false) {
				echo "błąd dodawania produktu <br>\n";
				$sLogInfo.= "błąd dodawania produktu ".$aValues['name']." \n";
				dump($aValues);
			}
			else{
				//echo "dodano produkt ".$PId." - ".$aValues['name']."<br>\n";
				$sLogInfo.= "dodano produkt ".$PId." - [".$sIsbn."] ".$aValues['name']."\n";
				// aktualizacja stanów
				$aValues = array(
					'profit_'.$sFileCode.'_status' => $aBufferItem['stock'],
					'profit_'.$sFileCode.'_price_brutto' => $aBufferItem['price_brutto'],
					'profit_'.$sFileCode.'_price_netto' => $aBufferItem['price_netto'],
          'profit_'.$sFileCode.'_wholesale_price' => $aBufferItem['wholesale_price'],
					'profit_'.$sFileCode.'_vat' => $aBufferItem['vat'],
					'profit_'.$sFileCode.'_shipment_time' => '0',
					'profit_'.$sFileCode.'_last_import' => 'NOW()',
					'profit_'.$sFileCode.'_location' => $aBufferItem['location']
				);
				if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues,"id = ".$PId)) === false) {
										echo("błąd aktualizacji stanu produktu ".$PId."<br/>\n");
										$sLogInfo.= "błąd aktualizacji stanu produktu ".$PId."\n";
				}
				if($iStatus == '1') {
					addNews($PId,$iNewsCategory);
					addExtraCategories($PId,$aCats);
				}
			}
		} else {
			// aktualizacja last import
      /*
			if (updateLastImport($aBookStock['id']) === false) {
				echo("Błąd zmiany last_import ".$aBookStock['id']."<br/>\n");
				$sLogInfo.= "Błąd zmiany last_import ".$aBookStock['id']."\n";
			}
			*/
      
			// jeśli jest to zapowiedź to ma zostać usunięta
			$aPreviews = $oCommonSynchro->getPreviews($aBookStock['id']);
			if (!empty($aPreviews)) {
				if ($oCommonSynchro->delPreviews($aBookStock['id']) === false) {
					echo("błąd usuwania zapowiedzi produktu ".$aBookStock['id']."<br/>\n");
					$sLogInfo.= "błąd usuwania zapowiedzi produktu ".$aBookStock['id']."\n";
				}
				// jeśli usunęliśmy z zapowiedzi to dodajemy do nowości, jeśli rok jest aktualnym rokiem
				if ($aBufferItem['year'] > 0 && $aBufferItem['year'] == date('Y')) {
					$aNews = getNews($aBookStock['id']);
					if (empty($aNews)) {
						addNews($aBookStock['id'],$iNewsCategory);
					}
				}
			}
			
      
      // korekta ceny brutto + dodajemy załącznik
      $fAttCorrection = 0;
      // czyli jst to pakiet i są różne ceny
      $aAtt = getAttachment($aBookStock['id']);
      if(!empty($aAtt) && $aAtt['price_brutto'] > 0.00){
        $fAttCorrection = $aAtt['price_brutto'];
      }
      
			if( ($aBookStock['profit_'.$sFileCode.'_status'] != $aBufferItem['stock']) ||
					($aBookStock['profit_'.$sFileCode.'_price_netto'] != $aBufferItem['price_netto']) ||
          ($aBookStock['profit_'.$sFileCode.'_wholesale_price'] != $aBufferItem['wholesale_price']) || 
          ($aBookStock['profit_'.$sFileCode.'_price_brutto'] != ($aBufferItem['price_brutto']+$fAttCorrection)) ||
					($aBookStock['profit_'.$sFileCode.'_location'] != $aBufferItem['location'])  ||
					($aBookStock['profit_'.$sFileCode.'_vat'] != vatMapper($aBufferItem['vat'])) ){
        

				// aktualizacja stanów
				$aValues = array(
					'profit_'.$sFileCode.'_status' => $aBufferItem['stock'],
					'profit_'.$sFileCode.'_price_brutto' => $aBufferItem['price_brutto']+$fAttCorrection,
					'profit_'.$sFileCode.'_price_netto' => $aBufferItem['price_netto'],
          'profit_'.$sFileCode.'_wholesale_price' => $aBufferItem['wholesale_price'],
					'profit_'.$sFileCode.'_vat' => vatMapper($aBufferItem['vat']),
					'profit_'.$sFileCode.'_shipment_time' => '0',
					'profit_'.$sFileCode.'_last_import' => 'NOW()',
					'profit_'.$sFileCode.'_location' => $aBufferItem['location']
				);
				if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues,"id = ".$aBookStock['id'])) === false) {
										echo("błąd aktualizacji stanu produktu ".$aBookStock['id']."<br/>\n");
										$sLogInfo.= "błąd aktualizacji stanu produktu ".$aBookStock['id']."\n";
				} else {
          if (updateStockLastImport($sFileCode, $aBookStock['id']) === false) {
            sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import w products_stock - id produktu '.$aBookStock['id']);
            echo("Błąd zmiany last_import w products_stock ".$aBookStock['id']."<br/>\n");
            $sLogInfo .= "Błąd zmiany wewnetrzny last_import w products_stock ".$aBookStock['id']."\n";
          }
				}
			} else {
        if (updateStockLastImport($sFileCode, $aBookStock['id']) === false) {
          sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import w products_stock - id produktu '.$aBookStock['id']);
          echo("Błąd zmiany last_import w products_stock ".$aBookStock['id']."<br/>\n");
          $sLogInfo .= "Błąd zmiany wewnetrzny last_import w products_stock ".$aBookStock['id']."\n";
        }
      }
		}
	}
  unset($aMemDB[$sFileCode]);
} // end of processInternalBuffer() function


/**
 * Prazetwarza tabele z buforem importu wewnetrznego i dodaje/aktualizuje produkty
 * @param string $sSource - opis źródła importu
 * @param int $iNewsCategory - id kategorii nowości
 * @param int $iCat - id kategorii głównej produktu
 * @param char $sFileCode - kod pliku źrodła (j/e/m)
 * @return void
 */
function processInternalBufferSourceG($sSource, $iNewsCategory,$iCat,$sFileCode){
  global $aConfig, $sLogInfo, $iUnsortedCategoryID, $oCommonSynchro, $aMemDB;

  if (empty($aMemDB[$sFileCode])) {
    echo "PUSTY ".$sFileCode."\n";
  }
  foreach ($aMemDB[$sFileCode] as $aBufferItem) {
		$aProductStock = array();
		$aBookStock = array();
		
		$sIsbn=isbn2plain(trim($aBufferItem['isbn']));
		
		$aProductPattern = array(
				'isbn_plain' => $aBufferItem['isbn']
		);
		$aProduct = $oCommonSynchro->refMatchProductByISBNs($aProductPattern, array('id', 'prod_status, published, source, publisher_id'));
		
		if (intval($aProduct['id']) > 0) {
			$aProductStock = $oCommonSynchro->getProductStock($aProduct['id']);
		}
		if (!empty($aProductStock) && !empty($aProduct) && is_array($aProduct)) {
			$aBookStock = array_merge($aProductStock, $aProduct);
		} 
		/*
		// aktualizacja last import
		if (updateLastImport($aBookStock['id']) === false) {
			sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import - id produktu '.$aBookStock['id']);
			echo("Błąd zmiany last_import ".$aBookStock['id']."<br/>\n");
			$sLogInfo.= "Błąd zmiany last_import ".$aBookStock['id']."\n";
		}
		
		// aktualizacja last_improt w products_stock @Arkadiusz Golba 02.10.2012
		if (updateStockLastImport($sFileCode, $aBookStock['id']) === false) {
			sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import w products_stock - id produktu '.$aBookStock['id']);
			echo("Błąd zmiany last_import w products_stock ".$aBookStock['id']."<br/>\n");
			$sLogInfo .= "Błąd zmiany wewnetrzny last_import w products_stock ".$aBookStock['id']."\n";
		}
		*/
    
		if(($aBufferItem['stock'] - $aProductStock['profit_g_reservations']) > 0){
  			$iStatus='1';
  	}	else{
  			$iStatus='0';
  	}
  	// dodajemy nową książkę 
		if(empty($aBookStock)){
			$aCats=array();
			$aCats[]=$iCat;
			
			$aValues=array(
				'page_id' => $iCat,
		    'name' => $aBufferItem['name'],
			  'plain_name' => $aBufferItem['name'],
			  'published' => '0',
				'reviewed' => '1',
			  'isbn' => $sIsbn, 
			  'isbn_plain' => $sIsbn,
  			'price_brutto' => $aBufferItem['price_brutto'],
				'price_netto' => $aBufferItem['price_netto'],
				'vat' => $aBufferItem['vat'],
				'last_import' => 'NOW()',
			  'created' => 'NOW()',
			  'created_by' => $sSource
			);
  		if (($PId = Common::Insert($aConfig['tabls']['prefix']."products", $aValues)) === false) {
				echo "błąd dodawania produktu <br>\n";
				$sLogInfo.= "błąd dodawania produktu ".$aValues['name']." \n";
				dump($aValues);
			}
			else{
				//echo "dodano produkt ".$PId." - ".$aValues['name']."<br>\n";
				$sLogInfo.= "dodano produkt ".$PId." - [".$sIsbn."] ".$aValues['name']."\n";
				// aktualizacja stanów
				$aValues = array(
					'profit_'.$sFileCode.'_act_stock' => $aBufferItem['stock'],
					'profit_'.$sFileCode.'_price_brutto' => $aBufferItem['price_brutto'],
					'profit_'.$sFileCode.'_price_netto' => $aBufferItem['price_netto'],
          'profit_'.$sFileCode.'_wholesale_price' => $aBufferItem['wholesale_price'],
					'profit_'.$sFileCode.'_vat' => $aBufferItem['vat'],
					'profit_'.$sFileCode.'_shipment_time' => '0',
					'profit_'.$sFileCode.'_last_import' => 'NOW()',
					'profit_'.$sFileCode.'_location' => $aBufferItem['location']
				);
				if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues,"id = ".$PId)) === false) {
										echo("błąd aktualizacji stanu produktu ".$PId."<br/>\n");
										$sLogInfo.= "błąd aktualizacji stanu produktu ".$PId."\n";
				}
				if($iStatus == '1') {
					addNews($PId,$iNewsCategory);
					addExtraCategories($PId,$aCats);
				}
			}
		} else {//$aBookStock['prod_status']!='2'
			// aktualizacja last import
      /*
			if (updateLastImport($aBookStock['id']) === false) {
				echo("Błąd zmiany last_import ".$aBookStock['id']."<br/>\n");
				$sLogInfo.= "Błąd zmiany last_import ".$aBookStock['id']."\n";
			}
			*/
      
			// jeśli jest to zapowiedź to ma zostać usunięta
			$aPreviews = $oCommonSynchro->getPreviews($aBookStock['id']);
			if (!empty($aPreviews)) {
				if ($oCommonSynchro->delPreviews($aBookStock['id']) === false) {
					echo("błąd usuwania zapowiedzi produktu ".$aBookStock['id']."<br/>\n");
					$sLogInfo.= "błąd usuwania zapowiedzi produktu ".$aBookStock['id']."\n";
				}
				// jeśli usunęliśmy z zapowiedzi to dodajemy do nowości, jeśli rok jest aktualnym rokiem
				if ($aBufferItem['year'] > 0 && $aBufferItem['year'] == date('Y')) {
					$aNews = getNews($aBookStock['id']);
					if (empty($aNews)) {
						addNews($aBookStock['id'],$iNewsCategory);
					}
				}
			}
			
      
      // korekta ceny brutto + dodajemy załącznik
      $fAttCorrection = 0;
      // czyli jst to pakiet i są różne ceny
      $aAtt = getAttachment($aBookStock['id']);
      if(!empty($aAtt) && $aAtt['price_brutto'] > 0.00){
        $fAttCorrection = $aAtt['price_brutto'];
      }
      
			if( ($aBookStock['profit_'.$sFileCode.'_act_stock'] != $aBufferItem['stock']) ||
					($aBookStock['profit_'.$sFileCode.'_price_netto'] != $aBufferItem['price_netto']) ||
          ($aBookStock['profit_'.$sFileCode.'_price_brutto'] != ($aBufferItem['price_brutto']+$fAttCorrection)) ||
          ($aBookStock['profit_'.$sFileCode.'_wholesale_price'] != $aBufferItem['wholesale_price']) ||    
					($aBookStock['profit_'.$sFileCode.'_location'] != $aBufferItem['location'])  ||
					($aBookStock['profit_'.$sFileCode.'_vat'] != vatMapper($aBufferItem['vat'])) ){
        

				// aktualizacja stanów
				$aValues = array(
					'profit_'.$sFileCode.'_act_stock' => $aBufferItem['stock'],
					'profit_'.$sFileCode.'_price_brutto' => $aBufferItem['price_brutto']+$fAttCorrection,
					'profit_'.$sFileCode.'_price_netto' => $aBufferItem['price_netto'],
          'profit_'.$sFileCode.'_wholesale_price' => $aBufferItem['wholesale_price'],
					'profit_'.$sFileCode.'_vat' => vatMapper($aBufferItem['vat']),
					'profit_'.$sFileCode.'_shipment_time' => '0',
					'profit_'.$sFileCode.'_last_import' => 'NOW()',
					'profit_'.$sFileCode.'_location' => $aBufferItem['location']
				);
				if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues,"id = ".$aBookStock['id'])) === false) {
										echo("błąd aktualizacji stanu produktu ".$aBookStock['id']."<br/>\n");
										$sLogInfo.= "błąd aktualizacji stanu produktu ".$aBookStock['id']."\n";
				} else {
          if (updateStockLastImport($sFileCode, $aBookStock['id']) === false) {
            sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import w products_stock - id produktu '.$aBookStock['id']);
            echo("Błąd zmiany last_import w products_stock ".$aBookStock['id']."<br/>\n");
            $sLogInfo .= "Błąd zmiany wewnetrzny last_import w products_stock ".$aBookStock['id']."\n";
          }
				}
			} else {
        if (updateStockLastImport($sFileCode, $aBookStock['id']) === false) {
          sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni - Błąd", 'Błąd zmiany last_import w products_stock - id produktu '.$aBookStock['id']);
          echo("Błąd zmiany last_import w products_stock ".$aBookStock['id']."<br/>\n");
          $sLogInfo .= "Błąd zmiany wewnetrzny last_import w products_stock ".$aBookStock['id']."\n";
        }
      }
		}
	}
  unset($aMemDB[$sFileCode]);
} // end of processInternalBufferSourceG() function


	$sDbFile1Src=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_j.dbf';
	$sDbFile2Src=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_n.dbf';
	$sDbFile3Src=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_m.dbf';
	$sDbFile4Src=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_g.dbf';
	$sDbFile5Src=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_x.dbf';
	$sDbFile1=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_j_copy.dbf';
	$sDbFile2=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_n_copy.dbf';
	$sDbFile3=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_m_copy.dbf';
	$sDbFile4=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_g_copy.dbf';
	$sDbFile5=$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/artykuly_x_copy.dbf';

	if (!copy($sDbFile1Src, $sDbFile1)) {
		echo "Nie skopiowano pliku '".$sDbFile1Src."' do '".$sDbFile1."'.<br/>\n";
		$sLogInfo.= "Nie skopiowano pliku '".$sDbFile1Src."' do '".$sDbFile1."'.\n";
	}
	//@unlink($sDbFile1Src);
	if (!copy($sDbFile2Src, $sDbFile2)) {
		echo "Nie skopiowano pliku '".$sDbFile2Src."' do '".$sDbFile2."'.<br/>\n";
		$sLogInfo.= "Nie skopiowano pliku '".$sDbFile2Src."' do '".$sDbFile2."'.\n";
	}
	//@unlink($sDbFile2Src);
	if (!copy($sDbFile3Src, $sDbFile3)) {
//		echo "Nie skopiowano pliku '".$sDbFile3Src."' do '".$sDbFile3."'.<br/>\n";
//		$sLogInfo.= "Nie skopiowano pliku '".$sDbFile3Src."' do '".$sDbFile3."'.\n";
	}
	//@unlink($sDbFile3Src);
	if (!copy($sDbFile4Src, $sDbFile4)) {
		echo "Nie skopiowano pliku '".$sDbFile4Src."' do '".$sDbFile4."'.<br/>\n";
		$sLogInfo.= "Nie skopiowano pliku '".$sDbFile4Src."' do '".$sDbFile4."'.\n";
	}
	//@unlink($sDbFile4Src);
	if (!copy($sDbFile5Src, $sDbFile5)) {
//		echo "Nie skopiowano pliku '".$sDbFile5Src."' do '".$sDbFile5."'.<br/>\n";
//		$sLogInfo.= "Nie skopiowano pliku '".$sDbFile5Src."' do '".$sDbFile5."'.\n";
	}
	//@unlink($sDbFile5Src);
	
	try {
		parseDBFfile($sDbFile1, 'j');
		processInternalBuffer('profit J',424,147,'j');
	  }
	catch(Exception $eE) {
		echo $e->getMessage()."<br/>\n";
		$sLogInfo.= $e->getMessage()."\n";
		}
	try {
		parseDBFfile($sDbFile2, 'e');
		processInternalBuffer('profit E',425,261,'e');
	  }
	catch(Exception $eE) {
		echo $e->getMessage()."<br/>\n";
		$sLogInfo.= $e->getMessage()."\n";
		}
	
	try {
		parseDBFfile($sDbFile3, 'm');
		processInternalBuffer('profit M',424,147,'m');
	  }
	catch(Exception $eE) {
		echo $e->getMessage()."<br/>\n";
		$sLogInfo.= $e->getMessage()."\n";
		}
	
	try {
		parseDBFfile($sDbFile4, 'g');
		processInternalBufferSourceG('profit G',424,147,'g');
	  }
	catch(Exception $eE) {
		echo $e->getMessage()."<br/>\n";
		$sLogInfo.= $e->getMessage()."\n";
		}
	
	try {
		parseDBFfile($sDbFile5, 'x');
		processInternalBuffer('profit X',424,147,'x');
	  }
	catch(Exception $eE) {
		echo $e->getMessage()."<br/>\n";
		$sLogInfo.= $e->getMessage()."\n";
		}
	
  if ($iParseCount > 100) {
    $aSources = array('e', 'j', 'm', 'g', 'x');
    foreach ($aSources as $sSource) {
      if ($sSource == 'g') {
        $sSql = "
        UPDATE products_stock
        SET 
          profit_".$sSource."_status = '0',
          profit_".$sSource."_act_stock = '0'
        WHERE profit_".$sSource."_status > 0
          AND DATE(profit_".$sSource."_last_import) < DATE( DATE_SUB( NOW( ) , INTERVAL 3 HOUR ) ) ";
        Common::Query($sSql);
      } else {
        $sSql = "
        UPDATE products_stock
        SET 
          profit_".$sSource."_status = '0'
        WHERE profit_".$sSource."_status > 0
          AND DATE(profit_".$sSource."_last_import) < DATE( DATE_SUB( NOW( ) , INTERVAL 3 HOUR ) ) ";
        Common::Query($sSql);
      }
    }
  }
    
	
	$sLogInfo = 'ZAKONCZONO IMPORT Z KS. WEWNETRZNEJ '.$sLogInfo."\r\n";
//  if(!empty($sLogInfo))
//   	sendInfoMail("Aktualizacja produków z Wewnętrznej Bazy Księgarni",$sLogInfo);