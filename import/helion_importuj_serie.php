<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);

include_once ('import_func.inc.php');

// zmienne
$iPublisherID = 1;
$sLogInfo='';


function existsHelionSeries($iSerId,$sBookstore) {
	global $aConfig;
	$sSql="SELECT COUNT(1)
					FROM ".$aConfig['tabls']['prefix']."products_helion_series
					WHERE source='".$sBookstore."'
					AND helion_id=".$iSerId;
	return (Common::GetOne($sSql) > 0);
}



function addHelionSeries($sFilename,$sBookstore) {
global $aConfig;
if (file_exists($sFilename)) {
	$oXml = new XMLReader();
	$oXml->open($sFilename);
	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'details':
						// KONIEC LISTINGU KATEGORII
						$oXml->close();
						return;
					break;
				}
			break;
		
			case XMLReader::ELEMENT :
				switch ($oXml->name) {
					case 'item':
						$iSerId = $oXml->getAttribute('id_seria');
						if(!existsHelionSeries($iSerId,$sBookstore)) {
							// dodawanie serii
							$aValues = array (
								'source' => $sBookstore,
								'helion_id' => $iSerId,
								'name' => $oXml->getAttribute('seria')
							);
						//	dump($aValues);
							if ((Common::Insert($aConfig['tabls']['prefix']."products_helion_series", $aValues,'',false)) === false) {
								// blad
								echo "<strong>Blad!</strong> - dodawanie Serii <strong>".$oXml->getAttribute('seria')."</strong><br />";
								exit(0);
							}
						}
						break;
				}
			break;
		}
	}

}
}


if(!downloadHelionXML("http://helion.pl/plugins/new/xml/lista-serie.cgi","helion-lista-serie.xml")){
	die("błąd pobierania xml serii helion");
}

addHelionSeries($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/helion-lista-serie.xml",'helion');

if(!downloadHelionXML("http://onepress.pl/plugins/new/xml/lista-serie.cgi","onepress-lista-serie.xml")){
	die("błąd pobierania xml serii onepress");
}

addHelionSeries($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/onepress-lista-serie.xml",'onepress');

if(!downloadHelionXML("http://sensus.pl/plugins/new/xml/lista-serie.cgi","sensus-lista-serie.xml")){
	die("błąd pobierania xml serii sensus");
}

addHelionSeries($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/sensus-lista-serie.xml",'sensus');

if(!downloadHelionXML("http://septem.pl/plugins/new/xml/lista-serie.cgi","septem-lista-serie.xml")){
	die("błąd pobierania xml serii septem");
}

addHelionSeries($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/septem-lista-serie.xml",'septem');





?>