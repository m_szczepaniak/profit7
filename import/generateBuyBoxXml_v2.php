<?php

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
ini_set('memory_limit', '512M');
$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';


include_once ('import_func.inc.php');
include_once ('ceneo_functions.php');

global $pDbMgr;

$shopDb = 'np';

	$sLog='';
  $sSql = "SELECT *  FROM `products_tags` WHERE `tag` = 'czasopisma'";
  $iTagId = Common::GetOne($sSql);
	//sendInfoMail('createShadow',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto tworzenie tablicy pomocniczej wyszukiwarki");
	$sSql = "SELECT A.id, A.plain_name, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.pages, A.price_brutto, A.description, 
									A.shipment_time, B.name AS publisher_name, 
									C.binding AS binding_name, D.dimension AS dimension_name,
									A.packet, A.weight, A.description_on_create, A.prod_status
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings C
					 	ON C.id=A.binding
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_dimensions D
					 	ON C.id=A.dimension
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id
           LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_tags PT
             ON PT.product_id = A.id AND PT.tag_id = ".$iTagId."
					 WHERE A.published = '1'
                AND PT.tag_id IS NULL
                AND A.packet = '0'
					 			AND (A.prod_status = '1' OR A.prod_status = '3')
					 			AND (A.shipment_time = '0' OR A.shipment_time='1')
					 			AND A.page_id <> ".$aConfig['import']['unsorted_category']."
					 			AND A.product_type IN (\"".implode('", "', $aConfig['common']['allowed_product_type'])."\")
					 			GROUP BY A.id";
	$oProducts = Common::PlainQuery($sSql);
	// AND A.product_type IN (\"".implode('", "', $aConfig['common']['allowed_product_type'])."\")
		
	$sXMLFilename = $_SERVER['DOCUMENT_ROOT'].'/buybox_v2.xml';
	if (!($fp = fopen($sXMLFilename, 'w'))) {
   echo "Nie mogę otworzyc pliku ceneo.xml do zapisu";
   die();
	} else {
    stream_set_write_buffer($fp, 81920);
		$sXml = '<?xml version="1.0" encoding="utf-8"?>
	<offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">
		<group name="books">
	';
		fwrite($fp,$sXml);
		$sXml='';
    
    $aShipmentTimes = getShipmentTimes();

		$i=0;
		while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
			$aCat = getProductCat($aItem['id']);
			if(!empty($aCat['name'])){
        $aUniqeIdents = getUniqueIdents($aItem);
        $bFirst = true;
        foreach ($aUniqeIdents as $sUniqueIdent) {
          $sXml .= generateElement($aItem, $sUniqueIdent, $aCat, $aShipmentTimes, $bFirst, false);
          $bFirst = false;
        }
				fwrite($fp, $sXml);
//				fflush($fp);
				$sXml='';
			}
			if ($i==10) {
				//dump($aItem);
				//be;
			}
			$i++;
		}

		$sXml = '		</group>'.
	'</offers>';

		fwrite($fp,$sXml);
		fclose($fp);
		$sXml='';
		
		
		// Enable user error handling
		libxml_use_internal_errors(true);

		// VALIDACJA
		$tempDom = new DOMDocument();
		$tempDom->load($sXMLFilename);
		if ($tempDom->schemaValidate($_SERVER['DOCUMENT_ROOT'].'import/XSD/Offer_ceneo_v2.xsd') === false) {
				echo 'BŁĄÐ';
				$sErr = libxml_display_errors();

				// XXX błąd walidacji pliku
				// XXX mail 
				$sContent = "BŁĄÐ podczas walidacji pliku: <br />log: <br />".$sErr;
				echo $sContent;
				//simpleSendMail('Błąd eksportu do Virtualo', $sContent, true);
				exit('Błąd generowania XML Ceneo');
		} else {
				echo 'OK';
		}
		
	}


/*


<?xml version="1.0" encoding="utf-8"?>
<offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1"><!--version (wersja szablonu): liczba całkowita, wymagany-->  
  <group name="books"><!--Oferty zgrupowane wg zdefiniowanych przez nas branż (odpowiednich predefiniowanych teraz XML'i). 
                      Grupa musi być jedną z: books, computers, tires, perfumes, music, games, movies, medicines, rims(felgi), grocery(delikatesy). 
                      Możemy rozszerzać tą listę.-->     
    <o id="151" url="http://www.sklep.tel.pl/id=158" price="980" avail="1" set="0"><!--Oferta sklepu;
                                                                                                                      id: tekst (długość 100), wymagany; 
                                                                                                                      url: tekst (długość 2048), wymagany;
                                                                                                                      price: liczba zmiennoprzecinkowa, wymagany, separator: kropka (przecinek spowoduje błąd walidacji); 
                                                                                                                      avail (dostępność): liczba, opcjonalny, dostępne wartości [1,3,7,14,99], domyślnie 99;
                                                                                                                      
                                                                                                                      set (czy zestaw): 0/1, opcjonalny, domyślnie 0-->
      <name><!--Nazwa oferty; tekst (długość 150), wymagany-->
        <![CDATA[Wspaniała książka]]>
      </name>
<cat><![CDATA[Książki/Wspaniałe książki]]></cat><!--cat (kategoria w sklepie): tekst (długość 255), wymagany-->
      <imgs><!--Zdjęcia oferty, opcjonalny-->
        <main url="http://www.sklep.tel.pl/jpg=158"/><!--zdjęcie główne, wymagany;
                                                        url (długość 2048), wymagany;-->
        <i url="http://www.sklep.tel.pl/jpg=158"/><!--zdjęcie dodatkowe, opcjonalny;
                                                        url (długość 2048), wymagany;-->

      </imgs>
      <desc><!--Opis oferty, opcjonalny-->
        <![CDATA[Opis]]>
      </desc>
      <attrs><!--Atrybuty oferty, wymagany-->
        <a name="Autor"><!--name: tekst (długość 512) wymagany-->          
          <![CDATA[Radek Niejadek]]>
        </a>
        <a name="ISBN">

          <![CDATA[832400782]]>
        </a>
        <a name="Ilosc_stron">
          <![CDATA[10]]>
        </a>        
      </attrs>      
    </o>
  </group>
</offers>


*/
?>