<?php

use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use LIB\Helpers\ArrayHelper;

//ini_set( 'display_errors', 'On' ); error_reporting( E_ALL );
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

//ini_set('memory_limit', '128M');


$aConfig['common']['base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'http://www.profit24.com.pl';
$aConfig['common']['client_base_url_http'] = 'http://www.profit24.com.pl/';
$aConfig['common']['client_base_url_http_no_slash'] = 'http://www.profit24.com.pl';


// dolaczenie biblioteki funkcji modulu
include_once('modules/m_oferta_produktowa/client/Common.class.php');

function getDateAfterWorkingDays($iDate,$iDays,&$aFreeDays){
  global $aConfig;
  $iDaysCount=0;
  // dopóki nie osiągniemy potrzebnej ilości dni
  while($iDaysCount < $iDays){
    // dodaj dzień
    $iDate += (24 * 60 * 60);
    $aDate = getdate($iDate);
    // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
    if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
      $iDaysCount++;
    }
  }
  return $iDate;
}

/**
 * 
 * @param int $iOrderId
 * @return array
 */
function getOrdersItemsToDeleteReservation($iOrderId) {
  global $pDbMgr;
  
  $sSql = 'SELECT id, source
           FROM orders_items
           WHERE order_id = ' . $iOrderId . '
             AND deleted = "0" AND packet = "0"';
  return $pDbMgr->GetAll('profit24', $sSql);
}// end of getOrdersItemsToDeleteReservation() method

$fp = fopen($_SERVER['DOCUMENT_ROOT'].'/import/cancel_log.txt', 'a');
fwrite($fp,date('Y-m-d H:i:s')."Rozpoczynam prace\r\n");
// pobieramy ustawienia
$sSql = "SELECT *
         FROM ".$aConfig['tabls']['prefix']."website_settings
         LIMIT 1";
$aSettings =& Common::GetRow($sSql);
$aConfig['_settings']['site'] =& $aSettings; 

$sSql = "SELECT *
         FROM ".$aConfig['tabls']['prefix']."orders_mails_times
             WHERE id = ".getModuleId('m_zamowienia');
$aMailSettings =& Common::GetRow($sSql);

// dni wolne
$sSql = "SELECT UNIX_TIMESTAMP(free_day)
             FROM ".$aConfig['tabls']['prefix']."orders_free_days";
$aFreeDays = Common::GetCol($sSql);

// pobieramy zamowienia 
$sSql="SELECT A.*, DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_format']."') AS order_date, 
              UNIX_TIMESTAMP(A.reminder_sent) AS reminder_sent_stamp
        FROM ".$aConfig['tabls']['prefix']."orders AS A
        WHERE payment_status = '0' AND
              paid_amount = 0.0 AND
              A.reminder_sent IS NOT NULL AND
              (order_status = '1' OR order_status = '2') AND
              dont_cancel = '0' AND 
              payment_type = 'bank_transfer'
              ";
$aOrders = Common::GetAll($sSql);


include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/Module.class.php');  
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');  
$oParent = new stdClass();

$oOrderModule = new Module($pSmarty, $oParent, TRUE);
$oOrderModule->sModule = 'm_zamowienia';

$oReservation = new \omniaCMS\lib\Products\ProductsStockReservations($pDbMgr);
    
$aNewArray = [];
if(!empty($aOrders))	{
  foreach($aOrders as $iKey=>$aOrder) {

    $reservatonManager = new ReservationManager($pDbMgr);
    if (getDateAfterWorkingDays($aOrder['reminder_sent_stamp'], $aMailSettings['cancel_days'], $aFreeDays) <= time()) {
      Common::BeginTransaction();
      
      $oldOrderReservations = $reservatonManager->getDataProvider()->getReservationsByOrderId($aOrder['id']);
      $oldOrderReservations = ArrayHelper::toKeyValues('id', $oldOrderReservations);
      
      try {
        $orderItems = getOrdersItemsToDeleteReservation($aOrder['id']);
        foreach ($orderItems as $aItem) {
          $oReservation->deleteReservation($aItem['source'], $aOrder['id'], $aItem['id']);
        }
        $reservatonManager->removeOrder($aOrder, $oldOrderReservations);
      } catch (Exception $ex) {
        fwrite($fp,date('Y-m-d H:i:s').'Błąd anulowania '.$aOrder['order_number']." " . $ex->getMessage() . "\r\n");
        $reservatonManager->rollbackChanges();
        Common::RollbackTransaction();
      }
      
      
      $aNewArray[] = $aOrder;
      $aValues = array(
        'status_5_update' => 'NOW()',
        'status_5_update_by' => 'automat',
        'order_status' => "5",
        'internal_status' => 'A'
      );
      Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$aOrder['id']);
      fwrite($fp,date('Y-m-d H:i:s').'Anuluje '.$aOrder['order_number']."\r\n");
      Common::CommitTransaction();
      
      $oOrderModule->sendOrderStatusMail($aOrder['id']);
    }
  }
}
dump(count($aNewArray));
dump($aNewArray);
fclose($fp);
