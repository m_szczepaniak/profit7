<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);


include_once ('import_func.inc.php');
//include_once ('CommonFunctionsHelion.php');
$iDevLimit=0;

$sLogInfo = '';
// zmienne

$sErrLogInfo = '';
$iCountMessages = 0;
$iCountErrMessages = 0;

function addMessLog($sMessage) {
	global $sLogInfo, $iCountMessages;
	
	$iCountMessages++;
	$sLogInfo .= $sMessage.'<br />';
}

function addMessErrLog($sMessage) {
	global $sErrLogInfo, $iCountErrMessages;
	
	$iCountErrMessages++;
	$sErrLogInfo .= $sMessage.'<br />';
}

/*
 * Metoda zamienia status produktu z helion na profit24
 *
 * @param integer $iHelionStatus
 * @return integer
 */
function getNewProdStatus($iHelionStatus) {
	
	switch($iHelionStatus){
		case 1:
		case 2:
			$iNewStatus = '1';
		break;
		case 7:
		case 6:
			$iNewStatus = NULL;
		break;
		default:
			$iNewStatus = '0';
		break;
	}
	return $iNewStatus;
}// end of getNewProdStatus() method


/**
 * Metoda pobiera ceny i status produktu
 *
 * @global array $aConfig
 * @param type $iBId
 * @return type 
 */
function getHelionBookStatusPrices($iBId) {
	global $aConfig;
	
	$sSql = "SELECT helion_status, helion_price_brutto, helion_price_netto, helion_vat, helion_last_import 
					 FROM ".$aConfig['tabls']['prefix']."products_stock
					 id=".$iBId;
	return Common::GetRow($sSql);
}// end of getHelionBookPrices() method


/**
 * Aktualizuj cenę ksiażki
 *
 * @param type $iBId
 * @param type $fPriceBrutto
 * @param type $fPriceNetto
 * @param type $iVat 
 */
function updateBookStock($iBId, $fPriceBrutto, $fPriceNetto, $iVat, $iNewProdStatus) {
	global $aConfig;
	
	// zmien cene produktu
	$aStatusValues = array(
		'helion_status' => $iNewProdStatus,
		'helion_price_brutto' => $fPriceBrutto,
		'helion_price_netto' => $fPriceNetto,
		'helion_vat' => vatMapper($iVat),
		'helion_last_import' => 'NOW()'
	);
	dump($aStatusValues);
	//return true; // TODO USUNAC
	if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aStatusValues,"id = ".$iBId." LIMIT 1")) === false) {
							echo mysql_error();
							echo("błąd aktualizacji stanu produktu ".$iId."<br/>\n");
							$sLogInfo.= "błąd aktualizacji stanu produktu ".$iId."\n";
							die;
	}
	return true;
}

/**
 * Funkcja ustawia helion_last_import
 * 
 * @param int $iBId - id produktu
 */
function lastImport($iBId) {
  global $aConfig;
  
  $aValues = array(
      'helion_last_import' => 'NOW()'
  );
  Common::Update($aConfig['tabls']['prefix']."products_stock", $aValues, "id = ".$iBId." LIMIT 1");
}// end of lastImport() method


/**
 * Funkcja zmienia status produktu
 *
 * @global array $aConfig
 * @param type $iBId
 * @param type $iNewProdStatus
 * @return type 
function updateBookStatus($iBId, $iNewProdStatus) {
	global $aConfig;
	
	return true; // TODO USUNAC
	
	$aValues = array('prod_status' => $iNewProdStatus);
	if (Common::Update($aConfig['tabls']['prefix']."products", $aValues, ' id='.$iBId) === false) {
		return false;
	}
	
	$aValues = array('prod_status' => $iNewProdStatus);
	if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues, ' id='.$iBId) === false) {
		return false;
	} {
		return true;
	}
}
*/

/**
 * Funkcja ustala status pojedynczej książki
 *
 * @param object $oXml 
 */
function parseBookStatus(&$oXml) {
	
	while(@$oXml->read()) {
		switch ($oXml->name) {
			case 'ksiazka':
					$sHelionIdent = $oXml->getAttribute('ident');
					$sISBN = isbn2plain($oXml->getAttribute('isbn'));
					$sEAN = $oXml->getAttribute('ean');
					$fPriceNetto = Common::formatPrice2($oXml->getAttribute('cena_netto'));
					$fPriceBrutto = Common::formatPrice2($oXml->getAttribute('cena_brutto'));
					$iVat = (int)$oXml->getAttribute('vat_procent');
					$aBookPriceStatus = getBookStockWithISBN($sISBN);
          
          $iNeweStatusHelion = getNewProdStatus($oXml->getAttribute('status_ehelion'));
          if (empty($aBookPriceStatus)) {
            // sprawdzmy EAN
            $aBookPriceStatus = getBookStockWithISBN($sEAN); 
          }
          if (!empty($aBookPriceStatus) && $aBookPriceStatus['id'] > 0 && $iNeweStatusHelion !== '0') {
            // aktualizacja helion_last_import jeśli status różny od niedostępnego
            // wtedy mamy pewność, że książki które mają stare helion_last_import
            // powinny być niedostępne
            lastImport($aBookPriceStatus['id']);
          }
					$aBookPriceStatus['helion_status'] = intval($aBookPriceStatus['helion_status']);
					if (!empty($aBookPriceStatus) && $aBookPriceStatus['helion_shipment_time'] != '' && $aBookPriceStatus['id'] > 0) {
						// odnaleziono książkę
						
						if ($iNeweStatusHelion !== NULL && ($aBookPriceStatus['helion_price_brutto'] != $fPriceBrutto ||
								$aBookPriceStatus['helion_price_netto'] != $fPriceNetto ||
								$aBookPriceStatus['helion_vat'] != $iVat || 
								$aBookPriceStatus['helion_status'] != $iNeweStatusHelion )) {
							if (updateBookStock($aBookPriceStatus['id'], $fPriceBrutto, $fPriceNetto, $iVat, $iNeweStatusHelion) === true) {
								// zmien cene produktu
								$aNewValues = array(
									'helion_status' => $iNeweStatusHelion,
									'helion_price_brutto' => $fPriceBrutto,
									'helion_price_netto' => $fPriceNetto,
									'helion_vat' => vatMapper($iVat),
								);
								// zmien cene produktu
								$aOldValues = array(
									'helion_status' => $aBookPriceStatus['helion_status'],
									'helion_price_brutto' => $aBookPriceStatus['helion_price_brutto'],
									'helion_price_netto' => $aBookPriceStatus['helion_price_netto'],
									'helion_vat' => $aBookPriceStatus['helion_vat'],
								);
								$sMsgNew = print_r($aNewValues, true);
								$sMsg = print_r($aOldValues, true);
								addMessLog('Zmiana products_stock id = '.$aBookPriceStatus['id'].' ISBN = '.$sISBN.' EAN = '.$sEAN.' : <br />'.$sMsg.' -> '.$sMsgNew);
							} else {
								addMessErrLog('Błąd - Zmiana products_stock id = '.$aBookPriceStatus['id'].' ISBN = '.$sISBN.' EAN = '.$sEAN.' : <br />'.$sMsg.' -> '.$sMsgNew);
							}
						} else {
							// Takie same
						}

						/*
						if ($iNeweStatusHelion != $aBookPriceStatus['prod_status']) {
							// aktualizuj
							if (updateBookStatus($aBookPriceStatus['id'], $iNeweStatusHelion) === true) {
								addMessLog('Zmiana statusu produktu id = '.$aBookPriceStatus['id'].' ISBN = '.$sISBN.' EAN = '.$sEAN.' : '.$aBookPriceStatus['prod_status'].' -> '.$iNeweStatusHelion);
							} else {
								addMessErrLog('Błąd - Zmiana ceny id = '.$aBookPriceStatus['id'].' ISBN = '.$sISBN.' EAN = '.$sEAN.' : '.$aBookPriceStatus['prod_status'].' -> '.$iNeweStatusHelion);
							}
						}
						 */
					}
			break;
		}
	}
}// end of parseBookStatus() method

/**
 * Metoda parsuje plik XML ze statusami
 *
 * @param string $sFilename 
 */
function parseSatusFile ($sFilename) {
	$oXml = new XMLReader();
	$oXml->open($sFilename);

	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'details':
						// KONIEC LISTINGU KATEGORII
						$oXml->close();
					break;
				}
			break;

			case XMLReader::ELEMENT :
				switch ($oXml->name) {
					case 'lista':
						case XMLReader::ELEMENT :
							parseBookStatus($oXml);
						break;
						
					break;
				}
			break;
		}
	}
}// end of parseSatusFile() method

$aFilesNames = array(
		'statusy-helion.xml', 
		'statusy-onepress.xml',
		'statusy-sensus.xml',
		'statusy-septem.xml'
);

foreach ($aFilesNames as $sFileName) {
	/*
	* TODO ODKOMENTOWAC
	*/
	if(!downloadHelionXML("http://helion.pl/xml/".$sFileName,$sFileName)){
		sendInfoMail('Błąd importu statusów - '.$sFileName,"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml produktow z helion.pl ".$sFileName);
		die("błąd pobierania xml produktow");
	}

	parseSatusFile($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sFileName);
	dump('<strong><h1>'.$sFileName.'</h1></strong>');
	dump($iCountMessages);
	dump($iCountErrMessages);
	dump($sLogInfo);
	dump($sErrLogInfo);
	$iCountMessages = 0;
	$iCountErrMessages = 0;
	$sLogInfo = '';
	$sErrLogInfo = '';
}
echo 'KONIEC';

?>
