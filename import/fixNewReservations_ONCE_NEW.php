<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-08-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */
/**
 * Description of fixReservations
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

include_once ('import_func.inc.php');

$sSql = '
DELETE  FROM `orders_items_lists` WHERE `created` < \'2015-12-31 00:00:00\'
LIMIT 300;


UPDATE `orders_items_lists` SET `books_serialized` = \'\', `books_to_send_serialized` = \'\', `scanned_books_serialized` = \'\' WHERE closed = \'1\' LIMIt 1000;

DELETE FROM products_stock_supplies WHERE id IN
(
  SELECT id FROM (
    SELECT PSS.id FROM products_stock_supplies AS PSS
    LEFT JOIN `products_stock_supplies_to_reservations` AS PSSTR ON PSS.id = PSSTR.`products_stock_supplies_id`
    WHERE PSSTR.id IS NULL AND PSS.reservation > 0
      ANd last_update < "2016-09-09 10:04:21"
  ) AS TMP
);


DELETE FROM products_stock_supplies WHERE id IN
(
  SELECT id FROM (
    SELECT PSS.product_id, PSS.id FROM products_stock_supplies AS PSS
    LEFT JOIN `products_stock_supplies_to_reservations` AS PSSTR ON PSS.id = PSSTR.`products_stock_supplies_id`
    WHERE PSSTR.id IS NULL AND PSS.reservation > 0
  ) AS TMP
);


SELECT *  FROM `orders` AS O
JOIN products_stock_reservations AS PSR
ON O.id = PSR.order_id
WHERE `erp_export` IS NOT NULL AND PSR.move_to_erp = "0"
ORDER BY `order_date` DESC;

DELETE FROM products_stock_reservations
WHERE id IN (
  SELECT id FROM (
    SELECT PSR.id FROM products_stock_reservations AS PSR
    JOIN orders AS O
    ON O.id = PSR.order_id AND erp_export IS NOT NULL
    WHERE PSR.move_to_erp = \'0\'
  ) AS TMP
);
';