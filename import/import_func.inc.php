<?php
$aTopCategories=array();
$aLevel2Categories=array();
/** 
 *  *************** Azymut ******************************
 */

/**
 * Sprawdza czy isnieje kategoria zymut o podanym id
 * @param int $iId - id kategorii azymut
 * @return bool
 */
function existAzymutCategory($iId) {
	global $aConfig;
	
	$sSql = "SELECT count(id)
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
					 WHERE id = ".$iId;	
	return (Common::GetOne($sSql) > 0);
} // end of existAzymutCategory() function


/**
 * Metoda pobiera id źródła dla danego importu
 *
 * @global type $aConfig
 * @global type $pDbMgr
 * @param string $sSourceSymbol - symbol źródła
 * @return type 
 */
function getSourceId($sSourceSymbol) {
  global $aConfig, $pDbMgr;

  $sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."sources
           WHERE symbol='".$sSourceSymbol."'";
  return $pDbMgr->GetOne('profit24', $sSql);
}// end of getSourceId() method

/**
 * Sprawdza czy istnieje powiązanie między kategorią a indeksem azymut
 * @param int $iId - id kategorii
 * @param string $sInd - indeks produktu azymut
 * @return bool
 */
function existAzymutInd2Cat($iId,$sInd) {
	global $aConfig;
	
	$sSql = "SELECT count(azymut_category)
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_ind2cat
					 WHERE azymut_category = ".$iId." AND azymut_index = '".$sInd."'";	
	return (Common::GetOne($sSql) > 0);
} // end of existAzymutInd2Cat() function

/**
 * Pobiera tablicę id kategorii Azymut dla danego indeksu
 * @param string $sInd - indeks azymut
 * @return array ref - tablica z id kategorii
 */
function &getAzymutCats($sInd){
	global $aConfig;
	$sSql ="SELECT DISTINCT azymut_category
					FROM ".$aConfig['tabls']['prefix']."products_azymut_ind2cat
					WHERE azymut_index = '".$sInd."'";
	return Common::GetCol($sSql);
} // end of getAzymutCats() function

/**
 * Rekurencyjnie pobiera ścieżkę kategorii rodziców dla danej kategorii Azymut
 * @param int $iId - id kategorii
 * @return string - tekstowa ścieąka kategorii
 */
function getAzymutParentPath($iId) {
	global $aConfig;
	
	$sSql="SELECT name, parent_id
				FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
				WHERE id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return ((!empty($aItem['parent_id'])&&($aItem['parent_id']!=$iId))?getAzymutParentPath($aItem['parent_id']):'').$aItem['name'].' / ';
} // edn of getAzymutParentPath() function

/**
 * Pobiera tablicę z mapowaniami kategorii (id kategorii ksiegarni)
 * dla id kategorii azymut zawartych w tablicy
 * @param array ref $aAzymutCats - tablica z id kategorii azymut
 * @return array ref
 */
function &getAzymutMappings(&$aAzymutCats){
	global $aConfig;
	$iUnsortedCategoryID=$aConfig['import']['unsorted_category'];
	$sSql ="SELECT DISTINCT A.page_id
					FROM ".$aConfig['tabls']['prefix']."products_azymut_mapping A
					WHERE A.azymut_category IN (".implode(',',$aAzymutCats).")";
	$aMappings=Common::GetCol($sSql);
	if(empty($aMappings))
		return array($iUnsortedCategoryID);
	else
		return $aMappings;
} // end of getAzymutMappings() function

/**
 * Sprawdza czy w tabeli produktów istnieje książka z danym indeksem azymut
 * @param string $sId - indeks azymut
 * @return bool
 */
function existAzymutBook($sId) {
	global $aConfig;
	
	$sSql = "SELECT count(id)
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE azymut_index = '".$sId."'";	
	return (Common::GetOne($sSql) > 0);
} // end of existAzymutBook() function


/**
 * Pobiera status w bazie ksiazki z danym 
 * @param string $sId - indeks azymut
 * @return array ref - tablica z id, statusem produktu i aktualnym źródłem
 */
function &getAzymutStatus($sId) {
	global $aConfig;
	
	$sSql = "SELECT id,prod_status,source
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE azymut_index = '".$sId."'";	
	return Common::GetRow($sSql);
} // end of getAzymutStatus() function


function &getAzymutStock($sId, $sAddCols = '') {
	global $aConfig;
	
	if ($sAddCols != '') {
		$sAddCols = ', '.$sAddCols;
	}
	
		$sSql = "SELECT A.id,A.azymut_status,A.azymut_price_brutto,A.azymut_price_netto,A.azymut_vat,A.azymut_stock,B.prod_status ".$sAddCols."
					 FROM ".$aConfig['tabls']['prefix']."products_stock A
					 JOIN ".$aConfig['tabls']['prefix']."products B
					 ON B.id=A.id
					 WHERE B.azymut_index = '".$sId."'";	
	return Common::GetRow($sSql);
} // end of getAzymutStock() function

function &getAzymutobcStock($sId, $sAddCols = '') {
	global $aConfig;
	
	if ($sAddCols != '') {
		$sAddCols = ', '.$sAddCols;
	}
	
		$sSql = "SELECT A.id,A.azymutobc_status,A.azymutobc_price_brutto,A.azymutobc_price_netto,A.azymutobc_vat,A.azymutobc_stock,B.prod_status ".$sAddCols."
					 FROM ".$aConfig['tabls']['prefix']."products_stock A
					 JOIN ".$aConfig['tabls']['prefix']."products B
					 ON B.id=A.id
					 WHERE B.azymut_index = '".$sId."'";	
	return Common::GetRow($sSql);
} // end of getAzymutStock() function

/**
 * Pobiera cenę załącznika o danym indeksie azymut z tabeli załączników
 * cena zostanie pobarana jeśli aktualnym źródłem produktu do którego należy
 * ten załącznik jest Azymut
 * @param string $sId - indeks azymut
 * @return array ref - tablica  z id załącznika i jego ceną brutto
 */
function &getAzymutAttachmentPrice($sId) {
	global $aConfig;
	
	$sSql = "SELECT A.id,A.price_brutto, A.vat
					 FROM ".$aConfig['tabls']['prefix']."products_attachments A
					 JOIN ".$aConfig['tabls']['prefix']."products B
					 ON A.product_id=B.id
					 WHERE B.source = '2' AND A.azymut_index = '".$sId."'";	
	return Common::GetRow($sSql);
} // end of getAzymutAttachmentPrice() function

/**
 * Pobiera aktualną cenę brutto produktu o danym indeksie azymut
 * Cena zostanie pobrana jeśli aktualnym źródłem produktu jest Azymut
 * @param string $sId - indeks azymut 
 * @return array ref - tablica z id produktu i jego cena brutto
 */
function &getAzymutPrice($sId) {
	global $aConfig;
	
	$sSql = "SELECT id,price_brutto
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE prod_status <> '2' AND source = '2' AND azymut_index = '".$sId."'";	
	return Common::GetRow($sSql);
} // end of getAzymutPrice() function

/**
 * Funkcja ustawia jako niedostępne produkty z azymut, które w biezącym przebiegu importu
 * nie zostały ustawione jako dostępne
 * @return bool success
 */
function &setAzymutUnavaible($sNow) {
	global $aConfig;
	
	/*$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
					 SET prod_status = '0', source = '2', modified = NOW(), last_import = NOW(), modified_by = 'azymut_import'
					 WHERE azymut_index IS NOT NULL
					 AND prod_status = '1'
					 AND last_import != CAST(NOW() AS DATE)
					 AND source <> '0'
					 AND source <> '1'";	
	*/
	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_stock A
					 SET 
            A.azymut_status = '0', 
            A.azymut_last_import = ".Common::Quote($sNow).",
            A.azymut_stock = NULL
					 WHERE A.azymut_status = '1'
					 AND A.azymut_last_import != ".Common::Quote($sNow);
	
	/* $sSql = "UPDATE ".$aConfig['tabls']['prefix']."products A
						LEFT JOIN ".$aConfig['tabls']['prefix']."products_shadow B
						ON B.id=A.id
					 SET A.prod_status = '0', A.source = '2', A.modified = NOW(), A.last_import = NOW(), A.modified_by = 'azymut_import',
					 B.prod_status = '0'
					 WHERE A.azymut_index IS NOT NULL
					 AND A.prod_status = '1'
					 AND A.last_import != CAST(NOW() AS DATE)
					 AND A.source <> '0'
					 AND A.source <> '1'";
	 */
	return Common::Query($sSql);
} // end of setAzymutUnavaible() function

/**
 * Metoda aktualizuje datę ostatniego importu dla produktu
 *
 * @global type $aConfig
 * @param type $iProdId
 * @return type 
 */
function updateLastImport($iProdId) {
	global $aConfig;
	
	if ($iProdId > 0) {
		$aValues['last_import'] = 'NOW()';
		return  Common::Update($aConfig['tabls']['prefix']."products", $aValues, " id=".$iProdId);
	}
	return true;
}// end of updateLastImport() method

/**
 * Metoda pobiera ID produktu dla podanego bookindexu
 *
 * @global type $aConfig
 * @param type $sABookindex
 * @return array
 */
function getProductIdByBookindex($sABookindex) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products
					 WHERE azymut_index='".$sABookindex."'";
	return Common::GetOne($sSql);
}// end of getProductIdByBookindex() method

/**
 * Sprawdza czy książka o danym indeksie azymut istnieje w buforze importu
 * @param string $sId - indeks azymut
 * @return bool
 */
function existAzymutBookInBuffer($sId) {
	global $aConfig;
	
	$sSql = "SELECT count(id)
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_buffer
					 WHERE azymut_index = '".$sId."'";	
	return (Common::GetOne($sSql) > 0);
} // end of existAzymutBookInBuffer() function

/**
 * Pobiera pozycję o danym id z bufora importu azymut
 * @param int $iId - id w buforze importu azymut
 * @return array ref - tablica z pobraną pozycją
 */
function &getAzymutBookFromBuffer($iId) {
	global $aConfig;
	
	$sSql = "SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_buffer
					 WHERE id = ".$iId;	
	return Common::GetRow($sSql);
} // end of getAzymutBookFromBuffer() function

/**
 * Pobiera z bufora importu produkt o danym indeksie azymut
 * @param string $sIndex - indeks azymut
 * @return array ref - tablica z danymi produktu
 */
function &getAzymutBookFromBufferByIndex($sIndex) {
	global $aConfig;
	
	$sSql = "SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_buffer
					 WHERE azymut_index = '".$sIndex."'";	
	return Common::GetRow($sSql);
} // end of getAzymutBookFromBufferByIndex() function

/**
 * Zwraca obiekt plain query do odczytywania bufora importu azymut
 * @return object - obiekt zapytania z polami id, indeks azymut, isbn, ilosc czasu w buforze
 */
function getAzymutBuffer() {
	global $aConfig;
	
	$sSql = "SELECT id, azymut_index, isbn_plain, ean_13, DATEDIFF(NOW(),created) AS age, publication_type, dzial, komplet, subject,foreign_language_book,attributes
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_buffer";	
	return Common::PlainQuery($sSql);
} // end of getAzymutBuffer() function

/**
 * Usuwa produkt z bufora importu azymut
 * @param int $iBufferId - id pozycji w buforze
 * @return bool success
 */
function deleteFromBuffer($iBufferId){
	global $aConfig;
	$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_azymut_buffer
					 WHERE id = ".$iBufferId;
	return Common::Query($sSql);
} // end of deleteFromBuffer() function

/**
 * Funkcja dodaje indeks azymut do istniejącej juz w bazie ksiazki
 * i usuwa pozycje z bufora w przypadku powodzenia
 * @param int $iPId - id produktu
 * @param string $sAzymutIndex - indeks azymut
 * @param int $iBufferId - id pozycji w buforze azymut
 * @return bool success
 */
function addAzymutIndexToBook($iPId,$sAzymutIndex,$iBufferId){
	global $aConfig,$sLogInfo;
	
	$aValues=array(
		'azymut_index' => $sAzymutIndex,
		'last_import' => 'NOW()'
	);
	if (Common::Update($aConfig['tabls']['prefix']."products", $aValues,'id='.$iPId) === false) {
		echo "error updating Product \n";
		$sLogInfo.="Błąd aktualizacji produktu ".$iPId."\n";
		dump($aValues);
		return false;
	}
	deleteFromBuffer($iBufferId);
	echo "adding index to book ".$iPId." \n";
	$sLogInfo.="dodano id azymut do produktu ".$iPId."\n";
	return true;
} // end of addAzymutIndexToBook() function

/**
 * funkcja dodaje okładkę do produktu azymut
 * @param int $iProductId - id produktu
 * @param string $sAzymutId - indeks azymut
 * @param string $sIsbn - isbn
 * @return bool - success
 */
function addAzymutImage($iProductId,$sAzymutId,$sIsbn){
	global $aConfig, $pDbMgr;
	// jeśli istniej w plikach z płyty
	// jeśli nie pobierz z serwera azymut

	$sDstName=$sIsbn.".jpg";
	$sSrcFile=$aConfig['common']['base_path'].'images/photos/azymut_okladki/'.$sAzymutId.".jpg";
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
					 WHERE product_id=".$iProductId;
	$iId = $pDbMgr->GetOne('profit24', $sSql);
	if ($iId > 0) {
		// zdjecie juz istnieje w bazie nie dodajemy ponownie
		return true;
	}

	usleep(20000);
	if(!downloadAzymutImage($sAzymutId, '&size=hq')) {
		usleep(20000);
		if(!downloadAzymutImage($sAzymutId, '')) {
			return false;
		}
	}
	
	createProductImageDir($iProductId);
	$iRange=intval(floor($iProductId/1000))+1;
	$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
	$sShortDir='okladki/'.$iRange.'/'.$iProductId;
	
	if(!file_exists($sSrcFile))
		return false;
		
	$aFileInfo = getimagesize($sSrcFile);

	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
	$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
	if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
		resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
	}
	unlink($sSrcFile);
	
	$aValues=array(
		'product_id' => $iProductId,
		'directory' => $sShortDir,
		'photo' => $sDstName,
		'mime' => 'image/jpeg',
		'name' => $sDstName,
		'order_by' => '1'
	);
	return (Common::Insert($aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
} // end of addAzymutImage() function




/**
 * funkcja dodaje okładkę do produktu azymut
 * @param int $iProductId - id produktu
 * @param string $sAzymutId - indeks azymut
 * @param string $sIsbn - isbn
 * @return bool - success
 */
function addAzymutImageDeleteBefore($iProductId, $sAzymutId, $sIsbn)
{
	global $aConfig, $pDbMgr;
	// jeśli istniej w plikach z płyty
	// jeśli nie pobierz z serwera azymut

	$sDstName = $sIsbn . ".jpg";
	$sSrcFile = $aConfig['common']['base_path'] . 'images/photos/azymut_okladki/' . $sAzymutId . ".jpg";

	$sSql = "SELECT * FROM " . $aConfig['tabls']['prefix'] . "products_images
					 WHERE product_id=" . $iProductId;
	$aImages = $pDbMgr->GetAll('profit24', $sSql);
	foreach ($aImages as $aImage) {
		if ($aImage['id'] > 0) {
			// zdjecie juz istnieje w bazie nie usuwamy
			$sBigFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/__b_' . $aImage['photo'];
			if (file_exists($sBigFile)) {
				return true;
			}
		}
	}

	//http://services.azymut.pl/oferta/servlet/?mode=getImg&indeks=55803302629ZA&id=10831&p=ncYX8PwJ&size=hq
	usleep(20000);
	if (!downloadAzymutImage($sAzymutId, '&size=hq')) {
		usleep(20000);
		if (!downloadAzymutImage($sAzymutId, '')) {
			return false;
		}
	}

	createProductImageDir($iProductId);
	$iRange = intval(floor($iProductId / 1000)) + 1;
	$sDestDir = $aConfig['common']['base_path'] . 'images/photos/okladki/' . $iRange . '/' . $iProductId;
	$sShortDir = 'okladki/' . $iRange . '/' . $iProductId;

	if (!file_exists($sSrcFile))
		return false;


	foreach ($aImages as $aImage) {
		if ($aImage['id'] > 0) {
			// zdjecie juz istnieje w bazie nie usuwamy

			$sBaseFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/' . $aImage['photo'];
			$sThumbFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/__t_' . $aImage['photo'];
			$sBigFile = $aConfig['common']['base_path'] . 'images/photos/' . $aImage['directory'] . '/__b_' . $aImage['photo'];
			unlink($sBaseFile);
			unlink($sThumbFile);
			unlink($sBigFile);
			var_dump($sBaseFile);
			echo "\n";
			$sSql = 'DELETE FROM products_images WHERE id = ' . $aImage['id'];
			$pDbMgr->Query('profit24', $sSql);
		}
	}


	$aFileInfo = getimagesize($sSrcFile);

	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_' . $sDstName);
	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
	$aSmallInfo = getimagesize($sDestDir . '/' . $sDstName);
	if ($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])) {
		resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_' . $sDstName);
	}
	unlink($sSrcFile);

	$aValues = array(
		'product_id' => $iProductId,
		'directory' => $sShortDir,
		'photo' => $sDstName,
		'mime' => 'image/jpeg',
		'name' => $sDstName,
		'order_by' => '1'
	);
	print_r($aValues);
	return (Common::Insert($aConfig['tabls']['prefix'] . "products_images", $aValues, '', false) != false);

	return true;
} // end of addAzymutImage() function


/**
 * Sciąga plik okładki z serwera azymut
 * @param string $sAzymutId - indeks azymut
 * @return bool - success
 */
function downloadAzymutImage($sAzymutId, $sOption) {
	global $aConfig;

		$sURL="http://services.azymut.pl/oferta/servlet/?mode=getImg&indeks=".$sAzymutId."&id=".$aConfig['import']['azymut_login']."&p=".$aConfig['import']['azymut_password'].$sOption;
		// otwarcie pliku
		if ($rFp = fopen($aConfig['common']['base_path'].'images/photos/azymut_okladki/'.$sAzymutId.".jpg", "w")) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sURL);
		 	curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
		 	curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
      //curl_setopt($oCURL, CURLOPT_INTERFACE, "188.165.19.148");
		 	if ($iTimeOut > 0) {
		 		curl_setopt($oCURL, CURLOPT_TIMEOUT, $iTimeOut);
		 	}
		 	curl_setopt($oCURL, CURLOPT_POST, FALSE);
		 	//curl_setopt($oCURL, CURLOPT_POSTFIELDS, $sParameters);
		 	curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
		 	curl_setopt($oCURL, CURLOPT_FILE, $rFp);
	
			curl_exec($oCURL);
			curl_close($oCURL);
			fclose ($rFp);
//			sleep(2);
			if(filesize($aConfig['common']['base_path'].'images/photos/azymut_okladki/'.$sAzymutId.".jpg")<1000){
//				dump('image download error: '.implode('', file($aConfig['common']['base_path'].'images/photos/azymut_okladki/'.$sAzymutId.".jpg")));
				return false;
			}
			$old_umask = umask(0);
			@chmod($aConfig['common']['base_path'].'images/photos/azymut_okladki/'.$sAzymutId.".jpg", 0664);
			umask($old_umask);
			return true;
		}
		return false;
} // end of downloadAzymutImage() function

/**
 * 
 * @param array $aFiles
 * @return array
 */
function filterOnlyProducsts($sType, $aFiles) {
  switch ($sType) {
    case 'products':
      $sRegexp = '/\.\/10831-\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}_\d{6}\.xml/';
    break;
    case 'prices':
      $sRegexp = '/10831-\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}_price\.xml/';
    break;
  
    case 'deleted':
      $sRegexp = '/10831-\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}_deleted\.xml/';
    break;
  }
  
  $aNewFiles = [];
  foreach ($aFiles as $sFile) {
    if (preg_match($sRegexp, $sFile)) {
      $aNewFiles[] = $sFile;
    }
  }
  return $aNewFiles;
}

/**
 * Pobiera xml z serwera azymut
 * @param string $sMode - tryb azymut
 * @param string $sDestFile - plik docelowy
 * @return bool result
 */
function getAzymutXMLFTPFile($sType, $sFilename) {
	global $aConfig;
  
  $conn_id = ftp_connect('ftp.azymut.pl');
  if (ftp_login($conn_id, $aConfig['import']['ftp_azymut_login'], $aConfig['import']['ftp_azymut_password']) === false) {
    return false;
  }
  $zawartosc = ftp_nlist($conn_id, ".");
  
  $aFiles = filterOnlyProducsts($sType, $zawartosc);
  if (!isset($aFiles[0])) {
    return false;
  }
  $sLocalFile = $aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sFilename;
  if (ftp_get($conn_id, $sLocalFile, $aFiles[0], FTP_BINARY)) {
      echo "Pomyślnie zapisano $sLocalFile\n";
      if ($aConfig['common']['status']	!= 'development') {
        ftp_delete($conn_id, $aFiles[0]);
      }
      dump($sLocalFile);
  } else {
      echo "Problem.\n";
  }

  // zamknięcie połączenia
  ftp_close($conn_id);
  return $sLocalFile;
} // end of downloadAzymutXML() function


/**
 * Pobiera xml z serwera azymut
 * @param string $sMode - tryb azymut
 * @param string $sDestFile - plik docelowy
 * @return bool result
 */
function downloadAzymutXML($sMode, $sDestFile, $sParams = '') {
	global $aConfig;
	$sURL="http://services.azymut.pl/oferta/servlet/?mode=".$sMode."&id=".$aConfig['import']['azymut_login']."&p=".$aConfig['import']['azymut_password']."&enc=utf8".$sParams;
		
		// otwarcie pliku
	if ($rFp = fopen($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile, "w")) {
		$oCURL = curl_init();
		curl_setopt($oCURL, CURLOPT_URL, $sURL);
	 	curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_POST, FALSE);
    //curl_setopt($oCURL, CURLOPT_INTERFACE, "188.165.19.148");
	 	//curl_setopt($oCURL, CURLOPT_POSTFIELDS, $sParameters);
	 	curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_FILE, $rFp);

		
		$output = curl_exec($oCURL);
		$info = curl_getinfo($oCURL);
		if ($output === false ) {
			dump($info);
		  if (curl_error($oCURL))
		    echo curl_error($oCURL)."<br>\n";
		  return false;
	  }
	  curl_close($oCURL);
		fclose ($rFp);
		if(filesize($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile)<100){
				dump('xml download error: '.implode('', file($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile)));
				return false;
			}

		$old_umask = umask(0);
		@chmod($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile, 0664);
		umask($old_umask);

		usleep(20000);
		return true;
	} else echo("nie moge otworzyc pliku ".$sDestFile);
	return false;
} // end of downloadAzymutXML() function

/**
 * Zgłasza potwierdzenie pobrania pliku produktów azymut
 * 
 * @param $sCode - kod potwierdzający otrzymany w xml produktów
 * @return bool - success
 */
function AzymutConfirm($sCode) {
	global $aConfig;
	$sURL="http://services.azymut.pl/oferta/servlet/?mode=confirm&id=".$aConfig['import']['azymut_login']."&p=".$aConfig['import']['azymut_password']."&transactionId=".$sCode;
		
		$oCURL = curl_init();
		curl_setopt($oCURL, CURLOPT_URL, $sURL);
	 	curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_POST, FALSE);
    //curl_setopt($oCURL, CURLOPT_INTERFACE, "188.165.19.148");
	 	//curl_setopt($oCURL, CURLOPT_POSTFIELDS, $sParameters);
	 	curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, TRUE);

		$sReturn=curl_exec($oCURL);
		curl_close($oCURL);
		if(preg_match('/(?<!NOT\s)CONFIRMED/',$sReturn)) {
			return true;
		}
		else {
			dump("confirmation error: ".$sReturn);
		}
} // end of AzymutConfirm() function

/**
 * tworzy folder na okładki azymut i ustawia dla niego własciwe prawa
 * @return unknown_type
 */
function createAzymutImagesDir() {
	global $aConfig;
	$old_umask = umask(0);
	if (!is_dir($aConfig['common']['base_path']."images/photos/azymut_okladki")) {
		mkdir($aConfig['common']['base_path']."images/photos/azymut_okladki",0777,true);
	}
	umask($old_umask);
} // end of createAzymutImagesDir() function



/** 
 *  *************** ABE ****************************
 */

/**
 * Sprawdza czy istnieje kategoria ABE o podanym id
 * @param int $iId - id kategorii ABE
 * @return bool
 */
function existAbeCategory($iId) {
	global $aConfig;
	
	$sSql = "SELECT count(id)
					 FROM ".$aConfig['tabls']['prefix']."products_abe_categories
					 WHERE id = ".$iId;	
	return (Common::GetOne($sSql) > 0);
} // end of existAbeCategory() function

/**
 * Sprawdza czy istnieje wydawca ABE o danym kodzie wydawcy
 * @param string $sId - kod wydawcy ABE
 * @return bool
 */
function existAbePublisher($sId) {
	global $aConfig,$pDB;
	
	$sSql = "SELECT count(id)
					 FROM ".$aConfig['tabls']['prefix']."products_abe_publishers
					 WHERE abe_code = ".$pDB->quoteSmart(stripslashes($sId));
	return (Common::GetOne($sSql) > 0);
} // end of existAbeCategory() function

/**
 * Pobiera nazwe wydawcy o danym kodzie ABE
 * @param string $sId - kod wydawcy ABE
 * @return string - nazwa wydawcy
 */
function getAbePublisher($sId) {
	global $aConfig,$pDB;
	
	$sSql = "SELECT name
					 FROM ".$aConfig['tabls']['prefix']."products_abe_publishers
					 WHERE abe_code = ".$pDB->quoteSmart(stripslashes($sId));
	return Common::GetOne($sSql);
} // end of getAbePublisher() function

/**
 * Pobiera ścieżkę rodziców kategorii ABE
 * @param int $iId - id kategorii ABE
 * @return string - ściezka tekstowa rodziców kategorii 
 */
function getABEParentPath($iId) {
	global $aConfig;
	
	$sSql="SELECT name, parent_id
				FROM ".$aConfig['tabls']['prefix']."products_abe_categories
				WHERE id = ".$iId;
	$aItem = Common::GetRow($sSql);
	return $aItem['name'].' / ';
} // end of getABEParentPath() function

/**
 * Pobiera tablicę mapowań kategorii ABE (id w księgarni)
 * jeśli mapowania sa puste zwraca w tablicy id kategorii "nieposortowane"
 * @param $iId - id kategorii abe
 * @return array ref - tablica z id kategorii księgarni
 */
function &getAbeCategoryMapping($iId) {
	global $aConfig;
	$iUncategorizedId=$aConfig['import']['unsorted_category'];
	$sSql = "SELECT DISTINCT page_id
					 FROM ".$aConfig['tabls']['prefix']."products_abe_mapping
					 WHERE abe_category = ".$iId;	
	$aMappings= Common::GetCol($sSql);
	if(empty($aMappings))
		return array($iUncategorizedId);
	else 
		return $aMappings;
} // end of getAbeCategoryMapping() function

/**
 * Zamienia typ oprawy ABE na opis tekstowy
 * @param $iId - id oprawy ABE
 * @return string - opis tekstowy typu oprawy
 */
function convertAbeBinding($iId){
	switch($iId){
		case 1: 
			return 'oprawa miękka';
		break;
		case 2: 
				return 'oprawa twarda';
			break;
		case 3: 
				return 'CD-ROM';
			break;
		case 4: 
				return 'video';
			break;
		case 5: 
				return 'audio';
			break;
		case 6: 
				return 'oprawa płócienna';
			break;
		case 7: 
				return 'slajdy';
			break;
		case 8: 
				return 'spirala';
			break;
		case 9: 
				return 'oprogramowanie';
			break;
		case 10: 
				return 'oprawa skórzana';
			break;
		case 11: 	
				return 'mikrofisze';
			break;
		default:
				return '';
			break;
	}
} // end of convertAbeBinding() function

/**
 * Zamienia kod typu publikacji azymut na opis tekstowy
 * @param string(2) $sType - kod tekstowy publikacji Azymut
 * @return string - opis typu publikacji
 */
function convertAzymutPublType($sType){
	switch($sType){
		case 'CD': 
					return 'CD ROM';
				break;		
		case 'BR': 
					return 'Broszura';
				break;			
		case 'CZ': 
					return 'Czasopismo';
				break;			
		case 'DS': 
					return 'Dyskietka';
				break;			
		case 'GR': 
					return 'Gra';
				break;			
		case 'KA': 
					return 'Kaseta audio';
				break;			
		case 'KV': 
					return 'Kaseta video';
				break;			
		case 'MP': 
					return 'Mapa';
				break;			
		case 'SK': 
					return 'Skrypt';
				break;			
		case 'AL': 
					return 'Album';
				break;			
		case 'AT': 
					return 'Atlas';
				break;			
		case 'AK': 
					return 'Akcesoria';
				break;		
		case 'KL': 
					return 'Kalendarz';
				break;			
		case 'DV': 
					return 'DVD';
				break;
		default:
				return '';
			break;
	}
}	// end of convertAzymutPublType() function

/**
 * Pobiera XML z serwera ABE
 * @param string $sQuery - adres do pobrania xml
 * @param string $sDestFile - nazwa pliku docelowego
 * @return bool - success
 */
function downloadAbeXML($sQuery, $sDestFile) {
	global $aConfig;
	// otwarcie pliku
	if ($rFp = fopen($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile, "w")) {
		$oCURL = curl_init();
		curl_setopt($oCURL, CURLOPT_URL, $sQuery);
	 	curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_POST, FALSE);
	 	curl_setopt($oCURL, CURLOPT_USERPWD, $aConfig['import']['abe_login'].':'.$aConfig['import']['abe_password']);
		curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_FILE, $rFp);

		curl_exec($oCURL);

 		curl_close($oCURL);
		fclose ($rFp);
		return true;
	} else die("nie moge otworzyc pliku");
	return false;
} // end of downloadAbeXML() function


	/**
	 * Metoda pobiera autorow produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @return	array	- autorzy produktu
	 */

	function getAuthors($iId) {
		global $aConfig;
		// pobranie autorow ksiazki
		$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
					FROM " . $aConfig['tabls']['prefix'] . "products_to_authors A
					JOIN " . $aConfig['tabls']['prefix'] . "products_authors_roles B
					ON B.id = A.role_id
					JOIN " . $aConfig['tabls']['prefix'] . "products_authors C
					ON C.id = A.author_id
					WHERE A.product_id = " . $iId . "
					ORDER BY B.order_by";

		$aResult = & Common::GetAll($sSql);
		$sAuthors = '';

		foreach ($aResult as $aAuthor) {
			$sAuthors .= $aAuthor['author'] . ', ';
		}
		return substr($sAuthors, 0, -2);
	}// end of getAuthors() function


/**
 * Dodaje okładkę do produktu ABE
 * @param int $iProductId - id produktu
 * @param int $iAbeId - id produktu abe
 * @param string $sIsbn - isbn produktu
 * @return bool - success
 */
function addAbeImage($iProductId,$iAbeId,$sIsbn){
	global $aConfig;
	
	if(!downloadAbeImage($iAbeId))
	{
		echo "fail downloading image with code ".$iAbeId." <br>\n";
		return false;
	}
	createProductImageDir($iProductId);
	$iRange=intval(floor($iProductId/1000))+1;
	$sSrcFile = $aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg";;
	$sDstName=$sIsbn.".jpg";
	
	$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
	$sShortDir='okladki/'.$iRange.'/'.$iProductId;

	if(!file_exists($sSrcFile)){
		echo "missing source image file ".$sSrcFile." <br>\n";
		return false;
	}
		
	$aFileInfo = getimagesize($sSrcFile);

	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
	$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
	if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
		resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
	}
	
	$aValues=array(
		'product_id' => $iProductId,
		'directory' => $sShortDir,
		'photo' => $sDstName,
		'mime' => 'image/jpeg',
		'name' => $sDstName,
		'order_by' => '1'
	);
	Common::Insert($aConfig['tabls']['prefix']."products_images", $aValues, '', false);
	@unlink($sSrcFile);
	return true;
} // end of addAbeImage() function

/**
 * Pobiera okładkę z serwera ABE
 * @param $iAbeId - id produktu ABE
 * @return bool - success
 */
function downloadAbeImage($iAbeId) {
	global $aConfig;

		$sURL="http://www.abe.pl/html/b2b/covers.php?id=".$iAbeId;
		// otwarcie pliku
		if ($rFp = fopen($aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg", "w")) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sURL);
		 	curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
	 		curl_setopt($oCURL, CURLOPT_TIMEOUT, 2);

		 	curl_setopt($oCURL, CURLOPT_POST, FALSE);
			curl_setopt($oCURL, CURLOPT_USERPWD, $aConfig['import']['abe_login'].':'.$aConfig['import']['abe_password']);
		 	curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
		 	curl_setopt($oCURL, CURLOPT_FILE, $rFp);
	
		//	curl_exec($oCURL);
			
		$output = curl_exec($oCURL);
		$info = curl_getinfo($oCURL);

		if ($output === false ) {
			dump($info);
		  if (curl_error($oCURL))
		    echo curl_error($oCURL)."<br>\n";
	  }
	
			
			curl_close($oCURL);
			fclose ($rFp);
			if(filesize($aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg") == 0){
				echo "filesize equals zero ".$aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg"."\n <br>";
				@unlink($aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg");
				return false;
			}
			@chmod($aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg", 0664);
			return true;
		} else{
			echo "cannot open ".$aConfig['common']['base_path'].'images/photos/abe_okladki/'.$iAbeId.".jpg"."\n <br>";
		}
		return false;
} // end of downloadAbeImage() function

/**
 * Tworzy iustawia uprawniania dla folderu na koładki abe
 * @return void
 */
function createAbeImagesDir() {
	global $aConfig;
	$old_umask = umask(0);
	if (!is_dir($aConfig['common']['base_path']."images/photos/abe_okladki")) {
		mkdir($aConfig['common']['base_path']."images/photos/abe_okladki",0777,true);
	}
	umask($old_umask);
} // end of createAbeImagesDir() function

/**
 * Zapisuje czas ostatniego pobrania produktów z ABE
 * @param date $sLast - poprzednia data ostatniego pobrania produktów z abe 
 * @return bool - success
 */
function &setLastAbeUpdate($sLast) {
	global $aConfig;
	if(empty($sLast)){
		$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."products_import (last_abe_import)
						 VALUES ( NOW())";
	} else {
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_import
					 SET last_abe_import = NOW()
					 WHERE 1=1";
	}
	return Common::Query($sSql);
} // end of setLastAbeUpdate() function

/**
 * Pobiera datę o 7 dni wcześniejszą od ostatniego pobrania
 * (7 dni wprowadzone zeby wyeliminowac mozliwe problemy z zgubieniem produktu
 * @return date
 */
function getLastAbeUpdate() {
	global $aConfig;
	
	$sSql = "SELECT DATE_SUB(last_abe_import, INTERVAL 7 DAY )
					 FROM ".$aConfig['tabls']['prefix']."products_import
					 LIMIT 0,1";	
	return Common::GetOne($sSql);
} // end of getLastAbeUpdate() function


/**
 *  ************** Baza wewnętrzna *************************
 */


/**
 *  **************** ABC ************************************
 */
/**
 * dodaje okładke do produktu ABC
 * @param int $iProductId - id produktu
 * @param string $sAbcImg - nazwa pliku okładki abc
 * @param string $sIsbn - isbn produktu
 * @return bool success
 */
function addABCImage($iProductId,$sAbcImg,$sIsbn){
	global $aConfig;
	
	if(file_exists($aConfig['common']['base_path'].'images/photos/ABC_okladki/okladki/'.$sAbcImg)){
	createProductImageDir($iProductId);
	$iRange=intval(floor($iProductId/1000))+1;
	$sSrcFile = $aConfig['common']['base_path'].'images/photos/ABC_okladki/okladki/'.$sAbcImg;
	$sDstName=$sIsbn.'jpg';
	
	$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
	$sShortDir='okladki/'.$iRange.'/'.$iProductId;
		
	if(!file_exists($sSrcFile))
		return false;
		
	$aFileInfo = getimagesize($sSrcFile);

	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
	resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
	$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
	if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
		resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
	}
	
	$aValues=array(
		'product_id' => $iProductId,
		'directory' => $sShortDir,
		'photo' => $sDstName,
		'mime' => 'image/jpeg',
		'name' => $sDstName,
		'order_by' => '1'
	);
	Common::Insert($aConfig['tabls']['prefix']."products_images", $aValues, '', false);
	return true;
	} else return false;
} // edn fo addABCImage() function

/** 
 *  *************** Pomocnicze ******************************
 */
/**
 * Wysyła maila z logiem importu
 * @param $sTopic - temat maila
 * @param $sContent - treść maila
 * @return void
 */
function sendInfoMail($sTopic,$sContent){
	global $aConfig;
	
	if(!Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'], $sTopic, $sContent))
		dump('error sending mail');
} // end of sendInfoMail() function

// skaluje obrazek do szerokości i przenosi
function resizeAndMoveImageTo($sTmpFileName, &$aImageSize, $sDestDir, $sNewWidthHeight, $sNewName='', $iMode=0664, $iQuality=90) {
	global $aConfig;
  $iNewWidth	= 0;
  $iNewHeight	= 0;

	// od tego miejsca zaczyna sie kod odpowiedzialny za zmiane rozmiaru obrazka
	$aNewSize = explode('x', $sNewWidthHeight);
	if ($aImageSize[0] > $aNewSize[0] || $aImageSize[1] > $aNewSize[1]) {
		// okreslenie nowych rozmiarow
		if ($aImageSize[0] > $aImageSize[1]) {
			// szerokosc jest wieksza od wysokosci
			$iNewWidth =& $aNewSize[0];
			$iNewHeight = ceil(($iNewWidth * $aImageSize[1]) / $aImageSize[0]);
		}
		else {
			// wysokosc jest wieksza od szerokosci
			$iNewHeight =& $aNewSize[1];
			$iNewWidth = ceil(($iNewHeight * $aImageSize[0]) / $aImageSize[1]);
		}
		if ($iNewWidth > $aNewSize[0]) {
			// szerokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
			// szerokosc maxymalna, dostosowanie szerokosci do szerokosci max.
			// przeliczenie na nowo wysokosci
			$iNewWidth = $aNewSize[0];
			$iNewHeight = ceil(($iNewWidth * $aImageSize[1]) / $aImageSize[0]);
		}
		if ($iNewHeight > $aNewSize[1]) {
			// wysokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
			// wysokosc maxymalna, dostosowanie wysokosci do wysokosci max.
			// przeliczenie na nowo szerokosci
			$iNewHeight = $aNewSize[1];
			$iNewWidth = ceil(($iNewHeight * $aImageSize[0]) / $aImageSize[1]);
		}
		// utworzenie obrazka o nowych rozmiarach
		if ($aImageSize['mime'] == "image/pjpeg" ||
				$aImageSize['mime'] == "image/jpeg" ||
				$aImageSize['mime'] == "image/jpg") {
			$oHandle = imagecreatefromjpeg($sTmpFileName);
		}
		elseif ($aImageSize['mime'] == "image/png") {
			$oHandle = imagecreatefrompng($sTmpFileName);
		}
		elseif ($aImageSize['mime'] == "image/gif") {
			$oHandle = imagecreatefromgif($sTmpFileName);
		}
		else {
			return false;
		}
		$oDstImg = imagecreatetruecolor($iNewWidth, $iNewHeight);
		if (!$oDstImg) {
			return false;
		}
		
	// przezroczystosc
		if ($aImageSize['mime'] == "image/png") {
			// PNG
			imagealphablending($oDstImg, false);
      $iColorTransparent = imagecolorallocatealpha($oDstImg, 0, 0, 0, 127);
      imagefill($oDstImg, 0, 0, $iColorTransparent);
      imagesavealpha($oDstImg, true);
		}
		elseif ($aImageSize['mime'] == "image/gif") {
			// GIF
			$iTranspInd = imagecolortransparent($oHandle);
			if ($iTranspInd >= 0) {
				$aTranspColor = imagecolorsforindex($oHandle, $iTranspInd);
        $iTranspIndex = imagecolorallocate($oDstImg, $aTranspColor['red'], $aTranspColor['green'], $aTranspColor['blue']);
        imagefill($oDstImg, 0, 0, $iTranspIndex);
       	imagecolortransparent($oDstImg, $iTranspIndex);
			}
		}
						
		if (!imagecopyresampled($oDstImg, $oHandle, 0, 0, 0, 0, $iNewWidth, $iNewHeight, $aImageSize[0], $aImageSize[1])) {
			return false;
		}
		imagedestroy($oHandle);
		if ($aImageSize['mime'] == "image/pjpeg" ||
				$aImageSize['mime'] == "image/jpeg" ||
				$aImageSize['mime'] == "image/jpg") {
			if (!imagejpeg($oDstImg, $sDestDir.'/'.$sNewName, $iQuality)) {
				return false;
			}
		}
		elseif ($aImageSize['mime'] == "image/png") {
			if (!imagepng($oDstImg, $sDestDir.'/'.$sNewName)) {
				return false;
			}
		}
		elseif ($aImageSize['mime'] == "image/gif") {
			if (!imagegif($oDstImg, $sDestDir.'/'.$sNewName)) {
				return false;
			}
		}
		else {
			return false;
		}
		@imagedestroy($oDstImg);
		@chmod($sDestDir.'/'.$sNewName, $iMode);
	}
	else {
		// zwykle przeniesienie pliku bez zmiany jego rozmiaru
		if (!@copy($sTmpFileName, $sDestDir.'/'.$sNewName)) {
			return false;
		}
	}
	// zmieniamy atrybuty
	@chmod($sDestDir.'/'.$sNewName, $iMode);
	return true;
} // end of resizeAndMoveImageTo() function\

/**
 * Pobiera bieżący czas/date serwera MySQL
 * @return date
 */
function getMysqlNow(){
	global $aConfig;
	$sSql="SELECT NOW()";
	return Common::GetOne($sSql);
} // end of getMysqlNow() function

/**
 * Pobiera bieżąca datę serwera MySQL
 * @return date
 */
function getMysqlDate(){
	global $aConfig;
	$sSql="SELECT CAST(NOW() AS DATE)";
	return Common::GetOne($sSql);
} // end of getMysqlDate() server

/**
 * pobiera id książki o danym isbn
 * @param $sISBN - kod ISBN
 * @return int - id książki
 */
function getBookIdWithISBN($sISBN) {
	global $aConfig;
	
	$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE isbn_plain = '".$sISBN."'";	
	return Common::GetOne($sSql);
} // end of getBookIdWithISBN() function

/**
 * Sprawdza czy w bazie istnieje książka o danym nr ISBN
 * @param string $sISBN - kod ISBN
 * @return bool
 */
function existBookWithISBN($sISBN) {
	global $aConfig;
	
	$sSql = "SELECT count(id)
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE isbn_plain = '".$sISBN."'";	
	return (Common::GetOne($sSql) > 0);
} // end of existBookWithISBN() function

/**
 * Pobiera id i indeks azymut
 * @param string $sISBN - kod ISBN
 * @return array - tablica id, indeks azymut
 */
function getBookIdWithISBN_A($sISBN) {
	global $aConfig;
	
	$sSql = "SELECT id, azymut_index, prod_status, published, source, publisher_id
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE isbn_plain = '".$sISBN."'";	
	return Common::GetRow($sSql);
} // end of getBookIdWithISBN_A() function

/**
 * Pobiera status produktu po isbn
 * @param $sISBN - kod ISBN
 * @return array - tablica z id, statusem, źródło, cena brutto, data utworzenia
 */
function getBookIdWithISBN_H($sISBN) {
	global $aConfig;
	
	$sSql = "SELECT id,prod_status, source, price_brutto, created_by
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE isbn_plain = '".$sISBN."'";	
	return Common::GetRow($sSql);
} // end of getBookIdWithISBN_H() function

/**
 * Pobiera status produktu po isbn
 * @param string $sISBN
 * @return array - tablica z id, statusem, źródło, cena brutto, cena netto
 */
function getBookWithISBN($sISBN) {
	global $aConfig;
	
	$sSql = "SELECT id,prod_status,source,price_brutto,price_netto
					 FROM ".$aConfig['tabls']['prefix']."products
					 WHERE isbn_plain = '".$sISBN."'";	
	return Common::GetRow($sSql);
} // end of getBookWithISBN() function

/**
 * Pobiera status produktu po isbn
 * @param string $sISBN
 * @return array - tablica z id, statusami i cenami w poszczególnych źródłach
 */
function getBookStockWithISBN($sISBN) {
	global $aConfig;
	
	$sSqlSelect = 'SELECT A.*, B.prod_status, B.published, B.source, B.publisher_id
					 FROM '.$aConfig['tabls']['prefix'].'products_stock A
					 JOIN '.$aConfig['tabls']['prefix'].'products B
					 ON B.id=A.id
					 WHERE B.isbn_plain = "'.$sISBN.'" OR 
							B.isbn_10 = "'.$sISBN.'" OR 
							B.isbn_13 = "'.$sISBN.'" OR 
							B.ean_13 = "'.$sISBN.'"';
	return Common::GetRow($sSqlSelect);
} // end of getBookStockWithISBN() function

/**
 * Dodawanie roku do słownika lat
 * @param $iYear - rok
 * @return bool - success
 */
function addYearToDict($iYear){
	global $aConfig;
	if($iYear > 0) {
		$sSql="SELECT count(id) FROM ".$aConfig['tabls']['prefix']."products_years
					 WHERE year = ".$iYear;
		if(intval(Common::GetOne($sSql)) == 0){
			$aValues = array(
				'year' => $iYear,
				'created' => 'NOW()',
				'created_by' => 'import'
			);
			if (Common::Insert($aConfig['tabls']['prefix']."products_years", $aValues, "", false) === false) {
				return false;
			} else {
				return true;
			}
		}
	}
	return true;
}// end of addYearToDict() function

/**
 * Pobiera id wydawnictwa, jeśli nie ma w bazie dodaje (do tabeli wydawnictw,
 * tabeli ukrytej i tworzy między nimi mapowianie (opisane w getPublisherId.png)
 * 
 * @param string $sPublisher - nazwa wydawnictwa
 * @return int - id wydawnictwa
 */
function getPublisherId($sPublisher){
	global $aConfig,$pDB,$sLogInfo;
	
	$sPublisher=trim($sPublisher);
	$sSql="SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers_import
				 WHERE name = ".$pDB->quoteSmart(stripslashes($sPublisher));
	$iIId=Common::GetOne($sSql);
	// wydawnictwo istnieje w tabeli ukrytej
	if($iIId > 0) {
		$sSql = "SELECT publisher_id 
						FROM ".$aConfig['tabls']['prefix']."products_publishers_mappings
						WHERE publisher_import_id = ".$iIId;
		$iPId=intval(Common::GetOne($sSql));
		// mamy mapowanie - zwracamy id
		if($iPId >0 ) {
			return $iPId;
		}
	}
	// nie mamy mapowania
	if(!empty($sPublisher)) {
		// dodajemy do tabeli glownej
		
		$iId = getProductPublisherId($sPublisher);
		
		if (intval($iId) <= 0) {
			// brak prod w bazie, dodajemy
			$aValues=array(
				'name'=>$sPublisher,
				'index_letter' => strtoupper(mb_substr($sPublisher, 0, 1, 'utf8')),
				'created' => 'NOW()',
				'created_by' => 'import'
			);
			$iId=0;
			if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_publishers", $aValues)) === false) {
				$sLogInfo.= "błąd dodawania wydawcy ".$sPublisher."\n";
				echo "błąd dodawania wydawcy ".$sPublisher."<br>\n";
				//dump($aValues);
				return false;
			}
		}
		
		if (intval($iId) > 0) {
			// mamy id wydawcy
			
			$sLogInfo.= "dodano wydawcę ".$sPublisher."\n";
			echo "dodano wydawcę ".$sPublisher."<br>\n";
			// nie ammy w tabeli importu - dodajemy
			if(intval($iIId) <= 0) {
				$aValues=array(
					'name'=>$sPublisher
				);
				if (($iIId = Common::Insert($aConfig['tabls']['prefix']."products_publishers_import", $aValues)) === false) {
					$sLogInfo.= "błąd dodawania wydawcy ".$sPublisher."\n";
					echo "błąd dodawania wydawcy ".$sPublisher."<br>\n";
					//dump($aValues);
					return false;
				}
			}
		}
		if ($iId > 0 && $iIId > 0) {
			// dodajemy mapowanie
			$aValues=array(
				'publisher_id' => $iId,
				'publisher_import_id' => $iIId
			);
			if ((Common::Insert($aConfig['tabls']['prefix']."products_publishers_mappings", $aValues,'',false)) === false) {
				$sLogInfo.= "błąd dodawania mapowania\n";
				echo "błąd dodawania mapowania<br>\n";
				//dump($aValues);
				return false;
			}
			return $iId;
		} else {
			return 'NULL';
		}
	} else {
		return 'NULL';
	}		
} // end of getPublisherId() function


/**
 * Metoda pobiera id wydawcy z własciwej tabeli wydawców produktów
 * 
 * @global array $aConfig
 * @global object $pDB
 * @param string $sName
 * @return int
 */
function getProductPublisherId($sPublisher) {
	global $aConfig, $pDB;
	$sPublisher = trim($sPublisher);
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers
					 WHERE name = ".$pDB->quoteSmart(stripslashes($sPublisher));
	return Common::GetOne($sSql);
}// end of getProductPublisherId() method


/**
 * Pobiera id autora, jeśli nie istnieje dodaje go do bazy
 * @param string $sAuthor - autor w postaci Nazwisko Imie
 * @return int - id autora
 */
function getAuthorId($sAuthor){
	global $aConfig,$pDB;
	
	if(($iSep = mb_strpos($sAuthor,' ',0, 'utf8')) !== false){
		$sSurname=trim(mb_substr($sAuthor,0,$iSep, 'utf8'));
		$sName=trim(mb_substr($sAuthor,$iSep+1,mb_strlen($sAuthor, 'utf8')-$iSep, 'utf8'));
	} else {
		$sSurname=$sAuthor;
		$sName='';
	}

	$sSql="SELECT id FROM ".$aConfig['tabls']['prefix']."products_authors
				 WHERE name = ".$pDB->quoteSmart(stripslashes($sName))." AND surname = ".$pDB->quoteSmart(stripslashes($sSurname));
	$iId=Common::GetOne($sSql);
	/*$sSql='SELECT author_id FROM '.$aConfig['tabls']['prefix'].'products_authors_mappings WHERE author_import_id='.$iId;
	$iId=Common::GetOne($sSql);*/
	
	if($iId > 0)
		return $iId;
	
	$aValues=array(
		'name'=>$sName,
		'surname'=>$sSurname,
		'index_letter'=>strtoupper(mb_substr($sSurname, 0, 1, 'utf8')),
		'created'=>'NOW()',
		'created_by'=>'import'
	);
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_authors_import", $aValues)) === false) {
		echo "error adding Author \n";
		dump($aValues);
		return false;
	}
	else{
		//dopisanie do tabeli autora i utworzenie mapowania
		$aValues2=$aValues;
		$aValues2['id']=$iId;
		$iAId = Common::Insert($aConfig['tabls']['prefix']."products_authors", $aValues2);
		if($iAId===false) return $bErr;
		
		$aValues3=array('author_id'=>$iAId, 	'author_import_id'=>$iId);
		
		Common::Insert($aConfig['tabls']['prefix']."products_authors_mappings",
									 $aValues3, '', false);

		if ($iAId>0) {
			return $iAId;
		} elseif ($iId>0) {
			return $iId;
		}
		return false;
		//koniec dopisania
	}		
} // end of getAuthorId() function

/**
 * Pobiera id autora, jeśli nie istnieje dodaje go do bazy
 * @param string $sSurname
 * @param string $sName
 * @return int - id autora
 */
function getAuthorId2($sSurname, $sName){
	global $aConfig,$pDB;

	$sSql="SELECT id FROM products_authors
				 WHERE name = ".$pDB->quoteSmart(stripslashes($sName))." AND surname = ".$pDB->quoteSmart(stripslashes($sSurname));
	$iId=Common::GetOne($sSql);
	/*$sSql='SELECT author_id FROM '.$aConfig['tabls']['prefix'].'products_authors_mappings WHERE author_import_id='.$iId;
	$iId=Common::GetOne($sSql);*/
	if($iId > 0)
		return $iId;
	
	$aValues=array(
		'name'=>$sName,
		'surname'=>$sSurname,
		'index_letter'=>strtoupper(mb_substr($sSurname, 0, 1, 'utf8')),
		'created'=>'NOW()',
		'created_by'=>'import'
	);
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_authors_import", $aValues)) === false) {
		echo "error adding Author \n";
		dump($aValues);
		return false;
	}
	else {
		//dopisanie do tabeli autora i utworzenie mapowania
		$aValues2=$aValues;
		$aValues2['id']=$iId;
		$bErr=Common::Insert($aConfig['tabls']['prefix']."products_authors", $aValues2, '', false);
		if($bErr===false) return $bErr;
		
		$aValues3=array('author_id'=>$iId, 	'author_import_id'=>$iId);
		
		Common::Insert($aConfig['tabls']['prefix']."products_authors_mappings",
									 $aValues3, '', false);			
		
		return $iId;
		//koniec dopisania
		}
} // end of getAuthorId2() function

/**
 * Pobiera id autora, jeśli nie istnieje dodaje go do bazy
 * @param string $sAuthor - autor w postaci Imie Nazwisko
 * @return int - id autora
 */
function getAuthorId3($sAuthor){
	global $aConfig,$pDB;
	
	if(($iSep = mb_strrpos($sAuthor,' ',0, 'utf8')) !== false){
		$sName=trim(mb_substr($sAuthor,0,$iSep, 'utf8'));
		$sSurname=trim(mb_substr($sAuthor,$iSep+1,mb_strlen($sAuthor, 'utf8')-$iSep, 'utf8'));
	} else {
		$sSurname=$sAuthor;
		$sName='';
	}

	$sSql="SELECT id FROM products_authors
				 WHERE name = ".$pDB->quoteSmart(stripslashes($sName))." AND surname = ".$pDB->quoteSmart(stripslashes($sSurname));
	$iId=Common::GetOne($sSql);
	/*$sSql='SELECT author_id FROM '.$aConfig['tabls']['prefix'].'products_authors_mappings WHERE author_import_id='.$iId;
	$iId=Common::GetOne($sSql);*/
	if($iId > 0)
		return $iId;
	
	$aValues=array(
		'name'=>$sName,
		'surname'=>$sSurname,
		'index_letter'=>strtoupper(mb_substr($sSurname, 0, 1, 'utf8')),
		'created'=>'NOW()',
		'created_by'=>'import'
	);
	if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_authors_import", $aValues)) === false) {
		echo "error adding Author \n";
		dump($aValues);
		return false;
	}
	else {
		//dopisanie do tabeli autora i utworzenie mapowania
		$aValues2=$aValues;
		$aValues2['id']=$iId;
		$bErr=Common::Insert($aConfig['tabls']['prefix']."products_authors", $aValues2, '', false);
		if($bErr===false) return $bErr;
		
		$aValues3=array('author_id'=>$iId, 	'author_import_id'=>$iId);
		
		Common::Insert($aConfig['tabls']['prefix']."products_authors_mappings",
									 $aValues3, '', false);			
		
		return $iId;
		//koniec dopisania
		}			
} // end of getAuthorId3() function

/**
 * Pobiera id jędyka, jeśli nie istanieje dodaje go do bazy
 * 
 * @param string $sLanguage - język
 * @return int - id języka
 */
function getLanguageId($sLanguage){
	global $aConfig,$pDB;
	if(!empty($sLanguage)){
		$sSql="SELECT id FROM products_languages
					 WHERE language = ".$pDB->quoteSmart(stripslashes(trim($sLanguage)));
		$iId=Common::GetOne($sSql);
		if($iId > 0)
			return $iId;
		
		$aValues=array(
			'language'=>trim($sLanguage),
			'created'=>'NOW()',
			'created_by'=>'import'
		);
		if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_languages", $aValues)) === false) {
			echo "error adding Language \n";
			dump($aValues);
			return 'NULL';
		}
		else
			return $iId;		
	}
	else
		return 'NULL';	
} // end of getLanguageId() function

/**
 * pobiera id typu załącznika, jeśli nie istnieje dodaje go do bazy
 * @param $sAttachmentType - typ załącznka
 * @return int - id typu załącznika
 */
function getAttachmentTypeId($sAttachmentType){
	global $aConfig,$pDB;
	if(!empty($sAttachmentType)){
		$sSql="SELECT id FROM products_attachment_types
					 WHERE name = ".$pDB->quoteSmart(stripslashes(trim($sAttachmentType)));
		$iId=Common::GetOne($sSql);
		if($iId > 0)
			return $iId;
		
		$aValues=array(
			'name'=>trim($sAttachmentType),
			'created'=>'NOW()',
			'created_by'=>'import'
		);
		if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_attachment_types", $aValues)) === false) {
			echo "error adding Attachment type \n";
			dump($aValues);
			return false;
		}
		else
			return $iId;		
	}
	else
		return false;	
} // end of getAttachmentTypeId() function

/**
 * Pobiera id oprawy, jeśli nie istnieje dodaje go
 * @param string $sBinding - oprawa
 * @return int - id oprawy
 */
function getBindingId($sBinding){
	global $aConfig,$pDB;
	if(!empty($sBinding)){
		$sSql="SELECT id FROM products_bindings
					 WHERE binding = ".$pDB->quoteSmart(stripslashes(trim($sBinding)));
		$iId=Common::GetOne($sSql);
		if($iId > 0)
			return $iId;
		
		$aValues=array(
			'binding'=>trim($sBinding),
			'created'=>'NOW()',
			'created_by'=>'import'
		);
		if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_bindings", $aValues)) === false) {
			echo "error adding Binding \n";
			dump($aValues);
			return 'NULL';
		}
		else
			return $iId;	
	}
	else
		return 'NULL';			
} // end of getBindingId() function



/**
 * Dodaje powiązania kategorii do produktu
 * @param int $PId - id produktu
 * @param array $aCategories - tablica z id kategorii
 * @return bool - success
 */
function addExtraCategories($PId,$aCategories){
	global $aConfig,$sLogInfo;
	$aCategories = array_unique($aCategories);
	// pobierz dotychczasowe kategorie
	$aOldCategories = getExtraCategories($PId);
	// sprawdź czy są niepuste oraz czy stare różnią się od nowych
	if (!empty($aCategories) && ($aOldCategories != $aCategories)) {
		foreach($aCategories as $iCat) {
			// dodaj jeśli nowa kategoria jest niepusta, 
			// jeśli jest różna od nieposortowane,
			// nie jest istnieje aktualnie to mapowanie
			if (!empty($iCat) && 
					($iCat != $aConfig['import']['unsorted_category']) && 
					($iCat != $aConfig['import']['unsorted_category_editio']) && 
					!in_array($iCat, $aOldCategories)) {
				$aValues=array(
					'product_id'=>$PId,
					'page_id'=>$iCat
				);

				if (Common::Insert($aConfig['tabls']['prefix']."products_extra_categories", $aValues,'',false) === false) {
					echo "error adding Extra Category \n";
					$sLogInfo.= "error adding Extra Category \n";
					dump($aValues);
					return false;
				} else {
					$sLogInfo.= "adding Extra Category ".print_r($aValues, true)." \n";
					print_r($aValues);
				}
			}
		}
	}
	return true;
} // end of addExtraCategories() function

/**
 * Metoda pobiera dodatkowe kategorie dla produktu
 *
 * @global type $aConfig
 * @param type $iId
 * @return type 
 */
function &getExtraCategories($iId){
	global $aConfig;
	$sSql="SELECT page_id
				 FROM ".$aConfig['tabls']['prefix']."products_extra_categories
				 WHERE product_id = ".$iId;
	return Common::GetCol($sSql);
}// end of getExtraCategories() function 


/**
 * Pobiera tekstową wartość węzła xml
 * @param object ref $oXml - obiekt XMLReader
 * @return string - wartosc
 */
function getTextNodeValue(&$oXml) {
	// przejscie do kolejnego wezla
	if (!$oXml->isEmptyElement) {
		$oXml->read();
		if ($oXml->nodeType == XMLReader::TEXT || $oXml->nodeType == XMLReader::CDATA)
			return trim($oXml->value);
	}
	return '';
} // end of getTextNodeValue() method

/**
 * Pobiera wartosc sekcji CDATA z XML
 * @param object ref $oXml - obiekt XMLReader
 * @return string - wartość
 */
function getCDATAValue(&$oXml) {
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::CDATA:
				// WARTOSC PARAMETRU
				return trim($oXml->value);
			break;
		}
	}
} // end of getCDATAValue() function

/**
 * Tworzy folder i ustawia prawa dostępu dla folderu na okładke produktu
 * @param unknown_type $iPId
 * @return unknown_type
 */
function createProductImageDir($iPId){
	global $aConfig;
	$old_umask = umask(0);
	$iRange=intval(floor($iPId/1000))+1;
	if (!is_dir($aConfig['common']['base_path']."images/photos/okladki/".$iRange.'/'.$iPId)) {
		mkdir($aConfig['common']['base_path']."images/photos/okladki/".$iRange.'/'.$iPId,0777,true);
	}
	umask($old_umask);
} // end of createProductImageDir() function

/**
 * Czysći pliki XML ABE z błędu występującego w nagłówku, 
 * uniemozliwiającego sparsowanie xml
 * @param string $sFileSrc - plik źródłowy
 * @param string $sFileDst - plik docelowy
 * @return void
 */
function clearABEerror($sFileSrc,$sFileDst){
	global $aConfig;
	if (file_exists($sFileSrc) && (($oF = fopen($sFileSrc, 'r'))!==false)) {	
		$oFdst = fopen($sFileDst, 'w');
		if($oFdst !== false){
			// pobieranie pliku
			while(!feof($oF)) {
				// czyszczenie i zapisa
				$sData= fread($oF, 1024);
				$sData=str_ireplace('xmlns:xlink=&quot;','',$sData);
				fwrite($oFdst,$sData);
			}
			fclose($oFdst);
		}else {
			echo "cannot create ".$sFileDst." \n <br>";
		}
		fclose($oF);
		@unlink($sFileSrc);
	} else {
		echo "missing file ".$sFileSrc." or cannot open \n <br>";
	}
}

/**
 * Pobiera kategorie główne (działy)
 * @return array - tablica z id kategorii głównych
 */
function getTopCategories(){
		global $aConfig;
		$sSql="SELECT id 
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL";
		return Common::GetCol($sSql);
	} // end of getTopCategories() function
	
	/**
	 * Pobiera id kategorii oferty produktowej 2 poziomu
	 * @return array - tablica z id kategorii
	 */
	function getLevel2Categories(){
		global $aConfig;
		$sSql="SELECT id 
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND priority > 0";
		return Common::GetCol($sSql);
	} // end of getLevel2Categories()

	/**
	 * Zwraca kategorę główną (pierwszego poziomu) dla produktu
	 * @param $aCats - lista kategorii produktu
	 * @return unknown_type
	 */
	function getPageId(&$aCats){
	global $aConfig, $aTopCategories;
		// pobierz kategorie glowne
		if(empty($aTopCategories)){
			$aTopCategories=getTopCategories();
		}
		// wybierz z kategorii dodatkowych produktu kategorie glowne
		$aMatched=array_values(array_intersect($aCats,$aTopCategories));
		// zwroc pierwsza kategorie glowna
		if(!empty($aMatched)){
			return $aMatched[0];
		}
		// lub unsorted
		else{
			return $aConfig['import']['unsorted_category'];
		}
	} // end of getPageId() function
	
	/**
	 * Sprawdza czy wśród kategorii są kategorie 2 poziomu
	 * 
	 * @param $aCats - kategorie
	 * @return bool
	 */
	function checkLevel2Cats(&$aCats){
	global $aLevel2Categories;
	
		// pobierz kategorie 2 poziomu
		if(empty($aLevel2Categories)){
			$aLevel2Categories = getLevel2Categories();
		}
		if(!empty($aCats)){
			// wybierz z kategorii dodatkowych produktu kategorie 2 poziomu
			$aMatched = array_values(array_intersect($aCats,$aLevel2Categories));
			return !empty($aMatched);
		}
		return false;
	} // end of checkLevel2Cats()
	
	
	/**
	 * Metoda sprawdza czy jest to nowość
	 *
	 * @global type $aConfig
	 * @param integer $iId
	 * @return return array
	 */
	function &getNews($iId){
		global $aConfig;
		$sSql="SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_news
					 WHERE product_id = ".$iId;
		return Common::GetRow($sSql);
	}// end of getNews() method
	
	/**
	 * Metoda sprawdza czy jest to zapowiedz
	 *
	 * @global type $aConfig
	 * @param integer $iId
	 * @return return array
	 */
	function &getPreviews($iId){
		global $aConfig;
		$sSql="SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_previews
					 WHERE product_id = ".$iId;
		return Common::GetRow($sSql);
	}// end of getPreviews() method

	/**
	 * Metoda sprawdza czy jest to nowość
	 *
	 * @global type $aConfig
	 * @param integer $iId
	 * @return return array
	 */
	function &getNewsBecome(){
		global $aConfig;
		
		$sSql="SELECT A.product_id, B.page_id, B.publisher_id
					 FROM ".$aConfig['tabls']['prefix']."products_news_become AS A
					 JOIN ".$aConfig['tabls']['prefix']."products AS B
						 ON A.product_id = B.id";
		return Common::GetAll($sSql);
	}// end of getNewsBecome() method
	

	/**
	 * Dodaje pozycję do nowości
	 * @param int $PId - id produktu
	 * @param int $pageId - id kategorii nowości
	 * @return bool success
	 */
	function delNews($PId){
		global $aConfig;
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='0'
							WHERE id = ".$PId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if(existShadow($PId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news='0'
							WHERE id = ".$PId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
	 	
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_news
						 WHERE product_id = ".$PId;
 		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		return !$bIsError;

	} // end of addNews() function
	
	
	/**
	 * Dodaje pozycję do nowości
	 * @param int $PId - id produktu
	 * @param int $pageId - id kategorii nowości
	 * @return bool success
	 */
	function addNews($PId,$pageId){
		global $aConfig;
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='1'
							WHERE id = ".$PId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if(existShadow($PId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news='1'
							WHERE id = ".$PId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
	 	
 		$aValues = array (
	 		'product_id' => $PId,
	 		'news_from' => 'NOW()',
	 		'page_id' => $pageId
	 	);
 	
 		if(Common::Insert($aConfig['tabls']['prefix']."products_news",
 										$aValues,'',false) === false) {
			$bIsError = true;
		}
		return !$bIsError;

	} // end of addNews() function
	

	/*************** HELION *****************************************/
	
	/**
	 * Pobiera xml Helion
	 * @param $sURL - adres pliku xml
	 * @param $sDestFile - plik docelowy
	 * @param $iTimeOut - timeout
	 * @return bool - success
	 */
	function downloadHelionXML($sURL, $sDestFile, $iTimeOut=0) {
	global $aConfig;

	// otwarcie pliku
	if ($rFp = fopen($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/".$sDestFile, "w")) {
		$oCURL = curl_init();
		curl_setopt($oCURL, CURLOPT_URL, $sURL);
	 	curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
	 	if ($iTimeOut > 0) {
	 		curl_setopt($oCURL, CURLOPT_TIMEOUT, $iTimeOut);
	 	}
	 	curl_setopt($oCURL, CURLOPT_POST, FALSE);
	 	//curl_setopt($oCURL, CURLOPT_POSTFIELDS, $sParameters);
	 	curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
	 	curl_setopt($oCURL, CURLOPT_FILE, $rFp);

		curl_exec($oCURL);
		curl_close($oCURL);
		fclose ($rFp);
		return true;
	}
	return false;
} // end of downloadHelionXMLFile() function

/**
 * Sprawdza czy istnieje wpis w tabeli shadow dla produktu o danym id
 * @param int $iId - id produktu
 * @return bool
 */
	function existShadow($iId) {
		global $aConfig;
		// usuwanie cienia
		$sSql = "SELECT count(id) FROM ".$aConfig['tabls']['prefix']."products_shadow
							 WHERE id = ".$iId;
		return (Common::GetOne($sSql) > 0);
	} // end of existShadow() function
	
	/**
	 * Pobiera dane załącznika produktu o danym id
	 * @param int $iId - id produktu
	 * @return array ref - tablica z id, cena brutto i netto załącznika 
	 */
	function getAttachment($iId) {
	global $aConfig;
	
	$sSql = "SELECT A.id,A.price_brutto,A.price_netto
					 FROM ".$aConfig['tabls']['prefix']."products_attachments A
					 WHERE A.product_id= ".$iId;	
	return Common::GetRow($sSql);
} // end of getAttachment() function

/**
 * Pobiera mapowania kategorii dla wydawnictwa
 * @param int $iPublisherId - id wydawnictwa
 * @return array - tablica z id kategorii głównej i id kategorii nowości
 */
	function getPublisherCategories($iPublisherId){
		global $aConfig;
		$sSql = "SELECT main_category, news_category 
						FROM ".$aConfig['tabls']['prefix']."products_publishers
						WHERE id = ".$iPublisherId;
		return Common::GetRow($sSql);
	} // end of getPublisherCategories()
	
/**
 * Pobiera mapowania kategorii dla serii
 * @param int $iSeriesId - id serii
 * @return array - tablica z id kategorii głównej i id kategorii nowości
 */
	function getSeriesCategories($iSeriesId){
		global $aConfig;
		$sSql = "SELECT main_category, news_category 
						FROM ".$aConfig['tabls']['prefix']."products_series
						WHERE id = ".$iSeriesId;
		return Common::GetRow($sSql);
	} // end of getSeriesCategories()
	
	
	/**
	 * Metoda pobiera id serii na podstawie id produktu
	 *
	 * @global type $aConfig
	 * @param integer $iProdId
	 * @return integer 
	 */
	function getSeries($iProdId) {
		global $aConfig;
		
		$sSql = "SELECT series_id FROM ".$aConfig['tabls']['prefix']."products_to_series 
						 WHERE product_id=".$iProdId;
		return Common::GetOne($sSql);
	}// end of getSeries() method

    /**
     * @return mixed
     */
    function getCountProductsInOrders() {
        $sSql = '
                 SELECT OI.product_id, count(O.id)
                 FROM orders AS O
                 JOIN orders_items AS OI 
                    ON O.id = OI.order_id
                 WHERE O.order_status = "4" AND O.order_date > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)
                 AND OI.product_id IS NOT NULL 
                 GROUP BY OI.product_id
                 ';
        $orders =  Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
        return $orders;
    }

	/**
 * Generowanie inta do sortowania na listach
 * @param array ref $aItem - rekord z tabeli shadow
 * @return int - liczba do sortowania
 */
function getOrderByNr(&$aItem, $countInOrders){
	if($aItem['prod_status'] == '1' || $aItem['prod_status'] == '3'){
			if($aItem['shipment_time'] == '0'){
				$iGroup = 2;
				$iAvail = 1;
			}
			elseif($aItem['shipment_time'] == '1'){
				$iGroup = 2;
				$iAvail = 0;
			}
			elseif($aItem['shipment_time'] == '2'){
				$iGroup = 1;
				$iAvail = 1;
			}
			elseif($aItem['shipment_time'] == '3'){
				$iGroup = 1;
				$iAvail = 0;
			} else {
				$iGroup = 0;
				$iAvail = 0;
			}
		} else {
			$iGroup = 0;
			$iAvail = 0;
		}
		$bIsPreviewsORNews = 0;
		if (intval($aItem['is_news']) > 0 || intval($aItem['is_previews']) > 0) {
			$bIsPreviewsORNews = 1;
		}
		return sprintf("%01u%03u%01u%04u%06u",$iGroup,$countInOrders, intval($bIsPreviewsORNews),intval($aItem['publication_year']),$aItem['id']);
} // end of getOrderByNr() function


/**
 * Generowanie inta do sortowania na listach
 * @param $aItem - rekord z tabeli shadow
 * @return int - liczba do sortowania
 */
function getOrderByStatusOldStyle(&$aItem){
    if($aItem['prod_status'] == '1' || $aItem['prod_status'] == '3'){
        if($aItem['shipment_time'] == '0'){
            $iGroup = 2;
            $iAvail = 1;
        }
        elseif($aItem['shipment_time'] == '1'){
            $iGroup = 2;
            $iAvail = 0;
        }
        elseif($aItem['shipment_time'] == '2'){
            $iGroup = 1;
            $iAvail = 1;
        }
        elseif($aItem['shipment_time'] == '3'){
            $iGroup = 1;
            $iAvail = 0;
        } else {
            $iGroup = 0;
            $iAvail = 0;
        }
    } else {
        $iGroup = 0;
        $iAvail = 0;
    }

    $bIsPreviewsORNews = 0;
    if (intval($aItem['is_news']) > 0 || intval($aItem['is_previews']) > 0) {
        $bIsPreviewsORNews = 1;
    }
    return sprintf("%01u%01u%04u%09u",$iGroup,intval($bIsPreviewsORNews),intval($aItem['publication_year']),$aItem['id']);
}

/**
 * Poiera wpis produktu z tabeli shadow
 * @param $iId - id produktu
 * @return array ref - wpis z tabeli shadow
 */
function &getProductShadow($iId) {
	global $aConfig;
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT *
  				 FROM ".$aConfig['tabls']['prefix']."products_shadow
  				 WHERE id =".$iId;
  return Common::GetRow($sSql);
} // end of getProductShadow()

/**
 * Czyści błędne kody ascii z opisu
 * @param string $sStr - string wejściowy
 * @return string - przeczyszczony
 */
function clearAsciiCrap($sStr) {
  $sStr = (string)str_replace("\x0A", '<br />', $sStr);
  return (string)str_replace(array("\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x09","\x0B","\x0C","\x0D","\x0E","\x0F","\x10","\x11","\x12","\x13","\x14","\x15","\x16","\x17","\x18","\x19","\x1A","\x1B","\x1C","\x1D","\x1E","\x1F"), ' ', $sStr);
} // end of clearAsciiCrap() function

/**
 * Mapuje stawki vat ze starych na nowe wg zadanej tablicy
 * @param iVat - wejsciowa stakwa vat
 * @return zmapowana stakwa vat
 * */
function vatMapper($iVat) {
	$aVatMap=array(
		7=>8,
		22=>23,
		'7'=>8,
		'22'=>23
		);
	if(isset($aVatMap[intval(trim($iVat))]) && $aVatMap[intval(trim($iVat))] > 0)
		return $aVatMap[$iVat];
	else 
		return $iVat;		
}//end of vatMapper() function

/**
 * Metoda sprawdza czy autor jest już powiązany z daną książką
 *
 * @global type $aConfig
 * @param type $PId
 * @param type $iAId
 * @param type $iAuthorRoleId
 * @return bool 
 */
function checkAutor($PId, $iAId, $iAuthorRoleId) {
	global $aConfig;
	
	if (intval($PId) > 0 && intval($iAId) > 0 && intval($iAuthorRoleId) > 0) {
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_to_authors 
						WHERE product_id=".$PId." AND author_id=".$iAId." AND role_id=".$iAuthorRoleId;
		return (intval(Common::GetOne($sSql)) > 0 ? true : false);
	}
	return false;
} // end of checkAutor() method

/**
 * Metoda dodaje autora - tłumacza
 *
 * @global type $aConfig
 * @global string $sLogInfo
 * @param type $PId
 * @param type $sAuthors
 * @return boolean 
 */
function addHelionTranslations($PId,$sAuthors){
	global $aConfig,$sLogInfo, $pDbMgr;
	$iAuthorRoleId=7;
	
	if(!empty($sAuthors)){
		$aAuthors = split(',',$sAuthors);
		foreach($aAuthors as $iKey => $sAuth){
			$sAuth = trim($sAuth);
			if( !empty($sAuth) ){
				$aValues = array(
					'product_id' => $PId,
					'author_id' => getAuthorId3($sAuth),
					'role_id' => $iAuthorRoleId,
					'order_by' => $iKey
				);
				if (checkAutor($aValues['product_id'], $aValues['author_id'], $aValues['role_id']) == false) {
					// brak aktualnie autora w pozycji, dodajemy
					if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."products_to_authors", $aValues,'',false) === false) {
						echo date('d.m.Y')." ".date('H:i:s')." błąd dodawania autorów do produktu ".$PId."<br>\n";
						$sLogInfo.= "błąd dodawania autorów do produktu ".$PId." \n";
						return false;
					}
				} else {
					// istnieje już ten autor przy tej pozycji
				}
			}
		}
	}
	return true;
} // end of addHelionTranslations() method


function addHelionAuthors($PId,$sAuthors){
	global $aConfig,$sLogInfo;
	$iAuthorRoleId=1;
	// XXX TODO usunac;
	/*
	$sSql = "DELETE FROM products_to_authors WHERE product_id=".$PId." AND role_id=".$iAuthorRoleId;
	Common::Query($sSql);
	*/
	if(!empty($sAuthors)){
		$aAuthors=split(',',$sAuthors);
		foreach($aAuthors as $iKey=>$sAuth){
			$sAuth=trim($sAuth);
			if(!empty($sAuth)){
				$aValues=array(
					'product_id'=>$PId,
					'author_id'=>getAuthorId3($sAuth),
					'role_id'=>$iAuthorRoleId,
					'order_by'=>$iKey
				);
				if (checkAutor($aValues['product_id'], $aValues['author_id'], $aValues['role_id']) == false) {
					if (Common::Insert($aConfig['tabls']['prefix']."products_to_authors", $aValues,'',false) === false) {
						echo date('d.m.Y')." ".date('H:i:s')." błąd dodawania autorów do produktu ".$PId."<br>\n";
						$sLogInfo.= "błąd dodawania autorów do produktu ".$PId." \n";
						//dump($aValues);
						return false;
					}
				} else {
					// istnieje już ten autor przy tej pozycji
				}
			}
		}
	}
	return true;
}


function getHelionPublisher($sBookstore,$aHelionCats) {
	global $aConfig;
	$sSql = "SELECT publisher 
						FROM ".$aConfig['tabls']['prefix']."products_helion_mapping
						WHERE source = '".$sBookstore."' AND helion_id IN (".implode(',',$aHelionCats).") LIMIT 1";
	$sPubl = Common::GetOne($sSql);
	return getPublisherId($sPubl);
}


/**
 * Metoda pobiera id wydawnictwa dla produktu z Helion
 *
 * @param integer $iMarka
 * @return string 
 */
function getHelionPublisherIdByBrand($iMarka) {
	$aHelionBrans = array(
			'1' => 'Helion Wydawnictwo', // - helion
			'2' => 'Helion - Onepress', //- onepress
			'3' => 'Helion - Editio', //- editio
			'4' => 'Helion - Sensus',//- sensus
			'5' => 'Helion - Septem',//- septem
			'6' => 'Bezdroża',//- bezdroza
			'8' => 'Helion - Edukacja',//- podrecznik
			'11' => 'Bezdroża'//- bezdroża obce
	);
	
	$sPublisher = '';
	if (empty($aHelionBrans[$iMarka])) {
		$sPublisher = 'Helion Wydawnictwo';
	} else {
		$sPublisher = $aHelionBrans[$iMarka];
	}
	$iPublisherId = getPublisherId($sPublisher);
	return $iPublisherId;
}// end of getHelionPublisherIdByBrand() function


/**
 * Funkcja określa rabat, na podstawie marki produktu
 * 
 * @param int $iMarka
 * @return int
 */
function getHelionDiscountByBrand($iMarka) {
	$aHelionBrans = array(
			'1' => 34,// 'Helion Wydawnictwo', // - helion
			'2' => 34,//'Helion - Onepress', //- onepress
			'3' => 34,//'Helion - Editio', //- editio
			'4' => 34,//'Helion - Sensus',//- sensus
			'5' => 34,//'Helion - Septem',//- septem
			'6' => 34,//'Bezdroża',//- bezdroza
			'8' => 20, // 'Helion - Edukacja',//- podrecznik
			'11' => 34,// 'Bezdroża'//- bezdroża obce
	);
  if (isset($aHelionBrans[$iMarka])) {
    return $aHelionBrans[$iMarka];
  } else {
    return 34;
  }
}// end of getHelionDiscountByBrand() method


function getOneHelionMapping($sBookstore,$iHelionId) {
	global $aConfig;
	$sSql = "SELECT page_id
					FROM ".$aConfig['tabls']['prefix']."products_helion_mapping 
					WHERE source = '".$sBookstore."' AND helion_id = ".$iHelionId;
	return Common::GetCol($sSql);
}

function getHelionMappings($sBookstore,$aHelionCats) {
	global $aConfig;
	$aCats=array();
	
	if (!empty($aHelionCats) && is_array($aHelionCats)) {
		foreach($aHelionCats as $iHelionCat) {
			$aTCats = getOneHelionMapping($sBookstore,$iHelionCat);
			if(!empty($aTCats)) {
				foreach($aTCats as $iCat) {
					$aPath = getCategoriesPathIDs($iCat,1);
					if(!empty($aPath)) {
						$aCats=array_merge($aCats,$aPath);
					}
				}
			}
		}
		if(!empty($aCats)){
			$aCats = array_unique($aCats);
		}
	}
	return $aCats;
}


	/**
   * Funkcja tworzaca na podstawie przekazanego do niej Id strony
   * jej pelna sciezke od korzenia do niej
   *
   * @param	integer	$iId	- ID strony do ktorej ma byc tworzona sciezka
   * @param	integer	$iLangId	- Id wersji jezykowej
   * @return	array	- uporzadkowana tablica ze sciezka do strony
   */
  function getCategoriesPathIDs($iId, $iLangId) {
	global $aConfig;
	$aPath		= array();
		
  	$sSql = "SELECT A.id, IFNULL(A.parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
  					 WHERE A.id = ".$iId." AND
  					 			 A.language_id = ".$iLangId;
  	$aItem =& Common::GetRow($sSql);
  	if(!empty($aItem)) {
	  	$aPath[] = $aItem['id'];
	
	  	while (intval($aItem['parent_id']) > 0) {
	  		$sSql = "SELECT id, IFNULL(parent_id, 0) AS parent_id
	  					 FROM ".$aConfig['tabls']['prefix']."menus_items
	  					 WHERE id = ".$aItem['parent_id']." AND
	  					 			 language_id = ".$iLangId;
		  	$aItem =& Common::GetRow($sSql);
		  	$aPath[] = $aItem['id'];
	  	}
		return array_reverse($aPath);
  	} else {
  		return false;
  	}
  } // end getPathItems() function
	

	/**
	 * Znajduje ksiazke która posiada odpowiednie id heliona
	 *
	 * @param type $aHid
	 * @return type 
	 */
	function findBookByHelionId($aHid) {
		global $aConfig;
		
		foreach($aHid as $iKey =>$sVal){
			if ( strlen($sVal) < 3 )
				unset($aHid[$iKey]);
		}
		//pobranie ksiazki ktora posiada zadany index i nie jest wyczerpana
		$sSql='SELECT id FROM '.$aConfig['tabls']['prefix'].'products 
					 WHERE `helion_index` IN (\''.implode('\',\'', $aHid).'\') 
						 AND `prod_status`<>\'2\' LIMIT 12';

		return Common::GetAll($sSql);
	}// end of findBookByHelionId() function

	
	/**
	 * Metoda sprawdza czy istnieje już powiazanie o podanych danych
	 *
	 * @global type $aConfig
	 * @param integer $iPId - id produktu 
	 * @param integer $iLId - id produktu wiazanego
	 * @return bool
	 */
	function checkLinked($iPId, $iLId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_linked
						 WHERE linked_id=".$iLId." AND product_id=".$iPId;
		return (intval(Common::GetOne($sSql)) > 0 ? true : false );
	}// end of checkLinked() function
	
	
	/**
	 * Dodaje wpisy o powiązanych do bazy
	 * 
	 * @param aLinks  powiązane
	 * @param iId  id produktu
	 * @return bool
	 */
	function addLinking($aLinks, $iId) {
		global $aConfig;

		$aValues['product_id']=$iId;
		foreach($aLinks as $aId) {
			$aValues['linked_id']=$aId['id'];
			if (checkLinked($aValues['product_id'], $aValues['linked_id']) === false) {
				$iIsErr+=(Common::Insert($aConfig['tabls']['prefix']."products_linked", $aValues) === false);
			} else {
				// istnieje już to powiązanie dla tego produktu
			}
		} 

		return $iIsErr;
	}// end of addLinking() function



	function addHelionImage($iProductId,$sCoverURL,$sIsbn,$sHelionId){
		global $aConfig;


		if(!downloadHelionImage($sCoverURL,$sHelionId.".jpg",''))
			return false;
		$sDstName=$sIsbn.".jpg";
		$sSrcFile=$aConfig['common']['base_path'].'images/photos/helion_okladki/'.$sHelionId.".jpg";


		createProductImageDir($iProductId);
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = $aConfig['common']['base_path'].'images/photos/okladki/'.$iRange.'/'.$iProductId;
		$sShortDir='okladki/'.$iRange.'/'.$iProductId;

		if(!file_exists($sSrcFile))
			return false;

		$aFileInfo = getimagesize($sSrcFile);

		resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['thumb_size'], '__t_'.$sDstName);
		resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['small_size'], $sDstName);
		$aSmallInfo = getimagesize($sDestDir.'/'.$sDstName);
		if($aFileInfo[0] > intval(1.5 * $aSmallInfo[0]) || $aFileInfo[1] > intval(1.5 * $aSmallInfo[1])){
			resizeAndMoveImageTo($sSrcFile, $aFileInfo, $sDestDir, $aConfig['import']['big_size'], '__b_'.$sDstName);
		}

		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id=".$iProductId;
		$iId = Common::GetOne($sSql);
		if ($iId > 0) {
			// zdjecie juz istnieje w bazie nie dodajemy ponownie
			return true;
		}
		$aValues=array(
			'product_id' => $iProductId,
			'directory' => $sShortDir,
			'photo' => $sDstName,
			'mime' => 'image/jpeg',
			'name' => $sDstName,
			'order_by' => '1'
		);
		return (Common::Insert($aConfig['tabls']['prefix']."products_images", $aValues, '', false)!=false);
	}
	

	function downloadHelionImage($sURL, $sDestFile, $sParameters, $iTimeOut=0) {
		global $aConfig;

		// otwarcie pliku
		if(file_exists($aConfig['common']['base_path'].'/images/photos/helion_okladki/'.$sDestFile))
			return false;
		
		if ($rFp = fopen($aConfig['common']['base_path'].'/images/photos/helion_okladki/'.$sDestFile, "w")) {
			$oCURL = curl_init();
			curl_setopt($oCURL, CURLOPT_URL, $sURL);
			curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
			if ($iTimeOut > 0) {
				curl_setopt($oCURL, CURLOPT_TIMEOUT, $iTimeOut);
			}
			curl_setopt($oCURL, CURLOPT_POST, FALSE);
			//curl_setopt($oCURL, CURLOPT_POSTFIELDS, $sParameters);
			curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, FALSE);
			curl_setopt($oCURL, CURLOPT_FILE, $rFp);

			curl_exec($oCURL);
			curl_close($oCURL);
			fclose ($rFp);
			return true;
		}
		return false;
	} // end of downloadXMLFile() function
?>