<?php
$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600);


include_once ('import_func.inc.php');


/**
 * Funkcja porównuje status produktu, czy jest potrzebna aktualizacja danych, 
 *  jeśli tak zwraca tablicę danych które się zmieniły
 * 
 * @param array $aNowStatus
 * @param array $aNewStatus
 * @return boolean
 */
function compareStatus($aNowStatus, $aNewStatus) {
  $aCompareStatus = array(
    'azymut_status',
    'azymut_shipment_time',
    'azymut_stock'
  );
  
  $aChagedStatus = array();
  foreach ($aCompareStatus as $sStatus) {
    if ($aNowStatus[$sStatus] != $aNewStatus[$sStatus] && 
            ($aNowStatus[$sStatus] != null || $aNewStatus[$sStatus] != 'NULL')) {
       $aChagedStatus[$sStatus] = $aNewStatus[$sStatus];
    }
  }
  return $aChagedStatus;
} // end of compareStatus() method


/**
 * Zoptymalizowane zapytanie wybierające status produktu
 * 
 * @global array $aConfig
 * @param type $sAsymutBookindex
 * @return type
 */
function getOptimizedAzymutStock($sAsymutBookindex) {
	global $aConfig;

	$sSql = "SELECT A.id, A.azymut_status, A.azymut_shipment_time, A.azymut_stock, B.prod_status
					 FROM ".$aConfig['tabls']['prefix']."products_stock A
					 JOIN ".$aConfig['tabls']['prefix']."products B
					 ON B.id = A.id
					 WHERE B.azymut_index = '".$sAsymutBookindex."'";	
	return Common::GetRow($sSql);
} // end of getOptimizedAzymutStock() function


// zmienne
include_once ('CommonSynchro.class.php');
$GLOBALS['oCommonSynchro'] = new CommonSynchro();
$oCommonSynchro->iSourceId = '2';

// XXX TODO development uwaga
if ($aConfig['common']['status']	!= 'development') {
  if(!downloadAzymutXML("getAvail","azymut_avail.xml")){
    sendInfoMail('Błąd importu',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." nie udało się pobrać pliku xml stanów magazynowych z Azymut");
    die("błąd pobierania xml stanów");
  }
}

$iCount = 0;
$sNow=getMysqlNow();
// dodanie dostępnych
if (file_exists($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/azymut_avail.xml")) {
//	sendInfoMail('Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." rozpoczęto aktualizacje stanów azymut");
	
	$oXml = new XMLReader();
	$oXml->open($aConfig['common']['base_path'].$aConfig['common']['import_dir']."/XML/azymut_avail.xml");
	
	while(@$oXml->read()) {
		switch ($oXml->nodeType) {
			case XMLReader::END_ELEMENT :
				switch ($oXml->name) {
					case 'avail':
						// KONIEC LISTINGU dostepnosci
						$oXml->close();
					break;
				}
			break;
		
		case XMLReader::ELEMENT :
			switch ($oXml->name) {
				case 'book':
        {
          $iCount++;
					$sId = $oXml->getAttribute('indeks');
					$sEnding = $oXml->getAttribute('naukonczeniu');
					$sStatusQuantity = $oXml->getAttribute('p');
					$aStatus = getAzymutStock($sId);
					// usunięcie  && empty($sEnding) - spowoduje zmiana na dostępne książek które są na ukończeniu
					// modyfikacja ustalona 13.03.2012 z Marcinem Chudy
					$sStatus = 'dostepny';
					if (!empty($sEnding)) {
						$sStatus = 'naukonczeniu';
					}
					
          if (!empty($sStatusQuantity)) {
            $sNewStatus = $oCommonSynchro->parseProductStock('azymut', $sStatusQuantity);
          } else {
            $sNewStatus = $oCommonSynchro->parseProductStock('azymut', $sStatus);
          }
          
          $aStatusValues = array(
            'azymut_status' => '1',
            'azymut_shipment_time' => '1',
            'azymut_stock' => $sNewStatus != '' ? $sNewStatus : 'NULL'
          );
          $aChangedValues = compareStatus($aStatus, $aStatusValues);
					if(!empty($aStatus) && ($aStatus['prod_status'] != '2')){
            // nie zmieniamy statusu jeśli zapowiedź, tutaj nie zmieniamy stanu produktu
            $aChangedValues['azymut_last_import'] = $sNow;

			$p = $oXml->getAttribute('p');

            if ("1-2" == $p) {
				$aChangedValues['azymut_status'] = '0';
			}

            if ((Common::Update($aConfig['tabls']['prefix']."products_stock", $aChangedValues,"id = ".$aStatus['id'])) === false) {
                        echo("błąd aktualizacji stanu produktu ".$aStatus['id']."<br/>\n");
            }
					} 
          // UWAGA optymalizacja z 06.12.2012 - zmiana stanu produktu przy cenach nie jest uwzględniana
          // zmiana last import w produktach która się nie zmieniły
          //updateLastImport(getProductIdByBookindex($sAsymutBookindex));
        }
				break;
			}
		}
	}
	
  // dla pewności przynajmniej 100 produktów w XML
  if ($iCount > 100) {
    // zmiana produktów na niedostepne
    setAzymutUnavaible($sNow);
  }
//  sendInfoMail('Azymut',"Dnia ".date('d.m.Y')." o godzinie ".date('H:i:s')." zakończono aktualizacje stanów azymut <br />".$sLogInfo);
	// usuniecie pliku
	@rename($aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/azymut_avail.xml',
					$aConfig['common']['base_path'].$aConfig['common']['import_dir'].'/XML/old/azymut_avail_'.date('d_m_Y').'.xml');
  
}
