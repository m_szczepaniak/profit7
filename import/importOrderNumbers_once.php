<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 25.08.17
 * Time: 08:41
 */


error_reporting(E_ERROR | E_WARNING | E_PARSE);
//error_reporting(E_ALL);
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');

$sSql = 'SELECT order_number FROM orders WHERE DATE(order_date) = "'.date('Y-m-d').'"';
$orderNumbers = $pDbMgr->GetCol('profit24', $sSql);
var_dump($orderNumbers);

foreach ($orderNumbers as $orderNumber) {

    preg_match('/\d{6}(\d{4})\d{2}/', $orderNumber, $refMatches);
    $newRandom = $refMatches[1];
    echo sprintf("%04d", $newRandom)."\r\n";
    $aValues = [
        'rand' => $newRandom,
        'date' => date('Ymd')
    ];
    if (false === $pDbMgr->Insert('profit24', 'orders_numbering_rand', $aValues)){
        $pDbMgr->RollbackTransaction('profit24');
        echo 'DUPLIKAT '.$newRandom."\r\n";
//                throw new \Exception('88829 - Wystąpił błąd podczas losowania kolejnego numeru zamówienia ');
    }
}