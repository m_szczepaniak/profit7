
aBooks = [{"ident":"1301912,4510744","quantity":"1","name":"Doktor Ty O wewn\u0119trznej sile organizmu i zdolno\u015bci do samouzdrawiania","publisher":"Otwarte","authors":"HOWICK JEREMY","publication_year":"2018","product_id":"1075919","isbn_plain":"9788375154993","isbn_10":null,"isbn_13":null,"ean_13":"9788375154993","pack_number":"2000004030","profit_g_act_stock":"43","photo":"okladki\/1076\/1075919|9788375154993.jpg","weight":"0.59","ident_summary":"1301912,4510744,1","sub_products":[{"id":"4510744","order_id":"1301912","quantity":"1","product_id":"1075919"}],"shelf_number":"2000004030","checked":"0","currquantity":"0"},{"ident":"1301912,4510745","quantity":"3","name":"Nieko\u0144cz\u0105ca si\u0119 historia, wyd. 3","publisher":"Znak","authors":"Ende Michael","publication_year":"2018","product_id":"1078864","isbn_plain":"9788324050482","isbn_10":null,"isbn_13":null,"ean_13":"9788324050482","pack_number":"2000004030","profit_g_act_stock":"28","photo":"okladki\/1079\/1078864|9788324050482.jpg","weight":"0.56","ident_summary":"1301912,4510745,3","sub_products":[{"id":"4510745","order_id":"1301912","quantity":"3","product_id":"1078864"}],"shelf_number":"2000004030","checked":"0","currquantity":"0"},{"ident":"1301912,4510743","quantity":"1","name":"Podatek od z\u0142udze\u0144","publisher":"POLIGRAF","authors":"Wajda Piotr S.","publication_year":"2013","product_id":"734322","isbn_plain":"9788378561811","isbn_10":null,"isbn_13":null,"ean_13":"9788378561811","pack_number":"2000004070","profit_g_act_stock":"5","photo":"okladki\/735\/734322|9788378561811.jpg","weight":"0.21","ident_summary":"1301912,4510743,1","sub_products":[{"id":"4510743","order_id":"1301912","quantity":"1","product_id":"734322"}],"shelf_number":"2000004070","checked":"0","currquantity":"0"},{"ident":"1307652,4530809","quantity":"1","name":"Quiz 3-latka Edukacyjna ksi\u0105\u017ceczka z naklejkami","publisher":"AKSJOMAT Piotr Nodzy\u0144ski","authors":"","publication_year":"2017","product_id":"1062094","isbn_plain":"9788381061438","isbn_10":null,"isbn_13":null,"ean_13":"9788381061438","pack_number":"2000015050","profit_g_act_stock":"0","photo":"okladki\/1063\/1062094|9788381061438.jpg","weight":"0.08","ident_summary":"1307652,4530809,1","sub_products":[{"id":"4530809","order_id":"1307652","quantity":"1","product_id":"1062094"}],"shelf_number":"2000015050","checked":"0","currquantity":"0"},{"ident":"1280909,4436909","quantity":"1","name":"Kulfon co z ciebie wyro\u015bnie przeboje profesora ciekawskiego + cd","publisher":"LITERATURA","authors":"Grabowski Andrzej","publication_year":"2010","product_id":"553919","isbn_plain":"9788376720784","isbn_10":null,"isbn_13":"9788376720784","ean_13":"9788376720784","pack_number":"2000020040","profit_g_act_stock":"0","photo":"okladki\/554\/553919|9788376720784.jpg","weight":"0.33","ident_summary":"1280909,4436909,1","sub_products":[{"id":"4436909","order_id":"1280909","quantity":"1","product_id":"553919"}],"shelf_number":"2000020040","checked":"0","currquantity":"0"},{"ident":"1312804,4548874","quantity":"1","name":"Diabe\u0142 woli \u015awi\u0119tych","publisher":"W DRODZE","authors":"KELEN JACQUELINE","publication_year":"2018","product_id":"1069056","isbn_plain":"9788379061921","isbn_10":null,"isbn_13":null,"ean_13":"9788379061921","pack_number":"2000026040","profit_g_act_stock":"0","photo":"okladki\/1070\/1069056|9788379061921.jpg","weight":"0.19","ident_summary":"1312804,4548874,1","sub_products":[{"id":"4548874","order_id":"1312804","quantity":"1","product_id":"1069056"}],"shelf_number":"2000026040","checked":"0","currquantity":"0"},{"ident":"1295285,4511853","quantity":"1","name":"Aurora","publisher":"ONGRYS","authors":"R\u00f3\u017ca\u0144 Krzysztof, Michalski Jacek, Janicki Andrzej","publication_year":"2017","product_id":"1043036","isbn_plain":"9788365803177","isbn_10":null,"isbn_13":null,"ean_13":"9788365803177","pack_number":null,"profit_g_act_stock":"1","photo":"okladki\/1044\/1043036|9788365803177.jpg","weight":"0.74","ident_summary":"1295285,4511853,1","sub_products":[{"id":"4511853","order_id":"1295285","quantity":"1","product_id":"1043036"}],"shelf_number":null,"checked":"0","currquantity":"0"},{"ident":"1295285,4511873","quantity":"1","name":"Osadnicy: Narodziny Imperium","publisher":"Portal Games","authors":"Trzewiczek Ignacy","publication_year":"2014","product_id":"996900","isbn_plain":"5902560380705","isbn_10":null,"isbn_13":"5908310266572","ean_13":"5902560380705","pack_number":null,"profit_g_act_stock":"1","photo":"1\/2346\/996900|5902560380705.png","weight":"1.30","ident_summary":"1295285,4511873,1","sub_products":[{"id":"4511873","order_id":"1295285","quantity":"1","product_id":"996900"}],"shelf_number":null,"checked":"0","currquantity":"0"},{"ident":"1227480,4252500","quantity":"1","name":"Puc, Bursztyn i go\u015bcie audiobook","publisher":"NASZA KSI\u0118GARNIA","authors":"Grabowski Jan","publication_year":"2011","product_id":"542855","isbn_plain":"9788310119391","isbn_10":null,"isbn_13":"9788310119391","ean_13":"9788310119391","pack_number":null,"profit_g_act_stock":"14","photo":"okladki\/543\/542855|9788310119391.jpg","weight":"0.09","ident_summary":"1227480,4252500,1","sub_products":[{"id":"4252500","order_id":"1227480","quantity":"1","product_id":"542855"}],"shelf_number":null,"checked":"0","currquantity":"0"},{"ident":"1295285,4511865","quantity":"1","name":"Ranxerox","publisher":"Kultura gniewu","authors":"LIBERATORE TANINO","publication_year":"2016","product_id":"956909","isbn_plain":"9788364858437","isbn_10":null,"isbn_13":null,"ean_13":"9788364858437","pack_number":null,"profit_g_act_stock":"2","photo":"okladki\/957\/956909|9788364858437.jpg","weight":"1.05","ident_summary":"1295285,4511865,1","sub_products":[{"id":"4511865","order_id":"1295285","quantity":"1","product_id":"956909"}],"shelf_number":null,"checked":"0","currquantity":"0"}];
aBooksToSend = [];

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
var t = typeof (obj);
if (t != "object" || obj === null) {
// simple data type
if (t == "string")
obj = '"' + obj + '"';
return String(obj);
}
else {
// recurse array or object
var n, v, json = [], arr = (obj && obj.constructor == Array);
for (n in obj) {
v = obj[n];
t = typeof (v);
if (t == "string")
v = '"' + v + '"';
else if (t == "object" && v !== null)
v = JSON.stringify(v);
json.push((arr ? "" : '"' + n + '":') + String(v));
}
return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
}
};


// pozostało do zeskanowania
$(function () {
aBookToScan = ({});
scannedBooks = ({});

if(typeof(Storage) !== "undefined") {
var iListId = $("#orders_items_lists_id").val();

tmpData = localStorage.getItem("aBooksToSend[" + iListId + "]");
var is_local_storage = false;

if (tmpData !== null) {
aBooksToSend = JSON.parse(tmpData);
is_local_storage = true;
}

tmpDataa = localStorage.getItem("aBooks[" + iListId + "]");
if (tmpDataa !== null) {
aBooks = JSON.parse(tmpDataa);
is_local_storage = true;
}

tmpDataa = localStorage.getItem("scannedBooks[" + iListId + "]");
if (tmpDataa !== null) {
scannedBooks = JSON.parse(tmpDataa);
is_local_storage = true;
}


if (is_local_storage === false) {
// pobieramy ajax
var aaa = ({
'method': 'get',
'orders_items_lists_id': $("#orders_items_lists_id").val()
});
$.ajax({
data: aaa,
dataType: "json",
type: "POST",
cache: false,
async: false,
url: "ajax/completationListStatus.php"
})
.done(function(data) {
if (data !== null) {
if (data['aBooks'] !== undefined && data['aBooksToSend'] !== undefined && data['scannedBooks'] !== undefined) {
aBooks = data['aBooks'];
aBooksToSend = data['aBooksToSend'];
scannedBooks = data['scannedBooks'];
}
}
});
}
}

quantityAllBooks = 0;
for (var i = 0; i < aBooks.length; i++)
{
if (aBooks[i] !== null && parseInt(aBooks[i].quantity) > 0) {
quantityAllBooks += parseInt(aBooks[i].quantity);
quantityAllBooks -= parseInt(aBooks[i].currquantity);
}
}
$("#left_order_items").html(quantityAllBooks);
});


// przekazanie danych
$("#ordered_itm").submit( function(){
var serializedBooksScanned = JSON.stringify(aBooksToSend);
var serializedBooksNotScanned = JSON.stringify(aBooks);

if (serializedBooksScanned !== undefined) {
$("#books").val(serializedBooksScanned);
}
if (serializedBooksNotScanned !== undefined) {
$("#not_scanned_books").val(serializedBooksNotScanned);
}

if(typeof(Storage) !== "undefined") {
localStorage.clear();
/*
var iListId = $("#orders_items_lists_id").val();
localStorage.setItem("aBooksToSend[" + iListId + "]", "");
localStorage.setItem("aBooks[" + iListId + "]", "");
*/
}
});

function addChar(char) {
if (char == '↵') {
$("#search_isbn").focus();
var e = $.Event('keydown');
e.which = 13;
$("#search_isbn").trigger(e);
} else {
$("#search_isbn").val($("#search_isbn").val() + char);
}
}

