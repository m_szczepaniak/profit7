<?php
header('Content-type: text/css');
?>
#lbOverlay { position: fixed; top: 0; left: 0; z-index: 99998; width: 100%; height: 500px; }
	#lbOverlay { background-color: #000000; }
	
#lbMain { position: absolute; left: 0; width: 100%; z-index: 99999; text-align: center; line-height: 0; }
#lbMain a img { border: none; }

#lbOuterContainer { position: relative; background-color: #fff; width: 200px; height: 200px; margin: 0 auto; }
	#lbOuterContainer { border: 3px solid #888888; }
	
#lbDetailsContainer {	font: 10px Verdana, Helvetica, sans-serif; background-color: #fff; width: 100%; line-height: 1.4em;	overflow: auto; margin: 0 auto; }
	#lbDetailsContainer { border: 3px solid #888888; border-top: none; }
	
#lbImageContainer, #lbIframeContainer { padding: 10px; }
#lbLoading {
	position: absolute; top: 45%; left: 0%; height: 32px; width: 100%; text-align: center; line-height: 0; background: url(/images/gfx/<?=$_GET['lang'];?>/lytebox/loading.gif) center no-repeat;
}

#lbHoverNav { position: absolute; top: 0; left: 0; height: 100%; width: 100%; z-index: 10; }
#lbImageContainer>#lbHoverNav { left: 0; }
#lbHoverNav a { outline: none; }

#lbPrev { width: 49%; height: 100%; background: transparent url(/images/gfx/<?=$_GET['lang'];?>/lytebox/blank.gif) no-repeat; display: block; left: 0; float: left; }
	#lbPrev:hover, #lbPrev:visited:hover { background: url(/images/gfx/<?=$_GET['lang'];?>/lytebox/prev_grey.gif) left 15% no-repeat; }
	
#lbNext { width: 49%; height: 100%; background: transparent url(/images/gfx/<?=$_GET['lang'];?>/lytebox/blank.gif) no-repeat; display: block; right: 0; float: right; }
	#lbNext:hover, #lbNext:visited:hover { background: url(/images/gfx/<?=$_GET['lang'];?>/lytebox/next_grey.gif) right 15% no-repeat; }
	
#lbPrev2, #lbNext2 { text-decoration: none; font-weight: bold; }
	#lbPrev2, #lbNext2, #lbSpacer { color: #333333; }
	
#lbPrev2_Off, #lbNext2_Off { font-weight: bold; }
	#lbPrev2_Off, #lbNext2_Off { color: #CCCCCC; }
	
#lbDetailsData { padding: 0 10px; }
	#lbDetailsData { color: #333333; }
	
#lbDetails { width: 60%; float: left; text-align: left; }
#lbCaption { display: block; font-weight: bold; padding-bottom: 10px; }
#lbNumberDisplay { float: left; display: block; padding-bottom: 1.0em; }
#lbNavDisplay { float: left; display: block; padding-bottom: 1.0em; }

#lbClose { width: 66px; height: 22px; float: right; margin-bottom: 1px; }
	#lbClose { background: url(/images/gfx/<?=$_GET['lang'];?>/lytebox/close_grey.gif) no-repeat; }