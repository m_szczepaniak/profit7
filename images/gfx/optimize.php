<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 28.07.17
 * Time: 10:26
 */


if ($handle = opendir('./')) {
    echo "Directory handle: $handle\n";
    echo "Entries:\n";

    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if (false !== stristr($entry, '.png')) {
            echo "$entry\n";
            var_dump(exec('optipng '.$entry));
            var_dump(exec('pngout '.$entry));
        } elseif (false !== stristr($entry, '.jpg') || false !== stristr($entry, '.jpeg')) {
            echo "$entry\n";
            var_dump(exec('jpegoptim '.$entry.' --strip-all'));
        }
    }

    closedir($handle);
}