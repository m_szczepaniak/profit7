<?php
/**
* Plik jezykowy
* jezyk polski
*
* @author Marcin Korecki <korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/
$aConfig['lang']['common']['css'] = '?v14-09-2018';

$aConfig['lang']['common']['realization']	= 'realizacja i <a rel="nofollow" href="http://www.omnia.pl/systemy-zarzadzania-trescia-cms" title="CMS omnia.CMS - System Zarządzania Treścią i Strukturą Serwisu Internetowego" target="_blank">CMS</a>: ';
$aConfig['lang']['common']['powered_by']	= 'Powered by: ';

$aConfig['lang']['common']['breadcrumbs']	= '';
$aConfig['lang']['common']['breadcrumbs_separator']	= ' <span></span>';

$aConfig['lang']['common']['main_page']	= 'księgarnia profit';
$aConfig['lang']['common']['recommend_page']	= 'Poleć serwis znajomemu';
$aConfig['lang']['common']['go_back']	= '<img src="/images/gfx/pl/powrot.gif" alt="" />';
$aConfig['lang']['common']['news']	= 'nowości';
$aConfig['lang']['common']['go_home']	= 'Powrót do strony głównej';

$aConfig['lang']['common']['pogoda'] = 'Pogoda';
$aConfig['lang']['common']['strona_mowiaca'] = 'Strona Mówiąca';
$aConfig['lang']['common']['biuletyn_informacji_publicznej'] = 'Biuletyn Informacji Publicznej';
$aConfig['lang']['common']['choose'] = '-- wybierz --';
$aConfig['lang']['common']['all'] = '-- wszystkie --';
$aConfig['lang']['common']['okruszki'] = 'Przeglądasz: ';

$aConfig['lang']['common']['chngpasswd_1'] = "Zmiana hasła";
$aConfig['lang']['common']['login_form'] = "Logowanie";
$aConfig['lang']['common']['nowosci'] = "Nowości";
$aConfig['lang']['common']['promocje'] = "Promocje";
$aConfig['lang']['common']['recent'] = "Ostatnio odwiedzane";
$aConfig['lang']['common']['bestsellery'] = "Bestsellery";
$aConfig['lang']['common']['check_also'] = "Sprawdź również:";
$aConfig['lang']['common']['also_bought'] = "Klienci kupili również:";
$aConfig['lang']['common']['pakiety'] = "W pakiecie taniej";
$aConfig['lang']['common']['hh'] = "Happy Hour";
$aConfig['lang']['common']['indeks'] = "Indeks rzeczowy";
$aConfig['lang']['common']['serie'] = "Serie wydawnicze";
$aConfig['lang']['common']['authors'] = "Autorzy:";
$aConfig['lang']['common']['repository'] = "Przechowalnia";
$aConfig['lang']['common']['bookstores'] = "Księgarnie patronackie";
$aConfig['lang']['common']['cart'] = "Koszyk";
$aConfig['lang']['common']['rejestracja'] = "Rejestracja";
$aConfig['lang']['common']['packet_price'] = "Cena pakietu: ";
$aConfig['lang']['common']['discount'] = "Oszczędzasz: ";
$aConfig['lang']['common']['price'] = "Cena w Księgarni IT: ";
$aConfig['lang']['common']['price_right'] = "Cena: ";
$aConfig['lang']['common']['price_for_you'] = "Twoja cena: ";
$aConfig['lang']['common']['old_price'] = "Cena katalogowa: ";
$aConfig['lang']['common']['series_name'] = "Seria: ";
$aConfig['lang']['common']['wyszukiwarka'] = "Wyszukiwarka";
$aConfig['lang']['common']['authors'] = "Autorzy";
$aConfig['lang']['common']['tags_box'] = "Chmurka tagów";
$aConfig['lang']['common']['tags'] = "Tagi";
$aConfig['lang']['common']['last_added'] = "Ostatnio dodano";
$aConfig['lang']['common']['wyniki_wyszukiwania'] = "Wyniki wyszukiwania";
$aConfig['lang']['common']['access'] = "Dostęp: ";
$aConfig['lang']['common']['currency'] = " zł";
$aConfig['lang']['common']['currency_fraction'] = " gr.";
$aConfig['lang']['common']['kilo'] = " kg";
$aConfig['lang']['common']['free'] = "darmowy";
$aConfig['lang']['common']['bought'] = "wykupiony";
$aConfig['lang']['common']['bought_sms'] = "wykupiony (SMS - 12h)";
$aConfig['lang']['common']['to_logged'] = "Strona dostępna tylko dla zalogowanych użytkowników!";
$aConfig['lang']['common']['no_positions'] = "W chwili obecnej w tym dziale nie ma żadnych pozycji";
$aConfig['lang']['common']['toCart'] = "&raquo; Przejdź do koszyka";
$aConfig['lang']['common']['box_cart'] = "do koszyka";
$aConfig['lang']['common']['publisher'] = "Wydawnictwo:";
$aConfig['lang']['common']['item_details'] = "opis produktu";
$aConfig['lang']['common']['product_szt'] = "Sztuk: ";
$aConfig['lang']['common']['unavalaible'] = "<strong>Produkt niedostępny</strong>";
$aConfig['lang']['common']['shipment'] = "Wysyłamy w <strong>3 dni!</strong>";

$aConfig['lang']['common']['cena_w_ksiegarni'] = "Cena Profit24: ";
$aConfig['lang']['common']['cena_uzytkownika'] = "Twoja cena: ";
$aConfig['lang']['common']['cena_kod_rabatowy'] = "Cena z kodem: ";

$aConfig['lang']['common']['sorter_links']['nazwa'] = 'alfabetu';
/*$aConfig['lang']['common']['sorter_links']['nazwa_desc'] = 'nazwy malejąco';*/
$aConfig['lang']['common']['sorter_links']['cena'] = 'ceny rosnąco';
$aConfig['lang']['common']['sorter_links']['cena_desc'] = 'ceny malejąco';
$aConfig['lang']['common']['sorter_links']['data'] = 'najbardziej popularne';
$aConfig['lang']['common']['sorter_links']['order_by_status_old_style'] = 'najnowsze';
/*$aConfig['lang']['common']['sorter_links']['data_desc'] = 'daty malejąco';*/
$aConfig['lang']['common']['sort'] = '<strong>sortuj według:</strong>';

$aConfig['lang']['common']['filter_links']['avaible'] = 'dostępne';
$aConfig['lang']['common']['filter_links']['all'] = 'wszystkie';
$aConfig['lang']['common']['filter'] = '<strong>Pokaż:</strong>';

$aConfig['lang']['common']['captcha'] = "Kod z obrazka";
$aConfig['lang']['common']['captcha_err'] = "Wpisany kod i kod z obrazka się różnią!";

$aConfig['lang']['common']['more'] = "więcej";
$aConfig['lang']['common']['all'] = "wszystkie";
$aConfig['lang']['common']['see_all'] = "zobacz wszystkie";
$aConfig['lang']['common']['see_all_index'] = "zobacz pełny indeks";
$aConfig['lang']['common']['see_all_authors'] = "zobacz wszystkich";


// podpowiedzi
$aConfig['lang']['common']['hint_email'] = "Twój adres e-mail będzie jednocześnie loginem do Twojego konta.";
$aConfig['lang']['common']['hint_passwd'] = "Od 6 do 16 znaków. NIE MOŻE zawierać znaków narodowych oraz: \ / \'\' \' (ukośniki, cudzysłów, apostrof).";
$aConfig['lang']['common']['hint_phone'] = 'Dozwolone formaty: <br />xx xxx xx xx (np. 12 544 54 54)<br />xxxxxxxxx (np. 125445454)<br />xxx xxx xxx (np. 564 654 654)<br />xx xx xx xxx (np. 12 34 56 789)<br />xxx xx xx xx (np. 123 45 67 89)<br />xx xxxxxxx (np. 12 3456789)';
$aConfig['lang']['common']['hint_phone2'] = 'Telefon osoby odbierającej przesyłkę.<br />Ta informacja nie pojawi się na fakturze<br /><br />Dozwolone formaty: <br />xx xxx xx xx (np. 12 544 54 54)<br />xxxxxxxxx (np. 125445454)<br />xxx xxx xxx (np. 564 654 654)<br />xx xx xx xxx (np. 12 34 56 789)<br />xxx xx xx xx (np. 123 45 67 89)<br />xx xxxxxxx (np. 12 3456789)';
$aConfig['lang']['common']['hint_postal'] = 'W formacie 11-222, np. 00-203';
$aConfig['lang']['common']['hint_nip'] = 'W formacie 111-222-33-44 lub 111-22-33-444 lub 1112223344';
$aConfig['lang']['common']['hint_name'] = 'Imię osoby odbierającej przesyłkę.<br />Ta informacja nie pojawi się na fakturze';
$aConfig['lang']['common']['hint_name2'] = 'Imię osoby odbierającej przesyłkę.';
$aConfig['lang']['common']['hint_surname'] = 'Nazwisko osoby odbierającej przesyłkę.<br />Ta informacja nie pojawi się na fakturze';
$aConfig['lang']['common']['hint_surname2'] = 'Nazwisko osoby odbierającej przesyłkę.';
$aConfig['lang']['common']['hint_addr_name'] = 'Jeśli posiadasz kilka adresów. możesz nadać im nazwy. Np. &Prime;adres domowy&Prime;, &Prime;praca&Prime;.';
$aConfig['lang']['common']['loading'] = '<img src="/images/gfx/loader.gif" alt="Wczytywanie danych..." />';

// nazwa strony z glowna mapa serwisu
$aConfig['lang']['common']['sitemap']	= 'mapa serwisu';
$aConfig['lang']['common']['sitemap_page']	= 'mapa_serwisu';

$aConfig['lang']['common']['yes']	= 'Tak';
$aConfig['lang']['common']['no']	= 'Nie';

// noscript
$aConfig['lang']['common']['noscript']	= 'Dla pełnej funkcjonalności serwisu wymagana jest włączona obsługa JavaScript!';

$aConfig['lang']['common']['regulations']	= 'Wyrażam dobrowolną zgodę na przetwarzanie moich danych osobowych w celach marketingowych przez Profit M Spółka z ograniczoną odpowiedzialnością w Warszawie (02-305) Al. Jerozolimskie 134, zgodnie z ustawą o ochronie danych osobowych z dnia 29 sierpnia 1997 r. (tekst jednolity: Dz. U. z 2002 r., Nr 101, poz. 926 z późn. zm.). Zgoda może być odwołana w każdym czasie.';

// lightbox
$aConfig['lang']['lytebox']['image']	= 'Zdjęcie';
$aConfig['lang']['lytebox']['from'] = 'z';

// pager
$aConfig['lang']['pager']['current_page']	= 'strona';
$aConfig['lang']['pager']['from'] = 'z';
$aConfig['lang']['pager']['total_items']	= 'wszystkich produktów: ';
$aConfig['lang']['pager']['page'] = 'Strona';
$aConfig['lang']['pager']['page_dots'] = 'Strona:';
$aConfig['lang']['pager']['pages'] = 'Strony: ';
$aConfig['lang']['pager']['first_page'] = 'pierwsza';
$aConfig['lang']['pager']['previous_page'] = 'poprzednia';
$aConfig['lang']['pager']['next_page'] = 'następna';
$aConfig['lang']['pager']['last_page'] = 'ostatnia';
$aConfig['lang']['pager']['per_page'] = 'Rekordów na stronie:';
$aConfig['lang']['pager']['sites'] = 'Stron ';

// logowanie
$aConfig['lang']['common']['login_header']	= 'Logowanie';
$aConfig['lang']['common']['login']	= 'Login';
$aConfig['lang']['common']['email']	= 'Twój e-mail';
$aConfig['lang']['common']['passwd']	= 'Hasło';
$aConfig['lang']['common']['do_login']	= 'Zaloguj się';
$aConfig['lang']['common']['remember']	= 'Zapamiętaj mnie';

$aConfig['lang']['common']['register']	= 'Załóż konto';
$aConfig['lang']['common']['remind_passwd']	= 'Zapomniałem hasła';

$aConfig['lang']['common']['do_send']	= 'Wyślij';

// konto uzytkownika
$aConfig['lang']['user_account']['balance_info'] = 'Stan Twojego konta:';

$aConfig['lang']['form']['error_prefix']				= 'Podane pola nie zostały wypełnione lub wypełniono je niepoprawnie';
$aConfig['lang']['form']['error_postfix']				= 'Popraw podane pola!';
$aConfig['lang']['form']['required_fields'] 		= '<span>*</span> Pola, które muszą zostać wypełnione';
$aConfig['lang']['form']['required_fields_select'] 		= '<span>*</span> Pola, które muszą zostać zaznaczone';
$aConfig['lang']['form']['too_long_text'] 			= "Tekst nie może przekraczać %s znaków!";
$aConfig['lang']['form']['add_err']							= 'Wystąpił błąd! Spróbuj ponownie';

$aConfig['lang']['pager']['page']								= 'Strona';
$aConfig['lang']['pager']['page_dots']	= 'Strona:';
$aConfig['lang']['pager']['pages']							= 'Strony: ';
$aConfig['lang']['pager']['first_page']					= 'pierwsza';
$aConfig['lang']['pager']['previous_page']			= 'poprzednia';
$aConfig['lang']['pager']['next_page']					= 'następna';
$aConfig['lang']['pager']['last_page']					= 'ostatnia';
$aConfig['lang']['pager']['per_page']						= 'Rekordów na stronie:';

$aConfig['lang']['months'][1] = 'Styczeń';
$aConfig['lang']['months'][2] = 'Luty';
$aConfig['lang']['months'][3] = 'Marzec';
$aConfig['lang']['months'][4] = 'Kwiecień';
$aConfig['lang']['months'][5] = 'Maj';
$aConfig['lang']['months'][6] = 'Czerwiec';
$aConfig['lang']['months'][7] = 'Lipiec';
$aConfig['lang']['months'][8] = 'Sierpień';
$aConfig['lang']['months'][9] = 'Wrzesień';
$aConfig['lang']['months'][10] = 'Październik';
$aConfig['lang']['months'][11] = 'Listopad';
$aConfig['lang']['months'][12] = 'Grudzień';

$aConfig['lang']['months2'][1] = 'stycznia';
$aConfig['lang']['months2'][2] = 'lutego';
$aConfig['lang']['months2'][3] = 'marca';
$aConfig['lang']['months2'][4] = 'kwietnia';
$aConfig['lang']['months2'][5] = 'maja';
$aConfig['lang']['months2'][6] = 'czerwca';
$aConfig['lang']['months2'][7] = 'lipca';
$aConfig['lang']['months2'][8] = 'sierpnia';
$aConfig['lang']['months2'][9] = 'września';
$aConfig['lang']['months2'][10] = 'października';
$aConfig['lang']['months2'][11] = 'listopada';
$aConfig['lang']['months2'][12] = 'grudnia';

$aConfig['lang']['days'][0] = 'Niedziela';
$aConfig['lang']['days'][1] = 'Poniedziałek';
$aConfig['lang']['days'][2] = 'Wtorek';
$aConfig['lang']['days'][3] = 'Środa';
$aConfig['lang']['days'][4] = 'Czwartek';
$aConfig['lang']['days'][5] = 'Piątek';
$aConfig['lang']['days'][6] = 'Sobota';

$aConfig['lang']['today_is'] = 'dzisiaj jest';

/*-------------- KALENDARZ --------------*/
$aConfig['lang']['calendar']['Warning'] = 'Niepoprawny format daty!';
$aConfig['lang']['calendar']['AltPrevYear'] = 'poprzedni rok';
$aConfig['lang']['calendar']['AltNextYear'] = 'następny rok';
$aConfig['lang']['calendar']['AltPrevMonth'] = 'poprzedni miesiąc';
$aConfig['lang']['calendar']['AltNextMonth'] = 'następny miesiąc';
$aConfig['lang']['calendar']['weekdays'][1] = 'Nd';
$aConfig['lang']['calendar']['weekdays'][2] = 'Po';
$aConfig['lang']['calendar']['weekdays'][3] = 'Wt';
$aConfig['lang']['calendar']['weekdays'][4] = 'Śr';
$aConfig['lang']['calendar']['weekdays'][5] = 'Czw';
$aConfig['lang']['calendar']['weekdays'][6] = 'Pi';
$aConfig['lang']['calendar']['weekdays'][7] = 'So';
$aConfig['lang']['calendar']['clear_date'] = 'Wyczyść pole daty';

/*----- Langi metod transportu ------------*/
$aConfig['lang']['delivery']['paczkomaty_24_7'] = 'PACZKOMATY';
$aConfig['lang']['delivery']['opek-przesylka-kurierska'] = 'KURIER';
$aConfig['lang']['delivery']['odbior-osobisty'] = 'ODBIÓR OSOBISTY';
$aConfig['lang']['delivery']['ruch'] = 'RUCH';
$aConfig['lang']['delivery']['poczta_polska'] = 'POCZTA-POLSKA (odbiór na poczcie)';
$aConfig['lang']['delivery']['poczta-polska-doreczenie-pod-adres'] = 'POCZTA-POLSKA (doręczenie pod adres)';
$aConfig['lang']['delivery']['orlen'] = 'PACZKA NA ORLENIE';

/* --- Rabaty --- */
$aConfig['lang']['discount']['info']	= '<b>%s%% rabatu</b> - %s';

$aConfig['lang']['discount']['promotion_discount']['default']	= '%s';
$aConfig['lang']['discount']['promotion_discount']['limited']	= 'Promocja "%s" ograniczona limitem rabatu zgodnie
   z&nbsp;regulaminem pod adresem <a style="color:#c30500; text-decoration: underline;" href="/ceny-i-rabaty" target="_blank">https://www.profit24.pl/ceny-i-rabaty</a>';

$aConfig['lang']['discount']['code_discount']['default']	= 'Kod rabatowy';
$aConfig['lang']['discount']['code_discount']['limited']	= 'Kod rabatowy ograniczony limitem rabatu zgodnie
   z&nbsp;regulaminem pod adresem <a style="color:#c30500; text-decoration: underline;" href="/ceny-i-rabaty" target="_blank">https://www.profit24.pl/ceny-i-rabaty</a>';

$aConfig['lang']['discount']['code_discount_additional']['default']	= 'Dodatkowy kod rabatowy';
$aConfig['lang']['discount']['code_discount_additional']['limited']	= 'Dodatkowy kod rabatowy ograniczony limitem rabatu zgodnie
   z&nbsp;regulaminem pod adresem <a style="color:#c30500; text-decoration: underline;" href="/ceny-i-rabaty" target="_blank">https://www.profit24.pl/ceny-i-rabaty</a>';

$aConfig['lang']['discount']['general_discount']['default']	= 'Rabat ogólny';
$aConfig['lang']['discount']['general_discount']['limited']	= 'Rabat ogólny'; // nie musi klient o tym wiedzieć

$aConfig['lang']['discount']['user_discount']['default']	= 'Rabat użytkownika';
$aConfig['lang']['discount']['user_discount']['limited']	= 'Rabat użytkownika ograniczony limitem rabatu zgodnie
   z&nbsp;regulaminem pod adresem <a style="color:#c30500; text-decoration: underline;" href="/ceny-i-rabaty" target="_blank">https://www.profit24.pl/ceny-i-rabaty</a>';

$aConfig['lang']['discount']['publishersseries_discount_default']['default']	= 'Rabat na %s';
$aConfig['lang']['discount']['publishersseries_discount']['series']	= 'serię wydawniczą';
$aConfig['lang']['discount']['publishersseries_discount']['publisher']	= 'wydawnictwo';
?>