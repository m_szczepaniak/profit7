<?php
/**
 * Skrypt wyświetla aktualne statystyki dla pakujących :)
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-10-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
// set_time_limit(90800); // maksymalny czas wykonywania 3 godziny - 
set_time_limit(272400);
ini_set("memory_limit", '1G');

$sSql = "
  SELECT COUNT(id) 
  FROM orders 
  WHERE 
    order_status <> '5' AND
    internal_status <> '0' AND
    IF(shipment_date_expected IS NOT NULL, shipment_date_expected <= CURDATE(), shipment_date <= CURDATE())
  ";
$iCountAll = Common::GetOne($sSql);
$iSendToday = 0;

$aPacks = Common::Query("SET SESSION group_concat_max_len = 1000000000000000000;");

$sSql = "
  SELECT IF(shipment_date > '2013-10-03', shipment_date, NULL) AS 'PLANOWANA DATA WYSYŁKI', COUNT(id) as 'LICZBA ZAMÓWIEŃ', GROUP_CONCAT(order_number SEPARATOR ',') AS ZAMOWIENIA
  FROM orders 
  WHERE 
    order_status <> '5' AND
    DATE(status_3_update) = CURDATE()
  GROUP BY IF(shipment_date > '2013-10-03', shipment_date, NULL) WITH ROLLUP
  
  ";
$aPacks = Common::GetAll($sSql);

echo date('d.m.Y H:i:s');
echo '<br /><br /> ZAMÓWIENIA ZATWIERDZONE DZISIAJ:';
echo '<table border=1>';
echo '<tr>';
echo '<td>PLANOWANA DATA WYSYŁKI</td><br /><br />';
echo '<td>LICZBA ZAMÓWIEŃ</td>';
echo '<td>ZAMÓWIENIA</td>';
echo '</tr>';
$dToday = date('Y-m-d');
foreach ($aPacks as $iKey => $aPack) {
  echo '<tr>';
  if (!isset($aPacks[$iKey+1])) {
    echo '<td style="padding: 6px">SUMA</td>';
  } else {
    echo '<td style="padding: 6px">'.($aPack['PLANOWANA DATA WYSYŁKI'] == NULL ? 'brak informacji' : formatDateClient($aPack['PLANOWANA DATA WYSYŁKI'])).'</td>';
  }
  echo '<td style="padding: 6px">'.$aPack['LICZBA ZAMÓWIEŃ'].'</td>';
  echo '<td>'.$aPack['ZAMOWIENIA'].'</td>';
  echo '</tr>';
  if ($aPack['PLANOWANA DATA WYSYŁKI'] == $dToday) {
    $iSendToday = $aPack['LICZBA ZAMÓWIEŃ'];
  }
}
echo '</table>';

echo '<br /><br />Liczba paczek z dzisiejszą planowaną datą wysłania: <b>'.$iCountAll.'</b>, zatwierdzono <b>'.$iSendToday.'</b>';

$sSql = "
  SELECT DISTINCT O.id, O.order_number, O.order_status
  FROM orders AS O
  LEFT JOIN orders_items AS OI
    ON O.id = OI.order_id 
      AND OI.preview = '1' 
      AND OI.status = '-1'
  WHERE
    OI.id IS NULL AND
    O.order_status <> '4' AND
    O.order_status <> '5' AND
    O.internal_status <> '0' AND
    IF(O.shipment_date_expected IS NOT NULL, O.shipment_date_expected <= CURDATE(), O.shipment_date <= CURDATE())  
  ORDER BY O.order_status
";
dump(Common::GetAll($sSql));
?>