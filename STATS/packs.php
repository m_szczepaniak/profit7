<?php
/**
 * Skrypt wyświetla aktualne statystyki dla pakujących :)
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-10-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
// set_time_limit(90800); // maksymalny czas wykonywania 3 godziny - 
set_time_limit(272400);
ini_set("memory_limit", '1G');

$sSql = "
  SELECT COUNT(DISTINCT O.id) 
  FROM orders AS O
  LEFT JOIN orders_items AS OI
    ON O.id = OI.order_id 
      AND OI.preview = '1' 
      AND OI.status = '-1'
  WHERE
    OI.id IS NULL AND
    O.order_status <> '4' AND
    O.order_status <> '5' AND
    O.internal_status <> '0' AND
    IF(O.shipment_date_expected IS NOT NULL, O.shipment_date_expected <= CURDATE(), O.shipment_date <= CURDATE())
  ";
$iCountAll = Common::GetOne($sSql);
$iSendToday = 0;


$sSql = "
  SELECT COUNT(DISTINCT O.id)
  FROM orders AS O
  WHERE 
    O.internal_status = '5' AND
    O.order_status <> '5' AND 
    O.transport_id = '1'
  ";
$iToSendOpek = Common::GetOne($sSql);

$sSql = "
  SELECT COUNT(DISTINCT O.id)
  FROM orders AS O
  WHERE 
    O.internal_status = '5' AND
    O.order_status <> '5' AND 
    O.transport_id = '3'
  ";
$iToSendPaczkomaty = Common::GetOne($sSql);

$sSql = "
  SELECT COUNT(DISTINCT O.id)
  FROM orders AS O
  JOIN orders_to_users AS OTU
    ON OTU.order_id = O.id
  WHERE 
    order_status <> '5' AND
    DATE(status_3_update) = CURDATE()
  ";
$iSendToday = Common::GetOne($sSql);

$sSql = "
  SELECT COUNT(DISTINCT O.id)
  FROM orders AS O
  JOIN orders_to_users AS OTU
    ON OTU.order_id = O.id
  WHERE 
    order_status <> '5' AND
    DATE(status_3_update) = CURDATE() AND
    O.transport_id = '3'
  ";
$iSendTodayPaczkomaty = Common::GetOne($sSql);

$sSql = "
  SELECT COUNT(DISTINCT O.id)
  FROM orders AS O
  JOIN orders_to_users AS OTU
    ON OTU.order_id = O.id
  WHERE 
    order_status <> '5' AND
    DATE(status_3_update) = CURDATE() AND
    O.transport_id = '1'
  ";
$iSendTodayOPEK = Common::GetOne($sSql);

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/Orders/ShipmentDateExpected.class.php');
$oShipmentDateExpected = new \ShipmentDateExpected($pDbMgr, $aConfig);
$dFirstWorkingDay = $oShipmentDateExpected->getDateAfterWorkingDays(1, $oShipmentDateExpected->getFreeDays());

$sSql = "
  SELECT COUNT(DISTINCT O.id) 
  FROM orders AS O
  LEFT JOIN orders_items AS OI
    ON O.id = OI.order_id 
      AND OI.preview = '1' 
      AND OI.status = '-1'
  WHERE
    OI.id IS NULL AND
    O.order_status <> '4' AND
    O.order_status <> '5' AND
    O.internal_status <> '0' AND
    IF(O.shipment_date_expected IS NOT NULL, O.shipment_date_expected <= '".$dFirstWorkingDay."', O.shipment_date <= '".$dFirstWorkingDay."')
  ";
$iPlannedAll = (Common::GetOne($sSql) - $iCountAll);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Statystyki pakujących</title>
<script type="text/javascript">
  setTimeout("location.reload(true);", 600000);
</script>

<style>
  body, html{height:100%; margin: 0;}
  div{height:100%}
  table{
    background: green;
    height: 100%; 
    width: 100%;
    font-size: 80px; 
    text-align: center;
  }  
  td {
        border: 1px solid black;
  }
  tr:first-child td{
    background:blue;
  }
  tr:last-child td{
    background:red
  }
</style>

</head>
<body width="100%" height="100%">
  <div style='position:absolute; right: 5px; top: 5px; color: #fff;'><?php echo date('d.m.Y H:i:s'); ?></div>
<?php
echo '<table>';
echo '<tr>';
echo '<td width="50%">PLAN</td>';
echo '<td colspan="2">'.$iCountAll.'</td>';
echo '</tr>';
echo '<tr>';
echo '<td style="background: red;">POZOSTAŁO</td>';
echo '<td style="background: red;" colspan="2">'.($iCountAll - $iSendToday).'</td>';
echo '</tr>';
echo '<tr style="background: #84ad94;">';
echo '<td>WYSYŁKA</td>';
echo '<td style="background: #58A078;"><span style="font-size: 32px;">O:</span> '.$iToSendOpek.'</td>';
echo '<td style="background: #53C284;"><span style="font-size: 32px;">P:</span> '.$iToSendPaczkomaty.'</td>';
echo '</tr>';
echo '<tr>';
echo '<td>SPAKOWANO</td>';
echo '<td style="background: #27a721;"><span style="font-size: 32px;">O:</span> '.$iSendTodayOPEK.'</td>';
echo '<td style="background: #0ad400;"><span style="font-size: 32px;">P:</span> '.$iSendTodayPaczkomaty.'</td>';
echo '</tr>';
echo '<tr height="10">';
echo '<td style="background: grey; font-size: 34px !important;">PLAN WYSYŁKI '.preg_replace('/(\d{4})-(\d{2})-(\d{2})/', '$3.$2.$1', $dFirstWorkingDay).'</td>';
echo '<td style="background: grey;" colspan="2">'.($iPlannedAll).'</td>';
echo '</tr>';
echo '</table>';

?>
</body>
</html>