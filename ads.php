<?php
include_once('omniaCMS/config/common.inc.php');
// nie wlaczaj sesji
$aConfig['common']['use_session'] = false;
include_once ('omniaCMS/config/ini.inc.php');


/**
 * Funkcja odpowiada za ziwkeszenie liczby klikniec dla bannera
 * 
 * @param	integer	$iLId	- Id wersji jezykowej
 * @param	integer	$iPId	- Id strony bannerow
 * @param	integer	$iId	- Id bannera
 */
function increaseHits($iLId, $iPId, $iId) {
	global $aConfig;
	
	$sSql = "UPDATE ".$aConfig['tabls']['prefix'].'banners'."
					 SET hits = hits + 1
					 WHERE page_id = ".$iPId." AND
					 			 id = ".$iId;
	Common::Query($sSql);
} // end of increaseHits() function

/**
 * Funkcja przekierowuje przegladarke uzytkownika na adres
 * zdefiniowany dla bannera
 * 
 * @param	string	$sUrl	- adres na ktory nastapi przekierowanie
 */
function redirect($sUrl) {
	header("Location: ".str_replace('{_;_}', ':', $sUrl));
} // end of redirect() function


if (!empty($_GET['data'])) {
	$aData = explode(':', base64_decode($_GET['data']));
	if (is_numeric($aData[0]) && is_numeric($aData[1]) &&
			is_numeric($aData[2]) && !empty($aData[3])) {
		// zwiekszenie liczby kliniec
		increaseHits($aData[0], $aData[1], $aData[2]);
		// przekierowanie na odpowiednia strone
		redirect($aData[3]);
	}
}
?>