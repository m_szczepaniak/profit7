<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-23 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$iProductId = intval($_GET['product_id']);
if ($iProductId > 0) {
  include_once('omniaCMS/config/common.inc.php');
  // nie wlaczaj sesji
  $aConfig['common']['use_session'] = false;
  include_once ('omniaCMS/config/ini.inc.php');
  
  $sSql = "SELECT *
          FROM ".$aConfig['tabls']['prefix']."products_images
          WHERE product_id = ".$iProductId." 
          LIMIT 1";
  $aValue = Common::GetRow($sSql);
  if(!empty($aValue)){
    if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'])){
      $sImage = 'images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'];
    }
    else if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/'.$aValue['photo'])){
      $sImage = 'images/photos/'.$aValue['directory'].'/'.$aValue['photo'];
    }
    else if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'])){
      $sImage = 'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];
    }
  }
  if ($sImage != '') {
    header('Content-Disposition: attachment; filename="'.$aValue['photo'].'"');
    echo file_get_contents($sImage);
  }
}
